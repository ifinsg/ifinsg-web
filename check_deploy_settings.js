var input = []

let global_constants = require('./global/constants')

console.log("Checking deployment settings ...");


process.argv.forEach(function (val, index, array) {
    // console.log(index + ': ' + val);
    if (index === 2){       // The first 2 are the first 2 arguments before "live" or "staging"
        input.push(val)
    }
    // console.log(input)
});

console.log(input);


if (global_constants.staging && input[0] === "staging") {
    // OK
} else if (!global_constants.staging && input[0] === "live") {
    // OK
} else if (global_constants.staging && input[0] === "live") {
    throw "application is on STAGING mode!"
} else if (!global_constants.staging && input[0] === "staging") {
    throw "application is on LIVE mode!"
} else {
    throw "ERROR - Please check deployment settings!"
}


console.log("Deployment settings OK")
