// Source: https://www.npmjs.com/package/react-mobile-datepicker#dateconfig

import React, { Component } from 'react';
import { NoSsr, Button, Typography, AppBar, Tabs, Tab, TextField, FormControl, InputLabel, MenuItem, Select, Card, FormControlLabel, Checkbox, Fab, Divider, IconButton, Grid, FormLabel, RadioGroup, Radio, Chip } from '@material-ui/core';

// import 'rmc-picker/assets/index.css';    // Imported in <Head/>
// import 'rmc-date-picker/assets/index.less';
// import 'rmc-picker/assets/popup.css';    // Imported in <Head/>
import PopPicker from './Popup.js';
import DatePicker from './DatePicker';
import zhCn from './locale/zh_CN';
import enUs from './locale/en_US';
import { cn, format, minDate, maxDate, now } from './utils';
import Head from 'next/head';
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import { constants } from 'crypto';
import Helpers from '../../../../global/helpers'
import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";

// const { detect } = require('detect-browser');
// const browser = detect();

let inputRef = ""

class DatePickerRMC extends Component {
  static defaultProps = {
    mode: 'datetime',
    // locale: cn ? zhCn : enUs,
    locale: zhCn,

  };

  constructor(props) {
    super(props);
    this.state = {
      // date: null,
      browser_is_ie: false,
      shrink_textfield_date: false
    }
  }

  componentDidMount = () => {
    // if (browser.name === 'ie') {
    //   this.setState({
    //     browser_is_ie: true
    //   })
    // }
  }

  onChange = (date) => {
    console.log('onChange', format(date));
    // this.setState({
    //   date,
    // });
    this.props.handleDateChange(date)
  }

  onDismiss = () => {
    console.log('onDismiss');
  }

  show = () => {
    console.log('my click');
  }

  focusOnThis = () => {
    console.log("focusOnThis")
    // if (this.state.shrink_textfield_date) {
    // setTimeout(() => {
      inputRef.focus();
      // input && input.focus();
      console.log("FOCUSINGGGG")
    // }, 5000)

    // }
  }

  renderDatePicker = () => {

    const props = this.props;
    const { date } = this.state;
    const datePicker = (
      <DatePicker
        minDate={new Date(this.props.minDate)}
        maxDate={this.props.maxDate ? new Date(this.props.maxDate) : new Date()}
        mode={'date'}
      />
    );


    if (isBrowser) { 
      return (
        <form style={{ display: 'flex', flexWrap: 'wrap' }} noValidate style={{ marginTop: "2px", marginBottom: "0px" }}>
          <TextField
            inputRef={(input) => { inputRef = input }}
            //   console.log("REFERENCEEEEEEEEEEEEEEEEEEEEEE")
            //   this.focusUsernameInputField(input)
            // }}
            id="date"
            label={this.props.placeholder}
            type= "date"
            // type={this.state.shrink_textfield_date ? "date" : "text"}
            // onClick={() => {
            //   this.setState({
            //     shrink_textfield_date: true
            //   }, () => {
            //     setTimeout(()=>{
            //       this.focusOnThis()
            //     },3000)
            //   })

            // }}

            // onChange={() => {
            //   this.props.handleDateChange(date)
            // }
            // }

            onChange={(e) => {
              console.log(e.target.value)
              console.log("DATE CHANGEDDDD ()")
              let date_dd_mm_yyyy = e.target.value      // format: dd-mm-yyyy
              // console.log("receiving date input as " + date_dd_mm_yyyy + ")")
              let year_string = date_dd_mm_yyyy.split('-')[0]
              let month_string = date_dd_mm_yyyy.split('-')[1]
              let date_string = date_dd_mm_yyyy.split('-')[2]
              let new_date_string = year_string + "-" + month_string + "-" + date_string
              // console.log("changing date to: new Date(" + new_date_string + ")")
              // console.log("showing value as:" + Helpers.displayDate(new Date(new_date_string)))
              this.props.handleDateChange(new Date(new_date_string))
            }}

            value={this.props.date === "" ? "" : Helpers.dateStringFormattedyyyyMMdd(this.props.date)}

            style={{ marginTop: "1px", marginBottom: "0px" }}
            fullWidth
            InputLabelProps={{
              shrink: true
              // shrink: this.state.shrink_textfield_date,
            }}
            InputProps={{
              // readOnly: true,
            }}
          />
        </form>
      )
    } else {
      return (
        <MuiThemeProvider theme={this.getMuiTheme()}>
          <div style={{}}>
            <PopPicker
              style={
                // isBrowser ? { height: this.props.height ? this.props.height : "320px", maxWidth: "500px", left: "50%", marginLeft: "-250px" }
                //   :
                { height: this.props.height ? this.props.height : "320px" }
              }
              datePicker={datePicker}
              transitionName="rmc-picker-popup-slide-fade"
              maskTransitionName="rmc-picker-popup-fade"
              // title="Date picker"
              // date={new Date(this.props.date === "" ? this.props.defaultDate : this.props.date)}
              date={this.props.date === "" ? this.props.defaultDate ? new Date(this.props.defaultDate) : new Date() : new Date(this.props.date)}

              onDismiss={this.onDismiss}
              onChange={this.onChange}
            >
              {/* <button onClick={this.show}>{date && format(date) || 'open'}</button> */}

              <form style={{ display: 'flex', flexWrap: 'wrap' }} noValidate style={{ marginTop: "2px", marginBottom: "0px" }}>
                <TextField
                  id="date"
                  label={this.props.placeholder}
                  // type="date"
                  // onChange={this.handleFieldChange("personal_dob")}
                  // onChange={() => { console.log("DATE CHANGEDDDD ()") }}

                  value={this.props.date === "" ? "" : Helpers.displayDate(this.props.date)}

                  style={{ marginTop: "1px", marginBottom: "0px" }}
                  fullWidth
                  InputLabelProps={{
                    // shrink: true,
                  }}
                  InputProps={{
                    readOnly: true,
                  }}
                />
              </form>

            </PopPicker>

          </div>
        </MuiThemeProvider>

      )
    }
  }


  getMuiTheme = () => {
    return createMuiTheme({
      overrides: {
        MuiInputBase: {
          input: {
            color: "transparent",
            textShadow: "0 0 0 black",
          }
        }
        // MuiExpansionPanelDetails: {
        //   root: {
        //     display: "block",
        //     padding: "0px 15px 0px 15px",
        //   }
        // },

      }
    });
  }




  render() {
    // console.log(this.props.date)
    // console.log(typeof (this.props.date))
    // console.log("" === (this.props.date))
    // console.log("THAT WAS THE CONTROLLING DATEEEEEEEEEEEE")




    return (
      <div>


        <Head>
          <link href="/rmc-picker-index.css" rel="stylesheet" />
          <link href="/rmc-picker-popup.css" rel="stylesheet" />
          {/* <link href="/rmc-date-picker-index.less" rel="stylesheet" /> */}
          <link href="/rmc-date-picker-index.css" rel="stylesheet" />

        </Head>



        <NoSsr>

          {this.renderDatePicker()}

        </NoSsr>


      </div>


    )
  }
}



export default DatePickerRMC;