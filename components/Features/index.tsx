import * as React from 'react';
import Change from 'components/icons/Change';
import Abc from 'components/icons/Abc';
import Law from 'components/icons/Law';
import Pizza from 'components/icons/Pizza';
import Revenue from 'components/icons/Revenue';
import Switch from 'components/icons/Switch';
import Shield from 'components/icons/Shield';
import Phone from 'components/icons/Phone';

import PriorityHighIcon from '@material-ui/icons/PriorityHigh';
import IconButton from '@material-ui/core/IconButton';
import styled from 'styled-components';
import { Modal } from '@material-ui/core';
import Fair from './template/Fair';
import Simple from './template/Simple';
import Clear from './template/Clear';
import Convenient from './template/Convenient';
import Considerate from './template/Considerate';
import Honorable from './template/Honorable';
import Friendly from './template/Friendly';
import MoneyBackGuarantee from './template/MoneyBackGuarantee';

import CloseIcon from '@material-ui/icons/Close';
import { useTheme, createStyles } from '@material-ui/core/styles';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import { isBrowser } from 'react-device-detect';

export interface IFeaturesProps extends WithStyles {}

const styles = (theme) => {
    let width = '85%';

    if (isBrowser) {
        width = '60%';
    }

    return createStyles({
        paper: {
            position: 'absolute',
            width: width, // theme.spacing.unit * 50,
            backgroundColor: theme.palette.background.paper,
            boxShadow: theme.shadows[5],
            padding: theme.spacing.unit * 4,
            outline: 'none',
            overflow: 'auto',
            maxHeight: '90%'
        }
    });
};

const Features: React.FC<IFeaturesProps> = (props: IFeaturesProps) => {
    const { classes } = props;
    const data = [
        {
            id: 'Fair',
            title: 'Fair',
            abstract: 'Make a 2nd Claim with us for the same accident / incident!',
            icon: <Law color={'#8b0000'} />
        },
        {
            id: 'Simple',
            title: 'Simple',
            abstract: 'Every policy is exactly as described, with no hidden exclusions.',
            icon: <Abc color={'#FF8C00'} />
        },
        {
            id: 'Clear',
            title: 'Clear',
            abstract:
                'Every $ of premium we collect is either paid out as a Claim, or returned to users as PremiumBack at the end of each cycle.',
            icon: <Change color={'#007FFF'} />
        },
        {
            id: 'Convenient',
            title: 'Convenient',
            abstract:
                'File a claim just from your phone. Take Photos. Tell us what happened. Call us if you prefer to talk!',
            icon: <Phone color={'#4a4a4a'} />
        },
        {
            id: 'Considerate',
            title: 'Considerate',
            abstract:
                'Never worry about Cash-flows, because some money comes back quickly every 3 months, even when you make 0 claims or low claims.',
            icon: <Revenue color={'#008453'} />
        },
        {
            id: 'Honorable',
            title: 'Honorable',
            abstract:
                'If none of the users of a cover makes a claim, you get back 100% of your premiums at the end of the cycle!',
            icon: <Pizza color={'#FFD60B'} />
        },
        {
            id: 'Friendly',
            title: 'Friendly',
            abstract:
                'Need to stop your cover? Want to re-continue your cover? Just 1 button to do so instantly. No preaching, no nagging, no forms to fill.',
            icon: <Switch color={'grey'} />
        },
        {
            id: 'MoneyBackGuarantee',
            title: 'Money back Guarantee',
            abstract: 'Not satisfied? Get a refund equivalent to 100% of the premiums you paid for your 1st cycle.',
            icon: <Shield color={'#90ee90'} />
        }
    ];

    const [tip, setTip] = React.useState(null);
    // console.log(tip)

    // const descriptionElementRef = React.useRef(null);
    // const theme = useTheme();
    // React.useEffect(() => {
    //     if (tip) {
    //         const { current: descriptionElement } = descriptionElementRef;
    //         if (descriptionElement !== null) {
    //             descriptionElement.focus();
    //         }
    //     }
    // }, [tip]);

    return (
        <Wrapper>
            <h1>Why Cyclic insurance</h1>
            <p>Check out these 8 amazing facts!</p>
            {data.map((item) => {
                return (
                    <div className="feature" key={item.id}>
                        <div className="text">
                            <div className="icons">
                                {/* <Blackboard /> */}
                                {item.icon}
                            </div>
                            <div className="group">
                                <div>
                                    <span className="group-name">{item.title}</span>
                                </div>
                                <p>{item.abstract}</p>
                            </div>
                        </div>
                        <div className="tips">
                            <IconButton
                                size="small"
                                style={{ background: 'gray', color: '#FFF', transform: 'scale(0.7)' }}
                                onClick={() => {
                                    setTip(item);
                                }}
                            >
                                <PriorityHighIcon fontSize="small" />
                            </IconButton>
                        </div>
                    </div>
                );
            })}

            <Modal
                style={{ margin: '20px' }}
                open={tip ? true : false}
                onClose={() => {
                    setTip(null);
                }}
            >
                <div
                    style={{ top: '50%', left: '50%', transform: 'translate(-50%, -50%)', position: 'relative' }}
                    className={classes.paper}
                >
                    <IconButton
                        onClick={() => {
                            setTip(null);
                        }}
                        size="small"
                        style={{ position: 'absolute', right: '10px', top: '10px' }}
                    >
                        <CloseIcon />
                    </IconButton>
                    {tip && tip.id === 'Fair' && <Fair />}
                    {tip && tip.id === 'Simple' && <Simple />}
                    {tip && tip.id === 'Clear' && <Clear />}
                    {tip && tip.id === 'Convenient' && <Convenient />}
                    {tip && tip.id === 'Considerate' && <Considerate />}
                    {tip && tip.id === 'Honorable' && <Honorable />}
                    {tip && tip.id === 'Friendly' && <Friendly />}
                    {tip && tip.id === 'MoneyBackGuarantee' && <MoneyBackGuarantee />}
                </div>
            </Modal>
        </Wrapper>
    );
};

export default withStyles(styles)(Features);

const Wrapper = styled.div`
    > h1 {
        font-family: Merriweather;
    }
    /* @media (max-width: 360px){
        width:100%;
        padding: 0 0 0 10px;
        .feature {
            .text {
                > p {
                    margin: 0;
                }
            }
        } 
    }

    @media (min-width: 360px) and (max-width: 414px){
        width:100%;
        padding: 0 0 0 10px;
        .feature {
            .text {
                > p {
                    margin: 0;
                }
            }
        } 
    } */

    @media (max-width: 479px) {
        width: 100%;
        /* padding: 0 0 0 10px; */
        padding: 0 10px;
        box-sizing: border-box;
        .feature {
            .text {
                .group {
                    > p {
                        margin: 0 !important;
                    }
                }
            }
        }
    }

    width: 800px;
    margin: 0 auto;

    > h1 {
        /* font-family: Merriweather, Georgia, serif; */
        font-size: 42px;
        font-weight: 400;
        line-height: 1.3;
        margin: 30px 0 0 0;
        padding: 0;
        text-align: center;
        color: #4a4a4a;
    }

    > p {
        font-family: Lato, 'Helvetica Neue', Arial, sans-serif;
        font-size: 24px;
        margin-top: 30px;
        color: #248b25;
        text-align: center;
    }

    .feature {
        display: flex;
        justify-content: space-between;
        margin-top: 50px;
        padding:0 15px;
        /* padding: 0 10px; */

        .text {
            display: flex;

            .icons {
                max-width: 40px;
                min-width: 40px;
                margin-right: 20px;
                margin-top: 36px;
                > svg {
                    width: 100%;
                    height: 100%;
                }
            }

            .group {
                display: flex;
                flex-flow: column;

                .group-tick {
                    margin-top: 6px;
                    margin-right: 10px;
                    width: 24px;
                }
                .group-name {
                    font-weight: bold;
                    font-size: 24px;
                    color: #7cda23;
                }

                > p {
                    text-align: left;
                    font-size: 20px;
                    margin: 0;
                    padding: 0;
                }
            }
        }

        .tips {
            /* display: flex;
            flex-direction: column;
            justify-content: center; */
        }
    }
`;
