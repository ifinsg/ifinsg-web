import * as React from 'react';
import { Wrapper } from './styled';

export interface IClearProps {
}

export default function Clear(props: IClearProps) {
    return (
        <Wrapper>
            <h2>2 - How does Cyclic insurance work?</h2>
            <p>
            We collect premiums + an <b>Admin Fee</b> from each user.
            </p>
            <p>
            Cyclic insurance only collects premiums as a pool in order to pay out claims, thus we treat collected premiums as if they were still your money, and return all premiums unused for claims, back to users as <b>PremiumBack</b> after the end of every cycle.
            </p>
            <p>
            Every dollar of premiums we collect, is only for 2 purposes: <b>PremiumBack</b> and Claims.
            </p>
            <p>
            The only income we earn as a business, is a flat and transparent monthly <b>Admin Fee</b> per policy per user.
            </p>
            <p>
            As iFinSG only earns a monthly <b>Admin Fee</b> per policy per user, we love all users equally, instead of focusing on wealthier users!
            </p>
        </Wrapper>
    );
}
