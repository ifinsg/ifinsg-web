import * as React from 'react';
import { Wrapper } from './styled';

export interface IConsiderateProps {
}

export default function Considerate(props: IConsiderateProps) {
    return (
        <Wrapper>
            <h2>3 - How is iFinSG different from a traditional insurance company?</h2>
            <p>
                iFinSG is an InsurTech firm which runs a P2P insurance system that focuses on the welfare of users, not the profit of shareholders.
            </p>
            <p>
                Traditional insurance companies keep the money they don’t pay out in claims. This means that every additional $1 paid out as Claims, translates to $1 less in underwriting Profit.</p>
            <p>
                We are structured to prevent any conflict of interest, by making sure that all premiums unused for claims, are given back to our users at the end of every cycle as <b>PremiumBack</b>. We gain nothing by delaying or denying claims.
                </p>
            <p>
                <b>PremiumBack</b> is given to 2 types of users: users who make 0 claims, and users who make low claims.</p>
            <p>
                Our proprietary algorithm is structured so that the lower the total claims you make, the higher the <b>PremiumBack</b> you will qualify for each cycle. If you made 0 claims, you’ll get the highest level of <b>PremiumBack</b>.</p>
            <p>
                As paying claims is our core priority, occasionally when premiums for a policy are fully paid out as claims, users may receive less <b>PremiumBack</b> than normal, however at all times do be assured that:</p>
            <p>
                A user who made 0 claims will always receive a <b>PremiumBack</b>, <u>guaranteed to be at least 10% of your premiums placed with us.</u></p>
            <p>
                A user who made low claims will always receive a <b>PremiumBack</b>, <u>guaranteed to be at least 5% of your premiums placed with us.</u></p>
            <p>
                You enjoy amazing protection with us (nobody would try to deny your claim), everything is based on real claims data, and there’s always money coming back to you!</p>
        </Wrapper>
    );
}
