import * as React from 'react';
import { Wrapper } from './styled';

export interface IEasyProps {
}

export default function Easy(props: IEasyProps) {
    return (
        <Wrapper>
            <h2>12 - How do I make a Claim?</h2>
            <p>
                We use an online claims system, which is fast and convenient for you.
            </p>
            <p style={{ textAlign: 'center' }}>
                [Cyclic: Small Accident]
            </p>
            <p>
                When you need to make a claim, just:
            </p>
            <p>
                1. Tell us:
            </p>
            <ul>
                <ol>
                    (i) What happened in the incident
                    </ol>
                <ol>
                    (ii) Who was the SMC-registered / TCMPB-registered Doctor you consulted
                    </ol>
                <ol>
                    (iii) Which clinic / hospital you visited
                    </ol>
            </ul>
            <p>
                2. Upload photos of:
            </p>
            <ul>
                <ol>
                    (i) Paid Receipt
                    </ol>
                <ol>
                    (ii) Itemized Invoice of treatment and medicine
                    </ol>
                <ol>
                    (iii) MC with diagnosis / Letter of diagnosis
                    </ol>
            </ul>
            <p>
                ( You must make sure the Doctor writes clearly the <u>Date of accident</u> + <u>Description of accident</u>, on the Receipt / Invoice / MC with diagnosis / Letter of diagnosis. )
            </p>
            <p>
                SMC is the Singapore Medical Council ( a statutory board under the Ministry of Health ), governed under Section 5 of the Medical Registration Act (Cap 174). You can check if your doctor is SMC registered, via this link:
                <a style={{ color: "#7cda24" }} href="https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC" target="_blank">https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC</a>.
            </p>

            <p>
                TCMPB is the Traditional Chinese Medicine Practitioners Board ( a statutory board under the charge of the Ministry of Health ), under the Traditional Chinese Medicine Practitioners Act 2000, governed under the Traditional Chinese Medicine Practitioners Act (Cap. 333A). You can check if your doctor is TCMPB registered, via this link:
                <a style={{ color: "#7cda24" }} href="https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=TCM" target="_blank">https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=TCM</a>.
            </p>

            <p>In the event that an overseas medical service provider is involved, we will contact them to verify the authenticity.</p>

            <p>At all times, if there are any extra costs involved in the claims verification process (such as translation costs), we may either bill you and deduct it from your claim payout, or bill you separately if the claim cannot be substantiated.</p>

            <p>* On the Date of any Claim Submission, your policy cover must be active in order for it to be a processable Claim.</p>
            <p>ie. If your policy cover is no longer active for any reason, a new Claim Submission will not be entertained.</p>

        </Wrapper>
    );
}


// const Wrapper = styled.div`
//     > h2 {
//       margin-top: 10px;
//       font-size: 25px;
//       font-weight: bold;
//     }
  
//     > p {
//       &:first-child {
//         margin-top: 40px;
//       }
  
//       margin-top: 20px;
//       font-size: 20px;
  
//       > a {
//       }
//     }
// `