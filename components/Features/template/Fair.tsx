import * as React from 'react';
import { Wrapper } from './styled';
// import styled from 'styled-components';

export interface IFairProps {
}

export default function Fair(props: IFairProps) {
    return (
        <Wrapper>
            <h2>13 - Can I make a claim from Cyclic insurance, after I make a claim from a traditional Insurer’s policy, for the same accident / incident?</h2>
            <p>
                Traditional Insurers may not allow you to make a Reimbursement claim from 2 different insurers for the same accident / incident, but we work differently at iFinSG.
            </p>
            <p>
                When an unfortunate accident / incident happens, you should be allowed to claim All the relevant benefits which you have already paid for, via the premiums of your policies which you have already been paying to different insurers.
            </p>
            <p>
                If a company wants to prevent you from making more than 1 Reimbursement claim from the same accident / incident, maybe that company should prevent you from buying a 2nd policy in the first place?
            </p>
            <p>
                <b>Cyclic insurance</b> will always allow you to make a claim from us for the same accident / incident, even after you make a similar claim from a traditional insurer.
            </p>
            <p>
                Make a 2nd claim with us for the same Accident ( using photos of the Receipt and related documents ), even after you make a Reimbursement claim for your policy with an Insurer ( using the original Receipt and related documents ) !
            </p>
        </Wrapper>
    );
}

// const Wrapper = styled.div`
//     > h2 {
//         margin-top: 10px;
//         font-size: 25px;
//         font-weight: bold;
//     }

//     > p {
//         margin-top: 20px;
//         font-size: 20px;
//     }
// `
