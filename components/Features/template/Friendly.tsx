import * as React from 'react';
import { Wrapper } from './styled';

export interface IFriendlyProps {}

export default function Friendly(props: IFriendlyProps) {
    return (
        <Wrapper>
            <h2>30 - How do I re-continue my cover if it has Lapsed or Stopped?</h2>
            <p>
                You would be delighted to know that you can Login to your <b>Profile</b> page, and use just 1 button to
                easily re-continue your cover with us.
            </p>
            <p>
                We’ll bill your credit / debit card after the Reinstatement of your cover is approved, based on the new
                starting date of your cover.
            </p>
        </Wrapper>
    );
}
