import * as React from 'react';
import { Wrapper } from './styled';
// import styled from 'styled-components';

export interface IHonorableProps {
}

export default function Honorable(props: IHonorableProps) {
    return (
        <Wrapper>
            <h2>7 - What PremiumBack amount would I get, if no user makes a claim in a particular cycle?</h2>
            <p>If no user makes a claim in a particular cycle, you would get back all your premiums for the cycle as <b>PremiumBack</b>.</p>
            <p>Eg. For the Small Accident cover, your monthly premium would be $6.80, while the monthly Admin Fee is $3. If you participated for a full cycle, the total premium would be $6.80 x 3 months = $20.40.</p>
            <p>If no user makes a claim in a particular cycle, you would thus receive a <b>PremiumBack</b> of $20.40.</p>
        </Wrapper>
    );
}


