import * as React from 'react';
import { Wrapper } from './styled';

export interface IMoneyBackGuaranteeProps {
}

export default function MoneyBackGuarantee(props: IMoneyBackGuaranteeProps) {
    return (
        <Wrapper>
            {/* <div style={{ textAlign: "center" }}> */}
            <div className="img_wrapper">
                <img src={('/img/Pic-6-Home.png')} />
            </div>
            <h1> Money back Guarantee </h1>
            <p> We are confident to give you our Money back Guarantee:</p>
            <p> After you have tried our service for a 1st cycle, should you decide not to use our services again, we'll still give you back a value equivalent to 100% of your 1st cycle Premiums!</p>
            <ul>
                <ol>1. Applicable if you cancel your cover within 14 days after your 1st cycle.</ol>
                <ol>2. Valid if you did not make any claim in your 1st cycle.</ol>
                <ol>3. Inclusive of the <b>PremiumBack</b> given for your 1st cycle.</ol>
                <ol>4. Applicable if you did not take any form of promotional vouchers, discounts or equivalent benefits for your purchase.</ol>
            </ul>
            {/* </div> */}
            <p> Try our service, because we know you’ll love it!</p>
            {/* </div> */}
        </Wrapper>
    );
}
