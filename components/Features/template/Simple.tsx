import * as React from 'react';
import { Wrapper } from './styled';

export interface ISimpleProps {
}

export default function Simple(props: ISimpleProps) {
    return (
        <Wrapper>
            <h2>32 - What are the things not covered under my Cyclic insurance policy?</h2>
            <p>We believe that insurance should be simple, so we work on a WYSIWYG principle. WYSIWYG = What you see is what you get.</p>
            <p>Each product's Policy Details on our <a href="/products" style={{ color: "#7cda24" }}>PRODUCTS</a> page, fully explains the policy’s features, with no hidden terms and conditions at all.</p>
            <p>Any exclusion which would disqualify a claim for a particular policy, would be stated explicitly and clearly in the Policy Details of each product.</p>
            <p>We do not have a huge chunk of small text exclusions such as “no Claims in case of an act of God”.</p>
            <p>We have some common-sense rules which all our users and policies operate within:</p>
            <p>(1) You cannot commit a crime intentionally and expect to make a successful claim.</p>
            <p>(2) You cannot commit self-inflicted damage and yet expect to make any claim.</p>
            <p>(3) We don’t have a 3rd rule yet, but if you do think of something applicable and fair, let us know so that we will consider if we should add it in.</p>
        </Wrapper>
    );
}

