import styled from 'styled-components'
export const Wrapper = styled.div`

    > h1 {
       text-align:center;
       font-weight:bold;
    }

    > h2 {
      margin-top: 10px;
      font-size: 25px;
      font-weight: bold;
    }
  
    > p {
      &:first-child {
        margin-top: 40px;
      }
  
      margin-top: 20px;
      font-size: 20px;
  
      > a {
      }
    }
  
    ul,ol {
        
        margin:0;
        padding:0;
    }
    
    > ul {

        > ol {
            font-size: 20px;  
            margin-top: 10px;
        }
    }

    .img_wrapper {
        text-align:center;
        > img {
            width:150px;
        }
    }

`