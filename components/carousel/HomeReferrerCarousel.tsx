import React from "react";
import Slider from "react-slick";
import Head from 'next/head'
import styled from 'styled-components'

export default class HomeReferrerCarousel extends React.Component {
    render() {
        var settings = {
            dots: true,
            nextArrow: null,
            prevArrow: null
        };
        const data = [
            {
                id: "1",
                referrer: 'Jessalin',
                comment: 'I like Cyclic Insurance as it’s affordable and I know that I can get a claim should I get injured and visit a clinic.',
                imgPath: '/img/testimonials/jessalin_tan_hui_yan.jpg'
            },
            {
                id: "2",
                referrer: 'Tze Ting',
                comment: 'It’s awesome that for every 3 months if I make 0 claims, I can receive a Minimum guaranteed 10% PremiumBack.',
                imgPath: '/img/testimonials/lee_tze_ting.jpg'
            },
            {
                id: "3",
                referrer: 'Wei Boon',
                comment: 'I like Cyclic insurance because it has cheap premium to pay to cover for the small health problem that most of people dealt with nowadays.',
                imgPath: '/img/testimonials/chong_wei_boon.jpg'
            },
            {
                id: "4",
                referrer: 'Kang Tze',
                comment: 'I like Cyclic insurance because its super easy and cheap!',
                imgPath: '/img/testimonials/kang_tze.jpg'
            }
        ]

        return (
            <Wrapper>
                <Head>
                    <link href="/slick.css" rel="stylesheet" />
                    <link href="/slick-theme.css" rel="stylesheet" />
                </Head>
                <img className="home-referrer-image" src={('/img/microphone-home.png')}></img>
                <div className="home-referrer-title">Why people love Cyclic insurance</div>
                <div className="home-slider">
                    <Slider {...settings}>
                        {data.map(item => {
                            return (
                                <div key={item.id}>
                                    <div className="gallery"
                                        key={item.id}>
                                        <img src={item.imgPath} alt={item.id} />
                                        <div className="referrer">
                                            <p className="comment">{item.comment}</p>
                                            <p className="name">- {item.referrer}</p>
                                        </div>
                                    </div>
                                </div>
                            );
                        })}
                    </Slider>
                </div>
            </Wrapper>
        );
    }
}



const Wrapper = styled.div`
    box-sizing: border-box;
    text-align:center;

    @media (max-width: 360px){
        width:300px;
        margin: 0 auto;
        padding: 0 15px;
    }

    @media (min-width: 360px) and (max-width: 414px){
        width:350px;
        margin: 0 auto;
        /* padding: 0 15px; */
    }

    @media (min-width: 415px) and (max-width: 479px){
        width: 440px;
        margin: 0 auto;
        padding: 0 15px;
    }

width:800px;
margin:0 auto;
padding-bottom:28px;
padding-top: 20px;

.home-slider {
    margin-top:30px;
    padding: 0;

    .slick-arrow {
        &.slick-prev {
            display:none!important;
        }
        &.slick-next {
            display:none!important;
        }
    } 
    
}

> img {
    &.home-referrer-image {
        max-width: 150px;
        width: 100%;
        margin: 0;
        padding: 0;
    }
}

.home-referrer-title{
    font-family: Merriweather;
    font-size: 42px;
    font-weight: 400;
    line-height: 1.3;
    margin: 30px 0 0 0;
    padding: 0;
    color: #4a4a4a;
}

.gallery {
    display: flex;
    flex-direction: row;

    > img {
        min-width: 110px;
        height: 147px;
        display: block;
    }

    .referrer {
        padding-top: 10px;

        > .comment {
            text-align: left;
            margin: 0px 20px;
            font-family: Lato;
            font-style: italic;
            font-size: 20px;
        }

        > .name {
            text-align: right;
            margin-right: 20px;
        }

    }

}

`
