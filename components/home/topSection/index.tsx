import React from 'react';
import { Link, Button } from '@material-ui/core';
import { Wrapper } from './styled';
import Router from 'next/router';

export interface ITopSectionProps {
}

const TopSection: React.FC<ITopSectionProps> = (props: ITopSectionProps) => {
    return (
        <Wrapper>
            <div className="top_section">
                <img src={('/home/cyclic_insurance.png')} />
                <h1 className="top_title">Insurance the way you want it</h1>
                <div className="top_keywords">
                    <p>Simple.</p>
                    <p>Fast.</p>
                    <p>Cheap.</p>
                </div>
                <div className="top_btn">
                    <Link data-track-id="check_it_out" key='check_it_out' component="button" onClick={() => Router.push('/products')}>
                        <Button
                            variant="contained"
                            style={{ fontSize: '20px', color: 'white', margin: "0", fontFamily: "Lato", padding: '15px 45px', background: '#7cda24' }}>
                            Check it out!
                        </Button>
                    </Link>
                </div>
                {/* <p className="top_paragraph">Yes, you can make a 2nd claim from us for the same accident / incident, even after you make a similar claim from a traditional insurer!</p> */}
            </div>
        </Wrapper>
    );
}
export default TopSection;