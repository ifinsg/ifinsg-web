import styled from 'styled-components';

export const Wrapper = styled.div`
    text-align: center;
    padding: 20px;
    font-size: 10px;

    .top_section {
        > img {
            width: 260px;
        }

        > h1 {
            font-family: 'Merriweather', Georgia, serif;
            font-size: 42px;
            font-weight: 400;
            line-height: 1.3;
            margin: 30px 0 0 0;
            padding: 0;
            color: #4a4a4a;

            &.top_title {
                font-family: 'Merriweather';
            }
        }

        .top_keywords {
            > p {
                font-size: 20px;
                margin: 20px 0 0 0;
                padding: 0;
            }
        }

        .top_btn {
            margin-top: 50px;
        }

        > p {
            &.top_paragraph {
                font-family: Lato, 'Helvetica Neue', Arial, sans-serif;
                font-size: 20px;
                margin-top: 30px;
                color: #4a4a4a;
            }
        }
    }

    @media (max-width: 360px) {
        margin: 0;
        > ul {
            &.intro-step {
                display: flex;
                flex-direction: column;
                > li {
                    margin: 0 auto;
                    width: 300px;
                    margin-bottom: 35px;
                }

                .step {
                    .top {
                        height: initial;
                    }
                }
            }
        }
    }

    @media (min-width: 361px) and (max-width: 375px) {
        margin: 0;
        > ul {
            &.intro-step {
                display: flex;
                flex-direction: column;
                > li {
                    margin: 0 auto;
                    width: 320px;
                    margin-bottom: 35px;
                }

                .step {
                    .top {
                        height: initial;
                    }
                }
            }
        }
    }

    @media (min-width: 376px) and (max-width: 600px) {
        margin: 0;
        > ul {
            &.intro-step {
                display: flex;
                flex-direction: column;
                > li {
                    margin: 0 auto;
                    width: 340px;
                    margin-bottom: 35px;
                }

                .step {
                    .top {
                        height: initial;
                    }
                }
            }
        }
    }
`;
