import React from 'react';
import { Link, Button } from '@material-ui/core';
import { Wrapper } from './styled';

export interface ITopSection2Props {
}

const TopSection2: React.FC<ITopSection2Props> = (props: ITopSection2Props) => {
    return (
        <Wrapper>
            <div className="top_section">
                <img src={('/home/atm.png')} />
                <h1 className="top_title">Receive money even</h1>
                <h1 className="top_title">if you make <span className="number_text">0</span> Claims</h1>
                <div className="top_paragraph">
                    <p>Cyclic insurance covers you for practical events, and gives you money at the end of every 3 months cycle!</p>
                    <p>Make 0 claims, and get a guaranteed <b>PremiumBack</b> of at least 10% of your premiums!</p>
                    <p>Make Low claims, and get a guaranteed <b>PremiumBack</b> of at least 5% of your premiums!</p>
                </div>
            </div>
        </Wrapper>
    );
}
export default TopSection2;