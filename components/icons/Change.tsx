import React from 'react';
interface IProps {
    color: string
}

const Change: React.FC<IProps> = ({ color }) => {
    const fill = color ? color : '#000';
    return (
        <svg height="100%" viewBox="0 0 192 192" width="100%" xmlns="http://www.w3.org/2000/svg">
            <path fill={fill} d="m96 16a80.2 80.2 0 0 1 64 32h-8v16h24a8 8 0 0 0 7.59-5.47l8-24-15.18-5.06-3.175 9.53a95.994 95.994 0 0 0 -173.235 57h16a80.091 80.091 0 0 1 80-80z" />
            <path fill={fill} d="m176 96a80 80 0 0 1 -144 48h8v-16h-24a8 8 0 0 0 -7.59 5.47l-8 24 15.18 5.06 3.175-9.53a95.994 95.994 0 0 0 173.235-57z" />
            <path fill={fill} d="m40 96a56 56 0 1 0 56-56 56.063 56.063 0 0 0 -56 56zm80-32v16h-28a4 4 0 0 0 0 8h8a20 20 0 0 1 4 39.6v8.4h-16v-8h-16v-16h28a4 4 0 0 0 0-8h-8a20 20 0 0 1 -4-39.6v-8.4h16v8z" />
        </svg>
    );
}
export default Change;



