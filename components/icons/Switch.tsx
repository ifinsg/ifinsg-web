import React from 'react';
interface IProps {
    color: string
}
const Switch: React.FC<IProps> = ({ color }) => {
    const fill = color ? color : '#000';
    return (
        <svg height="100%" width="100%" viewBox="0 0 600 600" xmlns="http://www.w3.org/2000/svg">
            <path fill={fill} d="M0 149.332c0 82.348 67.008 149.336 149.332 149.336h213.336C444.992 298.668 512 231.68 512 149.332 512 66.988 444.992 0 362.668 0H149.332C67.008 0 0 66.988 0 149.332zm277.332 0c0-47.059 38.273-85.332 85.336-85.332C409.727 64 448 102.273 448 149.332c0 47.063-38.273 85.336-85.332 85.336-47.063 0-85.336-38.273-85.336-85.336zm0 0" />
        </svg>

    );
}
export default Switch;