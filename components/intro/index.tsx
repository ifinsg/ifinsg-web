import React from 'react';
import { Wrapper } from './styled'
import { IconButton } from '@material-ui/core';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Theme, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";


const useStyles = makeStyles((theme: Theme) => ({
    details: {
        display: "block",
        textAlign: "left",
        textIndent: "10px",
        padding: "0 10px",
        margin: "0 0 10px 0",
    },
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: 'Lato',
        textIndent: 0,
        textAlign: 'left'
    },
    heading_detail: {
        fontSize: theme.typography.pxToRem(12),
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: 'Lato',
        padding: '0 20px',
        textIndent: 0
    },
    heading_detail_with_example: {
        '&::before': {
            content: '"Eg."',
            marginLeft: "-20px",
            position: "absolute"
        },
        fontSize: theme.typography.pxToRem(12),
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: 'Lato',
        padding: '0 20px',
        textIndent: 0,
    }
}));

export interface IIntroProps {

}

const Intro: React.FC<IIntroProps> = (props: IIntroProps) => {
    const classes = useStyles({});
    return (
        <Wrapper>
            <h1 className="intro-title">How It Works</h1>
            <ul className="intro-step">
                <li>
                    <div className="step step-1">
                        <div className="top">
                            <div className="icon">
                                <IconButton size="small" disabled style={{ background: '#8f8f8f', width: 30, height: 30, lineHeight: '26px', textIndent: "-1px", color: '#fff' }} aria-label="delete">
                                    1
                            </IconButton>
                            </div>
                            <h4>Pool Creation</h4>
                            <p className="intro-p">Premiums are pooled at the start of each cycle.</p>
                        </div>
                        <div className="down">
                            <img src="/intro/howitworks1.png" />
                            {/* <div className="smaple">
                                <p>Eg. 10,000 users are covered in a 3 month cycle for Cyclic: Small Accident.</p>
                                <p>$6.80 x 3 = $20.40</p>
                                <p>$20.40 x 10,000 = $204,000 in the premiums pool</p>
                            </div> */}
                            <div className="smaple">
                                <ExpansionPanel>
                                    <ExpansionPanelSummary
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls="panel1a-content"
                                        id="panel1a-header"
                                    >
                                        <Typography className={classes.heading}>
                                            Example of <b>Pool Creation</b>
                                            <br />
                                            <i>( Cyclic : Small Accident )</i>
                                        </Typography>

                                    </ExpansionPanelSummary>
                                    <ExpansionPanelDetails className={classes.details}>
                                        <Typography className={classes.heading_detail_with_example}>
                                            10,000 users are covered in a 3 month cycle for <i>Cyclic: Small Accident.</i>
                                        </Typography>
                                        <br />
                                        <Typography className={classes.heading_detail}>
                                            $6.80 x 3 =
                                        </Typography>
                                        <Typography className={classes.heading_detail}>
                                            $20.40 of premiums per user
                                        </Typography>
                                        <br />
                                        <Typography className={classes.heading_detail}>
                                            $20.40 x 10,000 users =
                                        </Typography>
                                        <Typography className={classes.heading_detail}>
                                            $204,000 in the premiums pool
                                        </Typography>
                                    </ExpansionPanelDetails>
                                </ExpansionPanel>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div className="step step-2">
                        <div className="top">
                            <div className="icon">
                                <IconButton size="small" disabled style={{ background: '#8f8f8f', width: 30, height: 30, lineHeight: '26px', textIndent: "-1px", color: '#fff' }} aria-label="delete">
                                    2
                            </IconButton>
                            </div>
                            <h4>Claims</h4>
                            <p className="intro-p">Claims are made from the pool.</p>
                        </div>
                        <div className="down">
                            <img src="/intro/howitworks2.png" />
                            {/* <div className="smaple">
                                <p>Eg. 500 users get into an accident and need to claim an average of $300 each.</p>
                                <p>500 x $300 = $150,000 is paid out as claims</p>
                            </div> */}
                            <div className="smaple" >
                                <ExpansionPanel>
                                    <ExpansionPanelSummary
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls="panel1a-content"
                                        id="panel1a-header"
                                    >
                                        <Typography className={classes.heading}>
                                            Example of <b>Claims</b>
                                            <br />
                                            <i>( Cyclic : Small Accident )</i>
                                        </Typography>
                                    </ExpansionPanelSummary>
                                    <ExpansionPanelDetails className={classes.details}>

                                        <Typography className={classes.heading_detail_with_example}>
                                            500 users get into an accident and need to claim an average of $300 each.
                                        </Typography>
                                        <br />
                                        <Typography className={classes.heading_detail}>
                                            500 x $300 =
                                        </Typography>
                                        <Typography className={classes.heading_detail}>
                                            $150,000 is paid out as claims
                                        </Typography>
                                    </ExpansionPanelDetails>
                                </ExpansionPanel>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div className="step step-3">
                        <div className="top">
                            <div className="icon">
                                <IconButton size="small" disabled style={{ background: '#8f8f8f', width: 30, height: 30, lineHeight: '26px', textIndent: "-1px", color: '#fff' }} aria-label="delete">
                                    3
                            </IconButton>
                            </div>
                            <h4>PremiumBack</h4>
                            <p className="intro-p">Unclaimed premiums in the pool, will be distributed back to users who made 0 claims & users who made low claims, at the end of each cycle.</p>
                        </div>
                        <div className="down">
                            <img src="/intro/howitworks3.png" />
                            {/* <div className="smaple">
                                <p>Eg. 9,500 users did not make a claim.</p>
                                <p>$204,000 - $150,000 = $54,000 of unclaimed premiums</p>
                                <p>$54,000 ÷ 9,500 = $5.68</p>
                            </div> */}
                            <div className="smaple">
                                <ExpansionPanel>
                                    <ExpansionPanelSummary
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls="panel1a-content"
                                        id="panel1a-header"
                                    >
                                        <Typography className={classes.heading}>Example of <b>PremiumBack</b>
                                            <br />
                                            <i>( Cyclic : Small Accident )</i>
                                        </Typography>
                                    </ExpansionPanelSummary>
                                    <ExpansionPanelDetails className={classes.details}>
                                        <Typography className={classes.heading_detail_with_example}>
                                            9,500 users did not make a claim at all.
                                        </Typography>
                                        <br />
                                        <Typography className={classes.heading_detail}>
                                            $204,000 - $150,000 =
                                        </Typography>
                                        <Typography className={classes.heading_detail}>
                                            $54,000 of unclaimed premiums
                                        </Typography>
                                        <br />
                                        <Typography className={classes.heading_detail}>
                                            $54,000 ÷ 9,500 users =
                                        </Typography>
                                        <Typography className={classes.heading_detail}>
                                            $5.68 of PremiumBack will be given at the end of the 3 months cycle
                                        </Typography>
                                    </ExpansionPanelDetails>
                                </ExpansionPanel>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
            <p className="intro-tail">
                We only earn a flat and transparent monthly <b>Admin Fee</b> per policy per user, with no hidden charges.
                </p>
        </Wrapper>
    );
}

export default Intro;
