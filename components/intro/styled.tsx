import styled from 'styled-components'


export const Wrapper = styled.div`

    margin: 0 -50px;
    padding: 20px 0;

    @media (max-width: 360px){
        margin: 0;
        > ul {
            &.intro-step  {
                display: flex;
                flex-direction: column;
                > li {
                    margin: 0 auto;
                    width: 300px;
                    margin-bottom: 80px;
                }

                .step {

                    .top {
                        height:initial;
                    }
                }
            }
        }

        .intro-tail {
            padding: 0 20px!important;
        }
    }


    @media (min-width: 361px) and (max-width: 375px){
        margin: 0;
        > ul {
            &.intro-step  {
                display: flex;
                flex-direction: column;
                > li {
                    margin: 0 auto;
                    width: 320px;
                    margin-bottom: 80px;
                }

                .step {

                    .top {
                        height:initial;
                    }
                }
            }
        }

        .intro-tail {
            padding: 0 20px!important;
        }
    }

    @media (min-width: 376px) and (max-width: 600px){
        margin: 0;
        > ul {
            &.intro-step  {
                display: flex;
                flex-direction: column;
                > li {
                    margin: 0 auto;
                    width: 340px;
                    margin-bottom: 35px;
                }

                .step {

                    .top {
                        height:initial;
                    }
                }
            }
        }
    
        .intro-tail {
            padding: 0 20px!important;
        }
    }

    > ul {
        list-style:none;
        margin:0;
        padding:0;
        > li {
            list-style:none;
            margin:0;
            padding:0;
        }
    }

    .intro-title {
        font-family: Merriweather,Georgia,serif;
        font-size: 42px;
        font-weight: 400;
        line-height: 1.3;
        margin: 0 0 40px 0;
        padding: 0;
        text-align: center;
        color: #4a4a4a;
    }

    .intro-step {
        display: flex;
        justify-content: space-between;
        > li {
            width: 280px; 
        }

        .step {

            height: 100%;
            display: flex;
            flex-direction: column;

            .top {
                height: 160px;
                text-align: center;
            }

            .down {
                text-align: center;
                > img {
                    height:200px;

                }

                .smaple {
                    margin-top:10px;
                    /* text-align:left;
                    > p {
                        padding:0;
                        margin:0;
                        font-size: 14px;
                        margin-top:15px;
                    } */
                }
            }

            .intro-p {
                color: #808080;
                font-size: 14px;
            }

            > img {
                height:200px;
            }

            &.step-1 {
                .down {
                    > img {
                        
                    }
                }
            }
            &.step-2 {
                .down {
                    > img {
                        
                    }
                }
            }
            &.step-3 {
                .down {
                    > img {
                        
                    }
                }
            }

            .icon {

            }

            > h4 {
                margin-top: .5rem;
            }
        }
    }

    .intro-tail {
        margin: 40px 0 0 0;
        padding: 0;
        font-size:20px;
    }
      

`