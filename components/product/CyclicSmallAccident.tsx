
import React from 'react';
import Tick from '../icons/Tick';
import Link from 'next/link';
import { useRouter } from 'next/router'
import { Button, withStyles } from '@material-ui/core';
import { Wrapper } from './AccidentStyled'

const ClaimButton = withStyles(theme => ({
    root: {
        color: "#FFF",
        height: 34,
        fontSize: 13,
        fontWeight: 500,
        
        backgroundColor: "#7cda23",
        "&:hover": {
            backgroundColor: "#569818"
        }
    }
}))(Button);

const CyclicSmallAccident = () => {
    const router = useRouter();
    const query = Object.assign({}, { ...router.query })

    return <Wrapper>
        <div className="group_policy">
            <section className="product_info">
                <div className="product_title">Cyclic: Small Accident</div>
                <p>Because you can't Control accidents</p>
                <div className="group_details">
                    <ul>
                        <li>
                            <div className="details">
                                <p>(1) Benefits</p>
                                <ul>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Reimburses you up to $1,500 per accident (even for medical treatment of sprains, cuts or falls), covering All sports and has no Reduction of claims for any sport.
                                        </p>
                                    </li>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>
                                            Additional benefits: includes TCM, food poisoning, insect/animal attacks, zika, dengue fever, and burns/scalds.
                            </p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div className="details">
                                <p>(2) Claims</p>
                                <ul>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Accident definition: Violent, Visible, External, Impact.</p>
                                    </li>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Covers only new accidents which happen after your cover is approved with us, and the SMC-registered / TCMPB-registered doctor ( practicing in Singapore ) must write clearly on the Receipt / Invoice / MC, the Date and Description of the accident which occurred.</p>
                                    </li>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Covers only treatments which take place within 3 months after the Date of each Accident.</p>
                                    </li>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p> Make a 2nd claim from us for the same accident / incident, even after you make a similar claim from a traditional insurer.</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div className="details">
                                <p>(3) Special Notes</p>
                                <ul>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Valid till user is age 77.</p>
                                    </li>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Treatment for accidents in All countries, 24/7 worldwide ( outside of Singapore, we accept claims from the A&E department of public hospitals only ).
</p>
                                    </li>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>User must have a Singapore Bank account to apply for this.</p>
                                    </li>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>This product is not currently not available for SMC-registered / TCMPB-registered doctors to purchase.</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div className="details">
                                <p>(4) Exclusions</p>
                                <ul>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Treatment for old accidents which happened before your cover is Approved with us.</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div className="details">
                                <p>(5) Low Claims definition</p>
                                <ul>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Users who claim SGD$60 or less per cycle.</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>
            <section className="different">
                <div className="product_title">How are we different</div>
                <p>* We cover you even when you are on a PMD for work.</p>
                <p>* We do not have a Reduction of Claims for high-risk sports, and we do not cover only a restrictive range of sports.</p>
                <p>* We cover you when you travel to exotic places such as North Korea, Iran, and Cuba.</p>
                <p>* We cover you even if your accident occured during a competition / marathon.</p>
            </section>
            <section className="how_to_claim">
                <div className="product_title">HOW TO CLAIM</div>
                <p>[Cyclic: Small Accident]</p>
                <p className="no_wrap text_left">When you need to make a claim, just:</p>
                <ul>
                    <li>
                        <p>1. Tell us:</p>
                        <div>(i) What happened in the incident</div>
                        <div>(ii) Who was the SMC-registered / TCMPB-registered Doctor you consulted</div>
                        <div>(iii) Which clinic / hospital you visited.</div>
                    </li>
                    <li>
                        <p>2. Upload photos of:</p>
                        <div>(i) Paid Receipt</div>
                        <div>(ii) Itemized Invoice of treatment and medicine</div>
                        <div>(iii) MC with diagnosis / Letter of diagnosis.</div>
                    </li>
                </ul>
                <div className="policy_instructions">
                    <p>
                        * Just make sure the Doctor writes the Date of accident + Description of accident, on any 1 of the documents in the photos which you upload ( either Receipt / Invoice / MC with diagnosis / Letter of diagnosis ).
            </p>
                    <p>
                        * If you received treatment from the A&E department of a Public Hospital outside of Singapore, please upload photos of the entry and exit stamps in your Passport as evidence that you were in that country.
            </p>
                    <p>
                        SMC is the Singapore Medical Council ( a statutory board under the Ministry of Health ), governed under Section 5 of the Medical Registration Act (Cap 174). You can check if your doctor is SMC registered, via this link:<a href='https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC' target="_blank">https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC</a>
                    </p>
                    <p>
                        TCMPB is the Traditional Chinese Medicine Practitioners Board ( a statutory board under the charge of the Ministry of Health ), under the Traditional Chinese Medicine Practitioners Act 2000, governed under the Traditional Chinese Medicine Practitioners Act (Cap. 333A). You can check if your doctor is TCMPB registered, via this link:<a href='https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=TCM' target="_blank">https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=TCM</a>
                    </p>
                    <p>
                        In the event that an overseas medical service provider is involved, we will contact them to verify the authenticity.
            </p>
                    <p>
                        At all times, if there are any extra costs involved in the claims verification process (such as translation costs), we may either bill you and deduct it from your claim payout, or bill you separately if the claim cannot be substantiated.
            </p>
                    <p>
                        * On the Date of any Claim Submission, your policy cover must be active in order for it to be a processable Claim.
            </p>
                    <p>
                        ie. If your policy cover is no longer active for any reason, a new Claim Submission will not be entertained.
            </p>
                </div>
            </section>
            <div className="page_btn">
                <Link href={{ pathname: '/claims', query }} >
                    <ClaimButton data-track-id="claims" variant="contained">Still unsure about claims?</ClaimButton>
                </Link>
            </div>
        </div >
    </Wrapper>
}

export default CyclicSmallAccident