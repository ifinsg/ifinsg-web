

import React from 'react';
import Tick from '../icons/Tick';
import Link from 'next/link';
import { useRouter } from 'next/router'
import { Button, withStyles } from '@material-ui/core';
import { Wrapper } from './AccidentStyled'

const ClaimButton = withStyles(theme => ({
    root: {
        color: "#FFF",
        height: 34,
        fontSize: 13,
        fontWeight: 500,
        
        backgroundColor: "#7cda23",
        "&:hover": {
            backgroundColor: "#569818"
        }
    }
}))(Button);

const CyclicSmallAccident = () => {
    const router = useRouter();
    const query = Object.assign({}, { ...router.query })
    console.log("TCL: ModalAccidentPolicy -> query", query)

    return <Wrapper>
        <div className="group_policy">
            <section className="product_info">
                <div className="product_title">Cyclic: Small Accident Income</div>
                <p>Because no Work no money</p>
                <div className="group_details">
                    <ul>
                        <li>
                            <div className="details">
                                <p>(1) Benefits</p>
                                <ul>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Claim $120 for each day that you are unable to work due to an accident, starting from the 3rd day of your accident MC in Singapore.
                                        </p>
                                    </li>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>
                                            No verification of your current income is required.
                            </p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div className="details">
                                <p>(2) Claims</p>
                                <ul>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Accident definition: Violent, Visible, External, Impact.</p>
                                    </li>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Covers only new accidents which happen after your cover is approved with us, and the SMC-registered doctor ( practicing in Singapore ) who issued the accident MC, must write clearly on the Receipt / Invoice / MC, the Date and Description of the accident which occurred.</p>
                                    </li>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>No hospitalization is needed for you to make a claim.</p>
                                    </li>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Claim up to 6 days for each cycle.</p>
                                    </li>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p> Make a 2nd claim from us for the same accident / incident, even after you make a similar claim from a traditional insurer.</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div className="details">
                                <p>(3) Special Notes</p>
                                <ul>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Valid for any user who is between age 21 to age 55.</p>
                                    </li>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>User must have a Singapore Bank account to apply for this.</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div className="details">
                                <p>(4) Exclusions</p>
                                <ul>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Old accidents which happened before your cover is Approved with us.</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div className="details">
                                <p>(5) Low Claims definition</p>
                                <ul>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Users who claim SGD$120 or less per cycle.</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>
            <section className="different">
                <div className="product_title">How are we different</div>
                <p>* We cover you even if your current income happens to be less than $120 a day.</p>
                <p>* We allow a claim even if you were not hospitalized for the accident.</p>
                <p>* We allow a claim right after your 2nd day of accident MC.</p>
            </section>
            <section className="how_to_claim">
                <div className="product_title">HOW TO CLAIM</div>
                <p>[Cyclic: Small Accident Income]</p>
                <p className="no_wrap text_left">When you need to make a claim, just:</p>
                <ul>
                    <li>
                        <p>1. Tell us:</p>
                        <div>(i) What happened in the incident,</div>
                        <div>(ii) Who was the SMC-registered Doctor you consulted,</div>
                        <div>(iii) Which clinic / hospital you visited.</div>
                    </li>
                    <li>
                        <p>2. Upload photos of:</p>
                        <div>(i) Paid Receipt,</div>
                        <div>(ii) Itemized Invoice of treatment and medicine,</div>
                        <div>(iii) MC with diagnosis / Letter of diagnosis.</div>
                    </li>
                </ul>
                <div className="policy_instructions">
                    <p>
                        * Just make sure the Doctor writes the Date of accident + Description of accident, on any 1 of the documents in the photos which you upload ( either Receipt / Invoice / MC with diagnosis / Letter of diagnosis ).
            </p>

                    <p>
                        SMC is the Singapore Medical Council ( a statutory board under the Ministry of Health ), governed under Section 5 of the Medical Registration Act (Cap 174). You can check if your doctor is SMC registered, via this link:<a href='https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC' target="_blank">https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC</a>
                    </p>
                    <p>* On the Date of any Claim Submission, your policy cover must be active in order for it to be a processable Claim.</p>
                    <p>ie. If your policy cover is no longer active for any reason, a new Claim Submission will not be entertained.</p>
                </div>
            </section>
            <div className="page_btn">
                <Link href={{ pathname: '/claims', query }} >
                    <ClaimButton data-track-id="claims" variant="contained">Still unsure about claims?</ClaimButton>
                </Link>
            </div>
        </div>
    </Wrapper>
}

export default CyclicSmallAccident