import React from 'react';
import Tick from '../icons/Tick';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Button, withStyles } from '@material-ui/core';
import { Wrapper } from './AccidentStyled';

const ClaimButton = withStyles((theme) => ({
    root: {
        color: '#FFF',
        height: 34,
        fontSize: 13,
        fontWeight: 500,
        backgroundColor: '#7cda23',
        '&:hover': {
            backgroundColor: '#569818'
        }
    }
}))(Button);

const CyclicSmallCOVID_19Income = () => {
    const router = useRouter();
    const query = Object.assign({}, { ...router.query });
    console.log('TCL: ModalAccidentPolicy -> query', query);

    return (
        <Wrapper>
            <div className="group_policy">
                <section className="product_info">
                    <div className="product_title">Cyclic: Small COVID-19 Income</div>
                    <p>Because you won’t know Who may spread it to you</p>
                    <div className="group_details">
                        <ul>
                            <li>
                                <div className="details">
                                    <p>(1) Benefits</p>
                                    <ul>
                                        <li>
                                            <div className="tick_icon">
                                                <Tick />
                                            </div>
                                            <p>
                                                Claim a lump sum of $1,000 to help out with your family income, if (a)
                                                you receive a Quarantine Order due to close contact with someone who was
                                                infected, OR if (b) you are diagnosed with the Coronavirus COVID-19!
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div className="details">
                                    <p>(2) Claims</p>
                                    <ul>
                                        <li>
                                            <div className="tick_icon">
                                                <Tick />
                                            </div>
                                            <p>
                                                Covers you in case you receive a Quarantine Order officially issued
                                                under the Infectious Diseases Act in Singapore, due to you being in
                                                close contact with someone who is infected with the Coronavirus
                                                COVID-19, when you are physically in Singapore.
                                            </p>
                                        </li>
                                        <li>
                                            <div className="tick_icon">
                                                <Tick />
                                            </div>
                                            <p>
                                                Covers you in case you are diagnosed with the Coronavirus COVID-19 in
                                                Singapore, where the confirmed diagnosis must be issued by a
                                                SMC-registered doctor practicing in Singapore, at a hospital or the NCID
                                                ( National Centre for Infectious Diseases ).
                                            </p>
                                        </li>

                                        <li>
                                            <div className="tick_icon">
                                                <Tick />
                                            </div>
                                            <p>
                                                In any time period of 60 days, you can only make 1 claim for either the
                                                Quarantine Order, or the confirmed diagnosis of the Coronavirus
                                                COVID-19.
                                            </p>
                                        </li>

                                        <li>
                                            <div className="tick_icon">
                                                <Tick />
                                            </div>
                                            <p>
                                                After 60 days of any claim, you are eligible to make a new claim for a
                                                subsequent Quarantine Order, or a new claim for a subsequent confirmed
                                                diagnosis of the Coronavirus COVID-19 due to a re-infection.
                                            </p>
                                        </li>

                                        <li>
                                            <div className="tick_icon">
                                                <Tick />
                                            </div>
                                            <p>
                                                Covers only if the Issue Date of the Quarantine Order / the Date of
                                                diagnosis of the Coronavirus COVID-19, is later than the date which your
                                                cover is approved by us.
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div className="details">
                                    <p>(3) Special Notes</p>
                                    <ul>
                                        <li>
                                            <div className="tick_icon">
                                                <Tick />
                                            </div>
                                            <p>
                                                This cover is newly designed, and will only start if we receive enough
                                                applications for this cover.
                                            </p>
                                        </li>
                                        <li>
                                            <div className="tick_icon">
                                                <Tick />
                                            </div>
                                            <p>
                                                This cover is only for people who have not travelled out of Singapore,
                                                but still received a Quarantine Order or an infection diagnosis, for the
                                                Coronavirus COVID-19.
                                            </p>
                                        </li>
                                        <li>
                                            <div className="tick_icon">
                                                <Tick />
                                            </div>
                                            <p>Valid till user is age 100.</p>
                                        </li>

                                        <li>
                                            <div className="tick_icon">
                                                <Tick />
                                            </div>
                                            <p>User must have a Singapore Bank account to apply for this.</p>
                                        </li>
                                        <li>
                                            <div className="tick_icon">
                                                <Tick />
                                            </div>
                                            <p>
                                                If you purchase this for yourself as an Individual, the $1,000 will be
                                                paid to you, whether you are employed or self-employed.
                                            </p>
                                        </li>
                                        <li>
                                            <div className="tick_icon">
                                                <Tick />
                                            </div>
                                            <p>
                                                If you purchase this for a Family member, you can choose whether you
                                                want us to pay the $1,000 to you, or to pay the $1,000 to your family
                                                member.
                                            </p>
                                        </li>
                                        <li>
                                            <div className="tick_icon">
                                                <Tick />
                                            </div>
                                            <p>
                                                If you purchase this as a Company cover for your staff / employee, you
                                                can choose whether you want us to pay the $1,000 to the company, or to
                                                pay the $1,000 to your staff / employee.
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div className="details">
                                    <p>(4) Exclusions</p>
                                    <ul>
                                        <li>
                                            <div className="tick_icon">
                                                <Tick />
                                            </div>
                                            <p>
                                                Leave of Absence ( LOA ) and Stay Home Notice ( SHN ), as they are both
                                                not considered as a Quarantine Order (
                                                <a href="https://www.moh.gov.sg/covid-19/faqs">
                                                    https://www.moh.gov.sg/covid-19/faqs
                                                </a>
                                                ).
                                            </p>
                                        </li>

                                        <li>
                                            <div className="tick_icon">
                                                <Tick />
                                            </div>
                                            <p>
                                                Quarantine Order issued because the life insured had travelled out of
                                                Singapore within the past 14 days before the Issue Date of Quarantine
                                                Order / Date of confirmed diagnosis for the Coronavirus COVID-19.
                                            </p>
                                        </li>

                                        <li>
                                            <div className="tick_icon">
                                                <Tick />
                                            </div>
                                            <p>
                                                Quarantine Order issued because the life insured has a PRC passport
                                                issued in Hubei.
                                            </p>
                                        </li>

                                        <li>
                                            <div className="tick_icon">
                                                <Tick />
                                            </div>
                                            <p>Users who flouted / defaulted the Quarantine Order from Singapore.</p>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div className="details">
                                    <p>(5) Low Claims definition</p>
                                    <ul>
                                        <li>
                                            <div className="tick_icon">
                                                <Tick />
                                            </div>
                                            <p>Low Claims is not applicable for this product.</p>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </section>
                <section className="different">
                    <div className="product_title">How are we different</div>
                    <p>* We do not require you to have any previous policy with us, to get this cover.</p>
                    <p>
                        * We do not restrict you to a 1 time claim, but we allow you to make new claims for a subsequent
                        Quarantine Order / a new claim for a subsequent confirmed diagnosis of the Coronavirus COVID-19
                        due to a re-infection, after 60 days of your previous claim.
                    </p>
                </section>
                <section className="how_to_claim">
                    <div className="product_title">HOW TO CLAIM</div>
                    <p>[[Cyclic: Small COVID-19 Income]</p>
                    <p className="no_wrap text_left">When you need to make a claim, just:</p>
                    <ul>
                        <li>
                            <p>1. Tell us:</p>
                            <div>(i) Issue Date of Quarantine Order / Date of confirmed diagnosis</div>
                            <div>(ii) Who was the SMC registered Doctor you consulted</div>
                            <div>
                                (iii) Which official institution issued you the Quarantine Order / the confirmed
                                diagnosis for the Coronavirus COVID-19
                            </div>
                        </li>
                        <li>
                            <p>2. Upload photos of:</p>

                            <div>
                                (i) Any credit / debit card payment receipt for a transaction which the Life Insured
                                paid for in Singapore within the past 14 days before the Issue Date of Quarantine Order
                                / Date of confirmed diagnosis, which shows evidence that the Life Insured was physically
                                in Singapore ( eg. Ride hailing, Taxi fare, FnB spending, or Grocery spending etc )
                            </div>
                            <div>
                                (ii) Front of the credit / debit card used for the above payment receipt to show Life
                                Insured’s ownership of the card
                            </div>
                            <div>
                                (iii) Official Quarantine Order letter issued under the Infectious Diseases Act in
                                Singapore, OR the Official diagnosis letter from the hospital / NCID which diagnosed you
                                with the Coronavirus COVID-19
                            </div>
                        </li>
                    </ul>
                    <div className="policy_instructions">
                        <p>
                            * Just make sure the Doctor writes the <u>Date of diagnosis</u> + <u>Description of diagnosis</u>, on any
                            1 of the documents in the photos which you upload ( either Receipt / Invoice / MC with
                            diagnosis / Letter of diagnosis ).
                        </p>
                        <p>
                            SMC is the Singapore Medical Council ( a statutory board under the Ministry of Health ),
                            governed under Section 5 of the Medical Registration Act (Cap 174). You can check if your
                            doctor is SMC registered, via this link:
                            <a href="https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC">
                                https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC
                            </a>
                        </p>
                        <p>
                            * On the Date of any Claim Submission, your policy cover must be active in order for it to
                            be a processable Claim.
                        </p>
                        <p>
                            ie. If your policy cover is no longer active for any reason, a new Claim Submission will not
                            be entertained.
                        </p>
                    </div>
                </section>
                <div className="page_btn">
                    <Link href={{ pathname: '/claims', query }}>
                        <ClaimButton data-track-id="claims" variant="contained">
                            Still unsure about claims?
                        </ClaimButton>
                    </Link>
                </div>
            </div>
        </Wrapper>
    );
};

export default CyclicSmallCOVID_19Income;
