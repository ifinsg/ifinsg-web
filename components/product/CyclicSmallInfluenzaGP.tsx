

import React from 'react';
import Tick from '../icons/Tick';
import Link from 'next/link';
import { useRouter } from 'next/router'
import { Button, withStyles } from '@material-ui/core';
import { Wrapper } from './AccidentStyled'

const ClaimButton = withStyles(theme => ({
    root: {
        color: "#FFF",
        height: 34,
        fontSize: 13,
        fontWeight: 500,
        
        backgroundColor: "#7cda23",
        "&:hover": {
            backgroundColor: "#569818"
        }
    }
}))(Button);

const CyclicSmallAccident = () => {
    const router = useRouter();
    const query = Object.assign({}, { ...router.query })
    console.log("TCL: ModalAccidentPolicy -> query", query)

    return <Wrapper>
        <div className="group_policy">
            <section className="product_info">
                <div className="product_title">Cyclic: Small Influenza GP</div>
                <p>Because a flu is Deadly while a cold is not</p>
                <div className="group_details">
                    <ul>
                        <li>
                            <div className="details">
                                <p>(1) Benefits</p>
                                <ul>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Reimburses you up to $70 per new diagnosis of Influenza ( Flu ) at a GP clinic / government polyclinic in Singapore.</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div className="details">
                                <p>(2) Claims</p>
                                <ul>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Covers Seasonal Flu, Influenza A, Influenza B, Influenza C, H & N Subtypes, Flu Pandemic, Swine Flu (H1N1), Bird Flu (H5N1).</p>
                                    </li>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Each claim must be at least 20 days apart from each other.</p>
                                    </li>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p> Make a 2nd claim from us for the same accident / incident, even after you make a similar claim from a traditional insurer.</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div className="details">
                                <p>(3) Special Notes</p>
                                <ul>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Valid till user is age 55.</p>
                                    </li>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Treatment must be provided by a GP clinic / government polyclinic, by a SMC-registered doctor ( practicing in Singapore ).
</p>
                                    </li>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>User must have a Singapore Bank account to apply for this.</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div className="details">
                                <p>(4) Exclusions</p>
                                <ul>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Claims from treatments in Specialist clinics and Hospitals.</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div className="details">
                                <p>(5) Low Claims definition</p>
                                <ul>
                                    <li>
                                        <div className="tick_icon">
                                            <Tick />
                                        </div>
                                        <p>Users who claim SGD$50 or less per cycle.</p>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>
            <section className="different">
                <div className="product_title">How are we different</div>
                <p>* There is currently No policy like this in Singapore.</p>
            </section>
            <section className="how_to_claim">
                <div className="product_title">HOW TO CLAIM</div>
                <p>[Cyclic: Small Influenza GP]</p>
                <p className="no_wrap text_left">When you need to make a claim, just:</p>
                <ul>
                    <li>
                        <p>1. Tell us:</p>
                        <div>(i) What happened in the incident</div>
                        <div>(ii) Who was the SMC-registered Doctor you consulted</div>
                        <div>(iii) Which GP clinic / government polyclinic you visited.</div>
                    </li>
                    <li>
                        <p>2. Upload photos of:</p>
                        <div>(i) Paid Receipt</div>
                        <div>(ii) Itemized Invoice of treatment and medicine,</div>
                        <div>(iii) MC with diagnosis / Letter of diagnosis.</div>
                    </li>
                </ul>
                <div className="policy_instructions">
                    <p>
                        * Just make sure the Doctor writes the Date of diagnosis + Description of diagnosis, on any 1 of the documents in the photos which you upload ( either Receipt / Invoice / MC with diagnosis / Letter of diagnosis ).
                </p>
                    <p>
                        SMC is the Singapore Medical Council ( a statutory board under the Ministry of Health ), governed under Section 5 of the Medical Registration Act (Cap 174). You can check if your doctor is SMC registered, via this link:
                <a href="https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC">https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC</a>
                    </p>
                    <p>
                        * On the Date of any Claim Submission, your policy cover must be active in order for it to be a processable Claim.
                </p>
                    <p>
                        ie. If your policy cover is no longer active for any reason, a new Claim Submission will not be entertained.
                </p>
                </div>
            </section>
            <div className="page_btn">
                <Link href={{ pathname: '/claims', query }} >
                    <ClaimButton data-track-id="claims" variant="contained">Still unsure about claims?</ClaimButton>
                </Link>
            </div>
        </div>
    </Wrapper>

}

export default CyclicSmallAccident