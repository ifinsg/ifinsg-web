import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Grid, Paper, Button } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import { makeStyles } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import Tick from '../icons/Tick';
import Star from '../icons/Star';
import { Wrapper } from './HomePageProductStyled';
import CyclicSmallAccident from './CyclicSmallAccident';
import CyclicSmallAccidentIncome from './CyclicSmallAccidentIncome';
import CyclicSmallInfluenzaGP from './CyclicSmallInfluenzaGP';
import CyclicSmallCOVID_19Income from './CyclicSmallCOVID_19Income';
import { currency_dollar_decimal_two } from 'global/helpers';
import { isMobile } from 'react-device-detect';

import ModalPricingDetails from './ModalPricingDetails';
import numeral from 'numeral';

const PurchaseButton = withStyles((theme) => ({
    root: {
        color: '#FFF',
        height: 34,
        fontSize: 13,
        fontWeight: 500,
        width: '100%',
        backgroundColor: '#7cda23',
        '&:hover': {
            backgroundColor: '#569818'
        }
    }
}))(Button);

const ComingSoonButton = withStyles((theme) => ({
    root: {
        color: '#FFF',
        height: 34,
        fontSize: 13,
        fontWeight: 500,
        width: '100%',
        backgroundColor: '#3D3A3A',
        '&:hover': {
            backgroundColor: '#3D3A3A'
        }
    }
}))(Button);

const BrowseButton = withStyles((theme) => ({
    root: {
        color: '#FFF',
        height: 34,
        fontSize: 13,
        textTransform: 'none',
        fontWeight: 500,
        backgroundColor: '#7cda23',
        '&:hover': {
            backgroundColor: '#569818'
        }
    }
}))(Button);

const PricingButton = withStyles((theme) => ({
    root: {
        color: '#FFF',
        height: 34,
        fontSize: 10,
        fontWeight: 500,
        backgroundColor: '#ddaa62',
        '&:hover': {
            backgroundColor: '#9a7644'
        }
    }
}))(Button);

const PolicyDetailsButton = withStyles((theme) => ({
    root: {
        color: '#FFF',
        fontSize: 10,
        fontWeight: 500,
        lineHeight: 1.1,
        backgroundColor: '#4c5fff',
        '&:hover': {
            backgroundColor: '#3542b2'
        }
    }
}))(Button);

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1
    },
    paper: {
        height: '100%',
        width: isMobile ? '290px' : '300px',
        display: 'flex',
        flexDirection: 'column',
        // justifyContent: 'space-between',
        overflow: 'hidden'
    },
    control: {
        padding: theme.spacing(2)
    }
}));

interface IHomePageProductProps {
    isHome?: boolean;
}

const HomePageProduct: React.FC<IHomePageProductProps> = ({ isHome }) => {
    const classes = useStyles({});

    const [openModalPolicy, setOpenModalPolicy] = React.useState(false);
    const [openModalPricing, setOpenModalPricing] = React.useState(false);

    const router = useRouter();

    // const query = Object.assign({}, { ...router.query }, { products: `["${products[productIndex].productId}"]` });

    const handlePolicyDetailsClicked = (p) => {
        setOpenModalPolicy(true);
        setProduct(p);
    };

    const handlePricingDetailsClicked = (p) => {
        setProduct(p);
        setOpenModalPricing(true);
    };

    const descriptionElementRef = React.useRef(null);
    React.useEffect(() => {
        if (openModalPolicy) {
            const { current: descriptionElement } = descriptionElementRef;
            if (descriptionElement !== null) {
                descriptionElement.focus();
            }
        }
    }, [openModalPolicy]);

    let data = [
        {
            id: 0,
            productId: 'small_accident',
            title: 'Cyclic: Small Accident',
            productNoticeMessage: [
                '<b>Up to $1,500 per accident!</b>',
                'No medical-history-related Loading, Exclusion or Reduction of claims!',
                'Make a 2nd claim from us for the same accident / incident, even after you make a similar claim from a traditional insurer.',
                'Yes we cover <b>all Sports</b> and <b>all Countries!</b>'
            ],
            priceNoticeMessage: [
                '<b>Up to $1,500 per accident!</b>',
                'Covers <b>All sports</b>, with no Reduction of claims!',
                'Covers <b>All countries!</b>',
                'Wide cover from <b>TCM to food poisoning to insect attacks!</b>',
                'Make a 2nd claim from us for the same accident / incident, even after you make a similar claim from a traditional insurer.'
            ],
            adminFee: 3.0,
            premiumPrice: 6.8,
            iconPath: '/product-img/smallaccident.png',
            available: true
        },
        {
            id: 1,
            productId: 'small_influenza_gp',
            title: 'Cyclic: Small Influenza GP',
            productNoticeMessage: [
                '<b>Up to $70 for each new Flu diagnosis!</b>',
                'No medical-history-related Loading, Exclusion or Reduction of claims!',
                'Make a 2nd claim from us for the same accident / incident, even after you make a similar claim from a traditional insurer.',
                'Yes we cover <b>all forms of Flu!</b>'
            ],
            priceNoticeMessage: [
                '<b>Up to $70 for each new Flu diagnosis!</b>',
                '<b>Wide range of cover</b> such as Seasonal Flu, Influenza A, Influenza B, Influenza C, H & N Subtypes, Flu Pandemic, Swine Flu (H1N1), Bird Flu (H5N1)!',
                'Make a 2nd claim from us for the same accident / incident, even after you make a similar claim from a traditional insurer.'
            ],
            premiumPrice: 6.0,
            adminFee: 3.0,
            iconPath: '/product-img/Flu.png',
            available: false
        },
        {
            id: 2,
            productId: 'small_accident_income',
            title: 'Cyclic: Small Accident Income',
            productNoticeMessage: [
                '<b>Claim $120 for each day that you are unable to work due to an accident!</b>',
                'No medical-history-related Loading, Exclusion or Reduction of claims!',
                'Make a 2nd claim from us for the same accident / incident, even after you make a similar claim from a traditional insurer.',
                'Yes to a claim <b>right after your 2nd day of accident MC!</b>'
            ],
            priceNoticeMessage: [
                '<b>Claim $120 for each day that you are unable to work due to an accident!</b>',
                'Make a claim even if you are <b>not hospitalized</b> for the accident!',
                'Make a claim <b>right after your 2nd day of accident MC!</b>',
                'Make a 2nd claim from us for the same accident / incident, even after you make a similar claim from a traditional insurer.'
            ],
            premiumPrice: 8.8,
            adminFee: 3.0,
            iconPath: '/product-img/Handicap.png',
            available: false
        }
    ];

    const [product, setProduct] = React.useState(data[0]);

    return (
        <Wrapper>
            <div className="home_page_product">
                <Grid justify="center" container spacing={3} direction="row" alignItems="stretch">
                    {data.map((item) => {
                        return (
                            <Grid item key={item.id}>
                                <Paper className={classes.paper}>
                                    <div style={{ margin: '20px 35px 0 35px' }}>
                                        <div className="product_title">{item.title}</div>
                                        <div className={'product_image ' + item.productId}>
                                            <img src={item.iconPath} />
                                        </div>
                                        {/* {this.renderPolicySellingPoints(product.policy)} */}
                                        <p className="price_month">
                                            {numeral(item.adminFee + item.premiumPrice).format(
                                                currency_dollar_decimal_two
                                            )}{' '}
                                            per month
                                        </p>
                                        {isHome &&
                                            item.productNoticeMessage.map((message, index) => {
                                                return (
                                                    <div className={'price_notice home_page'} key={index}>
                                                        <div className="tick_icon">
                                                            <Star />
                                                        </div>
                                                        <div
                                                            className="price_notice_message"
                                                            dangerouslySetInnerHTML={{ __html: message }}
                                                        ></div>
                                                    </div>
                                                );
                                            })}
                                        {!isHome &&
                                            item.priceNoticeMessage.map((message, index) => {
                                                return (
                                                    <div className={'price_notice'} key={index}>
                                                        <div className="tick_icon">
                                                            <Tick />
                                                        </div>
                                                        <div
                                                            className="price_notice_message"
                                                            dangerouslySetInnerHTML={{ __html: message }}
                                                        ></div>
                                                    </div>
                                                );
                                            })}
                                    </div>
                                    <div className={'purchase_btn ' + (!isHome ? 'product_page' : '')}>
                                        {!isHome && (
                                            <div className="product_policy_details">
                                                <PolicyDetailsButton
                                                    className="policy_details_btn"
                                                    variant="contained"
                                                    color="secondary"
                                                    onClick={() => {
                                                        handlePolicyDetailsClicked(item);
                                                        fbq('track', 'CustomizeProduct');
                                                    }}
                                                >
                                                    <div className="btn_policy">
                                                        <div>POLICY</div>
                                                        <div>DETAILS</div>
                                                    </div>
                                                </PolicyDetailsButton>
                                                <PricingButton
                                                    className="pricing_btn"
                                                    variant="contained"
                                                    onClick={() => {
                                                        handlePricingDetailsClicked(item);
                                                        fbq('track', 'AddToWishlist');
                                                    }}
                                                >
                                                    PRICING
                                                </PricingButton>
                                            </div>
                                        )}
                                        {!isHome ? (
                                            <div className="product_purchase">
                                                <Link
                                                    href={{
                                                        pathname: '/signup',
                                                        query: Object.assign(
                                                            {},
                                                            { ...router.query },
                                                            { products: `["${item.productId}"]` }
                                                        )
                                                    }}
                                                >
                                                    <div>
                                                        {item.available ? (
                                                            <PurchaseButton
                                                                onClick={() => {
                                                                    fbq('track', 'AddToCart');
                                                                }}
                                                                variant="contained"
                                                                data-track-id="get_insured"
                                                            >
                                                                PURCHASE
                                                            </PurchaseButton>
                                                        ) : (
                                                            <ComingSoonButton
                                                                onClick={(e) => {
                                                                    e.stopPropagation();
                                                                }}
                                                                variant="contained"
                                                                data-track-id="get_insured"
                                                            >
                                                                Coming soon
                                                            </ComingSoonButton>
                                                        )}
                                                    </div>
                                                </Link>
                                            </div>
                                        ) : (
                                            <div className="product_purchase">
                                                <Link
                                                    href={{
                                                        pathname: '/products'
                                                    }}
                                                >
                                                    <BrowseButton variant="contained" data-track-id="get_insured">
                                                        Browse Products
                                                    </BrowseButton>
                                                </Link>
                                                {/* {item.id === 0 && (
                                                    <div
                                                        style={{
                                                            position: 'relative',
                                                            marginTop: 17,
                                                            marginLeft: -23,
                                                            marginRight: -23,
                                                            border: '5px solid orange'
                                                        }}
                                                    >
                                                        <img
                                                            style={{ width: '100%', display: 'block' }}
                                                            src={'/promo/Sheng_Siong_vouchers.png'}
                                                        />
                                                    </div>
                                                )} */}
                                            </div>
                                        )}
                                    </div>
                                </Paper>
                            </Grid>
                        );
                    })}
                </Grid>
                <Dialog open={openModalPolicy} onClose={() => setOpenModalPolicy(false)} scroll={'paper'}>
                    <DialogContent dividers={true}>
                        <DialogContentText ref={descriptionElementRef} tabIndex={-1}>
                            <IconButton
                                style={{ position: 'absolute', top: 10, right: 10 }}
                                edge="start"
                                color="inherit"
                                onClick={() => setOpenModalPolicy(false)}
                                aria-label="close"
                            >
                                <CloseIcon />
                            </IconButton>
                            {product.id == 0 && <CyclicSmallAccident />}
                            {product.id == 1 && <CyclicSmallInfluenzaGP />}
                            {product.id == 2 && <CyclicSmallAccidentIncome />}
                            {product.id == 3 && <CyclicSmallCOVID_19Income />}
                        </DialogContentText>
                    </DialogContent>
                </Dialog>
                <Dialog open={openModalPricing} onClose={() => setOpenModalPricing(false)} scroll={'paper'}>
                    <DialogContent dividers={true}>
                        <DialogContentText ref={descriptionElementRef} tabIndex={-1}>
                            <IconButton
                                style={{ position: 'absolute', top: 10, right: 10 }}
                                edge="start"
                                color="inherit"
                                onClick={() => setOpenModalPricing(false)}
                                aria-label="close"
                            >
                                <CloseIcon />
                            </IconButton>
                            <ModalPricingDetails product={product} />
                        </DialogContentText>
                    </DialogContent>
                </Dialog>
            </div>
        </Wrapper>
    );
};

export default HomePageProduct;
