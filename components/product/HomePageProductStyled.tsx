import styled from 'styled-components'

export const Wrapper = styled.div`


  @media (max-width: 360px){
    .purchase_btn {
      /* padding: 0 40px */
      &.product_page {
        padding: 0;
      }
    }
  }

  .home_page_product {
    /* font-family: Helvetica, Arial, sans-serif; */
    padding: 0 15px;

    .product_title {
      font-size: 20px;
      font-weight: bold;
      color: #7cda24;
      min-height: 40px;
      padding: 0;
      margin: 0;
      text-align: center;
    }
    
    .product_image {
      text-align: center;
      > img {
        max-width: 150px;
      }
      &.small_novel_coronavirus{
        > img {
          width: 120px;
          margin: 15px 0px;
        }
      }
     
    }

    .price_month {
      font-size: 17px;
      color: #e09900;
      padding: 0;
      margin: 0 0 15px 0;
      text-align: center;
    }

    .price_notice {
      display: flex;
      margin-bottom: 15px;
      &.home_page{
        /* justify-content: center; */
      }

      > .tick_icon {
        max-width:16px;
        min-width:16px;
        margin-top: 4px;
      }

      > .price_notice_message {
        font-size: 13px;
        text-align: left;
        padding: 0 0 0 5px;
        line-height: 24px;
      }
    }

    .product_policy_details {
      display: flex;
      justify-content: space-between;
      margin-top: 10px;

      .policy_details_btn {
        margin-right: 20px;
        .btn_policy {
          flex-direction: row;
          font-size: 10px;
          width: 100%;
          line-height: 1.1;
        }
      }

      .pricing_btn {
      }
    }

    .purchase_btn {
        padding: 0 35px 20px 35px;
        &.product_page {
        }
      }

    .product_purchase {
      margin-top: 10px;
      text-align: center;
      
    }
}

`