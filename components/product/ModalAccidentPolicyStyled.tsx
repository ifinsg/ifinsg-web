import styled from 'styled-components'


export const Wrapper = styled.div`
.group_policy {
  color: #212529;

  .no_wrap {
    white-space: nowrap;
  }

  ul,
  li {
    list-style: none;
    margin: 0;
    padding: 0;
  }

  .policy_instructions {
    margin-top: 20px;
    > p {
      margin-bottom: 35px;
      > a {
        color: #7cda24;
        overflow-wrap: break-word;
      }
    }
  }

  > section {
    margin-top: 10px;

    &.product_info {

        .product_title {
            text-align:center;
            font-size: 19px;
            font-weight: bold;
            color: #7cda24;
            min-height: 40px;
            padding: 0;
            margin: 0;
        }
    }

    &.how_to_claim {
      > ul {
        > li {
          margin-top: 10px;
          > p {
          }

          > div {
            padding: 0 20px;
          }
        }
      }
    }

    &.different {
      > p {
        color: #228b22;
        text-align: left;
        font-weight: bold;
      }
    }

    > h4 {
      color: #7cda24;
      text-align: center;
      font-weight: bold;
      padding: 20px 20px 0 20px;
    }

    > p {
      text-align: center;
    }

    .group_details {
      > ul {
        > li {
          .details {
            > p {
              font-weight: bold;
            }

            > ul {
              > li {
                display: flex;

                .tick_icon {
                  margin-right: 10px;
                }

                > p {
                }
              }
            }
          }
        }
      }
    }
  }

  .page_btn {
    text-align: center;
  }
}

`