import React from 'react';
import { Wrapper } from './ModalPricingDetailsStyled'
import numeral from 'numeral';
import { currency_dollar_decimal_two } from 'global/helpers'

interface IModalPricingDetailsProps {
    product: {
        id: number
        productId: string;
        title: string;
        priceNoticeMessage: string[];
        adminFee: number;
        premiumPrice: number;
    }
}

const ModalPricingDetails: React.FC<IModalPricingDetailsProps> = (props) => {
    const premiumPrice = props.product.premiumPrice;
    const adminFee = props.product.adminFee;
    console.log(premiumPrice, adminFee)
    const total = (premiumPrice + adminFee) * 3;

    return <Wrapper>
        <div className="modal_pricing">
            <div className="product_title">{props.product.title}</div>
            <ul>
                <li>
                    <div>
                        <span>Premium</span>
                        <span className="single_price">{numeral(premiumPrice * 3).format(currency_dollar_decimal_two)}</span>
                    </div>
                    <p className="fee_price">({numeral(premiumPrice).format(currency_dollar_decimal_two)} per month)</p>
                </li>
                <li>
                    <div>
                        <span>Admin Fee</span>
                        <span className="single_price">{numeral(adminFee * 3).format(currency_dollar_decimal_two)}</span>
                    </div>
                    <p className="fee_price">({numeral(adminFee).format(currency_dollar_decimal_two)} per month)</p>
                </li>
                <li>
                    <div className="total">
                        <span>Total</span>
                        <span>{numeral(total).format(currency_dollar_decimal_two)}</span>
                    </div>
                    <p className="total_tips">per Cycle of <i>3 months</i></p>
                    <p className="fee_price">({numeral(total / 3).format(currency_dollar_decimal_two)} per month)</p>
                </li>
            </ul>
        </div>
    </Wrapper>
}

export default ModalPricingDetails