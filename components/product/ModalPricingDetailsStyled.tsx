import styled from 'styled-components'


export const Wrapper = styled.div`

.modal_pricing {
  margin: 0;
  color: #212529;
  width:260px;

  .product_title {
    text-align:center;
    font-size: 19px;
    font-weight: bold;
    color: #7cda24;
    min-height: 40px;
    padding: 0;
    margin: 0;
  }

  h5 {
    color: #7bda24;
    font-weight: bold;
  }

  > ul {
    list-style: none;
    margin: 0;
    padding: 0;
    > li {
      list-style: none;
      margin: 0;
      padding: 0;
      > div {
        display: flex;
        justify-content: space-between;

        .total {
          color: #228b22;
        }
      }

      .single_price {
      }

      .fee_price {
        color: #e09900;
        text-align: end;
        font-weight: normal;
      }

      &:last-child {
        color: #228b22;
        font-weight: bold;
      }

      .total_tips {
        text-align: end;
        color: #228b22;
        font-weight: bold;
        margin-bottom: 0;
        font-size: 12px;
        > i {
          color: #8695fd;
        }

        .fee_price {
          color: #e09900;
          text-align: end;
          font-weight: normal;
        }
      }
    }
  }
}

`