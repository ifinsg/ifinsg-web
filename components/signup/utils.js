import {
    Link, Element, Events, animateScroll as scroll, scrollSpy, scroller
} from "react-scroll";
import {
    BrowserView,
    MobileView,
    isBrowser,
    isMobile
} from "react-device-detect";

export const scrollToElement = element_indentifier => {
    if (isMobile) {
        scroller.scrollTo(element_indentifier, {
            duration: 500,
            delay: 0,
            smooth: true,
            offset: -180
        });
    }
};
