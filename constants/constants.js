const get_title_from_page = {
    faq: 'Frequently Asked Questions',
    disclaimers: 'Disclaimers',
    claims: 'Claims',
    'premium-back': 'PremiumBack',
    'about-us': 'About us',
    resources: 'Resources',
    products: 'Products',
    'disclaimer-of-liability': 'Disclaimer Of Liability',
    'forgot-password': 'Forgot Password',
    'general-terms-and-conditions': 'General Terms & Conditions',
    login: 'Login',
    'page-not-found': 'Page Not Found',
    'privacy-policy': 'Privacy Policy',
    profile: 'Profile',
    qr: 'QR',
    'reset-password': 'Reset Password',
    signup: 'Signup',
    'terms-of-service': 'Terms Of Service',
    'terms-of-use': 'Terms Of Use',
    'i-search-professional': 'iSearchProfessional'
};

const product_details_promotion = {
    small_accident: {
        promotion: {
            name: 'forever_admin_fee_100', //"small_accident_680_100",
            date_start: '2019-06-24',
            date_end: '2019-06-30'
        },
        name: 'Small Accident',
        admin_fee: 3,
        admin_fee_promotional_price: 1,
        admin_fee_on_promotion: true,
        premium: 6.8,
        premium_promotional_price: false,
        premium_on_promotion: false,
        total: 9.8,
        total_promotional_price: 7.8,
        total_on_promotion: true
    }
};

let promotion_ongoing = true;
////// Setting Promotion cut-off date //////
if (new Date().getTime() + 8 * 60 * 60 * 1000 > new Date('2019-10-01').getTime()) {
    promotion_ongoing = false;
}

const products_on_sale = [
    //  availability: 'available' OR 'coming_soon'
    { policy: 'small_accident', availability: 'available', img: 'smallaccident.png' },
    // { policy: 'medium_accident', availability: 'available', img: 'mediumaccident.png' },
    { policy: 'small_influenza_gp', availability: 'available', img: 'Flu.png' },
    { policy: 'small_accident_income', availability: 'available', img: 'Handicap.png' },
    { policy: 'small_covid19_income', availability: 'available', img: 'Small_Novel_Coronavirus_Green.png' }
    // { policy: 'small_pmd_protect', availability: 'coming_soon', img: 'smallpmdprotect.png' },
    // { policy: 'small_critical_checkup', availability: 'coming_soon', img: 'Checkup.png' },
];

const product_details = {
    small_accident: {
        promotion: null,
        name: 'Small Accident',
        cycle_length: 3, // months
        admin_fee: 3,
        admin_fee_promotional_price: false,
        admin_fee_on_promotion: false,
        premium: 6.8,
        premium_promotional_price: false,
        premium_on_promotion: false,
        total: 9.8,
        total_promotional_price: false,
        premium_on_promotion: false,
        sum_assured_text: 'Reimburses you up to $1,500 per accident',
        accident_type: 'accident',
        claim_required_fields: [
            'accident_id',
            'accident_description',
            'accident_date',
            'clinic_visit_date',
            'medical_registrar_smc',
            'doctor_seen',
            'clinic_visited',
            'treatment_description',
            'uploaded_images',
            'addon_notes',
            'ack_claim_from_other_insurers_first'
        ]
    },
    medium_accident: {
        promotion: null,
        name: 'Medium Accident',
        cycle_length: 6, // months
        admin_fee: 4,
        admin_fee_promotional_price: false,
        admin_fee_on_promotion: false,
        premium: 13.4,
        premium_promotional_price: false,
        premium_on_promotion: false,
        total: 17.4,
        total_promotional_price: false,
        premium_on_promotion: false,
        sum_assured_text: 'Reimburses you up to $4,000 per accident',
        accident_type: 'accident',
        claim_required_fields: [
            'accident_id',
            'accident_description',
            'accident_date',
            'clinic_visit_date',
            'medical_registrar_smc',
            'doctor_seen',
            'clinic_visited',
            'treatment_description',
            'uploaded_images',
            'addon_notes',
            'ack_claim_from_other_insurers_first'
        ]
    },
    small_influenza_gp: {
        promotion: null,
        name: 'Small Influenza GP',
        cycle_length: 3, // months
        admin_fee: 3,
        admin_fee_promotional_price: false,
        admin_fee_on_promotion: false,
        premium: 6,
        premium_promotional_price: false,
        premium_on_promotion: false,
        total: 9,
        total_promotional_price: false,
        premium_on_promotion: false,
        sum_assured_text: 'Reimburses you up to $70 per new diagnosis of Influenza (Flu)',
        accident_type: 'flu',
        claim_required_fields: [
            'clinic_visit_date',
            'medical_registrar_smc',
            'doctor_seen',
            'clinic_visited',
            'treatment_description',
            'uploaded_images',
            'addon_notes',
            'ack_claim_from_other_insurers_first'
        ]
    },
    small_accident_income: {
        promotion: null,
        name: 'Small Accident Income',
        cycle_length: 3, // months
        admin_fee: 3,
        admin_fee_promotional_price: false,
        admin_fee_on_promotion: false,
        premium: 8.8,
        premium_promotional_price: false,
        premium_on_promotion: false,
        total: 11.8,
        total_promotional_price: false,
        premium_on_promotion: false,
        sum_assured_text:
            'Claim $120 for each day that you are unable to work, starting from the 3rd day of your accident MC',
        accident_type: 'accident',
        claim_required_fields: [
            'accident_description',
            'accident_date',
            'clinic_visit_date',
            'medical_registrar_smc',
            'doctor_seen',
            'clinic_visited',
            'treatment_description',
            'uploaded_images',
            'addon_notes',
            'ack_claim_from_other_insurers_first'
        ]
    },
    small_covid19_income: {
        promotion: null,
        name: 'Small COVID-19 Income',
        cycle_length: 3, // months
        admin_fee: 3,
        admin_fee_promotional_price: false,
        admin_fee_on_promotion: false,
        premium: 6.88,
        premium_promotional_price: false,
        premium_on_promotion: false,
        total: 9.88,
        total_promotional_price: false,
        premium_on_promotion: false,
        sum_assured_text: ' ????      ????',
        accident_type: 'flu',
        claim_required_fields: [
            'clinic_visit_date',
            'medical_registrar_smc',
            'doctor_seen',
            'clinic_visited',
            'treatment_description',
            'uploaded_images',
            'addon_notes',
            'ack_claim_from_other_insurers_first'
        ]
    },
    small_pmd_protect: {
        promotion: null,
        name: 'Small PMD Protect',
        cycle_length: 3, // months
        admin_fee: 3,
        admin_fee_promotional_price: false,
        admin_fee_on_promotion: false,
        premium: 6.7,
        premium_promotional_price: false,
        premium_on_promotion: false,
        total: 9.7,
        total_promotional_price: false,
        premium_on_promotion: false,
        sum_assured_text:
            'Reimburses you 80% of the PMD repair bill ( capped at total bill size of $500 per cycle ), when it’s due to Collision damage and Water short circuit damage. Reimburses you up to $40 for a One-way transport fee to send your immobile PMD to the repair centre, when a repair is done for Collision damage and Water short circuit damage.',
        accident_type: 'accident',
        claim_required_fields: [
            'accident_description',
            'accident_date',
            'clinic_visit_date',
            'medical_registrar_smc',
            'doctor_seen',
            'clinic_visited',
            'treatment_description',
            'uploaded_images',
            'addon_notes',
            'ack_claim_from_other_insurers_first'
        ]
    },
    small_critical_checkup: {
        name: 'Small Critical Checkup',
        cycle_length: 3, // months
        admin_fee: 3,
        admin_fee_promotional_price: false,
        admin_fee_on_promotion: false,
        premium: 13.6,
        premium_promotional_price: false,
        premium_on_promotion: false,
        total: 16.6,
        total_promotional_price: false,
        premium_on_promotion: false,
        accident_type: 'accident'
    }
};

// if (new Date(product_details_promotion.small_accident.promotion.date_start) <= new Date() && new Date(product_details_promotion.small_accident.promotion.date_end) > new Date()) {
//     product_details.small_accident = product_details_promotion.small_accident
//     promotion_ongoing = true
// }

module.exports = {
    browsers_supported: ['ios', 'safari', 'chrome', 'firefox'], // Using detect-browser, browser.name
    products_on_sale,
    product_details,
    promotion_ongoing,
    // {
    // small_accident: {
    //     name: "Small Accident",
    //     price_monthly: {
    //         admin_fee: {
    //             normal: 3,
    //             forever_admin_fee_100: 1
    //         },
    //         premium: {
    //             normal: 6.8,
    //             forever_admin_fee_100: false
    //         },
    //         total: {
    //             normal: 9.8,
    //             forever_admin_fee_100: 7.8
    //         },
    //     },
    // },

    // small_critical_checkup: {
    //     name: "Small Critical Checkup",
    //     premium: 40.8,
    //     admin_fee: 9
    // }
    // },

    color: {
        green: '#7cda24',
        darkgreen: '#228b22',
        grey: 'rgba(0,0,0,.6)',
        gold: '#e09900'
        // red: ''
    },

    style: {
        page: {
            width: { maxWidth: '1200px', margin: '0 auto' },
            text_width: { maxWidth: '800px', margin: '0 auto' },
            padding: { padding: '82px 20px 0 20px' }
        },
        align: {
            full_width: { width: '100%' },
            center: { textAlign: 'center' },
            left: { textAlign: 'left' },
            right: { textAlign: 'right' },
            position_fixed: { position: 'fixed' }
        },
        text: {
            bold: { fontWeight: 'bold' },
            break_word: { wordBreak: 'break-word' },
            // grey: { color: "rgba(0,0,0,.6)" },
            // black: { color: "black" },

            title1: { fontSize: '42px', lineHeight: '1', color: 'black' },
            title2: { color: 'red' },

            body1: { fontSize: '20px', lineHeight: '1.8' }
        },
        margin_top: {
            xl: { marginTop: '150px' },
            lg: { marginTop: '100px' },
            md: { marginTop: '70px' },
            sm: { marginTop: '40px' },
            xs: { marginTop: '20px' },
            xxs: { marginTop: '10px' }
        }
    },

    bank_full_name_from_short_name: {
        dbs: 'DBS/POSB',
        ocbc: 'OCBC',
        uob: 'UOB',
        sc: 'Standard Charted',
        hsbc: 'HSBC ',
        citi: 'Citibank',
        maybank: 'Maybank',
        rhb: 'RHB',
        cimb: 'CIMB',
        bo_china: 'Bank of China',
        deutsche: 'Deutsche Bank ',
        hl_bank: 'HL Bank',
        bnp_paribas: 'BNP Paribas',
        far_eastern: 'Far Eastern Bank',
        mizuho: ' Mizuho Bank',
        sumitomo_mitsui: 'Sumitomo Mitsui Banking Corporation ',
        bo_tokyo_mitsubishi_ufj: 'The Bank of Tokyo-Mitsubishi UFJ',
        bo_scotland: 'The Royal Bank of Scotland',
        aus_nz_banking_grp: 'Australia and New Zealand Banking Group'
    },

    next_seo_config: (page) => {
        let config = {
            title: 'iFinSG | The same protection, done Differently.',
            description: 'Insurance the way you want it. Simple. Fast. Cheap.',
            canonical: 'https://www.ifinsg.com', // What is the master url
            openGraph: {
                url: 'https://www.ifinsg.com',
                title: 'iFinSG | The same protection, done Differently.',
                description: 'Insurance the way you want it. Simple. Fast. Cheap.',
                // description: "Pure protection in case of practical events, and still gives you money at the end of every cycle, even when you do not make a claim!",
                site_name: 'iFinSG',
                type: 'website',
                profile: {
                    firstName: 'ifinsg',
                    lastName: 'editor',
                    username: 'ifinsg editor',
                    gender: 'male'
                },
                images: [
                    {
                        url: 'https://www.ifinsg.com/ifinsg_logo.jpg',
                        width: 620,
                        height: 350,
                        alt: 'iFinSG Logo'
                    }
                    // {
                    //   url: 'https://i2.wp.com/ifinsg.com/wp-content/uploads/2019/02/iFinSG-Logo-OG-2.jpg?fit=520%2C270&ssl=1',
                    //   width: 620,
                    //   height: 350,
                    //   alt: 'iFinSG Logo',
                    // },
                    // {
                    //   url: 'https://ifinsg.com/favicon.ico',
                    //   width: 256,
                    //   height: 256,
                    //   alt: 'iFinSG Logo',
                    // },
                    // {
                    //   url: 'https://ifinsg.com/ifinsg_logo.png',
                    //   width: 800,
                    //   height: 800,
                    //   alt: 'iFinSG Logo',
                    // },
                ]
            }
        };

        let title = get_title_from_page[page] + ' | iFinSG';
        let canonical = config.canonical + '/' + page;
        let title_og = get_title_from_page[page];
        let description = 'The same protection, done Differently.'; // This should be over-writen, should not be seen as description

        switch (page) {
            case 'home':
                return config;
            case 'faq':
                config.title = title;
                config.openGraph.title = title_og;
                config.canonical = canonical;
                description = 'Understand why Cyclic insurance is right for you.';
                config.description = 'Understand why Cyclic insurance is right for you.';
                config.openGraph.description = 'Understand why Cyclic insurance is right for you.';
                return config;
            case 'disclaimers':
                config.title = title;
                config.openGraph.title = title_og;
                config.canonical = canonical;
                description =
                    'iFinSG is a insurance technology platform, which does not fall under the Insurance Act as an insurer or insurance intermediary broker, nor do we help any insurance companies to sell their policies.';
                config.description = description;
                config.openGraph.description = description;
                return config;
            case 'claims':
                config.title = title;
                config.openGraph.title = title_og;
                config.canonical = canonical;
                description =
                    'Claims are handled by our devoted team and a set of very smart algorithms. No magic, no exaggeration and no bullshit.';
                config.description = description;
                config.openGraph.description = description;
                return config;
            case 'premium-back':
                config.title = title;
                config.openGraph.title = title_og;
                config.canonical = canonical;
                description =
                    'At the end of every cycle, after paying out verified claims, all premiums unused for claims will be returned as PremiumBack to users who made 0 claims and users who made low claims.';
                config.description = description;
                config.openGraph.description = description;
                return config;
            case 'about-us':
                config.title = title;
                config.openGraph.title = title_og;
                config.canonical = canonical;
                description =
                    'At the end of every cycle, after paying out verified claims, all premiums unused for claims will be returned as PremiumBack to users who made 0 claims and users who made low claims.';
                config.description = description;
                config.openGraph.description = description;
                return config;
            case 'resources':
                config.title = title;
                config.openGraph.title = title_og;
                config.canonical = canonical;
                description = 'iConsumerForms and more.';
                config.description = description;
                config.openGraph.description = description;
                return config;
            case 'products':
                config.title = title;
                config.openGraph.title = title_og;
                config.canonical = canonical;
                description =
                    'Reimburses you up to $1,500 per accident (even for medical treatment of sprains, cuts or falls), covering All sports and has no Reduction of claims for any sport.';
                config.description = description;
                config.openGraph.description = description;
                return config;
            case 'i-search-professional':
                config.title = title;
                config.openGraph.title = title_og;
                config.canonical = canonical;
                description = 'iSearchProfessional';
                config.description = description;
                config.openGraph.description = description;
                return config;
            default:
                config.title = title;
                config.openGraph.title = title_og;
                return config;
        }
    },

    get_new_page: (page, referrer) => {
        let new_page = '/' + page + (referrer ? '/' + referrer : '');
        if (page === 'home' || page === '') {
            new_page = '/' + (referrer ? referrer : '');
        }
        return new_page;
    },

    store_referrer: (context) => {
        // Storing referrer
        let params = context && context.props && context.props.router && context.props.router.query;
        if (params && params.referrer) {
            context.setState({ referrer: params.referrer });
        }
    }
};
