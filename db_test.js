const path = require("path");
require("dotenv").config({ path: path.join(__dirname, "/.env.production") });

const { db } = require("./server/helpers/pg");

function getTransactionScope(pKnex, callback) {
    try {
        pKnex.transaction(function(trx) {
            return callback(trx);
        });
    } catch (error) {
        console.log(error);
    }
}

function ExecuteSQLQuery(pTranDB, pTrx, pQuery, pCallback) {
    try {
        var query = pTranDB.raw(pQuery);

        if (pTrx) {
            query = query.transacting(pTrx);
        }
        query
            .then(function(res, error) {
                try {
                    if (error) {
                        console.log(error);
                    } else {
                        return pCallback(res, error);
                    }
                } catch (error) {
                    console.log(error);
                }
            })
            .catch(function(error) {
                return pCallback(null, error);
            });
    } catch (error) {
        console.log(error);
    }
}

function Commit(pTrx, pIsCommit) {
    try {
        if (pIsCommit) {
            pTrx.commit();
        } else {
            pTrx.rollback();
        }
    } catch (error) {
        console.log(error);
    }
}

function getData() {
    db.raw("SELECT * FROM users").then((res) => {
        console.log(res.rows.length);
    });
}

getData();
