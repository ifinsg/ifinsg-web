module.exports = {
    apps: [
        {
            name: "ifinsg-web",
            script: "./server.js",
            env: {
                NODE_ENV: "production"
            },
            env_production: {
                PORT: 8080,
                NODE_ENV: "production"
            }
        }
    ]
};
