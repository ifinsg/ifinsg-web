const nodemailer = require('nodemailer');

const { google } = require('googleapis');

let client = {};
console.log('🚀 ~ file: app.js ~ line 7 ~ process.env.NODE_ENV', process.env.NODE_ENV);
if (process.env.NODE_ENV === 'production') {
    // adele account
    client = {
        USER: 'adele@ifinancialsingapore.com',
        CLIENT_ID: '268155917792-e50jtir62p9pus7rc8j23sv5u72qd26l.apps.googleusercontent.com',
        CLIENT_SECRET: 'bKGeK51YMxER6HscnTSRENzf',
        REDIRECT_URI: 'https://developers.google.com/oauthplayground',
        REFRESH_TOKEN:
            '1//04QhtWfX7HUPHCgYIARAAGAQSNwF-L9Irve7OJ4luUMEOoQTuUC4-wkpizWDEqRb99iCaUNLUv01EE1Al_G6PkYOmPC99AGmHgqU'
    };
} else {
    // test
    // yibu.wang account
    client = {
        USER: 'yibu.wang@ifinancialsingapore.com',
        CLIENT_ID: '268155917792-j3ac1nafv41qlp3sg0nc6d5fvfklgruf.apps.googleusercontent.com',
        CLIENT_SECRET: 'qoLpXcnJOdUIhblgeZyocxUZ',
        REDIRECT_URI: 'https://developers.google.com/oauthplayground',
        REFRESH_TOKEN:
            '1//04zP1z_lF42E0CgYIARAAGAQSNwF-L9IrTyIkp4owutmwV2-3NlpgPEbwGyCRy9KMn8THjUkWOlxsSdeDXLcUNvXz3BMBYYRU4PE'
    };
}

// const CLIENT_ID = client.CLIENT_ID;
// const CLIENT_SECRET = client.CLIENT_SECRET;
// const REDIRECT_URI = client.REDIRECT_URI;
// const REFRESH_TOKEN = client.REFRESH_TOKEN;

const oAuth2Client = new google.auth.OAuth2(client.CLIENT_ID, client.CLIENT_SECRET, client.REDIRECT_URI);

oAuth2Client.setCredentials({ refresh_token: client.REFRESH_TOKEN });

async function send_email({ to, subject, text, html }) {
    try {
        const accessToken = await oAuth2Client.getAccessToken();

        const transport = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                type: 'OAuth2',
                user: client.USER,
                clientId: client.CLIENT_ID,
                clientSecret: client.CLIENT_SECRET,
                refreshToken: client.REFRESH_TOKEN,
                accessToken: accessToken
            }
        });

        const mailOptions = {
            from: client.USER,
            to,
            subject,
            text,
            html
        };

        const result = await transport.sendMail(mailOptions);

        return result;
    } catch (error) {
        return error;
    }
}

module.exports = {
    send_email
};
