// const base_domain_live = "https://ifinsg.com"
// const base_domain_staging = 'https://staging-dot-ifinsgdev2.appspot.com';
// const base_domain_dev = "http://localhost:8080"

let mode_dev = process.env.NODE_ENV === 'development' ? true : false;

let collection_users = 'live_users';
let collection_policies = 'live_policies';
let collection_events = 'live_events';
let collection_users_raw_data = 'live_users_raw_data';

if (process.env.NODE_ENV === 'development') {
    collection_users = 'staging_users';
    collection_policies = 'staging_policies';
    collection_events = 'staging_events';
    collection_users_raw_data = 'staging_users_raw_data';
}

let stripe_key_publishable = '';

if (process.env.NODE_ENV === 'development') {
    stripe_key_publishable = 'pk_test_0t5UyM3s5YvsWVj9417Ws71a'; // test (hugo.khoo@ifinancialsingapore.com)
    // stripe_key_publishable = 'pk_test_Xc43Wua0VWsQLILDkaTyEA2l'        // test (jiexiongkhoo@gmail.com)
} else if (process.env.NODE_ENV === 'production') {
    stripe_key_publishable = 'pk_live_ElPNGjCMFqsY2q12HmLQoqTv'; // live (hugo.khoo@ifinancialsingapore.com)
}

//////////////////////////////////////////// Functions ////////////////////////////////////////////

let list_of_pages = [
    'home2',
    'claims',
    'disclaimer-of-liability',
    'disclaimers',
    'faq',
    'forgot-password',
    'general-terms-and-conditions',
    'login',
    'page-not-found',
    'about-us',
    'premium-back',
    'privacy-policy',
    'products',
    'profile',
    'reset-password',
    'resources',
    'signup',
    'terms-of-service',
    'terms-of-use',
    'i-search-professional'
];
//////////////////// Affiliates ////////////////////
list_of_pages.push('affiliates');
list_of_pages.push('qr');
list_of_pages.push('smallaccident');
list_of_pages.push('mediumaccident');
list_of_pages.push('smallaccidentincome');
list_of_pages.push('smallinfluenzagp');
list_of_pages.push('smallpmdprotect');

let list_of_affiliates_influencers = [
    'Facebook',
    'social',
    'MailingList',
    'business',
    'marq',
    'joanna',
    'ray',
    'hugo',
    'passion',
    'wattah',
    'kdc',
    'starngage',
    'aloride',
    'gmt',
    'matlifeglobal',
    'fb',
    'ftgroup',
    'singroll',
    'lyjads',
    'marzze',
    'linkedinselfemployed',
    'linkedinself-employed',
    'linkedinfreelance',
    'linkedinfreelancer',
    'linkedinopportunity',
    'linkedinfounder',
    'linkedincofounder',
    'linkedinco-founder',
    'lunapark',
    'paladium'
];

for (let i = 1; i <= 70; i++) {
    list_of_affiliates_influencers.push(`fb${i}`);
}
for (let i = 1; i <= 70; i++) {
    list_of_affiliates_influencers.push(`ftgroup${i}`);
}

const generate_array_of_numbers_string = (num_start, num_end) => {
    let array_of_numbers_string = [];
    for (let i = num_start; i <= num_end; i++) {
        array_of_numbers_string.push(i.toString());
    }
    return array_of_numbers_string;
    // return [...Array(num_end+1).keys()].map(x=>x.toString())       // Not supported by internet explorer & Edge:(
};
const list_of_affiliates_sales = generate_array_of_numbers_string(1, 999);
const list_of_affiliates = list_of_affiliates_sales.concat(list_of_affiliates_influencers);

//////////////////////////////////////////// Exporting ////////////////////////////////////////////

module.exports = {
    staging: process.env.NODE_ENV === 'development',
    mode_dev: mode_dev,
    // domain_base: domain_base,

    list_of_pages,
    list_of_affiliates,
    list_of_admins: [
        'JIEXIONGKHOO@GMAIL.COM',
        'W1BBOND@GMAIL.COM',
        'WANGYIBU123@GMAIL.COM',
        'MILDJX@GMAIL.COM',
        'JOANNASEE@IFINANCIALSINGAPORE.COM',
        'AFFILIATESMQ@EMAIL.COM',
        'MARQSIEW@IFINANCIALSINGAPORE.COM',
        'LUUDUCHIEUHARRY@GMAIL.COM',
        'YIWIGO2039@LANCASTERCOC.COM'
    ],

    collection_users,
    collection_policies,
    collection_events,
    collection_users_raw_data,

    stripe_key_publishable,
    stripe_policy_payment_plans: {
        small_accident: 'plan_3monthly_2940',
        small_influenza_gp: 'plan_3monthly_2700',
        small_accident_income: 'plan_3monthly_3540',
        small_covid19_Income: 'plan_3monthly_4710'
    },
    stripe_policy_coupons: {
        forever_admin_fee_100: 'small_accident_admin_fee_100',
        normal: false
    }
};
