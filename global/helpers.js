const global_constants = require("./constants");

var cloneDeep = require("lodash.clonedeep");
var isEqual = require("lodash.isequal");

const padNumber = (n, width, z) => {
    // pad(10, 4);      // 0010
    // pad(9, 4);       // 0009
    // pad(123, 4);     // 0123
    // pad(10, 4, '-'); // --10

    z = z || "0";
    n = n + "";
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
};

const displayDate = (date_input, format, offset) => {
    // offset in milliseconds

    let date_obj = new Date(date_input);
    if (offset) {
        date_obj = new Date(new Date(date_input).getTime() + offset);
    }

    const year = padNumber(date_obj.getFullYear(), 4);
    const month = padNumber(date_obj.getMonth() + 1, 2);
    const date = padNumber(date_obj.getDate(), 2);

    const hour = padNumber(date_obj.getHours(), 2);
    const min = padNumber(date_obj.getMinutes(), 2);
    const sec = padNumber(date_obj.getSeconds(), 2);
    const milli = padNumber(date_obj.getMilliseconds(), 3);

    switch (format) {
        case "dd-MM-yyyy":
            return date + "-" + month + "-" + year;
        case "dd-MM-yyyy HH:mm":
            return date + "-" + month + "-" + year + " " + hour + ":" + min;
        case "dd-MM-yyyy HH:mm:ss":
            return date + "-" + month + "-" + year + " " + hour + ":" + min + ":" + sec;
        case "dd-MM-yyyy HH:mm:ss:milli":
            return date + "-" + month + "-" + year + " " + hour + ":" + min + ":" + sec + ":" + milli;

        case "yyyy-MM-dd":
            return year + "-" + month + "-" + date;
        case "yyyy-MM-dd HH:mm":
            return year + "-" + month + "-" + date + " " + hour + ":" + min;
        case "yyyy-MM-dd HH:mm:ss":
            return year + "-" + month + "-" + date + " " + hour + ":" + min + ":" + sec;
        case "yyyy-MM-dd HH:mm:ss:milli":
            return year + "-" + month + "-" + date + " " + hour + ":" + min + ":" + sec + ":" + milli;
        default:
            return date + "-" + month + "-" + year;
    }
};

module.exports = {
    padNumber,
    showPreview: (value, max_chars) => {
        try {
            if (value.length > max_chars + 2) {
                // if (value.includes('@')) {
                //   if (value.split('@')[0] > max_chars + 2) {
                //     return (value.slice(0, max_chars) + "...")
                //   } else {
                //     return (value.split('@')[0] + '@...')
                //   }
                // }
                return value.slice(0, max_chars) + "...";
            }
        } catch (err) {}
        return value;
    },

    displayDate,

    dateStringFormattedyyyyMMdd: (date_input) => {
        let date_obj = new Date();

        if (date_input) {
            date_obj = new Date(date_input);
        }

        const year = padNumber(new Date(date_obj).getFullYear(), 4);
        const month = padNumber(new Date(date_obj).getMonth() + 1, 2);
        const date = padNumber(new Date(date_obj).getDate(), 2);
        // const years_ago_77_string = (parseInt(year) - max_age_for_purchase).toString() + '-' + month + '-' + date
        // const years_ago_77 = new Date(years_ago_77_string)

        const returnable = year + "-" + month + "-" + date;
        return returnable.toString();
    },

    convertDateISOToNormal: (date_iso) => {
        // Warning: not tested for robustness
        try {
            const dob_year = date_iso.split("-")[0];
            const dob_month = date_iso.split("-")[1];
            const dob_day = date_iso.split("-")[2];
            const display_dob = dob_day + "-" + dob_month + "-" + dob_year;
            return display_dob;
        } catch (err) {
            return "";
        }
    },

    generateRandomInteger: (max) => {
        return Math.floor(Math.random() * Math.floor(max));
    },

    make_financial: (number) => {
        if (number < 0) {
            return "(" + (number * -1).toFixed(2) + ")";
        } else {
            return number.toFixed(2);
        }
    },

    normalise_string: (string) => {
        // Sample output = '   hI    helLoW  99 '
        // Sample output = 'HI_HELLOW_99'
        string = string.toString();
        string = string.replace(/ {1,}/g, " "); // Combine multiple spaces to just 1
        string = string.replace(/\s+$/, ""); // Remove space if last character
        string = string.replace(/^ /, ""); // Remove space if first character
        string = string.replace(/\s/gi, "_"); // Replace all spaces with underscores
        string = string.toUpperCase();
        return string;
    },

    clone_deep: (clonable) => {
        return cloneDeep(clonable);
    },
    obj_is_empty: (obj) => {
        if (Object.keys(obj).length === 0 && obj.constructor === Object) {
            // If there are updates to any policy
            return true;
        } else {
            return false;
        }
    },

    obj_is_equal: (obj1, obj2) => {
        return isEqual(obj1, obj2);
    },

    stripe_get_next_cycle_time_seconds: (period, offset_sec) => {
        // offset_sec = 8 * 60 * 60
        let date_obj = new Date(new Date().getTime() + 8 * 60 * 60 * 1000); // Need to offset here, as server is in GMT time,
        if (global_constants.staging) {
            date_obj = new Date(new Date().getTime());
        }

        let month = date_obj.getMonth() + 1; // getMonth() returns 0 to 11
        let year = date_obj.getFullYear();

        const add_months = (months_to_add) => {
            for (let i = 0; i < months_to_add; i++) {
                if (month >= 11) {
                    month = 1;
                    year = year + 1;
                } else {
                    month = month + 1;
                }
            }
        };

        if (period === "monthly") {
            add_months(1);
        } else if (period === "quarterly") {
            if ([1, 4, 7, 10].indexOf(month) !== -1) {
                add_months(3);
            } else if ([2, 5, 8, 11].indexOf(month) !== -1) {
                add_months(2);
            } else if ([3, 6, 9, 12].indexOf(month) !== -1) {
                add_months(1);
            }
        }

        const date_string = "01";
        let month_string = padNumber(month, 2); // getMonth() returns 0 to 11
        let year_string = padNumber(year, 4);
        let start_of_next_cycle_seconds =
            new Date(year_string + "-" + month_string + "-" + date_string).getTime() / 1000 -
            (offset_sec ? offset_sec : 0);
        return start_of_next_cycle_seconds;
    },

    claim_data_to_json_and_array: (claim) => {
        let claim_json_and_array = {
            json: {},
            array: []
        };

        claim_json_and_array.json = {
            id: claim.id,
            created_at: claim.created_at,
            ref_username_norm: claim.ref_username_norm,
            ref_policy_id: claim.ref_policy_id,
            ref_accident_id: claim.ref_accident_id,
            medical_registrar_smc: claim.medical_registrar_smc ? "SMC" : "TCM",
            clinic_visit_date: claim.clinic_visit_date,
            doctor_seen: claim.doctor_seen,
            clinic_visited: claim.clinic_visited,
            treatment_description: claim.treatment_description,
            uploaded_images: claim.uploaded_images

            // { name: "Clinic Visit Date", options: { filter: true, sort: true, } },
            // { name: "Doctor Seen", options: { filter: false, sort: true, } },
            // { name: "Clinic Visited", options: { filter: false, sort: true, } },
            // { name: "Treatment Description", options: { filter: false, sort: false, } },
            // id: claim.id,
            // id: claim.id,
            // id: claim.id,
            // id: claim.id,
            // id: claim.id,
            // id: claim.id,
        };

        claim_json_and_array.array = Object.values(claim_json_and_array.json);
        return claim_json_and_array;
    },

    policy_data_to_json_and_array: (policy) => {
        let policy_json_and_array = {
            json: {},
            array: []
        };

        if (policy.policy.representing === "self") {
            policy_json_and_array.json = {
                id: policy.policy.id,
                product: policy.policy.product,
                status: policy.policy.status,
                username_norm: policy.user.username_norm,
                representing: policy.policy.representing,
                buyer: policy.user.full_name,
                buyer_pref_name: policy.user.preferred_name,
                full_name: policy.user.full_name,
                // preferred_name: policy.user.preferred_name,
                insured_pref_name: policy.user.preferred_name,
                nric_number: policy.user.nric_number,
                dob: policy.user.dob,
                username: policy.user.username, // email
                gender: policy.user.gender,
                mobile_number: policy.user.mobile_number,
                occupation: policy.user.occupation,
                self_employed:
                    policy.user.self_employed == undefined ? "" : policy.user.self_employed ? "true" : "false",
                place_of_birth: policy.user.place_of_birth,

                referrer: policy.policy.referrer,
                source: policy.policy.source,

                relationship_to_payer: policy.policy.relationship_to_payer
                    ? policy.policy.relationship_to_payer
                    : policy.policy.representing,
                created_at: global_constants.mode_dev
                    ? displayDate(policy.policy.created_at, "yyyy-MM-dd HH:mm:ss")
                    : displayDate(policy.policy.created_at, "yyyy-MM-dd HH:mm:ss", 8 * 60 * 60 * 1000), // This is in GMT+0

                bank: policy.user.bank + " - " + policy.user.bank_acct_number,
                // bank_acct_number: policy.user.bank_acct_number,

                bank_claims_to_payer: policy.policy.bank_claims ? true : true,
                bank_claims: policy.user.bank_claims
                    ? policy.user.bank_claims + " - " + policy.user.bank_acct_number_claims
                    : policy.user.bank + " - " + policy.user.bank_acct_number,
                // bank_acct_number_claims: policy.user.bank_acct_number_claims ? policy.user.bank_acct_number_claims : policy.user.bank_acct_number,

                bank_premiumback_to_payer: policy.policy.bank_premiumback ? true : true,
                bank_premiumback: policy.user.bank_premiumback
                    ? policy.user.bank_premiumback + " - " + policy.user.bank_acct_number_premiumback
                    : policy.user.bank + " - " + policy.user.bank_acct_number,
                // bank_acct_number_premiumback: policy.user.bank_acct_number_premiumback ? policy.user.bank_acct_number_premiumback : policy.user.bank_acct_number,

                uploaded_images: policy.policy.uploaded_images
                    ? policy.policy.uploaded_images
                    : policy.user.uploaded_images,
                product_details: policy.policy.product_details,
                status_history: policy.policy.status_history,
                email: policy.user.username,
                pending_rework_items: policy.policy.pending_rework_items,
                pending_rework_message: policy.policy.pending_rework_message
            };
        } else if (policy.policy.representing === "entity") {
            policy_json_and_array.json = {
                id: policy.policy.id,
                product: policy.policy.product,
                status: policy.policy.status,
                username_norm: policy.user.username_norm,
                representing: policy.policy.representing,
                buyer: policy.user.company_details.name,
                buyer_pref_name: policy.user.company_details.rep_name,
                full_name: policy.policy.full_name,
                // preferred_name: policy.user.preferred_name,
                insured_pref_name: policy.policy.preferred_name,
                nric_number: policy.user.nric_number,
                dob: policy.user.dob,
                username: policy.user.username, // email
                gender: policy.user.gender,
                mobile_number: policy.user.mobile_number,
                occupation: policy.user.occupation,
                self_employed:
                    policy.user.self_employed == undefined ? "" : policy.user.self_employed ? "true" : "false",
                place_of_birth: policy.user.place_of_birth,

                referrer: policy.policy.referrer,
                source: policy.policy.source,

                relationship_to_payer: policy.policy.relationship_to_payer
                    ? policy.policy.relationship_to_payer
                    : policy.policy.representing,
                created_at: global_constants.mode_dev
                    ? displayDate(policy.policy.created_at, "yyyy-MM-dd HH:mm:ss")
                    : displayDate(policy.policy.created_at, "yyyy-MM-dd HH:mm:ss", 8 * 60 * 60 * 1000), // This is in GMT+0

                bank: policy.user.bank + " - " + policy.user.bank_acct_number,
                // bank_acct_number: policy.user.bank_acct_number,

                bank_claims_to_payer: policy.policy.bank_claims ? true : true,
                bank_claims: policy.user.bank_claims
                    ? policy.user.bank_claims + " - " + policy.user.bank_acct_number_claims
                    : policy.user.bank + " - " + policy.user.bank_acct_number,
                // bank_acct_number_claims: policy.user.bank_acct_number_claims ? policy.user.bank_acct_number_claims : policy.user.bank_acct_number,

                bank_premiumback_to_payer: policy.policy.bank_premiumback ? true : true,
                bank_premiumback: policy.user.bank_premiumback
                    ? policy.user.bank_premiumback + " - " + policy.user.bank_acct_number_premiumback
                    : policy.user.bank + " - " + policy.user.bank_acct_number,
                // bank_acct_number_premiumback: policy.user.bank_acct_number_premiumback ? policy.user.bank_acct_number_premiumback : policy.user.bank_acct_number,

                uploaded_images: policy.policy.uploaded_images
                    ? policy.policy.uploaded_images
                    : policy.user.uploaded_images,
                product_details: policy.policy.product_details,
                status_history: policy.policy.status_history,
                email: policy.user.username,
                pending_rework_items: policy.policy.pending_rework_items,
                pending_rework_message: policy.policy.pending_rework_message
            };
        } else {
            // For anything other than representing === "self"
            policy_json_and_array.json = {
                id: policy.policy.id,
                product: policy.policy.product,
                status: policy.policy.status,

                username_norm: policy.user.username_norm,
                representing: policy.policy.representing,
                buyer: policy.user.full_name,
                buyer_pref_name: policy.user.preferred_name,
                full_name: policy.policy.full_name,
                // preferred_name: policy.policy.preferred_name,
                insured_pref_name: policy.policy.preferred_name,
                nric_number: policy.policy.nric_number,
                dob: policy.policy.dob,
                email: policy.policy.email ? policy.policy.email : policy.policy.username,
                gender: policy.policy.gender,
                mobile_number: policy.policy.mobile_number,
                occupation: policy.policy.occupation,
                self_employed:
                    policy.policy.self_employed == undefined ? "" : policy.policy.self_employed ? "true" : "false",
                place_of_birth: policy.policy.place_of_birth,

                referrer: policy.policy.referrer,
                source: policy.policy.source,

                relationship_to_payer: policy.policy.relationship_to_payer,
                created_at: global_constants.mode_dev
                    ? displayDate(policy.policy.created_at, "yyyy-MM-dd HH:mm:ss")
                    : displayDate(policy.policy.created_at, "yyyy-MM-dd HH:mm:ss", 8 * 60 * 60 * 1000), // This is in GMT+0

                bank: policy.policy.bank
                    ? policy.policy.bank + " - " + policy.policy.bank_acct_number
                    : policy.user.bank + " - " + policy.user.bank_acct_number,
                // bank_acct_number: policy.policy.bank_acct_number ? policy.policy.bank_acct_number : policy.user.bank_acct_number,

                bank_claims_to_payer: policy.policy.bank_claims ? false : true,
                bank_claims: policy.policy.bank_claims
                    ? policy.policy.bank_claims + " - " + policy.policy.bank_acct_number_claims
                    : policy.user.bank + " - " + policy.user.bank_acct_number,
                // bank_acct_number_claims: policy.policy.bank_acct_number_claims ? policy.policy.bank_acct_number_claims : policy.user.bank_acct_number,

                bank_premiumback_to_payer: policy.policy.bank_premiumback ? false : true,
                bank_premiumback: policy.policy.bank_premiumback
                    ? policy.policy.bank_premiumback + " - " + policy.policy.bank_acct_number_premiumback
                    : policy.user.bank + " - " + policy.user.bank_acct_number,
                // bank_acct_number_premiumback: policy.policy.bank_acct_number_premiumback ? policy.policy.bank_acct_number_premiumback : policy.user.bank_acct_number,

                uploaded_images: policy.policy.uploaded_images,
                product_details: policy.policy.product_details,
                status_history: policy.policy.status_history,
                pending_rework_items: policy.policy.pending_rework_items,
                pending_rework_message: policy.policy.pending_rework_message
            };
        }

        policy_json_and_array.array = Object.values(policy_json_and_array.json);
        return policy_json_and_array;
    },

    array_of_jsons_find_index_where: (array_of_jsons, where_filter) => {
        // Required
        //    array_of_jsons : [{},]
        //    where_filter: [ key, value ]

        for (let i = 0; i < array_of_jsons.length; i++) {
            let json = array_of_jsons[i];
            if (json[where_filter[0]] === where_filter[1]) {
                return i;
            }
        }
        return false;
    },

    filter_rework_items: (array_of_reworkables, representing, client_side) => {
        let returnable = {
            user: [],
            policy: []
        };

        array_of_reworkables.forEach((ele) => {
            switch (ele) {
                case "id":
                    if (client_side) {
                        alert("Cannot rework Policy ID");
                        return false;
                    }
                    break;
                case "product":
                    if (client_side) {
                        alert("Cannot rework Product");
                        return false;
                    }
                    break;
                case "status":
                    if (client_side) {
                        alert("Cannot rework Status");
                        return false;
                    }
                    break;
                case "username":
                    if (client_side) {
                        alert("Cannot rework Username");
                        return false;
                    }
                    break;
                case "representing":
                    if (client_side) {
                        alert("Cannot rework Representing");
                        return false;
                    }
                    break;
                case "full_name":
                    if (representing === "self") {
                        returnable.user.push(ele);
                    } else {
                        returnable.policy.push(ele);
                    }
                    break;
                case "preferred_name":
                    if (representing === "self") {
                        returnable.user.push(ele);
                    } else {
                        returnable.policy.push(ele);
                    }
                    break;
                case "nric_number":
                    if (representing === "self") {
                        returnable.user.push(ele);
                    } else {
                        returnable.policy.push(ele);
                    }
                    break;
                case "dob":
                    break;
                case "email":
                    if (client_side && representing === "self") {
                        alert("Cannot rework email - email is the same as username for self-purchase");
                    } else if (representing !== "self") {
                        returnable.policy.push(ele);
                    }
                    break;
                case "gender":
                    if (representing === "self") {
                        returnable.user.push(ele);
                    } else {
                        returnable.policy.push(ele);
                    }
                    break;
                case "mobile_number":
                    if (representing === "self") {
                        returnable.user.push(ele);
                    } else {
                        returnable.policy.push(ele);
                    }
                    break;
                case "occupation":
                    if (representing === "self") {
                        returnable.user.push(ele);
                    } else {
                        returnable.policy.push(ele);
                    }
                    break;
                case "self_employed":
                    if (representing === "self") {
                        returnable.user.push(ele);
                    } else {
                        returnable.policy.push(ele);
                    }
                    break;
                case "place_of_birth":
                    if (representing === "self") {
                        returnable.user.push(ele);
                    } else {
                        returnable.policy.push(ele);
                    }
                    break;
                case "referrer":
                    if (client_side) {
                        alert("Cannot rework Referrer");
                        return false;
                    }
                    break;
                case "source":
                    if (client_side) {
                        alert("Cannot rework Source");
                        return false;
                    }
                    break;
                case "relationship_to_payer":
                    if (client_side && representing === "self") {
                        alert("Cannot rework Relationship to Payer for self-purchase");
                    } else if (representing !== "self") {
                        returnable.policy.push(ele);
                    }
                    break;
                case "created_at":
                    if (client_side) {
                        alert("Cannot rework Date Started");
                        return false;
                    }
                    break;
                case "bank":
                    returnable.user.push(ele);
                    break;
                // case "bank_acct_number":
                //   returnable.user.push(ele)
                //   break;
                case "bank_claims":
                    returnable.policy.push(ele);
                    break;
                // case "bank_acct_number_claims":
                //   returnable.policy.push(ele)
                //   break;
                case "bank_premiumback":
                    returnable.policy.push(ele);
                    break;
                // case "bank_acct_number_premiumback":
                //   returnable.policy.push(ele)
                //   break;
                case "uploaded_images":
                    if (client_side) {
                        alert("Cannot rework Date Started");
                        return false;
                    }

                    break;

                default:
                    // Should not happen
                    break;
            }
        });

        return returnable;
    },
    currency_dollar_decimal_two: "$ 0,0.00"
};
