// next.config.js
// const withImages = require('next-images')
// const withSass = require('@zeit/next-sass')
// const withPlugins = require("next-compose-plugins");
// const withCSS = require("@zeit/next-css");
// const path = require('path')
// const withCSS = require('@zeit/next-css')
// const withTM = require('next-plugin-transpile-modules');

// module.exports = withImages(withTM(withCSS({
//     cssModules: true,
//     cssLoaderOptions: {
//         importLoaders: 1,
//         localIdentName: "[local]___[hash:base64:5]",
//     }
// })))

// module.exports = withPlugins([
//     [withCSS],
//     [withSass],
//     [withImages]
// ], {
//     webpack: (config, { isServer }) => {
//         config.resolve.alias['components'] = path.join(__dirname, 'components')
//         config.resolve.alias['constants'] = path.join(__dirname, 'constants')
//         config.resolve.alias['global'] = path.join(__dirname, 'global')
//         config.resolve.alias['data'] = path.join(__dirname, 'data')
//         return config
//     }
// });

// module.exports = withCSS({
//     cssModules: true,
//     cssLoaderOptions: {
//         importLoaders: 1,
//         localIdentName: "[local]___[hash:base64:5]",
//     }
// })

const withPlugins = require('next-compose-plugins');
const withImages = require('next-images');
const withSass = require('@zeit/next-sass');
const withCSS = require('@zeit/next-css');
const path = require('path');
const Dotenv = require('dotenv-webpack');

module.exports = withPlugins([[withCSS], [withSass], [withImages]], {
    webpack(config, options) {
        // console.log('webpack -> config', process.env.TEST_VAR);
        console.log('****** next.config.js ******* build : ', process.env.NODE_ENV);
        if (process.env.NODE_ENV === 'production') {
            config.plugins.push(new Dotenv({ path: path.join(__dirname, '.env.production') }));
        } else if (process.env.NODE_ENV === 'development') {
            config.plugins.push(new Dotenv({ path: path.join(__dirname, '.env.development') }));
        } else {
            throw new Error(`Build failed ,didn't set NODE_ENV `);
        }

        const originalEntry = config.entry;
        config.entry = async () => {
            const entries = await originalEntry();

            if (entries['main.js'] && !entries['main.js'].includes('./client/polyfills.js')) {
                entries['main.js'].unshift('./client/polyfills.js');
            }

            return entries;
        };

        config.resolve.alias['components'] = path.join(__dirname, 'components');
        config.resolve.alias['constants'] = path.join(__dirname, 'constants');
        config.resolve.alias['global'] = path.join(__dirname, 'global');
        config.resolve.alias['data'] = path.join(__dirname, 'data');
        config.resolve.alias['lib'] = path.join(__dirname, 'lib');
        config.resolve.alias['@api'] = path.resolve(__dirname, 'universal/api');
        config.resolve.alias['@components'] = path.resolve(__dirname, 'universal/components');
        config.resolve.alias['@constants'] = path.resolve(__dirname, 'universal/constants');
        config.resolve.alias['@models'] = path.resolve(__dirname, 'universal/models');
        config.resolve.alias['@providers'] = path.resolve(__dirname, 'universal/providers');
        config.resolve.alias['@screens'] = path.resolve(__dirname, 'universal/screens');
        config.resolve.alias['@utils'] = path.resolve(__dirname, 'universal/utils');
        return config;
    }
});
