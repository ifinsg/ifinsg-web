import React, { Component } from 'react';
import { NoSsr } from '@material-ui/core';
import Router from 'next/router';
import AdminTopNavBar from './AdminTopNavBar';
import PageCMS from './PageCMS';
import PageApprovals from './PageApprovals';
import PageHistory from './PageHistory';
import PageClaims from './PageClaims';
import PageNuclear from './PageNuclear';
import PagePremiumback from './PagePremiumback';
import Constants from 'constants/constants';

class AdminPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page_loading: true,
            current_page: {
                page: 'approvals',
                config: {
                    // [optional]
                    current_tab: 0 // [optional] tab within that page
                }
            },
            page_props: {
                AdminPage: {
                    control_access_to_only: [
                        // "AFFILIATESMQ@EMAIL.COM",
                        'MARQSIEW@IFINANCIALSINGAPORE.COM',
                        'JIEXIONGKHOO@GMAIL.COM',
                        'W1BBOND@GMAIL.COM',
                        'WANGYIBU123@GMAIL.COM',
                        'YIWIGO2039@LANCASTERCOC.COM',
                        'MILDJX@GMAIL.COM',
                        'JOANNASEE@IFINANCIALSINGAPORE.COM',
                        'LUUDUCHIEUHARRY@GMAIL.COM'
                    ]
                },
                approvals: {
                    control_access_to_only: [
                        // "AFFILIATESMQ@EMAIL.COM",
                        'MARQSIEW@IFINANCIALSINGAPORE.COM',
                        'JIEXIONGKHOO@GMAIL.COM',
                        'W1BBOND@GMAIL.COM',
                        'YIWIGO2039@LANCASTERCOC.COM',
                        'WANGYIBU123@GMAIL.COM',
                        'MILDJX@GMAIL.COM',
                        'JOANNASEE@IFINANCIALSINGAPORE.COM',
                        'LUUDUCHIEUHARRY@GMAIL.COM'
                    ]
                },
                CMS: {
                    control_access_to_only: [
                        // "AFFILIATESMQ@EMAIL.COM",
                        'MARQSIEW@IFINANCIALSINGAPORE.COM',
                        'JIEXIONGKHOO@GMAIL.COM',
                        'W1BBOND@GMAIL.COM',
                        'YIWIGO2039@LANCASTERCOC.COM',
                        'WANGYIBU123@GMAIL.COM',
                        'MILDJX@GMAIL.COM',
                        'JOANNASEE@IFINANCIALSINGAPORE.COM',
                        'LUUDUCHIEUHARRY@GMAIL.COM'
                    ]
                },
                claims: {
                    control_access_to_only: [
                        // "AFFILIATESMQ@EMAIL.COM",
                        'MARQSIEW@IFINANCIALSINGAPORE.COM',
                        'JIEXIONGKHOO@GMAIL.COM',
                        'W1BBOND@GMAIL.COM',
                        'YIWIGO2039@LANCASTERCOC.COM',
                        'WANGYIBU123@GMAIL.COM',
                        'MILDJX@GMAIL.COM',
                        'JOANNASEE@IFINANCIALSINGAPORE.COM',
                        'LUUDUCHIEUHARRY@GMAIL.COM'
                    ]
                },
                history: {
                    control_access_to_only: [
                        // "AFFILIATESMQ@EMAIL.COM",
                        'MARQSIEW@IFINANCIALSINGAPORE.COM',
                        'JIEXIONGKHOO@GMAIL.COM',
                        'W1BBOND@GMAIL.COM',
                        'YIWIGO2039@LANCASTERCOC.COM',
                        'WANGYIBU123@GMAIL.COM',
                        'MILDJX@GMAIL.COM',
                        'JOANNASEE@IFINANCIALSINGAPORE.COM',
                        'LUUDUCHIEUHARRY@GMAIL.COM'
                    ]
                },
                nuclear: {
                    control_access_to_only: [
                        // "AFFILIATESMQ@EMAIL.COM",
                        'MARQSIEW@IFINANCIALSINGAPORE.COM',
                        'JIEXIONGKHOO@GMAIL.COM',
                        'W1BBOND@GMAIL.COM',
                        'YIWIGO2039@LANCASTERCOC.COM',
                        'WANGYIBU123@GMAIL.COM',
                        'MILDJX@GMAIL.COM',
                        'JOANNASEE@IFINANCIALSINGAPORE.COM',
                        'LUUDUCHIEUHARRY@GMAIL.COM'
                    ]
                }
            }
        };
    }

    componentDidMount = () => {
        this.setState({ page_loading: false });
    };

    handleChangeCurrentPage = (new_current_page) => {
        let current_page = {};
        current_page = new_current_page;
        this.setState({
            current_page
        });
    };

    getUserInfo = (user_info) => {
        const list = this.state.page_props['AdminPage'].control_access_to_only;
        if (list.includes(user_info.username.toUpperCase())) {
            // Do nothing
        } else {
            let page = Constants.get_new_page('login');
            Router.push(page);
        }

        this.setState({
            user_info
        });
    };

    renderCurrentPage = () => {
        if (this.state.user_info) {
            if (
                this.state.page_props['AdminPage'].control_access_to_only.indexOf(
                    this.state.user_info.username_norm
                ) !== -1
            ) {
                switch (this.state.current_page.page) {
                    case 'CMS':
                        return <PageCMS user_info={this.state.user_info} page_props={this.state.page_props} />;
                    case 'approvals':
                        return <PageApprovals user_info={this.state.user_info} page_props={this.state.page_props} />;
                    case 'history':
                        return <PageHistory user_info={this.state.user_info} page_props={this.state.page_props} />;
                    case 'claims':
                        return <PageClaims user_info={this.state.user_info} page_props={this.state.page_props} />;
                    case 'nuclear':
                        return <PageNuclear user_info={this.state.user_info} page_props={this.state.page_props} />;
                    case 'premiumback':
                        return <PagePremiumback user_info={this.state.user_info} page_props={this.state.page_props} />;
                    default:
                        return <div> Oops. There's a bug. This should not happen</div>;
                }
            } else {
                let page = Constants.get_new_page('login');
                Router.push(page);
                return <div style={{ paddingTop: '80px' }}> Please login to gain access to this page </div>;
            }
        } else {
            return (
                <NoSsr>
                    <div style={{ paddingTop: '130px', textAlign: 'center' }}>
                        {' '}
                        Please login to gain access to this page{' '}
                    </div>{' '}
                </NoSsr>
            );
        }
    };

    render() {
        const { classes } = this.props;
        return (
            <div>
                {this.state.page_loading ? 'Loading...' : ''}
                <AdminTopNavBar
                    selected={this.state.current_page.page}
                    getUserInfo={this.getUserInfo}
                    page_props={this.state.page_props}
                    handleChangeCurrentPage={this.handleChangeCurrentPage}
                />
                {this.renderCurrentPage()}
            </div>
        );
    }
}

export default AdminPage;
