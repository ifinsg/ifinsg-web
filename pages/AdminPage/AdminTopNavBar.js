import React, { Component } from 'react';

import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';

import { withWidth, NoSsr, Button, Typography, IconButton, AppBar, Toolbar, Menu, MenuItem, Divider, ClickAwayListener } from '@material-ui/core';
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";
import axios from 'axios'

import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MenuIcon from '@material-ui/icons/Menu';
import AvatarIcon from '@material-ui/icons/AccountCircle';

import Helpers from '../../global/helpers'

import CardMedia from '@material-ui/core/CardMedia';
import Hidden from '@material-ui/core/Hidden';
import Link from "next/link";
import Head from 'next/head';
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo

import Router from 'next/router';

import GlobalConstants from '../../global/constants';
import Constants from 'constants/constants';
import ScrollToTop from '../components/widgets/ScrollToTop';
import Cookies from 'universal-cookie';
const cookie = new Cookies();
import './AdminTopNavBar.css'

const styles = {
  button: { color: Constants.color.grey, justifyContent: "left", minWidth: "44px" },
  button_menu_expanded: { color: Constants.color.grey, justifyContent: "left", minWidth: "44px", width: "100%" },
  button_selected: { color: Constants.color.green },


  page_width: Constants.style.page.width,
  page_padding: Constants.style.page.padding,

  full_width: Constants.style.align.full_width,

  align_center: Constants.style.align.center,
  align_left: Constants.style.align.left,

  bold: Constants.style.text.bold,

  title1: Constants.style.text.title1,
  body1: Constants.style.text.body1,

  margin_top_xl: Constants.style.margin_top.xl,
  margin_top_lg: Constants.style.margin_top.lg,
  margin_top_md: Constants.style.margin_top.md,
  margin_top_sm: Constants.style.margin_top.sm,
  margin_top_xs: Constants.style.margin_top.xs,
  margin_top_xxs: Constants.style.margin_top.xxs,



};


class TopNavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null,
      expanded_menu: false,
      render_expand_icon: false,
      user_info: false
    }
  }

  componentWillReceiveProps = (nextProps) => {

    if (this.props.width != nextProps.width) {
      this.changeWhetherMenuButtonIsShown(nextProps.width)
    }

    if (this.props.username !== nextProps.username) {
    }
  }


  changeWhetherMenuButtonIsShown = (width) => {

    let render_expand_icon = false

    switch (width) {
      case 'xs':
        render_expand_icon = true
        break;
      case 'sm':
        // render_expand_icon = true
        break
      case 'md':
        break
      default:
        break;
    }


    if (this.state.render_expand_icon === true && render_expand_icon === false) {    // If we are taking away the menu icon, collapse expandable as well

      this.setState({
        render_expand_icon,
        expanded_menu: false
      })
    } else {
      this.setState({
        render_expand_icon,
      })
    }
  }




  getUserPolicies = () => {
    return new Promise((resolve, reject) => {

      try {
        const token = localStorage.getItem('ifinsg_token');

        if (token) {
          axios.post("/api/auth/getUserDataFromToken", {

            token,
          }).then(res => {
            if (res.status === 200) {
              if (res.data.err) {
                resolve()
              } else {
                try {
                  this.setState({
                    user_info: res.data.user_info,
                  }, () => {
                    if (this.props.getUserInfo) {
                      this.props.getUserInfo(this.state.user_info)
                    }
                    resolve()
                  })

                } catch (err) {
                  resolve()

                }

              }
            } else {
              resolve()

            }
          }).catch(err => {
            resolve()

          });
        } else {
          resolve()
        }



      } catch (err) {
        resolve()
      }


    })

  }




  componentDidMount = () => {
    this.changeWhetherMenuButtonIsShown(this.props.width)

    this.getUserPolicies()

  }


  handleAccountButtonClicked = event => {
    if (this.state.anchorEl !== null) {
      this.setState({ anchorEl: null });
    } else {
      this.setState({ anchorEl: event.currentTarget })
    }

  };


  handleClose = () => {
    this.setState({ anchorEl: null });
  };


  handleMenuItemClicked = (clicked) => {
    // let page = Constants.get_new_page("home", this.props.referrer)

    // switch (clicked) {

    //   case 'logout':
    //     localStorage.removeItem('ifinsg_token')
    //     page = Constants.get_new_page("login", this.props.referrer)
    //     Router.push(page)
    //     break;
    //   case 'login':
    //     page = Constants.get_new_page("login", this.props.referrer)
    //     Router.push(page)
    //     break;
    //   default:
    //     break;
    // }

    // this.handleClose()

  }




  renderTopNavBar = (expanded_menu) => {
    const { classes } = this.props;

    // if (!this.state.user_info) { return false }

    let renderable = []

    let button_style = { justifyContent: "left", minWidth: "44px" }
    if (expanded_menu === 'expanded_menu') {
      button_style = { justifyContent: "left", minWidth: "44px", width: "100%" }
    }


    const get_button_base_style = () => {
      if (expanded_menu === 'expanded_menu') {
        return (
          classes.button_menu_expanded
        )
      } else {
        return (
          classes.button
        )
      }
    }


    const get_style_if_selected = (selected) => {
      if (this.props.selected === selected) {
        return (
          classes.button_selected
        )
      }
    }


    const page_buttons = {
      approvals: <Button onClick={() => { this.setState({ expanded_menu: false }); this.props.handleChangeCurrentPage({ page: "approvals", config: { current_tab: 0 } }) }} className={classNames(get_button_base_style(), get_style_if_selected("approvals"))} > Inactive Policies </Button>,
      CMS: <Button onClick={() => { this.setState({ expanded_menu: false }); this.props.handleChangeCurrentPage({ page: "CMS", config: { current_tab: 0 } }) }} className={classNames(get_button_base_style(), get_style_if_selected("CMS"))} > Active Policies  </Button>,
      claims: <Button onClick={() => { this.setState({ expanded_menu: false }); this.props.handleChangeCurrentPage({ page: "claims", config: { current_tab: 0 } }) }} className={classNames(get_button_base_style(), get_style_if_selected("claims"))} > CLAIMS </Button>,
      history: <Button onClick={() => { this.setState({ expanded_menu: false }); this.props.handleChangeCurrentPage({ page: "history", config: { current_tab: 0 } }) }} className={classNames(get_button_base_style(), get_style_if_selected("history"))} > History </Button>,
      nuclear: <Button onClick={() => { this.setState({ expanded_menu: false }); this.props.handleChangeCurrentPage({ page: "nuclear", config: { current_tab: 0 } }) }} className={classNames(get_button_base_style(), get_style_if_selected("nuclear"))} > Nuclear </Button>,
      premiumback: <Button onClick={() => { this.setState({ expanded_menu: false }); this.props.handleChangeCurrentPage({ page: "premiumback", config: { current_tab: 0 } }) }} className={classNames(get_button_base_style(), get_style_if_selected("premiumback"))} > Premiumback </Button>,
    }



    const account_button = () => {
      let account_button_container_style = { float: 'right' }
      let account_button_style = { fontSize: "10px", color: Constants.color.grey }

      let account_button_color = "darkgrey"
      let logged_in = false



      let render_menu_items = () => {
        let renderable = []
        const menu_item_logout = <MenuItem onClick={() => {
          localStorage.removeItem('ifinsg_token')
          Router.push('/login')
        }}>Logout</MenuItem>

        const menu_item_login = <MenuItem onClick={() => {
          console.log('login')
          Router.push('/login')
        }}>Login</MenuItem>
        if (logged_in) {
          // Align left:
          // const menu_item_logged_in_as = <MenuItem disabled style={{ height:"50px", wordBreak:"break-word",}}>  <div style={{lineHeight:1.2}}><span style={{ fontSize: "12px" }}>logged in as: </span><br /> <span style={{fontSize:"15px"}}>{Helpers.showPreview(this.state.user_info.username.split('@')[0], 20)}</span></div>  </MenuItem>

          // Centralised:
          // const menu_item_logged_in_as = <MenuItem disabled style={{ height: "50px", wordBreak: "break-word", textAlign: "center" }}>  <div style={{ lineHeight: 1.2, textAlign: "center", width: "100%" }}><div style={{ fontSize: "12px" }}>logged in as: </div> <div style={{ fontSize: "15px", textAlign: "center", marginTop: "5px" }}>{Helpers.showPreview(this.state.user_info.username.split('@')[0], 20)}</div></div>  </MenuItem>

          // With green background, white text
          const menu_item_logged_in_as = <MenuItem disabled style={{
            height: "50px", wordBreak: "break-word", textAlign: "center",
            backgroundColor: Constants.color.darkgreen, opacity: 0.8,
            // backgroundColor:"rgb(34,139,34)",
            color: "white"
          }}>  <div style={{ lineHeight: 1.2, textAlign: "center", width: "100%" }}><div style={{ fontSize: "12px" }}>logged in as: </div> <div style={{ fontSize: "15px", textAlign: "center", marginTop: "5px" }}>{Helpers.showPreview(this.props.username ? this.props.username.split('@')[0] : this.state.user_info.username.split('@')[0], 20)}</div></div>  </MenuItem>

          renderable = [menu_item_logged_in_as, menu_item_logout]
        } else {
          renderable = [menu_item_login]
        }
        return renderable
      }

      try {
        if (this.props.username || (this.state.user_info && this.state.user_info.username)) {    // If logged in
          account_button_color = Constants.color.green
          logged_in = true
        } else {
          logged_in = false
        }

        return (
          <div style={account_button_container_style}>
            <IconButton
              onClick={this.handleAccountButtonClicked}>
              <AvatarIcon style={{ color: account_button_color }} />
            </IconButton>


            <div style={{ float: "right" }}>
              <Menu
                id="simple-menu"
                anchorEl={this.state.anchorEl}
                open={Boolean(this.state.anchorEl)}
                onClose={this.handleClose}
                style={{
                  transform: "translate(0px,50px)",
                  float: "right"
                }}
              >
                {render_menu_items()}
              </Menu>
            </div>

          </div>
        )


      } catch (err) {
      }
    }


    let menu_collapsed = false

    switch (this.props.width) {
      case 'xs':
        menu_collapsed = true
        break;
      case 'sm':
        menu_collapsed = false
        break
      case 'md':
        break
      default:
        break;
    }


    let renderable_container = ''

    const control_page_access = (array_of_page_button_names) => {
      let allowed_page_buttons = []
      for (let i = 0; i < array_of_page_button_names.length; i++) {
        let page_button_name = array_of_page_button_names[i]
        switch (page_button_name) {
          default:
            if (this.props.page_props && this.props.page_props[page_button_name] && this.props.page_props[page_button_name].control_access_to_only) {
              let control_access_to_only = this.props.page_props[page_button_name].control_access_to_only
              if (control_access_to_only.indexOf(this.state.user_info.username_norm) != -1) {
                allowed_page_buttons.push(page_buttons[page_button_name])
              } else {
              }
            } else {
              allowed_page_buttons.push(page_buttons[page_button_name])
            }
            break;
        }
      }

      return allowed_page_buttons
    }

    if (menu_collapsed) {   // Mobile view  OR small desktop view
      renderable = [<div style={{ marginRight: '10px' }}> {account_button()}</div>]
      renderable_container =
        <div style={{ paddingTop: '10px' }}>
          {renderable}
        </div>
    } else {    // Large desktop view
      // let renderable_buttons = [CMS, approvals]
      let renderable_buttons = control_page_access(['approvals', 'CMS', 'claims', 'history', 'nuclear', 'premiumback'])
      let renderable_icons = [account_button()]
      renderable_container = <div style={{ margin: '10px 0px 0px 100px' }}>
        <div style={{ float: 'right' }}>{renderable_icons}</div>
        <div style={{ paddingTop: '5px' }}>{renderable_buttons}</div>
      </div>
    }




    if (expanded_menu === 'expanded_menu') {
      // renderable = [home, <Divider />, FAQ, <Divider />, disclaimers, <Divider />, claims, <Divider />, premiumback, <Divider />, resources, <Divider />]
      renderable = control_page_access(['approvals', 'CMS', 'claims', 'history', 'nuclear', 'premiumback'])

      renderable = renderable.map(item => <div style={{ borderBottom: 'solid', borderWidth: '2px', borderBottomColor: '#a9a9a94d' }}> {item} </div>)
      // renderable.pop()
      // renderable.push(resources)   // Removing the bottom border of the last item

      let renderable_full_width = []

      for (let i = 0; i < renderable.length; i++) {
        renderable_full_width.push(<div style={{ margin: "0px 27px 0px 27px" }}> {renderable[i]}</div>)
      }

      renderable_container = <div style={{ width: "100%" }}>  {renderable_full_width} </div>

    }


    return (renderable_container)


  }


  handleVerifyEmail = () => {
    this.setState({
      verify_email_button_disabled: true
    }, () => {
      axios.post("/api/auth/verifyEmail", {
        signup_username: this.state.login_email,
        email_verification_code: this.state.email_verification_code
      }).then(res => {
        if (res.status === 200) {
          if (res.data.err) {
            alert(res.data.err)     // Eg: ERROR - Email already in use
            this.setState({
              verify_email_button_disabled: false
            })
          } else {
            if (res.data.verification_result) {
              if (res.data.token) {
                cookie.set('ifinsg_token', res.data.token, { path: '/' })
                localStorage.setItem('ifinsg_token', res.data.token)
              }
              this.setState({
                expanded_login: false,
                expansian_panel_login_disabled: true
              })
              this.nextPanelAfterSignup()
            } else {
            }
          }
        } else {
        }
      }).catch(err => {
      });
    })

  }


  renderExpandIcon = () => {
    if (this.state.render_expand_icon) {
      return (
        <MenuIcon style={{ fontSize: "30px", color: "dimgrey", margin: "0px 10px 0px 10px" }}
          onClick={() => { this.state.expanded_menu ? console.log("manu is already expanded") : console.log("expanding menu"); this.setState({ expanded_menu: true }) }}
        />
      )
    }
  }


  getMuiTheme = () => {
    return createMuiTheme({
      overrides: {
        MuiPaper: {
          elevation1: {
            boxShadow: 0
          }
        },
        MuiExpansionPanelSummary: {
          content: {
            margin: '5px 0px 10px 0px!important'
          },
          expandIcon: {
            marginTop: '3px!important',
            transition: 'transform 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms'
          }
        },

      }
    });
  }


  render() {
    const { classes } = this.props;

    return (
      <div className="admin-top-nav" style={{ position: "fixed", width: "100%", pointerEvents: "none", zIndex: 99 }}>
        {/* <Head>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous" />
          <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"></link>
        </Head> */}
        <MuiThemeProvider theme={this.getMuiTheme()}>
          <div className={classNames(classes.align_center, classes.full_width)}>
            <div className={classNames(classes.align_left, classes.full_width)}>
              <div style={{ width: "100%", borderBottom: 'solid', borderWidth: '2px', borderBottomColor: '#a9a9a94d', pointerEvents: "auto" }}>
                <ClickAwayListener onClickAway={() => { this.state.expanded_menu ? this.setState({ expanded_menu: false }) : console.log() }}>
                  <ExpansionPanel expanded={this.state.expanded_menu} style={{ cursor: "auto", margin: "0px 0px 0px 0px" }}>
                    <ExpansionPanelSummary
                      style={{ cursor: "auto", margin: "0px 0px 0px 0px" }}
                      onClick={() => {
                        this.state.expanded_menu ? this.setState({ expanded_menu: false }) : console.log()    // Close expandable menu if needed
                        this.state.anchorEl !== null ? this.setState({ anchorEl: null }) : console.log()      // Close account_button menu if needed
                      }}
                      expandIcon={this.renderExpandIcon()}>
                      <div style={{ width: "100%" }}>
                        <div>
                          <a href={Constants.get_new_page("home", this.props.referrer)}> <img style={{ float: "left", maxWidth: '60px', width: '100%', margin: '6px 0px 0px 15px', padding: '0px 0px 0px 0px' }} src={('/img/ifinsg_logo.png')} />  </a>
                        </div>
                        {this.renderTopNavBar()}
                      </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails style={{ margin: "0px 0px 0px 0px" }}>
                      {this.renderTopNavBar('expanded_menu')}
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                </ClickAwayListener>
              </div>
            </div>
          </div>
        </MuiThemeProvider>
      </div >
    );
  }
};

export default withStyles(styles)(withWidth()(TopNavBar));
