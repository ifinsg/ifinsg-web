import React, { Component } from 'react';

import { NoSsr, Button, Typography, IconButton, AppBar, Toolbar } from '@material-ui/core';

// import Logo from './ifinsg_logo.png'
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import GlobalHelpers from '../../global/helpers';
import Constants from 'constants/constants';
import Table from './tables/TableAdmin'


const styles = {
  page_width: Constants.style.page.width,
  page_text_width: Constants.style.page.text_width,

  page_padding: Constants.style.page.padding,

  align_center: Constants.style.align.center,
  align_left: Constants.style.align.left,

  bold: Constants.style.text.bold,
  break_word: Constants.style.text.break_word,

  title1: Constants.style.text.title1,
  body1: Constants.style.text.body1,

  margin_top_xl: Constants.style.margin_top.xl,
  margin_top_lg: Constants.style.margin_top.lg,
  margin_top_md: Constants.style.margin_top.md,
  margin_top_sm: Constants.style.margin_top.sm,
  margin_top_xs: Constants.style.margin_top.xs,
  margin_top_xxs: Constants.style.margin_top.xxs,
}


class PageCMS extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date_start: this.getDateStart(0),
      offset: 0,
    };
  }


  getDateStart = (offset) => {    // offset in days from the start of today
    let date_object = new Date()
    if (this.state && offset) {
      date_object = new Date(new Date().getTime() + offset * 24 * 60 * 60 * 1000)
    }
    return GlobalHelpers.dateStringFormattedyyyyMMdd(date_object)
  }



  componentDidMount = () => { }

  renderCMS = () => {

    if (this.props.user_info && this.props.page_props && this.props.page_props["CMS"].control_access_to_only.indexOf(this.props.user_info.username_norm) !== -1) {
      return (
        <div style={{ textAlign: "center", width: "100%" }}>

          <div style={{ width: "100%", display: "inline-block", textAlign: "left" }}>
            <Table table="cms"  />
          </div>

        </div>

      )
    } else {
      return (
        <div style={{ textAlign: "center", marginTop: "45px" }}>
          <Typography> You do not have rights access to this page </Typography>
        </div>
      )
    }
  }


  render() {
    const { classes } = this.props;

    return (
      <div>

        <NoSsr>

          <BrowserView>
            <div className={classNames(classes.align_center)}>
              <div className={classNames(classes.page_padding, classes.break_word, classes.align_left)} style={{ display: "inline-block", width:"-webkit-fill-available" }}>
                <div style={{ marginTop: "125px" }}> </div>
              {this.renderCMS()}
            </div>
            </div>
          </BrowserView>


        <MobileView>
          <div style={{ paddingTop: "80px" }}> </div>
          {this.renderCMS()}
        </MobileView>

        </NoSsr>

      </div >
    );
  }
}


export default withStyles(styles)(PageCMS);