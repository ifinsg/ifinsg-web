import React, { Component } from 'react';

import { NoSsr, Button, Typography, AppBar, Tabs, Tab, Paper, TextField, FormControl, InputLabel, MenuItem, Select, Card, FormControlLabel, Checkbox, Fab, Divider, IconButton, Grid, FormLabel, RadioGroup, Radio, Switch } from '@material-ui/core';

// import Logo from './ifinsg_logo.png'
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import GlobalHelpers from '../../global/helpers';
import Constants from 'constants/constants';
import Table from './tables/TableAdmin'
import ModalIndividualClaim from './modals/ModalIndividualClaim'


const styles = {
  page_width: Constants.style.page.width,
  page_text_width: Constants.style.page.text_width,

  page_padding: Constants.style.page.padding,

  align_center: Constants.style.align.center,
  align_left: Constants.style.align.left,

  bold: Constants.style.text.bold,
  break_word: Constants.style.text.break_word,

  title1: Constants.style.text.title1,
  body1: Constants.style.text.body1,

  margin_top_xl: Constants.style.margin_top.xl,
  margin_top_lg: Constants.style.margin_top.lg,
  margin_top_md: Constants.style.margin_top.md,
  margin_top_sm: Constants.style.margin_top.sm,
  margin_top_xs: Constants.style.margin_top.xs,
  margin_top_xxs: Constants.style.margin_top.xxs,
}


function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}


const get_mode_from_tab = {
  0: "pending_approval",
  1: "rework",
  2: "cancelled",
  3: "rejected",
  4: "approved"

}

class Affiliates extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tab: 0,
      modal_open: false,
      // modal_data: [],
      modal_data_json: {},
      columns: ["City222", "Company", "City", "State"],
      data: [
        ["Joe James", "Test Corp", "Yonkers", "NY"],
      ],
    };
  }

  handleTabChange = (event, value) => {
    this.setState({ tab: value });
  };

  componentDidMount = () => { }


  handleTableCellClicked = (table, cell_data, data_json, data_json_split) => {
    if (cell_data) {
      this.setState({
        modal_open: true,
        // modal_data: cell_data,
        modal_data_json: data_json,
        modal_data_json_split: data_json_split
      })
    }
  }


  handleCloseModal = () => {
    this.setState({ modal_open: false })
  }


  renderTab = () => {
    switch (this.state.tab) {
      case 0:
        return (
          <div>
            {/* <Typography variant="h6" style={{ float: "left", paddingTop: "20px" }}> Pending Approval Signups </Typography> */}
            <Table table="claims" table_variant="pending_approval" handleTableCellClicked={this.handleTableCellClicked} />
          </div>
        )
      case 1:
        return (
          <div>
            <Table table="claims" table_variant="rework" handleTableCellClicked={this.handleTableCellClicked} />
          </div>
        )
      case 2:
        return (
          <div>
            <Table table="claims" table_variant="cancelled" handleTableCellClicked={this.handleTableCellClicked} />
          </div>
        )
      case 3:
        return (
          <div>
            <Table table="claims" table_variant="rejected" handleTableCellClicked={this.handleTableCellClicked} />
          </div>
        )
      case 4:
        return (
          <div>
            <Table table="claims" table_variant="approved" handleTableCellClicked={this.handleTableCellClicked} />
          </div>
        )
      default:
        // Should not happen
        break
    }

    this.setState({ hi: 1 })

  }


  renderPageApprovals = () => {
    // Controlling access
    // if (!(this.props.user_info && this.props.page_props && this.props.page_props["CMS"].control_access_to_only.indexOf(this.props.user_info.username_norm) !== -1)) {
    // return (
    //     //     <div style={{ textAlign: "center", marginTop: "45px" }}>
    //     //       <Typography> You do not have rights access to this page </Typography>
    //     //     </div>
    //     //   )
    // } else {
    //  

    return (
      <div>


        <AppBar position="static" color="default" style={{ backgroundColor: "white", }}>
          <Tabs
            value={this.state.tab}
            onChange={this.handleTabChange}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
            style={{ backgroundColor: "white" }}
          >
            <Tab label="Pending Approval" style={{ width: "20%" }} />
            <Tab label="Rework" style={{ width: "20%" }} />
            <Tab label="Cancelled" style={{ width: "20%" }} />
            <Tab label="Rejected" style={{ width: "20%" }} />
            <Tab label="Approved" style={{ width: "20%" }} />

          </Tabs>
        </AppBar>

        <TabContainer >
          {/* tab {this.state.tab} */}
          {this.renderTab()}
        </TabContainer>


      </div>


    )


  }





  render() {
    const { classes } = this.props;

    return (
      <div>

        <NoSsr>
          <ModalIndividualClaim
            open={this.state.modal_open}
            mode={get_mode_from_tab[this.state.tab]}
            //  data={this.state.modal_data}
            data_json={this.state.modal_data_json}
            data_json_split={this.state.modal_data_json_split}
            handleClose={this.handleCloseModal} />

          <BrowserView>
            <div className={classNames(classes.align_center)}>
              <div className={classNames(classes.page_padding, classes.break_word, classes.align_left)} style={{ display: "inline-block", width: "-webkit-fill-available" }}>
                <div style={{ marginTop: "125px" }}> </div>
                {this.renderPageApprovals()}
              </div>
            </div>
          </BrowserView>


          <MobileView>
            <div style={{ paddingTop: "80px" }}> </div>
            {this.renderPageApprovals()}
          </MobileView>

        </NoSsr>

      </div>
    );
  }
}


export default withStyles(styles)(Affiliates);