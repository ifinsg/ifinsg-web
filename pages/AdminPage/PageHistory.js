import React, { Component } from 'react';

import { NoSsr, Button, Typography, AppBar, Tabs, Tab, Paper, TextField, FormControl, InputLabel, MenuItem, Select, Card, FormControlLabel, Checkbox, Fab, Divider, IconButton, Grid, FormLabel, RadioGroup, Radio, Switch } from '@material-ui/core';

// import Logo from './ifinsg_logo.png'
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import GlobalHelpers from '../../global/helpers';
import Constants from 'constants/constants';
import Table from './tables/TableAdmin'


const styles = {
  page_width: Constants.style.page.width,
  page_text_width: Constants.style.page.text_width,

  page_padding: Constants.style.page.padding,

  align_center: Constants.style.align.center,
  align_left: Constants.style.align.left,

  bold: Constants.style.text.bold,
  break_word: Constants.style.text.break_word,

  title1: Constants.style.text.title1,
  body1: Constants.style.text.body1,

  margin_top_xl: Constants.style.margin_top.xl,
  margin_top_lg: Constants.style.margin_top.lg,
  margin_top_md: Constants.style.margin_top.md,
  margin_top_sm: Constants.style.margin_top.sm,
  margin_top_xs: Constants.style.margin_top.xs,
  margin_top_xxs: Constants.style.margin_top.xxs,
}


function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}


const get_mode_from_tab = {
  0: "pending_approval",
  1: "rework",
  2: "cancelled",
  3: "rejected"
}

class PageHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tab: 0,
      modal_open: false,
      modal_data: [],
      modal_data_json: {},
      columns: ["City222", "Company", "City", "State"],
      data: [
        ["Joe James", "Test Corp", "Yonkers", "NY"],
        ["John Walsh", "Test Corp", "Hartford", "CT"],
        ["Bob Herm", "Test Corp", "Tampa", "FL"],
        ["James Houston", "Test Corp", "Dallas", "TX"],
      ],
    };
  }

  handleTabChange = (event, value) => {
    this.setState({ tab: value });
  };

  componentDidMount = () => { }


  handleTableCellClicked = (table, cell_data, data_json) => {
  }


  handleCloseModal = () => {
    this.setState({ modal_open: false })
  }


  renderPageHistory = () => {

    return (
      <div>
        {/* Page History */}
        <Table table="history" table_variant="all" handleTableCellClicked={this.handleTableCellClicked} />
      </div>
    )
  }





  render() {
    const { classes } = this.props;

    return (
      <div>

        <NoSsr>
          {/* <ModalIndividualPolicy open={this.state.modal_open} mode={get_mode_from_tab[this.state.tab]} data={this.state.modal_data} data_json={this.state.modal_data_json} handleClose={this.handleCloseModal} /> */}

          <BrowserView>
            <div className={classNames(classes.align_center)}>
              <div className={classNames(classes.page_padding, classes.break_word, classes.align_left)} style={{ display: "inline-block", width: "-webkit-fill-available" }}>
                <div style={{ marginTop: "125px" }}> </div>
                {this.renderPageHistory()}
              </div>
            </div>
          </BrowserView>


          <MobileView>
            <div style={{ paddingTop: "80px" }}> </div>
            {this.renderPageHistory()}
          </MobileView>

        </NoSsr>

      </div>
    );
  }
}


export default withStyles(styles)(PageHistory);