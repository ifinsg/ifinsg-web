import React, { Component } from 'react';
import { NoSsr, Button, Typography, AppBar, Tabs, Tab, Paper, TextField, FormControl, InputLabel, MenuItem, Select, Card, FormControlLabel, Checkbox, Fab, Divider, IconButton, Grid, FormLabel, RadioGroup, Radio, Switch } from '@material-ui/core';
// import Logo from './ifinsg_logo.png'
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import GlobalHelpers from '../../global/helpers';
import Constants from 'constants/constants';
import Table from './tables/TableAdmin'
import ModalIndividualClaim from './modals/ModalIndividualClaim'
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import MUIDataTable from "mui-datatables"; // Source: https://github.com/gregnb/mui-datatables

const styles = {
  page_width: Constants.style.page.width,
  page_text_width: Constants.style.page.text_width,
  page_padding: Constants.style.page.padding,
  align_center: Constants.style.align.center,
  align_left: Constants.style.align.left,
  bold: Constants.style.text.bold,
  break_word: Constants.style.text.break_word,
  title1: Constants.style.text.title1,
  body1: Constants.style.text.body1,
  margin_top_xl: Constants.style.margin_top.xl,
  margin_top_lg: Constants.style.margin_top.lg,
  margin_top_md: Constants.style.margin_top.md,
  margin_top_sm: Constants.style.margin_top.sm,
  margin_top_xs: Constants.style.margin_top.xs,
  margin_top_xxs: Constants.style.margin_top.xxs,
}
let alreadyRenderedSummaryTable = false
class Affiliates extends Component {
  constructor(props) {
    super(props);
    this.state = {
      summary_table_options: {
        responsive: "scroll", // stacked or scroll
        selectableRows: false, // show checkboxes
        rowsPerPageOptions: [10, 20, 50],
        // rowsPerPage: 20, // Should be one of above
        filterType: "dropdown", // 'checkbox', 'dropdown', 'multiselect', 'textField'
        sort: false,
        print: false,
        download: false,
        search: false,
        caseSensitive: false, //caseSensitivity does not work well - cannot find all letters in CAPs :(
        viewColumns: false,
        filter: false,
        pagination: false,
        onCellClick: (colData, cellMeta) => {
          let dataIndex = cellMeta.dataIndex
          if (this.props.handleTableCellClicked) {
            let table = this.props.table
            let cell_data = this.state.data[dataIndex]
            let data_json = this.state.data_json_array[dataIndex]
            let data_json_split = this.state.data_json_array_split[dataIndex]
            this.props.handleTableCellClicked(table, cell_data, data_json, data_json_split)
          }
        },
      }
    };
  }

  componentWillUnmount = () => {
    alreadyRenderedSummaryTable = false
  }

  passDataToParent = (dataFromChild) => {
    this.setState({
      summary_data: dataFromChild[0].data,
      summary_columns: dataFromChild[0].columns,
    })

  }

  getMuiTheme = () => {
    return createMuiTheme({
      overrides: {
        MuiPaper: {
          elevation4: {
            boxShadow: "0px 1px 1px 0px rgba(0, 0, 0, 0.1), 0px 0px 0px 0px rgba(0, 0, 0, 0.1), 0px 0px 1px 0px rgba(0, 0, 0, 0.1)" //Reduce the perceived elevation
          },
        },

        MuiTableRow: {
          root: {
            height: "30px"
          }
        },
        MuiTableHead: {
          root: {
            display: "none"
          }
        },

        MUIDataTableHeadRow: {
          //Change table header CSS
          root: {
            "&>th": {
              // width: '10rem',
              padding: 0,
              visibility: "hidden"
            },
            // "&>th:nth-child(2)": { //first column
            // paddingLeft: "25px" //padding before first column for non-checkboxed tables
            // },
            // "&>th:nth-child(1)": { //first column
            // backgroundColor: "yellow",
            // display: "none"
            // },
          }
        },
        MUIDataTableBodyCell: {
          //CSS of all cells
          root: {
            padding: "1px 5px 1px 5px"
          }
        },
      }
    });
  }

  shouldComponentUpdate = (nextProps, nextState) => {
    if (nextState.summary_data != this.state.summary_data || nextState.summary_columns != this.state.summary_columns || nextState.options != this.state.summary_table_options) {
      if (alreadyRenderedSummaryTable) {
        return false
      } else {
        alreadyRenderedSummaryTable = true
        return true
      }
    }
  }
  renderPagePremiumback = () => {


    return (
      <div style={{ padding: "0px 0px 70px 0px" }}>
        <div style={{ maxWidth: "500px" }}>
          <MuiThemeProvider theme={this.getMuiTheme()}>
            <div style={{ wordBreak: 'normal' }} >
              <MUIDataTable
                title="Accounts Summary"
                data={this.state.summary_data}
                columns={this.state.summary_columns}
                options={this.state.summary_table_options}
              />
            </div>
          </MuiThemeProvider>
        </div>
        <div style={{ marginTop: "30px" }}>
          <Table table="premiumback" passDataToParent={this.passDataToParent} download={true} />
        </div>
      </div>)
  }




  render() {
    const { classes } = this.props;
    return (
      <div>
        <NoSsr>
          <BrowserView>
            <div className={classNames(classes.align_center)}>
              <div className={classNames(classes.page_padding, classes.break_word, classes.align_left)} style={{ display: "inline-block", width: "-webkit-fill-available" }}>
                <div style={{ marginTop: "125px" }}> </div>
                {this.renderPagePremiumback()}
              </div>
            </div>
          </BrowserView>

          <MobileView>
            <div style={{ paddingTop: "80px" }}> </div>
            {this.renderPagePremiumback()}
          </MobileView>
        </NoSsr>
      </div>
    );
  }
}

export default withStyles(styles)(Affiliates);
