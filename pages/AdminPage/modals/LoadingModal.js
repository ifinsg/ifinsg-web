import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary } from '@material-ui/core';
import { NoSsr, Button, Typography, Checkbox, Grid, Paper, FormControlLabel, Modal, IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';


function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  // const top = 50 + rand();
  // const left = 50 + rand();
  // const top = 50
  // const left = 50
  const top = 50
  const left = 50
  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
    textAlign: "center"
  };
}

const styles = theme => {
  let width = '200px'//'85%'

  // if (isBrowser) {
  //   width = '100px'//'60%'
  // }

  return ({
    paper: {
      position: 'absolute',
      width: width,// theme.spacing.unit * 50,
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing.unit * 4,
      outline: 'none',
      // overflow: 'scroll',
      maxHeight: '90%',

    },
  })
}

class SimpleModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: this.props.open,
    };
  }


  componentWillReceiveProps = (nextProps) => {
    if (this.props.open === false && nextProps.open === true) {
      this.handleOpen()
    } else if (this.props.open === true && nextProps.open === false) {
      this.handleClose()
    }
  }


  handleOpen = () => {
    this.setState({ open: true });
    // this.setBackProtection('on')
  };

  handleClose = (backPressed) => {
    // if (backPressed !== "backPressed"){ Router.back() }   // If back button not pressed, Router.back() called to delete the '#' 
    // this.setBackProtection('off')
    // this.props.handleClose()
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;

    return (
      <div>

        <Modal
          // aria-labelledby="simple-modal-title"
          // aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
          disableBackdropClick
        >
          <div style={getModalStyle()} className={classes.paper}>
            <Typography style={{ fontSize: "25px" }}> Loading... </Typography>
            <Typography style={{ marginTop: "7px", fontSize: "13px" }}> Please do not refresh or navigate away from this page. </Typography>
          </div>
        </Modal>
      </div>
    );
  }
}



const SimpleModalWrapped = withStyles(styles)(SimpleModal);

export default SimpleModalWrapped;