import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary } from '@material-ui/core';
import { NoSsr, Button, Typography, Checkbox, Grid, Paper, FormControlLabel, Modal, TextField } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";
// import { NoSsr, Button, Typography, AppBar, Tabs, Tab, , FormControl, InputLabel, MenuItem, Select, Card, FormControlLabel, Checkbox, Fab, Divider, IconButton, Grid, FormLabel, RadioGroup, Radio, Chip } from '@material-ui/core';
// import { AndroidBackHandler } from 'react-navigation-backhandler';

import axios from 'axios'


var owasp = require('owasp-password-strength-test');

owasp.config({
  allowPassphrases: false,
  maxLength: 50,
  minLength: 6,
  minPhraseLength: 20,
  minOptionalTestsToPass: 4,
});


function getModalStyle() {
  // const top = 50 + rand();
  // const left = 50 + rand();
  const top = 50
  const left = 50
  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => {
  let width = '85%'

  if (isBrowser) {
    width = '60%'
  }

  return ({
    paper: {
      position: 'absolute',
      width: width,// theme.spacing.unit * 50,
      maxWidth: "600px",
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing.unit * 4,
      outline: 'none',
      maxHeight: '90%',

    },
  })
}

class SimpleModal extends React.Component {
  state = {
    open: this.props.open,
    password_old: "",
    password_new: "",
    password_new_retype: "",
    submit_button_disabled: false,
    message: "",
    number_field: null,
  };


  componentWillReceiveProps = (nextProps) => {
    if (this.props.open === false && nextProps.open === true) {
      this.handleOpen()
    } else if (this.props.open === true && nextProps.open === false) {
      this.handleClose()
    }
    // if (this.props.modal_confirm_action_data !== nextProps.modal_confirm_action_data) {
    //   this.setState({
    //     modal_confirm_action_data: false
    //   })
    // }
  }

  componentDidMount = () => {
  }

  handleBackPressed = () => {
    history.pushState(null, null, null);
    this.handleClose()
  }

  handleOpen = () => {
    this.setState({ 
      open: true ,
      submit_button_disabled: false
    
    });

    // Adding back listener
    history.pushState(null, null, null);
    window.addEventListener("popstate", this.handleBackPressed)
  };

  handleClose = (close_parent) => {
    this.props.handleClose(close_parent)
    this.setState({ open: false });

    // Removing back listener
    // history.go(-1)
    window.removeEventListener('popstate', this.handleBackPressed)

  };

  handleSubmitAction = () => {

    let user_or_policy = this.props.modal_confirm_action_data.user_or_policy
    let pending_approval_action = this.props.modal_confirm_action_data.pending_approval_action
    let body = this.props.modal_confirm_action_data.body

    if (pending_approval_action === "rework" || pending_approval_action === "reject" || pending_approval_action === "approve_cancel") {
      body.pending_rework_message = this.state.message
    }
    if (pending_approval_action === "refund" || pending_approval_action === "charge" ) {
      body.amount = parseFloat(this.state.number_field)
      body.pending_rework_message = this.state.message
    }
    

    if (pending_approval_action === "cancellation_requested") {
      body.message = this.state.message
      axios.post("/api/cancelOrReinstate", body).then(res => {
        if (res.status === 200) {
          if (res.data.err) {
            alert(res.data.err)     // Eg: ERROR - Email already in use
          } else {
            try {
              alert("SUCCESSFUL")
              this.handleClose()

            } catch (err) {
              alert(err)
            }
          }
        } else {
          alert("ERROR - ", res)
        }
      }).catch(err => {
        alert(err)
      });
    } else {

      try {
        this.setState({submit_button_disabled:true})
        axios.post("/api/admin/pendingApprovalAction", body).then(res => {
          if (res.status === 200) {
            if (res.data.err) {
              alert(res.data.err)     // Eg: ERROR - Email already in use
            } else {
              try {
                // SUCCESSFUL
                alert("SUCCESSFUL")
                this.props.onSuccess(user_or_policy, pending_approval_action, body)
                this.handleClose(true)

              } catch (err) {
                alert(err)
              }
            }
          } else {
            alert("ERROR - ", res)
          }
        }).catch(err => {
          alert(err)
        });

      } catch (err) {
        alert(err)
      }
    }
  }


  handleFieldChange = (prop) => e => {
    this.setState({
      [prop]: e.target.value
    }, () => {
    });


  }


  handleChangePassword = () => {
    const password_old = this.state.password_old
    const password_new = this.state.password_new
    const password_new_retype = this.state.password_new_retype
    const password_strength = owasp.test(password_new);
    if (password_strength.requiredTestErrors.length > 0) {
      alert(password_strength.requiredTestErrors[0])
      return
    } else if (password_strength.optionalTestErrors.length > 2) {
      alert(password_strength.optionalTestErrors[0])
      return
    }

    if (password_new !== password_new_retype) {
      alert("Error - Passwords do not match")
      return
    }

    this.setState({
      submit_button_disabled: true
    }, () => {
      this.props.handleChangePassword(password_old, password_new)
    })
  }


  renderContent = () => {
    let modal_confirm_action_data = this.props.modal_confirm_action_data
    let user_or_policy = modal_confirm_action_data.user_or_policy
    let pending_approval_action = modal_confirm_action_data.pending_approval_action
    let body = modal_confirm_action_data.body

    let modal_title = modal_confirm_action_data.modal_title
    let text_field_label = modal_confirm_action_data.text_field_label
    let number_field_label = modal_confirm_action_data.number_field_label

    let renderable = []

    const title =
      <Typography variant="h6" style={{ textAlign: "center" }}> {modal_title}</Typography>

    const message_field =
      <div style={{ padding: "20px 0px 0px 0px" }}>
        <TextField
          label={text_field_label}
          value={this.state.message}
          onChange={this.handleFieldChange('message')}
          style={{ width: "90%" }}
        />
      </div>

    const number_field =
      <div style={{ padding: "10px 0px 0px 0px" }}>
        <TextField
          label={number_field_label}
          value={this.state.number_field}
          onChange={this.handleFieldChange('number_field')}
          style={{ width: "90%" }}
          type="Number"
        />
      </div>

    const password_old =
      <div style={{ padding: "10px 0px 2px 0px" }}>
        <TextField
          label="Old password"
          value={this.state.password_old}
          onChange={this.handleFieldChange('password_old')}
          type="password"
          style={{ width: "90%" }}
        />
      </div>

    const password_new =
      <div style={{ padding: "0px 0px 2px 0px" }}>
        <TextField
          label="New password"
          onKeyPress={(ev) => { if (ev.key === 'Enter') { this.handleChangePassword() } }}
          value={this.state.password_new}
          onChange={this.handleFieldChange('password_new')}
          type="password"
          style={{ width: "90%" }}
        />
      </div>

    const password_new_retype =
      <div style={{ padding: "0px 0px 20px 0px" }}>
        <TextField
          label="Re-type your new password"
          onKeyPress={(ev) => { if (ev.key === 'Enter') { this.handleChangePassword() } }}
          value={this.state.password_new_retype}
          onChange={this.handleFieldChange('password_new_retype')}
          type="password"
          style={{ width: "90%" }}
        // fullWidth
        />
      </div>


    const submit_button =
      <Button disabled={this.state.submit_button_disabled} style={{ float: "right", marginTop: "20px" }} onClick={() => { this.handleSubmitAction(user_or_policy, pending_approval_action, body) }}> Submit </Button>

    const cancel_button =
      <Button style={{ float: "right", float: "right", marginTop: "20px" }} onClick={() => { this.props.handleClose() }}> Cancel </Button>


    renderable = [title, message_field, submit_button, cancel_button]
    if (pending_approval_action === "refund" || pending_approval_action === "charge") {
      renderable = [title, message_field, number_field, submit_button, cancel_button]
    } else if (pending_approval_action === "approve") {
      renderable = [title, submit_button, cancel_button]
    }

    return (<div style={{ padding: "10px", textAlign: "center" }}>
      {renderable}
    </div>)
  }

  render() {
    const { classes } = this.props;

    return (
      <div>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>
            {this.renderContent()}
          </div>
        </Modal>

      </div>
    );
  }
}

SimpleModal.propTypes = {
  classes: PropTypes.object.isRequired,
};

// We need an intermediary variable for handling the recursive nesting.
const SimpleModalWrapped = withStyles(styles)(SimpleModal);

export default SimpleModalWrapped;