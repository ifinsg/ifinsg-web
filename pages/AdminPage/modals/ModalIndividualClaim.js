import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary } from '@material-ui/core';
import { NoSsr, Button, Typography, Checkbox, Grid, Paper, FormControlLabel, Modal, IconButton, TextField } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";
import Router from 'next/router';

import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import MUIDataTable from "mui-datatables";      // Source: https://github.com/gregnb/mui-datatables
import GlobalHelpers from '../../../global/helpers';
import ModalConfirmActionClaim from './ModalConfirmActionClaim'
import axios from 'axios'

let data_index_map_to_field_name_accident = []
let data_index_map_to_field_name_claim = []

function getModalStyle() {
  // const top = 50 + rand();
  // const left = 50 + rand();
  const top = 50
  const left = 50
  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => {
  let width = '90%'

  if (isBrowser) {
    width = '90%'
  }

  return ({
    paper: {
      position: 'absolute',
      width: width,// theme.spacing.unit * 50,
      // maxWidth: "900px",
      backgroundColor: theme.palette.background.paper,
      padding: theme.spacing.unit * 4,
      outline: 'none',
      overflow: 'scroll',
      maxHeight: '90%',
    },
  })
}


class SimpleModal extends React.Component {
  state = {
    open: this.props.open,
    columns: ["Field", "Value"],

    signed_urls_accident: [],
    signed_urls_claim: [],
    // claim_columns: [
    //   { name: "name", options: { filter: false, sort: true, } },
    //   { label: "Company", options: { filter: true, sort: false, } },
    //   { label: "City_label", options: { filter: true, sort: true, } },
    //   {
    //     name: "Age",
    //     options: {
    //       filter: false,
    //       customBodyRender: (value, tableMeta, updateValue) => (
    //         <FormControlLabel
    //           control={<TextField value={value || ''} type='number' />}
    //           onChange={event => updateValue(event.target.value)}
    //         />
    //       )
    //     }
    //   },
    //   { label: "State", options: { filter: true, sort: true, } },
    // ],
    data_accident: [],
    data_claim: [],

    pending_approval_mode_rework_accident: false,
    pending_approval_mode_rework_claim: false,

    fields_selected: [],

    modal_confirm_action_open: false,
    modal_confirm_action: {}
  }

  populate_table = (accident_or_claim, data) => {
    try {
      if (accident_or_claim === "accident") {
        let data_accident = [
          ["id", data.id],
          ["Product:", data.product],
          ["Verified:", data.accident_details_verified ? "true" : "false"],
          ["Date Submited:", GlobalHelpers.displayDate(data.created_at)],
          ["Accident Date:", data.accident_date ? GlobalHelpers.displayDate(data.accident_date) : ""],
          ["Accident Description:", data.accident_description],
        ]

        data_index_map_to_field_name_accident = [
          "id",
          "product",
          "accident_details_verified",
          "created_at",
          "accident_date",
          "accident_description"
        ]

        this.setState({
          data_accident,
          accident_details_verified: data.accident_details_verified
        })


      } else {
        let data_claim = [
          // ["claim", "123"]
          ["id", data.id],
          ["Status", data.status],
          // ["status_history", JSON.stringify(data.status_history)],
          ["Accident ID", data.ref_accident_id],
          ["Date Submited", GlobalHelpers.displayDate(data.created_at, "dd-MM-yyyy")],
          ["Clinic Type", data.medical_registrar_smc ? "Western" : "TCM"],
          ["Clinic Visit Date", GlobalHelpers.displayDate(data.clinic_visit_date, "dd-MM-yyyy")],
          ["Doctor Seen", data.doctor_seen],
          ["Clinic Visited", data.clinic_visited],
          ["Treatment Description", data.treatment_description],
          ["Addon Notes:", data.addon_notes],
          ["Uploaded Images", data.uploaded_images]

        ]
        data_index_map_to_field_name_claim = [
          "id",
          "status",
          // "status_history",
          "ref_accident_id",
          "created_at",
          "medical_registrar_smc",
          "clinic_visit_date",
          "doctor_seen",
          "clinic_visited",
          "treatment_description",
          "addon_notes",
          "uploaded_images"
        ]

        this.setState({
          data_claim,
        })
        this.getSignedUrlsOfImagesUploaded("claim", data.ref_username_norm, data.uploaded_images)

      }
    } catch (err) {
    }
  }


  getSignedUrlsOfImagesUploaded = (accident_or_claim, username, uploaded_images) => {
    try {
      if (uploaded_images.length <= 0) { console.log("No uploaded_images found"); console.log(typeof (uploaded_images)); console.log(uploaded_images.length); return false }
    } catch (err) { console.log(err); return false }
    try {
      let token = localStorage.getItem('ifinsg_token')
      let url = '/api/image/getFileSignedUrl'
      axios.post(url, {
        token: token,
        username: username, // "JIEXIONGKHOO@GMAIL.COM",
        // file_names: ["JIEXIONGKHOO@GMAIL.COM_SIGNUP-IMAGE_2019-06-12T11:39:46.950Z_283_500 error.JPG"]
        file_names: uploaded_images
      }).then(res => {
        if (res.status === 200) {
          if (res.data.err) {
            alert(res.data.err)     // Eg: ERROR - Email already in use
          } else {
            if (accident_or_claim === "accident") { this.setState({ signed_urls_accident: res.data.signed_urls }) }
            else { this.setState({ signed_urls_claim: res.data.signed_urls }) }
          }
        }

      }).catch(err => {
        alert(err)
      })

    } catch (err) {
      alert(err)

    }
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props.open === false && nextProps.open === true) {
      this.handleOpen()
    }
    if (this.props.data_json_split != nextProps.data_json_split) {
      this.populate_table('claim', nextProps.data_json_split)
      this.getAccident(nextProps.data_json_split.ref_username_norm, nextProps.data_json_split.ref_accident_id)
    }
  }

  componentDidMount = () => {

  }



  handleBackPressed = () => {
    history.pushState(null, null, null);
    this.handleClose()
  }

  getAccident = (username, accident_id) => {
    axios.post('/api/getAccident', {
      // axios.post("/api/admin/pendingApprovalAction", {

      token: localStorage.getItem('ifinsg_token'),
      accident_id,
      username: username
    }).then(res => {
      if (res.status === 200) {
        if (res.data.err) {
          alert(res.data.err)     // Eg: ERROR - Email already in use
        } else {

          this.populate_table('accident', res.data.get_accident_result)

          // this.setState({
          //   data_accident_json: res.data.get_accident_result
          // })
        }
      }

    }).catch(err => {
      alert(err)
    })
  }


  handleOpen = () => {
    this.setState({ open: true, pending_approval_mode_rework_accident: false, pending_approval_mode_rework_claim: false });
    history.pushState(null, null, null);
    window.addEventListener("popstate", this.handleBackPressed)

  };

  handleClose = () => {

    this.props.handleClose()
    this.setState({ open: false });

    // Removing back listener
    history.go(-1)
    window.removeEventListener('popstate', this.handleBackPressed)
  };


  handleModalConfirmActionClose = () => {
    this.setState({
      modal_confirm_action_open: false
    })
  }

  renderFields = () => {

  }


  getMuiThemeAccident = () => {
    return createMuiTheme({
      overrides: {
        MuiPaper: {
          elevation4: {
            // boxShadow: "0px 1px 1px 0px rgba(0, 0, 0, 0.1), 0px 0px 0px 0px rgba(0, 0, 0, 0.1), 0px 0px 1px 0px rgba(0, 0, 0, 0.1)" //Reduce the perceived elevation
            boxShadow: 0,
          },
        },

        MUIDataTableHeadRow: {
          //Change table header CSS
          root: {
            display: "none",
            "&>th": {
              // width: '10rem',
              padding: "1px 3px 1px 3px"
            },
            "&>th:nth-child(1)": { //first column
              paddingLeft: "100px" //padding before first column for non-checkboxed tables
            },
            // "&>th:nth-child(1)": showCheckboxes()
          }
        },


        MUIDataTableRow: {
          //Change table header CSS
          root: {

            // "&>th": {
            //   // width: '10rem',
            //   padding: "1px 3px 1px 3px"
            // },
            "&>th:nth-child(2)": { //first column
              paddingLeft: "100px" //padding before first column for non-checkboxed tables
            },
            // "&>th:nth-child(1)": showCheckboxes()
          }
        },

        MUIDataTableBodyCell: {
          //CSS of all cells
          root: {
            padding: "1px 10px 1px 10px"
          },
        },

        // Remove checkbox from display
        MUIDataTableSelectCell: this.state.pending_approval_mode_rework_accident ? {} : { checkboxRoot: { display: "none" } },
        MuiTableCell: { paddingCheckbox: this.state.pending_approval_mode_rework_accident ? { padding: 0 } : {} },
        MUIDataTableToolbarSelect: this.state.pending_approval_mode_rework_accident ? {} : { root: { display: "none" } }
      }
    });
  }





  getMuiThemeClaim = () => {
    return createMuiTheme({
      overrides: {
        MuiPaper: {
          elevation4: {
            // boxShadow: "0px 1px 1px 0px rgba(0, 0, 0, 0.1), 0px 0px 0px 0px rgba(0, 0, 0, 0.1), 0px 0px 1px 0px rgba(0, 0, 0, 0.1)" //Reduce the perceived elevation
            boxShadow: 0,
          },
        },

        MUIDataTableHeadRow: {
          //Change table header CSS
          root: {
            display: "none",
            "&>th": {
              // width: '10rem',
              padding: "1px 3px 1px 3px"
            },
            "&>th:nth-child(1)": { //first column
              paddingLeft: "100px" //padding before first column for non-checkboxed tables
            },
            // "&>th:nth-child(1)": showCheckboxes()
          }
        },


        MUIDataTableRow: {
          //Change table header CSS
          root: {

            // "&>th": {
            //   // width: '10rem',
            //   padding: "1px 3px 1px 3px"
            // },
            "&>th:nth-child(2)": { //first column
              paddingLeft: "100px" //padding before first column for non-checkboxed tables
            },
            // "&>th:nth-child(1)": showCheckboxes()
          }
        },

        MUIDataTableBodyCell: {
          //CSS of all cells
          root: {
            padding: "1px 10px 1px 10px"
          },
        },

        // Remove checkbox from display
        MUIDataTableSelectCell: this.state.pending_approval_mode_rework_claim ? {} : { checkboxRoot: { display: "none" } },
        MuiTableCell: { paddingCheckbox: this.state.pending_approval_mode_rework_claim ? { padding: 0 } : {} },
        MUIDataTableToolbarSelect: this.state.pending_approval_mode_rework_claim ? {} : { root: { display: "none" } }
      }
    });
  }

  toggleReworkMode = (accident_or_claim) => {
    if (accident_or_claim === "accident") {
      if (this.state.pending_approval_mode_rework_accident) {
        this.setState({ pending_approval_mode_rework_accident: false })
      } else {
        this.setState({ pending_approval_mode_rework_accident: true })
      }
    } else {
      if (this.state.pending_approval_mode_rework_claim) {
        this.setState({ pending_approval_mode_rework_claim: false })
      } else {
        this.setState({ pending_approval_mode_rework_claim: true })
      }
    }

  }



  handleActionButtonClicked = (accident_or_claim, pending_approval_action) => {

    let pending_rework_items = []
    let title = "Confirm Approve"
    if (pending_approval_action === "reject") {
      title = "Confirm Reject"
    }

    if (pending_approval_action === "rework") {
      title = "Confirm Rework"
      if (this.state.fields_selected.length <= 0) {
        alert("Please check items which need to be reworked")
        return false
      } else {
        pending_rework_items = this.state.fields_selected
      }
    }
    const token = localStorage.getItem('ifinsg_token');
    if (token) {
      let body = {
        token,
        signups_or_claims: "claims",
        accident_or_claim,
        id: accident_or_claim === "accident" ? this.props.data_json_split.ref_accident_id : this.props.data_json_split.id,
        username: this.props.data_json.ref_username_norm,
        pending_approval_action: pending_approval_action,
        pending_rework_items,
      }

      this.setState({
        modal_confirm_action_open: true,
        modal_confirm_action: {
          modal_title: title,
          text_field_label: "Message to customer",
          number_field_label: "Amount claimed for",
          accident_or_claim,
          pending_approval_action,
          body
        }
      })

    } else {
      alert("Please login and try again")
    }
  }

  onSuccess = (accident_or_claim, pending_approval_action, body) => {
    if (accident_or_claim === "accident" && pending_approval_action === "approve") {
      this.setState({ accident_details_verified: true })
    }
  }

  renderActionButtons = (accident_or_claim) => {

    const action_buttons_approval_set = (accident_or_claim) => {
      if (accident_or_claim === "accident") {
        return (
          <div>
            <Button style={{ textTransform: "none" }} onClick={() => { this.setState({ filter: false }); this.toggleReworkMode("accident") }}> {this.state.pending_approval_mode_rework_accident ? "Actually, nothing's wrong" : "Something's wrong?"} </Button>
            {this.state.pending_approval_mode_rework_accident ?
              [
                < Button onClick={() => { this.handleActionButtonClicked(accident_or_claim, "reject") }} style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "red", opacity: 0.7 }}>REJECT</Button>,
                < Button onClick={() => { this.handleActionButtonClicked(accident_or_claim, "rework") }} style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "blue", opacity: 0.7 }}> REWORK </Button>,
              ]
              :
              < Button onClick={() => { this.handleActionButtonClicked(accident_or_claim, "approve") }} style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "green", opacity: 0.7 }}> APPROVE </Button>
            }
          </div>
        )
      } else {
        return (
          <div>
            <Button style={{ textTransform: "none" }} onClick={() => { this.setState({ filter: false }); this.toggleReworkMode("claim") }}> {this.state.pending_approval_mode_rework_claim ? "Actually, nothing's wrong" : "Something's wrong?"} </Button>
            {this.state.pending_approval_mode_rework_claim ?
              [
                // < Button style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "red", opacity: 0.7 }}> <span> REJECT <br></br> <span style={{ fontSize: "11px" }}> (completely non-salvagable)</span> </span></Button>,
                < Button onClick={() => { this.handleActionButtonClicked(accident_or_claim, "reject") }} style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "red", opacity: 0.7 }}>REJECT</Button>,
                < Button onClick={() => { this.handleActionButtonClicked(accident_or_claim, "rework") }} style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "blue", opacity: 0.7 }}> REWORK </Button>,
              ]
              :
              < Button onClick={() => { this.handleActionButtonClicked(accident_or_claim, "approve") }} style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "green", opacity: 0.7 }}> APPROVE </Button>
            }
          </div>
        )
      }
    }

    const action_buttons_nuclear_set =
      <div>
        < Button onClick={() => { alert("Cancel this person money") }} style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "black", opacity: 0.7 }}> Cancel claim </Button>
        < Button onClick={() => { alert("Charge this person money") }} style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "red", opacity: 0.7 }}> Charge </Button>
        < Button onClick={() => { alert("Refund this person money") }} style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "green", opacity: 0.7 }}> Refund </Button>
      </div>

    switch (this.props.mode) {
      case "pending_approval":
        return (action_buttons_approval_set(accident_or_claim))
      case "rework":
        return (action_buttons_approval_set(accident_or_claim))
      case "cancelled":
        // No action buttons to show
        break;
      case "rejected":
        // No action buttons to show
        break;
      case "nuclear":
        if (accident_or_claim === "claim") { return (action_buttons_nuclear_set) }
        break;
      default:
        // Should not happen
        break;
    }


  }

  renderUploadedImages = (accident_or_claim) => {
    let returnable = []
    let signed_urls = []
    if (accident_or_claim === "accident") { signed_urls = this.state.signed_urls_accident }
    else { signed_urls = this.state.signed_urls_claim }

    for (let i = 0; i < signed_urls.length; i++) {
      let image =
        <img style={{ maxWidth: "400px" }} src={signed_urls[i]}></img>
      returnable.push(image)
    }

    return (<div> {returnable} </div>)
  }

  render() {
    const { classes } = this.props;
    const data_options = (accident_or_claim) => {
      return {
        responsive: "scroll", // stacked or scroll
        selectableRows: true, // show checkboxes
        rowsPerPageOptions: [10, 20, 50],
        sort: false,
        print: false,
        download: false,
        search: false,
        caseSensitive: false, //caseSensitivity does not work well - cannot find all letters in CAPs :(
        viewColumns: false,
        filter: false,
        pagination: false,
        onRowsSelect: (currentRowsSelected, rowsSelected) => {
          let fields_selected = []
          rowsSelected.forEach(element => {
            let field = data_index_map_to_field_name_accident[element.dataIndex]
            if (accident_or_claim === "claim") {
              field = data_index_map_to_field_name_claim[element.dataIndex]
            }

            fields_selected.push(field)
          });
          this.setState({
            fields_selected
          })

        },
        customToolbarSelect: (selectedRows, displayData, setSelectedRows) => {
        },

        textLabels: {
          selectedRows: {
            text: "field(s) selected",
          },
        },
        serverSide: true,
        onTableChange: (action, tableState) => {
        },
        isRowSelectable: (dataIndex) => {
          let not_selectable = []
          if (accident_or_claim === "accident") { not_selectable = [0, 1, 2] }
          else { not_selectable = [0, 1, 2, 3] }
          if (not_selectable.indexOf(dataIndex) !== -1) {
            return false
          }
          else { return true }
        }
      }
    }

    return (
      <div>
        <ModalConfirmActionClaim
          open={this.state.modal_confirm_action_open}
          handleClose={this.handleModalConfirmActionClose}
          onSuccess={this.onSuccess}
          modal_confirm_action_data={this.state.modal_confirm_action}
        />
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>
            <IconButton onClick={() => { this.handleClose() }} style={{ float: "right", transform: "translate(20px,-20px)" }}><CloseIcon /> </IconButton>
            <Grid container style={{}} >
              <Grid item xs={6} style={{ textAlign: 'left', padding: "0px 15px 0px 0px" }}>
                <div style={{ position: "relative" }}>
                  <div style={{ textAlign: "center", marginBottom: "10px" }}>   <Typography variant="h4" style={{ fontWeight: "bold" }}> Accident </Typography> </div>
                  {this.props.mode === "pending_approval" || this.props.mode === "rework" ?
                    <Typography variant="h6" > {this.state.pending_approval_mode_rework_accident ? "Which fields are wrong?" : "Check accident details"}  </Typography>
                    :
                    ""
                  }
                  <MuiThemeProvider theme={this.getMuiThemeAccident()}>
                    <div style={{ wordBreak: 'normal', marginTop: "20px" }} >
                      <MUIDataTable
                        // title={this.state.title}
                        data={this.state.data_accident}
                        columns={this.state.columns}
                        options={data_options('accident')}
                      />
                    </div>
                  </MuiThemeProvider>
                  <div>
                    {this.renderUploadedImages("accident")}
                  </div>
                  <div style={{ textAlign: "right", marginTop: "20px" }}>
                    {this.renderActionButtons("accident")}
                  </div>
                  {this.state.accident_details_verified && (this.props.mode === "pending_approval" || this.props.mode === "rework") ? <p style={{
                    position: "absolute", bottom: 0, fontSize: "24px", background: "rgba(251,251,251,0.6)",
                    height: "100%", width: "100%", textAlign: "center", display: "flex", margin: 0
                  }}>
                    <Typography style={{
                      fontSize: "26px", color: "darkblue", opacity: 0.5, flex: "auto", alignSelf: "center", fontWeight: "bold",
                    }}> Accident details verified </Typography>
                  </p>
                    :
                    ""
                  }

                </div>

              </Grid>
              <Grid item xs={6} style={{ textAlign: 'left', padding: "0px 0px 0px 15px" }}>

                <div style={{ position: "relative" }}>
                  <div style={{ textAlign: "center", marginBottom: "10px" }}>  <Typography variant="h4" style={{ fontWeight: "bold" }}>  Claim </Typography></div>

                  {this.props.mode === "pending_approval" || this.props.mode === "rework" ?
                    <Typography variant="h6" > {this.state.pending_approval_mode_rework_claim ? "Which fields are wrong?" : "Check claim details carefully"}  </Typography>
                    :
                    ""
                  }
                  <MuiThemeProvider theme={this.getMuiThemeClaim()}>
                    <div style={{ wordBreak: 'normal', marginTop: "20px" }} >
                      <MUIDataTable
                        // title={this.state.title}
                        data={this.state.data_claim}
                        columns={this.state.columns}
                        options={data_options('claim')}
                      />
                    </div>
                  </MuiThemeProvider>


                  <div>
                    {this.renderUploadedImages("claim")}
                  </div>

                  <div style={{ textAlign: "right", marginTop: "20px" }}>
                    {this.renderActionButtons("claim")}
                  </div>


                  {!this.state.accident_details_verified && (this.props.mode === "pending_approval" || this.props.mode === "rework") ? <p style={{
                    position: "absolute", bottom: 0, fontSize: "24px", background: "rgba(251,251,251,0.6)",
                    height: "100%", width: "100%", textAlign: "center", display: "flex", margin: 0
                  }}>
                    <Typography style={{
                      fontSize: "26px", color: "darkblue", opacity: 0.5, flex: "auto", alignSelf: "center", fontWeight: "bold",
                      // transform: "rotate(-45deg)" 
                    }}> Verify accident details first </Typography>
                  </p>
                    :
                    ""
                  }
                </div>


              </Grid>
            </Grid>
          </div>
        </Modal>
      </div>
    );
  }
}

SimpleModal.propTypes = {
  classes: PropTypes.object.isRequired,
};

// We need an intermediary variable for handling the recursive nesting.
const SimpleModalWrapped = withStyles(styles)(SimpleModal);

export default SimpleModalWrapped;














