import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary } from '@material-ui/core';
import { NoSsr, Button, Typography, Checkbox, Grid, Paper, FormControlLabel, Modal, IconButton, TextField } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import moment from 'moment'

import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";
import Constants from 'constants/constants';
import Router from 'next/router';

import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import MUIDataTable from "mui-datatables";      // Source: https://github.com/gregnb/mui-datatables
import GlobalConstants from '../../../global/constants';
import GlobalHelpers from '../../../global/helpers';
import ModalConfirmAction from './ModalConfirmAction'
import axios from 'axios'

let data_index_map_to_field_name_user = []
let data_index_map_to_field_name_policy = []

function getModalStyle() {
  // const top = 50 + rand();
  // const left = 50 + rand();
  const top = 50
  const left = 50
  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => {
  let width = '90%'

  if (isBrowser) {
    width = '90%'
  }

  return ({
    paper: {
      position: 'absolute',
      width: width,// theme.spacing.unit * 50,
      // maxWidth: "900px",
      backgroundColor: theme.palette.background.paper,
      padding: theme.spacing.unit * 4,
      outline: 'none',
      overflow: 'scroll',
      maxHeight: '90%',
    },
  })
}


class SimpleModal extends React.Component {
  state = {
    open: this.props.open,
    columns: ["Field", "Value"],
    // [
    //   {
    //     name: "field",
    //     label: "Field",
    //     options: {
    //       filter: false,
    //       sort: false,
    //     }
    //   },
    //   {
    //     name: "value",
    //     label: "Value",
    //     options: {
    //       filter: false,
    //       sort: false,
    //     }
    //   }],

    signed_urls_user: [],
    signed_urls_policy: [],
    // policy_columns: [
    //   { name: "name", options: { filter: false, sort: true, } },
    //   { label: "Company", options: { filter: true, sort: false, } },
    //   { label: "City_label", options: { filter: true, sort: true, } },
    //   {
    //     name: "Age",
    //     options: {
    //       filter: false,
    //       customBodyRender: (value, tableMeta, updateValue) => (
    //         <FormControlLabel
    //           control={<TextField value={value || ''} type='number' />}
    //           onChange={event => updateValue(event.target.value)}
    //         />
    //       )
    //     }
    //   },
    //   { label: "State", options: { filter: true, sort: true, } },
    // ],
    data_user: [],
    data_policy: [],

    pending_approval_mode_rework_user: false,
    pending_approval_mode_rework_policy: false,

    fields_selected: [],

    modal_confirm_action_open: false,
    modal_confirm_action: {},
    modal_confirm_action_action: "",
  }




  populate_table = (data_json_split) => {

    console.log("populate_table")
    console.log(data_json_split)

    // Getting the signed URLs of the images_uploaded
    // this.getSignedUrlsOfImagesUploaded("user", data_json_split.user.username_norm, data_json_split.user.uploaded_images)
    if (data_json_split.policy.representing !== "self") {
      this.getSignedUrlsOfImagesUploaded("policy", data_json_split.user.username_norm, data_json_split.policy.uploaded_images)
    } else {
      this.getSignedUrlsOfImagesUploaded("user", data_json_split.user.username_norm, data_json_split.user.uploaded_images)
    }

    // console.log(data_json_split.user.username_norm)
    // console.log('data_json_split.user.username_norm')
    // console.log(data_json_split.policy.uploaded_images)
    // console.log('data_json_split.policy.uploaded_images')

    // this.getSignedUrlsOfImagesUploaded("policy", data_json_split.user.username_norm, data_json_split.policy.uploaded_images)

    console.log('modal individual policy',data_json_split)
    let data_user = [
      // { field: "Username", value: data_json_split.user.username_norm },
      ["Username", data_json_split.user.username_norm],
      ["Full name", data_json_split.user.full_name],
      ["Preferred name", data_json_split.user.preferred_name],
      ["NRIC", data_json_split.user.nric_number],
      ["DOB", data_json_split.user.dob_norm],
      ["Gender", data_json_split.user.gender],
      ["Mobile", data_json_split.user.mobile_number],
      ["Occupation", data_json_split.user.occupation],
      ["Self-employed", data_json_split.user.self_employed ? "true" : "false"],
      ["Place of Birth", data_json_split.user.place_of_birth],
      ["Referrer (signup)", data_json_split.user.referrer],
      ["Date of Signup",moment(data_json_split.policy.created_at).format('YYYY-MM-DD HH:mm:ss')],
      ["Bank (payer)", data_json_split.user.bank],
      // ["Bank Acct Num (payer)", data_json_split.user.bank_acct_number],
      ["Uploaded images", data_json_split.user.uploaded_images],
    ]

    data_index_map_to_field_name_user = [
      "username",
      "full_name",
      "preferred_name",
      "nric_number",
      "dob",
      "gender",
      "mobile_number",
      "occupation",
      "self_employed",
      "place_of_birth",
      "referrer",
      "date_of_signup",
      "bank",
      "uploaded_images",
    ]

    let data_policy = []

    if (data_json_split.policy.representing === "self") {
      data_policy = [
        ["Policy ID", data_json_split.policy.id],
        ["Product", data_json_split.policy.product],
        ["Status", data_json_split.policy.status],
        ["Representing", data_json_split.policy.representing],
        ["Referrer", data_json_split.policy.referrer],
        ["Source", data_json_split.policy.source],
        ["Date started", GlobalConstants.mode_dev ? GlobalHelpers.displayDate(data_json_split.policy.created_at, "yyyy-MM-dd HH:mm:ss") : GlobalHelpers.displayDate(data_json_split.policy.created_at, "yyyy-MM-dd HH:mm:ss", 8 * 60 * 60 * 1000)],
      ]
      data_index_map_to_field_name_policy = [
        "id",
        "product",
        "status",
        "representing",
        "referrer",
        "source",
        "created_at"
      ]
    } else {
      data_policy = [
        ["Policy ID", data_json_split.policy.id],
        ["Product", data_json_split.policy.product],
        ["Status", data_json_split.policy.status],
        ["Representing", data_json_split.policy.representing],
        ["Referrer", data_json_split.policy.referrer],
        ["Source", data_json_split.policy.source],
        ["Date started", GlobalConstants.mode_dev ? GlobalHelpers.displayDate(data_json_split.policy.created_at, "yyyy-MM-dd HH:mm:ss") : GlobalHelpers.displayDate(data_json_split.policy.created_at, "yyyy-MM-dd HH:mm:ss", 8 * 60 * 60 * 1000)],

        ["Relationship to Payer", data_json_split.policy.relationship_to_payer],
        ["Full name", data_json_split.policy.full_name],
        ["Preferred name", data_json_split.policy.preferred_name],
        ["NRIC", data_json_split.policy.nric_number],
        ["DOB", data_json_split.policy.dob_norm],
        ["Email", data_json_split.policy.email],
        ["Gender", data_json_split.policy.gender],
        ["Mobile", data_json_split.policy.mobile_number],
        ["Occupation", data_json_split.policy.occupation],
        ["Self-employed", data_json_split.policy.self_employed ? "true" : "false"],
        ["Place of Birth", data_json_split.policy.place_of_birth],

        ["Bank (claims)", data_json_split.policy.bank_claims_to_payer ? "Same as payer" : data_json_split.policy.bank_claims],
        // ["Bank Acct Num (claims)", data_json_split.policy.bank_claims_to_payer ? "Same as payer" : data_json_split.policy.bank_acct_number_claims],
        ["Bank (premiumback)", data_json_split.policy.bank_premiumback_to_payer ? "Same as payer" : data_json_split.policy.bank_premiumback],
        // ["Bank Acct Num (premiumback)", data_json_split.policy.bank_premiumback_to_payer ? "Same as payer" : data_json_split.policy.bank_acct_number_premiumback],
        ["Uploaded images", data_json_split.policy.uploaded_images],
      ]

      data_index_map_to_field_name_policy = [
        "id",
        "product",
        "status",
        "representing",
        "referrer",
        "source",
        "created_at",

        "relationship_to_payer",
        "full_name",
        "preferred_name",
        "nric_number",
        "dob",
        "email",
        "gender",
        "mobile_number",
        "occupation",
        "self_employed",
        "place_of_birth",

        "bank_claims",
        // "bank_acct_number_claims",
        "bank_premiumback",
        // "bank_acct_number_premiumback",
        "uploaded_images",
      ]
    }


    this.setState({
      data_user,
      data_policy,


      payer_details_verified: data_json_split.user.payer_details_verified
    })

  }


  getSignedUrlsOfImagesUploaded = (user_or_policy, username, uploaded_images) => {
    console.log("getSignedUrlsOfImagesUploaded", user_or_policy, username, uploaded_images)

    try {
      let token = localStorage.getItem('ifinsg_token')
      let url = '/api/image/getFileSignedUrl'

      // console.log(data_json)
      // console.log("this.state.policy_data")

      axios.post(url, {
        token: token,
        username: username, // "JIEXIONGKHOO@GMAIL.COM",
        // file_names: ["JIEXIONGKHOO@GMAIL.COM_SIGNUP-IMAGE_2019-06-12T11:39:46.950Z_283_500 error.JPG"]
        file_names: uploaded_images
      }).then(res => {
        if (res.status === 200) {
          if (res.data.err) {
            alert(res.data.err)     // Eg: ERROR - Email already in use
          } else {
            if (user_or_policy === "user") { this.setState({ signed_urls_user: res.data.signed_urls }) }
            else { this.setState({ signed_urls_policy: res.data.signed_urls }) }
          }
        }

      }).catch(err => {
        console.log(err)
        alert(err)
      })

    } catch (err) {
      console.log("Unable to get token from local storage")
      alert(err)

    }
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props.open === false && nextProps.open === true) {
      this.handleOpen()
    }
    if (this.props.data_json_split != nextProps.data_json_split) {
      this.populate_table(nextProps.data_json_split)

      // this.setState({
      //   policy_data_split: 
      // })
    }


    // this.getSignedUrlsOfImagesUploaded(nextProps.data_json)
    // this.stategetSignedUrlsOfImagesUploaded = (user_or_policy, username, uploaded_images) => {




    // if (this.props.data_json_split != nextProps.data_json_split) {
    //   // console.log(nextProps.data_json)

    //   this.getSignedUrlsOfImagesUploaded(nextProps.data_json_split)

    //   this.setState({
    //     policy_data: get_policy_data_from_data_json(nextProps.data_json_split)
    //   })
    // }
  }

  componentDidMount = () => {

  }



  handleBackPressed = () => {
    history.pushState(null, null, null);
    this.handleClose()
  }

  handleOpen = () => {
    this.setState({ open: true, pending_approval_mode_rework_user: false, pending_approval_mode_rework_policy: false });

    // Adding back listener
    history.pushState(null, null, null);
    window.addEventListener("popstate", this.handleBackPressed)

  };


  handleModalConfirmActionClose = (close_parent) => {
    this.setState({
      modal_confirm_action_open: false
    }, () => {
      if (close_parent) { this.handleClose() }
    })
  }


  handleClose = () => {

    this.props.handleClose()
    this.setState({ open: false });

    // Removing back listener
    history.go(-1)
    window.removeEventListener('popstate', this.handleBackPressed)
  };


  renderFields = () => {

  }


  getMuiThemeUser = () => {
    return createMuiTheme({
      overrides: {
        MuiPaper: {
          elevation4: {
            // boxShadow: "0px 1px 1px 0px rgba(0, 0, 0, 0.1), 0px 0px 0px 0px rgba(0, 0, 0, 0.1), 0px 0px 1px 0px rgba(0, 0, 0, 0.1)" //Reduce the perceived elevation
            boxShadow: 0,
          },
        },

        MUIDataTableHeadRow: {
          //Change table header CSS
          root: {
            display: "none",
            "&>th": {
              // width: '10rem',
              padding: "1px 3px 1px 3px"
            },
            "&>th:nth-child(1)": { //first column
              paddingLeft: "100px" //padding before first column for non-checkboxed tables
            },
            // "&>th:nth-child(1)": showCheckboxes()
          }
        },


        MUIDataTableRow: {
          //Change table header CSS
          root: {

            // "&>th": {
            //   // width: '10rem',
            //   padding: "1px 3px 1px 3px"
            // },
            "&>th:nth-child(2)": { //first column
              paddingLeft: "100px" //padding before first column for non-checkboxed tables
            },
            // "&>th:nth-child(1)": showCheckboxes()
          }
        },

        MUIDataTableBodyCell: {
          //CSS of all cells
          root: {
            padding: "1px 10px 1px 10px"
          },
        },

        // Remove checkbox from display
        MUIDataTableSelectCell: this.state.pending_approval_mode_rework_user ? {} : { checkboxRoot: { display: "none" } },
        MuiTableCell: { paddingCheckbox: this.state.pending_approval_mode_rework_user ? { padding: 0 } : {} },
        MUIDataTableToolbarSelect: this.state.pending_approval_mode_rework_user ? {} : { root: { display: "none" } }
      }
    });
  }





  getMuiThemePolicy = () => {
    return createMuiTheme({
      overrides: {
        MuiPaper: {
          elevation4: {
            // boxShadow: "0px 1px 1px 0px rgba(0, 0, 0, 0.1), 0px 0px 0px 0px rgba(0, 0, 0, 0.1), 0px 0px 1px 0px rgba(0, 0, 0, 0.1)" //Reduce the perceived elevation
            boxShadow: 0,
          },
        },

        MUIDataTableHeadRow: {
          //Change table header CSS
          root: {
            display: "none",
            "&>th": {
              // width: '10rem',
              padding: "1px 3px 1px 3px"
            },
            "&>th:nth-child(1)": { //first column
              paddingLeft: "100px" //padding before first column for non-checkboxed tables
            },
            // "&>th:nth-child(1)": showCheckboxes()
          }
        },


        MUIDataTableRow: {
          //Change table header CSS
          root: {

            // "&>th": {
            //   // width: '10rem',
            //   padding: "1px 3px 1px 3px"
            // },
            "&>th:nth-child(2)": { //first column
              paddingLeft: "100px" //padding before first column for non-checkboxed tables
            },
            // "&>th:nth-child(1)": showCheckboxes()
          }
        },

        MUIDataTableBodyCell: {
          //CSS of all cells
          root: {
            padding: "1px 10px 1px 10px"
          },
        },

        // Remove checkbox from display
        MUIDataTableSelectCell: this.state.pending_approval_mode_rework_policy ? {} : { checkboxRoot: { display: "none" } },
        MuiTableCell: { paddingCheckbox: this.state.pending_approval_mode_rework_policy ? { padding: 0 } : {} },
        MUIDataTableToolbarSelect: this.state.pending_approval_mode_rework_policy ? {} : { root: { display: "none" } }
      }
    });
  }




  toggleReworkMode = (user_or_policy) => {
    if (user_or_policy === "user") {
      if (this.state.pending_approval_mode_rework_user) {
        this.setState({ pending_approval_mode_rework_user: false })
      } else {
        this.setState({ pending_approval_mode_rework_user: true })
      }
    } else {
      if (this.state.pending_approval_mode_rework_policy) {
        this.setState({ pending_approval_mode_rework_policy: false })
      } else {
        this.setState({ pending_approval_mode_rework_policy: true })
      }
    }

  }



  handleActionButtonClicked = (user_or_policy, pending_approval_action) => {

    console.log(user_or_policy)
    console.log(pending_approval_action)

    let pending_rework_items = []
    let title = "Confirm Approve"
    if (pending_approval_action === "reject") {
      title = "Confirm Reject"
    }

    if (pending_approval_action === "rework") {
      console.log(this.state.fields_selected)
      if (this.state.fields_selected.length <= 0) {
        alert("Please check items which need to be reworked")
        return false
      } else {
        pending_rework_items = this.state.fields_selected
      }
    }
    console.log("handleActionButtonClicked()")

    const token = localStorage.getItem('ifinsg_token');
    if (token) {

      let body = {
        token,
        signups_or_claims: "signups",
        user_or_policy,
        id: this.props.data_json.id,
        username: this.props.data_json.username_norm,
        pending_approval_action: pending_approval_action,
        pending_rework_items,
        policy: this.props.data_json.product
      }

      try {
        if (user_or_policy === "policy" && pending_approval_action === "approve" && this.props.data_json.product_details.promotion.name !== "normal") {
          body.coupon_name = this.props.data_json.product_details.promotion.name
        }
      } catch (err) {

        console.log(err)
        console.log(err)
      }

      this.setState({
        modal_confirm_action_open: true,
        modal_confirm_action: {
          modal_title: title,
          text_field_label: "Message to customer",
          user_or_policy,
          pending_approval_action,
          body
        }
      })

    } else {
      alert("Please login and try again")
    }

  }

  onSuccess = (user_or_policy, pending_approval_action, body) => {
    if (user_or_policy === "user" && pending_approval_action === "approve") {
      this.setState({ payer_details_verified: true })
    }
  }



  // handleActionButtonClicked = (user_or_policy, pending_approval_action) => {

  //   let pending_rework_items = []
  //   if (pending_approval_action === "rework") {
  //     console.log(this.state.fields_selected)
  //     if (this.state.fields_selected.length <= 0) {
  //       alert("Please check items which need to be reworked")
  //       return false
  //     } else {
  //       pending_rework_items = this.state.fields_selected
  //     }
  //   }
  //   console.log("handleActionButtonClicked()")
  //   try {
  //     const token = localStorage.getItem('ifinsg_token');

  //     if (token) {
  //       axios.post("/api/admin/pendingApprovalAction", {
  //         token,
  //         signups_or_claims: "signups",
  //         user_or_policy,
  //         id: this.props.data_json.id,
  //         username: this.props.data_json.username_norm,
  //         pending_approval_action: pending_approval_action,
  //         pending_rework_items
  //       }).then(res => {
  //         console.log("componentDidMount()")
  //         if (res.status === 200) {
  //           if (res.data.err) {
  //             console.log(res.data.err)     // Eg: ERROR - Email already in use
  //             alert(res.data.err)     // Eg: ERROR - Email already in use
  //           } else {
  //             try {
  //               // SUCCESSFUL
  //               console.log("SUCCESSFUL")
  //               alert("SUCCESSFUL")
  //               if (user_or_policy === "user" && pending_approval_action === "approve") {
  //                 this.setState({ payer_details_verified: true })
  //               }
  //             } catch (err) {
  //               console.log(err)
  //               console.log("Unexpected params received from api/auth/getUserDataFromToken")
  //               alert(err)
  //             }
  //           }
  //         } else {
  //           console.log("ERROR - ", res)
  //           alert("ERROR - ", res)
  //         }
  //       }).catch(err => {
  //         console.log(err);
  //         alert(err)

  //       });
  //     } else {
  //       alert("Please login and try again")
  //     }
  //   } catch (err) {
  //     console.log(err)
  //     alert(err)
  //   }


  // }

  renderActionButtons = (user_or_policy) => {
    console.log(this.props.mode)
    console.log('this.props.mode')

    const action_buttons_approval_set = (user_or_policy) => {

      if (user_or_policy === "user") {

        return (
          <div>
            <Button style={{ textTransform: "none" }} onClick={() => { this.setState({ filter: false }); this.toggleReworkMode("user") }}> {this.state.pending_approval_mode_rework_user ? "Actually, nothing's wrong" : "Something's wrong?"} </Button>
            {this.state.pending_approval_mode_rework_user ?
              [
                // < Button style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "red", opacity: 0.7 }}> <span> REJECT <br></br> <span style={{ fontSize: "11px" }}> (completely non-salvagable)</span> </span></Button>,
                < Button onClick={() => { this.handleActionButtonClicked(user_or_policy, "reject") }} style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "red", opacity: 0.7 }}>REJECT</Button>,
                < Button onClick={() => { this.handleActionButtonClicked(user_or_policy, "rework") }} style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "blue", opacity: 0.7 }}> REWORK </Button>,
              ]
              :
              < Button onClick={() => { this.handleActionButtonClicked(user_or_policy, "approve") }} style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "green", opacity: 0.7 }}> APPROVE </Button>
            }
          </div>
        )

      } else {
        return (
          <div>
            <Button style={{ textTransform: "none" }} onClick={() => { this.setState({ filter: false }); this.toggleReworkMode("policy") }}> {this.state.pending_approval_mode_rework_policy ? "Actually, nothing's wrong" : "Something's wrong?"} </Button>
            {this.state.pending_approval_mode_rework_policy ?
              [
                // < Button style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "red", opacity: 0.7 }}> <span> REJECT <br></br> <span style={{ fontSize: "11px" }}> (completely non-salvagable)</span> </span></Button>,
                < Button onClick={() => { this.handleActionButtonClicked(user_or_policy, "reject") }} style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "red", opacity: 0.7 }}>REJECT</Button>,
                < Button onClick={() => { this.handleActionButtonClicked(user_or_policy, "rework") }} style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "blue", opacity: 0.7 }}> REWORK </Button>,
              ]
              :
              < Button onClick={() => { this.handleActionButtonClicked(user_or_policy, "approve") }} style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "green", opacity: 0.7 }}> APPROVE </Button>
            }
          </div>
        )
      }

    }

    const action_buttons_nuclear_set =
      <div>
        < Button onClick={() => {
          this.setState({
            modal_confirm_action_action: "nuclear",
            modal_confirm_action_open: true,
            modal_confirm_action: {
              modal_title: "Request Policy Cancel?",
              text_field_label: "Message to customer",
              // user_or_policy: "",
              pending_approval_action: "cancellation_requested",
              body: {
                // token: localStorage.getItem('ifinsg_token'),
                // signups_or_claims: "signups",
                // user_or_policy: "policy",
                // id: this.props.data_json.id,
                // username: this.props.data_json.username_norm,
                // pending_approval_action: "approve_cancel",

                token: localStorage.getItem('ifinsg_token'),
                username: this.props.data_json.username_norm,
                policy_id: this.props.data_json.id,
                policy_status: "cancellation_requested",
                message: "",
                cancel_or_reinstate: "cancel"
              }

            }
          })
        }} style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "black", opacity: 0.7 }}> Cancel Policy </Button>
        < Button onClick={() => {
          this.setState({
            modal_confirm_action_action: "nuclear",
            modal_confirm_action_open: true,
            modal_confirm_action: {
              modal_title: "Charge Customer? (actually charges a customer's card)",
              text_field_label: "Message to customer",
              number_field_label: "Charge Amount (Singapore dollars)",
              // user_or_policy: "",
              pending_approval_action: "charge",
              body: {
                token: localStorage.getItem('ifinsg_token'),
                signups_or_claims: "signups",
                user_or_policy: "policy",
                id: this.props.data_json.id,
                username: this.props.data_json.username_norm,
                pending_approval_action: "charge",
              }
            }
          })
        }} style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "red", opacity: 0.7 }}> Charge </Button>
        < Button onClick={() => {
          this.setState({
            modal_confirm_action_action: "nuclear",
            modal_confirm_action_open: true,
            modal_confirm_action: {
              modal_title: "Record a refund? (does not actually refund)",
              text_field_label: "Message to customer",
              number_field_label: "Refund Amount",
              pending_approval_action: "refund",
              body: {
                token: localStorage.getItem('ifinsg_token'),
                signups_or_claims: "signups",
                user_or_policy: "policy",
                id: this.props.data_json.id,
                username: this.props.data_json.username_norm,
                pending_approval_action: "refund",
              }
            }
          })
        }} style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "green", opacity: 0.7 }}> Refund </Button>
      </div>


    switch (this.props.mode) {
      case "pending_approval":
        return (action_buttons_approval_set(user_or_policy))
      case "rework":
        return (action_buttons_approval_set(user_or_policy))
      case "pending_reinstatement":
        if (user_or_policy !== "user") {
          return (
            <div>
              <Button
                style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: Constants.color.darkgreen, opacity: 0.7 }}
                // style={{ textTransform: "none" }} 
                onClick={() => { this.handleActionButtonClicked(user_or_policy, "approve") }} > Approve Reinstatement </Button>
            </div>
          )
        }
        break;
      case "pending_cancel":
        if (user_or_policy !== "user") {
          return (
            <div>
              <Button
                style={{ fontSize: "20px", color: "white", marginLeft: "10px", padding: "10px 30px 10px 30px", backgroundColor: "red", opacity: 0.7 }}
                // style={{ textTransform: "none" }} 
                onClick={() => { this.handleActionButtonClicked(user_or_policy, "approve_cancel") }} > Approve Cancellation </Button>
            </div>
          )
        }
        break;
      case "cancelled":
        // No action buttons to show
        break;
      case "rejected":
        // No action buttons to show
        break;
      case "nuclear":
        if (user_or_policy === "policy") { return (action_buttons_nuclear_set) }
        break;
      default:
        // Should not happen
        break;
    }


  }

  renderUploadedImages = (user_or_policy) => {

    console.log("renderUploadedImages", user_or_policy)
    console.log(this.state)

    let returnable = []
    let signed_urls = []

    if (user_or_policy === "user") { signed_urls = this.state.signed_urls_user }
    else { signed_urls = this.state.signed_urls_policy }

    for (let i = 0; i < signed_urls.length; i++) {
      let image =
        <img style={{ maxWidth: "400px" }} src={signed_urls[i]}></img>
      returnable.push(image)
    }

    console.log(signed_urls)
    console.log(signed_urls.length)

    // console.log(this.state.signed_urls_user)
    // console.log(this.state.signed_urls_policy)

    return (<div> {returnable} </div>)
  }

  render() {
    const { classes } = this.props;
    const policy_data_options_user = {
      responsive: "scroll", // stacked or scroll
      selectableRows: true, // show checkboxes
      rowsPerPageOptions: [10, 20, 50],
      // rowsPerPage: 20,  // Should be one of above
      // filterType: "dropdown", // 'checkbox', 'dropdown', 'multiselect', 'textField'
      sort: false,
      print: false,
      download: false,
      search: false,
      caseSensitive: false, //caseSensitivity does not work well - cannot find all letters in CAPs :(
      viewColumns: false,
      filter: false,
      pagination: false,

      // resizableColumns:true,
      // expandableRows: true,
      // renderExpandableRow: (rowData, rowMeta) => {
      //   return (
      //     <tr style={{ rowspan: "3", width: "100%" }}>
      //       <td  colspan="100%" >testing testing test test test testing test test test testing </td>
      //     </tr>
      //   )
      // },

      // onCellClick: (colData, cellMeta) => {
      //   // console.log(colData)
      //   // console.log(cellMeta)
      //   let dataIndex = cellMeta.dataIndex
      //   // console.log(dataIndex)
      //   // console.log(this.state.data[dataIndex])

      //   if (this.props.handleTableCellClicked) {
      //     let table = this.props.table
      //     let cell_data = this.state.data[dataIndex]
      //     this.props.handleTableCellClicked(table, cell_data)
      //   }

      // },

      onRowsSelect: (currentRowsSelected, rowsSelected) => {
        console.log(JSON.stringify(rowsSelected));
        console.log(this.props.data_json)

        let fields_selected = []
        rowsSelected.forEach(element => {
          // console.log(element.dataIndex)
          let field = data_index_map_to_field_name_user[element.dataIndex]
          fields_selected.push(field)
        });
        console.log(fields_selected)
        this.setState({
          fields_selected
        })

      },
      customToolbarSelect: (selectedRows, displayData, setSelectedRows) => {
        // console.log(selectedRows, displayData, setSelectedRows)

      },

      textLabels: {
        selectedRows: {
          text: "field(s) selected",
          // delete: "Delete",
          // deleteAria: "Delete Selected Rows",
        },
      },
      serverSide: true,
      onTableChange: (action, tableState) => {
        console.log("onTableChange")
        // this.setState({ 
        //   data: result 
        // });

      },
      isRowSelectable: (dataIndex) => {
        let not_selectable = [0, 10, 11]
        if (not_selectable.indexOf(dataIndex) !== -1) {
          return false
        }
        else { return true }
      }
    }
    const policy_data_options_policy = {
      responsive: "scroll", // stacked or scroll
      selectableRows: true, // show checkboxes
      rowsPerPageOptions: [10, 20, 50],
      // rowsPerPage: 20,  // Should be one of above
      // filterType: "dropdown", // 'checkbox', 'dropdown', 'multiselect', 'textField'
      sort: false,
      print: false,
      download: false,
      search: false,
      caseSensitive: false, //caseSensitivity does not work well - cannot find all letters in CAPs :(
      viewColumns: false,
      filter: false,
      pagination: false,

      // resizableColumns:true,
      // expandableRows: true,
      // renderExpandableRow: (rowData, rowMeta) => {
      //   return (
      //     <tr style={{ rowspan: "3", width: "100%" }}>
      //       <td  colspan="100%" >testing testing test test test testing test test test testing </td>
      //     </tr>
      //   )
      // },

      // onCellClick: (colData, cellMeta) => {
      //   // console.log(colData)
      //   // console.log(cellMeta)
      //   let dataIndex = cellMeta.dataIndex
      //   // console.log(dataIndex)
      //   // console.log(this.state.data[dataIndex])

      //   if (this.props.handleTableCellClicked) {
      //     let table = this.props.table
      //     let cell_data = this.state.data[dataIndex]
      //     this.props.handleTableCellClicked(table, cell_data)
      //   }

      // },

      onRowsSelect: (currentRowsSelected, rowsSelected) => {
        console.log(JSON.stringify(rowsSelected));
        console.log(this.props.data_json)

        let fields_selected = []
        rowsSelected.forEach(element => {
          // console.log(element.dataIndex)
          let field = data_index_map_to_field_name_policy[element.dataIndex]
          fields_selected.push(field)
        });
        console.log(fields_selected)
        this.setState({
          fields_selected
        })

      },
      customToolbarSelect: (selectedRows, displayData, setSelectedRows) => {
        // console.log(selectedRows, displayData, setSelectedRows)

      },

      textLabels: {
        selectedRows: {
          text: "field(s) selected",
          // delete: "Delete",
          // deleteAria: "Delete Selected Rows",
        },
      },
      serverSide: true,
      onTableChange: (action, tableState) => {
        console.log("onTableChange")
        // this.setState({ 
        //   data: result 
        // });

      },
      isRowSelectable: (dataIndex) => {
        let not_selectable = [0, 1, 2, 3, 4, 5, 6, 19]
        if (not_selectable.indexOf(dataIndex) !== -1) {
          return false
        }
        else { return true }
      }
    }

    return (
      <div>
        {/* <Typography gutterBottom>Click to get the full Modal experience!</Typography>
        <Button onClick={this.handleOpen}>Open Modal</Button> */}


        <ModalConfirmAction
          open={this.state.modal_confirm_action_open}
          handleClose={this.handleModalConfirmActionClose}
          onSuccess={this.onSuccess}
          modal_confirm_action_data={this.state.modal_confirm_action}
          action={this.state.modal_confirm_action_action}
        />



        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >





          <div style={getModalStyle()} className={classes.paper}>

            {/* <Typography> Inbox: {this.props.mode}</Typography> */}

            <IconButton onClick={() => { this.handleClose() }} style={{ float: "right", transform: "translate(20px,-20px)" }}><CloseIcon /> </IconButton>


            <Grid container style={{}} >


              <Grid item xs={6} style={{ textAlign: 'left', padding: "0px 15px 0px 0px" }}>

                <div style={{ position: "relative" }}>

                  <div style={{ textAlign: "center", marginBottom: "10px" }}>   <Typography variant="h4" style={{ fontWeight: "bold" }}> Payer </Typography> </div>

                  {this.props.mode === "pending_approval" || this.props.mode === "rework" ?
                    <Typography variant="h6" > {this.state.pending_approval_mode_rework_user ? "Which fields are wrong?" : "Check payer details"}  </Typography>
                    :
                    ""
                  }
                  <MuiThemeProvider theme={this.getMuiThemeUser()}>
                    <div style={{ wordBreak: 'normal', marginTop: "20px" }} >
                      <MUIDataTable
                        // title={this.state.title}
                        data={this.state.data_user}
                        columns={this.state.columns}
                        options={policy_data_options_user}
                      />
                    </div>
                  </MuiThemeProvider>


                  <div>
                    {this.renderUploadedImages("user")}
                  </div>

                  <div style={{ textAlign: "right", marginTop: "20px" }}>
                    {this.renderActionButtons("user")}
                  </div>



                  {this.state.payer_details_verified && (this.props.mode === "pending_approval" || this.props.mode === "rework") ? <p style={{
                    position: "absolute", bottom: 0, fontSize: "24px", background: "rgba(251,251,251,0.6)",
                    height: "100%", width: "100%", textAlign: "center", display: "flex", margin: 0
                  }}>
                    <Typography style={{
                      fontSize: "26px", color: "darkblue", opacity: 0.5, flex: "auto", alignSelf: "center", fontWeight: "bold",
                      // transform: "rotate(-45deg)" 
                    }}> Payer details verified </Typography>
                  </p>
                    :
                    ""
                  }

                </div>

              </Grid>




              <Grid item xs={6} style={{ textAlign: 'left', padding: "0px 0px 0px 15px" }}>

                <div style={{ position: "relative" }}>
                  <div style={{ textAlign: "center", marginBottom: "10px" }}>  <Typography variant="h4" style={{ fontWeight: "bold" }}>  Insured </Typography></div>

                  {this.props.mode === "pending_approval" || this.props.mode === "rework" ?
                    <Typography variant="h6" > {this.state.pending_approval_mode_rework_policy ? "Which fields are wrong?" : "Check policy details carefully"}  </Typography>
                    :
                    ""
                  }
                  <MuiThemeProvider theme={this.getMuiThemePolicy()}>
                    <div style={{ wordBreak: 'normal', marginTop: "20px" }} >
                      <MUIDataTable
                        // title={this.state.title}
                        data={this.state.data_policy}
                        columns={this.state.columns}
                        options={policy_data_options_policy}
                      />
                    </div>
                  </MuiThemeProvider>


                  <div>
                    {this.renderUploadedImages("policy")}
                  </div>

                  <div style={{ textAlign: "right", marginTop: "20px" }}>
                    {this.renderActionButtons("policy")}
                  </div>


                  {!this.state.payer_details_verified && (this.props.mode === "pending_approval" || this.props.mode === "rework") ? <p style={{
                    position: "absolute", bottom: 0, fontSize: "24px", background: "rgba(251,251,251,0.6)",
                    height: "100%", width: "100%", textAlign: "center", display: "flex", margin: 0
                  }}>
                    <Typography style={{
                      fontSize: "26px", color: "darkblue", opacity: 0.5, flex: "auto", alignSelf: "center", fontWeight: "bold",
                      // transform: "rotate(-45deg)" 
                    }}> Verify payer details first </Typography>
                  </p>
                    :
                    ""
                  }
                </div>


              </Grid>
            </Grid>







            {/* <SimpleModalWrapped /> */}
          </div>
        </Modal>
      </div>
    );
  }
}

SimpleModal.propTypes = {
  classes: PropTypes.object.isRequired,
};

// We need an intermediary variable for handling the recursive nesting.
const SimpleModalWrapped = withStyles(styles)(SimpleModal);

export default SimpleModalWrapped;














