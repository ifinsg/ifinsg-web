/*!
=========================================================
* NextJS Material Kit v1.0.0 based on Material Kit Free - v2.0.2 (Bootstrap 4.0.0 Final Edition) and Material Kit React v1.8.0
=========================================================
* Product Page: https://www.creative-tim.com/product/nextjs-material-kit
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/nextjs-material-kit/blob/master/LICENSE.md)
* Coded by Creative Tim
=========================================================
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/
import React from 'react';
import App from 'next/app';
import Head from 'next/head';
import Router from 'next/router';
import ThemeProvider from '@providers/Theme';
import { detect } from 'detect-browser';
import * as gtag from 'lib/gtag';

Router.events.on('routeChangeComplete', (url) => {
    console.log('routeChangeComplete');
    gtag.pageview(url);
});
export default class MyApp extends App {
    componentDidMount() {
        const browser = detect();
        if (browser && browser.name && browser.name === 'ie') {
            Router.push('/badbrowser');
        }
    }
    static async getInitialProps({ Component, router, ctx }) {
        let pageProps = {};

        if (Component.getInitialProps) {
            pageProps = await Component.getInitialProps(ctx);
        }

        return { pageProps };
    }
    render() {
        const { Component, pageProps } = this.props;

        return (
            <React.Fragment>
                <Head>
                    <title>iFinSG.com</title>
                </Head>
                <ThemeProvider>
                    <Component {...pageProps} />
                </ThemeProvider>
            </React.Fragment>
        );
    }
}
