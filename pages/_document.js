import React from 'react';
import Document, { Html, Head, Main, NextScript } from 'next/document';
import { ServerStyleSheet } from 'styled-components'
import { ServerStyleSheets } from '@material-ui/styles';
import { GA_TRACKING_ID } from 'lib/gtag'
class MyDocument extends Document {
    static async getInitialProps(ctx) {
        const styledComponentsSheet = new ServerStyleSheet()
        const materialSheets = new ServerStyleSheets()
        const originalRenderPage = ctx.renderPage;

        try {
            ctx.renderPage = () => originalRenderPage({
                enhanceApp: App => props => styledComponentsSheet.collectStyles(materialSheets.collect(<App {...props} />))
            })
            const initialProps = await Document.getInitialProps(ctx)
            return {
                ...initialProps,
                styles: (
                    <React.Fragment>
                        {initialProps.styles}
                        {materialSheets.getStyleElement()}
                        {styledComponentsSheet.getStyleElement()}
                    </React.Fragment>
                )
            }
        } finally {
            styledComponentsSheet.seal()
        }
    }

    render() {
        return (
            <Html lang="en" dir="ltr">
                <Head>
                    <meta charSet="utf-8" />
                    {/* Use minimum-scale=1 to enable GPU rasterization */}
                    <meta
                        name="viewport"
                        content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
                    />
                    <meta httpEquiv="X-UA-Compatible" content="IE=edge"></meta>
                    <meta name="description" content="Reimburses you up to $1,500 per accident (even for medical treatment of sprains, cuts or falls), covering All sports and has no Reduction of claims for any sport." />
                    <meta name="googlebot" content="index,follow" />
                    <meta name="description" content="Reimburses you up to $1,500 per accident (even for medical treatment of sprains, cuts or falls), covering All sports and has no Reduction of claims for any sport." />
                    <meta property="og:url" content="https://www.ifinsg.com" />
                    <meta property="og:type" content="website" />
                    <meta property="og:title" content="Products" />
                    <meta property="og:description" content="Reimburses you up to $1,500 per accident (even for medical treatment of sprains, cuts or falls), covering All sports and has no Reduction of claims for any sport." />
                    <meta property="og:image" content="https://www.ifinsg.com/ifinsg_logo.jpg" />
                    <meta property="og:image:alt" content="iFinSG Logo" />
                    <meta property="og:image:width" content="620" />
                    <meta property="og:image:height" content="350" />
                    <meta property="og:site_name" content="iFinSG" />
                    <link rel="stylesheet" href="/style.css" />
                    {/* PWA primary color */}
                    {/* <meta
                        name="theme-color"
                        content={theme.palette.primary.main}
                    /> */}
                    {/* <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous" /> */}
                    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"></link>
                    {/* <!-- Fonts to support Material Design --> */}
                    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Lato&family=Merriweather&display=swap" />
                    {/* <!-- Icons to support Material Design --> */}
                    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
                    <meta name="google-site-verification" content="INFcuIU60KmEMaJpyxoyCfYpDo4_BX4ch78qm0drqoQ" />
                    {/* Global Site Tag (gtag.js) - Google Analytics */}
                    <script async src={`https://www.googletagmanager.com/gtag/js?id=${GA_TRACKING_ID}`} />
                    <script
                        dangerouslySetInnerHTML={{
                            __html: `
                                window.dataLayer = window.dataLayer || [];
                                function gtag(){dataLayer.push(arguments);}
                                gtag('js', new Date());
                                gtag('config', '${GA_TRACKING_ID}', {
                                    page_path: window.location.pathname,
                                });
                            `,
                        }}
                    />
                    {/* Global Site Tag (gtag.js) - Smartlook */}
                    <script dangerouslySetInnerHTML={{
                        __html: `
                            window.smartlook||(function(d) {
                                let o=smartlook=function(){o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
                                var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
                                c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
                                })(document);
                                smartlook('init', 'fa28757d10a6631ca2dd86dc4701f74db8f352f3');
                            `
                    }}
                    />

                    {/* facebook analysis  */}
                    <script dangerouslySetInnerHTML={{
                        __html: `
                        !function(f,b,e,v,n,t,s)
                        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                        n.queue=[];t=b.createElement(e);t.async=!0;
                        t.src=v;s=b.getElementsByTagName(e)[0];
                        s.parentNode.insertBefore(t,s)}(window,document,'script',
                        'https://connect.facebook.net/en_US/fbevents.js');
                        fbq('init', '2081490255486355'); 
                        fbq('track', 'PageView');
                    `}} />


                    <noscript>
                        <img height="1" width="1" src="https://www.facebook.com/tr?id=2081490255486355&ev=PageView&noscript=1" />
                    </noscript>

                    <link href="/main.css" rel="stylesheet" />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        );
    }
}

export default MyDocument;