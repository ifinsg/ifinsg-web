import React, { Component } from 'react';

// import Logo from './ifinsg_logo.png'
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import TopNavBar from './components/TopNavBar';
import BotNavBar from './components/BotNavBar';

import { NoSsr, Grid, Divider, Button } from '@material-ui/core';

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Constants from 'constants/constants';
import Head from 'next/head';
import Link from 'next/link';
import './about-us.css';

const styles = {
    page_width: Constants.style.page.width,
    page_text_width: Constants.style.page.text_width,

    page_padding: Constants.style.page.padding,

    align_center: Constants.style.align.center,

    bold: Constants.style.text.bold,
    break_word: Constants.style.text.break_word,

    title1: Constants.style.text.title1,
    body1: Constants.style.text.body1,

    margin_top_xl: Constants.style.margin_top.xl,
    margin_top_lg: Constants.style.margin_top.lg,
    margin_top_md: Constants.style.margin_top.md,
    margin_top_sm: Constants.style.margin_top.sm,
    margin_top_xs: Constants.style.margin_top.xs,
    margin_top_xxs: Constants.style.margin_top.xxs
};

class AboutUs extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount = () => {
        window.scrollTo(0, 0);
        Constants.store_referrer(this);
    };

    render() {
        const { classes } = this.props;

        return (
            <div className="about-us">
                <TopNavBar selected="about-us" referrer={this.state.referrer} />

                <NextSeo config={Constants.next_seo_config('about-us')} />

                <NoSsr>
                    <div className={classNames(classes.align_center)}>
                        <div className={classNames(classes.page_text_width, classes.page_padding, classes.break_word)}>
                            <div
                                style={{
                                    margin: '10px 0px',
                                    fontWeight: 'bold',
                                    fontSize: '40px',
                                    fontFamily: 'Merriweather'
                                }}
                            >
                                {' '}
                                About us
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> </div>
                            <div>
                                {' '}
                                <img
                                    style={{
                                        width: '100%',
                                        margin: '20px 0px 0px 0px',
                                        padding: '0px 0px 0px 0px'
                                    }}
                                    src={'/img/about-us.jpg'}
                                />{' '}
                            </div>
                            <div style={{ paddingTop: '150px' }}></div>
                            <Divider></Divider>
                            <div
                                style={{
                                    margin: '100px 0px 0px 0px',
                                    fontWeight: 'bold',
                                    fontSize: '40px',
                                    lineHeight: '1',
                                    fontFamily: 'Merriweather',
                                }}
                            >
                                Pains
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                "Insurance is like a black box."
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                "Insurance is fast to collect money, and complicated to pay claims."
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                "Insurance is where I'll have to keep paying all the way for a long time, until
                                something happens to me."
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                "Insurance is slow to process my requests."
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                "Insurance has so many exclusions."
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                "Insurance has so many charges."
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                Haven't we heard of these pains before?
                            </div>
                            <div style={{ paddingTop: '150px' }}></div>
                            <Divider></Divider>
                            <div
                                style={{
                                    margin: '100px 0px 0px 0px',
                                    fontSize: '40px',
                                    fontWeight: 'bold',
                                    fontFamily: 'Merriweather',
                                    lineHeight: '1'
                                }}
                            >
                                Solution
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                iFinSG collects premiums only for the purpose of paying claims, and creates insurance at
                                the lowest possible real cost for you.
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                Every $ of premium we collect is either paid out as a claim, or returned to users as
                                PremiumBack at the end of each cycle.
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                No conflict of interest with our users:
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                nobody will try to lower the amount of claims, because we return all unclaimed premiums
                                back to our users at the end of each 3 months cycle.
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                No worry about Cash-flows:
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                because some money comes back quickly every 3 months, even when you make 0 claims or low
                                claims.
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>No hidden charges:</div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                we only earn a flat and transparent monthly Admin Fee per policy per user.
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                On top of the features above, it’s insurance the way you want it. Simple. Fast. Cheap.
                            </div>
                            <div style={{ paddingTop: '150px' }}></div>
                            <Divider></Divider>
                            <div
                                style={{
                                    margin: '100px 0px 0px 0px',
                                    fontSize: '40px',
                                    fontWeight: 'bold',
                                    fontFamily: 'Merriweather',
                                    lineHeight: '1'
                                }}
                            >
                                Background
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                The mission is to help insure and protect as many people as possible, regardless of your
                                income level.
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                After many years of experience in the insurance industry, we discovered that the problem
                                was not about who was selling it, but the Product itself.
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                It’s the Pricing system, Product features, and Usage system which caused all the pains.
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                The only solution is to create a revolutionary system which uses the original roots of
                                insurance, and make insurance painless, fair, and affordable for everyone!
                            </div>

                            <div style={{ paddingTop: '150px' }}></div>
                            <Divider></Divider>
                            <div
                                style={{
                                    margin: '100px 0px 0px 0px',
                                    fontSize: '40px',
                                    fontWeight: 'bold',
                                    fontFamily: 'Merriweather',
                                    lineHeight: '1'
                                }}
                            >
                                Join Us
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                Try out any product with us: you can always make a 2nd claim from us for the same
                                accident / incident, even after you make a similar claim from a traditional insurer.
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                Let’s make insurance painless, fair, and affordable for everyone!
                            </div>

                            <div style={{ paddingTop: '150px' }}></div>
                        </div>
                    </div>
                </NoSsr>

                <BotNavBar referrer={this.state.referrer} />
            </div>
        );
    }
}

export default withStyles(styles)(AboutUs);
