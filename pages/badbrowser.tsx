import * as React from 'react';
import styled from 'styled-components';

export interface IBadbrowserProps {}

const Badbrowser = (props: IBadbrowserProps) => {
    return (
        <Wrapper>
            <div className="container">
                <div className="support-info">
                    <h1>Oops, your browser is not supported!</h1>
                    <p>Please use one of the following browsers instead:</p>
                </div>
                <div className="supported-browsers">
                    <a href="https://www.google.com/chrome/" target="_blank">
                        <div className="browser-icon">
                            <img src="/browser/chrome.svg" />
                        </div>
                        <strong className="browser-title">Chrome</strong>
                    </a>
                    <a href="https://www.mozilla.org/firefox/new/" target="_blank">
                        <div className="browser-icon">
                            <img src="/browser/firefox.svg" />
                        </div>
                        <strong className="browser-title">Firefox</strong>
                    </a>
                    <a href="https://support.apple.com/downloads/safari" target="_blank">
                        <div className="browser-icon">
                            <img src="/browser/Safari.svg" />
                        </div>
                        <strong className="browser-title">Safari</strong>
                    </a>
                    <a href="https://www.opera.com/" target="_blank">
                        <div className="browser-icon">
                            <img src="/browser/Opera.svg" />
                        </div>
                        <strong className="browser-title">Opera</strong>
                    </a>
                    <a href="https://www.microsoft.com/edge" target="_blank">
                        <div className="browser-icon">
                            <img src="/browser/Edge.svg" />
                        </div>
                        <strong className="browser-title">Edge</strong>
                    </a>
                </div>
            </div>
        </Wrapper>
    );
};

export default Badbrowser;

const Wrapper = styled.div`
    height: 100%;
    display: flex;
    align-items: center;
    text-align: center;
    position: absolute;
    height: 100%;
    width: 100%;

    .container {
        width: 800px;
        margin: 0 auto;

        @media (max-width: 768px) {
            width: 100%;
            .support-info {
                > h1 {
                    font-size: 24px !important;
                }

                > p {
                    font-size: 16px !important;
                }
            }
        }

        .support-info {
            > h1 {
                font-size: 2.5rem;
            }

            > p {
                font-size: 1.2rem;
            }
        }

        .supported-browsers {
            display: flex;
            margin: 0 auto;
            justify-content: space-around;
            flex-direction: row;

            @media (max-width: 768px) {
                /* flex-direction: column; */
                /* flex-direction: row; */
                text-align: center;

                > a {
                    .browser-icon {
                        width: 40px !important;
                    }
                }
            }

            > a {
                text-align: center;

                .browser-icon {
                    width: 120px;
                    margin: 0 auto;
                    > img {
                    }
                }

                .browser-title {
                }
            }
        }
    }
`;
