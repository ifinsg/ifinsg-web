import React, { Component } from 'react';

// import Logo from './ifinsg_logo.png'
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import TopNavBar from './components/TopNavBar';
import BotNavBar from './components/BotNavBar';

import { NoSsr, Grid, Divider, Button } from '@material-ui/core';

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Constants from 'constants/constants';
import Head from 'next/head';
import Link from 'next/link';
import './claims.css';

const styles = {
    page_width: Constants.style.page.width,
    page_text_width: Constants.style.page.text_width,

    page_padding: Constants.style.page.padding,

    align_center: Constants.style.align.center,

    bold: Constants.style.text.bold,
    break_word: Constants.style.text.break_word,

    title1: Constants.style.text.title1,
    body1: Constants.style.text.body1,

    margin_top_xl: Constants.style.margin_top.xl,
    margin_top_lg: Constants.style.margin_top.lg,
    margin_top_md: Constants.style.margin_top.md,
    margin_top_sm: Constants.style.margin_top.sm,
    margin_top_xs: Constants.style.margin_top.xs,
    margin_top_xxs: Constants.style.margin_top.xxs
};

class Claims extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount = () => {
        window.scrollTo(0, 0);
        Constants.store_referrer(this);
    };

    render() {
        const { classes } = this.props;

        return (
            <div className="claims">
                <TopNavBar selected="claims" referrer={this.state.referrer} />

                <NextSeo config={Constants.next_seo_config('claims')} />

                <NoSsr>
                    <div className={classNames(classes.align_center)}>
                        <div className={classNames(classes.page_text_width, classes.page_padding, classes.break_word)}>
                            <div
                                style={{
                                    margin: '10px 0px',
                                    fontSize: '40px',
                                    fontFamily: 'Merriweather'
                                }}
                            >
                                {' '}
                                The engine behind Cyclic Insurance claims.{' '}
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                {' '}
                                Claims are handled by our devoted team and a set of very smart algorithms. No magic, no
                                exaggeration and no bullshit.{' '}
                            </div>

                            <div>
                                {' '}
                                <img
                                    style={{
                                        maxWidth: '300px',
                                        width: '100%',
                                        margin: '60px 0px 0px 0px',
                                        padding: '0px 0px 0px 0px'
                                    }}
                                    src={'/img/Pic-8-Claims.png'}
                                />{' '}
                            </div>

                            <div style={{ paddingTop: '150px' }}></div>

                            <Divider></Divider>

                            <div
                                style={{
                                    margin: '100px 0px 0px 0px',
                                    fontSize: '40px',
                                    lineHeight: '1',
                                    fontFamily: 'Merriweather'
                                }}
                            >
                                How do I make a Claim
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                {' '}
                                File a claim directly from your phone, 24/7 anytime and anywhere.{' '}
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                {' '}
                                Just take some Photos and Tell us what happened.{' '}
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                {' '}
                                If you need assistance, Email us, message us via Whatsapp, or Call us if you prefer to
                                talk!{' '}
                            </div>
                            <div>
                                {' '}
                                <img
                                    style={{
                                        maxWidth: '300px',
                                        width: '100%',
                                        margin: '60px 0px 0px 0px',
                                        padding: '0px 0px 0px 0px'
                                    }}
                                    src={'/img/Pic-9-Claims.png'}
                                />{' '}
                            </div>
                            <div style={{ margin: '0px 0px 0px 0px', fontSize: '20px' }}>
                                {' '}
                                Login to your account, and click on the{' '}
                                <span style={{ fontWeight: 'bold' }}>Submit Claim</span> module.{' '}
                            </div>
                            <div style={{ paddingTop: '50px' }}></div>
                            <div>
                                {' '}
                                <img
                                    style={{
                                        maxWidth: '300px',
                                        width: '100%',
                                        margin: '60px 0px 0px 0px',
                                        padding: '0px 0px 0px 0px'
                                    }}
                                    src={'/img/Pic-10-Claims.png'}
                                />{' '}
                            </div>
                            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>
                                {' '}
                                Take required Photos (such as your Receipt), and Type in what happened.{' '}
                            </div>
                            <div style={{ paddingTop: '50px' }}></div>
                            <div>
                                {' '}
                                <img
                                    style={{
                                        maxWidth: '300px',
                                        width: '100%',
                                        margin: '60px 0px 0px 0px',
                                        padding: '0px 0px 0px 0px'
                                    }}
                                    src={'/img/Pic-11-Claims.png'}
                                />{' '}
                            </div>
                            <div style={{ margin: '0px 0px 0px 0px', fontSize: '20px' }}>
                                {' '}
                                We do the Verification and Authentication of your claims as soon as we can.{' '}
                            </div>
                            <div style={{ paddingTop: '50px' }}></div>
                            <div>
                                {' '}
                                <img
                                    style={{
                                        maxWidth: '300px',
                                        width: '100%',
                                        margin: '60px 0px 0px 0px',
                                        padding: '0px 0px 0px 0px'
                                    }}
                                    src={'/img/Pic-12-Claims.png'}
                                />{' '}
                            </div>
                            <div style={{ margin: '0px 0px 0px 0px', fontSize: '20px' }}>
                                {' '}
                                Once your claim is Approved, your claim amount will be directly transferred to your
                                local Bank account.{' '}
                            </div>
                            <div style={{ margin: '30px 0px 0px 0px', fontSize: '20px' }}>
                                {' '}
                                If your claim requires more clarification before we can decide it’s Approval, we will
                                contact you ASAP through Email or your Mobile Phone, if we need more information.{' '}
                            </div>

                            <div style={{ paddingTop: '30px' }}></div>

                            <Divider style={{ margin: '100px 0px 100px 0px' }}></Divider>

                            <div
                                style={{
                                    margin: '50px 0px 0px 0px',
                                    fontSize: '40px',
                                    lineHeight: '1',
                                    fontFamily: 'Merriweather'
                                }}
                            >
                                Why Cyclic insurance Claims are Revolutionary
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                {' '}
                                Traditional insurance companies keep the money they don’t pay out in claims. This means
                                that every additional $1 paid out as Claims, translates to $1 less in underwriting
                                Profit.{' '}
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                {' '}
                                We are structured to prevent any conflict of interest, by making sure that all Premiums
                                unused for Claims, are given back to our users at the end of every cycle as{' '}
                                <span style={{ fontWeight: 'bold' }}>PremiumBack</span>. We gain nothing by delaying or
                                denying claims.{' '}
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                {' '}
                                iFinSG only earns a flat and transparent monthly{' '}
                                <span style={{ fontWeight: 'bold' }}>Admin Fee</span> per policy per user, to run a
                                Transparent, Accurate and Fair insurance system for you!{' '}
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> Simple. Fast. Cheap. </div>
                            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
                                {' '}
                                That’s exactly how you like your insurance.{' '}
                            </div>

                            <Link href={{ pathname: Constants.get_new_page('products', this.props.referrer) }}>
                                <Button
                                    variant="contained"
                                    style={{
                                        margin: '50px 0px 0px 0px',
                                        padding: '10px 20px 10px 20px',
                                        fontSize: '15px',
                                        backgroundColor: '#7cda24',
                                        color: 'white'
                                    }}
                                >
                                    What cover can i get?
                                </Button>
                            </Link>

                            <div style={{ paddingTop: '50px' }}></div>
                        </div>
                    </div>
                </NoSsr>

                <BotNavBar referrer={this.state.referrer} />
            </div>
        );
    }
}

export default withStyles(styles)(Claims);
