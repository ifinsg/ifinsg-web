import React, { Component } from 'react';
import { NoSsr, Grid, Button } from '@material-ui/core';
import Link from 'next/link';
import Constants from 'constants/constants';

const { detect } = require('detect-browser');
const browser = detect();

class BotNavBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            browser_is_ie: false
        };
    }

    componentDidMount = () => {
        if (browser) {
            if (browser.name === 'ie') {
                this.setState({
                    browser_is_ie: true
                });
            }
        }
    };

    getMarginTop = () => {
        if (this.state.browser_is_ie) {
            margin_top = '50px';
            switch (this.props.page) {
                case 'home':
                    margin_top = '50px';
                    break;
                case 'faq':
                    margin_top = '50px';
                    break;
                case 'disclaimers':
                    margin_top = '50px';
                    break;
                case 'claims':
                    margin_top = '50px';
                    break;
                case 'about-us':
                    margin_top = '50px';
                    break;
                case 'premiumback':
                    margin_top = '50px';
                    break;
                case 'resources':
                    margin_top = '50px';
                    break;
                case 'products':
                    margin_top = '50px';
                    break;
                case 'home':
                    margin_top = '50px';
                    break;
                default:
                    break;
            }
            return margin_top;
        } else {
            return '50px';
        }
    };

    render() {
        return (
            <div style={{ margin: '50px 0px 0px 0px', backgroundColor: '#272b30' }}>
                <Grid container spacing={0} alignItems="stretch">
                    <Grid item xs={12} md={6} lg={3}>
                        <div style={{ textAlign: 'center', marginTop: '50px' }}>
                            <img
                                style={{ maxWidth: '150px', width: '100%' }}
                                src={'/img/iFin-SG-Footer-White-Logo.png'}
                            />
                        </div>
                    </Grid>
                    <Grid item xs={12} md={6} lg={3}>
                        <div style={{ padding: '40px' }}>
                            <div
                                style={{
                                    margin: '0px 0px 0px 0px',
                                    color: '#7cda24',
                                    fontSize: '34px',
                                    lineHeight: '1'
                                }}
                            >
                                About www.iFinSG.com
                            </div>
                            <div style={{ color: 'white', lineHeight: '1.8', fontSize: '17px', marginTop: '15px' }}>
                                iFinSG is an InsurTech platform which makes insurance simple, and returns to users all
                                unused premiums as PremiumBack.
                            </div>
                        </div>
                    </Grid>
                    <Grid item xs={12} md={6} lg={3}>
                        <div style={{ padding: '40px' }}>
                            <div
                                style={{
                                    margin: '0px 0px 0px 0px',
                                    color: '#7cda24',
                                    fontSize: '34px',
                                    lineHeight: '1'
                                }}
                            >
                                Shortcuts
                            </div>
                            <ul
                                style={{
                                    margin: '10px 0px 0px 0px',
                                    color: '#7cda24',
                                    fontSize: '17px',
                                    lineHeight: '1.8'
                                }}
                            >
                                <li>
                                    <a href={Constants.get_new_page('premium-back', this.props.referrer)}>
                                        <span style={{ color: 'white' }}>PremiumBack</span>
                                    </a>
                                </li>
                                <li>
                                    <a href={Constants.get_new_page('products', this.props.referrer)}>
                                        <span style={{ color: 'white' }}>Sign Up for Cyclic Insurance</span>
                                    </a>
                                </li>
                                <li>
                                    <a href={Constants.get_new_page('resources', this.props.referrer)}>
                                        <span style={{ color: 'white' }}>iConsumerForms</span>
                                    </a>
                                </li>
                                <li>
                                    <a href={Constants.get_new_page('i-search-professional', this.props.referrer)}>
                                        <span style={{ color: 'white' }}>iSearchProfessional</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </Grid>
                    <Grid item xs={12} md={6} lg={3} style={{ margin: '0px 0px 30px 0px' }}>
                        <div style={{ padding: '40px' }}>
                            <div
                                style={{
                                    margin: '0px 0px 0px 0px',
                                    color: '#7cda24',
                                    fontSize: '34px',
                                    lineHeight: '1'
                                }}
                            >
                                Connect with us
                            </div>
                            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                                <a
                                    href="https://www.facebook.com/iFinSG/"
                                    target="blank"
                                    style={{ margin: '0px 5px 0px 5px' }}
                                >
                                    <svg
                                        id="icon-facebook"
                                        viewBox="0 0 24 24"
                                        width="50px"
                                        height="50px"
                                        style={{ fill: 'white' }}
                                    >
                                        <path d="M20.007,3H3.993C3.445,3,3,3.445,3,3.993v16.013C3,20.555,3.445,21,3.993,21h8.621v-6.971h-2.346v-2.717h2.346V9.31 c0-2.325,1.42-3.591,3.494-3.591c0.993,0,1.847,0.074,2.096,0.107v2.43l-1.438,0.001c-1.128,0-1.346,0.536-1.346,1.323v1.734h2.69 l-0.35,2.717h-2.34V21h4.587C20.555,21,21,20.555,21,20.007V3.993C21,3.445,20.555,3,20.007,3z"></path>
                                    </svg>
                                </a>
                                <a
                                    href="https://www.linkedin.com/company/ifinancial-singapore"
                                    target="blank"
                                    style={{ margin: '0px 5px 0px 5px' }}
                                >
                                    <svg
                                        id="icon-facebook"
                                        viewBox="0 0 24 24"
                                        width="50px"
                                        height="50px"
                                        style={{ fill: 'white' }}
                                    >
                                        <path d="M19.7,3H4.3C3.582,3,3,3.582,3,4.3v15.4C3,20.418,3.582,21,4.3,21h15.4c0.718,0,1.3-0.582,1.3-1.3V4.3 C21,3.582,20.418,3,19.7,3z M8.339,18.338H5.667v-8.59h2.672V18.338z M7.004,8.574c-0.857,0-1.549-0.694-1.549-1.548 c0-0.855,0.691-1.548,1.549-1.548c0.854,0,1.547,0.694,1.547,1.548C8.551,7.881,7.858,8.574,7.004,8.574z M18.339,18.338h-2.669 v-4.177c0-0.996-0.017-2.278-1.387-2.278c-1.389,0-1.601,1.086-1.601,2.206v4.249h-2.667v-8.59h2.559v1.174h0.037 c0.356-0.675,1.227-1.387,2.526-1.387c2.703,0,3.203,1.779,3.203,4.092V18.338z"></path>
                                    </svg>
                                </a>
                                <a
                                    href="mailto:Adele@iFinancialSingapore.com"
                                    target="blank"
                                    style={{ margin: '0px 5px 0px 5px' }}
                                >
                                    <svg
                                        id="icon-facebook"
                                        viewBox="0 0 24 24"
                                        width="50px"
                                        height="50px"
                                        style={{ fill: 'white' }}
                                    >
                                        <path d="M20,4H4C2.895,4,2,4.895,2,6v12c0,1.105,0.895,2,2,2h16c1.105,0,2-0.895,2-2V6C22,4.895,21.105,4,20,4z M20,8.236l-8,4.882 L4,8.236V6h16V8.236z"></path>
                                    </svg>
                                </a>
                            </div>
                            <div style={{ margin: '25px 0px 0px 0px', height: '50px' }}>
                                <a
                                    href="https://api.whatsapp.com/send?phone=6596898676"
                                    target="blank"
                                    style={{ color: 'white' }}
                                >
                                    <img
                                        style={{ float: 'left', maxWidth: '42px', width: '100%' }}
                                        src={'/img/icon-whatsapp.png'}
                                    />
                                    <div style={{ float: 'left', padding: '7px 0px 0px 20px', fontSize: '18px' }}>
                                        WhatsApp Live Chat
                                    </div>
                                </a>
                            </div>
                            <div style={{ margin: '25px 0px 0px 0px', color: 'white', height: '50px' }}>
                                <a href="tel:+6596898676" target="blank" style={{ color: 'white' }}>
                                    <img
                                        style={{ float: 'left', maxWidth: '41px', width: '100%' }}
                                        src={'/img/icon-phone.png'}
                                    />
                                    <div style={{ float: 'left', padding: '7px 0px 0px 20px', fontSize: '18px' }}>
                                        Call Us
                                    </div>
                                </a>
                            </div>
                            <div style={{ margin: '25px 0px 0px 0px', color: 'white', height: '50px' }}>
                                <a href="https://t.me/iFinSG_QNA" target="blank" style={{ color: 'white' }}>
                                    <img
                                        style={{ float: 'left', maxWidth: '42px', width: '100%' }}
                                        src={'/img/icon-telegram.png'}
                                    />
                                    <div style={{ float: 'left', padding: '7px 0px 0px 20px', fontSize: '18px' }}>
                                        {' '}
                                        Telegram Group Q&A
                                    </div>
                                </a>
                            </div>
                        </div>
                    </Grid>
                </Grid>
                <div style={{ textAlign: 'center', backgroundColor: '#3c3a3a', padding: '20px 15px 17px 15px' }}>
                    <Link
                        href={{ pathname: Constants.get_new_page('general-terms-and-conditions', this.props.referrer) }}
                    >
                        <Button style={{ color: 'white', padding: '5px 15px 5px 15px' }}>
                            {' '}
                            General Terms & Conditions
                        </Button>
                    </Link>
                    <Link href={{ pathname: Constants.get_new_page('terms-of-use', this.props.referrer) }}>
                        <Button style={{ color: 'white', padding: '5px 15px 5px 15px' }}> Terms of Use</Button>
                    </Link>
                    <Link href={{ pathname: Constants.get_new_page('terms-of-service', this.props.referrer) }}>
                        <Button style={{ color: 'white', padding: '5px 15px 5px 15px' }}> Terms of Service</Button>
                    </Link>
                    <Link href={{ pathname: Constants.get_new_page('disclaimer-of-liability', this.props.referrer) }}>
                        <Button style={{ color: 'white', padding: '5px 15px 5px 15px' }}>
                            {' '}
                            Disclaimer of Liability
                        </Button>
                    </Link>
                    <Link href={{ pathname: Constants.get_new_page('privacy-policy', this.props.referrer) }}>
                        <Button style={{ color: 'white', padding: '5px 15px 5px 15px' }}> Privacy Policy</Button>
                    </Link>
                </div>
                <div
                    style={{
                        textAlign: 'center',
                        color: '#757575',
                        backgroundColor: '#272b30',
                        padding: '15px 20px 15px 20px'
                    }}
                >
                    {' '}
                    Copyright © by ICH iFinancial Singapore Private Limited. UEN: 201631146G. 3D Martia Road, Singapore
                    (424786){' '}
                </div>
            </div>
        );
    }
}

export default BotNavBar;
