import React, { Component } from "react";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
// import { NoSsr, Card, CardMedia, CardContent, Button, Grid, TextField } from '@material-ui/core';
import {
    NoSsr,
    Button,
    Typography,
    AppBar,
    Tabs,
    Tab,
    TextField,
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    Card,
    CardContent,
    FormControlLabel,
    Checkbox,
    Fab,
    Divider,
    IconButton,
    Grid,
    FormLabel,
    RadioGroup,
    Radio,
    Chip
} from "@material-ui/core";

import NextSeo from "next-seo"; //Source: https://www.npmjs.com/package/next-seo
import TopNavBar from "./TopNavBar";
import BotNavBar from "./BotNavBar";
import Router from "next/router";
import axios from "axios";
import Cookies from "universal-cookie";
const cookie = new Cookies();

import GlobalConstants from "../../global/constants";
import Constants from "constants/constants";

var validator = require("email-validator"); //Source: https://www.npmjs.com/package/email-validator

var owasp = require("owasp-password-strength-test");

owasp.config({
    allowPassphrases: false,
    maxLength: 50,
    minLength: 6,
    minPhraseLength: 20,
    minOptionalTestsToPass: 4
});

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loginTab: 1, // Default display tab for login/signup

            login_email: "",
            login_password: "",

            email_error: false,
            email_error_text: ""
        };
    }

    handleLogin = () => {
        const login_email = this.state.login_email;
        const login_password = this.state.login_password;
        if (!validator.validate(login_email)) {
            alert("Error - Invalid email");
            return;
        }

        // Server login Link with DB to actually login
        this.setState({ login_button_disabled: true }, () => {
            // Send to server
            axios
                .post("/api/auth/login", {
                    login_email,
                    login_password
                })
                .then((res) => {
                    if (res.status === 200) {
                        // referrer reset
                        if (
                            res.data &&
                            res.data.user_info &&
                            res.data.user_info.stripe_details &&
                            res.data.user_info.stripe_details.id
                        ) {
                            sessionStorage.setItem(
                                "ifinsg_referrer",
                                res.data.user_info.referrer ? res.data.user_info.referrer : ""
                            );
                        }

                        if (res.data.err) {
                            alert(res.data.err);
                            this.setState({ login_button_disabled: false });
                        } else {
                            try {
                                cookie.set("ifinsg_token", res.data.token, { path: "/" });
                                localStorage.setItem("ifinsg_token", res.data.token);
                                this.props.onLogin();
                            } catch (err) {
                                alert(err);
                                this.setState({ login_button_disabled: false });
                            }
                        }
                    } else {
                        alert(res);
                        this.setState({ login_button_disabled: false });
                    }
                })
                .catch((err) => {
                    alert(err);
                    this.setState({ login_button_disabled: false });
                });
        });
    };

    handleKeyPress(target) {
        if (target.charCode == 13) {
            return this.handleLogin();
        }
    }

    handleLoginTabChange = (event, value) => {
        this.setState({ loginTab: value });
    };

    handleFieldChange = (prop) => (e) => {
        switch (prop) {
            case "email":
                this.setState({
                    email_error: false,
                    email_error_text: ""
                });
                break;
            default:
                break;
        }
        this.setState({
            [prop]: e.target.value
        });
    };

    render() {
        return (
            <div>
                <NextSeo config={Constants.next_seo_config("login")} />

                <NoSsr>
                    <form>
                        <div style={{ padding: "0px 0px 5px 0px" }}>
                            <TextField
                                label="Email"
                                autoComplete="email"
                                value={this.state.login_email}
                                onChange={this.handleFieldChange("login_email")}
                                fullWidth
                                // InputProps={{
                                //   endAdornment: <InputAdornment>@email.com</InputAdornment>,
                                // }}
                            />
                        </div>

                        <div style={{ padding: "0px 0px 2px 0px" }}>
                            <TextField
                                label="Password"
                                value={this.state.login_password}
                                onChange={this.handleFieldChange("login_password")}
                                type="password"
                                onKeyPress={(ev) => {
                                    if (ev.key === "Enter") {
                                        this.handleLogin();
                                    }
                                }}
                                fullWidth
                            />
                        </div>
                    </form>

                    <Button
                        data-track-id="forgot-password"
                        style={{ fontSize: "9px", padding: 0, marginTop: "5px" }}
                        onClick={() => {
                            Router.push("/forgot-password");
                        }}
                    >
                        {" "}
                        Forgot Password
                    </Button>
                    <Button
                        data-track-id="login"
                        style={{ float: "right", margin: "10px 0px 0px 0px" }}
                        disabled={this.state.login_button_disabled}
                        onClick={() => {
                            this.handleLogin();
                        }}
                    >
                        {" "}
                        Login{" "}
                    </Button>
                </NoSsr>
            </div>
        );
    }
}

export default Login;
