
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import CloseIcon from '@material-ui/icons/Close';

// const useStyles = makeStyles(theme => ({
//   typography: {
//     padding: theme.spacing(2),
//   },
// }));

export default function SimplePopover(props) {
  //   const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  function renderPopoverContent() {
    //   let returnable = ""

    switch (props.feature) {
      case "reasonable":
        return (
          <div style={{ padding: "30px" }}>
            <div onClick={() => { handleClose() }} style={{ float: "right" }}> <IconButton> <CloseIcon /> </IconButton> </div>
            <div style={{ margin: '10px 0px 0px 0px', fontSize: '25px', fontWeight: "bold" }}>
              13 - Can I make a claim from Cyclic insurance, after I make a claim from a traditional Insurer’s policy, for the same accident / incident? </div>
            <div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>Traditional Insurers may not allow you to make a Reimbursement claim from 2 different insurers for the same accident / incident, but we work differently at iFinSG.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>When an unfortunate accident / incident happens, you should be allowed to claim All the relevant benefits which you have already paid for, via the premiums of your policies which you have already been paying to different insurers.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If a company wants to prevent you from making more than 1 Reimbursement claim from the same accident / incident, maybe that company should prevent you from buying a 2nd policy in the first place?</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}><span style={{ fontWeight: 'bold' }}> Cyclic insurance</span> will always allow you to make a claim from us for the same accident / incident, even after you make a similar claim from a traditional insurer.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> Make a 2nd claim with us for the same Accident ( using photos of the Receipt and related documents ), even after you make a Reimbursement claim for your policy with an Insurer ( using the original Receipt and related documents ) !</div>

            </div>
          </div>
          // </div>

        )
        break;
      case "flexible":
        return (
          <div style={{ padding: "30px" }}>
            <div onClick={() => { handleClose() }} style={{ float: "right" }}> <IconButton> <CloseIcon /> </IconButton> </div>
            <div style={{ margin: '10px 0px 0px 0px', fontSize: '25px', fontWeight: "bold" }}>
              4 - Can I purchase just 1 cycle of your Cyclic insurance to try it out?
                </div>
            <div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> Of course you can. 1 cycle of <span style={{ fontWeight: "bold" }}>Cyclic insurance</span> is currently defined as 3 months.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> This will eventually be converted to 6 months or 12 months, and we will inform our users before we administer any changes.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> Premiums unused for claims, are given back to our users at the end of every cycle as <span style={{ fontWeight: "bold" }}>PremiumBack.</span></div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> Your <span style={{ fontWeight: "bold" }}>Cyclic insurance</span> will be renewed at the end of each cycle.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> If your policy starts in the midst of our cycle, you only pay a pro-rated premium plus a pro-rated <span style={{ fontWeight: "bold" }}>Admin Fee.</span></div>
            </div>
          </div>
        )
        break;
      case "unshakeable":
        return (
          <div style={{ padding: "30px" }}>
            <div onClick={() => { handleClose() }} style={{ float: "right" }}> <IconButton> <CloseIcon /> </IconButton> </div>
            <div style={{ margin: '10px 0px 0px 0px', fontSize: '25px', fontWeight: "bold" }}>
              3 - How is iFinSG different from a traditional insurance company?
                    </div>

            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> iFinSG is an InsurTech firm which runs a P2P insurance system that focuses on the welfare of users, not the profit of shareholders. </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> Traditional insurance companies keep the money they don’t pay out in claims. This means that every additional $1 paid out as Claims, translates to $1 less in underwriting Profit. </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> We are structured to prevent any conflict of interest, by making sure that all premiums unused for claims, are given back to our users at the end of every cycle as <span style={{ fontWeight: "bold" }}>PremiumBack</span>. We gain nothing by delaying or denying claims. </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> <span style={{ fontWeight: "bold" }}>PremiumBack</span> is given to 2 types of users: users who make 0 claims, and users who make low claims. </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> Our proprietary algorithm is structured so that the lower the total claims you make, the higher the <span style={{ fontWeight: "bold" }}>PremiumBack</span> you will qualify for each cycle. If you made 0 claims, you’ll get the highest level of <span style={{ fontWeight: "bold" }}>PremiumBack</span>. </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> As paying claims is our core priority, occasionally when premiums for a policy are fully paid out as claims, users may receive less <span style={{ fontWeight: "bold" }}>PremiumBack</span> than normal, however at all times do be assured that: </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> A user who made 0 claims will always receive a <span style={{ fontWeight: "bold" }}>PremiumBack</span>, <u>guaranteed to be at least 10% of your premiums placed with us.</u> </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> A user who made low claims will always receive a <span style={{ fontWeight: "bold" }}>PremiumBack</span>, <u>guaranteed to be at least 5% of your premiums placed with us.</u> </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> You enjoy amazing protection with us (nobody would try to deny your claim), everything is based on real claims data, and there’s always money coming back to you! </div>

          </div>
          // </div>

        )
        break;
      case "loveable":
        return (
          <div style={{ padding: "30px" }}>
            <div onClick={() => { handleClose() }} style={{ float: "right" }}> <IconButton> <CloseIcon /> </IconButton> </div>
            <div style={{ margin: '10px 0px 0px 0px', fontSize: '25px', fontWeight: "bold" }}>
              7 - What PremiumBack amount would I get, if no user makes a claim in a particular cycle?
                </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> If no user makes a claim in a particular cycle, you would get back all your premiums for the cycle as <span style={{ fontWeight: "bold" }}>PremiumBack</span>.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Eg. For the Small Accident cover, your monthly premium would be $6.80, while the monthly Admin Fee is $3. If you participated for a full cycle, the total premium would be $6.80 x 3 months = $20.40.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If no user makes a claim in a particular cycle, you would thus receive a <span style={{ fontWeight: "bold" }}>PremiumBack</span> of $20.40.</div>

          </div>

        )
        break;
      case "manageable":
        return (
          <div style={{ padding: "30px" }}>
            <div onClick={() => { handleClose() }} style={{ float: "right" }}> <IconButton> <CloseIcon /> </IconButton> </div>
            <div style={{ margin: '10px 0px 0px 0px', fontSize: '25px', fontWeight: "bold" }}>
              30 - How do I re-continue my cover if it has Lapsed or Stopped?
            </div>

            <div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>You would be delighted to know that you can Login to your <span style={{ fontWeight: 'bold' }}>Profile</span> page, and use just 1 button to easily re-continue your cover with us. </div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> We’ll bill your credit / debit card after the Reinstatement of your cover is approved, based on the new starting date of your cover. </div>
            </div>

          </div>
        )
        break;
      default:
        // Should not happen
        break;
    }
    //   returnable = 
    //   <div style={{padding:"30px"}}> 
    //       YOOOO
    // {props.feature}
    //   </div>
    //   return returnable
  }

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
    <span>
      {/* <Button aria-describedby={id} variant="contained" onClick={handleClick}>
        Open Popover
      </Button> */}



      {/* <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>Traditional Insurers may not allow you to make a Reimbursement claim from 2 different insurers for the same accident / incident, but we work differently at iFinSG.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>When an unfortunate accident / incident happens, you should be allowed to claim All the relevant benefits which you have already paid for, via the premiums of your policies which you have already been paying to different insurers.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If a company wants to prevent you from making more than 1 Reimbursement claim from the same accident / incident, maybe that company should prevent you from buying a 2nd policy in the first place?</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}><span style={{ fontWeight: 'bold' }}> Cyclic insurance</span> will always allow you to make a claim from us for the same accident / incident, even after you make a similar claim from a traditional insurer.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> Make a 2nd claim with us for the same Accident ( using photos of the Receipt and related documents ), even after you make a Reimbursement claim for your policy with an Insurer ( using the original Receipt and related documents ) !</div>


          </div>
           </div>} > */}
      <IconButton onClick={handleClick} style={{ margin: "0px 0px 5px 0px", padding: "0px 7px 0px 7px", fontSize: "12px", fontWeight: 600, backgroundColor: "grey", color: "white", opacity: 0.7 }}>i</IconButton>

      <div> hii</div>

      <Modal
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        style={{ maxWidth: "90%" }}
      >
        {renderPopoverContent()}
      </Modal>
    </span>
  );
}