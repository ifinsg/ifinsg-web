import React, { Component } from 'react';
import { NoSsr, Button, Typography, AppBar, Tabs, Tab, TextField, FormControl, InputLabel, MenuItem, Select, Card, FormControlLabel, Checkbox, Fab, Divider, IconButton, Grid, FormLabel, RadioGroup, Radio, Chip } from '@material-ui/core';
import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

// import GlobalConstants from '../global/constants';
import Helpers from 'global/helpers';
import Constants from 'constants/constants';
import GlobalConstants from 'global/constants';
import DatePickerRMC from 'components/DatePickerRMC/DatePickerRMC'    // Adapted from: https://github.com/react-component/m-date-picker
import ImageUpload from './widgets/ImageUpload'

import SelectOccupation from './selects/SelectOccupation'
import SelectCountry from './selects/SelectCountry'

class SignupPayerDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            personal_full_name: "",
            personal_preferred_name: "",
            personal_NRIC_number: "",
            personal_gender: "",
            // personal_dob: new Date("1991-01-01").getFullYear() + "-" + (parseInt(new Date("1991-01-01").getMonth()) + 1) + "-" + new Date("1991-01-01").getDate(),
            personal_dob: "",

            personal_place_of_birth: "",

            personal_mobile_number: "",
            personal_occupation: "",
            personal_self_employed: false,
            personal_uploaded_images: [],
        };
    }

    handleFieldChange = (prop) => e => {
        this.setState({
            [prop]: e.target.value
        });
    }



    renderPersonalFilledDetails = () => {
        let renderable = []

        const personal_full_name =
            <Grid item xs={5} style={{ textAlign: 'left', padding: "0px 0px 0px 4px" }}>
                <Typography style={{ fontSize: "13px", color: "grey" }}>{this.props.personal_full_name}</Typography>
            </Grid>
        const preferred_name =
            <Grid item xs={5} style={{ textAlign: 'left', padding: "0px 0px 0px 4px" }}>
                <Typography style={{ fontSize: "13px", color: "grey" }}>{this.personal_preferred_name}</Typography>
            </Grid>
        const personal_NRIC_number =
            <Grid item xs={5} style={{ textAlign: 'left', padding: "0px 0px 0px 4px" }}>
                <Typography style={{ fontSize: "13px", color: "grey" }}>{this.personal_NRIC_number}</Typography>
            </Grid>
        const personal_gender = () => {
            let gender = ""
            switch (this.props.personal_gender) {
                case "M":
                    return (
                        <Grid item xs={5} style={{ textAlign: 'left', padding: "0px 0px 0px 4px" }}>
                            <Typography style={{ fontSize: "13px", color: "grey" }}>Male</Typography>
                        </Grid>
                    )
                case "F":
                    return (
                        <Grid item xs={5} style={{ textAlign: 'left', padding: "0px 0px 0px 4px" }}>
                            <Typography style={{ fontSize: "13px", color: "grey" }}>Female</Typography>
                        </Grid>
                    )
                default:
                    // Do nothing
                    break
            }

        }

        const personal_dob = () => {
            if (this.props.personal_dob) {

                // const dob_year = this.props.personal_dob.split('-')[0]
                // const dob_month = this.props.personal_dob.split('-')[1]
                // const dob_day = this.props.personal_dob.split('-')[2]
                // const display_dob = dob_day + '-' + dob_month + '-' + dob_year

                const date_obj = this.props.personal_dob
                const year = Helpers.padNumber(date_obj.getFullYear(), 4)
                const month = Helpers.padNumber(date_obj.getMonth() + 1, 2)
                const date = Helpers.padNumber(date_obj.getDate(), 2)
                const display_dob = date + '-' + month + '-' + year

                return (
                    <Grid item xs={5} style={{ textAlign: 'left', padding: "0px 0px 0px 4px" }}>
                        <Typography style={{ fontSize: "13px", color: "grey" }}>{display_dob}</Typography>
                    </Grid>
                )
            }
        }

        const personal_place_of_birth =
            <Grid item xs={5} style={{ textAlign: 'left', padding: "0px 0px 0px 4px" }}>
                <Typography style={{ fontSize: "13px", color: "grey" }}>{this.props.personal_place_of_birth ? (this.getSelectValue(this.props.personal_place_of_birth)) : ""}</Typography>
            </Grid>
        const personal_mobile_number =
            <Grid item xs={5} style={{ textAlign: 'left', padding: "0px 0px 0px 4px" }}>
                <Typography style={{ fontSize: "13px", color: "grey" }}>{this.props.personal_mobile_number}</Typography>
            </Grid>
        const personal_occupation =
            <Grid item xs={5} style={{ textAlign: 'left', padding: "0px 0px 0px 4px" }}>
                <Typography style={{ fontSize: "13px", color: "grey" }}>{this.props.personal_occupation ? this.getSelectValue(this.props.personal_occupation) + (this.props.personal_self_employed ? ' (self-employed)' : ' (employed)') : ""}  </Typography>
            </Grid>


        const personal_uploaded_images = () => {
            let personal_uploaded_images_props = this.props.personal_uploaded_images
            let personal_uploaded_images = ""

            for (let i = 0; i < personal_uploaded_images_props.length; i++) {
                if (i !== 0) { personal_uploaded_images = personal_uploaded_images + ", " }
                // personal_uploaded_images = personal_uploaded_images + Helpers.showPreview(personal_uploaded_images_props[i].split("_")[2],20)
                personal_uploaded_images = personal_uploaded_images + (personal_uploaded_images_props[i].split("_")[2])

            }

            return (
                <Grid item xs={5} style={{ textAlign: 'left', padding: "0px 0px 0px 4px" }}>
                    <Typography style={{ fontSize: "13px", color: "grey" }}>{personal_uploaded_images}</Typography>
                </Grid>
            )
        }
        const personal_bank =
            <Grid item xs={5} style={{ textAlign: 'left', padding: "0px 0px 0px 4px" }}>
                <Typography style={{ fontSize: "13px", color: "grey" }}>{Constants.bank_full_name_from_short_name[this.props.personal_bank]}</Typography>
            </Grid>
        const personal_bank_acct_number =
            <Grid item xs={5} style={{ textAlign: 'left', padding: "0px 0px 0px 4px" }}>
                <Typography style={{ fontSize: "13px", color: "grey" }}>{this.props.personal_bank_acct_number}</Typography>
            </Grid>


        if (this.props.personal_info_already_in_db) {
            renderable = [personal_full_name, preferred_name, personal_NRIC_number, personal_gender(), personal_dob(), personal_place_of_birth, personal_mobile_number, personal_occupation, personal_bank, personal_bank_acct_number]
        } else {
            renderable = [personal_full_name, preferred_name, personal_NRIC_number, personal_gender(), personal_dob(), personal_place_of_birth, personal_mobile_number, personal_occupation, personal_uploaded_images(), personal_bank, personal_bank_acct_number]
        }

        return (
            <Grid container spacing={8} style={{ padding: "4px 0px 0px 0px", lineHeight: 0.5 }} >
                {renderable}
            </Grid>
        )
    }






    renderPersonalInfoFields = () => {

        let arrayOfPersonalInfoFields = []

        const full_name =
            <TextField
                label="Full Name in English (NRIC/Passport)"
                value={this.props.personal_full_name}
                onChange={this.props.handleFieldChange('personal_full_name')}
                fullWidth
            />

        const preferred_name =
            <TextField style={{ margin: "5px 0px 0px 0px" }}
                label="Preferred Name"
                value={this.personal_preferred_name}
                onChange={this.handleFieldChange('personal_preferred_name')}
                fullWidth
            />


        const NRIC_number =
            <TextField style={{ margin: "5px 0px 0px 0px" }}
                label="NRIC/Passport number"
                value={this.props.personal_NRIC_number}
                onChange={this.handleFieldChange('personal_NRIC_number')}
                fullWidth
            />

        const gender =
            <div style={{ marginTop: "20px", textAlign: "center" }}>
                {/* <div style={{ textAlign: "center" }}> <Typography variant="subtitle1" style={{ margin: "0px 0px 0px 0px" }}> Gender </Typography>  </div> */}
                <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="personal_gender" checked={this.props.personal_gender === "M"} onChange={() => { this.props.setProps({ personal_gender: "M" }) }} />} label="Male" />
                <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="personal_gender" checked={this.props.personal_gender === "F"} onChange={() => { this.props.setProps({ personal_gender: "F" }) }} />} label="Female" />
            </div>

        // const gender =
        //   <FormControl style={{ width: "100%", marginBottom: "20px" }}>
        //     <InputLabel>Gender</InputLabel>
        //     <Select
        //       value={this.props.personal_gender}
        //       onChange={this.handleFieldChange("personal_gender")}
        //     >
        //       <MenuItem value={"M"}>Male</MenuItem>
        //       <MenuItem value={"F"}>Female</MenuItem>
        //     </Select>
        //   </FormControl>



        const DOB =

            <DatePickerRMC
                date={this.props.personal_dob}
                handleDateChange={this.props.handleDateChange("personal_dob")}
                placeholder="Date of Birth"
                defaultDate="1992-01-01"  // default = today
                minDate="1850-01-01"
            // maxDate = "1993-01-01" // default = today
            />

        // <form style={{ display: 'flex', flexWrap: 'wrap' }} noValidate style={{ marginTop: "2px", marginBottom: "0px" }}
        // >
        //   <TextField
        //     id="date"
        //     label="Date of Birth"
        //     type="date"
        //     onChange={this.handleFieldChange("personal_dob")}
        //     value={this.props.personal_dob}
        //     style={{ marginTop: "1px", marginBottom: "0px" }}
        //     fullWidth
        //     // error={this.props.errors_numbers.paymentDate.error}
        //     // helperText={this.props.errors_numbers.paymentDate.errorText}
        //     // defaultDate={ new Date("1991-01-01")}
        //     defaultDate={"1991-01-01"}

        //     InputLabelProps={{
        //       shrink: true,
        //     }}
        //   />
        // </form>

        // const place_of_birth =
        //   <TextField style={{ margin: "5px 0px 0px 0px" }}
        //     label="Place of Birth (as of NRIC/Passport)"
        //     value={this.props.personal_place_of_birth}
        //     onChange={this.handleFieldChange('personal_place_of_birth')}
        //     fullWidth
        //   />

        const place_of_birth =
            <SelectCountry country={this.props.personal_place_of_birth} handleCountryFieldChange={this.handlePlaceOfBirthFieldChange} type="personal" placeholder="Place of Birth (as of NRIC/Passport)"></SelectCountry>




        const HP_number =
            <TextField style={{ margin: "5px 0px 0px 0px" }}
                label="Mobile Number"
                value={this.props.personal_mobile_number}
                onChange={this.handleFieldChange('personal_mobile_number')}
                fullWidth
            // type="number"
            />

        const occupation =
            <SelectOccupation occupation={this.props.personal_occupation} handleOccupationFieldChange={this.handleOccupationFieldChange} type="personal"></SelectOccupation>


        const self_employed =
            <div style={{ marginTop: "20px", textAlign: "center" }}>
                {/* <div style={{ textAlign: "center" }}> <Typography variant="subtitle1" style={{ margin: "30px 0px 0px 0px" }}> Self-employed? </Typography>  </div> */}

                {/* <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="personal_employed_or_self_employed" checked={true} />} label="Employed" />
        <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="personal_employed_or_self_employed" />} label="Self-employed" /> */}
                <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="personal_self_employed" checked={!this.props.personal_self_employed} onChange={() => { this.props.setProps({ personal_self_employed: false }) }} />} label="Employed" />
                <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="personal_self_employed" checked={this.props.personal_self_employed} onChange={() => { this.props.setProps({ personal_self_employed: true }) }} />} label="Self-employed" />
            </div>



        const bank_details =
            <div>
                <div style={{ textAlign: "center" }}> <Typography variant="subtitle1" style={{ margin: "20px 0px 0px 0px" }}> Payer's Bank Account </Typography>  </div>
                <FormControl style={{ width: "100%", marginTop: "0px" }}>
                    <InputLabel>Bank</InputLabel>
                    <Select
                        value={this.props.personal_bank}
                        onChange={this.handleFieldChange("personal_bank")}
                    >
                        <MenuItem value={"dbs"}>DBS/POSB (preferred)</MenuItem>
                        <MenuItem value={"ocbc"}>OCBC</MenuItem>
                        <MenuItem value={"uob"}>UOB</MenuItem>
                        <MenuItem value={"sc"}>Standard Charted</MenuItem>
                        <MenuItem value={"hsbc"}>HSBC </MenuItem>
                        <MenuItem value={"citi"}>Citibank</MenuItem>
                        <MenuItem value={"maybank"}>Maybank</MenuItem>
                        <MenuItem value={"rhb"}>RHB</MenuItem>
                        <MenuItem value={"cimb"}>CIMB</MenuItem>
                        <MenuItem value={"bo_china"}>Bank of China</MenuItem>
                        <MenuItem value={"deutsche"}> Deutsche Bank </MenuItem>
                        <MenuItem value={"hl_bank"}> HL Bank</MenuItem>
                        <MenuItem value={"bnp_paribas"}> BNP Paribas</MenuItem>
                        <MenuItem value={"far_eastern"}>  Far Eastern Bank</MenuItem>
                        <MenuItem value={"mizuho"}> Mizuho Bank</MenuItem>
                        <MenuItem value={"sumitomo_mitsui"}>Sumitomo Mitsui Banking Corporation </MenuItem>
                        <MenuItem value={"bo_tokyo_mitsubishi_ufj"}> The Bank of Tokyo-Mitsubishi UFJ</MenuItem>
                        <MenuItem value={"bo_scotland"}> The Royal Bank of Scotland</MenuItem>
                        <MenuItem value={"aus_nz_banking_grp"}> Australia and New Zealand Banking Group</MenuItem>


                    </Select>
                </FormControl>

                <TextField style={{ margin: "0px 0px 0px 0px" }}
                    label="Bank account number"
                    value={this.props.personal_bank_acct_number}
                    onChange={this.handleFieldChange('personal_bank_acct_number')}
                    fullWidth
                // type="number"
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={this.props.personal_ack_50_cents_fee}
                            onChange={this.props.handleCheckboxToggle('personal_ack_50_cents_fee')}
                            value="ack_50_cents_fee"
                            color="primary"
                        />
                    }
                    // label={<span style={{ color: "rgba(0, 0, 0, 0.54)", fontSize: "13px", lineHeight: 0 }}>I acknowledge that a fee of $0.50 will be imposed on all Premiumback & Claims payouts. FREE for payouts to DBS/POSB accounts</span>}
                    label={<span style={{ color: "rgba(0, 0, 0, 0.54)", fontSize: "13px", lineHeight: 0 }}>I am aware that if the Singapore bank account I provide is not a POSB/DBS bank account, the bank will impose a 50 cents charge to every transaction ( Claims and PremiumBack ) I receive from www.iFinSG.com.</span>}

                    style={{ margin: "30px 0px 0px 0px" }}
                />
            </div>


        const upload_image =
            <div>
                <div style={{ textAlign: "center" }}> <Typography variant="subtitle1" style={{ margin: "30px 0px 10px 0px" }}>Upload NRIC/Passport (front & back): </Typography>  </div>

                {/* NOTE TO DEV for ImageUpload: add in username={this.props.username} AND image_type="SIGNUP-IMAGE" */}
                <ImageUpload maxFiles={3} type={"personal"} fileUploadedPersonal={this.fileUploadedPersonal} fileRemovedPersonal={this.fileRemovedPersonal} />

            </div>



        arrayOfPersonalInfoFields = [full_name, preferred_name, NRIC_number, gender, DOB, place_of_birth, HP_number, occupation, self_employed, upload_image, bank_details, <Button style={{ float: "right", margin: "20px 0px 0px 0px" }} onClick={() => { this.handleNextAfterPersonalInfo() }}> Next</Button>]


        return (
            arrayOfPersonalInfoFields
        )
    }







    render() {
        return (
            <table style={this.props.style_personal_info}>
                <tbody>
                    <tr >
                        <ExpansionPanel disabled={this.props.expansion_panel_personal_info_disabled} expanded={this.props.expanded_personal_info} onChange={(event, expanded) => {
                            if (expanded) { this.props.configureExpansionPanels(['personal_info_expand']) } else { this.props.configureExpansionPanels(['personal_info_collapse']) }
                        }}>
                            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                <div style={{ width: "100%" }}>
                                    <Typography style={{ fontSize: "15px" }}>Payer Details</Typography>
                                    {this.renderPersonalFilledDetails()}
                                </div>
                            </ExpansionPanelSummary>
                            <ExpansionPanelDetails>
                                <div style={{ width: "100%" }}>
                                    <div style={{ padding: "0px 20px 5px 20px" }}>
                                        {this.renderPersonalInfoFields()}
                                    </div>
                                </div>
                            </ExpansionPanelDetails>
                        </ExpansionPanel>

                    </tr>
                </tbody>
            </table>
        );
    }
}


export default SignupPayerDetails;