import React, { Component } from "react";

import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";

import {
    withWidth,
    NoSsr,
    Button,
    Typography,
    IconButton,
    AppBar,
    Toolbar,
    Menu,
    MenuItem,
    Divider,
    ClickAwayListener
} from "@material-ui/core";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";
import axios from "axios";

import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary } from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MenuIcon from "@material-ui/icons/Menu";
import AvatarIcon from "@material-ui/icons/AccountCircle";
// import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";

import Helpers from "global/helpers";

import CardMedia from "@material-ui/core/CardMedia";
import Hidden from "@material-ui/core/Hidden";
import Link from "next/link";
import Head from "next/head";
import NextSeo from "next-seo"; //Source: https://www.npmjs.com/package/next-seo

import Router from "next/router";

import GlobalConstants from "global/constants";
import Constants from "constants/constants";
import ScrollToTop from "./widgets/ScrollToTop";
import Cookies from "universal-cookie";
const cookie = new Cookies();
import "./TopNavBar.css";

const styles = {
    button: { color: Constants.color.grey, justifyContent: "left", minWidth: "44px" },
    button_menu_expanded: { color: Constants.color.grey, justifyContent: "left", minWidth: "44px", width: "100%" },
    button_selected: { color: Constants.color.green },

    page_width: Constants.style.page.width,
    page_padding: Constants.style.page.padding,

    full_width: Constants.style.align.full_width,

    align_center: Constants.style.align.center,
    align_left: Constants.style.align.left,

    bold: Constants.style.text.bold,

    title1: Constants.style.text.title1,
    body1: Constants.style.text.body1,

    margin_top_xl: Constants.style.margin_top.xl,
    margin_top_lg: Constants.style.margin_top.lg,
    margin_top_md: Constants.style.margin_top.md,
    margin_top_sm: Constants.style.margin_top.sm,
    margin_top_xs: Constants.style.margin_top.xs,
    margin_top_xxs: Constants.style.margin_top.xxs
};

class TopNavBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            anchorEl: null,
            expanded_menu: false,
            render_expand_icon: false,
            user_info: false
        };
    }

    componentWillReceiveProps = (nextProps) => {
        // console.log("componentWillReceivePropscomponentWillReceivePropscomponentWillReceivePropscomponentWillReceivePropscomponentWillReceivePropscomponentWillReceiveProps")

        if (this.props.width != nextProps.width) {
            console.log(nextProps.width);
            this.changeWhetherMenuButtonIsShown(nextProps.width);
        }

        if (this.props.username !== nextProps.username) {
            console.log("USERNAME IS CHANGEDDD");
        }
    };

    changeWhetherMenuButtonIsShown = (width) => {
        let render_expand_icon = false;

        switch (width) {
            case "xs":
                render_expand_icon = true;
                break;
            case "sm":
                render_expand_icon = true;
                break;
            case "md":
                break;
            default:
                break;
        }

        if (this.state.render_expand_icon === true && render_expand_icon === false) {
            // If we are taking away the menu icon, collapse expandable as well

            this.setState({
                render_expand_icon,
                expanded_menu: false
            });
        } else {
            this.setState({
                render_expand_icon
            });
        }
    };

    componentDidUpdate = (nextProps) => {
        if (this.props.homepageVariant != nextProps.homepageVariant) {
            console.log("homepageVariant CHANGED to " + nextProps.homepageVariant);
        } else if (this.props.title != nextProps.title) {
            console.log("homepageVariant CHANGED to " + nextProps.title);
        }
    };

    getUserPolicies = () => {
        return new Promise((resolve, reject) => {
            try {
                const token = localStorage.getItem("ifinsg_token");

                if (token) {
                    axios
                        .post("/api/auth/getUserDataFromToken", {
                            token
                        })
                        .then((res) => {
                            if (
                                res.data.user_info.policies.stripe_details &&
                                res.data.user_info.policies.stripe_details.id
                            ) {
                                sessionStorage.setItem("ifinsg_referrer", res.data.user_info.referrer);
                            }

                            if (res.status === 200) {
                                if (res.data.err) {
                                    console.log(res.data.err); // Eg: ERROR - Email already in use
                                    resolve();
                                    // alert(res.data.err)     // Eg: ERROR - Email already in use
                                } else {
                                    try {
                                        // console.log(res.data)
                                        this.setState(
                                            {
                                                user_info: res.data.user_info
                                            },
                                            () => {
                                                console.log(this.state);
                                                if (this.props.getUserInfo) {
                                                    this.props.getUserInfo(this.state.user_info);
                                                }
                                                resolve();
                                            }
                                        );
                                    } catch (err) {
                                        console.log(err);
                                        console.log("Unexpected params received from api/auth/getUserDataFromToken");
                                        resolve();
                                    }
                                }
                            } else {
                                console.log("ERROR - ", res);
                                resolve();
                            }
                        })
                        .catch((err) => {
                            console.log(err);
                            resolve();
                        });
                } else {
                    resolve();
                }
            } catch (err) {
                console.log(err);
                resolve();
            }
        });
    };

    componentDidMount = () => {
        this.changeWhetherMenuButtonIsShown(this.props.width);
        this.getUserPolicies();
        let path = location.pathname.split("/").splice(-1)[0];
        if (GlobalConstants.list_of_affiliates.includes(path)) {
            sessionStorage.setItem("ifinsg_referrer", path);
        }
    };

    handleAccountButtonClicked = (event) => {
        console.log("handleAccountButtonClicked()");
        console.log(event.currentTarget);

        if (this.state.anchorEl !== null) {
            this.setState({ anchorEl: null });
        } else {
            this.setState({ anchorEl: event.currentTarget });
        }
    };

    handleClose = () => {
        this.setState({ anchorEl: null });
    };

    handleMenuItemClicked = (clicked) => {
        let page = Constants.get_new_page("home", this.props.referrer);

        switch (clicked) {
            case "account":
                page = Constants.get_new_page("profile", this.props.referrer);
                Router.push(page);
                break;
            case "logout":
                localStorage.removeItem("ifinsg_token");
                page = Constants.get_new_page("login", this.props.referrer);
                Router.push(page);
                break;
            case "login":
                page = Constants.get_new_page("login", this.props.referrer);
                Router.push(page);
                break;
            default:
                break;
        }

        this.handleClose();
    };

    renderTopNavBar = (expanded_menu) => {
        const { classes } = this.props;

        let renderable = [];

        let button_style = { justifyContent: "left", minWidth: "44px" };
        if (expanded_menu === "expanded_menu") {
            button_style = { justifyContent: "left", minWidth: "44px", width: "100%" };
        }

        const get_button_base_style = () => {
            if (expanded_menu === "expanded_menu") {
                return classes.button_menu_expanded;
            } else {
                return classes.button;
            }
        };

        const get_style_if_selected = (selected) => {
            if (this.props.selected === selected) {
                return classes.button_selected;
            }
        };

        const home = (
            <Link key={"index"} href="/">
                <Button className={classNames(get_button_base_style(), get_style_if_selected("home"))}> Home </Button>
            </Link>
        );

        const FAQ = (
            <Link key={"faq"} href="/faq">
                <Button className={classNames(get_button_base_style(), get_style_if_selected("faq"))}> FAQ </Button>
            </Link>
        );

        const disclaimers = (
            <Link key={"disclaimers"} href="/disclaimers">
                <Button className={classNames(get_button_base_style(), get_style_if_selected("disclaimers"))}>
                    {" "}
                    Disclaimers{" "}
                </Button>
            </Link>
        );

        const aboutUs = (
            <Link key={"about-us"} href="/about-us">
                <Button className={classNames(get_button_base_style(), get_style_if_selected("about-us"))}>
                    {" "}
                    About us{" "}
                </Button>
            </Link>
        );

        const claims = (
            <Link key={"claims"} href="/claims">
                <Button className={classNames(get_button_base_style(), get_style_if_selected("claims"))}>
                    {" "}
                    Claims{" "}
                </Button>
            </Link>
        );
        const premiumback = (
            <Link key={"premium-back"} href="/premium-back">
                <Button className={classNames(get_button_base_style(), get_style_if_selected("premiumback"))}>
                    {" "}
                    PremiumBack{" "}
                </Button>
            </Link>
        );
        const resources = (
            <Link key={"resources"} href="/resources">
                <Button className={classNames(get_button_base_style(), get_style_if_selected("resources"))}>
                    {" "}
                    Resources{" "}
                </Button>
            </Link>
        );

        const products = (
            <Link key={"products"} href="/products">
                <Button
                    onClick={() => {
                        if (this.props.homepageVariant && this.props.title) {
                            axios
                                .post("/api/trackingData", {
                                    tracking_data: {
                                        homepage_variant: this.props.homepageVariant,
                                        title: this.props.title,
                                        button: "menu_bar_products"
                                    }
                                })
                                .catch((err) => {
                                    console.log(err);
                                });
                        }
                    }}
                    className={classNames(get_button_base_style(), get_style_if_selected("products"))}
                >
                    {" "}
                    Products{" "}
                </Button>
            </Link>
        );
        const login = (
            <Link key={"login"} href="/login">
                <Button className={classNames(get_button_base_style(), get_style_if_selected("login"))}> Login </Button>
            </Link>
        );

        const shopping_cart_button = (
            <Link key={"products"} href="/products">
                {/* <Button className={classNames(get_button_base_style(), get_style_if_selected("login"))} > Login </Button> */}
                <IconButton
                    className={classNames(get_style_if_selected("products"))}
                    style={{ float: "right", marginRight: "2px" }}
                    onClick={() => {
                        if (this.props.homepageVariant && this.props.title) {
                            axios
                                .post("/api/trackingData", {
                                    tracking_data: {
                                        homepage_variant: this.props.homepageVariant,
                                        title: this.props.title,
                                        button: "shopping_cart_icon"
                                    }
                                })
                                .catch((err) => {
                                    console.log(err);
                                });
                        }
                    }}
                >
                    {" "}
                    <ShoppingCartIcon />{" "}
                </IconButton>
            </Link>
        );

        const account_button = () => {
            let account_button_container_style = { float: "right" };
            let account_button_style = { fontSize: "10px", color: Constants.color.grey };

            let account_button_color = "darkgrey";
            let logged_in = false;

            let render_menu_items = () => {
                let renderable = [];
                const menu_item_user_dashboard = (
                    <MenuItem
                        key={"menu_item_user_dashboard"}
                        onClick={() => {
                            this.handleMenuItemClicked("account");
                        }}
                    >
                        Profile
                    </MenuItem>
                );
                const menu_item_logout = (
                    <MenuItem
                        key={"menu_item_logout"}
                        onClick={() => {
                            this.handleMenuItemClicked("logout");
                        }}
                    >
                        Logout
                    </MenuItem>
                );
                const menu_item_login = (
                    <MenuItem
                        key={"menu_item_login"}
                        onClick={() => {
                            this.handleMenuItemClicked("login");
                        }}
                    >
                        Login
                    </MenuItem>
                );
                if (logged_in) {
                    // With green background, white text
                    const menu_item_logged_in_as = (
                        <MenuItem
                            key={"menu_item_logged_in_as"}
                            disabled
                            style={{
                                height: "50px",
                                wordBreak: "break-word",
                                textAlign: "center",
                                backgroundColor: Constants.color.darkgreen,
                                opacity: 0.8,
                                // backgroundColor:"rgb(34,139,34)",
                                color: "white"
                            }}
                        >
                            {" "}
                            <div style={{ lineHeight: 1.2, textAlign: "center", width: "100%" }}>
                                <div style={{ fontSize: "12px" }}>logged in as: </div>{" "}
                                <div style={{ fontSize: "15px", textAlign: "center", marginTop: "5px" }}>
                                    {Helpers.showPreview(
                                        this.props.username
                                            ? this.props.username.split("@")[0]
                                            : this.state.user_info.username.split("@")[0],
                                        20
                                    )}
                                </div>
                            </div>{" "}
                        </MenuItem>
                    );

                    renderable = [menu_item_logged_in_as, menu_item_user_dashboard, menu_item_logout];
                } else {
                    renderable = [menu_item_login];
                }
                return renderable;
            };

            try {
                if (this.props.username || (this.state.user_info && this.state.user_info.username)) {
                    // If logged in
                    account_button_color = Constants.color.green;
                    logged_in = true;
                } else {
                    logged_in = false;
                }

                return (
                    <div key="account_button_key" style={account_button_container_style}>
                        {/* <Button
              aria-owns={this.state.anchorEl ? 'simple-menu' : undefined}
              aria-haspopup="true"
              onClick={this.handleAccountButtonClicked}
              style={{ minWidth: "44px", marginTop: "6px" }} >

              <AvatarIcon style={{ color: account_button_color }} />
              <span style={account_button_style}>{Helpers.showPreview(this.state.user_info.username.split('@')[0], 20)} </span>
            </Button> */}

                        <IconButton onClick={this.handleAccountButtonClicked}>
                            <AvatarIcon style={{ color: account_button_color }} />
                        </IconButton>

                        <div className="dddd" style={{ float: "right" }}>
                            <Menu
                                id="simple-menu"
                                anchorEl={this.state.anchorEl}
                                open={Boolean(this.state.anchorEl)}
                                onClose={this.handleClose}
                                style={{
                                    transform: "translate(0px,50px)",
                                    float: "right"
                                }}
                            >
                                {render_menu_items()}
                            </Menu>
                        </div>
                    </div>
                );
            } catch (err) {
                console.log(err);
                console.log("Unable to get user_info");
            }
        };

        let menu_collapsed = false;

        switch (this.props.width) {
            case "xs":
                menu_collapsed = true;
                break;
            case "sm":
                menu_collapsed = true;
                break;
            case "md":
                break;
            default:
                break;
        }

        let renderable_container = "";

        if (menu_collapsed) {
            // Mobile view  OR small desktop view
            renderable = [
                <div key="dldlese" style={{ marginRight: "10px" }}>
                    {shopping_cart_button} {account_button()}
                </div>
            ];
            renderable_container = <div style={{ paddingTop: "10px" }}>{renderable}</div>;
        } else {
            // Large desktop view
            let renderable_buttons = [home, aboutUs, FAQ, disclaimers, claims, premiumback, resources, products];
            let renderable_icons = [shopping_cart_button, account_button()];
            renderable_container = (
                <div>
                    <div style={{ float: "right" }}>{renderable_icons}</div>
                    <div style={{ paddingTop: "5px" }}>{renderable_buttons}</div>
                </div>
            );
        }

        if (expanded_menu === "expanded_menu") {
            renderable = [home, aboutUs, FAQ, disclaimers, claims, premiumback, resources, products];
            renderable = renderable.map((item, index) => (
                <div
                    key={"expanded_menu " + index}
                    style={{ borderBottom: "solid", borderWidth: "2px", borderBottomColor: "#a9a9a94d" }}
                >
                    {item}
                </div>
            ));

            let renderable_full_width = [];

            for (let i = 0; i < renderable.length; i++) {
                renderable_full_width.push(
                    <div key={"renderable_full_width " + i} style={{ margin: "0px 27px 0px 27px" }}>
                        {renderable[i]}
                    </div>
                );
            }

            renderable_container = <div style={{ width: "100%" }}> {renderable_full_width} </div>;
        }

        return renderable_container;
    };

    handleVerifyEmail = () => {
        console.log("handleVerifyEmail()");
        this.setState(
            {
                verify_email_button_disabled: true
            },
            () => {
                // Verify the verification code
                axios
                    .post("/api/auth/verifyEmail", {
                        signup_username: this.state.login_email,
                        email_verification_code: this.state.email_verification_code
                    })
                    .then((res) => {
                        if (res.status === 200) {
                            if (res.data.err) {
                                alert(res.data.err); // Eg: ERROR - Email already in use
                                this.setState({
                                    verify_email_button_disabled: false
                                });
                            } else {
                                // console.log(res.data)
                                if (res.data.verification_result) {
                                    console.log("Verification OK");
                                    if (res.data.token) {
                                        cookie.set("ifinsg_token", res.data.token, { path: "/" });
                                        localStorage.setItem("ifinsg_token", res.data.token);
                                    }
                                    this.setState({
                                        expanded_login: false,
                                        expansian_panel_login_disabled: true
                                    });
                                    this.nextPanelAfterSignup();
                                } else {
                                    console.log("Unable to get token from server");
                                }
                            }
                        } else {
                            console.log("ERROR - ", res);
                        }
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            }
        );
    };

    renderExpandIcon = () => {
        if (this.state.render_expand_icon) {
            return (
                <MenuIcon
                    style={{ fontSize: "30px", color: "dimgrey", margin: "0px 10px 0px 10px" }}
                    onClick={() => {
                        this.state.expanded_menu
                            ? console.log("manu is already expanded")
                            : console.log("expanding menu");
                        this.setState({ expanded_menu: true });
                    }}
                />
            );
        }
    };

    getMuiTheme = () => {
        return createMuiTheme({
            overrides: {
                MuiPaper: {
                    elevation1: {
                        boxShadow: 1
                    }
                }
            }
            // overrides: {
            //   MuiPaper: {
            //     elevation1: {
            //       boxShadow: 1
            //     }
            //   },
            //   MuiExpansionPanelSummary: {
            //     content: {
            //       margin: '5px 0px 10px 0px!important'
            //     },
            //     expandIcon: {
            //       marginTop: '3px!important',
            //       transition: 'transform 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms'
            //     }
            //   },

            // }
        });
    };

    render() {
        const { classes } = this.props;

        return (
            <div className="TopNavBar">
                <ScrollToTop
                    // style={{zIndex: 99}}
                    showScrollToTopButton={this.props.showScrollToTopButton}
                />

                <MuiThemeProvider theme={this.getMuiTheme()}>
                    <div className={classNames(classes.align_center, classes.full_width)}>
                        <div className={classNames(classes.page_width, classes.align_left, classes.full_width)}>
                            <div
                                style={{
                                    width: "100%",
                                    borderBottom: "solid",
                                    borderWidth: "2px",
                                    borderBottomColor: "#a9a9a94d",
                                    pointerEvents: "auto"
                                }}
                            >
                                <ClickAwayListener
                                    onClickAway={() => {
                                        this.state.expanded_menu
                                            ? this.setState({ expanded_menu: false })
                                            : console.log();
                                    }}
                                >
                                    <ExpansionPanel
                                        expanded={this.state.expanded_menu}
                                        style={{ cursor: "auto", margin: "0px 0px 0px 0px" }}
                                    >
                                        <ExpansionPanelSummary
                                            style={{ cursor: "auto", margin: "0px 0px 0px 0px" }}
                                            onClick={() => {
                                                this.state.expanded_menu
                                                    ? this.setState({ expanded_menu: false })
                                                    : console.log(); // Close expandable menu if needed
                                                this.state.anchorEl !== null
                                                    ? this.setState({ anchorEl: null })
                                                    : console.log(); // Close account_button menu if needed
                                            }}
                                            expandIcon={this.renderExpandIcon()}
                                        >
                                            <div style={{ width: "100%" }}>
                                                <div>
                                                    <a href={Constants.get_new_page("home", this.props.referrer)}>
                                                        {" "}
                                                        <img
                                                            style={{
                                                                float: "left",
                                                                maxWidth: "46px",
                                                                width: "100%",
                                                                margin: "6px 0px 0px 15px",
                                                                padding: "0px 0px 0px 0px"
                                                            }}
                                                            src={"/img/ifinsg_logo.png"}
                                                        />{" "}
                                                    </a>
                                                </div>
                                                {this.renderTopNavBar()}
                                            </div>
                                        </ExpansionPanelSummary>
                                        <ExpansionPanelDetails style={{ margin: "0px 0px 0px 0px" }}>
                                            {this.renderTopNavBar("expanded_menu")}
                                        </ExpansionPanelDetails>
                                    </ExpansionPanel>
                                </ClickAwayListener>
                            </div>
                        </div>
                    </div>
                </MuiThemeProvider>
            </div>
        );
    }
}

export default withStyles(styles)(withWidth()(TopNavBar));
