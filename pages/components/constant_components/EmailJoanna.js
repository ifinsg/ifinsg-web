import React, { Component } from 'react';

class EmailJoanna extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        if (this.props.showScrollToTopButton === false) {
            // Do nothing - taking it as show unless stated otherwise
        } else {
            return (
                <a href="mailto:JoannaSee@iFinancialSingapore.com" target="blank"> <span style={{ color: "#7cda24" }}><u>JoannaSee@iFinancialSingapore.com</u></span> </a>
            );
        }

    }
}

export default EmailJoanna;