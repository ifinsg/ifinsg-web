import React, { Component } from 'react';
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo

import { NoSsr, Button, Typography, AppBar, Tabs, Tab, Paper, TextField, FormControl, InputLabel, MenuItem, Select, Card, FormControlLabel, Checkbox, Fab, Divider, IconButton, Grid, FormLabel, RadioGroup, Radio, Switch } from '@material-ui/core';

import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';

import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import SelectDoctor from '../selects/SelectDoctor';
import SelectClinic from '../selects/SelectClinic';

import ImageUpload from '../widgets/ImageUpload';
// import DatePickerRMC from '../../../components/widgets/DatePickerRMC/DatePickerRMC'    // Adapted from: https://github.com/react-component/m-date-picker
import DatePickerRMC from 'components/DatePickerRMC/DatePickerRMC'

import FormData from 'form-data'


import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";
import axios from 'axios'

import Head from 'next/head';
import Router from 'next/router';
import 'url-search-params-polyfill';      //Source: https://www.npmjs.com/package/url-search-params-polyfill
import { Link, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Constants from 'constants/constants';
import GlobalHelpers from 'global/helpers';
import GlobalConstants from 'global/constants';

import ModalEditFieldClaim from '../modals/ModalEditFieldClaim'


let allow_submit_claim = false
if (GlobalConstants.staging) {
  allow_submit_claim = true
}

// const allow_submit_claim = false

const styles = {
  page_width: Constants.style.page.width,
  page_padding: Constants.style.page.padding,

  align_center: Constants.style.align.center,

  bold: Constants.style.text.bold,
  break_word: Constants.style.text.break_word,

  title1: Constants.style.text.title1,
  body1: Constants.style.text.body1,

  margin_top_xl: Constants.style.margin_top.xl,
  margin_top_lg: Constants.style.margin_top.lg,
  margin_top_md: Constants.style.margin_top.md,
  margin_top_sm: Constants.style.margin_top.sm,
  margin_top_xs: Constants.style.margin_top.xs,
  margin_top_xxs: Constants.style.margin_top.xxs,
}




/////////////////////////////////// Developer Settings ///////////////////////////////////

const using_fake_getUserDataFromToken_data = false
const do_not_login_via_token = false


/////////////////////////////////// Constants ///////////////////////////////////


const convertStatusHistoryStatusCodeToEvent = {
  "pending_approval": "Purchased",
  "approved": "Active"
}

const convertStatusCodeToStatus = {
  "pending_approval": "Pending Approval",
  "approved": "Active"
}

const getStatusColor = {
  "pending_approval": "skyblue",
  "approved": "green"
}

var timerToSetBackProtection = ""


function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}


let backProtect = false


class FormSubmitClaim extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accident_id: "new",
      accident_description: '',
      accident_date: '',
      clinic_visit_date: '',
      medical_registrar_smc: true,
      doctor_seen: '',
      clinic_visited: '',
      treatment_description: '',
      uploaded_images: [],
      addon_notes: '',
      ack_claim_from_other_insurers_first: '',

      edit_field_modal_open: false,
      edit_field_accident_or_claim: false,
      edit_field_id: false,
      edit_field_name: false,
      edit_field_title: false,

      image_upload_claim_screenshot: "",
    };
  }

  componentWillReceiveProps = (nextProps) => {

  }


  screenshotClaimPage() {
    return new Promise((resolve, reject) => {

      try {
        const input = document.getElementById('submit_claim_page');
        const html2canvas = require('html2canvas')
        html2canvas(input)
          .then((canvas) => {
            // const imgData = canvas.toDataURL('image/png');
            // saveAs(imgData, 'thisisafile.png')

            try {
              canvas.toBlob((blob) => {

                let data = new FormData();
                const file_name = "claim-screenshot.jpeg"

                data.append('file', blob, file_name);

                axios.post("/api/image/upload", data, {
                  headers: {
                    'accept': 'application/json',
                    'Accept-Language': 'en-US,en;q=0.8',
                    'Content-Type': `multipart/form-data; boundary=${data._boundary}`,
                    'Content-Type': "image/jpeg",
                    username: this.props.username,
                    image_type: "CLAIM-SCREENSHOT"
                  }
                }).then(res => {
                  if (res.data) {
                    this.setState({
                      image_upload_claim_screenshot: res.data
                    }, () => {
                      resolve()
                    })
                  } else {
                    reject("ERROR - Successful upload, but unable to save image file name")
                  }
                }).catch(err => {
                  reject(err)
                });

              }, 'image/jpeg', 0.2); // JPEG at 10% quality

            } catch (err) {
              alert("Something went wrong :/")
            }

          })

      } catch (err) {
        alert("Something went wrong :/")
      }
    })
  }


  handleSubmitClaim = () => {
    alert("Please ensure that all required photos/documents have been uploaded.")
    alert("Claim submissions are currently disabled and will be enabled once your policy is live!")
    let product = this.props.policy_details.product

    let submit_claim = {
      token: localStorage.getItem('ifinsg_token'),
      username: this.props.username,
      // preferred_name: this.props.username,

      policy_id: this.props.policy_details.id,

      accident_id: this.state.accident_id ? this.state.accident_id.split('_')[0] : "",
      accident_description: this.state.accident_description,
      accident_date: this.state.accident_date,
      accident_type: Constants.product_details[this.props.policy_details.product].accident_type,
      product: this.props.policy_details.product,
      clinic_visit_date: this.state.clinic_visit_date,
      medical_registrar_smc: this.state.medical_registrar_smc,
      doctor_seen: this.state.doctor_seen,
      clinic_visited: this.state.clinic_visited,
      treatment_description: this.state.treatment_description,
      uploaded_images: this.state.uploaded_images,
      addon_notes: this.state.addon_notes,
      ack_claim_from_other_insurers_first: this.state.ack_claim_from_other_insurers_first,
    }

    // // Pre approve accident_details_verified
    // switch (product) {
    //   case "small_influenza_gp":
    //     submit_claim.accident_details_verified = true
    //     break;
    //   default:
    //     // Not supposed to happen
    //     break;
    // }


    const field_name_to_text = {
      token: "token",
      username: "username",
      policy_id: "policy ID",

      accident_id: "incident ID",
      accident_description: "incident description",
      accident_date: "incident date",
      clinic_visit_date: "clinic visit date",
      medical_registrar_smc: "Western doctor or TCM doctor",
      doctor_seen: "doctor seen",
      clinic_visited: "clinic visited",
      treatment_description: "treatment description",
      uploaded_images: "uploaded images",
      addon_notes: "add-on notes",
      ack_claim_from_other_insurers_first: "acknowledgement checkbox",
    }


    // Validating data
    let error_message = ""
    let json_keys = Object.keys(submit_claim)

    for (let i = 0; i < json_keys.length; i++) {
      let key = json_keys[i]
      let value = submit_claim[key]
      switch (product) {
        case "small_accident":
          if (Constants.product_details[product].claim_required_fields.indexOf(key) != -1) {
            // It is required to check
          } else {
            continue // Dont check for this
          }
          break;
        case "medium_accident":
          if (Constants.product_details[product].claim_required_fields.indexOf(key) != -1) {
            // It is required to check
          } else {
            continue // Dont check for this
          }
          break;
        case "small_influenza_gp":
          if (Constants.product_details[product].claim_required_fields.indexOf(key) != -1) {
            // It is required to check
          } else {
            continue // Dont check for this
          }
          break;
        case "small_covid19_income":
          if (Constants.product_details[product].claim_required_fields.indexOf(key) != -1) {
            // It is required to check
          } else {
            continue // Dont check for this
          }
          break;
        case "small_accident_income":

          if (Constants.product_details[product].claim_required_fields.indexOf(key) != -1) {
            // It is required to check
          } else {
            continue // Dont check for this
          }
          break;
        default:
          // Not supposed to happen
          break;

      }

      if (key === "medical_registrar_smc") {
        if (value === null) {
          error_message = "ERROR - Please enter a value for " + field_name_to_text[key]
          break;
        }
      } else {
        if (value == "" || value == null || value == undefined) {
          error_message = "ERROR - Please enter a value for " + field_name_to_text[key]
          break;
        }
      }

    }

    if (error_message) {
      alert(error_message)
      return false
    }


    if (allow_submit_claim) {

      this.screenshotClaimPage().then(() => {

        submit_claim.image_upload_claim_screenshot = this.state.image_upload_claim_screenshot

        let token = localStorage.getItem('ifinsg_token')
        if (token) {
          axios.post("/api/submitClaim", {
            submit_claim
          }).then(res => {
            if (res.status === 200) {
              if (res.data.err) {
                alert(res.data.err)
                this.setState({ enable_change_password_button: this.state.enable_change_password_button + 1 })
              } else {
                try {
                  alert("SUCCESS")
                  this.props.handleClose()
                } catch (err) {
                  alert(err)
                }
              }
            }
          })

        }
      })
    }
  }


  scrollToElement = (element_indentifier) => {
    if (isMobile) {
      scroller.scrollTo(element_indentifier, {
        duration: 500,
        delay: 0,
        smooth: true,
        offset: -180, // Scrolls to element + 50 pixels down the page
        // containerId: 'ContainerElementID',
      })
    }
  }


  /////////////////////////////////// Submit Claim ///////////////////////////////////


  handleSubmitClaimFieldChange = (prop) => e => {

    // Blocking off text entered in number-only fields
    const fields_with_type_number = ["claims_bank_acct_number"]
    if (fields_with_type_number.includes(prop)) {
      if ("" === e.target.value || (isFinite(e.target.value) && !e.target.value.includes('.'))) {   // Only allows integers in type strings or int to go through
        // Do nothing
      } else {
        return
      }
    }

    let specific_accident_selected = false
    let accident_description = ""
    let accident_date = ""

    if (prop === "accident_id") {
      let accident_id = e.target.value.split('_')[0]
      let accident_index_in_accidents_and_claims = e.target.value.split('_')[1]

      if (accident_id === "new") {
        this.setState({
          [prop]: e.target.value,
          accident_description_disabled: false,
          accident_date_disabled: false,
          accident_description,
          accident_date
        })
      } else {
        specific_accident_selected = true
        accident_description = this.props.accidents_and_claims[accident_index_in_accidents_and_claims].accident_description
        accident_date = this.props.accidents_and_claims[accident_index_in_accidents_and_claims].accident_date

        this.setState({
          [prop]: e.target.value,
          accident_description_disabled: true,
          accident_date_disabled: true,
          accident_description,
          accident_date
        })
      }


    } else {
      this.setState({
        [prop]: e.target.value
      })
    }
  }


  handleDateChange = (field) => (date) => {
    this.setState({
      [field]: date
    }, () => {
    })
  }




  handleEditFieldClicked = (accident_or_claim, id, field_name, field_title) => {
    // Setting states for modal to rceive
    this.setState({
      edit_field_modal_open: true,
      enable_edit_field_button: true,
      edit_field_accident_or_claim: accident_or_claim,
      edit_field_id: id,
      edit_field_name: field_name,
      edit_field_title: field_title,
    }, () => {
    })

  }

  renderFieldEditButton = (allow_edit_field, always_allow, accident_or_claim, id, field_name, field_title) => {
    if (!allow_edit_field) { return "" }
    // else {
    try {
      let to_rework = false
      // let to_rework = true

      if (accident_or_claim === "accident" && this.props.edit_accident && this.props.edit_accident.pending_rework_items) { to_rework = this.props.edit_accident.pending_rework_items.indexOf(field_name) !== -1 }
      else { if (accident_or_claim === "claim" && this.props.edit_claim && this.props.edit_claim.pending_rework_items) { to_rework = this.props.edit_claim.pending_rework_items.indexOf(field_name) !== -1 } }

      if (always_allow || to_rework) {
        let fontWeight = false
        let color = ""
        let button_text = "Edit"

        if (field_name === "uploaded_images") { button_text = "Upload" }
        if (to_rework) { color = "red" }

        return (
          <div style={{ float: "right" }}>
            <Button
              onClick={() => { this.handleEditFieldClicked(accident_or_claim, id, field_name, field_title) }}
              style={{ fontSize: "14px", fontWeight: "unset", padding: 0, minWidth: "40px", height: "20px", lineHeight: 0 }}>
              <span style={{ color: color, fontWeight: fontWeight }}> {button_text}  </span>
            </Button>
          </div>
        )
      }
    } catch (err) {
    }

    // }

  }



  renderClaimForm = (product) => {
    let renderable = []
    let renderable1 = []
    let renderable2 = []
    let renderable3 = []

    const disabled_all = false
    try {

      const instructions =
        <Typography variant="body1" style={{ textAlign: "center", padding: "10px 0px 10px 0px" }}> The 'Submit my Claim' button at the bottom will not be active, until your cover has been approved. </Typography>

      const live_chat = //() => {
        // if (this.props.mode !== "edit_claim" && this.props.mode !== "edit_accident") {
        //   return (
        <div style={{ margin: '20px 0px 0px 44px', height: '50px', }} >
          <a href="https://api.whatsapp.com/send?phone=6596898676" target="blank" style={{ color: 'white' }}>
            <img style={{ float: 'right', maxWidth: '35px', width: '100%', marginLeft: "10px" }} src={('/img/icon-whatsapp.png')} />
            {/* <div style={{ float: 'left', padding: '10px 0px 0px 20px', fontSize: '20px' }}>WhatsApp Live Chat</div> */}
          </a>
          <a href="tel:+6596898676" target="blank" style={{ color: 'white' }}>
            <img style={{ float: 'right', maxWidth: '35px', width: '100%', marginLeft: "10px" }} src={('/img/icon-phone.png')} />
            {/* <div style={{ float: 'left', padding: '10px 0px 0px 20px', fontSize: '20px' }}>Call Us</div> */}
          </a>
          <div style={{ float: 'right', padding: '8px 0px 0px 20px', fontSize: '15px' }}>Support</div>
        </div>

      const user_details_row = (field, value, allow_edit_field, accident_or_claim, id, field_name) => {
        return (
          <Grid container spacing={4} style={{ padding: "0px 0px 0px 0px", margin: "0px 0px 0px 0px", wordBreak: "break-word" }} >
            <Grid item xs={3} style={{ textAlign: 'left', marginTop: '5px', minWidth: "80px" }}> <Typography style={{ fontSize: "15px" }}> {field}</Typography> </Grid>
            <Grid item xs={7} style={{ textAlign: 'left', marginTop: '5px', paddingLeft: "10px" }}> <Typography style={{ fontSize: "15px" }}> {value}</Typography> </Grid>
            <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: "5px", marginTop: '5px' }}>
              {/* <Button> hiiii</Button> */}
              {this.renderFieldEditButton(allow_edit_field, false, accident_or_claim, id, field_name, field)}
              {/* renderFieldEditButton = (always_allow, accident_or_claim, id, field_name, field_title, i) => { */}
            </Grid>
          </Grid>
        )
      }

      const user_details_value = (value) => {
        return (
          <Grid item xs={8} style={{ textAlign: 'left', marginTop: '5px' }}> <Typography style={{ fontSize: "15px" }}> {value}</Typography> </Grid>
        )
      }

      const user_details = () => {
        let returnable = []

        if ((this.props.mode === "edit_accident" && this.props.edit_accident.pending_rework_message && this.props.edit_accident.pending_rework_items.length > 0)
          ||
          (this.props.mode === "edit_claim" && this.props.edit_claim.pending_rework_message && this.props.edit_claim.pending_rework_items.length > 0)) {
          returnable.push(
            <Grid container spacing={4} style={{ padding: "0px 0px 0px 0px", margin: "0px 0px 0px 0px", wordBreak: "break-word" }} >
              <Grid item xs={12} style={{ textAlign: 'left', marginTop: '5px', minWidth: "80px" }}>
                <div style={{ textAlign: "center" }}>
                  <Typography style={{ color: "red" }}> {this.props[this.props.mode].pending_rework_message} </Typography>
                  {this.props[this.props.mode].pending_rework_items.indexOf("uploaded_images") === -1 ? "" :
                    this.renderFieldEditButton(true, false, "accident", this.props[this.props.mode].ref_username_norm, "uploaded_images", "Upload Image")
                  }
                </div>
              </Grid>
            </Grid>
          )
        }


        returnable.push(user_details_row("Full Name:", this.props.policy_details.full_name, false))
        returnable.push(user_details_row("NRIC:", this.props.policy_details.nric_number, false))
        returnable.push(user_details_row("Policy ID", this.props.policy_details.id, false))
        returnable.push(user_details_row("Policy Name:", this.props.policy_details.product_details.name, false))

        if (this.props.mode === "edit_accident") {
          returnable.push(<div style={{ marginTop: "20px" }}> </div>)
          returnable.push(user_details_row("Incident ID:", this.props.edit_accident.id, false))
          returnable.push(user_details_row("Date Submited:", GlobalHelpers.displayDate(this.props.edit_accident.created_at), true, "accident", this.props.edit_accident.id, 'created_at'))
          returnable.push(user_details_row("Incident Date:", GlobalHelpers.displayDate(this.props.edit_accident.accident_date), true, "accident", this.props.edit_accident.id, 'accident_date'))
          returnable.push(user_details_row("Incident Description:", this.props.edit_accident.accident_description, true, "accident", this.props.edit_accident.id, 'accident_description'))

        } else if (this.props.mode === "edit_claim") {
          returnable.push(<div style={{ marginTop: "20px" }}> </div>)
          returnable.push(user_details_row("Claim ID:", this.props.edit_claim.id, false))
          returnable.push(user_details_row("Status:", this.props.edit_claim.status, false))
          returnable.push(user_details_row("Incident ID:", this.props.edit_claim.ref_accident_id, false))
          returnable.push(user_details_row("Date Submited:", GlobalHelpers.displayDate(this.props.edit_claim.created_at, "dd-MM-yyyy"), false))

          returnable.push(user_details_row("Clinic Type:", this.props.edit_claim.medical_registrar_smc ? "Western" : "TCM", true, "claim", this.props.edit_claim.id, 'medical_registrar_smc'))
          returnable.push(user_details_row("Clinic Visit Date:", GlobalHelpers.displayDate(this.props.edit_claim.clinic_visit_date, "dd-MM-yyyy"), true, "claim", this.props.edit_claim.id, 'clinic_visit_date'))
          returnable.push(user_details_row("Doctor Seen:", this.props.edit_claim.doctor_seen, true, "claim", this.props.edit_claim.id, 'doctor_seen'))
          returnable.push(user_details_row("Clinic Visited:", this.props.edit_claim.clinic_visited, true, "claim", this.props.edit_claim.id, 'clinic_visited'))
          returnable.push(user_details_row("Treatment Description:", this.props.edit_claim.treatment_description, true, "claim", this.props.edit_claim.id, 'treatment_description'))
          returnable.push(user_details_row("Addon Notes:", this.props.edit_claim.addon_notes, true, "claim", this.props.edit_claim.id, 'addon_notes'))
        }

        return (
          <div style={{ maxWidth: "600px" }}>
            {returnable}
          </div>
        )
      }


      const accident_id = () => {

        let accident_list = () => {
          let returnable = []
          let accidents_and_claims = this.props.accidents_and_claims
          if (accidents_and_claims) {
            returnable.push(<MenuItem value={"new"}>New Incident</MenuItem>)
            for (let i = 0; i < accidents_and_claims.length; i++) {
              // accidents_and_claims.forEach((ele)=>{
              let ele = accidents_and_claims[i]
              if (ele.ref_policy_id === this.props.policy_details.id) {
                returnable.push(<MenuItem value={ele.id + "_" + i}>Incident {ele.id}</MenuItem>)
              }
            }
            return returnable
          } else {
            return ([
              <MenuItem value={"new"}>New Incident</MenuItem>,
              // <MenuItem value={"9"}>Accident 1</MenuItem>
            ])
          }
        }



        return (
          < FormControl disabled={disabled_all} style={{ margin: "30px 0px 0px 0px", textAlign: 'left' }
          } fullWidth>
            <InputLabel>Incident</InputLabel>
            <Select
              value={this.state.accident_id}
              onChange={this.handleSubmitClaimFieldChange('accident_id')}
            >
              {/* <MenuItem value={"1"}>Accident 1</MenuItem>
        <MenuItem value={"2"}>Accident 2</MenuItem> */}
              {accident_list()}

            </Select>
          </FormControl >

        )
      }



      const accident_description =
        <Element name={"accident_description_" + this.props.i} className="element">
          <div onFocus={() => { this.scrollToElement("accident_description_" + this.props.i) }}>
            <TextField label="Incident Description"
              value={this.state.accident_description}
              disabled={this.state.accident_description_disabled}
              onChange={this.handleSubmitClaimFieldChange('accident_description')}
              fullWidth
              multiline
            />
          </div>
        </Element>




      const accident_date =
        <Element name={"accident_date_" + this.props.i} className="element">
          <div onFocus={() => { this.scrollToElement("accident_date_" + this.props.i) }}>
            <DatePickerRMC
              disabled={this.state.accident_date_disabled}
              // disabled={true}

              date={this.state.accident_date}
              handleDateChange={this.handleDateChange("accident_date")}
              placeholder="Incident Date"
              // defaultDate = "1992-01-01"  // default = today
              minDate="1850-01-01"
              useBrowserDatePicker={true}

            // maxDate = "1993-01-01" // default = today
            />
          </div>
        </Element>

      const clinic_visit_date = () => {
        let placeholder = "Date of Visit"
        switch (product) {
          case "small_pmd_protect":
            placeholder = "Date Sent for Repair"
            break;
          case "small_influenza_gp":
            placeholder = "Date of Influenza diagnosis"
            break;
          case "small_covid19_income":
            placeholder = "Issue Date of Quarantine Order / Date of confirmed diagnosis"
            break;
          default:
            break;
        }

        return (
          <Element name={"clinic_visit_date_" + this.props.i} className="element" style={{ marginTop: "50px" }}>
            <div onFocus={() => { this.scrollToElement("clinic_visit_date_" + this.props.i) }}>
              <DatePickerRMC
                date={this.state.clinic_visit_date}
                handleDateChange={this.handleDateChange("clinic_visit_date")}
                placeholder={placeholder}
                // defaultDate = "1992-01-01"   // default = today
                minDate="1850-01-01"
                // maxDate = "1993-01-01" // default = today
                useBrowserDatePicker={true}

              />
            </div>
          </Element>
        )
      }



      const medical_registrar_smc = () => {
        switch (product) {
          case "small_accident":
            return (
              <div style={{ textAlign: "center", marginTop: '20px' }}>
                {/* <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === true} onChange={() => { this.setState({ medical_registrar_smc: true, doctor_seen: "", clinic_visited: "" }, () => { console.log(this.state) }) }} />} label="Western Doctor" />
              <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === false} onChange={() => { this.setState({ medical_registrar_smc: false, doctor_seen: "", clinic_visited: "" }, () => { console.log(this.state) }) }} />} label="TCM Doctor" /> */}
                <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === true} onChange={() => { this.setState({ medical_registrar_smc: true }, () => { }) }} />} label="Western Doctor" />
                <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === false} onChange={() => { this.setState({ medical_registrar_smc: false }, () => { }) }} />} label="TCM Doctor" />
              </div>
            )
            break;
          case "medium_accident":
            return (
              <div style={{ textAlign: "center", marginTop: '20px' }}>
                {/* <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === true} onChange={() => { this.setState({ medical_registrar_smc: true, doctor_seen: "", clinic_visited: "" }, () => { console.log(this.state) }) }} />} label="Western Doctor" />
                <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === false} onChange={() => { this.setState({ medical_registrar_smc: false, doctor_seen: "", clinic_visited: "" }, () => { console.log(this.state) }) }} />} label="TCM Doctor" /> */}
                <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === true} onChange={() => { this.setState({ medical_registrar_smc: true }, () => { }) }} />} label="Western Doctor" />
                <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === false} onChange={() => { this.setState({ medical_registrar_smc: false }, () => { }) }} />} label="TCM Doctor" />
              </div>
            )
            break;
          case "small_influenza_gp":
            return (
              <div style={{ textAlign: "center", marginTop: '20px' }}>
                {/* <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === true} onChange={() => { this.setState({ medical_registrar_smc: true, doctor_seen: "", clinic_visited: "" }, () => { console.log(this.state) }) }} />} label="Western Doctor" />
              <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === false} onChange={() => { this.setState({ medical_registrar_smc: false, doctor_seen: "", clinic_visited: "" }, () => { console.log(this.state) }) }} />} label="TCM Doctor" /> */}
                <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === true} onChange={() => { this.setState({ medical_registrar_smc: true }, () => { }) }} />} label="Western Doctor" />
                {/* <FormControlLabel disabled style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === false} onChange={() => { this.setState({ medical_registrar_smc: false }, () => { }) }} />} label="TCM Doctor" /> */}
              </div>
            )
            break;
          case "small_covid19_income":
            return (
              <div style={{ textAlign: "center", marginTop: '20px' }}>
                {/* <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === true} onChange={() => { this.setState({ medical_registrar_smc: true, doctor_seen: "", clinic_visited: "" }, () => { console.log(this.state) }) }} />} label="Western Doctor" />
              <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === false} onChange={() => { this.setState({ medical_registrar_smc: false, doctor_seen: "", clinic_visited: "" }, () => { console.log(this.state) }) }} />} label="TCM Doctor" /> */}
                <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === true} onChange={() => { this.setState({ medical_registrar_smc: true }, () => { }) }} />} label="Western Doctor" />
                {/* <FormControlLabel disabled style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === false} onChange={() => { this.setState({ medical_registrar_smc: false }, () => { }) }} />} label="TCM Doctor" /> */}
              </div>
            )
            break;
          case "small_accident_income":
            return (
              <div style={{ textAlign: "center", marginTop: '20px' }}>
                {/* <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === true} onChange={() => { this.setState({ medical_registrar_smc: true, doctor_seen: "", clinic_visited: "" }, () => { console.log(this.state) }) }} />} label="Western Doctor" />
              <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === false} onChange={() => { this.setState({ medical_registrar_smc: false, doctor_seen: "", clinic_visited: "" }, () => { console.log(this.state) }) }} />} label="TCM Doctor" /> */}
                <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === true} onChange={() => { this.setState({ medical_registrar_smc: true }, () => { }) }} />} label="Western Doctor" />
                {/* <FormControlLabel disabled style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === false} onChange={() => { this.setState({ medical_registrar_smc: false }, () => { }) }} />} label="TCM Doctor" /> */}
              </div>
            )
            break;
          default:
            // Not supposed to happen
            break;
        }
      }



      const select_doctor = () => {
        switch (product) {
          case "small_accident":
            return (
              <Element name={"select_doctor_" + this.props.i} className="element">
                <div onFocus={() => { this.scrollToElement("select_doctor_" + this.props.i) }}>
                  <SelectDoctor disabled={disabled_all} handleSelectFieldChange={this.handleSelectFieldChange} medical_registrar_smc={this.state.medical_registrar_smc} />
                </div>
              </Element>
            )
            break;
          case "medium_accident":
            return (
              <Element name={"select_doctor_" + this.props.i} className="element">
                <div onFocus={() => { this.scrollToElement("select_doctor_" + this.props.i) }}>
                  <SelectDoctor disabled={disabled_all} handleSelectFieldChange={this.handleSelectFieldChange} medical_registrar_smc={this.state.medical_registrar_smc} />
                </div>
              </Element>
            )
            break;
          case "small_influenza_gp":
            return (
              <Element name={"select_doctor_" + this.props.i} className="element">
                <div onFocus={() => { this.scrollToElement("select_doctor_" + this.props.i) }}>
                  <SelectDoctor disabled={disabled_all} handleSelectFieldChange={this.handleSelectFieldChange} medical_registrar_smc={true} />
                </div>
              </Element>
            )
            break;
          case "small_covid19_income":
            return (
              <Element name={"select_doctor_" + this.props.i} className="element">
                <div onFocus={() => { this.scrollToElement("select_doctor_" + this.props.i) }}>
                  <SelectDoctor disabled={disabled_all} handleSelectFieldChange={this.handleSelectFieldChange} medical_registrar_smc={true} />
                </div>
              </Element>
            )
            break;
          case "small_accident_income":
            return (
              <Element name={"select_doctor_" + this.props.i} className="element">
                <div onFocus={() => { this.scrollToElement("select_doctor_" + this.props.i) }}>
                  <SelectDoctor disabled={disabled_all} handleSelectFieldChange={this.handleSelectFieldChange} medical_registrar_smc={true} />
                </div>
              </Element>
            )
            break;
          default:
            // Not supposed to happen
            break;
        }
      }


      const select_clinic = () => {
        switch (product) {
          case "small_accident":
            return (
              <Element name={"select_clinic_" + this.props.i} className="element">
                <div onFocus={() => { this.scrollToElement("select_clinic_" + this.props.i) }}>
                  <SelectClinic disabled={disabled_all} handleSelectFieldChange={this.handleSelectFieldChange} medical_registrar_smc={this.state.medical_registrar_smc} />
                </div>
              </Element>
            )
            break;
          case "medium_accident":
            return (
              <Element name={"select_clinic_" + this.props.i} className="element">
                <div onFocus={() => { this.scrollToElement("select_clinic_" + this.props.i) }}>
                  <SelectClinic disabled={disabled_all} handleSelectFieldChange={this.handleSelectFieldChange} medical_registrar_smc={this.state.medical_registrar_smc} />
                </div>
              </Element>
            )
            break;
          case "small_influenza_gp":
            return (
              <Element name={"select_clinic_" + this.props.i} className="element">
                <div onFocus={() => { this.scrollToElement("select_clinic_" + this.props.i) }}>
                  <SelectClinic disabled={disabled_all} handleSelectFieldChange={this.handleSelectFieldChange} medical_registrar_smc={true} placeholder={"GP Clinic / Gov Polyclinic visited"} />
                </div>
              </Element>
            )
            break;
          case "small_covid19_income":
            return (
              <Element name={"select_clinic_" + this.props.i} className="element">
                <div onFocus={() => { this.scrollToElement("select_clinic_" + this.props.i) }}>
                  <SelectClinic disabled={disabled_all} handleSelectFieldChange={this.handleSelectFieldChange} medical_registrar_smc={true} placeholder={"Institution which issued Quarantine / Medical institution which issued diagnosis"} />
                </div>
              </Element>
            )
            break;
          case "small_accident_income":
            return (
              <Element name={"select_clinic_" + this.props.i} className="element">
                <div onFocus={() => { this.scrollToElement("select_clinic_" + this.props.i) }}>
                  <SelectClinic disabled={disabled_all} handleSelectFieldChange={this.handleSelectFieldChange} medical_registrar_smc={true} />
                </div>
              </Element>
            )
            break;
          default:
            // Not supposed to happen
            break;
        }

      }



      const treatment_description =
        <Element name={"treatment_description_" + this.props.i} className="element">
          <div onFocus={() => { this.scrollToElement("treatment_description_" + this.props.i) }}>
            <TextField label="Treatment Description"
              value={this.state.treatment_description}
              onChange={this.handleSubmitClaimFieldChange('treatment_description')}
              fullWidth
              multiline
            />
          </div>
        </Element>

      const upload_image = () => {
        switch (product) {
          case "small_accident":
            return (
              <div style={{}}>
                <div style={{ textAlign: "left" }}> <Typography variant="body2" style={{ margin: "30px 0px 0px 0px", color: "grey" }}>Upload photos of:</Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 0px 0px", color: "grey" }}>(i) Paid Receipt </Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 0px 0px", color: "grey" }}>(ii) Itemized Invoice of treatment and medicine </Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 5px 0px", color: "grey" }}>(iii) MC with diagnosis / Letter of diagnosis </Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 5px 0px", color: "grey", marginTop: "20px" }}>* Just make sure the Doctor writes the <u>Date of accident</u> + <u>Description of accident</u>, on any 1 of the documents in the photos which you upload ( either Receipt / Invoice / MC with diagnosis / Letter of diagnosis ). </Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 5px 0px", color: "grey", marginTop: "20px" }}> * If you received treatment from the A&E department of a Public Hospital outside of Singapore, please upload photos of the entry and exit stamps in your Passport as evidence that you were in that country. </Typography>  </div>
                <ImageUpload image_type="CLAIM-IMAGE" username={this.props.policy_details.username} maxFiles={3} type={"claim"} fileUploadedClaim={this.fileUploadedClaim} fileRemovedClaim={this.fileRemovedClaim} />
              </div>
            )
            break;
          case "medium_accident":
            return (
              <div style={{}}>
                <div style={{ textAlign: "left" }}> <Typography variant="body2" style={{ margin: "30px 0px 0px 0px", color: "grey" }}>Upload photos of:</Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 0px 0px", color: "grey" }}>(i) Paid Receipt </Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 0px 0px", color: "grey" }}>(ii) Itemized Invoice of treatment and medicine </Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 0px 0px", color: "grey" }}>(iii) MC with diagnosis / Letter of diagnosis </Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 5px 0px", color: "grey" }}>(iv) Ride Hailing receipt for the journeys To-and-Fro for the 1st treatment of each accident.</Typography>  </div>

                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 5px 0px", color: "grey", marginTop: "20px" }}>* Just make sure the Doctor writes the <u>Date of accident</u> + <u>Description of accident</u>, on any 1 of the documents in the photos which you upload ( either Receipt / Invoice / MC with diagnosis / Letter of diagnosis ). </Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 5px 0px", color: "grey", marginTop: "20px" }}>* If you received treatment from the A&E department of a Public Hospital outside of Singapore, please upload photos of the entry and exit stamps in your Passport as evidence that you were in that country. </Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 5px 0px", color: "grey", marginTop: "20px" }}>* Just make sure the receipt of the Ride Hailing trip to the 1st treatment must display the clinic / hospital address as the exact Destination location; while the receipt of the Ride Hailing trip to leave from the 1st treatment must display the clinic / hospital address as the exact Pickup location.</Typography>  </div>
                <ImageUpload image_type="CLAIM-IMAGE" username={this.props.policy_details.username} maxFiles={3} type={"claim"} fileUploadedClaim={this.fileUploadedClaim} fileRemovedClaim={this.fileRemovedClaim} />
              </div>
            )
            break;
          case "small_influenza_gp":
            return (
              <div style={{}}>
                <div style={{ textAlign: "left" }}> <Typography variant="body2" style={{ margin: "30px 0px 0px 0px", color: "grey" }}>Upload photos of:</Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 0px 0px", color: "grey" }}>(i) Paid Receipt </Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 0px 0px", color: "grey" }}>(ii) Itemized Invoice of treatment and medicine </Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 5px 0px", color: "grey" }}>(iii) MC with diagnosis / Letter of diagnosis </Typography>  </div>

                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 5px 0px", color: "grey", marginTop: "20px" }}>Just make sure the Doctor writes the <u>Date of diagnosis</u> + <u>Description of diagnosis</u>, on any 1 of the documents in the photos which you upload ( either Receipt / Invoice / MC with diagnosis / Letter of diagnosis ). </Typography>  </div>

                <ImageUpload image_type="CLAIM-IMAGE" username={this.props.policy_details.username} maxFiles={3} type={"claim"} fileUploadedClaim={this.fileUploadedClaim} fileRemovedClaim={this.fileRemovedClaim} />
              </div>
            )
          case "small_covid19_income":
            return (
              <div style={{}}>
                <div style={{ textAlign: "left" }}> <Typography variant="body2" style={{ margin: "30px 0px 0px 0px", color: "grey" }}>Upload photos of:</Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 0px 0px", color: "grey" }}>(i) Paid Receipt </Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 0px 0px", color: "grey" }}>(ii) Itemized Invoice of treatment and medicine </Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 5px 0px", color: "grey" }}>(iii) MC with diagnosis / Letter of diagnosis </Typography>  </div>

                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 5px 0px", color: "grey", marginTop: "20px" }}>Just make sure the Doctor writes the <u>Date of diagnosis</u> + <u>Description of diagnosis</u>, on any 1 of the documents in the photos which you upload ( either Receipt / Invoice / MC with diagnosis / Letter of diagnosis ). </Typography>  </div>

                <ImageUpload image_type="CLAIM-IMAGE" username={this.props.policy_details.username} maxFiles={3} type={"claim"} fileUploadedClaim={this.fileUploadedClaim} fileRemovedClaim={this.fileRemovedClaim} />
              </div>
            )
          case "small_accident_income":
            return (
              <div style={{}}>
                <div style={{ textAlign: "left" }}> <Typography variant="body2" style={{ margin: "30px 0px 0px 0px", color: "grey" }}>Upload photos of:</Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 0px 0px", color: "grey" }}>(i) Paid Receipt </Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 0px 0px", color: "grey" }}>(ii) Itemized Invoice of treatment and medicine </Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 5px 0px", color: "grey" }}>(iii) MC with diagnosis / Letter of diagnosis </Typography>  </div>
                <div style={{ textAlign: "left" }}> <Typography variant="body1" style={{ margin: "0px 0px 5px 0px", color: "grey", marginTop: "20px" }}>Just make sure the Doctor writes the <u>Date of accident</u> + <u>Description of accident</u>, on any 1 of the documents in the photos which you upload ( either Receipt / Invoice / MC with diagnosis / Letter of diagnosis ).</Typography>  </div>
                <ImageUpload image_type="CLAIM-IMAGE" username={this.props.policy_details.username} maxFiles={3} type={"claim"} fileUploadedClaim={this.fileUploadedClaim} fileRemovedClaim={this.fileRemovedClaim} />
              </div>
            )
          default:
            // Not supposed to happen
            break;
        }

      }



      const notes =
        <Element name={"addon_notes" + this.props.i} className="element">
          <div onFocus={() => { this.scrollToElement("addon_notes" + this.props.i) }}>
            <TextField label="Add-on Notes"
              style={{ marginTop: '10px' }}
              value={this.state.addon_notes}
              onChange={this.handleSubmitClaimFieldChange('addon_notes')}
              fullWidth
              multiline
            />
          </div>
        </Element>

      const ack_claim_from_other_insurers_first =
        <FormControlLabel
          control={
            <Checkbox
              checked={this.state.ack_claim_from_other_insurers_first}
              onChange={this.handleCheckboxToggle('ack_claim_from_other_insurers_first')}      // name, policy_index, submit_claim
              value="ack_claim_from_other_insurers_first"
              color="primary"
            />
          }
          label={<span style={{ color: "rgba(0, 0, 0, 0.54)", fontSize: "13px", lineHeight: 0 }}>I am aware that if I have a similar policy with a Traditional Insurer, I must Submit a Claim for my traditional insurers' policy first, before submitting a Claim with iFinSG. ( Traditional insurers do not allow a double-claim for a Reimbursement cover, unlike iFinSG. )</span>}
          style={{ margin: "30px 0px 0px 0px" }}
        />


      const submit_button =
        <div style={{ textAlign: 'center' }}>
          <Button style={{ marginTop: "20px", backgroundColor: Constants.color.grey, color: "white" }} onClick={() => { this.handleSubmitClaim() }}> Submit my Claim </Button>
        </div>


      if (this.props.mode === "edit_claim" || this.props.mode === "edit_accident") {
        renderable1 = [user_details()] //, accident_id, accident_description, accident_date, clinic_visit_date]
        // renderable2 = [medical_registrar_smc, select_doctor(), select_clinic(), treatment_description]
        // renderable3 = [upload_image(), notes, ack_claim_from_other_insurers_first, submit_button]
      } else {  // Submit new claim
        switch (product) {
          case "small_accident":
            renderable1 = [instructions, live_chat, user_details(), accident_id(), accident_description, accident_date, clinic_visit_date()]
            renderable2 = [medical_registrar_smc(), select_doctor(), select_clinic(), treatment_description]
            renderable3 = [upload_image(), notes, ack_claim_from_other_insurers_first, submit_button]
            break;
          case "medium_accident":
            renderable1 = [instructions, live_chat, user_details(), accident_id(), accident_description, accident_date, clinic_visit_date()]
            renderable2 = [medical_registrar_smc(), select_doctor(), select_clinic(), treatment_description]
            renderable3 = [upload_image(), notes, ack_claim_from_other_insurers_first, submit_button]
            break;
          case "small_influenza_gp":
            renderable1 = [instructions, live_chat, user_details(), clinic_visit_date()]
            renderable2 = [medical_registrar_smc(), select_doctor(), select_clinic(), treatment_description]
            renderable3 = [upload_image(), notes, ack_claim_from_other_insurers_first, submit_button]
            break;
          case "small_covid19_income":
            renderable1 = [instructions, live_chat, user_details(), clinic_visit_date()]
            renderable2 = [medical_registrar_smc(), select_doctor(), select_clinic(), treatment_description]
            renderable3 = [upload_image(), notes, ack_claim_from_other_insurers_first, submit_button]
            break;
          case "small_accident_income":
            renderable1 = [instructions, live_chat, user_details(), accident_id(), accident_description, accident_date, clinic_visit_date()]
            renderable2 = [medical_registrar_smc(), select_doctor(), select_clinic(), treatment_description]
            renderable3 = [upload_image(), notes, ack_claim_from_other_insurers_first, submit_button]
            break;
          case "small_pmd_protect":
            renderable1 = [instructions, live_chat, user_details(), accident_id(), accident_description, accident_date, clinic_visit_date()]
            renderable2 = [medical_registrar_smc(), select_doctor(), select_clinic(), treatment_description]
            renderable3 = [upload_image(), notes, ack_claim_from_other_insurers_first, submit_button]
            break;

          default:
            // Not supposed to happen
            break;
        }

      }

    } catch (err) {
    }

    return (
      <div style={{ padding: '30px 0px 30px 0px' }}>
        <form > {renderable1} </form >
        <form > {renderable2} </form >
        {renderable3}
      </div >
    )
  }


  handleCheckboxToggle = (name) => event => {

    let setStateObject = { [name]: event.target.checked }

    this.setState(setStateObject);

  };

  handleSelectFieldChange = (select_type, value) => {
    let state_name = ""
    if (select_type === "select_doctor") { state_name = "doctor_seen" }
    else if (select_type === "select_clinic") { state_name = "clinic_visited" }

    try {
      this.setState({
        [state_name]: value.label
      })
    } catch (err) {
    }

  }


  fileUploadedClaim = (i, uploaded_file_name) => {
    let uploaded_images = JSON.parse(JSON.stringify(this.state.uploaded_images))

    uploaded_images.push(uploaded_file_name)

    this.setState({
      uploaded_images
    })
  }


  fileRemovedClaim = (i, uploaded_file_name) => {
    let uploaded_images = JSON.parse(JSON.stringify(this.state.uploaded_images))
    uploaded_images.splice(uploaded_images.indexOf(uploaded_file_name), 1);   // Remove uploaded_file_name from the array uploaded_images
    this.setState({
      uploaded_images
    })
  }



  handleEditField = (password, accident_or_claim, id, field_name, new_value) => {
    const token = localStorage.getItem('ifinsg_token');

    if (!password) { alert("Password not filled in"); return }
    if (!accident_or_claim) { console.log("ERROR - accident_or_claim is empty!"); alert("Something went wrong :/"); return }
    if (!id) { console.log("ERROR - id is empty!"); alert("Something went wrong :/"); return }
    if (!field_name) { console.log("ERROR - accident_or_claim is empty!"); alert("Something went wrong. field_name empty :/"); return }
    if (field_name === "self_employed" || field_name === "medical_registrar_smc") { } else { if (!new_value) { alert("Something went wrong with new_value " + new_value + ":/"); return } }

    if (token) {
      axios.post("/api/editFieldClaim", {
        token,
        // username: this.state.user_info.username_norm,
        username: this.props.username,
        password,
        accident_or_claim,
        id,
        field_name,
        new_value
      }).then(res => {
        if (res.status === 200) {
          if (res.data.err) {
            alert(res.data.err)
            this.setState({ enable_change_password_button: this.state.enable_change_password_button + 1 })
          } else {
            try {
              alert("Field changed successfully")
              this.props.getAccidentsAndClaims()
              this.props.handleClose()

            } catch (err) {
              alert("ERROR - Field change failed")
            }
          }
        } else {
          alert("ERROR - Field change failed")
        }

        this.setState({ edit_field_modal_open: false, enable_edit_field_button: true })

      }).catch(err => {
        alert("ERROR - Field change failed")
        this.setState({ edit_field_modal_open: false, enable_edit_field_button: true })

      });
    } else {
      alert("Sorry, your session has expired. Please login again and retry")
      this.setState({ edit_field_modal_open: false, enable_edit_field_button: true })

    }

  }


  /////////////////////////////////// Main Views ///////////////////////////////////




  render() {
    const { classes } = this.props;

    return (

      <div id="submit_claim_page">

        <ModalEditFieldClaim
          open={this.state.edit_field_modal_open}
          handleClose={() => { this.setState({ edit_field_modal_open: false }) }}
          handleEditField={this.handleEditField}
          enable_edit_field_button={this.state.enable_edit_field_button}

          accident_or_claim={this.state.edit_field_accident_or_claim}
          id={this.state.edit_field_id}
          field_name={this.state.edit_field_name}
          field_title={this.state.edit_field_title}

          // medical_registrar_smc={this.state.medical_registrar_smc}
          medical_registrar_smc={this.props.mode === "edit_claim" ? this.props.edit_claim.medical_registrar_smc : null}

          // i={this.state.edit_field_i}
          // username={this.props.edit_accident ? this.props.edit_accident.ref_username_norm : this.props.edit_claim.ref_username_norm}
          // username={this.props.policy_details.ref_username_norm}
          username={this.props.username}

        />

        <div style={{ margin: '10px 10px 0px 10px', textAlign: "center" }}>
          <Typography style={{ fontSize: '25px', fontWeight: 'bold', lineHeight: '1', color: '#7cda24',fontFamily: "Merriweather" }}>  {this.props.mode === "new_claim" ? "Submit Claim" : this.props.mode === "edit_accident" ? "Incident" : "Claim"} </Typography>
        </div>

        {this.renderClaimForm(this.props.policy_details.product)}

      </div >
    );
  }
}


export default withStyles(styles)(FormSubmitClaim);