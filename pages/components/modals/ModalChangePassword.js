import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary } from '@material-ui/core';
import { NoSsr, Button, Typography, Checkbox, Grid, Paper, FormControlLabel, Modal, TextField } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";
// import { NoSsr, Button, Typography, AppBar, Tabs, Tab, , FormControl, InputLabel, MenuItem, Select, Card, FormControlLabel, Checkbox, Fab, Divider, IconButton, Grid, FormLabel, RadioGroup, Radio, Chip } from '@material-ui/core';
// import { AndroidBackHandler } from 'react-navigation-backhandler';



var owasp = require('owasp-password-strength-test');

owasp.config({
  allowPassphrases: false,
  maxLength: 50,
  minLength: 6,
  minPhraseLength: 20,
  minOptionalTestsToPass: 4,
});


function getModalStyle() {
  // const top = 50 + rand();
  // const left = 50 + rand();
  const top = 50
  const left = 50
  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => {
  let width = '85%'

  if (isBrowser) {
    width = '60%'
  }

  return ({
    paper: {
      position: 'absolute',
      width: width,// theme.spacing.unit * 50,
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing.unit * 4,
      outline: 'none',
      maxHeight: '90%',
    },
  })
}


class SimpleModal extends React.Component {
  state = {
    open: this.props.open,
    password_old: "",
    password_new: "",
    password_new_retype: "",
    change_password_button_disabled: false
  };


  componentWillReceiveProps = (nextProps) => {
    if (this.props.open === false && nextProps.open === true) {
      this.handleOpen()
    } else if (this.props.open === true && nextProps.open === false) {
      this.handleClose()
    }
    if (this.props.enable_change_password_button !== nextProps.enable_change_password_button) {
      this.setState({ change_password_button_disabled: !nextProps.enable_change_password_button })
    }
  }

  componentDidMount = () => {
  }

  handleBackPressed = () => {
    console.log("handleBackPressed")
    history.pushState(null, null, null);


    this.handleClose()
  }

  handleOpen = () => {
    this.setState({ open: true, change_password_button_disabled: false });

    // Adding back listener
    history.pushState(null, null, null);
    window.addEventListener("popstate", this.handleBackPressed)
  };

  handleClose = () => {
    console.log("handleClose")

    this.props.handleClose()
    this.setState({ open: false });

    history.go(-1)

    // Removing back listener
    window.removeEventListener('popstate', this.handleBackPressed)

  };


  handleFieldChange = (prop) => e => {
    console.log("handleFieldChange()")

    // console.log(e.target.value)
    // console.log(Number.isInteger(e.target.value))

    // const fields_with_type_number = ["personal_mobile_number", "entity_uen", "personal_bank_acct_number", "mobile_number", "premiumback_bank_acct_number", "claims_bank_acct_number"]

    // if (fields_with_type_number.includes(prop)) {
    //   if ("" === e.target.value || (isFinite(e.target.value) && !e.target.value.includes('.'))) {   // Only allows integers in type strings or int to go through
    //     // Do nothing
    //   } else {
    //     // console.log(e.target.value)
    //     // console.log("Not a number")
    //     return
    //   }
    // }

    this.setState({
      [prop]: e.target.value
    }, () => { console.log(this.state) });


  }


  handleChangePassword = () => {
    const password_old = this.state.password_old
    const password_new = this.state.password_new
    const password_new_retype = this.state.password_new_retype

    console.log(password_old)
    console.log(password_new)
    console.log(password_new_retype)

    const password_strength = owasp.test(password_new);
    console.log(password_strength)

    if (password_strength.requiredTestErrors.length > 0) {
      alert(password_strength.requiredTestErrors[0])
      return
    } else if (password_strength.optionalTestErrors.length > 2) {
      alert(password_strength.optionalTestErrors[0])
      return
    }

    if (password_new !== password_new_retype) {
      alert("Error - Passwords do not match")
      return
    }

    this.setState({
      change_password_button_disabled: true
    }, () => {
      this.props.handleChangePassword(password_old, password_new)
    })


  }


  renderContent = () => {

    let renderable = []

    const title =
      <Typography variant="h6" style={{ textAlign: "center" }}> Change Password</Typography>


    const password_old =
      <div style={{ padding: "10px 0px 2px 0px" }}>
        <TextField
          label="Old password"
          value={this.state.password_old}
          onChange={this.handleFieldChange('password_old')}
          type="password"
          style={{ width: "90%" }}
        />
      </div>

    const password_new =
      <div style={{ padding: "0px 0px 2px 0px" }}>
        <TextField
          label="New password"
          onKeyPress={(ev) => { if (ev.key === 'Enter') { this.handleChangePassword() } }}
          value={this.state.password_new}
          onChange={this.handleFieldChange('password_new')}
          type="password"
          style={{ width: "90%" }}
        />
      </div>

    const password_new_retype =
      <div style={{ padding: "0px 0px 20px 0px" }}>
        <TextField
          label="Re-type your new password"
          onKeyPress={(ev) => { if (ev.key === 'Enter') { this.handleChangePassword() } }}
          value={this.state.password_new_retype}
          onChange={this.handleFieldChange('password_new_retype')}
          type="password"
          style={{ width: "90%" }}
        // fullWidth
        />
      </div>


    const change_button =
      <Button disabled={this.state.change_password_button_disabled} style={{ float: "right" }} onClick={() => { this.handleChangePassword() }}> Change </Button>


    renderable = [title, password_old, password_new, password_new_retype, change_button]

    return (<div style={{ padding: "10px", textAlign: "center" }}>
      {renderable}
    </div>)
  }

  render() {
    const { classes } = this.props;

    return (
      <div>
        {/* <Typography gutterBottom>Click to get the full Modal experience!</Typography>
        <Button onClick={this.handleOpen}>Open Modal</Button> */}

        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>


            {this.renderContent()}


            {/* <SimpleModalWrapped /> */}
          </div>
        </Modal>

      </div>
    );
  }
}

SimpleModal.propTypes = {
  classes: PropTypes.object.isRequired,
};

// We need an intermediary variable for handling the recursive nesting.
const SimpleModalWrapped = withStyles(styles)(SimpleModal);

export default SimpleModalWrapped;