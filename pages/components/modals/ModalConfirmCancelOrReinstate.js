import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary } from '@material-ui/core';
import { NoSsr, Button, Typography, Checkbox, Grid, Paper, FormControlLabel, Modal, TextField } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";
// import { NoSsr, Button, Typography, AppBar, Tabs, Tab, , FormControl, InputLabel, MenuItem, Select, Card, FormControlLabel, Checkbox, Fab, Divider, IconButton, Grid, FormLabel, RadioGroup, Radio, Chip } from '@material-ui/core';
// import { AndroidBackHandler } from 'react-navigation-backhandler';

import axios from 'axios'


var owasp = require('owasp-password-strength-test');

owasp.config({
  allowPassphrases: false,
  maxLength: 50,
  minLength: 6,
  minPhraseLength: 20,
  minOptionalTestsToPass: 4,
});


function getModalStyle() {
  // const top = 50 + rand();
  // const left = 50 + rand();
  const top = 50
  const left = 50
  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => {
  let width = '85%'

  if (isBrowser) {
    width = '60%'
  }

  return ({
    paper: {
      position: 'absolute',
      width: width,// theme.spacing.unit * 50,
      maxWidth: "600px",
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing.unit * 4,
      outline: 'none',
      maxHeight: '90%',

    },
  })
}

class SimpleModal extends React.Component {
  state = {
    open: this.props.open,
    password_old: "",
    password_new: "",
    password_new_retype: "",
    change_password_button_disabled: false,
    message: ""
  };


  componentWillReceiveProps = (nextProps) => {
    if (this.props.open === false && nextProps.open === true) {
      this.handleOpen()
    } else if (this.props.open === true && nextProps.open === false) {
      this.handleClose()
    }
    // if (this.props.modal_confirm_action_data !== nextProps.modal_confirm_action_data) {
    //   this.setState({
    //     modal_confirm_action_data: false
    //   })
    // }
  }

  componentDidMount = () => {
  }

  handleBackPressed = () => {
    history.pushState(null, null, null);
    this.handleClose()
  }

  handleOpen = () => {
    this.setState({ open: true });

    // Adding back listener
    history.pushState(null, null, null);
    window.addEventListener("popstate", this.handleBackPressed)
  };

  handleClose = () => {
    this.props.handleClose()
    this.setState({ open: false });

    // Removing back listener
    // history.go(-1)
    window.removeEventListener('popstate', this.handleBackPressed)

  };

  handleSubmitAction = (policy_id, policy_status, checked, policy_index) => {

    let body = {
      token: localStorage.getItem('ifinsg_token'),
      username: this.props.username,
      policy_id,
      policy_status, //cancellation_requested
      message: "",
      cancel_or_reinstate: checked ? "reinstate" : "cancel"
    }

    if (this.state.message) { body.message = this.state.message }
    
    console.log(body)

    try {

      axios.post("/api/cancelOrReinstate", body).then(res => {
        if (res.status === 200) {
          if (res.data.err) {
            console.log(res.data.err)     // Eg: ERROR - Email already in use
            alert(res.data.err)     // Eg: ERROR - Email already in use
          } else {
            try {
              // SUCCESSFUL
              console.log("SUCCESSFUL")
              alert("SUCCESSFUL")
              this.props.onSuccess(policy_id, policy_status, checked, policy_index)
              this.props.handleClose()

            } catch (err) {
              console.log(err)
              alert(err)
            }
          }
        } else {
          console.log("ERROR - ", res)
          alert("ERROR - ", res)
        }
      }).catch(err => {
        console.log(err);
        alert(err)

      });

    } catch (err) {
      console.log(err)
      alert(err)
    }
  }


  handleFieldChange = (prop) => e => {
    // console.log("handleFieldChange()")

    // console.log(e.target.value)
    // console.log(Number.isInteger(e.target.value))

    // const fields_with_type_number = ["personal_mobile_number", "entity_uen", "personal_bank_acct_number", "mobile_number", "premiumback_bank_acct_number", "claims_bank_acct_number"]

    // if (fields_with_type_number.includes(prop)) {
    //   if ("" === e.target.value || (isFinite(e.target.value) && !e.target.value.includes('.'))) {   // Only allows integers in type strings or int to go through
    //     // Do nothing
    //   } else {
    //     // console.log(e.target.value)
    //     // console.log("Not a number")
    //     return
    //   }
    // }

    this.setState({
      [prop]: e.target.value
    }, () => {
      // console.log(this.state)
    });


  }


  handleChangePassword = () => {
    const password_old = this.state.password_old
    const password_new = this.state.password_new
    const password_new_retype = this.state.password_new_retype

    // console.log(password_old)
    // console.log(password_new)
    // console.log(password_new_retype)

    const password_strength = owasp.test(password_new);
    // console.log(password_strength)

    if (password_strength.requiredTestErrors.length > 0) {
      alert(password_strength.requiredTestErrors[0])
      return
    } else if (password_strength.optionalTestErrors.length > 2) {
      alert(password_strength.optionalTestErrors[0])
      return
    }

    if (password_new !== password_new_retype) {
      alert("Error - Passwords do not match")
      return
    }

    this.setState({
      change_password_button_disabled: true
    }, () => {
      this.props.handleChangePassword(password_old, password_new)
    })


  }


  renderContent = () => {

    if (!this.props.data) { return }
    let modal_title = this.props.data.modal_title
    let message_label = this.props.data.message_label
    let policy_id = this.props.data.policy_id
    let policy_status = this.props.data.policy_status
    let checked = this.props.data.checked
    let policy_index = this.props.data.policy_index


    let renderable = []

    const title =
      <Typography variant="h6" style={{ textAlign: "center" }}> {modal_title}</Typography>

    const message_field =
      <TextField
        label={message_label}
        value={this.state.message}
        onChange={this.handleFieldChange('message')}
        style={{ width: "90%" }}
      />

    const submit_button =
      <Button style={{ float: "right", marginTop: "20px" }} onClick={() => { this.handleSubmitAction(policy_id, policy_status, checked, policy_index) }}> Submit </Button>

    const cancel_button =
      <Button style={{ float: "right", float: "right", marginTop: "20px" }} onClick={() => { this.props.handleClose() }}> Cancel </Button>


    renderable = [title, submit_button, cancel_button]
    if (message_label) {
      renderable = [title, message_field, submit_button, cancel_button]
    }

    return (<div style={{ padding: "10px", textAlign: "center" }}>
      {renderable}
    </div>)
  }

  render() {
    const { classes } = this.props;

    return (
      <div>
        {/* <Typography gutterBottom>Click to get the full Modal experience!</Typography>
        <Button onClick={this.handleOpen}>Open Modal</Button> */}

        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>


            {this.renderContent()}


            {/* <SimpleModalWrapped /> */}
          </div>
        </Modal>

      </div>
    );
  }
}

SimpleModal.propTypes = {
  classes: PropTypes.object.isRequired,
};

// We need an intermediary variable for handling the recursive nesting.
const SimpleModalWrapped = withStyles(styles)(SimpleModal);

export default SimpleModalWrapped;