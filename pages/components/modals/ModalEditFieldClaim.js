import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary } from '@material-ui/core';
// import { NoSsr, Button, Typography, Checkbox, Grid, Paper, FormControlLabel, Modal, TextField } from '@material-ui/core';
import { NoSsr, Button, Typography, AppBar, Tabs, Tab, TextField, FormControl, InputLabel, MenuItem, Select, Modal, Card, FormControlLabel, Checkbox, Fab, Divider, IconButton, Grid, FormLabel, RadioGroup, Radio, Chip } from '@material-ui/core';
import SelectOccupation from '../selects/SelectOccupation'
import SelectCountry from '../selects/SelectCountry'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";
// import { NoSsr, Button, Typography, AppBar, Tabs, Tab, , FormControl, InputLabel, MenuItem, Select, Card, FormControlLabel, Checkbox, Fab, Divider, IconButton, Grid, FormLabel, RadioGroup, Radio, Chip } from '@material-ui/core';
// import { AndroidBackHandler } from 'react-navigation-backhandler';
import DatePickerRMC from '../../../components/DatePickerRMC/DatePickerRMC'    // Adapted from: https://github.com/react-component/m-date-picker
import ImageUpload from '../widgets/ImageUpload'

import GlobalHelpers from '../../../global/helpers';
import GlobalConstants from '../../../global/constants';
import SelectDoctor from '../selects/SelectDoctor';
import SelectClinic from '../selects/SelectClinic';

function getModalStyle() {
  // const top = 50 + rand();
  // const left = 50 + rand();
  const top = 50
  const left = 50
  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => {
  let width = '85%'

  if (isBrowser) {
    width = '60%'
  }

  return ({
    paper: {
      position: 'absolute',
      width: width,// theme.spacing.unit * 50,
      maxWidth: "400px",
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing.unit * 4,
      outline: 'none',
      maxHeight: '90%',

    },
  })
}

class SimpleModal extends React.Component {
  state = {
    open: this.props.open,
    edit_field_button_disabled: false,
    personal_occupation: "",
    password: "",
    new_value: "",
    relationship_to_payer: "",
    gender: false,
    date: "",
    place_of_birth: "",
    uploaded_images: [],
    medical_registrar_smc: null

  };


  componentWillReceiveProps = (nextProps) => {
    console.log("receiving props", this.props.open, nextProps.open)
    if (this.props.open === false && nextProps.open === true) {
      this.handleOpen()
    } else if (this.props.open === true && nextProps.open === false) {
      this.handleClose()
    }
    if (this.props.enable_edit_field_button !== nextProps.enable_edit_field_button) {
      this.setState({
        edit_field_button_disabled: false
      })
    }

    console.log(this.props, "componentWillReceiveProps")
    console.log(nextProps, "NextProps")

    // if (this.props.medical_registrar_smc !== nextProps.medical_registrar_smc) {
    //   console.log("ModalEditFieldClaim medical_registrar_smc = " + this.nextProps.medical_registrar_smc)
    // }
  }


  handleDateChange = (field, i) => (date) => {
    this.setState({
      [field]: date,
      new_value: GlobalHelpers.displayDate(date, 'yyyy-MM-dd')
    }, () => {
      // console.log(this.state[field])
    })


  }



  componentDidMount = () => {
  }

  handleBackPressed = () => {
    history.pushState(null, null, null);
    this.handleClose()
  }

  handleOpen = () => {
    this.setState({ open: true, edit_field_button_disabled: false });

    // Adding back listener
    history.pushState(null, null, null);
    window.addEventListener("popstate", this.handleBackPressed)
  };

  handleClose = () => {
    this.props.handleClose()
    this.setState({ open: false });

    // Removing back listener
    // history.go(-1)
    window.removeEventListener('popstate', this.handleBackPressed)

  };


  handleFieldChange = (prop) => e => {
    console.log("handleFieldChange()")
    if (prop === "password") {
      this.setState({
        [prop]: e.target.value
      }, () => {
        console.log(this.state), console.log("set for prop === password")
      })
    } else {
      this.setState({
        [prop]: e.target.value,
        new_value: e.target.value,
      }, () => {
        console.log(this.state)
      });
    }
  }



  // handleFieldChange = (prop) => e => {
  //   console.log("handleFieldChange()")

  //   console.log(e.target.value)
  //   console.log(Number.isInteger(e.target.value))

  //   const fields_with_type_number = ["personal_mobile_number", "entity_uen", "bank_acct_number", "mobile_number", "premiumback_bank_acct_number", "claims_bank_acct_number"]

  //   if (fields_with_type_number.indexOf(prop) != -1) {
  //     if ("" === e.target.value || (isFinite(e.target.value) && !e.target.value.indexOf('.') != -1)) {   // Only allows integers in type strings or int to go through
  //       // Do nothing
  //     } else {
  //       // console.log(e.target.value)
  //       // console.log("Not a number")
  //       return
  //     }
  //   }

  //   this.setState({
  //     [prop]: e.target.value
  //   }, () => {
  //     // console.log(this.state)
  //     // this.validate_special_small_accident_max_age()

  //   });


  // }



  handleOccupationFieldChange = (value, type, i) => {
    console.log("handleOccupationFieldChange()")

    console.log(value, type, i)
    switch (type) {
      case 'personal':
        this.setState({
          personal_occupation: value,
          new_value: value.label
        })
        break;

      case 'insured':
        let insured = GlobalHelpers.clone_deep(this.state.insured)
        insured[i].occupation = value
        this.setState({
          insured
        })
        break;

      default:
        // Should not happen
        break;
    }

  }


  handleEditField = () => {
    const password = this.state.password

    this.setState({
      edit_field_button_disabled: true
    }, () => {
      if (this.props.field_name === "bank" || this.props.field_name === "bank_claims" || this.props.field_name === "bank_premiumback") {
        this.props.handleEditField(password, this.props.accident_or_claim, this.props.id, this.props.field_name, [this.state.bank, this.state.bank_acct_number])
      } else {
        this.props.handleEditField(password, this.props.accident_or_claim, this.props.id, this.props.field_name, this.state.new_value)
      }
    })
  }

  fileUploadedPersonal = (uploaded_file_name) => {
    console.log("fileUploadedPersonal()", uploaded_file_name)
    let uploaded_images = GlobalHelpers.clone_deep(this.state.uploaded_images)
    uploaded_images.push(uploaded_file_name)
    this.setState({
      uploaded_images,
      new_value: uploaded_images
    }, () => { console.log(this.state.uploaded_images) })
  }


  fileRemovedPersonal = (uploaded_file_name) => {
    console.log("fileRemovedPersonal()", uploaded_file_name)
    let uploaded_images = GlobalHelpers.clone_deep(this.state.uploaded_images)
    // uploaded_images.push(uploaded_file_name)
    personal_uploaded_images.splice(uploaded_images.indexOf(uploaded_file_name), 1);   // Remove uploaded_file_name from the array uploaded_images

    this.setState({
      uploaded_images,
      new_value: uploaded_images

    }, () => { console.log(this.state.uploaded_images) })
  }


  handleSelectFieldChange = (select_type, value) => {
    console.log(select_type, value)
    let state_name = ""
    if (select_type === "select_doctor") { state_name = "doctor_seen" }
    else if (select_type === "select_clinic") { state_name = "clinic_visited" }

    try {
      this.setState({
        [state_name]: value.label,
        new_value: value.label
      })
    } catch (err) {
      console.log(err)
    }

  }


  renderContent = () => {

    let renderable = []

    let title_text = "Change" + this.props.field_title ? this.props.field_title : "Field"

    if (this.props.field_name === "uploaded_images") { title_text = "Upload Images" }

    const title =
      <Typography variant="h6" style={{ textAlign: "center" }}> {title_text} </Typography>


    const text_field =
      <div style={{ padding: "10px 0px 2px 0px" }}>
        <TextField
          label={"New " + (this.props.field_title ? this.props.field_title : "Value")}
          value={this.state.new_value}
          onChange={this.handleFieldChange('new_value')}
          type="text"
          fullWidth
        />
      </div>



    const relationship_to_payer =
      // <Element name={"relationship_to_payer_"+i} className="element">
      // <div onFocus={() => { this.scrollToElement("relationship_to_payer_"+i) }}>
      <FormControl style={{ marginBottom: "0px" }} fullWidth>
        <InputLabel style={{ width: "100%" }}>I am financially protecting my</InputLabel>
        <Select
          value={this.state.relationship_to_payer}
          onChange={this.handleFieldChange('relationship_to_payer')}
        >
          <MenuItem value={"child"}>Child</MenuItem>
          <MenuItem value={"spouse"}>Spouse</MenuItem>
          <MenuItem value={"parent"}>Parent</MenuItem>

        </Select>
      </FormControl>



    const gender =
      <div style={{ marginTop: "20px", textAlign: "center" }}>
        {/* <div style={{ textAlign: "center" }}> <Typography variant="subtitle1" style={{ margin: "0px 0px 0px 0px" }}> Gender </Typography>  </div> */}
        <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="gender" checked={this.state.gender === "M"} onChange={() => { this.setState({ gender: "M", new_value: "M" }) }} />} label="Male" />
        <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="gender" checked={this.state.gender === "F"} onChange={() => { this.setState({ gender: "F", new_value: "F" }) }} />} label="Female" />
      </div>


    const date =
      <DatePickerRMC
        date={this.state.date}
        handleDateChange={this.handleDateChange("date")}
        placeholder={this.props.field_title}
        defaultDate="1992-01-01"  // default = today
        minDate="1850-01-01"
        useBrowserDatePicker={true}
      />

    const place_of_birth =
      <SelectCountry country={this.state.place_of_birth} handleCountryFieldChange={(value) => { this.setState({ place_of_birth: value, new_value: value.value }), () => { console.log(this.state) } }} type="personal" placeholder="Place of Birth (as of NRIC/Passport)"></SelectCountry>



    const mobile_number =
      <TextField
        style={{ marginTop: "4px", }}
        label="Mobile Number"
        value={this.state.personal_mobile_number}
        onChange={this.handleFieldChange('personal_mobile_number')}
        fullWidth

      />


    const occupation =
      <div style={{ textAlign: "center" }}>
        <SelectOccupation occupation={this.state.personal_occupation} handleOccupationFieldChange={this.handleOccupationFieldChange} type="personal" placeholder="Select Occupation"></SelectOccupation>
      </div>



    const self_employed =
      <div style={{ marginTop: "20px", textAlign: "center", }}>
        <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="personal_self_employed" checked={!this.state.personal_self_employed} onChange={() => { this.setState({ personal_self_employed: false, new_value: false }) }} />} label="Employed" />
        <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="personal_self_employed" checked={this.state.personal_self_employed} onChange={() => { this.setState({ personal_self_employed: true, new_value: true }) }} />} label="Self-employed" />
      </div>



    const bank_details =
      <div>
        <div style={{ textAlign: "left", }}> <Typography variant="subtitle1" style={{ margin: "20px 0px 0px 0px" }}> Bank </Typography>  </div>
        <FormControl style={{ width: "100%", marginTop: "0px" }}>
          {/* <InputLabel>Bank (Singapore)</InputLabel> */}
          <Select
            style={{ textAlign: "left" }}
            value={this.state.bank}
            onChange={this.handleFieldChange("bank")}
          >
            <MenuItem value={"dbs"}>DBS/POSB (preferred)</MenuItem>
            <MenuItem value={"ocbc"}>OCBC</MenuItem>
            <MenuItem value={"uob"}>UOB</MenuItem>
            <MenuItem value={"sc"}>Standard Charted</MenuItem>
            <MenuItem value={"hsbc"}>HSBC </MenuItem>
            <MenuItem value={"citi"}>Citibank</MenuItem>
            <MenuItem value={"maybank"}>Maybank</MenuItem>
            <MenuItem value={"rhb"}>RHB</MenuItem>
            <MenuItem value={"cimb"}>CIMB</MenuItem>
            <MenuItem value={"bo_china"}>Bank of China</MenuItem>
            <MenuItem value={"deutsche"}> Deutsche Bank </MenuItem>
            <MenuItem value={"hl_bank"}> HL Bank</MenuItem>
            <MenuItem value={"bnp_paribas"}> BNP Paribas</MenuItem>
            <MenuItem value={"far_eastern"}>  Far Eastern Bank</MenuItem>
            <MenuItem value={"mizuho"}> Mizuho Bank</MenuItem>
            <MenuItem value={"sumitomo_mitsui"}>Sumitomo Mitsui Banking Corporation </MenuItem>
            <MenuItem value={"bo_tokyo_mitsubishi_ufj"}> The Bank of Tokyo-Mitsubishi UFJ</MenuItem>
            <MenuItem value={"bo_scotland"}> The Royal Bank of Scotland</MenuItem>
            <MenuItem value={"aus_nz_banking_grp"}> Australia and New Zealand Banking Group</MenuItem>


          </Select>
        </FormControl>


        <TextField style={{ margin: "4px 0px 0px 0px" }}
          label="Bank account number (Singapore)"
          value={this.state.bank_acct_number}
          onChange={this.handleFieldChange('bank_acct_number')}
          fullWidth
        // type="number"
        />
        {/* <FormControlLabel
          control={
            <Checkbox
              checked={this.state.personal_ack_50_cents_fee}
              onChange={this.handleCheckboxToggle('personal_ack_50_cents_fee')}
              value="ack_50_cents_fee"
              color="primary"
            />
          }
          // label={<span style={{ color: "rgba(0, 0, 0, 0.54)", fontSize: "13px", lineHeight: 0 }}>I acknowledge that a fee of $0.50 will be imposed on all Premiumback & Claims payouts. FREE for payouts to DBS/POSB accounts</span>}
          label={<span style={{ color: "rgba(0, 0, 0, 0.54)", fontSize: "13px", lineHeight: 0 }}>I am aware that if the Singapore bank account I provide is not a POSB/DBS bank account, the bank will impose a 50 cents charge to every transaction ( Claims and PremiumBack ) I receive from www.iFinSG.com.</span>}

          style={{ margin: "30px 0px 0px 0px" }}
        /> */}
      </div>


    const upload_image =
      <div>
        <div style={{ textAlign: "center" }}> <Typography variant="subtitle1" style={{ margin: "30px 0px 10px 0px" }}>Upload NRIC/Passport (front & back): </Typography>  </div>
        <ImageUpload image_type="SIGNUP-IMAGE" username={this.props.username} maxFiles={3} type={"personal"} fileUploadedPersonal={this.fileUploadedPersonal} fileRemovedPersonal={this.fileRemovedPersonal} />
      </div>


    const password =
      <div style={{ padding: "10px 0px 2px 0px" }}>
        <TextField
          label="Password (for security)"
          value={this.state.password}
          onChange={this.handleFieldChange('password')}
          type="password"
          fullWidth
          onKeyPress={(ev) => { if (ev.key === 'Enter') { this.handleEditField() } }}
        />
      </div>


    const select_doctor =
      <SelectDoctor handleSelectFieldChange={this.handleSelectFieldChange} medical_registrar_smc={this.props.medical_registrar_smc} />

    const select_clinic =
      <SelectClinic handleSelectFieldChange={this.handleSelectFieldChange} medical_registrar_smc={this.props.medical_registrar_smc} />


    const medical_registrar_smc =
      // <div>HIIIIIIIII</div>
      <div style={{ textAlign: "center", marginTop: '20px' }}>
        <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === true} onChange={() => { this.setState({ medical_registrar_smc: true, new_value: true }, () => { console.log(this.state.medical_registrar_smc) }) }} />} label="Western Doctor" />
        <FormControlLabel style={{ margin: "0px 10px 0px 10px" }} control={<Radio style={{ padding: "6px" }} name="medical_registrar_smc" checked={this.state.medical_registrar_smc === false} onChange={() => { this.setState({ medical_registrar_smc: false, new_value: false }, () => { console.log(this.state.medical_registrar_smc) }) }} />} label="TCM Doctor" />
      </div>

    ///////////////////////////////////////////////
    /////////  ACCIDENT AND CLAIM FIELDS  /////////
    ///////////////////////////////////////////////


    // const accident_date =
    // <DatePickerRMC
    //   date={this.state.date}
    //   handleDateChange={this.handleDateChange("date")}
    //   placeholder="Date of Birth"
    //   defaultDate="1992-01-01"  // default = today
    //   minDate="1850-01-01"
    // />


    const change_button =
      <Button disabled={this.state.edit_field_button_disabled} style={{ float: "right" }} onClick={() => { this.handleEditField() }}> Change </Button>


    console.log(this.props.field_name)
    console.log("FIELD NAMEEEEEEEEEEEEEEEEEEEEEEEEE")

    let field_to_add = text_field
    switch (this.props.field_name) {
      case "mobile_number":
        field_to_add = mobile_number
        break;
      case "occupation":
        field_to_add = occupation
        break;
      case "self_employed":
        field_to_add = self_employed
        break;
      case "bank":
        field_to_add = bank_details
        break;


      case "relationship_to_payer":
        field_to_add = relationship_to_payer
        break;
      case "gender":
        field_to_add = gender
        break;
      case "dob":
        field_to_add = date
        break;
      case "place_of_birth":
        field_to_add = place_of_birth
        break;
      case "upload_image":
        field_to_add = upload_image
        break;
      case "bank_claims":
        field_to_add = bank_details
        break;
      case "bank_premiumback":
        field_to_add = bank_details
        break;
      case "uploaded_images":
        field_to_add = upload_image
        break;


      case "accident_date":
        field_to_add = date
        break;
      case "clinic_visited":
        field_to_add = select_clinic
        break;
      case "doctor_seen":
        field_to_add = select_doctor
        break;
      case "clinic_visit_date":
        field_to_add = date
        break;
      case "medical_registrar_smc":
        field_to_add = medical_registrar_smc
        break;
      default:
        field_to_add = text_field
        break;
    }
    renderable = [title, field_to_add, password, change_button]

    return (<div style={{ padding: "10px 25px 15px 25px", textAlign: "center", }}>
      {renderable}
    </div>)
  }

  render() {
    const { classes } = this.props;

    return (
      <div>
        {/* <Typography gutterBottom>Click to get the full Modal experience!</Typography>
        <Button onClick={this.handleOpen}>Open Modal</Button> */}

        <NoSsr>

          <Modal
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={this.state.open}
            onClose={this.handleClose}
          >
            <div style={getModalStyle()} className={classes.paper}>

              {this.renderContent()}

              {/* <SimpleModalWrapped /> */}
            </div>
          </Modal>
        </NoSsr>

      </div>
    );
  }
}

SimpleModal.propTypes = {
  classes: PropTypes.object.isRequired,
};

// We need an intermediary variable for handling the recursive nesting.
const SimpleModalWrapped = withStyles(styles)(SimpleModal);

export default SimpleModalWrapped;