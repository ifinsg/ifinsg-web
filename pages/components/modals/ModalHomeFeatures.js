import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary } from '@material-ui/core';
import { NoSsr, Button, Typography, Checkbox, Grid, Paper, FormControlLabel, Modal, IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";
import Constants from 'constants/constants';
import Tick from 'components/icons/Tick';
import Router from 'next/router';
import Link from "next/link";


const name_display = (policy) => {
  return Constants.product_details[policy].name
}


const price_display = (policy, price_component, period, line_break) => {
  let price_original = -1
  let price_promotion = -1
  let on_promotion = false

  switch (price_component) {
    case "premium":
      price_original = Constants.product_details[policy].premium
      if (Constants.product_details[policy].premium_on_promotion) {
        price_promotion = Constants.product_details[policy].premium_promotional_price
        on_promotion = true
      }
      break;
    case "admin_fee":
      price_original = Constants.product_details[policy].admin_fee
      if (Constants.product_details[policy].admin_fee_on_promotion) {
        price_promotion = Constants.product_details[policy].admin_fee_promotional_price
        on_promotion = true
      }
      break;
    case "total":
      price_original = Constants.product_details[policy].total
      if (Constants.product_details[policy].total_on_promotion) {
        price_promotion = Constants.product_details[policy].total_promotional_price
        on_promotion = true
      }
      break;
    default:
      break;
  }



  if (on_promotion) {
    return (
      <span>
        {/* <span style={{ textDecoration: "line-through" }}>${(period * price_original).toFixed(2)}</span> {line_break ? <br></br> : ""} */}
        <span style={{ color: Constants.color.green, fontWeight: "bold" }}> <i>${(period * price_promotion).toFixed(2)}</i> </span>
      </span>
    )
  } else {
    return <span>${(period * price_original).toFixed(2)}  </span>
  }
}


function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  // const top = 50 + rand();
  // const left = 50 + rand();
  const top = 50
  const left = 50
  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
    position:"relative"
  };
}

const styles = theme => {
  let width = '85%'

  if (isBrowser) {
    width = '60%'
  }

  return ({
    paper: {
      position: 'absolute',
      width: width,// theme.spacing.unit * 50,
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing.unit * 4,
      outline: 'none',
      overflow: 'scroll',
      maxHeight: '90%',
    },
  })
}

class SimpleModal extends React.Component {
  state = {
    open: this.props.open,
  };


  componentWillReceiveProps = (nextProps) => {
    if (this.props.open === false && nextProps.open === true) {
      this.handleOpen()
    }
  }



  handleBackPressed = () => {
    history.pushState(null, null, null);
    this.handleClose()
  }

  handleOpen = () => {
    this.setState({ open: true });

    // Adding back listener
    history.pushState(null, null, null);
    window.addEventListener("popstate", this.handleBackPressed)
  };

  handleClose = () => {

    // this.props.handleClose()
    this.setState({ open: false });

    // Removing back listener
    history.go(-1)
    window.removeEventListener('popstate', this.handleBackPressed)
  };

  renderPopoverContent() {
      let renderable = []

      const close_button =
      <IconButton onClick={() => { this.handleClose() }} style={{ position:"absolute", right: "0px", top:"0px" }}><CloseIcon /> </IconButton>

    renderable.push(close_button)

    switch (this.props.feature) {
      case "price":
          let policy = this.props.policy
          let cycle_length = Constants.product_details[policy].cycle_length

          renderable.push(
            <div style={{width:"100%", textAlign:"center"}}>
              <div style={{maxWidth:"300px", display:"inline-block"}}>

              <div style={{ margin: '10px 3px 0px 3px', fontSize: '19px', fontWeight: 'bold', lineHeight: '1', color: '#7cda24', minHeight: "40px" }}>Cyclic: {name_display(policy)} </div>

                <table style={{ marginLeft: "auto", marginRight: "auto", width: isBrowser ? "90%" : "100%" }}>
                  <tbody style={{ textAlign: "center", width: "100%" }}>
                    {/* <tr > <td colspan="2" style={{ textAlign: "center", paddingTop: "0px", fontSize: "16px", fontWeight:"bold" }}>Per 3 Months</td> </tr> */}
                    <tr> <td style={{ width: "55%", textAlign: "left", paddingLeft: "5px" }}>Premium</td> <td style={{ textAlign: "right", paddingRight: "5px" }}>{price_display(policy, "premium", cycle_length, true)} </td> </tr>
                    <tr style={{ height: "5px", padding: 0, margin: 0 }}> <td colspan="2" style={{ padding: "0px", margin: "0px", textAlign: "right", padding: "0px 5px 0px 1px", fontSize: "17px", transform: "translateY(-5px)", color: Constants.color.gold }}> ({price_display(policy, "premium", 1)} per month) </td> </tr>

                    <tr> <td style={{ width: "55%", textAlign: "left", paddingLeft: "5px" }}>Admin Fee</td> <td style={{ textAlign: "right", paddingRight: "5px" }}>{price_display(policy, "admin_fee", cycle_length, true)}</td> </tr>
                    <tr style={{ height: "5px", padding: 0, margin: 0 }}> <td colspan="2" style={{ padding: "0px", margin: "0px", textAlign: "right", padding: "0px 5px 0px 1px", fontSize: "17px", transform: "translateY(-5px)", color: Constants.color.gold }}> ({price_display(policy, "admin_fee", 1)} per month) </td> </tr>
                    {/* <tr style={{ height: "5px", padding: 0, margin: 0 }}> <td colspan="2" style={{ padding: "0px", margin: "0px", textAlign: "right", padding: "0px 5px 0px 1px", fontSize: "13px", transform: "translateY(-8px)", color: Constants.color.darkgreen }}><b>40% discount!</b></td> </tr> */}

                    <tr> <td style={{ width: "55%", textAlign: "left", paddingLeft: "5px", fontWeight: "bold", color: Constants.color.darkgreen }}>Total</td> <td style={{ textAlign: "right", paddingRight: "5px", fontWeight: "bold", color: Constants.color.darkgreen }}>{price_display(policy, "total", cycle_length, true)}</td> </tr>
                    <tr > <td colspan="2" style={{ textAlign: "right", padding: "5px 5px 1px 1px", fontSize: "13px", fontWeight: "bold", transform: "translateY(-6px)", color: Constants.color.darkgreen, lineHeight: 1.1 }}>per Cycle of <span style={{ fontStyle: "italic", color: cycle_length == 6 ? "rgb(50, 67, 183)" : "rgb(134, 149, 253)" }}>{cycle_length} months</span></td> </tr>
                    <tr style={{ height: "5px", padding: 0, margin: 0 }}> <td colspan="2" style={{ padding: "0px", margin: "0px", textAlign: "right", padding: "0px 5px 0px 1px", fontSize: "17px", transform: "translateY(-5px)", color: Constants.color.gold }}> ({price_display(policy, "total", 1, true)} per month) </td> </tr>


                    {/* text-shadow: 0 0 1px #fff, 0 0 2px #fff, 0 0 2px #00ff39, 0 0 4px #00ff39, 0 0 5px #00ff39, 0 0 2px #00ff39, 0 0 2px #00ff39;                 */}
                  </tbody>
                </table>
              </div>
            </div>
            )
      break;
      case "policy_details_small_accident":
        renderable.push(
          <div style={{ textAlign: "center", wordWrap: "break-word" }}>
            <div style={{ margin: '10px 10px 0px 10px', fontSize: '25px', fontWeight: 'bold', lineHeight: '1', color: '#7cda24', transform: "translateX(20px)" }}>  Cyclic: Small Accident</div>

            <div style={{ width: "100%", fontSize: "15px" }}>
              <div style={{ padding: "0px 20px 5px 0px", textAlign: "left" }}>

                <div style={{ margin: '10px 0px 0px 0px', fontWeight: "bold" }}>(1) Benefits </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} >Reimburses you up to $1,500 per accident (even for medical treatment of sprains, cuts or falls), covering All sports and has no Reduction of claims for any sport.</div>
                </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} > Additional benefits: includes TCM, food poisoning, insect/animal attacks, zika, dengue fever, and burns/scalds.</div>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontWeight: "bold" }}>(2) Claims </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} >Accident definition: Violent, Visible, External, Impact. </div>
                </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  {/* <div style={{ margin: '0px 0px 0px 35px' }} > Covers only new accidents which happen after your cover is Approved with us, and the Doctor must write clearly on the Receipt / Invoice / MC, the Date and Description of the Accident which occurred.</div> */}
                  <div style={{ margin: '0px 0px 0px 35px' }} > Covers only new accidents which happen after your cover is approved with us, and the SMC-registered / TCMPB-registered doctor ( practicing in Singapore ) must write clearly on the Receipt / Invoice / MC, the Date and Description of the accident which occurred.</div>
                </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} >Covers only treatments which take place within 3 months after the Date of each Accident. </div>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontWeight: "bold" }}>(3) Special Notes </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} >Valid till user is age 77. </div>
                </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} > Treatment for accidents in All countries, 24/7 worldwide ( outside of Singapore, we accept claims from the A&E department of public hospitals only ). </div>
                </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} > User must have a Singapore Bank account to apply for this.</div>
                </div>
                {/* <div style={{ marginTop: "20px" }}>
              <div style={{ float: "left" }}> <Tick /> </div>
              <div style={{ margin: '0px 0px 0px 35px' }} >For doctors who sign up as a user with us, if you receive treatment from a TCMPB registered doctor who happens to be your spouse, relative, colleague or your employee, we will still allow you to make claims, up to 2 TCM treatments per Accident. ( claims for any treatment administered by yourself for your own injury, will not be valid. )</div>
            </div> */}
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} >This product is not currently not available for SMC-registered / TCMPB-registered doctors to purchase.</div>
                </div>





                <div style={{ margin: '40px 0px 0px 0px', fontWeight: "bold" }}>(4) Exclusions </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} >Treatment for old accidents which happened before your cover is Approved with us. </div>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontWeight: "bold" }}>(5) Low Claims definition </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} >Users who claim SGD$60 or less per cycle. </div>
                </div>


                <div style={{ margin: '30px 0px 0px 0px' }}> * Other Accident policies normally restrict Sports cover, by a Reduction of Claims for high-risk sports, or by only covering certain Sports.</div>
                <div style={{ margin: '30px 0px 0px 0px' }}> * Other Accident policies normally restrict the Countries available, by excluding your protection when you travel to North Korea, Iran, Cuba and other countries, while we provide cover in Any country which you travel to.</div>


              </div>
            </div>




            <div style={{ textAlign: "left", marginTop: "50px" }}>
              <div style={{ margin: '10px 10px 0px 10px', fontSize: '25px', fontWeight: 'bold', lineHeight: '1', color: '#7cda24', textAlign: "center" }}> HOW TO CLAIM </div>


              <div style={{ margin: '20px 0px 0px 0px', textAlign: "center" }}> [Cyclic: Small Accident] </div>

              <div style={{ margin: '20px 0px 0px 0px' }}>When you need to make a claim, just:</div>
              <div style={{ margin: '20px 0px 0px 0px' }}>1. Tell us:</div>
              <div style={{ margin: '10px 0px 0px 15px' }}> (i) What happened in the incident,</div>
              <div style={{ margin: '0px 0px 0px 15px' }}> (ii) Who was the SMC-registered / TCMPB-registered Doctor you consulted, </div>
              <div style={{ margin: '0px 0px 0px 15px' }}> (iii) Which clinic / hospital you visited.</div>

    
              <div style={{ margin: '20px 0px 0px 0px' }}>2. Upload photos of:</div>
              <div style={{ margin: '10px 0px 0px 15px' }}> (i) Paid Receipt, </div>
              <div style={{ margin: '0px 0px 0px 15px' }}> (ii) Itemized Invoice of treatment and medicine,</div>
              <div style={{ margin: '0px 0px 0px 15px' }}> (iii) MC with diagnosis / Letter of diagnosis.</div>


              <div style={{ margin: '40px 0px 0px 0px' }}>* Just make sure the Doctor writes the <u>Date of accident</u> + <u>Description of accident</u>, on any 1 of the documents in the photos which you upload ( either Receipt / Invoice / MC with diagnosis / Letter of diagnosis ).</div>
              <div style={{ margin: '40px 0px 0px 0px' }}>* If you received treatment from the A&E department of a Public Hospital outside of Singapore, please upload photos of the entry and exit stamps in your Passport as evidence that you were in that country. </div>
              <div style={{ margin: '40px 0px 0px 0px' }}>SMC is the Singapore Medical Council ( a statutory board under the Ministry of Health ), governed under Section 5 of the Medical Registration Act (Cap 174). You can check if your doctor is SMC registered, via this link:  <a href={"https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC"} target={"_blank"} style={{ color: "#7cda24", wordWrap: "break-word" }}> <span style={{ wordWrap: "break-word" }}> https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC </span></a>.</div>
              <div style={{ margin: '40px 0px 0px 0px' }}>TCMPB is the Traditional Chinese Medicine Practitioners Board ( a statutory board under the charge of the Ministry of Health ), under the Traditional Chinese Medicine Practitioners Act 2000, governed under the Traditional Chinese Medicine Practitioners Act (Cap. 333A). You can check if your doctor is TCMPB registered, via this link: <a href={"https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=TCM"} target={"_blank"} style={{ color: "#7cda24" }}>https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=TCM</a>.</div>

              <div style={{ margin: '40px 0px 0px 0px' }}> In the event that an overseas medical service provider is involved, we will contact them to verify the authenticity. </div>

              <div style={{ margin: '40px 0px 0px 0px' }}> At all times, if there are any extra costs involved in the claims verification process (such as translation costs), we may either bill you and deduct it from your claim payout, or bill you separately if the claim cannot be substantiated. </div>
              <div style={{ margin: '40px 0px 0px 0px' }}> * On the Date of any Claim Submission, your policy cover must be active in order for it to be a processable Claim.</div>
              <div style={{ margin: '10px 0px 0px 0px' }}>ie. If your policy cover is no longer active for any reason, a new Claim Submission will not be entertained.


            {
                  this.props.page === "products" ?
                    <div style={{ textAlign: "center" }}>
                      <Link href={Constants.get_new_page("claims", this.state.referrer)}>
                        <Button variant="contained" style={{ margin: '30px 0px 0px 0px', padding: '10px 15px 10px 15px', fontSize: '14px', backgroundColor: '#7cda24', color: 'white', textAlign: "center" }}>Still unsure about claims?</Button>
                      </Link>
                    </div>
                    : ""
                }

              </div>

            </div>

          </div>
        )
        break;
      case "policy_details_medium_accident":
        renderable.push(
          <div style={{ textAlign: "center", wordWrap: "break-word" }}>
          <div style={{ margin: '10px 10px 0px 10px', fontSize: '25px', fontWeight: 'bold', lineHeight: '1', color: '#7cda24', transform: "translateX(20px)" }}>
            <div>Cyclic: Medium Accident</div>
            {/* <div style={{ fontWeight: "normal", marginTop: "20px", display: "inline-block", fontSize: "17px", fontFamily: "tahoma", color: "grey" }}> Because you can't Control accidents </div> */}
          </div>
  
  
          <div style={{ width: "100%", fontSize: "15px" }}>
            <div style={{ padding: "0px 20px 5px 0px", textAlign: "left" }}>
  
              <div style={{ margin: '10px 0px 0px 0px', fontWeight: "bold" }}>(1) Benefits </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} >Reimburses you up to $4,000 per accident (even for medical treatment of sprains, cuts or falls), covering All sports and has no Reduction of claims for any sport.</div>
              </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} > Additional benefits: includes TCM, food poisoning, insect/animal attacks, zika, dengue fever, and burns/scalds. </div>
              </div>
  
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} > Transport benefits: reimburses you up to $15 per Ride Hailing trip, To-and-Fro for the 1st treatment of each accident.</div>
              </div>
  
  
              <div style={{ margin: '40px 0px 0px 0px', fontWeight: "bold" }}>(2) Claims </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} >Accident definition: Violent, Visible, External, Impact. </div>
              </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                {/* <div style={{ margin: '0px 0px 0px 35px' }} > Covers only new accidents which happen after your cover is Approved with us, and the Doctor must write clearly on the Receipt / Invoice / MC, the Date and Description of the Accident which occurred.</div> */}
                <div style={{ margin: '0px 0px 0px 35px' }} > Covers only new accidents which happen after your cover is approved with us, and the SMC-registered / TCMPB-registered doctor ( practicing in Singapore ) must write clearly on the Receipt / Invoice / MC, the Date and Description of the accident which occurred.</div>
              </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} > Covers only treatments which take place within 3 months after the Date of each Accident. </div>
              </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} >Transport benefits: the receipt of the Ride Hailing trip to the 1st treatment must display the clinic / hospital address as the exact Destination location; while the receipt of the Ride Hailing trip to leave from the 1st treatment must display the clinic / hospital address as the exact Pickup location.</div>
              </div>
  
  
              <div style={{ margin: '40px 0px 0px 0px', fontWeight: "bold" }}>(3) Special Notes </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} >Valid till user is age 77. </div>
              </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} > Treatment for accidents in All countries, 24/7 worldwide ( outside of Singapore, we accept claims from the A&E department of public hospitals only ).  </div>
              </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} > User must have a Singapore Bank account to apply for this.</div>
              </div>
              {/* <div style={{ marginTop: "20px" }}>
          <div style={{ float: "left" }}> <Tick /> </div>
          <div style={{ margin: '0px 0px 0px 35px' }} >For doctors who sign up as a user with us, if you receive treatment from a TCMPB registered doctor who happens to be your spouse, relative, colleague or your employee, we will still allow you to make claims, up to 2 TCM treatments per Accident. ( claims for any treatment administered by yourself for your own injury, will not be valid. )</div>
        </div> */}
              {/* <div style={{ marginTop: "20px" }}>
          <div style={{ float: "left" }}> <Tick /> </div>
          <div style={{ margin: '0px 0px 0px 35px' }} >This product is not currently not available for SMC-registered / TCMPB-registered doctors to purchase.</div>
        </div> */}
  
              <div style={{ margin: '40px 0px 0px 0px', fontWeight: "bold" }}>(4) Exclusions </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} >Treatment for old accidents which happened before your cover is Approved with us. </div>
              </div>
  
              <div style={{ margin: '40px 0px 0px 0px', fontWeight: "bold" }}>(5) Low Claims definition </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} >Users who claim SGD$100 or less per cycle. </div>
              </div>
  
              <div style={{ margin: '60px 10px 20px 10px', fontSize: '25px', fontWeight: 'bold', lineHeight: '1', color: '#7cda24', textAlign: "center" }}> How are we different </div>
  
              <div style={{ margin: '10px 0px 0px 0px', fontWeight: "bold", color: Constants.color.darkgreen }}> * We cover you even when you are on a PMD for work.</div>
              <div style={{ margin: '10px 0px 0px 0px', fontWeight: "bold", color: Constants.color.darkgreen }}> * We do not have a Reduction of Claims for high-risk sports, and we do not cover only a restrictive range of sports.</div>
              <div style={{ margin: '10px 0px 0px 0px', fontWeight: "bold", color: Constants.color.darkgreen }}> * We cover you when you travel to exotic places such as North Korea, Iran, and Cuba. </div>
              <div style={{ margin: '10px 0px 0px 0px', fontWeight: "bold", color: Constants.color.darkgreen }}> * We cover you even if your accident occured during a competition / marathon. </div>
              <div style={{ margin: '10px 0px 0px 0px', fontWeight: "bold", color: Constants.color.darkgreen }}> * We reimburse you up to $15 per ride hailing trip, To-and-Fro for the 1st treatment of each accident. </div>
  
            </div>
          </div>
  
  
  
  
          <div style={{ textAlign: "left", marginTop: "50px" }}>
            <div style={{ margin: '10px 10px 0px 10px', fontSize: '25px', fontWeight: 'bold', lineHeight: '1', color: '#7cda24', textAlign: "center" }}> HOW TO CLAIM </div>
  
  
            <div style={{ margin: '20px 0px 0px 0px', textAlign: "center" }}> [Cyclic: Medium Accident] </div>
  
            <div style={{ margin: '20px 0px 0px 0px' }}>When you need to make a claim, just:</div>
            <div style={{ margin: '20px 0px 0px 0px' }}>1. Tell us:</div>
            <div style={{ margin: '20px 0px 0px 0px' }}>
              <ol type="i">
                <li>What happened in the incident,</li>
                <li>Who was the SMC-registered / TCMPB-registered Doctor you consulted,</li>
                <li>Which clinic / hospital you visited.</li>
              </ol>
            </div>
  
            <div style={{ margin: '20px 0px 0px 0px' }}>2. Upload photos of:</div>
            <div style={{ margin: '20px 0px 0px 0px' }}>
              <ol type="i">
                <li>Paid Receipt,</li>
                <li>Itemized Invoice of treatment and medicine,</li>
                <li>MC with diagnosis / Letter of diagnosis,</li>
                <li>Ride Hailing receipt for the journeys To-and-Fro for the 1st treatment of each accident.</li>
              </ol>
            </div>
  
  
            <div style={{ margin: '40px 0px 0px 0px' }}>* Just make sure the Doctor writes the <u>Date of accident</u> + <u>Description of accident</u>, on any 1 of the documents in the photos which you upload ( either Receipt / Invoice / MC with diagnosis / Letter of diagnosis ).</div>
            <div style={{ margin: '40px 0px 0px 0px' }}>* If you received treatment from the A&E department of a Public Hospital outside of Singapore, please upload photos of the entry and exit stamps in your Passport as evidence that you were in that country. </div>
            <div style={{ margin: '40px 0px 0px 0px' }}>* Just make sure the receipt of the Ride Hailing trip to the 1st treatment must display the clinic / hospital address as the exact Destination location; while the receipt of the Ride Hailing trip to leave from the 1st treatment must display the clinic / hospital address as the exact Pickup location.</div>
  
            <div style={{ margin: '40px 0px 0px 0px' }}>SMC is the Singapore Medical Council ( a statutory board under the Ministry of Health ), governed under Section 5 of the Medical Registration Act (Cap 174). You can check if your doctor is SMC registered, via this link:  <a href={"https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC"} target={"_blank"} style={{ color: "#7cda24", wordWrap: "break-word" }}> <span style={{ wordWrap: "break-word" }}> https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC </span></a>.</div>
            <div style={{ margin: '40px 0px 0px 0px' }}>TCMPB is the Traditional Chinese Medicine Practitioners Board ( a statutory board under the charge of the Ministry of Health ), under the Traditional Chinese Medicine Practitioners Act 2000, governed under the Traditional Chinese Medicine Practitioners Act (Cap. 333A). You can check if your doctor is TCMPB registered, via this link: <a href={"https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=TCM"} target={"_blank"} style={{ color: "#7cda24" }}>https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=TCM</a>.</div>
  
            <div style={{ margin: '40px 0px 0px 0px' }}> In the event that an overseas medical service provider is involved, we will contact them to verify the authenticity. </div>
  
            <div style={{ margin: '40px 0px 0px 0px' }}> At all times, if there are any extra costs involved in the claims verification process (such as translation costs), we may either bill you and deduct it from your claim payout, or bill you separately if the claim cannot be substantiated. </div>
            <div style={{ margin: '40px 0px 0px 0px' }}> * On the Date of any Claim Submission, your policy cover must be active in order for it to be a processable Claim.</div>
            <div style={{ margin: '10px 0px 0px 0px' }}>ie. If your policy cover is no longer active for any reason, a new Claim Submission will not be entertained.
  
              {
                this.props.page === "products" ?
                  <div style={{ textAlign: "center" }}>
                    <Link href={Constants.get_new_page("claims", this.props.referrer)}>
                      <Button variant="contained" style={{ margin: '30px 0px 0px 0px', padding: '10px 15px 10px 15px', fontSize: '14px', backgroundColor: '#7cda24', color: 'white', textAlign: "center" }}>Still unsure about claims?</Button>
                    </Link>
                  </div>
                  : ""
              }
  
            </div>
  
          </div>
  
        </div>
        )
        break;
      case "policy_details_small_influenza_gp":
        renderable.push(
          <div style={{ textAlign: "center", wordWrap: "break-word" }}>
            <div style={{ margin: '10px 10px 0px 10px', fontSize: '25px', fontWeight: 'bold', lineHeight: '1', color: '#7cda24', minHeight: "50px", transform: "translateX(20px)" }}>  Cyclic: Small Influenza GP</div>


            <div style={{ textAlign: "left", fontSize: "15px" }}>
              <div style={{ margin: '20px 0px 0px 0px', fontWeight: "bold" }}>(1) Benefits </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} >  Reimburses you up to $70 per new diagnosis of Influenza ( Flu ) at a GP clinic / government polyclinic in Singapore. </div>
              </div>


              <div style={{ margin: '40px 0px 0px 0px', fontWeight: "bold" }}>(2) Claims </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} > Covers Seasonal Flu, Influenza A, Influenza B, Influenza C, H &amp; N Subtypes, Flu Pandemic, Swine Flu (H1N1), Bird Flu (H5N1).  </div>
              </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} >Each claim must be at least 20 days apart from each other.</div>
              </div>

              <div style={{ margin: '40px 0px 0px 0px', fontWeight: "bold" }}>(3) Special Notes </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} >  Valid till user is age 55. </div>
              </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} >Treatment must be provided by a GP clinic / government polyclinic, by a SMC-registered doctor ( practicing in Singapore ).   </div>
              </div>          <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} > User must have a Singapore Bank account to apply for this.  </div>
              </div>


              <div style={{ margin: '40px 0px 0px 0px', fontWeight: "bold" }}>(4) Exclusions </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} > Claims from treatments in Specialist clinics and Hospitals.</div>
              </div>

              <div style={{ margin: '40px 0px 0px 0px', fontWeight: "bold" }}>(5) Low Claims definition </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} > Users who claim SGD$50 or less per cycle. </div>
              </div>

              <div style={{ margin: '60px 10px 20px 10px', fontSize: '25px', fontWeight: 'bold', lineHeight: '1', color: '#7cda24', textAlign: "center" }}> How are we different </div>
              <div style={{ margin: '10px 0px 0px 0px', fontWeight: "bold", color: Constants.color.darkgreen }}> * There is currently No policy like this in Singapore.</div>

            </div>


            <div style={{ textAlign: "left", marginTop: "50px" }}>
              <div style={{ margin: '10px 10px 0px 10px', fontSize: '25px', fontWeight: 'bold', lineHeight: '1', color: '#7cda24', textAlign: "center" }}> HOW TO CLAIM </div>


              <div style={{ margin: '20px 0px 0px 0px', textAlign: "center" }}> [Cyclic: Small Influenza GP] </div>

              <div style={{ margin: '20px 0px 0px 0px' }}>When you need to make a claim, just:</div>
              <div style={{ margin: '20px 0px 0px 0px' }}>1. Tell us:</div>
            <div style={{ margin: '10px 0px 0px 15px' }}> (i) What happened in the incident,</div>
            <div style={{ margin: '0px 0px 0px 15px' }}> (ii) Who was the SMC-registered Doctor you consulted,  </div>
            <div style={{ margin: '0px 0px 0px 15px' }}> (iii) Which GP clinic / government polyclinic you visited. </div>

            <div style={{ margin: '20px 0px 0px 0px' }}>2. Upload photos of:</div>
            <div style={{ margin: '10px 0px 0px 15px' }}> (i) Paid Receipt, </div>
            <div style={{ margin: '0px 0px 0px 15px' }}> (ii) Itemized Invoice of treatment and medicine,</div>
            <div style={{ margin: '0px 0px 0px 15px' }}> (iii) MC with diagnosis / Letter of diagnosis.</div>

              <div style={{ margin: '40px 0px 0px 0px' }}>* Just make sure the Doctor writes the <u>Date of diagnosis</u> + <u>Description of diagnosis</u>, on any 1 of the documents in the photos which you upload ( either Receipt / Invoice / MC with diagnosis / Letter of diagnosis ).</div>
              <div style={{ margin: '40px 0px 0px 0px' }}>SMC is the Singapore Medical Council ( a statutory board under the Ministry of Health ), governed under Section 5 of the Medical Registration Act (Cap 174). You can check if your doctor is SMC registered, via this link:  <a href={"https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC"} target={"_blank"} style={{ color: "#7cda24", wordWrap: "break-word" }}> <span style={{ wordWrap: "break-word" }}> https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC </span></a>.</div>
              <div style={{ margin: '40px 0px 0px 0px' }}>  * On the Date of any Claim Submission, your policy cover must be active in order for it to be a processable Claim.  </div>
              <div style={{ margin: '10px 0px 0px 0px' }}> ie. If your policy cover is no longer active for any reason, a new Claim Submission will not be entertained. </div>

              {
                this.props.page === "products" ?
                  <div style={{ textAlign: "center" }}>
                    <Link href={Constants.get_new_page("claims", this.props.referrer)}>
                      <Button variant="contained" style={{ margin: '30px 0px 0px 0px', padding: '10px 15px 10px 15px', fontSize: '14px', backgroundColor: '#7cda24', color: 'white', textAlign: "center" }}>Still unsure about claims?</Button>
                    </Link>
                  </div>
                  : ""
              }


            </div>

          </div>
        )
        break;
      case "policy_details_small_accident_income":
        renderable.push(
          <div style={{ textAlign: "center", wordWrap: "break-word" }}>
            <div style={{ margin: '10px 10px 0px 10px', fontSize: '25px', fontWeight: 'bold', lineHeight: '1', color: '#7cda24', transform: "translateX(20px)" }}>  Cyclic: Small Accident Income</div>

            <div style={{ textAlign: "left", fontSize: "15px" }}>
              <div style={{ margin: '20px 0px 0px 0px', fontWeight: "bold" }}>(1) Benefits </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} >  Claim $120 for each day that you are unable to work due to an accident, starting from the 3rd day of your accident MC in Singapore. </div>
              </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} >  No verification of your current income is required. </div>
              </div>



              <div style={{ margin: '40px 0px 0px 0px', fontWeight: "bold" }}>(2) Claims </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} > Accident definition: Violent, Visible, External, Impact.  </div>
              </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} >Covers only new accidents which happen after your cover is approved with us, and the SMC-registered doctor ( practicing in Singapore ) who issued the accident MC, must write clearly on the Receipt / Invoice / MC, the Date and Description of the accident which occurred.  </div>
              </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} > No hospitalization is needed for you to make a claim.</div>
              </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} > Claim up to 6 days for each cycle. </div>
              </div>

              <div style={{ margin: '40px 0px 0px 0px', fontWeight: "bold" }}>(3) Special Notes </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} > Valid for any user who is between age 21 to age 55. </div>
              </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} >User must have a Singapore Bank account to apply for this.</div>
              </div>


              <div style={{ margin: '40px 0px 0px 0px', fontWeight: "bold" }}>(4) Exclusions </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} > Old accidents which happened before your cover is Approved with us.</div>
              </div>

              <div style={{ margin: '40px 0px 0px 0px', fontWeight: "bold" }}>(5) Low Claims definition </div>
              <div style={{ marginTop: "20px" }}>
                <div style={{ float: "left" }}> <Tick /> </div>
                <div style={{ margin: '0px 0px 0px 35px' }} > Users who claim SGD$120 or less per cycle. </div>
              </div>

              <div style={{ margin: '60px 10px 20px 10px', fontSize: '25px', fontWeight: 'bold', lineHeight: '1', color: '#7cda24', textAlign: "center" }}> How are we different </div>
              <div style={{ margin: '10px 0px 0px 0px', fontWeight: "bold", color: Constants.color.darkgreen }}> * We cover you even if your current income happens to be less than $120 a day.</div>
              <div style={{ margin: '10px 0px 0px 0px', fontWeight: "bold", color: Constants.color.darkgreen }}> * We allow a claim even if you were not hospitalized for the accident.</div>
              <div style={{ margin: '10px 0px 0px 0px', fontWeight: "bold", color: Constants.color.darkgreen }}> * We allow a claim right after your 2nd day of accident MC. </div>



              <div style={{ textAlign: "left", marginTop: "50px" }}>
                <div style={{ margin: '10px 10px 0px 10px', fontSize: '25px', fontWeight: 'bold', lineHeight: '1', color: '#7cda24', textAlign: "center" }}> HOW TO CLAIM </div>


                <div style={{ margin: '20px 0px 0px 0px', textAlign: "center" }}> [Cyclic: Small Accident Income] </div>

                <div style={{ margin: '20px 0px 0px 0px' }}>When you need to make a claim, just:</div>
                <div style={{ margin: '20px 0px 0px 0px' }}>1. Tell us:</div>
            <div style={{ margin: '10px 0px 0px 15px' }}> (i) What happened in the incident,</div>
            <div style={{ margin: '0px 0px 0px 15px' }}> (ii) Who was the SMC-registered Doctor you consulted,  </div>
            <div style={{ margin: '0px 0px 0px 15px' }}> (iii) Which clinic / hospital you visited. </div>

            <div style={{ margin: '20px 0px 0px 0px' }}>2. Upload photos of:</div>
            <div style={{ margin: '10px 0px 0px 15px' }}> (i) Paid Receipt, </div>
            <div style={{ margin: '0px 0px 0px 15px' }}> (ii) Itemized Invoice of treatment and medicine,</div>
            <div style={{ margin: '0px 0px 0px 15px' }}> (iii) MC with diagnosis / Letter of diagnosis.</div>


                <div style={{ margin: '40px 0px 0px 0px' }}>* Just make sure the Doctor writes the <u>Date of accident</u> + <u>Description of accident</u>, on any 1 of the documents in the photos which you upload ( either Receipt / Invoice / MC with diagnosis / Letter of diagnosis ).</div>
                <div style={{ margin: '40px 0px 0px 0px' }}>SMC is the Singapore Medical Council ( a statutory board under the Ministry of Health ), governed under Section 5 of the Medical Registration Act (Cap 174). You can check if your doctor is SMC registered, via this link:  <a href={"https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC"} target={"_blank"} style={{ color: "#7cda24", wordWrap: "break-word" }}> <span style={{ wordWrap: "break-word" }}> https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC </span></a>.</div>
                <div style={{ margin: '40px 0px 0px 0px' }}>  * On the Date of any Claim Submission, your policy cover must be active in order for it to be a processable Claim.  </div>
                <div style={{ margin: '10px 0px 0px 0px' }}> ie. If your policy cover is no longer active for any reason, a new Claim Submission will not be entertained. </div>


                {
                  this.props.page === "products" ?
                    <div style={{ textAlign: "center" }}>
                      <Link href={Constants.get_new_page("claims", this.props.referrer)}>
                        <Button variant="contained" style={{ margin: '30px 0px 0px 0px', padding: '10px 15px 10px 15px', fontSize: '14px', backgroundColor: '#7cda24', color: 'white', textAlign: "center" }}>Still unsure about claims?</Button>
                      </Link>
                    </div>
                    : ""
                }


              </div>

            </div>
          </div>
        )
        break;
        case "policy_details_small_pmd_protect":
          renderable.push(
            <div style={{ textAlign: "center", wordWrap: "break-word" }}>
            <div style={{ margin: '10px 10px 0px 10px', fontSize: '25px', fontWeight: 'bold', lineHeight: '1', color: '#7cda24', transform: "translateX(20px)" }}>
              <div>Cyclic: Small PMD Protect</div>
              {/* <div style={{ fontWeight: "normal", marginTop: "20px", display: "inline-block", fontSize: "17px", fontFamily: "tahoma", color: "grey" }}> Because you can't Control accidents </div> */}
            </div>
    
    
            <div style={{ width: "100%", fontSize: "15px" }}>
              <div style={{ padding: "0px 20px 5px 0px", textAlign: "left" }}>
    
                <div style={{ margin: '10px 0px 0px 0px', fontWeight: "bold" }}>(1) Benefits </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} >Reimburses you 80% of the PMD repair bill ( capped at total bill size of $500 per cycle ), when it’s due to Collision damage and Water short circuit damage.</div>
                </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} >Reimburses you up to $40 for a One-way transport fee to send your immobile PMD to the repair centre, when a repair is done for Collision damage and Water short circuit damage.</div>
                </div>
    
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} >Additional medical benefits: Reimburses you up to $500 per accident (even for medical treatment of sprains, cuts or falls) for physical injuries directly due to your PMD.</div>
                </div>
    
    
                <div style={{ margin: '40px 0px 0px 0px', fontWeight: "bold" }}>(2) Claims </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} >Reimbursement of 80% of the PMD repair bill is claimable, once your PMD is immobile ( unable to move on it’s own ) due to Collision damage and Water short circuit damage. </div>
                </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  {/* <div style={{ margin: '0px 0px 0px 35px' }} > Covers only new accidents which happen after your cover is Approved with us, and the Doctor must write clearly on the Receipt / Invoice / MC, the Date and Description of the Accident which occurred.</div> */}
                  <div style={{ margin: '0px 0px 0px 35px' }} >For the reimbursement of a One-way transport fee, we accept the receipt by iPassion Group Pte Ltd / the receipt of a Ride Hailing trip which must display iPassion Group Pte Ltd’s repair centre address as the exact Destination location.</div>
                </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} >All PMD repairs must be done by iPassion Group Pte Ltd <a href="https://www.PassionGadgets.com">https://www.PassionGadgets.com</a> ( 6 Harper Road, Leong Huat Building, #05-08, Singapore 369674, right outside Tai Seng MRT station CC11 ).</div>
                </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} >Additional medical benefits definition: Violent, Visible, External, Impact.</div>
                </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} >Additional medical benefits:  Covers only new accidents which happen after your cover is approved with us, and the SMC-registered / TCMPB-registered doctor ( practicing in Singapore ) must write clearly on the Receipt / Invoice / MC, the Date and Description of the accident which occurred.</div>
                </div>
    
    
    
                <div style={{ margin: '40px 0px 0px 0px', fontWeight: "bold" }}>(3) Special Notes </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} >Valid for any user who is between age 16 to age 55. </div>
                </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} >Additional medical benefits: Treatment for accidents in Singapore only. </div>
                </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} >User must have a Singapore Bank account to apply for this.</div>
                </div>
                {/* <div style={{ marginTop: "20px" }}>
      <div style={{ float: "left" }}> <Tick /> </div>
      <div style={{ margin: '0px 0px 0px 35px' }} >For doctors who sign up as a user with us, if you receive treatment from a TCMPB registered doctor who happens to be your spouse, relative, colleague or your employee, we will still allow you to make claims, up to 2 TCM treatments per Accident. ( claims for any treatment administered by yourself for your own injury, will not be valid. )</div>
    </div> */}
                {/* <div style={{ marginTop: "20px" }}>
      <div style={{ float: "left" }}> <Tick /> </div>
      <div style={{ margin: '0px 0px 0px 35px' }} >This product is not currently not available for SMC-registered / TCMPB-registered doctors to purchase.</div>
    </div> */}
    
                <div style={{ margin: '40px 0px 0px 0px', fontWeight: "bold" }}>(4) Exclusions </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} >Claims within the 1st 30 days of Approved cover for any new PMD registered with us, in case of fraud risk. ( this will be waived, if you provide us with photos of the Receipt showing that you applied for our cover within 48 hours of buying a brand new PMD )</div>
                </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} >Claims related to old accidents which happened before your cover is Approved with us.</div>
                </div>
    
                <div style={{ margin: '40px 0px 0px 0px', fontWeight: "bold" }}>(5) Low Claims definition </div>
                <div style={{ marginTop: "20px" }}>
                  <div style={{ float: "left" }}> <Tick /> </div>
                  <div style={{ margin: '0px 0px 0px 35px' }} >Users who claim SGD$80 or less per cycle. </div>
                </div>
    
                <div style={{ margin: '60px 10px 20px 10px', fontSize: '25px', fontWeight: 'bold', lineHeight: '1', color: '#7cda24', textAlign: "center" }}> How are we different </div>
    
                <div style={{ margin: '10px 0px 0px 0px', fontWeight: "bold", color: Constants.color.darkgreen }}> * We cover your PMD for Collision damage and Water short circuit damage, which a standard warranty does not cover.</div>
                <div style={{ margin: '10px 0px 0px 0px', fontWeight: "bold", color: Constants.color.darkgreen }}> * We cover the cost to transport your PMD to the repair centre when a repair is done for Collision damage and Water short circuit damage.</div>
                <div style={{ margin: '10px 0px 0px 0px', fontWeight: "bold", color: Constants.color.darkgreen }}> * We cover your accident medical bills for injuries directly due to your PMD, even when you are on a PMD for work. </div>
    
              </div>
            </div>
    
    
    
    
            <div style={{ textAlign: "left", marginTop: "50px" }}>
              <div style={{ margin: '10px 10px 0px 10px', fontSize: '25px', fontWeight: 'bold', lineHeight: '1', color: '#7cda24', textAlign: "center" }}> HOW TO CLAIM </div>
    
    
              <div style={{ margin: '20px 0px 0px 0px', textAlign: "center" }}> [Cyclic: Small PMD Protect] </div>
    
              <div style={{ margin: '20px 0px 0px 0px' }}>When you need to make a <b>PMD Repair</b> claim, just:</div>
              <div style={{ margin: '20px 0px 0px 0px' }}>1. Tell us:</div>
              <div style={{ margin: '10px 0px 0px 15px' }}> (i) When you sent it in for repair, </div>
              <div style={{ margin: '0px 0px 0px 15px' }}> (ii) What is the Scooter Service Form number,</div>
              <div style={{ margin: '0px 0px 0px 15px' }}> (iii) What is the Repair Report number.</div>

    
              <div style={{ margin: '20px 0px 0px 0px' }}>2. Upload photos of:</div>
              <div style={{ margin: '10px 0px 0px 15px' }}> (i) Paid repair Receipt, </div>
              <div style={{ margin: '0px 0px 0px 15px' }}> (ii) Scooter Service Form,</div>
              <div style={{ margin: '0px 0px 0px 15px' }}> (iii) Repair Report ( just request it upon payment for the repair ),</div>
              <div style={{ margin: '0px 0px 0px 15px' }}> (iv) Receipt of a One-way Ride Hailing trip which must display iPassion Group Pte Ltd’s repair centre address as the exact Destination location ( if applicable ).</div>

    
              <div style={{ margin: '20px 0px 0px 0px' }}>When you need to make an <b>Additional medical</b> claim, just:</div>
              <div style={{ margin: '20px 0px 0px 0px' }}>1. Tell us:</div>
              <div style={{ margin: '10px 0px 0px 15px' }}> (i) What happened in the incident,</div>
              <div style={{ margin: '0px 0px 0px 15px' }}> (ii) Who was the SMC registered / TCMPB registered Doctor you consulted,</div>
              <div style={{ margin: '0px 0px 0px 15px' }}> (iii) Which clinic / hospital you visited.</div>
        
              <div style={{ margin: '20px 0px 0px 0px' }}>2. Upload photos of:</div>
              <div style={{ margin: '10px 0px 0px 15px' }}> (i) Paid Receipt,</div>
              <div style={{ margin: '0px 0px 0px 15px' }}> (ii) Itemized Invoice of treatment and medicine,</div>
              <div style={{ margin: '0px 0px 0px 15px' }}> (iii) MC with diagnosis / Letter of diagnosis.</div>
    
              <div style={{ margin: '40px 0px 0px 0px' }}>* Just make sure the Doctor writes the <u>Date of accident</u> + <u>Description of accident</u>, on any 1 of the documents in the photos which you upload ( either Receipt / Invoice / MC with diagnosis / Letter of diagnosis ).</div>
    
              <div style={{ margin: '40px 0px 0px 0px' }}>SMC is the Singapore Medical Council ( a statutory board under the Ministry of Health ), governed under Section 5 of the Medical Registration Act (Cap 174). You can check if your doctor is SMC registered, via this link:  <a href={"https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC"} target={"_blank"} style={{ color: "#7cda24", wordWrap: "break-word" }}> <span style={{ wordWrap: "break-word" }}> https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC </span></a>.</div>
              <div style={{ margin: '40px 0px 0px 0px' }}>TCMPB is the Traditional Chinese Medicine Practitioners Board ( a statutory board under the charge of the Ministry of Health ), under the Traditional Chinese Medicine Practitioners Act 2000, governed under the Traditional Chinese Medicine Practitioners Act (Cap. 333A). You can check if your doctor is TCMPB registered, via this link: <a href={"https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=TCM"} target={"_blank"} style={{ color: "#7cda24" }}>https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=TCM</a>.</div>
              <div style={{ margin: '40px 0px 0px 0px' }}> * On the Date of any Claim Submission, your policy cover must be active in order for it to be a processable Claim.</div>
              <div style={{ margin: '10px 0px 0px 0px' }}>ie. If your policy cover is no longer active for any reason, a new Claim Submission will not be entertained.
    
          {
                  this.props.page === "products" ?
                    <div style={{ textAlign: "center" }}>
                      <Link href={Constants.get_new_page("claims", this.props.referrer)}>
                        <Button variant="contained" style={{ margin: '30px 0px 0px 0px', padding: '10px 15px 10px 15px', fontSize: '14px', backgroundColor: '#7cda24', color: 'white', textAlign: "center" }}>Still unsure about claims?</Button>
                      </Link>
                    </div>
                    : ""
                }
    
              </div>
    
            </div>
    
          </div>
        )
        break;
      case "practical":
        renderable.push(
          <div>
            <div style={{ margin: '10px 0px 0px 0px', fontSize: '25px', fontWeight: "bold" }}>
              13 - Can I make a claim from Cyclic insurance, after I make a claim from a traditional Insurer’s policy, for the same accident / incident? </div>
            <div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>Traditional Insurers may not allow you to make a Reimbursement claim from 2 different insurers for the same accident / incident, but we work differently at iFinSG.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>When an unfortunate accident / incident happens, you should be allowed to claim All the relevant benefits which you have already paid for, via the premiums of your policies which you have already been paying to different insurers.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If a company wants to prevent you from making more than 1 Reimbursement claim from the same accident / incident, maybe that company should prevent you from buying a 2nd policy in the first place?</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}><span style={{ fontWeight: 'bold' }}> Cyclic insurance</span> will always allow you to make a claim from us for the same accident / incident, even after you make a similar claim from a traditional insurer.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> Make a 2nd claim with us for the same Accident ( using photos of the Receipt and related documents ), even after you make a Reimbursement claim for your policy with an Insurer ( using the original Receipt and related documents ) !</div>
            </div>
          </div>
        )
        break;
      case "simple":
        renderable.push(
          <div>
            <div style={{ margin: '10px 0px 0px 0px', fontSize: '25px', fontWeight: "bold" }}>
              32 - What are the things not covered under my Cyclic insurance policy? </div>
            <div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>We believe that insurance should be simple, so we work on a WYSIWYG principle. WYSIWYG = What you see is what you get.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Each product's Policy Details on our <a href={Constants.get_new_page("products", this.state.referrer)} style={{ color: "#7cda24" }}>PRODUCTS</a> page, fully explains the policy’s features, with no hidden terms and conditions at all.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Any exclusion which would disqualify a claim for a particular policy, would be stated explicitly and clearly in the Policy Details of each product.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>We do not have a huge chunk of small text exclusions such as “no Claims in case of an act of God”.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>We have some common-sense rules which all our users and policies operate within:</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>(1) You cannot commit a crime intentionally and expect to make a successful claim.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>(2) You cannot commit self-inflicted damage and yet expect to make any claim.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>(3) We don’t have a 3rd rule yet, but if you do think of something applicable and fair, let us know so that we will consider if we should add it in.</div>

            </div>
          </div>
        )
        break;
      case "clear":
        renderable.push(
          <div>
            <div style={{ margin: '10px 0px 0px 0px', fontSize: '25px', fontWeight: "bold" }}>
              2 - Premiums and Admin Fee</div>
            <div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> We collect premiums + an <span style={{ fontWeight: "bold" }}>Admin Fee</span> from each user. </div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> Cyclic insurance only collects premiums as a pool in order to pay out claims, thus we treat collected premiums as if they were still your money, and return all premiums unused for claims, back to users as  <span style={{ fontWeight: "bold" }}>PremiumBack</span> after the end of every cycle.</div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> Every dollar of premiums we collect, is only for 2 purposes: <span style={{ fontWeight: "bold" }}>PremiumBack</span> and Claims. </div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>The only income we earn as a business, is a flat and transparent monthly <span style={{ fontWeight: "bold" }}>Admin Fee</span> per policy per user. </div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>As iFinSG only earns a monthly  <span style={{ fontWeight: "bold" }}>Admin Fee</span> per policy per user, we love all users equally, instead of focusing on wealthier users!</div>


            </div>
          </div>
        )
        break;
      case "easy":
        renderable.push(
          <div >
            <div style={{ margin: '10px 0px 0px 0px', fontSize: '25px', fontWeight: "bold" }}>
              12 - How do I make a Claim?
                </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>We use an online claims system, which is fast and convenient for you.</div>

            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px', textAlign: "center" }}> [Cyclic: Small Accident] </div>

            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>When you need to make a claim, just:</div>
            <div style={{ margin: '20px 0px 0px 0px' }}>1. Tell us:</div>
            <div style={{ margin: '10px 0px 0px 15px' }}> (i) What happened in the incident,</div>
            <div style={{ margin: '0px 0px 0px 15px' }}> (ii) Who was the SMC-registered / TCMPB-registered Doctor you consulted, </div>
            <div style={{ margin: '0px 0px 0px 15px' }}> (iii) Which clinic / hospital you visited.</div>

  
            <div style={{ margin: '20px 0px 0px 0px' }}>2. Upload photos of:</div>
            <div style={{ margin: '10px 0px 0px 15px' }}> (i) Paid Receipt, </div>
            <div style={{ margin: '0px 0px 0px 15px' }}> (ii) Itemized Invoice of treatment and medicine,</div>
            <div style={{ margin: '0px 0px 0px 15px' }}> (iii) MC with diagnosis / Letter of diagnosis.</div>

              
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>( You must make sure the Doctor writes clearly the <u>Date of accident</u> + <u>Description of accident</u>, on the Receipt / Invoice / MC with diagnosis / Letter of diagnosis. )</div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>SMC is the Singapore Medical Council ( a statutory board under the Ministry of Health ), governed under Section 5 of the Medical Registration Act (Cap 174). You can check if your doctor is SMC registered, via this link:  <a href={"https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC"} target={"_blank"} style={{ color: "#7cda24", wordWrap: "break-word" }}> <span style={{ wordWrap: "break-word" }}> https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC </span></a>.</div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>TCMPB is the Traditional Chinese Medicine Practitioners Board ( a statutory board under the charge of the Ministry of Health ), under the Traditional Chinese Medicine Practitioners Act 2000, governed under the Traditional Chinese Medicine Practitioners Act (Cap. 333A). You can check if your doctor is TCMPB registered, via this link: <a href={"https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=TCM"} target={"_blank"} style={{ color: "#7cda24" }}>https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=TCM</a>.</div>

            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>  In the event that an overseas medical service provider is involved, we will contact them to verify the authenticity.  </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> At all times, if there are any extra costs involved in the claims verification process (such as translation costs), we may either bill you and deduct it from your claim payout, or bill you separately if the claim cannot be substantiated.</div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>  * On the Date of any Claim Submission, your policy cover must be active in order for it to be a processable Claim.  </div>
            <div style={{ margin: '10px 0px 0px 0px', fontSize: '20px' }}>  ie. If your policy cover is no longer active for any reason, a new Claim Submission will not be entertained. </div>

          </div>
        )
        break;
      case "considerate":
        renderable.push(
          <div >
            <div style={{ margin: '10px 0px 0px 0px', fontSize: '25px', fontWeight: "bold" }}>
              3 - How is iFinSG different from a traditional insurance company?
                    </div>

            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> iFinSG is an InsurTech firm which runs a P2P insurance system that focuses on the welfare of users, not the profit of shareholders. </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> Traditional insurance companies keep the money they don’t pay out in claims. This means that every additional $1 paid out as Claims, translates to $1 less in underwriting Profit. </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> We are structured to prevent any conflict of interest, by making sure that all premiums unused for claims, are given back to our users at the end of every cycle as <span style={{ fontWeight: "bold" }}>PremiumBack</span>. We gain nothing by delaying or denying claims. </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> <span style={{ fontWeight: "bold" }}>PremiumBack</span> is given to 2 types of users: users who make 0 claims, and users who make low claims. </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> Our proprietary algorithm is structured so that the lower the total claims you make, the higher the <span style={{ fontWeight: "bold" }}>PremiumBack</span> you will qualify for each cycle. If you made 0 claims, you’ll get the highest level of <span style={{ fontWeight: "bold" }}>PremiumBack</span>. </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> As paying claims is our core priority, occasionally when premiums for a policy are fully paid out as claims, users may receive less <span style={{ fontWeight: "bold" }}>PremiumBack</span> than normal, however at all times do be assured that: </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> A user who made 0 claims will always receive a <span style={{ fontWeight: "bold" }}>PremiumBack</span>, <u>guaranteed to be at least 10% of your premiums placed with us.</u> </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> A user who made low claims will always receive a <span style={{ fontWeight: "bold" }}>PremiumBack</span>, <u>guaranteed to be at least 5% of your premiums placed with us.</u> </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> You enjoy amazing protection with us (nobody would try to deny your claim), everything is based on real claims data, and there’s always money coming back to you! </div>

          </div>
          // </div>
        )
        break;
      case "fair":
        renderable.push(
          <div >
            <div style={{ margin: '10px 0px 0px 0px', fontSize: '25px', fontWeight: "bold" }}>
              7 - What PremiumBack amount would I get, if no user makes a claim in a particular cycle?
                </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> If no user makes a claim in a particular cycle, you would get back all your premiums for the cycle as <span style={{ fontWeight: "bold" }}>PremiumBack</span>.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Eg. For the Small Accident cover, your monthly premium would be $6.80, while the monthly Admin Fee is $3. If you participated for a full cycle, the total premium would be $6.80 x 3 months = $20.40.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If no user makes a claim in a particular cycle, you would thus receive a <span style={{ fontWeight: "bold" }}>PremiumBack</span> of $20.40.</div>

          </div>
        )
        break;
      case "friendly":
        renderable.push(
          <div >
            <div style={{ margin: '10px 0px 0px 0px', fontSize: '25px', fontWeight: "bold" }}>
              30 - How do I re-continue my cover if it has Lapsed or Stopped?
            </div>

            <div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>You would be delighted to know that you can Login to your <span style={{ fontWeight: 'bold' }}>Profile</span> page, and use just 1 button to easily re-continue your cover with us. </div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> We’ll bill your credit / debit card after the Reinstatement of your cover is approved, based on the new starting date of your cover. </div>
            </div>

          </div>
        )
        break;
      case "Money Back Guarantee":
        renderable.push(
          <div >
            <img src={'/img/Pic-6-Home.png'} style={{width: 200,margin:'0 auto', display: 'block'}}/>
            <div style={{textAlign: 'center' }}>
                <h2>Money Back Guarantee</h2>
                <div style={{ marginTop: "20px", fontSize: "20px", lineHeight: "1.8" }}> We are confident to give you our Money back Guarantee:</div>
                  <div style={{ fontSize: "20px", lineHeight: "1.8", marginTop: "40px" }}> After you have tried our service for a 1st cycle, should you decide not to use our services again, we'll still give you back a value equivalent to 100% of your 1st cycle Premiums!</div>
                  <div style={{ textAlign: "left", fontSize: "20px", lineHeight: "1.8", marginTop: "40px" }}>
                    <ul>
                      <li>Applicable if you cancel your cover within 14 days after your 1st cycle.</li>
                      <li>Valid if you did not make any claim in your 1st cycle.</li>
                      <li>Inclusive of the <b>PremiumBack</b> given for your 1st cycle.</li>
                      <li>Applicable if you did not take any form of promotional vouchers, discounts or equivalent benefits for your purchase.</li>
                    </ul>
                  </div>
               <div style={{ textAlign: "center", fontSize: "20px", lineHeight: "1.8", marginTop: "40px" }} > Try our service, because we know you’ll love it!</div>
            </div>

          </div>
        )
        break;
      case 6:
        renderable.push(
          <div >

            <div style={{ margin: '10px 0px 0px 0px', fontSize: '25px', fontWeight: "bold" }}>
              6 - Is a PremiumBack guaranteed?
            </div>
            <div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> Yes, it is.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> Do take note however, that <span style={{ fontWeight: "bold" }}>PremiumBack</span> will only be given to 2 types of users: users who make 0 claims, and users who make low claims.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> A user who made 0 claims will always receive a <span style={{ fontWeight: "bold" }}>PremiumBack</span>, <u>guaranteed to be at least 10% of your premiums placed with us.</u></div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> A user who made low claims will always receive a <span style={{ fontWeight: "bold" }}>PremiumBack</span>, <u>guaranteed to be at least 5% of your premiums placed with us.</u></div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> If there is any user who cancels a policy prematurely before the end of a cycle, the user will immediately be fully removed from that policy’s <span style={{ fontWeight: "bold" }}>PremiumBack</span> calculation system, to ensure that <span style={{ fontWeight: "bold" }}>PremiumBack</span> for other users will not be affected.</div>

            </div>
          </div>
        )
      default:
        // Should not happen
        break;
    }

    return renderable

  }

  render() {
    const { classes } = this.props;

    return (
      <div>
{this.props.feature == "price" ? 
(  <Button style={{ width:"100%", fontSize: '10px', height:"31px", padding: "5px 0px 5px 0px", backgroundColor: "rgb(206, 133, 0)", opacity: 0.7, color: 'white', lineHeight: 1.1 }} onClick={this.handleOpen}> Pricing </Button>
) :
(
  <IconButton onClick={this.handleOpen} style={{ margin: "0px 0px 5px 0px", padding: "0px 7px 0px 7px", fontSize: 12, fontWeight: 600, backgroundColor: "grey", color: "white", opacity: 0.7 }}>i</IconButton>
)
}
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>


          {this.renderPopoverContent()}
          </div>
        </Modal>
      </div>
    );
  }
}

SimpleModal.propTypes = {
  classes: PropTypes.object.isRequired,
};

// We need an intermediary variable for handling the recursive nesting.
const SimpleModalWrapped = withStyles(styles)(SimpleModal);

export default SimpleModalWrapped;