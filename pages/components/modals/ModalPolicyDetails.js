import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary } from '@material-ui/core';
import { NoSsr, Button, Typography, Checkbox, Grid, Paper, FormControlLabel, Modal, IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";
import Constants from 'constants/constants';
import Tick from 'components/icons/Tick';
import Router from 'next/router';
import Link from "next/link";
import CyclicSmallCOVID_19Income from 'components/product/CyclicSmallCOVID_19Income'
import CyclicSmallAccident from 'components/product/CyclicSmallAccident'
import CyclicSmallAccidentIncome from 'components/product/CyclicSmallAccidentIncome'
import CyclicSmallInfluenzaGP from 'components/product/CyclicSmallInfluenzaGP'


function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  // const top = 50 + rand();
  // const left = 50 + rand();
  const top = 50
  const left = 50
  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => {
  let width = '85%'

  if (isBrowser) {
    width = '60%'
  }

  return ({
    paper: {
      position: 'absolute',
      width: width,// theme.spacing.unit * 50,
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing.unit * 4,
      outline: 'none',
      overflow: 'scroll',
      maxHeight: '90%',

    },
  })
}

class SimpleModal extends React.Component {
  state = {
    open: this.props.open,
  };


  componentWillReceiveProps = (nextProps) => {
    if (this.props.open === false && nextProps.open === true) {
      this.handleOpen()
    }
  }



  handleBackPressed = () => {
    history.pushState(null, null, null);
    this.handleClose()
  }

  handleOpen = () => {
    this.setState({ open: true });

    // Adding back listener
    history.pushState(null, null, null);
    window.addEventListener("popstate", this.handleBackPressed)
  };

  handleClose = () => {

    this.props.handleClose()
    this.setState({ open: false });

    // Removing back listener
    history.go(-1)
    window.removeEventListener('popstate', this.handleBackPressed)
  };

  renderPolicyDetails = () => {

    let renderable = []

    let image_max_width = 1

    if (isBrowser) {
    }

    const close_button =
      <IconButton onClick={() => { this.handleClose() }} style={{ float: "right", transform: "translate(20px,-20px)" }}><CloseIcon /> </IconButton>


    renderable.push(close_button)


    const small_accident = <CyclicSmallAccident />
    const small_covid19_income = <CyclicSmallCOVID_19Income />
    const small_influenza_gp = <CyclicSmallInfluenzaGP />
    const small_accident_income = <CyclicSmallAccidentIncome />

    // renderable.push(title)

    renderable.push(eval(this.props.policy))    // This replaces switch case below
    // switch (this.props.policy) {
    //   case 'small_critical_checkup':
    //     renderable.push(small_critical_checkup)
    //     break;
    //     ...
    // }

    return (renderable)
  }

  render() {
    const { classes } = this.props;

    return (
      <div>
        {/* <Typography gutterBottom>Click to get the full Modal experience!</Typography>
        <Button onClick={this.handleOpen}>Open Modal</Button> */}



        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>


            {this.renderPolicyDetails()}


            {/* <SimpleModalWrapped /> */}
          </div>
        </Modal>
      </div>
    );
  }
}

SimpleModal.propTypes = {
  classes: PropTypes.object.isRequired,
};

// We need an intermediary variable for handling the recursive nesting.
const SimpleModalWrapped = withStyles(styles)(SimpleModal);

export default SimpleModalWrapped;