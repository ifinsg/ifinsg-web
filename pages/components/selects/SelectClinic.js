import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Select from 'react-select';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import NoSsr from '@material-ui/core/NoSsr';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import MenuItem from '@material-ui/core/MenuItem';
import CancelIcon from '@material-ui/icons/Cancel';
import { emphasize } from '@material-ui/core/styles/colorManipulator';
import { clinic_list_tcm, clinic_list_smc } from 'data/clinicList';

// const disabled_all = true
const disabled_all = false

//     ].map(suggestion => ({
//       value: suggestion.label,
//       label: suggestion.label,
//     }))

//   )
// }




const styles = theme => ({
  root: {
    flexGrow: 1,
    height: '100%',
    marginTop: '15px',
  },
  input: {
    display: 'flex',
    padding: 0,
  },
  valueContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    flex: 1,
    alignItems: 'center',
    overflow: 'hidden',
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
  },
  chipFocused: {
    backgroundColor: emphasize(
      theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
      0.08,
    ),
  },
  noOptionsMessage: {
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
  },
  singleValue: {
    fontSize: 16,
  },
  placeholder: {
    position: 'absolute',
    left: 2,
    fontSize: 16,
  },
  paper: {
    position: 'absolute',
    zIndex: 50,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
  },
  divider: {
    height: theme.spacing.unit * 2,
  },
});

function NoOptionsMessage(props) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.noOptionsMessage}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

function inputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />;
}

function Control(props) {

  // handleFieldChange

  return (
    <TextField
      disabled={disabled_all}
      onChange={(e) => {
        changeSuggestions()
      }}
      fullWidth
      InputProps={{
        inputComponent,
        inputProps: {
          className: props.selectProps.classes.input,
          inputRef: props.innerRef,
          children: props.children,
          ...props.innerProps,
        },
      }}
    // {...props.selectProps.textFieldProps}
    />
  );
}

function Option(props) {
  return (
    <MenuItem
      disabled={disabled_all}

      buttonRef={props.innerRef}
      selected={props.isFocused}
      component="div"
      style={{
        fontWeight: props.isSelected ? 500 : 400,
      }}
      {...props.innerProps}
    >
      {props.children}
    </MenuItem>
  );
}

function Placeholder(props) {
  return (
    <Typography
      disabled={disabled_all}

      color="textSecondary"
      className={props.selectProps.classes.placeholder}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

function SingleValue(props) {
  return (
    <Typography className={props.selectProps.classes.singleValue} {...props.innerProps} disabled={disabled_all}    >
      {props.children}
    </Typography>
  );
}

function ValueContainer(props) {
  return <div className={props.selectProps.classes.valueContainer}>{props.children}</div>;
}

// function MultiValue(props) {
//   return (
//     <Chip
//       tabIndex={-1}
//       label={props.children}
//       className={classNames(props.selectProps.classes.chip, {
//         [props.selectProps.classes.chipFocused]: props.isFocused,
//       })}
//       onDelete={props.removeProps.onClick}
//       deleteIcon={<CancelIcon {...props.removeProps} />}
//     />
//   );
// }

function Menu(props) {
  return (
    <Paper square className={props.selectProps.classes.paper} {...props.innerProps}>
      {props.children}
    </Paper>
  );
}


let changeSuggestions = () => {
}


var IntegrationReactSelect = class IntegrationReactSelect extends React.Component {
  state = {
    single: null,
    multi: null,
    // otherOccupation: "HIHI",

    suggestions: this.props.medical_registrar_smc ? clinic_list_smc : clinic_list_tcm

    // [
    //   { label: `Clinic 1` },
    //   // { label: `Clinic 2` },
    //   // { label: `Clinic 3` },
    //   // { label: `Others: Please specify`},

    // ].map(suggestion => ({
    //   value: suggestion.label,
    //   label: suggestion.label,
    // })),

  };

  // handleFieldChange = name => value => {
  //   this.setState({
  //     [name]: value,
  //   });
  // };


  control = (props) => {
    return (
      <TextField
        disabled={disabled_all}
        fullWidth
        InputProps={{
          inputComponent,
          inputProps: {
            className: props.selectProps.classes.input,
            inputRef: props.innerRef,
            children: props.children,
            ...props.innerProps,
          },
        }}
      // {...props.selectProps.textFieldProps}
      />
    )
  }


  handleChange = name => value => {
    this.props.handleSelectFieldChange("select_clinic", value)
  };


  render() {
    const { classes, theme } = this.props;

    const selectStyles = {
      input: base => ({
        ...base,
        color: theme.palette.text.primary,
        '& input': {
          font: 'inherit',
        },
      }),
      indicatorsContainer: (provided, state) => ({
        display: "none"
      }),
    };

    return (
      <div className={classes.root}>
        <NoSsr>

          <Select
            classes={classes}
            styles={selectStyles}
            // options={this.state.suggestions}
            options={this.props.medical_registrar_smc ? clinic_list_smc : clinic_list_tcm}

            disabled={disabled_all}

            // components={components}
            components={
              {
                Control: this.control,     // This handles the change when an text is typed into TextField. Will update this component's state.suggestions
                Menu,
                // MultiValue,
                NoOptionsMessage,
                Option,
                Placeholder,
                SingleValue,
                ValueContainer,
              }
            }

            value={this.props.select_clinic}
            onChange={this.handleChange()}      // This handles the change when an option is selected. Will update parent component

            // onChange={this.handleChange('single')}

            placeholder={this.props.placeholder ? this.props.placeholder : "Clinic / Hospital visited"}
            // placeholder = {this.props.placeholder}

            isClearable
          />
        </NoSsr>
      </div>
    );
  }
}

IntegrationReactSelect.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(IntegrationReactSelect);