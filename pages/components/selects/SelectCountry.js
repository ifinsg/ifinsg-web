import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Select from 'react-select';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import NoSsr from '@material-ui/core/NoSsr';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import MenuItem from '@material-ui/core/MenuItem';
import CancelIcon from '@material-ui/icons/Cancel';
import { emphasize } from '@material-ui/core/styles/colorManipulator';



const styles = theme => ({
  root: {
    flexGrow: 1,
    height: '100%',
    marginTop: '0px',
  },
  input: {
    display: 'flex',
    padding: "6px 0 7px",
  },
  valueContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    flex: 1,
    alignItems: 'center',
    overflow: 'hidden',
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
  },
  chipFocused: {
    backgroundColor: emphasize(
      theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
      0.08,
    ),
  },
  noOptionsMessage: {
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
  },
  singleValue: {
    fontSize: 16,
  },
  placeholder: {
    position: 'absolute',
    left: 2,
    fontSize: 16,
  },
  paper: {
    position: 'absolute',
    zIndex: 50,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
  },
  divider: {
    height: theme.spacing.unit * 2,
  },
});

function NoOptionsMessage(props) {
  return (

    <Typography
      color="textSecondary"
      className={props.selectProps.classes.noOptionsMessage}
      {...props.innerProps}
    >
      {props.children}

    </Typography>
  );
}

function inputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />;
}

function Control(props) {

  // handleFieldChange

  return (
    <TextField
      // label= {this.props.placeholder}
      onChange={(e) => {
        changeSuggestions()
      }}
      fullWidth
      InputProps={{
        inputComponent,
        inputProps: {
          className: props.selectProps.classes.input,
          inputRef: props.innerRef,
          children: props.children,
          ...props.innerProps,
        },
      }}
    // {...props.selectProps.textFieldProps}
    />
  );
}

function Option(props) {
  return (
    <MenuItem
      buttonRef={props.innerRef}
      selected={props.isFocused}
      component="div"
      style={{
        fontWeight: props.isSelected ? 500 : 400,
      }}
      {...props.innerProps}
    >
      {props.children}
    </MenuItem>
  );
}

function Placeholder(props) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.placeholder}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

function SingleValue(props) {
  return (
    <Typography className={props.selectProps.classes.singleValue} {...props.innerProps}>
      {props.children}
    </Typography>
  );
}

function ValueContainer(props) {
  return <div className={props.selectProps.classes.valueContainer}>{props.children}</div>;
}

// function MultiValue(props) {
//   return (
//     <Chip
//       tabIndex={-1}
//       label={props.children}
//       className={classNames(props.selectProps.classes.chip, {
//         [props.selectProps.classes.chipFocused]: props.isFocused,
//       })}
//       onDelete={props.removeProps.onClick}
//       deleteIcon={<CancelIcon {...props.removeProps} />}
//     />
//   );
// }

function Menu(props) {
  return (
    <Paper square className={props.selectProps.classes.paper} {...props.innerProps}>
      {props.children}
    </Paper>
  );
}


let changeSuggestions = () => {
  console.log('changeSuggestions')
}


var IntegrationReactSelect = class IntegrationReactSelect extends React.Component {

  // label = (value) => {
  //   const tick_green =
  //     <svg style={{ height: "24px", width: "24px", fill: '#7cda24', stroke: '#7cda24', strokeWidth: 2.5, opacity: 0.6, float: "right" }}>
  //       <path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z"></path>
  //     </svg>

  //   return (
  //     <div style={{ width: "100%" }}>
  //       <span> {value} </span>
  //       {tick_green}
  //     </div>
  //   )
  // }

  state = {
    single: null,
    multi: null,
    // otherOccupation: "HIHI",

    suggestions: [
      // { label: this.label(`Abattoir worker / Butcher`) },
      { label: `Singapore` },
      { label: `Afghanistan` },
      { label: `Albania` },
      { label: `Algeria` },
      { label: `Andorra` },
      { label: `Angola` },
      { label: `Antigua and Barbuda` },
      { label: `Argentina` },
      { label: `Armenia` },
      { label: `Australia` },
      { label: `Austria` },
      { label: `Azerbaijan` },
      { label: `The Bahamas` },
      { label: `Bahrain` },
      { label: `Bangladesh` },
      { label: `Barbados` },
      { label: `Belarus` },
      { label: `Belgium` },
      { label: `Belize` },
      { label: `Benin` },
      { label: `Bhutan` },
      { label: `Bolivia` },
      { label: `Bosnia and Herzegovina` },
      { label: `Botswana` },
      { label: `Brazil` },
      { label: `Brunei` },
      { label: `Bulgaria` },
      { label: `Burkina Faso` },
      { label: `Burundi` },
      { label: `Cabo Verde` },
      { label: `Cambodia` },
      { label: `Cameroon` },
      { label: `Canada` },
      { label: `Central African Republic` },
      { label: `Chad` },
      { label: `Chile` },
      { label: `China` },
      { label: `Colombia` },
      { label: `Comoros` },
      { label: `Congo, Democratic Republic of the` },
      { label: `Congo, Republic of the` },
      { label: `Costa Rica` },
      { label: `Côte d’Ivoire` },
      { label: `Croatia` },
      { label: `Cuba` },
      { label: `Cyprus` },
      { label: `Czech Republic` },
      { label: `Denmark` },
      { label: `Djibouti` },
      { label: `Dominica` },
      { label: `Dominican Republic` },
      { label: `East Timor (Timor-Leste)` },
      { label: `Ecuador` },
      { label: `Egypt` },
      { label: `El Salvador` },
      { label: `Equatorial Guinea` },
      { label: `Eritrea` },
      { label: `Estonia` },
      { label: `Eswatini` },
      { label: `Ethiopia` },
      { label: `Fiji` },
      { label: `Finland` },
      { label: `France` },
      { label: `Gabon` },
      { label: `The Gambia` },
      { label: `Georgia` },
      { label: `Germany` },
      { label: `Ghana` },
      { label: `Greece` },
      { label: `Grenada` },
      { label: `Guatemala` },
      { label: `Guinea` },
      { label: `Guinea-Bissau` },
      { label: `Guyana` },
      { label: `Haiti` },
      { label: `Honduras` },
      { label: `Hungary` },
      { label: `Iceland` },
      { label: `India` },
      { label: `Indonesia` },
      { label: `Iran` },
      { label: `Iraq` },
      { label: `Ireland` },
      { label: `Israel` },
      { label: `Italy` },
      { label: `Jamaica` },
      { label: `Japan` },
      { label: `Jordan` },
      { label: `Kazakhstan` },
      { label: `Kenya` },
      { label: `Kiribati` },
      { label: `Korea, North` },
      { label: `Korea, South` },
      { label: `Kosovo` },
      { label: `Kuwait` },
      { label: `Kyrgyzstan` },
      { label: `Laos` },
      { label: `Latvia` },
      { label: `Lebanon` },
      { label: `Lesotho` },
      { label: `Liberia` },
      { label: `Libya` },
      { label: `Liechtenstein` },
      { label: `Lithuania` },
      { label: `Luxembourg` },
      { label: `Madagascar` },
      { label: `Malawi` },
      { label: `Malaysia` },
      { label: `Maldives` },
      { label: `Mali` },
      { label: `Malta` },
      { label: `Marshall Islands` },
      { label: `Mauritania` },
      { label: `Mauritius` },
      { label: `Mexico` },
      { label: `Micronesia, Federated States of` },
      { label: `Moldova` },
      { label: `Monaco` },
      { label: `Mongolia` },
      { label: `Montenegro` },
      { label: `Morocco` },
      { label: `Mozambique` },
      { label: `Myanmar (Burma)` },
      { label: `Namibia` },
      { label: `Nauru` },
      { label: `Nepal` },
      { label: `Netherlands` },
      { label: `New Zealand` },
      { label: `Nicaragua` },
      { label: `Niger` },
      { label: `Nigeria` },
      { label: `North Macedonia` },
      { label: `Norway` },
      { label: `Oman` },
      { label: `Pakistan` },
      { label: `Palau` },
      { label: `Panama` },
      { label: `Papua New Guinea` },
      { label: `Paraguay` },
      { label: `Peru` },
      { label: `Philippines` },
      { label: `Poland` },
      { label: `Portugal` },
      { label: `Qatar` },
      { label: `Romania` },
      { label: `Russia` },
      { label: `Rwanda` },
      { label: `Saint Kitts and Nevis` },
      { label: `Saint Lucia` },
      { label: `Saint Vincent and the Grenadines` },
      { label: `Samoa` },
      { label: `San Marino` },
      { label: `Sao Tome and Principe` },
      { label: `Saudi Arabia` },
      { label: `Senegal` },
      { label: `Serbia` },
      { label: `Seychelles` },
      { label: `Sierra Leone` },
      { label: `Slovakia` },
      { label: `Slovenia` },
      { label: `Solomon Islands` },
      { label: `Somalia` },
      { label: `South Africa` },
      { label: `Spain` },
      { label: `Sri Lanka` },
      { label: `Sudan` },
      { label: `Sudan, South` },
      { label: `Suriname` },
      { label: `Sweden` },
      { label: `Switzerland` },
      { label: `Syria` },
      { label: `Taiwan` },
      { label: `Tajikistan` },
      { label: `Tanzania` },
      { label: `Thailand` },
      { label: `Togo` },
      { label: `Tonga` },
      { label: `Trinidad and Tobago` },
      { label: `Tunisia` },
      { label: `Turkey` },
      { label: `Turkmenistan` },
      { label: `Tuvalu` },
      { label: `Uganda` },
      { label: `Ukraine` },
      { label: `United Arab Emirates` },
      { label: `United Kingdom` },
      { label: `United States` },
      { label: `Uruguay` },
      { label: `Uzbekistan` },
      { label: `Vanuatu` },
      { label: `Vatican City` },
      { label: `Venezuela` },
      { label: `Vietnam` },
      { label: `Yemen` },
      { label: `Zambia` },
      { label: `Zimbabwe` }
    ].map(suggestion => ({
      value: suggestion.label,
      label: suggestion.label,
    })),

  };

  // handleFieldChange = name => value => {
  //   this.setState({
  //     [name]: value,
  //   });
  // };


  control = (props) => {
    return (
      <TextField
        label={this.props.placeholder}
        value={this.props.country}
        // label= {this.props.placeholder}
        onChange={(e) => {
        }}
        fullWidth
        InputProps={{
          inputComponent,
          inputProps: {
            className: props.selectProps.classes.input,
            inputRef: props.innerRef,
            children: props.children,
            ...props.innerProps,
          },
        }}
      // {...props.selectProps.textFieldProps}
      />
    )
  }


  handleChange = name => value => {
    this.props.handleCountryFieldChange(value, this.props.type, this.props.i)
  };


  render() {
    const { classes, theme } = this.props;

    const selectStyles = {
      input: base => ({
        ...base,
        color: theme.palette.text.primary,
        '& input': {
          font: 'inherit',
        },
      }),
      indicatorsContainer: (provided, state) => ({
        display: "none"
      }),
    };

    return (
      <div className={classes.root}>
        <NoSsr>

          <Select
            classes={classes}
            styles={selectStyles}
            options={this.state.suggestions}
            // components={components}
            components={
              {
                Control: this.control,     // This handles the change when an text is typed into TextField. Will update this component's state.suggestions
                Menu,
                // MultiValue,
                NoOptionsMessage,
                Option,
                Placeholder,
                SingleValue,
                ValueContainer,
              }
            }


            value={this.props.country}
            onChange={this.handleChange()}      // This handles the change when an option is selected. Will update parent component

            // onChange={this.handleChange('single')}
            label={this.props.placeholder ? this.props.placeholder : "Select Country"}
            placeholder="" //{this.props.placeholder ? this.props.placeholder : "Select Country"}
            isClearable
          />
        </NoSsr>
      </div>
    );
  }
}

IntegrationReactSelect.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(IntegrationReactSelect);