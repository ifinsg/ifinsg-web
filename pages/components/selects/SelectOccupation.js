import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Select from 'react-select';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import NoSsr from '@material-ui/core/NoSsr';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import MenuItem from '@material-ui/core/MenuItem';
import CancelIcon from '@material-ui/icons/Cancel';
import { emphasize } from '@material-ui/core/styles/colorManipulator';
import GlobalHelpers from '../../../global/helpers';

let disable_onblur = false
let selected_option = false

const styles = theme => ({
  root: {
    flexGrow: 1,
    height: '100%',
    marginTop: '0px',
  },
  input: {
    display: 'flex',
    padding: "6px 0 7px",
  },
  valueContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    flex: 1,
    alignItems: 'center',
    overflow: 'hidden',
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
  },
  chipFocused: {
    backgroundColor: emphasize(
      theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
      0.08,
    ),
  },
  noOptionsMessage: {
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
  },
  singleValue: {
    fontSize: 16,
  },
  placeholder: {
    position: 'absolute',
    left: 2,
    fontSize: 16,
  },
  paper: {
    position: 'absolute',
    zIndex: 50,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
  },
  divider: {
    height: theme.spacing.unit * 2,
  },
});

function NoOptionsMessage(props) {
  return (

    <Typography
      color="textSecondary"
      className={props.selectProps.classes.noOptionsMessage}
      {...props.innerProps}
    >
      {props.children}

    </Typography>
  );
}

function inputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />;
}


function Option(props) {
  return (
    <MenuItem
      buttonRef={props.innerRef}
      selected={props.isFocused}
      component="div"
      style={{
        fontWeight: props.isSelected ? 500 : 400,
      }}
      {...props.innerProps}
    >
      {props.children}
    </MenuItem>
  );
}

function Placeholder(props) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.placeholder}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

function SingleValue(props) {
  return (
    <Typography className={props.selectProps.classes.singleValue} {...props.innerProps}>
      {props.children}
    </Typography>
  );
}

function ValueContainer(props) {
  return <div className={props.selectProps.classes.valueContainer}>{props.children}</div>;
}

// function MultiValue(props) {
//   return (
//     <Chip
//       tabIndex={-1}
//       label={props.children}
//       className={classNames(props.selectProps.classes.chip, {
//         [props.selectProps.classes.chipFocused]: props.isFocused,
//       })}
//       onDelete={props.removeProps.onClick}
//       deleteIcon={<CancelIcon {...props.removeProps} />}
//     />
//   );
// }

function Menu(props) {
  return (
    <Paper square className={props.selectProps.classes.paper} {...props.innerProps}>
      {props.children}
    </Paper>
  );
}


let changeSuggestions = () => {
}


var IntegrationReactSelect = class IntegrationReactSelect extends React.Component {

  // label = (value) => {
  //   const tick_green =
  //     <svg style={{ height: "24px", width: "24px", fill: '#7cda24', stroke: '#7cda24', strokeWidth: 2.5, opacity: 0.6, float: "right" }}>
  //       <path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z"></path>
  //     </svg>

  //   return (
  //     <div style={{ width: "100%" }}>
  //       <span> {value} </span>
  //       {tick_green}
  //     </div>
  //   )
  // }

  state = {
    single: null,
    multi: null,
    // otherOccupation: "HIHI",

    suggestions: [
      // { label: `Abattoir worker / Butcher` },
      { label: `Accountant` },
      { label: `Accounts clerk` },
      { label: `Actor` },
      { label: `Actress` },
      { label: `Actuary` },
      { label: `Acupuncturist` },
      { label: `Admin clerk` },
      { label: `Admin officer` },
      { label: `Advertising specialist` },
      { label: `Aeronautical engineer` },
      { label: `Aged care worker` },
      { label: `Agricultural consultant` },
      { label: `Agricultural engineer` },
      { label: `Agricultural scientist` },
      { label: `Air force (admin)` },
      { label: `Air force (combat officer)` },
      { label: `Air force (mechanic)` },
      { label: `Air force (NS)` },
      { label: `Air force (officer)` },
      { label: `Air force (pilot airplane)` },
      { label: `Air force (pilot helicopter)` },
      { label: `Air force (soldier)` },
      { label: `Air force (technician)` },
      { label: `Air traffic controller` },
      { label: `Aircraft baggage handler and airline ground crew` },
      { label: `Aircraft maintenance engineer (avionics)` },
      { label: `Aircraft maintenance engineer (mechanical)` },
      { label: `Aircraft maintenance engineer (structural)` },
      { label: `Alarm, security or surveillance officer` },
      { label: `Ambulance officer` },
      { label: `Anaesthetist` },
      { label: `Animal trainer` },
      { label: `Aquaculture farmer` },
      { label: `Arborist` },
      { label: `Archaeologist` },
      { label: `Architect` },
      { label: `Architectural draftsperson` },
      { label: `Archivist` },
      { label: `Army (admin)` },
      { label: `Army (driver)` },
      { label: `Army (mechanic)` },
      { label: `Army (NS)` },
      { label: `Army (officer)` },
      { label: `Army (soldier)` },
      { label: `Army (technician)` },
      { label: `Art director (film, television, stage)` },
      { label: `Arts performer` },
      { label: `Art teacher` },
      { label: `Athlete` },
      { label: `Auctioneer` },
      { label: `Audiologist` },
      { label: `Audiovisual technician` },
      { label: `Auditor (internal)` },
      { label: `Auditor (external)` },
      { label: `Author` },
      { label: `Automotive electrician` },
      { label: `Baby` },
      { label: `Baker` },
      { label: `Bank admin` },
      { label: `Bank manager` },
      { label: `Bank private banker` },
      { label: `Bank sales` },
      { label: `Bank teller` },
      { label: `Bar attendant` },
      { label: `Barista` },
      { label: `Barrister` },
      { label: `Beauty therapist` },
      { label: `Beautician` },
      { label: `Beef cattle farmer` },
      { label: `Binder and finisher` },
      { label: `Biomedical engineer` },
      { label: `Biosecurity officer` },
      { label: `Biotechnologist` },
      { label: `Blogger` },
      { label: `Boat builder and repairer` },
      { label: `Body artist or tattooist` },
      { label: `Boiler or engine operator` },
      { label: `Book or script editor` },
      { label: `Bookkeeper` },
      { label: `Bookmaker` },
      { label: `Botanist` },
      { label: `Brewer` },
      { label: `Bricklayer` },
      { label: `Broadcast transmitter operator` },
      { label: `Builder's Labourer` },
      { label: `Building insulation installer` },
      { label: `Building surveyor` },
      { label: `Cabinetmaker` },
      { label: `Cafe or restaurant manager` },
      { label: `Call or contact centre operator` },
      { label: `Call or contact centre team leader` },
      { label: `Camera operator (film, television or video)` },
      { label: `Canvas goods fabricator` },
      { label: `Car park attendant` },
      { label: `Cardiologist` },
      { label: `Cardiothoracic surgeon` },
      { label: `Carpenter` },
      { label: `Cartographer` },
      { label: `Cashier` },
      { label: `Chef` },
      { label: `Chemical engineer` },
      { label: `Chemist` },
      { label: `Chief financial officer CFO` },
      { label: `Chief executive officer CEO` },
      { label: `Child care centre manager` },
      { label: `Child protection worker` },
      { label: `Chiropractor` },
      { label: `Cinema or theatre manager` },
      { label: `Civil celebrant` },
      { label: `Civil engineer` },
      { label: `Civil engineering draftsperson` },
      { label: `Civil engineering technician` },
      { label: `Clay, concrete, glass or stone machine operator` },
      { label: `Cleaner` },
      { label: `Clerk` },
      { label: `Clinic Assistant` },
      { label: `Clinical haematologist` },
      { label: `Clinical psychologist` },
      { label: `Clothing production worker` },
      { label: `Coastal engineer` },
      { label: `Commercial cleaner` },
      { label: `Commercial housekeeper` },
      { label: `Commodities trader` },
      { label: `Communications operator` },
      { label: `Community corrections officer` },
      { label: `Community worker` },
      { label: `Computer engineer` },
      { label: `Computer-aided design (CAD) technician` },
      { label: `Concierge` },
      { label: `Concreter` },
      { label: `Confectionery maker` },
      { label: `Conference and event organiser` },
      { label: `Conservation Officer` },
      { label: `Conservator` },
      { label: `Construction estimator` },
      { label: `Construction project manager` },
      { label: `Construction rigger` },
      { label: `Contract Administrator` },
      { label: `Contractor (home services)` },
      { label: `Contractor (renovation & interior design)` },
      { label: `Cook` },
      { label: `Copywriter` },
      { label: `Corporate general manager` },
      { label: `Counsellor (career)` },
      { label: `Counsellor (drug and alcohol)` },
      { label: `Counsellor (youths)` },
      { label: `Coxswain` },
      { label: `Crane chaser` },
      { label: `Crane, hoist or lift operator` },
      { label: `Customer service manager` },
      { label: `Customs officer` },
      { label: `Data scientist` },
      { label: `Dairy cattle farmer` },
      { label: `Dancer or choreographer` },
      { label: `Debt collector` },
      { label: `Deck hand` },
      { label: `Deejay (disco)` },
      { label: `Deejay (radio)` },
      { label: `Dental assistant` },
      { label: `Dental hygienist` },
      { label: `Dental specialist` },
      { label: `Dental technician` },
      { label: `Dental therapist` },
      { label: `Dentist` },
      { label: `Dermatologist` },
      { label: `Developer programmer` },
      { label: `Dietician` },
      { label: `Digital Marketer (search)` },
      { label: `Digital Marketer (social media)` },
      { label: `Director (film, television, radio or stage)` },
      { label: `Disability services officer` },
      { label: `Disability worker` },
      { label: `Diver (commercial)` },
      { label: `Diversional therapist` },
      { label: `Doctor (GP)` },
      { label: `Doctor (psychiatrist)` },
      { label: `Doctor (specialist)` },
      { label: `Doctor (surgeon)` },
      { label: `Doctor (TCM)` },
      { label: `Dog handler or trainer` },
      { label: `Domestic cleaner / helper` },
      { label: `Doorperson or luggage porter` },
      { label: `Driller` },
      { label: `Drilling engineer` },
      { label: `Driver (ambulance)` },
      { label: `Driver (bus)` },
      { label: `Driver (chauffeur)` },
      { label: `Driver (forklift)` },
      { label: `Driver (goods and delivery)` },
      { label: `Driver (race car)` },
      { label: `Driver (ride hailing)` },
      { label: `Driver (taxi)` },
      { label: `Driver (train)` },
      { label: `Driver (truck)` },
      { label: `Drycleaner` },
      { label: `Early childhood educator` },
      { label: `Earth science technician` },
      { label: `Earthmoving plant operator` },
      { label: `Economist` },
      { label: `Editor` },
      { label: `Electorate officer` },
      { label: `Electrical engineer` },
      { label: `Electrical engineering draftsperson` },
      { label: `Electrical engineering technician` },
      { label: `Electrical linesworker` },
      { label: `Electrician (general)` },
      { label: `Electronic engineering technician` },
      { label: `Electronic equipment trades worker` },
      { label: `Electronic instrument trades worker` },
      { label: `Electronics engineer` },
      { label: `Electroplater` },
      { label: `Embalmer` },
      { label: `Emergency medicine specialist` },
      { label: `Endocrinologist` },
      { label: `Engineering patternmaker` },
      { label: `Engraver` },
      { label: `Entertainer or variety artist` },
      { label: `Entrepreneur` },
      { label: `Environmental consultant` },
      { label: `Environmental engineer` },
      { label: `Environmental health officer` },
      { label: `Environmental manager` },
      { label: `Environmental research scientist` },
      { label: `Exercise scientist` },
      { label: `Facilities manager` },
      { label: `Farm manager` },
      { label: `Fashion designer` },
      { label: `Film and video producer` },
      { label: `Finance broker` },
      { label: `Financial consultant / planner` },
      { label: `Financial institution branch manager` },
      { label: `Financial investment adviser` },
      { label: `Financial investment manager` },
      { label: `Fire fighter` },
      { label: `Fisheries officer` },
      { label: `Fishing hand` },
      { label: `Fleet manager` },
      { label: `Flight attendant (air steward stewardess)` },
      { label: `Floor finisher` },
      { label: `Florist` },
      { label: `Food delivery professional` },
      { label: `Food processing worker` },
      { label: `Food technologist` },
      { label: `Forensic scientist` },
      { label: `Forester` },
      { label: `Fruit and vegetable picker` },
      { label: `Funeral director` },
      { label: `Funeral assistant` },
      { label: `Funeral related service adviser` },
      { label: `Furniture finisher` },
      { label: `Furniture mover` },
      { label: `Gallery or museum curator` },
      { label: `Gallery or museum guide` },
      { label: `Gallery or museum technician` },
      { label: `Game developer` },
      { label: `Gaming worker` },
      { label: `Gardener` },
      { label: `Gasfitter` },
      { label: `Gastroenterologist` },
      { label: `General clerk` },
      { label: `General Manager` },
      { label: `Geologist` },
      { label: `Geophysicist` },
      { label: `Geotechnical engineer` },
      { label: `Glazier` },
      { label: `Goldsmith` },
      { label: `Grain oilseed or pasture grower` },
      { label: `Grape grower` },
      { label: `Graphic designer` },
      { label: `Graphic pre-press trades worker` },
      { label: `Greenkeeper` },
      { label: `Gunsmith` },
      { label: `Gynaecologist` },
      { label: `Hairdresser` },
      { label: `Handyman` },
      { label: `Hawker` },
      { label: `Hawker assistant` },
      { label: `Health information manager` },
      { label: `Health promotion officer` },
      { label: `Historian` },
      { label: `Homemaker` },
      { label: `Horse trainer` },
      { label: `Hostess` },
      { label: `Hotel manager` },
      { label: `Human resource adviser` },
      { label: `Human resource manager` },
      { label: `Hydrologist` },
      { label: `ICT business analyst` },
      { label: `ICT project manager` },
      { label: `ICT security specialist` },
      { label: `ICT support technician` },
      { label: `ICT systems test engineer` },
      { label: `Immigration officer` },
      { label: `Industrial designer` },
      { label: `Industrial engineer` },
      { label: `Influencer` },
      { label: `Instructor (dance)` },
      { label: `Instructor (driving)` },
      { label: `Instructor (flying)` },
      { label: `Instructor (gym or fitness)` },
      { label: `Instructor (martial arts or mma)` },
      { label: `Instructor (outdoor adventure)` },
      { label: `Instructor (skating or rollerblading)` },
      { label: `Instructor (sports)` },
      { label: `Instructor (swimming)` },
      { label: `Instructor (yoga and pilates)` },
      { label: `Insurance agent` },
      { label: `Insurance broker` },
      { label: `Intelligence officer` },
      { label: `Intensive care specialist` },
      { label: `Interior designer` },
      { label: `Interpreter` },
      { label: `IT administrator` },
      { label: `IT coder` },
      { label: `IT programmer` },
      { label: `IT project manager` },
      { label: `IT specialist` },
      { label: `Jewellery designer` },
      { label: `Jockey (horse)` },
      { label: `Joiner` },
      { label: `Journalist` },
      { label: `Judge` },
      { label: `Laboratory manager` },
      { label: `Laboratory technician` },
      { label: `Landscape architect` },
      { label: `Landscape gardener` },
      { label: `Laundry worker` },
      { label: `Law clerk` },
      { label: `Lecturer` },
      { label: `Legal secretary` },
      { label: `Librarian` },
      { label: `Library assistant` },
      { label: `Library technician` },
      { label: `Life coach` },
      { label: `Lifeguard` },
      { label: `Light technician` },
      { label: `Locksmith` },
      { label: `Logistics clerk` },
      { label: `Machine shorthand reporter` },
      { label: `Maintenance planner` },
      { label: `Make up artist` },
      { label: `Manager` },
      { label: `Managing director` },
      { label: `Marine biologist` },
      { label: `Marine fabricator` },
      { label: `Marine surveyor` },
      { label: `Market research analyst` },
      { label: `Marketing specialist` },
      { label: `Massage therapist` },
      { label: `Master fisher` },
      { label: `Materials engineer` },
      { label: `Materials recycler` },
      { label: `Materials technician` },
      { label: `Mathematician` },
      { label: `Meat inspector` },
      { label: `Mechanic (air conditioning)` },
      { label: `Mechanic (automotive or car)` },
      { label: `Mechanic (bicycle)` },
      { label: `Mechanic (business machine)` },
      { label: `Mechanic (diesel motor)` },
      { label: `Mechanic (motor)` },
      { label: `Mechanic (motorcycle)` },
      { label: `Mechanic (optical)` },
      { label: `Mechanic (plant)` },
      { label: `Mechanic (refrigeration)` },
      { label: `Mechanic (textile, clothing and footwear)` },
      { label: `Mechanical engineer` },
      { label: `Mechanical engineering draftsperson` },
      { label: `Mechanical engineering technician` },
      { label: `Mechanical fitter` },
      { label: `Mechatronic engineer` },
      { label: `Medical administrator` },
      { label: `Medical diagnostic radiographer` },
      { label: `Medical laboratory scientist` },
      { label: `Medical oncologist` },
      { label: `Medical radiation therapist` },
      { label: `Member of parliament` },
      { label: `Mental health worker` },
      { label: `Metal engineering process worker` },
      { label: `Metal fabricator` },
      { label: `Metal machinist` },
      { label: `Metallurgist` },
      { label: `Meteorologist` },
      { label: `Meter reader` },
      { label: `Microbiologist` },
      { label: `Midwife` },
      { label: `Migration agent` },
      { label: `Milliner` },
      { label: `Miner` },
      { label: `Mining engineer` },
      { label: `Mining production manager` },
      { label: `Mining support worker` },
      { label: `Mixed crop and livestock farm worker` },
      { label: `Model` },
      { label: `Mortgage broker` },
      { label: `Motion picture projectionist` },
      { label: `Multimedia specialist` },
      { label: `Musician` },
      { label: `Nanny` },
      { label: `Naturopath` },
      { label: `Naval architect` },
      { label: `Navy (admin)` },
      { label: `Navy (diver)` },
      { label: `Navy (mechanic)` },
      { label: `Navy (NS)` },
      { label: `Navy (officer)` },
      { label: `Navy (soldier or sailor)` },
      { label: `Navy (technician)` },
      { label: `Network administrator` },
      { label: `Network engineer` },
      { label: `Neurologist` },
      { label: `Neurosurgeon` },
      { label: `Nuclear medicine technologist` },
      { label: `Nurse educator` },
      { label: `Nurse manager` },
      { label: `Nurse` },
      { label: `Nurseryperson` },
      { label: `Nursing clinical director` },
      { label: `Nursing support worker` },
      { label: `Nutritionist` },
      { label: `Obstetrician` },
      { label: `Occupational health and safety adviser` },
      { label: `Occupational therapist` },
      { label: `Office manager` },
      { label: `Ophthalmologist` },
      { label: `Optometrist` },
      { label: `Orthopaedic surgeon` },
      { label: `Orthoptist` },
      { label: `Orthotist or prosthetist` },
      { label: `Osteopath` },
      { label: `Otorhinolaryngologist` },
      { label: `Paediatric surgeon` },
      { label: `Paediatrician` },
      { label: `Painter` },
      { label: `Painting trades worker` },
      { label: `Panelbeater` },
      { label: `Park ranger` },
      { label: `Parking inspector` },
      { label: `Pastor` },
      { label: `Pathologist` },
      { label: `Patient care assistant` },
      { label: `Payroll clerk` },
      { label: `Personal assistant` },
      { label: `Pest controller` },
      { label: `Pet groomer` },
      { label: `Petroleum engineer` },
      { label: `Petrophysicist` },
      { label: `Pharmacist (hospital)` },
      { label: `Pharmacist (industrial)` },
      { label: `Pharmacist (retail)` },
      { label: `Pharmacologist` },
      { label: `Pharmacy technician` },
      { label: `Photographer` },
      { label: `Photographer's assistant` },
      { label: `Physical education (PE) teacher` },
      { label: `Physicist` },
      { label: `Physiotherapist` },
      { label: `Picture framer` },
      { label: `Pilot (commercial airplane)` },
      { label: `Pilot (commercial helicopter)` },
      { label: `Plastic and reconstructive surgeon` },
      { label: `Plastics technician` },
      { label: `Plumber` },
      { label: `Podiatrist` },
      { label: `Police force (admin)` },
      { label: `Police force (driver)` },
      { label: `Police force (mechanic)` },
      { label: `Police force (NS)` },
      { label: `Police force (technician)` },
      { label: `Police officer` },
      { label: `Policy analyst` },
      { label: `Political scientist` },
      { label: `Polymer factory worker` },
      { label: `Postal delivery officer` },
      { label: `Power generation plant operator` },
      { label: `Precision instrument maker and repairer` },
      { label: `Priest` },
      { label: `Printing machinist` },
      { label: `Prison officer` },
      { label: `Private investigator` },
      { label: `Process engineer` },
      { label: `Process plant operator` },
      { label: `Product examiner` },
      { label: `Production manager` },
      { label: `Production or plant engineer` },
      { label: `Project builder` },
      { label: `Project or program administrator` },
      { label: `Prop and scenery maker` },
      { label: `Property Agent` },
      { label: `Property developer` },
      { label: `Property manager` },
      { label: `Psychologist` },
      { label: `Public relations officer` },
      { label: `Public relations entertainer` },
      { label: `Publisher` },
      { label: `Quantity Assessor` },
      { label: `Quantity Surveyor` },
      { label: `Radiation oncologist` },
      { label: `Radio presenter` },
      { label: `Radio producer` },
      { label: `Radiologist` },
      { label: `Railway track worker` },
      { label: `Real estate representative` },
      { label: `Receptionist` },
      { label: `Records manager` },
      { label: `Recreation officer` },
      { label: `Recruitment consultant` },
      { label: `Recycling or rubbish collector` },
      { label: `Renal medicine specialist` },
      { label: `Researcher` },
      { label: `Resident medical officer` },
      { label: `Retail buyer` },
      { label: `Retail manager` },
      { label: `Retirement village manager` },
      { label: `Rheumatologist` },
      { label: `Road worker` },
      { label: `Roof plumber` },
      { label: `Roof tiler` },
      { label: `Safety inspector` },
      { label: `Sales and marketing manager` },
      { label: `Sales assistant` },
      { label: `Sales representative` },
      { label: `Saw maker and repairer` },
      { label: `Sawmill or timber yard worker` },
      { label: `Scaffolder` },
      { label: `SCDF (admin)` },
      { label: `SCDF (driver)` },
      { label: `SCDF (mechanic)` },
      { label: `SCDF (NS)` },
      { label: `SCDF (officer)` },
      { label: `SCDF (technician)` },
      { label: `School principal` },
      { label: `Screen printer` },
      { label: `Seafood process worker` },
      { label: `Security officer` },
      { label: `Security systems installer` },
      { label: `Set designer` },
      { label: `Settlement agent` },
      { label: `Shearer` },
      { label: `Sheep farmer` },
      { label: `Sheetmetal trades worker` },
      { label: `Shelf filler` },
      { label: `Ship's engineer` },
      { label: `Ship's master` },
      { label: `Ship's officer` },
      { label: `Shoe repairer` },
      { label: `Shotfirer` },
      { label: `Signwriter` },
      { label: `Silversmith` },
      { label: `Singer` },
      { label: `Small business owner` },
      { label: `Social worker` },
      { label: `Software engineer` },
      { label: `Soil scientist` },
      { label: `Sole proprietor` },
      { label: `Solicitor` },
      { label: `Solid plasterer` },
      { label: `Sonographer` },
      { label: `Sound technician` },
      { label: `Special needs teacher` },
      { label: `Speech pathologist` },
      { label: `Sports administrator` },
      { label: `Sports coach` },
      { label: `Sports development officer` },
      { label: `Sportsperson` },
      { label: `Startup founder / co founder` },
      { label: `Statistician` },
      { label: `Steel fixer` },
      { label: `Stockbroking dealer` },
      { label: `Stonemason` },
      { label: `Storeperson` },
      { label: `Streetsweeper operator` },
      { label: `Structural engineer` },
      { label: `Student (preschool)` },
      { label: `Student (primary school)` },
      { label: `Student (secondary school)` },
      { label: `Student (tertiary education)` },
      { label: `Student (post graduate)` },
      { label: `Stunt performer` },
      { label: `Supply and distribution manager` },
      { label: `Surgeon (general)` },
      { label: `Surveyor` },
      { label: `Surveyor's assistant` },
      { label: `Systems administrator` },
      { label: `Systems analyst` },
      { label: `Talent agent` },
      { label: `Teacher (childcare)` },
      { label: `Teacher (nursery)` },
      { label: `Teacher (pre school)` },
      { label: `Teacher (primary school)` },
      { label: `Teacher (secondary school)` },
      { label: `Teacher (tertiary education)` },
      { label: `Teacher of the hearing impaired` },
      { label: `Teacher of the sight impaired` },
      { label: `Teachers' aide` },
      { label: `Teaching assistant` },
      { label: `Technical cable jointer` },
      { label: `Telecommunications engineer` },
      { label: `Textile production worker` },
      { label: `Thoracic medicine specialist` },
      { label: `Ticket collector or usher` },
      { label: `Tool pusher` },
      { label: `Tour guide` },
      { label: `Tourist information officer` },
      { label: `Trader` },
      { label: `Trade union official` },
      { label: `Train controller` },
      { label: `Training and development professional` },
      { label: `Transit officer` },
      { label: `Translator` },
      { label: `Transport company manager` },
      { label: `Transport engineer` },
      { label: `Travel consultant` },
      { label: `Tree faller` },
      { label: `Turf grower` },
      { label: `Tuition teacher (primary school)` },
      { label: `Tuition teacher (secondary school)` },
      { label: `Tuition teacher (tertiary education)` },
      { label: `Tyre fitter` },
      { label: `Upholsterer` },
      { label: `Urban and regional planner` },
      { label: `Urologist` },
      { label: `Valuer` },
      { label: `Vascular surgeon` },
      { label: `Vet` },
      { label: `Veterinary nurse` },
      { label: `Video editor` },
      { label: `Vineyard worker` },
      { label: `Visual arts and crafts professional` },
      { label: `Visual merchandiser` },
      { label: `Vlogger` },
      { label: `Waiter` },
      { label: `Waitress` },
      { label: `Wall and ceiling fixer` },
      { label: `Wall and floor tiler` },
      { label: `Warehouse administrator` },
      { label: `Waste water or water plant operator` },
      { label: `Watch and clock maker and repairer` },
      { label: `Water inspector` },
      { label: `Waterside worker` },
      { label: `Website designer` },
      { label: `Weight loss consultant` },
      { label: `Welder` },
      { label: `Welfare centre manager` },
      { label: `Welfare worker` },
      { label: `Wine maker` },
      { label: `Wood machinist` },
      { label: `Wood turner` },
      { label: `Wool classer` },
      { label: `​​​Zoologist` },
      { label: `Others: Please specify` },
    ].map(suggestion => ({
      value: suggestion.label,
      label: suggestion.label,
    })),

  };

  // handleFieldChange = name => value => {
  //   this.setState({
  //     [name]: value,
  //   });
  // };

  updateSuggestions = (value) => {
    return new Promise((resolve, reject) => {
      let suggestions = GlobalHelpers.clone_deep(this.state.suggestions)
      let new_value = "Others: " + value
      if (new_value == "Others: ") {
        new_value = "";
      }
      suggestions[suggestions.length - 1].value = new_value
      suggestions[suggestions.length - 1].label = new_value
      this.setState({
        suggestions
      }, () => { resolve() })
    })
  }

  control = (props) => {
    return (
      <TextField
        onFocus={() => {
          disable_onblur = false
        }}
        onBlur={() => {
          if (!disable_onblur && !selected_option) {
            let save_this_entry = this.state.suggestions[this.state.suggestions.length - 1]
            if (save_this_entry && save_this_entry.label === "Others: Please specify") {
              save_this_entry = null
            }
            this.props.handleOccupationFieldChange(save_this_entry, this.props.type, this.props.i)

          } else {
          }

        }}
        value={this.props.occupation}
        label={this.props.placeholder}
        onChange={(e) => {
          disable_onblur = false
          selected_option = false
          this.updateSuggestions(e.target.value)
        }}
        fullWidth
        InputProps={{
          inputComponent,
          inputProps: {
            className: props.selectProps.classes.input,
            inputRef: props.innerRef,
            children: props.children,
            ...props.innerProps,
          },
        }}
      // {...props.selectProps.textFieldProps}
      />
    )
  }


  handleChange = name => value => {
    // Selection is tapped / clicked
    disable_onblur = true
    selected_option = true
    console.log("DISABLING ONBLURRRRR")


    if (value && value.label === "Others: Please specify") {
      value = null
    }

    this.props.handleOccupationFieldChange(value, this.props.type, this.props.i)
  };


  render() {
    const { classes, theme } = this.props;

    const selectStyles = {
      input: base => ({
        ...base,
        color: theme.palette.text.primary,
        '& input': {
          font: 'inherit',
        },
      }),
      indicatorsContainer: (provided, state) => ({
        display: "none"
      }),
    };

    return (
      <div className={classes.root}>
        <NoSsr>

          <Select
            classes={classes}
            styles={selectStyles}
            options={this.state.suggestions}
            // components={components}
            components={
              {
                Control: this.control,     // This handles the change when an text is typed into TextField. Will update this component's state.suggestions
                Menu,
                // MultiValue,
                NoOptionsMessage,
                Option,
                Placeholder,
                SingleValue,
                ValueContainer,
              }
            }


            value={this.props.occupation}
            onChange={this.handleChange()}      // This handles the change when an option is selected. Will update parent component

            // onChange={this.handleChange('single')}
            label={this.props.placeholder ? this.props.placeholder : "Select Occupation"}
            // label={"Select Occupation"}

            placeholder="" //{this.props.placeholder ? this.props.placeholder : "Select Occupation"}
            isClearable
          />
        </NoSsr>
      </div>
    );
  }
}

IntegrationReactSelect.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(IntegrationReactSelect);
