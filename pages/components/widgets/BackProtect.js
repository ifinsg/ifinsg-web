import React, { Component } from 'react';

let backProtect = false


class BackProtect extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentWillReceiveProps = (nextProps) => {
        if (this.props.back_protect_active && nextProps.back_protect_active === false) {
            window.removeEventListener('popstate', this.backProtect)
        } else if (this.props.unload_protect_active && nextProps.unload_protect_active === false) {
            window.removeEventListener("beforeunload", this.beforeunloadProtect);
        }
    }

    componentWillUnmount = () => {
        window.removeEventListener('popstate', this.backProtect)
        window.removeEventListener("beforeunload", this.beforeunloadProtect);
    }


    backProtect = (e) => {
        if (backProtect) {
            var stayOnPage = confirm("Leave this page? Changes might not be saved.");
            if (!stayOnPage) {      // Cancel
                history.pushState(null, null, null);
                backProtect = true
            } else {                // OK or Leave
                backProtect = false
                history.back()
            }
        }
    }


    beforeunloadProtect = (e) => {
        e.preventDefault();
        // Chrome requires returnValue to be set
        e.returnValue = '';
    }



    componentDidMount = () => {
        history.pushState(null, null, null);
        backProtect = true
        window.addEventListener("popstate", this.backProtect)
        window.addEventListener("beforeunload", this.beforeunloadProtect)

    }

    render() {
        return (
            <div>  </div>
        );
    }
}



export default BackProtect;