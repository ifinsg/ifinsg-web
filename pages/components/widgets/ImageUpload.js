import React, { Component } from 'react';
import DropzoneComponent from 'react-dropzone-component';   // Source: https://github.com/felixrieseberg/React-Dropzone-Component
import { NoSsr, IconButton } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import GlobalConstants from '../../../global/constants';

// import '../css/filepicker.css'
// import '../css/dropzone.min.css'

class DefaultUpload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // Dev Note: state needs to be updated manually in componentWillUpdate. Using props alone will not work somehow :/ ???
      djsConfig: {
        addRemoveLinks: true,
        acceptedFiles: "image/*,.pdf",
        maxFiles: this.props.maxFiles ? this.props.maxFiles : 3,
        params: {
          image_type: this.props.image_type,
          username: this.props.username //!== "" ? this.props.username : "UNKNOWN"
        }
      }
    }


    this.componentConfig = {
      // iconFiletypes: ['.jpg', '.png', '.gif'],
      iconFiletypes: ['.999'],

      showFiletypeIcon: false,
      postUrl:  '/api/image/upload'
    };

    // If you want to attach multiple callbacks, simply
    // create an array filled with all your callbacks.
    this.callbackArray = [() => console.log('Hi!'), () => console.log('Ho!')];

    // Simple callbacks work too, of course
    this.callback = (file) => {

    }

    this.success = (file, uploaded_file_name) => {


      if (uploaded_file_name) {
        file.uploaded_file_name = uploaded_file_name

        if (this.props.type === "insured") {
          const insuredIndex = this.props.insuredIndex
          this.props.fileUploadedInsured(insuredIndex, uploaded_file_name)
        } else if (this.props.type === "claim") {
          const i = this.props.i
          this.props.fileUploadedClaim(i, uploaded_file_name)
        } else {
          this.props.fileUploadedPersonal(uploaded_file_name)
        }

      } else {
      }
    }

    this.progress = file => {
    }

    this.removedfile = file => {

      if (file.uploaded_file_name) {

        const uploaded_file_name = file.uploaded_file_name

        if (this.props.type === "insured") {
          const insuredIndex = this.props.insuredIndex
          this.props.fileRemovedInsured(insuredIndex, uploaded_file_name)
        } else if (this.props.type === "claim") {
          const i = this.props.i
          this.props.fileRemovedClaim(i, uploaded_file_name)
        } else {
          this.props.fileRemovedPersonal(uploaded_file_name)
        }

        // DEVELOPER TO-DO
        // Tell server to remove this file with name uploaded_file_name from the object storage
        // axios file.uploaded_file_name

      }
      // this.props.fileRemoved()
    }

    this.dropzone = null;
  }


  componentWillUpdate = (nextProps) => {
    if (this.state.djsConfig.params.username !== nextProps.username || this.state.djsConfig.params.image_type !== nextProps.image_type ){
      let djsConfig = JSON.parse(JSON.stringify(this.state.djsConfig))
      djsConfig.params.username = nextProps.username
      djsConfig.params.image_type = nextProps.image_type
      this.setState({
        djsConfig
      })
    }
    return true
  }

  render() {
    const config = this.componentConfig;
    const djsConfig = this.state.djsConfig;

    // For a list of all possible events (there are many), see README.md!
    const eventHandlers = {
      init: dz => this.dropzone = dz,
      drop: this.onDrop,// this.callbackArray,
      addedfile: this.callback,
      success: this.success,
      removedfile: this.removedfile,
      uploadprogress: this.progress,
      maxfilesexceeded: (file) => { this.dropzone.removeFile(file); alert("Maximum number of file uploads reached!") },
      error: (file) => { this.dropzone.removeFile(file); alert("File upload failed") },
    }


    if (this.props.disabled) {
      return (
        <div style={{ textAlign: "center", padding: "5px", backgroundColor: "#E1E1E1", borderRadius: "5px", minHeight: "60px", border: "2px dashed #C7C7C7", height: "90px" }}>
          <div style={{ marginTop: "25px" }}>
            <span style={{ color: "grey" }}> Drop files here to upload </span>
          </div>
        </div>
      )
    }

    return (
      <div>

        {/* <NoSsr> */}
        <DropzoneComponent config={config} eventHandlers={eventHandlers} djsConfig={djsConfig} >

          <div style={{ marginTop: "20px", pointerEvents: "none" }}> <IconButton>  <AddIcon />  </IconButton> </div>
          {/* <div> HELLOOOOOOO </div>
        <div> HELLOOOOOOO </div>
        <div> HELLOOOOOOO </div>
        <div> HELLOOOOOOO </div> */}

        </DropzoneComponent>
        {/* </NoSsr> */}
      </div>
    );



  }
}


export default DefaultUpload;