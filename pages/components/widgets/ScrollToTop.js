import React, { Component } from 'react';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import * as Scroll from 'react-scroll';
import { Link, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'
import Constants from 'constants/constants';
import { NoSsr, Button, Typography, Checkbox, Grid, Paper, FormControlLabel, Modal, IconButton, TextField } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";
// import { NoSsr, Button, Typography, AppBar, Tabs, Tab, TextField, FormControl, InputLabel, MenuItem, Select, Card, FormControlLabel, Checkbox, Fab, Divider, IconButton, Grid, FormLabel, RadioGroup, Radio, Chip } from '@material-ui/core';
import axios from 'axios'
import CloseIcon from '@material-ui/icons/Close';
import styled from 'styled-components'

var validator = require("email-validator")   //Source: https://www.npmjs.com/package/email-validator


function getModalStyle() {
    // const top = 50 + rand();
    // const left = 50 + rand();
    const top = 50
    const left = 50
    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}

const styles = theme => {
    let width = '85%'

    if (isBrowser) {
        width = '60%'
    }

    return ({
        paper: {
            position: 'absolute',
            width: width,// theme.spacing.unit * 50,
            backgroundColor: theme.palette.background.paper,
            boxShadow: theme.shadows[5],
            padding: "20px",
            outline: 'none',
            overflow: 'scroll',
            maxHeight: '90%',
            maxWidth: "480px"

        },
    })
}

class ScrollToTop extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show_scroll_to_top_button: "hidden",
            open: false,
            name: "",
            email: "",
            message: "",
            suggestions: "",
            keep_me_informed: true,
        };
    }


    componentDidMount = () => {
        // window.scrollTo(0, 0)
        scroll.scrollToTop();
        window.addEventListener('scroll', this.handleScroll);
        try {
            this.setState({
                window_height: window.innerHeight
            })
        } catch (err) {
        }
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);

    }

    handleScroll = (e) => {
        const lastScrollY = window.scrollY
        if (lastScrollY > 500 && this.state.show_scroll_to_top_button === "hidden") {
            this.setState({
                show_scroll_to_top_button: "visible"
            })
        }

        if (lastScrollY < 400 && this.state.show_scroll_to_top_button === "visible") {
            this.setState({
                show_scroll_to_top_button: "hidden"
            })
        }
    }


    handleFieldChange = (prop) => e => {
        this.setState({
            [prop]: e.target.value
        })
    }



    renderHelp = () => {
        return (
            <div style={{ padding: "20px 10px 20px 10px" }}>
                <div style={{ fontWeight: "bold", textAlign: "center", fontSize: "30px" }}>

                    <Typography style={{ fontWeight: "bold", fontSize: "25px", color: "grey" }}> WE'RE HERE TO HELP </Typography>
                </div>
                <div style={{ marginTop: "10px" }}>
                    <TextField
                        label="Name"
                        autoComplete="mame"
                        value={this.state.name}
                        onChange={this.handleFieldChange('name')}
                        fullWidth
                    />
                </div>

                <div style={{ marginTop: "10px" }}>
                    <TextField
                        label="Email address"
                        autoComplete="email"
                        value={this.state.email}
                        onChange={this.handleFieldChange('email')}
                        fullWidth
                    />
                </div>

                <div style={{ marginTop: "10px" }}>
                    <TextField
                        label="Have a Question for us?"
                        value={this.state.message}
                        onChange={this.handleFieldChange('message')}
                        fullWidth
                        multiline
                    />
                </div>

                <div style={{ marginTop: "10px" }}>
                    <TextField
                        // label={<span>Any Cover/Product suggestions?</span>}
                        label={<span>Any other Product you'd want?</span>}

                        // label={<span>What other Cover/Product would you want?</span>}

                        value={this.state.suggestions}
                        onChange={this.handleFieldChange('suggestions')}
                        fullWidth
                        multiline
                    // InputLabelProps={{
                    //     style: {
                    //         textOverflow: 'ellipsis',
                    //         whiteSpace: 'nowrap',
                    //         overflow: 'hidden',
                    //         width: '100%',
                    //         color: 'green'
                    //     }
                    // }}
                    />
                </div>

                <div>

                    <FormControlLabel
                        style={{ margin: "10px 0px 0px 0px" }}
                        control={
                            <Checkbox
                                checked={this.state.keep_me_informed}
                                onChange={() => {
                                    this.setState({ keep_me_informed: !this.state.keep_me_informed })
                                }}
                                value='ack_50_cents_fee'
                                color='primary'
                            />
                        }
                        // label={<span style={{ color: "rgba(0, 0, 0, 0.54)", fontSize: "13px", lineHeight: 0 }}>I acknowledge that a fee of $0.50 will be imposed on all Premiumback & Claims payouts. FREE for payouts to DBS/POSB accounts</span>}
                        label={
                            <span
                                style={{
                                    color: "rgba(0, 0, 0, 0.54)",
                                    fontSize: "13px",
                                    lineHeight: 0
                                }}
                            >
                                Keep me informed of Promotions and Deals from iFinSG!
                        </span>
                        }
                    />
                </div>


                <div >
                    {this.state.status ? this.state.status === "submitting" ?
                        <div style={{ marginTop: "20px", textAlign: "center", color: "grey" }}>
                            Submitting
                    </div>
                        :
                        <div style={{ marginTop: "20px", textAlign: "center", color: Constants.color.green }}>
                            Successfully submitted.<br />Thank you!
                </div>
                        : ""}
                </div>

                <div style={{ marginTop: "20px", textAlign: "center" }}>
                    <Button style={{ backgroundColor: "lightgrey", width: "100%" }}
                        onClick={() => {

                            if (!this.state.name && !this.state.email && !this.state.message && !this.state.suggestions) {
                                alert("Form is empty")
                                return false
                            }
                            if (!validator.validate(this.state.email)) {
                                alert("Error - Invalid email address")
                                return
                            }


                            this.setState({ status: "submitting" })
                            axios.post("/api/feedback", {
                                name: this.state.name,
                                email: this.state.email,
                                message: this.state.message,
                                suggestions: this.state.suggestions,
                                keep_me_informed: this.state.keep_me_informed
                            }).then(() => {
                                this.setState({
                                    status: "submitted",
                                    name: "",
                                    email: "",
                                    message: "",
                                    suggestions: "",
                                    keep_me_informed: true
                                })
                            }).catch(err => {
                                alert("Failed to send :(")
                                this.setState({ open: false })

                            });
                        }}
                    > Submit</Button>
                </div>


            </div>
        )
    }

    render() {
        const { classes } = this.props;
        if (this.props.showScrollToTopButton === false) {
            // Do nothing - taking it as show unless stated otherwise
        } else {
            return (
                <Wrapper>
                    <NoSsr>
                        <div className="fixed" style={{}}>
                            <div style={{ visibility: this.state.show_scroll_to_top_button }}>
                                <IconButton style={{ pointerEvents: "auto", backgroundColor: "grey", opacity: 0.2, marginRight: "5px" }}
                                    onClick={() => {
                                        // window.scrollTo(0, 0) 
                                        scroll.scrollToTop({ duration: 500 });
                                    }}>
                                    <ExpandLessIcon style={{ color: "white" }} />
                                </IconButton>
                            </div>
                            <div>
                                <IconButton style={{ pointerEvents: "auto", backgroundColor: Constants.color.green, opacity: 0.7, marginRight: "5px", height: "47px", width: "47px" }}
                                    onClick={() => {
                                        this.setState({
                                            open: true,
                                            status: false
                                        })
                                    }}>
                                    <span style={{ color: "white", transform: "translateY(-5px)" }} >  ? </span>
                                </IconButton>
                            </div>

                            <Modal
                                aria-labelledby="simple-modal-title"
                                aria-describedby="simple-modal-description"
                                open={this.state.open}
                                onClose={() => (this.setState({ open: false }))}
                            >
                                <div style={getModalStyle()} className={classes.paper}>

                                    <IconButton style={{ padding: '4px', float: "right" }} onClick={() => {
                                        this.setState({ open: false })
                                    }}> <CloseIcon style={{}} /> </IconButton>

                                    {this.renderHelp()}
                                </div>
                            </Modal>

                        </div>
                    </NoSsr>

                </Wrapper>
            );
        }

    }
}


export default withStyles(styles)(ScrollToTop);


const Wrapper = styled.div`

    .fixed {
        position: fixed; 
        right: 0; 
        bottom: 240px
    }

    @media (max-width:600px) {
        .fixed {
            position: fixed; 
            right: 0; 
            bottom: 40px
        }

    }


`