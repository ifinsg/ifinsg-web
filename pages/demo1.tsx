import * as React from 'react';
import axios from 'axios'
export interface IDemo1Props {
}

const Demo1: React.FC<IDemo1Props> = (props: IDemo1Props) => {

    function getData() {

        const params = {
            name: 'wang',
            age: 32
        }

        axios.get("/api/sample_api", { params }).then((res) => {

            console.log(res)
        })
    }

    return (
        <div>
            <button onClick={() => {
                getData();
            }}>点击</button>
            Demo1
            </div>
    );
}


export default Demo1