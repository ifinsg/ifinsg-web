import React, { Component } from 'react';
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import { NoSsr } from '@material-ui/core';

import TopNavBar from './components/TopNavBar'
import BotNavBar from './components/BotNavBar'

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Constants from 'constants/constants';
import EmailJoanna from './components/constant_components/EmailJoanna';
import Head from 'next/head';


const styles = {
  page_width: Constants.style.page.width,
  page_text_width: Constants.style.page.text_width,

  page_padding: Constants.style.page.padding,

  align_center: Constants.style.align.center,
  align_left: Constants.style.align.left,

  bold: Constants.style.text.bold,
  break_word: Constants.style.text.break_word,

  title1: Constants.style.text.title1,
  body1: Constants.style.text.body1,

  margin_top_xl: Constants.style.margin_top.xl,
  margin_top_lg: Constants.style.margin_top.lg,
  margin_top_md: Constants.style.margin_top.md,
  margin_top_sm: Constants.style.margin_top.sm,
  margin_top_xs: Constants.style.margin_top.xs,
  margin_top_xxs: Constants.style.margin_top.xxs,
}

class DisclaimerOfLiability extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }


  componentDidMount = () => {
    Constants.store_referrer(this)
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <TopNavBar referrer={this.state.referrer} />

        <NextSeo config={Constants.next_seo_config("disclaimer-of-liability")} />

        <NoSsr>


          <div className={classNames(classes.align_center)}>
            <div className={classNames(classes.page_width, classes.page_padding, classes.break_word, classes.align_left)}>


              <div style={{ padding: '50px 0px 0px 0px', textAlign: "center" }}>
                <img style={{ maxWidth: '200px', margin: '40px 0px 0px 0px', padding: '0px 0px 0px 0px' }} src={('/img/iFin-SG-Logo.png')} />
                <div style={{ fontSize: '50px', lineHeight: '1', margin: '30px 0px 0px 0px', color: "#4cae4f", fontWeight: "bold" }}>DISCLAIMER OF LIABILITY</div>
              </div>



              <div style={{ padding: '0px 0px 50px 0px' }}>


                <div style={{ margin: '50px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Legal Disclaimers </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>The Company does not provide any insurance business or act as an insurance intermediary under the Insurance Act (Cap. 142) and the Insurance Intermediaries Act (Cap. 142A).</li>
                    <li>The Company is not required to be licensed and/or regulated by the Monetary Authority of Singapore (“MAS”).</li>
                    <li>Our products  and  services  do  not  fall  within  the coverage of and users of this Website will not be offered protection under the  Deposit Insurance and Policy Owners’ Protection Schemes Act (Cap. 77B) and will not have access to the dispute resolution scheme managed by the Financial Industry Disputes Resolution Centre.</li>
                    <li>We do not issue any policies of insurance nor do we assume any risk or undertake any liabilities under any policies of insurance. We merely provide an online platform whereby users of this Website provide insurance or assume or undertake risks on a peer to peer basis for each other amongst themselves.</li>
                  </ol>
                </div>


                <div style={{ margin: '50px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Self-Diligence Measures </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>Every user of this Website is advised to keep their Member Login PIN confidential and not to post any personal information or information of such a nature that may be misused or used to cause harm to you or others.</li>
                    <li>You are solely responsible for your interaction with the other users of this Website, whether online or offline. ICH iFinancial Singapore Private Limited, UEN: 201631146G is not responsible or liable for the conduct of any other user or Service Provider, adverse consequences or damages suffered by your failure to comply with the above user advisory. ICH iFinancial Singapore Private Limited, UEN: 201631146G reserves the right, but has no obligation, to monitor or become involved in ANY disputes between you and other users.</li>
                  </ol>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> No Endorsement </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>The Company is not responsible for, and does not endorse any content posted by another user or Service Provider. We cannot and do not endorse, and cannot be responsible for the messages, views, opinions and recommendations of any user, or Service Provider or advocate patronage of any particular Service Provider, course of transactions or dealings in any commercial context, investments, nor does the inclusion on the Website of a link to other Website(s) imply any endorsement by us.</li>
                    <li>We do not have any obligation to actively pre-screen, monitor, edit or remove any content, even if these are made by the Registered Service Providers or users of the Website. While the information is obtained from reliable sources, none of the data, information, recommendations or opinions are customized or tailored to fit any specific person. Users are cautioned to use the Website at their own risk.</li>
                  </ol>
                </div>


                <div style={{ margin: '50px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Approval of Claims </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>Approval of claims is strictly within the absolute discretion of the Company. Amongst others, the Company is entitled not to approve a claim in the event that there is any incorrect statement, inaccurate declaration, or withholding of material information at the time of signing up for P2P insurance or anytime during the filing of a claim, including but not limited to misrepresentations, failure to disclose any material facts or any fraud involved, or where the incident giving rise to a claim is due to causes not covered by the policy including but not limited to a pre-existing medical condition which was known or reasonably ought to have been known, deliberate or intentional acts of self-inflicted injury, or as a result of participation in a criminal act such as a riot or a fight. </li>
                  </ol>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Third Party Websites </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>We don’t have control over Websites that the Company may link to. This Website may contain links to third party Websites that are not owned, operated, or controlled by the Company. Therefore, we cannot and do not assume responsibility for the content, privacy policies, or practices of such Websites or the entities that own them. Additionally, we cannot and will not censor or edit the content of any third party site. By using this Website you expressly relieve us from any and all liability arising from your use of any third party Website.</li>
                  </ol>
                </div>


                <div style={{ margin: '50px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Directory Section </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>The Directory section of the Website serves only as a directory to link up Service Providers and End-Users. The Service Provider specified as the provider of the relevant Service that an End-User has contracted for (or may contract for) is (i). solely responsible for performing the relevant Service (ii). fully responsible for all Services it provides to End-Users, and (iii). liable for all damages or losses arising out of the goods or services provided. </li>
                    <li>The Company is not responsible for (i). any price adjustments made by a Service Provider related to a Service or appointment time (although these must be agreed between the relevant Service Provider and End-User through this Website), or (ii). any claims for injuries, illnesses, damages, liabilities and costs (“Liabilities”) that any Service Provider or End-User may suffer, directly or indirectly, in full or in part, whether related to the performance of any of the Services (or non-performance of any of any Services which have been agreed to have been performed) or otherwise. </li>
                    <li>The Company does not guarantee accuracy of information provided through all means including but not limited to forums, webinar, web pages, social network, verbal or telephone communication on or through the Website. By using the Website, you are agreeing to verify such data on your own prior to the use of any information provided to you  by or through this Website.  </li>
                    <li>This material on the Website is for informative purposes only.    </li>
                    <li>The information presented on the Website or by Registered Service Providers should only be relied on by people who have verified such data on their own through other sources and are aware of the risk inherent in carrying on activities associated with the services provided by our Registered Service Providers.  </li>
                    <li> Past performance by any Service Provider is no guarantee of future return or future profit or performance. The Company is not an investment advisor. The Company does not make any recommendation or provide any endorsement for buying or selling any product or service offered by Registered Service Providers. The Company will have no liability for any investment or business decision made by users of the Website.</li>
                    <li>TO THE FULLEST EXTENT PERMITTED BY LAW, YOU AGREE TO AND HEREBY WAIVE AND RELEASE THE COMPANY AND ITS HOLDING COMPANY(IES), SUBSIDIARIES, ASSOCIATED COMPANIES OR ORGANISATIONS (WHETHER INCORPORATED OR NOT), AFFILIATES PARTNERS, OFFICERS, DIRECTORS, STAFF MEMBERS, SHAREHOLDERS AND AGENTS FROM ANY LIABILITIES ARISING FROM OR RELATED TO (I) ANY ACT OR OMISSION BY SERVICE PROVIDER IN THE PROVISION OF ANY SERVICES, INCLUDING A SERVICE PROVIDER’S FAILURE TO COMPLY WITH APPLICABLE LAW AND/OR FAILURE TO ABIDE BY THE AGREED TERMS OF ANY SERVICE, AND/OR (II) ANY ACTION OR INACTION BY A SERVICE PROVIDER AND/OR (III) ANY INDIRECT, SPECIAL, PUNITIVE, CONSEQUENTIAL, (INCLUDING, LOST PROFITS OR LOST DATA COLLECTED THROUGH THIS WEBSITE OR BY A SERVICE PROVIDER), OR INCIDENTAL DAMAGES, WHETHER BASED ON A CLAIM OR ACTION OF CONTRACT, WARRANTY, NEGLIGENCE, STRICT LIABILITY, OR OTHER TORT, BREACH OF ANY STATUTORY DUTY, INDEMNITY OR CONTRIBUTION, OR OTHERWISE, EVEN IF THE COMPANY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. IN NO EVENT WILL THE COMPANY’S LIABILITY ARISING OUT OF OR RELATED TO THESE <span style={{ fontWeight: "bold" }}>TERMS OF USE</span> , OR IN RESPECT OF THE PROVISION (OR NON-PROVISION) OF ANY SERVICE CONTRACTED TO BE PERFORMED BY A SERVICE PROVIDER EXCEED THE AGREED PRICE IN RESPECT OF ANY PARTICULAR SERVICE OR SINGAPORE DOLLARS ONE THOUSAND (OR ITS EQUIVALENT IN YOUR LOCAL CURRENCY), WHICHEVER IS LOWER. </li>
                    <li> TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THE COMPANY ASSUMES NO LIABILITY OR RESPONSIBILITY FOR ANY (I) ERRORS, MISTAKES, OR INACCURACIES OF CONTENT; (II) PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO THE PLATFORM OR USE OF OUR SERVICE; (III) ANY UNAUTHORISED ACCESS TO OR USE OF OUR SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION STORED THEREIN; (IV) ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM THE WEBSITE; (V) ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE THAT MAY BE TRANSMITTED TO OR THROUGH THE WEBSITE BY ANY PERSON; (VI) ANY ERRORS OR OMISSIONS IN ANY CONTENT OR FOR ANY LOSS OR DAMAGE INCURRED AS A RESULT OF THE USE OF ANY CONTENT POSTED, EMAILED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE THROUGH THE WEBSITE; AND/OR (VII) USER CONTENT OR THE DEFAMATORY, OFFENSIVE, INAPPROPRIATE OR ILLEGAL CONDUCT OF ANY THIRD PARTY. THE FOREGOING LIMITATION OF LIABILITY SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY LAW IN THE APPLICABLE JURISDICTION IN WHICH YOU ARE LOCATED OR IN WHICH YOU ARE ACCESSING THE WEBSITE.</li>
                    <li> Except as stated in the <span style={{ fontWeight: "bold" }}>TERMS AND CONDITIONS</span>, the Company accepts no liability for any consequential loss, damage or expense of any kind arising from, or in consequence of any fault of defect of any nature whatsoever. We can accept no liability for death, injury, loss or damage of any kind whatsoever whether to person or property however caused, arising out of or in connection with the use of any product or service recommended or provided by the Service Providers on this Website. We accept no liability in respect of loss or damage to third parties caused directly or indirectly by any product or service supplied by any advertiser on this Website. </li>
                    <li>To the fullest extent permitted at law the Company disclaims any liability for damages arising out of or in connection with the use of this Website including without limitation the implied warranties of merchantability, fitness for a particular purpose, title, any direct, incidental, consequential, special or indirect including, but not limited to damages for loss of data, loss of income or profits, loss of business information, loss of programs, costs of procurement of substitute services or product, service interruptions, damage to property, death of personal injury and claims of third parties even if the Company or its agents or employees or representatives know or have been advised of the possibility of such damages.  </li>
                    <li>Use of this Website is at your own risk. The Company is not liable for any direct, indirect, punitive, incidental or special or consequential damages or other injury arising out of or in any way connected with the use of this Website or with the delay or inability to use this Website, any hacking of this Website for any information, software, products and services obtained through this Website, or otherwise arising out of the use of this Website, whether resulting in whole or in part, from breach of contract, tortious behaviour, negligence, strict liability or otherwise, even if the Company or its agents or its employees, or representatives know or have been advised of the possibility of such damages.  </li>
                    <li> Use of products and services including but not limited to any software, website, forum, social network page, videos and the like in any form or manner (including but not limited to real money trading, paper trading, accessing the Website,  forum, pages, social networks etc.) implies an acceptance of all the terms and conditions in this <span style={{ fontWeight: "bold" }}>DISCLAIMER OF LIABILITY</span>. Any commitment from the Company not explicitly mentioned in this <span style={{ fontWeight: "bold" }}>DISCLAIMER OF LIABILITY</span> is implicitly excluded.</li>
                    <li>The exclusions and limitations of liability under this Web User Agreement will not apply to the extent prohibited by applicable law. </li>
                  </ol>
                </div>


                {/* <div style={{ margin: '50px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Contacting Us </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                   <li> If you would like to bring any issue regarding misleading information, lewd content, theft of identity etc. found on this Website to us, please use our Email icon, WhatsApp icon, or Call us icon found on our Website, which will allow you to describe the issue in detail. You will be required to verify your identity with relevant documents should we require you to do so.</li>
                  </ol>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Role Of Directory </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>The Website serves only as a directory to link up Service Providers and End-Users. The Service Provider specified as the provider of the relevant Service that an End-User has contracted for (or may contract for) is (i). solely responsible for performing the relevant Service (ii). fully responsible for all Services it provides to End-Users, and (iii). liable for all damages or losses arising out of the goods or services provided.</li>
                  </ol>
                </div>



                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Disclaimer & Limitation of Liabilities </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>The Company is not responsible for (i). any price adjustments made by a Service Provider related to a Service or appointment time (although these must be agreed between the relevant Service Provider and End-User through this Website), or (ii). any claims for injuries, illnesses, damages, liabilities and costs (“Liabilities”) that any Service Provider or End-User may suffer, directly or indirectly, in full or in part, whether related to the performance of any of the Services (or non-performance of any of any Services which have been agreed to have been performed) or otherwise.</li>
                    <li>The Company does not guarantee accuracy of information provided through all means including but not limited to forums, webinar, web pages, social network, verbal or telephone communication on or through the Website. By using the Website, you are agreeing to verify such data on your own prior to the use of any information provided to you by or through this Website.</li>
                    <li>This material on the Website is for informative purposes only.</li>
                    <li>The information presented on the Website or by Registered Service Providers should only be relied on by people who have verified such data on their own through other sources and are aware of the risk inherent in carrying on activities associated with the services provided by our Registered Service Providers.</li>
                    <li>Past performance by any Service Provider is no guarantee of future return or future profit or performance. The Company is not an investment advisor. The Company does not make any recommendation or provide any endorsement for buying or selling any product or service offered by Registered Service Providers. The Company will have no liability for any investment or business decision made by users of the Website.</li>
                    <li>TO THE FULLEST EXTENT PERMITTED BY LAW, YOU AGREE TO AND HEREBY WAIVE AND RELEASE THE COMPANY AND ITS HOLDING COMPANY(IES), SUBSIDIARIES, ASSOCIATED COMPANIES OR ORGANISATIONS (WHETHER INCORPORATED OR NOT), AFFILIATES PARTNERS, OFFICERS, DIRECTORS, STAFF MEMBERS, SHAREHOLDERS AND AGENTS FROM ANY LIABILITIES ARISING FROM OR RELATED TO (I) ANY ACT OR OMISSION BY SERVICE PROVIDER IN THE PROVISION OF ANY SERVICES, INCLUDING A SERVICE PROVIDER’S FAILURE TO COMPLY WITH APPLICABLE LAW AND/OR FAILURE TO ABIDE BY THE AGREED TERMS OF ANY SERVICE, AND/OR (II) ANY ACTION OR INACTION BY A SERVICE PROVIDER AND/OR (III) ANY INDIRECT, SPECIAL, PUNITIVE, CONSEQUENTIAL, (INCLUDING, LOST PROFITS OR LOST DATA COLLECTED THROUGH THIS WEBSITE OR BY A SERVICE PROVIDER), OR INCIDENTAL DAMAGES, WHETHER BASED ON A CLAIM OR ACTION OF CONTRACT, WARRANTY, NEGLIGENCE, STRICT LIABILITY, OR OTHER TORT, BREACH OF ANY STATUTORY DUTY, INDEMNITY OR CONTRIBUTION, OR OTHERWISE, EVEN IF THE COMPANY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. IN NO EVENT WILL THE COMPANY’S LIABILITY ARISING OUT OF OR RELATED TO THESE TERMS OF USE, OR IN RESPECT OF THE PROVISION (OR NON-PROVISION) OF ANY SERVICE CONTRACTED TO BE PERFORMED BY A SERVICE PROVIDER EXCEED THE AGREED PRICE IN RESPECT OF ANY PARTICULAR SERVICE OR SINGAPORE DOLLARS ONE THOUSAND (OR ITS EQUIVALENT IN YOUR LOCAL CURRENCY), WHICHEVER IS LOWER.</li>
                    <li>TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THE COMPANY ASSUMES NO LIABILITY OR RESPONSIBILITY FOR ANY (I) ERRORS, MISTAKES, OR INACCURACIES OF CONTENT; (II) PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO THE PLATFORM OR USE OF OUR SERVICE; (III) ANY UNAUTHORISED ACCESS TO OR USE OF OUR SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION STORED THEREIN; (IV) ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM THE WEBSITE; (V) ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE THAT MAY BE TRANSMITTED TO OR THROUGH THE WEBSITE BY ANY PERSON; (VI) ANY ERRORS OR OMISSIONS IN ANY CONTENT OR FOR ANY LOSS OR DAMAGE INCURRED AS A RESULT OF THE USE OF ANY CONTENT POSTED, EMAILED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE THROUGH THE WEBSITE; AND/OR (VII) USER CONTENT OR THE DEFAMATORY, OFFENSIVE, INAPPROPRIATE OR ILLEGAL CONDUCT OF ANY THIRD PARTY. THE FOREGOING LIMITATION OF LIABILITY SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY LAW IN THE APPLICABLE JURISDICTION IN WHICH YOU ARE LOCATED OR IN WHICH YOU ARE ACCESSING THE WEBSITE.</li>
                    <li>Except as stated in the TERMS AND CONDITIONS, the Company accepts no liability for any consequential loss, damage or expense of any kind arising from, or in consequence of any fault of defect of any nature whatsoever. We can accept no liability for death, injury, loss or damage of any kind whatsoever whether to person or property however caused, arising out of or in connection with the use of any product or service recommended or provided by the Service Providers on this Website. We accept no liability in respect of loss or damage to third parties caused directly or indirectly by any product or service supplied by any advertiser on this Website.</li>
                    <li>To the fullest extent permitted at law the Company disclaims any liability for damages arising out of or in connection with the use of this Website including without limitation the implied warranties of merchantability, fitness for a particular purpose, title, any direct, incidental, consequential, special or indirect including, but not limited to damages for loss of data, loss of income or profits, loss of business information, loss of programs, costs of procurement of substitute services or product, service interruptions, damage to property, death of personal injury and claims of third parties even if the Company or its agents or employees or representatives know or have been advised of the possibility of such damages.</li>
                    <li>Use of this Website is at your own risk. The Company is not liable for any direct, indirect, punitive, incidental or special or consequential damages or other injury arising out of or in any way connected with the use of this Website or with the delay or inability to use this Website, any hacking of this Website for any information, software, products and services obtained through this Website, or otherwise arising out of the use of this Website, whether resulting in whole or in part, from breach of contract, tortious behaviour, negligence, strict liability or otherwise, even if the Company or its agents or its employees, or representatives know or have been advised of the possibility of such damages.</li>
                    <li>Use of products and services including but not limited to any software, website, forum, social network page, videos and the like in any form or manner (including but not limited to real money trading, paper trading, accessing the Website, forum, pages, social networks etc.) implies an acceptance of all the terms and conditions in this DISCLAIMER OF LIABILITY. Any commitment from the Company not explicitly mentioned in this DISCLAIMER OF LIABILITY is implicitly excluded.</li>
                    <li>The exclusions and limitations of liability under this Web User Agreement will not apply to the extent prohibited by applicable law.</li>

                  </ol>
                </div> */}



                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Contacting Us </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>If you would like to bring any issue regarding misleading information, lewd content, theft of identity etc. found on this Website to us, please use our Email icon, WhatsApp icon, or Call us icon found on our Website, which will allow you to describe the issue in detail. You will be required to verify your identity with relevant documents should we require you to do so.</li>
                    <li>If you have any questions about our <span style={{ fontWeight: "bold" }}>DISCLAIMER OF LIABILITY</span>, the practices of this site, or have any comments and suggestions about this Online Site, please contact ICH iFinancial Singapore Private Limited, UEN: 201631146G at <EmailJoanna /></li>




                  </ol>
                </div>

              </div>
            </div>
          </div>
        </NoSsr>
        <BotNavBar referrer={this.state.referrer} />

      </div>
    );
  }
}


export default withStyles(styles)(DisclaimerOfLiability);