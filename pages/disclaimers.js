import React, { Component } from 'react';
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import { NoSsr, Grid } from '@material-ui/core';
import TopNavBar from './components/TopNavBar'
import BotNavBar from './components/BotNavBar'

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Constants from 'constants/constants';
import Head from 'next/head';


const styles = {
  page_width: Constants.style.page.width,
  page_text_width: Constants.style.page.text_width,

  page_padding: Constants.style.page.padding,

  align_center: Constants.style.align.center,
  align_left: Constants.style.align.left,

  bold: Constants.style.text.bold,
  break_word: Constants.style.text.break_word,

  title1: Constants.style.text.title1,
  body1: Constants.style.text.body1,

  margin_top_xl: Constants.style.margin_top.xl,
  margin_top_lg: Constants.style.margin_top.lg,
  margin_top_md: Constants.style.margin_top.md,
  margin_top_sm: Constants.style.margin_top.sm,
  margin_top_xs: Constants.style.margin_top.xs,
  margin_top_xxs: Constants.style.margin_top.xxs,
}


class Disclaimers extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount = () => {
    Constants.store_referrer(this)
  }

  render() {
    const { classes } = this.props;

    return (
      <div className="disclaimers">
        <TopNavBar selected="disclaimers" referrer={this.state.referrer} />

        <NextSeo config={Constants.next_seo_config("disclaimers")} />

        <NoSsr>

          <div className={classNames(classes.align_center)}>
            <div className={classNames(classes.page_text_width, classes.page_padding, classes.break_word, classes.align_left)}>
              <div style={{
                margin: "10px 0",
                fontSize: "40px",
                fontWeight: "bold",
                fontFamily: 'Merriweather'
              }}>
                Disclaimers
              </div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}><span style={{ fontWeight: "bold" }}>(I)</span> iFinSG is a insurance technology platform, which does not fall under the Insurance Act as an insurer or insurance intermediary broker, nor do we help any insurance companies to sell their policies.</div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>We do not earn a potentially unlimited Underwriting Profit from premiums, in return for the potential possibility to pay out unlimited claims within a cycle.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>We do not earn any Underwriting Profit / Income at all. ( Click here to understand more : <a target="blank" href={"https://www.investopedia.com/terms/u/underwriting-income.asp"} style={{ color: "#7cda24" }}>https://www.investopedia.com/terms/u/underwriting-income.asp</a> . )</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> Cyclic insurance only earns a flat and transparent monthly <span style={{ fontWeight: 'bold' }}>Admin Fee</span> per policy per user, collects premiums as a pool in order to pay out claims, so at the end of every cycle, after paying out verified claims, all premiums unused for claims will be returned as <span style={{ fontWeight: 'bold' }}>PremiumBack</span> to users who made 0 claims and users who made low claims. </div>


              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}><span style={{ fontWeight: "bold" }}>(II)</span> iFinSG is not regulated by MAS as an insurer or insurance intermediary, therefore our users are not included under the Deposit Insurance and Policy Owners’ Protection Schemes Act (Cap. 77B), and our users will not have access to the dispute resolution scheme managed by the Financial Industry Disputes Resolution Centre.</div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}><span style={{ fontWeight: "bold" }}>(III)</span> We do not guarantee that our premiums will always stay the same.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Although we strongly prefer to keep our premiums the same, premiums for your policy are determined based on true claims data from the previous cycle of cover.</div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}><span style={{ fontWeight: "bold" }}>(IV)</span> Premiums may be increased in the exceptional occasions of Situation [B] or Situation [C] :</div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}><span style={{ fontWeight: "bold" }}>Situation [A]</span> – Premiums are sufficient</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>As all users are allowed make Claims for up to 3 months after the Date of an Accident / Incident, some Claims ( whether made by you or other users ) which take place in your current cycle could be due to an Accident / Incident which happened in an earlier cycle, so you must be aware that part of your Premiums for your current cycle may be shared for that purpose.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}><span style={{ fontWeight: "bold" }}>Situation [B]</span> – Premiums are insufficient due to a Brief increase of claims</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If there are exceptional situations where there is an insufficiency of premiums which lead to Pending Claims ( whether made by you or other users ) from an earlier cycle, the premiums pool will need to be temporarily increased in your current cycle to pay out the Pending Claims from the earlier cycle, so you must be aware that you may pay a temporary Higher Premium ( eg. if the Small Accident cover’s premium is temporarily more than $6.80 a month ) for your current cycle for that purpose.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>We will temporarily increase premiums just for the immediate next cycle, and the following cycle’s premium will revert back to the original.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}><span style={{ fontWeight: "bold" }}>Situation [C]</span> – Premiums are insufficient due to a Trend of a general increase in claims</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If there is an exceptional situation where there is a trend of an insufficiency of premiums / an insufficiency of premiums which lead to Pending Claims, the premiums pool will need to be permanently increased for future cycles to ensure premium sufficiency, so you must be aware that you may need to pay a permanently Higher Premium ( eg. the Small Accident cover’s premium becomes permanently more than $6.80 a month ) for future cycles for that purpose.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>We will permanently increase premiums for future cycles to ensure that the premiums accurately reflect the trend of claim increases.</div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>For new users, before any increase of premiums takes place in Situation [B] or Situation [C], rest assured that we will definitely update the info on our website, and any new premium amount will only start on the 1st Day of each new cycle.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>For existing users, before any increase of premiums takes place in Situation [B] or Situation [C], rest assured that we will definitely inform you via email at least 3 days in advance ( in case you do not wish to continue with your cover, just reply us to stop the Credit or Debit card billing ), and any new premium amount will only start on the 1st Day of each new cycle.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>In the unforeseen event that you did not manage to read your email within the 3 days in advance, should you decide to discontinue your cover, just reply us that you would like to stop, and we will give you a <span style={{ fontWeight: "bold" }}>Cancellation Refund</span> based on the remaining unused days of the cycle = pro-rated premiums ( from the premiums pool ) + pro-rated Admin Fees – full Stripe’s charges incurred.</div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}><span style={{ fontWeight: "bold" }}>(V)</span> We don’t earn a single dollar from premiums, thus we will never increase premiums unnecessarily. Every dollar of premiums is only for 2 purposes: <span style={{ fontWeight: "bold" }}>PremiumBack</span> and Claims.</div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>The only income we earn as a business is a flat and transparent monthly <span style={{ fontWeight: 'bold' }}>Admin Fee</span> per policy per user.</div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}><span style={{ fontWeight: "bold" }}>(VI)</span> Users of iFinSG must be clear and aware that using our <span style={{ fontWeight: "bold" }}>Cyclic insurance</span> services is a totally different approach, compared to buying policies from an insurer or insurance intermediary broker.</div>
              <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>If you are clear about why you want to be a <span style={{ fontWeight: 'bold' }}>Cyclic insurance</span> user, <a href={Constants.get_new_page("products", this.state.referrer)} style={{ color: "#7cda24" }}>Sign Up</a> with us now!</div>

              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>  </div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>  </div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>  </div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>  </div>
              <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>  </div>



            </div>


          </div>

        </NoSsr>

        <BotNavBar referrer={this.state.referrer} />

      </div>
    );
  }
}


export default withStyles(styles)(Disclaimers);