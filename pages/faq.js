import React, { Component } from 'react';
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo

import { NoSsr, Grid, Button, Divider, FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';

import * as Scroll from 'react-scroll';
import { Link, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'

import TopNavBar from './components/TopNavBar'
import BotNavBar from './components/BotNavBar'

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Constants from 'constants/constants';

import Head from 'next/head';
// import LinkNext from "next/link";



const styles = {
  page_width: Constants.style.page.width,
  page_text_width: Constants.style.page.text_width,

  page_padding: Constants.style.page.padding,

  align_center: Constants.style.align.center,
  align_left: Constants.style.align.left,

  bold: Constants.style.text.bold,
  break_word: Constants.style.text.break_word,

  title1: Constants.style.text.title1,
  body1: Constants.style.text.body1,

  margin_top_xl: Constants.style.margin_top.xl,
  margin_top_lg: Constants.style.margin_top.lg,
  margin_top_md: Constants.style.margin_top.md,
  margin_top_sm: Constants.style.margin_top.sm,
  margin_top_xs: Constants.style.margin_top.xs,
  margin_top_xxs: Constants.style.margin_top.xxs,
}


class FAQ extends Component {
  constructor(props) {
    super(props);
    this.state = {
      referrer: "",
      how_to_claim_policy_selected: 0
    };
  }

  componentDidMount = () => {

    Constants.store_referrer(this)



    Events.scrollEvent.register('begin', function (to, element) {
    });

    Events.scrollEvent.register('end', function (to, element) {
    });

    scrollSpy.update();
  }
  componentWillUnmount = () => {
    Events.scrollEvent.remove('begin');
    Events.scrollEvent.remove('end');
  }


  handleSetActive = (to) => {
  }


  renderHowToClaim = () => {
    switch (this.state.how_to_claim_policy_selected) {
      case 0:
        return (
          <div>


            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>When you need to make a claim, just:</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>1. Tell us:</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>
              <ol>
                <li style={{ listStyle: 'none' }}>(i) What happened in the incident</li>
                <li style={{ listStyle: 'none' }}>(ii) Who was the SMC-registered / TCMPB-registered Doctor you consulted</li>
                <li style={{ listStyle: 'none' }}>(iii) Which clinic / hospital you visited</li>
              </ol>
            </div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>2. Upload photos of:</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>
              <ol>
                <li style={{ listStyle: 'none' }}>(i) Paid Receipt</li>
                <li style={{ listStyle: 'none' }}>(ii) Itemized Invoice of treatment and medicine</li>
                <li style={{ listStyle: 'none' }}>(iii) MC with diagnosis / Letter of diagnosis</li>
              </ol>
            </div>

            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>* Just make sure the Doctor writes the <u>Date of accident</u> + <u>Description of accident</u>, on any 1 of the documents in the photos which you upload ( either Receipt / Invoice / MC with diagnosis / Letter of diagnosis ).</div>

            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>* If you received treatment from the A&E department of a Public Hospital outside of Singapore, please upload photos of the entry and exit stamps in your Passport as evidence that you were in that country.</div>

            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
              SMC is the Singapore Medical Council ( a statutory board under the Ministry of Health ), governed under Section 5 of the Medical Registration Act (Cap 174). You can check if your doctor is SMC registered, via this link: <a href={"https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC"} target={"_blank"} style={{ color: "#7cda24", wordWrap: "break-word" }}> <span style={{ wordWrap: "break-word" }}> https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC </span></a>.
            </div>

            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
              TCMPB is the Traditional Chinese Medicine Practitioners Board ( a statutory board under the charge of the Ministry of Health ), under the Traditional Chinese Medicine Practitioners Act 2000, governed under the Traditional Chinese Medicine Practitioners Act (Cap. 333A). You can check if your doctor is TCMPB registered, via this link: <a href={"https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=TCM"} target={"_blank"} style={{ color: "#7cda24" }}>https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=TCM</a>.
            </div>

            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>In the event that an overseas medical service provider is involved, we will contact them to verify the authenticity.</div>

            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>At all times, if there are any extra costs involved in the claims verification process (such as translation costs), we may either bill you and deduct it from your claim payout, or bill you separately if the claim cannot be substantiated.</div>

            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>* On the Date of any Claim Submission, your policy cover must be active in order for it to be a processable Claim.</div>

            <div style={{ margin: '10px 0px 0px 0px', fontSize: '20px' }}>ie. If your policy cover is no longer active for any reason, a new Claim Submission will not be entertained.</div>


          </div>
        )
      case 1:
        return (
          <div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>When you need to make a claim, just:</div>

            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>1. Tell us:</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>
              <ol>
                <li style={{ listStyle: 'none' }}>(i) What happened in the incident</li>
                <li style={{ listStyle: 'none' }}>(ii) Who was the SMC-registered Doctor you consulted</li>
                <li style={{ listStyle: 'none' }}>(iii) Which GP clinic / government polyclinic you visited</li>
              </ol>
            </div>

            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>2. Upload photos of:</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>
              <ol>
                <li style={{ listStyle: 'none' }}>(i) Paid Receipt</li>
                <li style={{ listStyle: 'none' }}>(ii) Itemized Invoice of treatment and medicine</li>
                <li style={{ listStyle: 'none' }}>(iii) MC with diagnosis / Letter of diagnosis</li>
              </ol>
            </div>

            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>* Just make sure the Doctor writes the <u>Date of diagnosis</u> + <u>Description of diagnosis</u>, on any 1 of the documents in the photos which you upload ( either Receipt / Invoice / MC with diagnosis / Letter of diagnosis ).</div>

            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
              SMC is the Singapore Medical Council ( a statutory board under the Ministry of Health ), governed under Section 5 of the Medical Registration Act (Cap 174). You can check if your doctor is SMC registered, via this link:  <a href={"https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC"} target={"_blank"} style={{ color: "#7cda24", wordWrap: "break-word" }}> <span style={{ wordWrap: "break-word" }}>https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC</span></a>.
            </div>

            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>* On the Date of any Claim Submission, your policy cover must be active in order for it to be a processable Claim.</div>

            <div style={{ margin: '10px 0px 0px 0px', fontSize: '20px' }}>ie. If your policy cover is no longer active for any reason, a new Claim Submission will not be entertained.</div>

          </div>
        )
      case 2:
        return (
          <div>

            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>When you need to make a claim, just:</div>

            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>1. Tell us:</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>
              <ol>
                <li style={{ listStyle: 'none' }}>(i) What happened in the incident</li>
                <li style={{ listStyle: 'none' }}>(ii) Who was the SMC-registered Doctor you consulted</li>
                <li style={{ listStyle: 'none' }}>(iii) Which clinic / hospital you visited</li>
              </ol>
            </div>

            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>2. Upload photos of:</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>
              <ol>
                <li style={{ listStyle: 'none' }}>(i) Paid Receipt</li>
                <li style={{ listStyle: 'none' }}>(ii) Itemized Invoice of treatment and medicine</li>
                <li style={{ listStyle: 'none' }}>(iii) MC with diagnosis / Letter of diagnosis</li>
              </ol>
            </div>

            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
              * Just make sure the Doctor writes the <u>Date of accident</u> + <u>Description of accident</u>, on any 1 of the documents in the photos which you upload ( either Receipt / Invoice / MC with diagnosis / Letter of diagnosis ).
            </div>

            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>SMC is the Singapore Medical Council ( a statutory board under the Ministry of Health ), governed under Section 5 of the Medical Registration Act (Cap 174). You can check if your doctor is SMC registered, via this link: <a href={"https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC"} target={"_blank"} style={{ color: "#7cda24", wordWrap: "break-word" }}> <span style={{ wordWrap: "break-word" }}> https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC </span></a>.
            </div>

            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>* On the Date of any Claim Submission, your policy cover must be active in order for it to be a processable Claim.</div>

            <div style={{ margin: '10px 0px 0px 0px', fontSize: '20px' }}>ie. If your policy cover is no longer active for any reason, a new Claim Submission will not be entertained.</div>

          </div>
        )

    }
  }

  renderQuestionsList = (faq_content) => {
    let renderable = []
    renderable.push(<div style={{ marginTop: "15px" }}></div>)

    for (let i = 0; i < faq_content.length; i++) {
      const question =
        <div style={{ width: "100%" }}>
          {i === 0 ? <div style={{ fontWeight: "bold", marginTop: "30px" }}> <u>Features </u> </div> : ""}
          {i === 5 ? <div style={{ fontWeight: "bold", marginTop: "30px" }}> <u>PremiumBack </u> </div> : ""}
          {i === 11 ? <div style={{ fontWeight: "bold", marginTop: "30px" }}> <u>Claims </u> </div> : ""}
          {i === 15 ? <div style={{ fontWeight: "bold", marginTop: "30px" }}> <u>Sign Ups </u> </div> : ""}
          {i === 22 ? <div style={{ fontWeight: "bold", marginTop: "30px" }}> <u>Cancellations </u> </div> : ""}
          {i === 24 ? <div style={{ fontWeight: "bold", marginTop: "30px" }}> <u>Premiums </u> </div> : ""}
          {i === 26 ? <div style={{ fontWeight: "bold", marginTop: "30px" }}> <u>General FAQ </u> </div> : ""}

          <Link activeClass="active" to={i.toString()} spy={true} smooth={true} offset={-100} duration={500} onSetActive={this.handleSetActive}>
            <Button style={{ marginTop: "10px", paddingLeft: "2px", paddingRight: "2px", textAlign: "left", textTransform: 'unset' }}> {i + 1} - {faq_content[i].question} </Button>
          </Link>
        </div>

      renderable.push(question)

    }

    return renderable
  }


  renderContent = () => {
    const { classes } = this.props;

    const faq_content = [
      {
        // question: <div className={classNames(classes.title1, classes.bold)}> How was Cyclic insurance created? </div>,
        section: "Features",
        question: "How was Cyclic insurance created?",
        answer: <div>
          <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> Insurance need not be a grudge purchase, nor a pain to use. </div>
          <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> We believe that insurance should be a basic Human right which every human being should be able to afford, regardless of wealth. </div>
          <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> iFinSG uses technology and domain knowledge to create a form of P2P insurance benefits which is Simple, Fast and Cheap for you.</div>
          <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> As our type of P2P insurance benefits operate in cycles of time periods, we call it  <span className={classNames(classes.bold)}>Cyclic insurance</span>.  </div>
        </div>
      }, {
        question: "How does Cyclic insurance work?",
        answer: <div>
          <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> We collect premiums + an <span className={classNames(classes.bold)}>Admin Fee</span> from each user. </div>
          <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> Cyclic insurance only collects premiums as a pool in order to pay out claims, thus we treat collected premiums as if they were still your money, and return all premiums unused for claims, back to users as  <span className={classNames(classes.bold)}>PremiumBack</span> after the end of every cycle.</div>
          <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> Every dollar of premiums we collect, is only for 2 purposes: <span className={classNames(classes.bold)}>PremiumBack</span> and Claims. </div>
          <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>The only income we earn as a business, is a flat and transparent monthly <span className={classNames(classes.bold)}>Admin Fee</span> per policy per user. </div>
          <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>As iFinSG only earns a monthly  <span className={classNames(classes.bold)}>Admin Fee</span> per policy per user, we love all users equally, instead of focusing on wealthier users!</div>
        </div>
      },
      {
        question: "How is iFinSG different from a traditional insurance company?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> iFinSG is an InsurTech firm which runs a P2P insurance system that focuses on the welfare of users, not the profit of shareholders. </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> Traditional insurance companies keep the money they don’t pay out in claims. This means that every additional $1 paid out as Claims, translates to $1 less in underwriting Profit. </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> We are structured to prevent any conflict of interest, by making sure that all premiums unused for claims, are given back to our users at the end of every cycle as <span className={classNames(classes.bold)}>PremiumBack</span>. We gain nothing by delaying or denying claims. </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> <span className={classNames(classes.bold)}>PremiumBack</span> is given to 2 types of users: users who make 0 claims, and users who make low claims. </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> Our proprietary algorithm is structured so that the lower the total claims you make, the higher the <span className={classNames(classes.bold)}>PremiumBack</span> you will qualify for each cycle. If you made 0 claims, you’ll get the highest level of <span className={classNames(classes.bold)}>PremiumBack</span>. </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> As paying claims is our core priority, occasionally when premiums for a policy are fully paid out as claims, users may receive less <span className={classNames(classes.bold)}>PremiumBack</span> than normal, however at all times do be assured that: </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> A user who made 0 claims will always receive a <span className={classNames(classes.bold)}>PremiumBack</span>, <u>guaranteed to be at least 10% of your premiums placed with us.</u> </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> A user who made low claims will always receive a <span className={classNames(classes.bold)}>PremiumBack</span>, <u>guaranteed to be at least 5% of your premiums placed with us.</u> </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> You enjoy amazing protection with us (nobody would try to deny your claim), everything is based on real claims data, and there’s always money coming back to you! </div>
          </div>
      },
      {
        question: "Can I purchase just 1 cycle of your Cyclic insurance to try it out?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> Of course you can. 1 cycle of Small cover is currently defined as 3 months.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> This may eventually be converted to 6 months or 12 months, but we will inform our users before we administer any changes.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> Premiums unused for claims, are given back to our users at the end of every cycle as <span style={{ fontWeight: "bold" }}>PremiumBack.</span></div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> Your <span style={{ fontWeight: "bold" }}>Cyclic insurance</span> will be renewed at the end of each cycle.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> If your policy starts in the midst of our cycle, you only pay a pro-rated premium plus a pro-rated <span style={{ fontWeight: "bold" }}>Admin Fee.</span></div>
          </div>
      },
      {
        question: "Money back Guarantee",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> We are confident to give you our Money back Guarantee:</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> After you have tried our service for a 1st cycle, should you decide not to use our services again, we'll still give you back a value equivalent to 100% of your 1st cycle Premiums!</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px', textAlign: "left" }}>
              <ul>
                <li>Applicable if you cancel your cover within 14 days after your 1st cycle.</li>
                <li>Valid if you did not make any claim in your 1st cycle.</li>
                <li>Inclusive of the <b>PremiumBack</b> given for your 1st cycle.</li>
                <li>Applicable if you did not take any form of promotional vouchers, discounts or equivalent benefits for your purchase.</li>

              </ul>
            </div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> Try our service, because we know you’ll love it!</div>
          </div>
      },
      {
        section: "PremiumBack",
        question: "Is a PremiumBack guaranteed?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> Yes, it is.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> Do take note however, that <span style={{ fontWeight: "bold" }}>PremiumBack</span> will only be given to 2 types of users: users who make 0 claims, and users who make low claims.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> A user who made 0 claims will always receive a <span style={{ fontWeight: "bold" }}>PremiumBack</span>, <u>guaranteed to be at least 10% of your premiums placed with us.</u></div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> A user who made low claims will always receive a <span style={{ fontWeight: "bold" }}>PremiumBack</span>, <u>guaranteed to be at least 5% of your premiums placed with us.</u></div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> If there is any user who cancels a policy prematurely before the end of a cycle, the user will immediately be fully removed from that policy’s <span style={{ fontWeight: "bold" }}>PremiumBack</span> calculation system, to ensure that <span style={{ fontWeight: "bold" }}>PremiumBack</span> for other users will not be affected.</div>

          </div>
      },
      {
        question: "What PremiumBack amount would I get, if no user makes a claim in a particular cycle?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> If no user makes a claim in a particular cycle, you would get back all your premiums for the cycle as <span style={{ fontWeight: "bold" }}>PremiumBack</span>.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Eg. For the Small Accident cover, your monthly premium would be $6.80, while the monthly Admin Fee is $3. If you participated for a full cycle, the total premium would be $6.80 x 3 months = $20.40.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If no user makes a claim in a particular cycle, you would thus receive a <span style={{ fontWeight: "bold" }}>PremiumBack</span> of $20.40.</div>
          </div>
      },
      {
        question: "What defines Low claims for PremiumBack?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>It will be different for each policy, and stated clearly. </div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> Eg.</div>
            <div style={{ margin: '50px 0px 0px 0px', fontSize: '20px' }}>Cyclic: Small Accident</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Users who claim SGD$60 or less per cycle.</div>
            <div style={{ margin: '50px 0px 0px 0px', fontSize: '20px' }}>Cyclic: Small Influenza GP</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Users who claim SGD$50 or less per cycle</div>
            <div style={{ margin: '50px 0px 0px 0px', fontSize: '20px' }}>Cyclic: Small Accident Income</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Users who claim SGD$120 or less per cycle.</div>
            <div style={{ margin: '50px 0px 0px 0px', fontSize: '20px' }}>Cyclic: Small Critical Checkup</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Users who claim SGD$180 or less per cycle.</div>
            <div style={{ margin: '50px 0px 0px 0px', fontSize: '20px' }}>Cyclic: Medium Accident</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Users who claim SGD$100 or less per cycle.</div>
          </div>
      },
      {
        question: "How do I receive PremiumBack?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}><span style={{ fontWeight: "bold" }}>PremiumBack</span> will be paid as a direct transfer to your Singapore bank account.</div>
          </div>
      },
      {
        question: "When would I receive the PremiumBack? ",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}><span style={{ fontWeight: "bold" }}>PremiumBack</span> will be paid as a direct transfer to your Singapore bank account, within 7 working days after the end of each cycle.</div>
          </div>
      },
      {
        question: "When would I know the exact PremiumBack I would get in a cycle?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>After the end of each cycle, we will transfer the relevant <span style={{ fontWeight: "bold" }}>PremiumBack</span>  monies to all qualified users within 7 working days.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>We will then proceed to inform all users via email.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>After you log in to our website, your personal <b>Profile</b> page would show clearly how much <span style={{ fontWeight: "bold" }}>PremiumBack</span> you received for the most recent cycle.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Your actual insurance Cost for a cycle = Total premium you gave to us, minus the <span style={{ fontWeight: "bold" }}>PremiumBack</span> which we have given back to you.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Divide that out by the 3 months in a cycle, and you’ll be amazed by how affordable we are!</div>
          </div>
      },
      {
        section: "Claims",
        question: "How do I make a Claim?",
        answer:
          <div style={{ wordWrap: "break-word" }}>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>We use an online claims system, which is fast and convenient for you.</div>
            <div style={{ marginTop: "40px", textAlign: "center", fontSize: '20px' }}>
              <div style={{ backgroundColor: "rgb(124, 218, 36, 0.5)", width: "fit-content", display: "inline-block", padding: "20px",lineHeight: "36px" }}>
                <div style={{ padding: "0px 10px 0px 0px", fontSize: '20px', display: "inline" }}> Policy:  </div>
                <FormControl>
                  {/* <InputLabel htmlFor="age-simple">Age</InputLabel> */}
                  <Select
                    style={{ fontSize: "20px" }}
                    value={this.state.how_to_claim_policy_selected}
                    onChange={(e) => {
                      this.setState({
                        how_to_claim_policy_selected: e.target.value
                      })
                    }}
                  >
                    <MenuItem value={0} style={{ fontSize: '20px' }}>Cyclic: Small Accident</MenuItem>
                    <MenuItem value={1} style={{ fontSize: '20px' }}>Cyclic: Small Influenza GP</MenuItem>
                    <MenuItem value={2} style={{ fontSize: '20px' }}>Cyclic: Small Accident Income</MenuItem>
                  </Select>
                </FormControl>
              </div>
            </div>



            {this.renderHowToClaim()}

          </div>
      },
      {
        question: "Can I make a claim from Cyclic insurance, after I make a claim from a traditional Insurer’s policy, for the same accident / incident?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>Traditional Insurers may not allow you to make a Reimbursement claim from 2 different insurers for the same accident / incident, but we work differently at iFinSG.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>When an unfortunate accident / incident happens, you should be allowed to claim All the relevant benefits which you have already paid for, via the premiums of your policies which you have already been paying to different insurers.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If a company wants to prevent you from making more than 1 Reimbursement claim from the same accident / incident, maybe that company should prevent you from buying a 2nd policy in the first place?</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}><span style={{ fontWeight: 'bold' }}> Cyclic insurance</span> will always allow you to make a claim from us for the same accident / incident, even after you make a similar claim from a traditional insurer.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}> Make a 2nd claim with us for the same Accident ( using photos of the Receipt and related documents ), even after you make a Reimbursement claim for your policy with an Insurer ( using the original Receipt and related documents ) !</div>


          </div>
      },
      {
        question: "How will I get paid for a Claim?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>A claim will be paid directly as a transfer to your Singapore bank account, after the authenticity and legitimacy of your claim has been verified, and subsequently approved by us.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If a claim submitted to us is found to be of a fraudulent nature, regardless of whether monies have been paid, we reserve all rights for the appropriate legal action.</div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>* On the Date of any Claim Submission, your policy cover must be active in order for it to be a processable Claim.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>ie. If your policy cover is no longer active for any reason, a new Claim Submission will not be entertained.</div>
          </div>
      },
      {
        question: "Can I renew my coverage of a Policy after a Claim?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>We believe that insurance should be simple, so we work on a WYSIWYG principle. WYSIWYG = What you see is what you get.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>For Cyclic insurance, each products's Policy Details will state clearly whether there is any limit of claims in a lifetime.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If you see a policy detail such as “ Valid for 2 Claims per Lifetime only ” or “ Valid for 1 Claim per Lifetime only ”, then that policy will only have a limited number of claims.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If you do not see such a policy detail for the policy, it means that you can continuously renew your Cyclic insurance cover even after a claim.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Of course, if anyone is deemed to display any fraudulent behavior, iFinSG will always reserve the right to prevent a person from renewing a policy from us.</div>

          </div>
      },
      {
        section: "Sign Ups",
        question: "How do I purchase a Cyclic insurance cover on iFinSG?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>You would be delighted to know that we use Stripe, which accepts both Credit card and Debit card.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Stripe works with Grab, Deliveroo, Unicef, Facebook, and National Geographic. (To view Stripe’s other customers, click here <a target={"_blank"} href={"https://stripe.com/us/customers"} style={{ color: "#7cda24" }}>https://stripe.com/us/customers</a> .)</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If you want to sign up for a Singapore policy with us, you must have an existing personal Singapore bank account, in order to receive claims / <span className={classNames(classes.bold)}>PremiumBack</span> / <span className={classNames(classes.bold)}>Cancellation Refund</span> from us.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If your policy starts in the midst of our cycle, you only pay a pro-rated premium plus a pro-rated <span className={classNames(classes.bold)}>Admin Fee</span>.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Visit our <a target={"_blank"} href={Constants.get_new_page("products", this.state.referrer)} style={{ color: "#7cda24" }}>PRODUCTS</a> page now!</div>

          </div>
      },
      {
        question: "How do I sign up for my Child?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>You would be delighted to know that you can sign up on behalf of your Child, as long as you upload a photo of the Birth Certificate of your child + a photo of your own NRIC as verification.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Should we require more verification of your relationship, we will request further information via email or call you via your mobile phone number.</div>


          </div>
      },
      {
        question: "Is my information kept confidential?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>We highly value and respect your privacy (see our Privacy Policy). We will not share or sell your information for marketing purposes without your explicit approval.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>In order to ensure our service runs smoothly ( such as the verification / authentication of claims ), on a need-to-do-so basis we may share certain personal information with our partners and providers.</div>

          </div>
      },
      {
        question: "When can I expect Cyclic insurance to be available in my city?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>If you’re in an ASEAN country, we’re on our way as fast as we can.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If you would like our service to reach your city earlier, contact us to discuss how we could accomplish that together. We would be happy to hear from you.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Stay tuned and follow us on Facebook and LinkedIn!</div>
          </div>
      },
      {
        question: "How can I reach you guys?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>You can conveniently get in touch with our team through our website.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Email us, use our WhatsApp chat channel, or Call us anytime!</div>
          </div>
      },
      {
        question: "What happens if my Application for a policy is not approved?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>Your Credit / Debit card deduction only takes place, after we have approved your application for a policy.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If your policy is not approved, your Credit or Debit card will not be billed, and naturally you would not be covered by our policy.</div>

          </div>
      },
      {
        question: "What if there is a Delay between my Application and the approval by iFinSG?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>Your policy will never start before we approve your Application.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Your Credit or Debit card would also never have a deduction, before we approve your Application.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>The amount which you will be billed via Stripe, will always be based on your actual policy start date, after we approve your Application.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If your policy starts at the <span style={{ fontWeight: 'bold' }}>beginning of our cycle</span>, you will pay <span style={{ fontWeight: 'bold' }}>a premium plus an Admin Fee.</span></div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If your policy starts in the <span style={{ fontWeight: 'bold' }}>midst of our cycle</span>, you only pay <span style={{ fontWeight: 'bold' }}>a pro-rated premium plus a pro-rated Admin Fee.</span></div>

          </div>
      },
      {
        section: "Cancellations",
        question: "If I want to cancel my Cyclic insurance policy, will I get a Cancellation Refund?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>You can cancel your <span style={{ fontWeight: 'bold' }}>Cyclic insurance</span> policy at any time by emailing us, and receive a <span style={{ fontWeight: 'bold' }}>Cancellation Refund</span> based on the remaining unused days of the cycle.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>A <span style={{ fontWeight: 'bold' }}>Cancellation Refund</span> will be based on the remaining unused days of the cycle = pro-rated premiums ( from the premiums pool ) + pro-rated Admin Fees – full Stripe’s charges incurred.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Any outstanding bills involved (such as translation costs in any Claims verification process), would also be deducted.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>A <span className={classNames(classes.bold)}>Cancellation Refund</span> will be paid as a direct transfer to your Singapore bank account, once we have calculated the accurate amount.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Once you have cancelled a particular policy, you will no longer be eligible to receive any related <span style={{ fontWeight: 'bold' }}>PremiumBack</span> and related Claims for that particular policy.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Of course, we welcome you to join us back anytime you like!</div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>* A user who has already made a successful claim and yet cancels a policy within the same cycle, will not receive any <span className={classNames(classes.bold)}>Cancellation Refund</span>, nor receive any <span style={{ fontWeight: 'bold' }}>PremiumBack.</span></div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If such behavior is repeated, iFinSG reserves the right to assess whether we will still approve your new policy application.</div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>* On the Date of any Claim Submission, your policy cover must be active in order for it to be a processable Claim.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>ie. If your policy cover is no longer active for any reason, a new Claim Submission will not be entertained.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>* In certain scenarios, if the full Stripe charges incurred is higher than the pro-rated premiums refund and the pro-rated Admin Fees refund, even if your <span className={classNames(classes.bold)}>Cancellation Refund</span> is $0, we will always give you a detailed explanation why it is so.</div>
          </div>
      },
      {
        question: "Can I cancel my Cyclic insurance policy after I make a Claim in the same cycle?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>Yes, you can still do so.</div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>* A user who has already made a successful claim and yet cancels a policy within the same cycle, will not receive any Cancellation Refund, nor receive any <span style={{ fontWeight: 'bold' }}>PremiumBack.</span></div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If such behavior is repeated, iFinSG reserves the right to assess whether we will still approve your new policy application.</div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>* On the Date of any Claim Submission, your policy cover must be active in order for it to be a processable Claim.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>ie. If your policy cover is no longer active for any reason, a new Claim Submission will not be entertained.</div>

          </div>
      },
      {
        section: "Premiums",
        question: "Are the Premiums for each policy, guaranteed to be the same within the cycle of my purchase?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>Yes. Once you have made a purchase and paid via Stripe, your premium amount is fixed for that cycle.</div>

          </div>
      },
      {
        question: "Are the Premiums for each policy, guaranteed to be the same forever?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>No. We do not guarantee that our premiums will always stay the same.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Although we strongly prefer to keep our premiums the same, premiums for your policy are determined based on true claims data from the previous cycle of cover.</div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>Premiums may be increased in the exceptional occasions of Situation [B] or Situation [C] :</div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}><span style={{ fontWeight: 'bold' }}>Situation [A]</span> – Premiums are sufficient</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>As all users are allowed make Claims for up to 3 months after the Date of an Accident / Incident, some Claims ( whether made by you or other users ) which take place in your current cycle could be due to an Accident / Incident which happened in an earlier cycle, so you must be aware that part of your Premiums for your current cycle may be shared for that purpose.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}><span style={{ fontWeight: 'bold' }}>Situation [B]</span> – Premiums are insufficient due to a Brief increase of claims</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If there are exceptional situations where there is an insufficiency of premiums which lead to Pending Claims ( whether made by you or other users ) from an earlier cycle, the premiums pool will need to be temporarily increased in your current cycle to pay out the Pending Claims from the earlier cycle, so you must be aware that you may pay a temporary Higher Premium ( eg. if the Small Accident cover’s premium is temporarily more than $6.80 a month ) for your current cycle for that purpose.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>We will temporarily increase premiums just for the immediate next cycle, and the following cycle’s premium will revert back to the original.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}><span style={{ fontWeight: 'bold' }}>Situation [C]</span> – Premiums are insufficient due to a Trend of a general increase in claims</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>If there is an exceptional situation where there is a trend of an insufficiency of premiums / an insufficiency of premiums which lead to Pending Claims, the premiums pool will need to be permanently increased for future cycles to ensure premium sufficiency, so you must be aware that you may need to pay a permanently Higher Premium ( eg. the Small Accident cover’s premium becomes permanently more than $6.80 a month ) for future cycles for that purpose.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>We will permanently increase premiums for future cycles to ensure that the premiums accurately reflect the trend of claim increases.</div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>For new users, before any increase of premiums takes place in Situation [B] or Situation [C], rest assured that we will definitely update the info on our website, and any new premium amount will only start on the 1st Day of each new cycle.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>For existing users, before any increase of premiums takes place in Situation [B] or Situation [C], rest assured that we will definitely inform you via email at least 3 days in advance ( in case you do not wish to continue with your cover, just reply us to stop the Credit or Debit card billing ), and any new premium amount will only start on the 1st Day of each new cycle.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>In the unforeseen event that you did not manage to read your email within the 3 days in advance, should you decide to discontinue your cover, just reply us that you would like to stop, and we will give you a <span style={{ fontWeight: 'bold' }}>Cancellation Refund</span> based on the remaining unused days of the cycle = pro-rated premiums ( from the premiums pool ) + pro-rated Admin Fees – full Stripe’s charges incurred.</div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>We don’t earn a single dollar from premiums, thus we will never increase premiums unnecessarily. Every dollar of premiums is only for 2 purposes: <span style={{ fontWeight: 'bold' }}>PremiumBack</span> and Claims.</div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>The only income we earn as a business is a flat and transparent monthly <span style={{ fontWeight: 'bold' }}>Admin Fee</span> per policy per user.</div>
          </div>
      },
      {
        section: "General FAQ",
        question: "How do I change Personal details for my Cyclic insurance policy?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>
              Just Login to your <b>Profile</b> page, go to <span style={{ fontWeight: 'bold' }}>Payer Info</span> or <span style={{ fontWeight: 'bold' }}>Life Insured Info</span>, click / press on the relevant <span style={{ fontWeight: 'bold' }}>Edit</span> buttons to make changes.
            </div>
          </div>
      },
      {
        question: "What Bank account should I use to receive Claims and PremiumBack?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>  You <span style={{ fontWeight: 'bold' }}>must have a Singapore bank account to apply</span> for a cover with us, because claims and PremiumBack will both only be paid to  <span style={{ fontWeight: 'bold' }}>a bank account opened in Singapore.</span> </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>  If you register <span style={{ fontWeight: 'bold' }}>an invalid bank account</span> for our service (eg. bank account was not opened in Singapore), for <span style={{ fontWeight: 'bold' }}>each unrectified</span> bank transfer of a claim or PremiumBack to you, we <span style={{ fontWeight: 'bold' }}>will have to bill you $1.10 via Stripe as a Penalty fee</span> to place $0.56 back to the premiums pool. </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>  If you register <span style={{ fontWeight: 'bold' }}>an invalid bank account</span> for our service (eg. bank account was not opened in Singapore), and you <span style={{ fontWeight: 'bold' }}>fail to rectify it in time before the last 15 days</span> of each cycle, you may not receive your monies for claims and PremiumBack as we cannot transfer money to an invalid bank account. </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>  If you have given us an invalid bank account by accident, you can easily change it on the <span style={{ fontWeight: 'bold' }}>Edit User Info</span>  button inside your <b>Profile</b> page. </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>   If you use a <span style={{ fontWeight: 'bold' }}>valid non-DBS/POSB Singapore bank account</span>, there will be <span style={{ fontWeight: 'bold' }}>a $0.50 charge deducted</span> for each claim and each PremiumBack which you receive from us via Bank transfer.</div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> If you use a <span style={{ fontWeight: 'bold' }}>valid DBS/POSB Singapore bank account</span>, there will be <span style={{ fontWeight: 'bold' }}>no charge</span> for each claim and each PremiumBack which you receive from us via Bank transfer.  </div>

          </div>
      },
      {
        question: "What if I miss a Premium payment?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>If your credit or debit card is declined, we’ll email you to provide us with a different one.</div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> Please note, we’ll remind you to make your payment, but eventually we’ll have to cancel your policy if your premium does not reach us.</div>

          </div>
      },
      {
        question: "How do I re-continue my cover if it has Lapsed or Stopped?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>You would be delighted to know that you can Login to your <span style={{ fontWeight: 'bold' }}>Profile</span> page, and use just 1 button to easily re-continue your cover with us. </div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> We’ll bill your credit / debit card after the Reinstatement of your cover is approved, based on the new starting date of your cover. </div>
          </div>
      },
      {
        question: "How do you ensure that Premiums are not mixed with the Admin Fees?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>Premiums collected for a particular policy, will be held in a 1 bank account with a local bank, and used for 2 purposes only: <span style={{ fontWeight: 'bold' }}>PremiumBack</span> and Claims.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Each policy will have 1 separate bank account for all related premiums, <span style={{ fontWeight: 'bold' }}>PremiumBack</span> and Claims, and the account does not mingle with other funds.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}><span style={{ fontWeight: 'bold' }}>Admin Fees</span> which we earn from all policies, are consolidated into another separate bank account as well.</div>

          </div>
      },
      {
        question: "What are the things not covered under my Cyclic insurance policy?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>We believe that insurance should be simple, so we work on a WYSIWYG principle. WYSIWYG = What you see is what you get.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Each product's Policy Details on our <a href={Constants.get_new_page("products", this.state.referrer)} style={{ color: "#7cda24" }}>PRODUCTS</a> page, fully explains the policy’s features, with no hidden terms and conditions at all.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>Any exclusion which would disqualify a claim for a particular policy, would be stated explicitly and clearly in the Policy Details of each product.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>We do not have a huge chunk of small text exclusions such as “no Claims in case of an act of God”.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>We have some common-sense rules which all our users and policies operate within:</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>(1) You cannot commit a crime intentionally and expect to make a successful claim.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>(2) You cannot commit self-inflicted damage and yet expect to make any claim.</div>
            <div style={{ margin: '20px 0px 0px 0px', fontSize: '20px' }}>(3) We don’t have a 3rd rule yet, but if you do think of something applicable and fair, let us know so that we will consider if we should add it in.</div>

          </div>
      },
      {
        question: "Will anyone be banned from using the Cyclic insurance service?",
        answer:
          <div>
            <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>At all times, if any user displays any form of behavior detrimental to iFinSG, Cyclic insurance and it's users ( such as but not limited to cheating / deception / fraud ), we reserve the right to immediately terminate our service for the user without any Cancellation Refund, and/or permanently ban the user from using our services in the future.</div>
          </div>
      },

    ]


    let renderable = []

    renderable.push(
      <div style={{
        margin: "10px 0",
        fontSize: "40px",
        fontWeight: "bold",
        fontFamily: 'Merriweather'
      }}> Frequently Asked Questions </div>
    )
    renderable.push(this.renderQuestionsList(faq_content))
    // renderable.push(<div style={{ marginTop: "20px" }} />)

    for (let i = 0; i < faq_content.length; i++) {

      renderable.push(
        <Element name={i.toString()} className="element">
          <div className={classNames(classes.margin_top_xl)}></div>

          {faq_content[i].section ? <div className={classNames(classes.title1, classes.bold)} style={{ textAlign: "center", margin: "50px 0px 60px 0px" }} > <Divider />  <div style={{ padding: "50px 0px 50px 0px", color: "grey" }}> {faq_content[i].section} </div> <Divider />   </div> : ""}
          <div className={classNames(classes.title1, classes.bold)}> {i + 1} - {faq_content[i].question} </div>
          {faq_content[i].answer}
        </Element>
      )
    }

    renderable.push(<div className={classNames(classes.margin_top_xl)}></div>)
    return renderable
  }


  render() {
    const { classes } = this.props;

    return (
      <div className="faq">
        <TopNavBar selected="faq" referrer={this.state.referrer} />


        <NextSeo config={Constants.next_seo_config("faq")} />

        <NoSsr>

          <div className={classNames(classes.align_center)}>
            <div className={classNames(classes.page_text_width, classes.page_padding, classes.break_word, classes.align_left)}>

              {/* <div style={{ marginTop: "140px" }}></div> */}


              {this.renderContent()}


            </div>



          </div>

        </NoSsr>

        <BotNavBar referrer={this.state.referrer} />

      </div>
    );
  }
}


export default withStyles(styles)(FAQ);