import React, { Component } from 'react';
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
// import { NoSsr, Card, CardMedia, CardContent, Button, Grid, TextField } from '@material-ui/core';
import { NoSsr, Button, Typography, TextField, FormControl, InputLabel, MenuItem, Select, Card, CardContent, FormControlLabel, Checkbox, Fab, Divider, IconButton, Grid, FormLabel, RadioGroup, Radio, Chip } from '@material-ui/core';

import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import TopNavBar from './components/TopNavBar'
import BotNavBar from './components/BotNavBar'
import Router from 'next/router';
import axios from 'axios'
import Head from 'next/head';


import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Constants from 'constants/constants';
import GlobalConstants from '../global/constants';

var validator = require("email-validator")   //Source: https://www.npmjs.com/package/email-validator

const styles = {
  page_width: Constants.style.page.width,
  page_padding: Constants.style.page.padding,

  align_center: Constants.style.align.center,

  bold: Constants.style.text.bold,
  break_word: Constants.style.text.break_word,

  title1: Constants.style.text.title1,
  body1: Constants.style.text.body1,

  margin_top_xl: Constants.style.margin_top.xl,
  margin_top_lg: Constants.style.margin_top.lg,
  margin_top_md: Constants.style.margin_top.md,
  margin_top_sm: Constants.style.margin_top.sm,
  margin_top_xs: Constants.style.margin_top.xs,
  margin_top_xxs: Constants.style.margin_top.xxs,
}

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginTab: 1,  // Default display tab for login/signup
      login_email: "",
      get_email_button_disabled: false,
      email_error: false,
      email_error_text: ""
    };
  }


  handleGetEmailLink = () => {
    const login_email = this.state.login_email
    if (!validator.validate(login_email)) {
      alert("Error - Invalid email")
      return
    }

    this.setState({ get_email_button_disabled: true }, () => {
      // Send to server
      axios.post("/api/auth/getForgetPasswordEmailLink", {
        login_email,
      }).then(res => {
        if (res.status === 200) {
          if (res.data.err) {
            alert(res.data.err)
            this.setState({ get_email_button_disabled: false })
          } else {
            alert("Please check your email for reset password instructions")
          }
        } else {
          alert(res.data)
          this.setState({ get_email_button_disabled: false })
        }
      }).catch(err => {
        alert(err)
        this.setState({ get_email_button_disabled: false })
      });

    })


  }

  handleKeyPress(target) {
    if (target.charCode == 13) {
      return this.handleLogin()
    }
  }


  handleFieldChange = (prop) => e => {

    switch (prop) {
      case "login_email":
        this.setState({
          email_error: false,
          email_error_text: "",
        })
        break;
      default:
        break;
    }
    this.setState({
      [prop]: e.target.value
    });
  }


  getMuiTheme = () => {
    return createMuiTheme({
      overrides: {
        MuiPaper: {
          elevation4: {
            boxShadow: "0px 1px 1px 0px rgba(0, 0, 0, 0.1), 0px 0px 0px 0px rgba(0, 0, 0, 0.1), 0px 0px 1px 0px rgba(0, 0, 0, 0.1)" //Reduce the perceived elevation
          },
          rounded: {
            borderRadius: "0"
          }
        },
      }
    });
  }


  renderContent = () => {

    return (
      <div>



        <div style={{ padding: "0px" }}>
          <div style={{ textAlign: "left" }}>
            <Typography variant="h6"> Forgot Password </Typography>
            <Typography variant="subtitle1" style={{ marginTop: "10px", lineHeight: 1.2 }}> A reset password link will be sent to you via your registered email </Typography>
          </div>


          <div style={{ marginTop: "20px", padding: "0px 0px 5px 0px" }}>
            <TextField
              label="Email"
              value={this.state.login_email}
              onChange={this.handleFieldChange('login_email')}
              onKeyPress={(ev) => { if (ev.key === 'Enter') { this.handleGetEmailLink() } }}
              fullWidth
            />
          </div>


          {/* <Button style={{ fontSize: "9px", padding: 0, marginTop: "5px" }}> Forgot Password</Button> */}
          <Button style={{ float: "right", margin: "10px 0px 0px 0px" }} disabled={this.state.get_email_button_disabled} onClick={() => { this.handleGetEmailLink() }}> Get Email Link </Button>

        </div>

      </div>
    )

  }


  componentDidMount = () => {
    Constants.store_referrer(this)
  }


  render() {
    const { classes } = this.props;

    return (
      <div className="forgot-password">
        <TopNavBar referrer={this.state.referrer} />


        <NextSeo config={Constants.next_seo_config("forgot-password")} />

        <NoSsr>

          <MuiThemeProvider theme={this.getMuiTheme()}>

            {/* <Grid container spacing={0} direction="column" align="center"> */}
            {/* <Grid container spacing={0} >

              <Grid item xs={0} sm={3} md={4} lg={4} style={{ marginTop: "50px" }} />

              <Grid xs={12} sm={6} md={4} lg={4} style={{ marginTop: "50px" }}> */}

            <div className={classNames(classes.align_center)}>
              <div className={classNames(classes.page_width, classes.page_padding, classes.break_word)}>
                <div style={{ marginTop: "100px" }}></div>

                <CardContent style={{ paddingBottom: "100px" }}>
                  {this.renderContent()}
                </CardContent>


                {/* </Grid>

              <Grid item xs={0} sm={3} md={4} lg={4} style={{ marginTop: "50px" }} />

            </Grid> */}


              </div>
            </div>


          </MuiThemeProvider>

        </NoSsr>


        <BotNavBar referrer={this.state.referrer} />

      </div>
    );
  }
}


export default withStyles(styles)(Login);