import React, { Component } from 'react';
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import { NoSsr } from '@material-ui/core';


import TopNavBar from './components/TopNavBar'
import BotNavBar from './components/BotNavBar'
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Constants from 'constants/constants';
import EmailJoanna from './components/constant_components/EmailJoanna'
import Head from 'next/head';

const styles = {
  page_width: Constants.style.page.width,
  page_text_width: Constants.style.page.text_width,

  page_padding: Constants.style.page.padding,

  align_center: Constants.style.align.center,
  align_left: Constants.style.align.left,

  bold: Constants.style.text.bold,
  break_word: Constants.style.text.break_word,

  title1: Constants.style.text.title1,
  body1: Constants.style.text.body1,

  margin_top_xl: Constants.style.margin_top.xl,
  margin_top_lg: Constants.style.margin_top.lg,
  margin_top_md: Constants.style.margin_top.md,
  margin_top_sm: Constants.style.margin_top.sm,
  margin_top_xs: Constants.style.margin_top.xs,
  margin_top_xxs: Constants.style.margin_top.xxs,
}

class GeneralTermsAndConditions extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }


  componentDidMount = () => {
    Constants.store_referrer(this)
  }


  render() {
    const { classes } = this.props;

    return (
      <div className="general-terms-and-conditions">
        <TopNavBar referrer={this.state.referrer} />

        <NextSeo config={Constants.next_seo_config("general-terms-and-conditions")} />

        <NoSsr>
          <div className={classNames(classes.align_center)}>
            <div className={classNames(classes.page_width, classes.page_padding, classes.break_word, classes.align_left)}>

              <div style={{ wordWrap: "break-word" }}>
                <div style={{ padding: '50px 0px 0px 0px', textAlign: "center" }}>
                  <img style={{ maxWidth: '200px', margin: '40px 0px 0px 0px', padding: '0px 0px 0px 0px' }} src={('/img/iFin-SG-Logo.png')} />
                  <div style={{ fontSize: '50px', lineHeight: '1', margin: '30px 0px 0px 0px', color: "#4cae4f", fontWeight: "bold" }}>GENERAL TERMS AND CONDITIONS</div>
                </div>


                <div style={{ padding: '0px 0px 50px 0px', lineHeight: '1.8' }}>
                  <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}>These pages, content and infrastructure, as well as any other corresponding service provided online are owned and operated by ICH iFinancial Singapore Private Limited, UEN: 201631146G (“the company” or “we” or “us”). </div>
                  <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> These terms and conditions apply to all our products and services made available online and to all Service Providers and End-Users making use of our services on or through any mobile application or Internet Website or any other electronic platform. By accessing, browsing and using our Website or any of our applications through whatever platform (hereafter collectively referred to as “the Website”), whether you are a Service Provider or an End-User thereby accept unconditionally, all terms and conditions on this Website including our <span style={{ fontWeight: "bold" }}>GENERAL TERMS AND CONDITIONS, TERMS OF USE, TERMS OF SERVICE, DISCLAIMER OF LIABILITY</span> and <span style={{ fontWeight: "bold" }}>PRIVACY POLICY</span>, as modified or amended from time to time and which constitute the Web User Agreement with us. The Company reserves the right to amend these terms and conditions at any time and such amendments take effect immediately upon the posting of such agreement as amended or modified on the Website.</div>
                  <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> The Company reserves the right to amend these terms and conditions at any time and such amendments take effect immediately upon the posting of such agreement as amended or modified on the Website. </div>

                  <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> <span style={{ fontWeight: "bold" }}> Contacting us</span></div>

                  <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px' }}> If you have any questions about our <span style={{ fontWeight: "bold" }}>GENERAL TERMS AND CONDITIONS, TERMS OF USE, TERMS OF SERVICE, DISCLAIMER OF LIABILITY</span> and <span style={{ fontWeight: "bold" }}>PRIVACY POLICY</span>, or the practices of this Website, or any comments and suggestions, please contact ICH iFinancial Singapore Private Limited, UEN: 201631146G at <EmailJoanna /> .</div>
                </div>
              </div>

            </div>
          </div>
        </NoSsr>
        <BotNavBar referrer={this.state.referrer} />

      </div>
    );
  }
}


export default withStyles(styles)(GeneralTermsAndConditions);