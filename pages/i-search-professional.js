import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NoSsr, Button, Divider, Grid, Paper, IconButton, Typography, Tooltip } from '@material-ui/core';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
// import Logo from './ifinsg_logo.png'
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import TopNavBar from './components/TopNavBar'
import BotNavBar from './components/BotNavBar'
import Router from 'next/router';
import Constants from 'constants/constants';
import ScrollToTop from './components/widgets/ScrollToTop';
import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";
import Head from 'next/head';
import Link from "next/link";
import ModalHomeFeatures from "./components/modals/ModalHomeFeatures";
// import Helpers from '../global/helpers'
import GlobalConstants from 'global/constants'      // This doe not work, but helpers works. wut???
import GlobalHelpers from 'global/helpers'      // This doe not work, but helpers works. wut???
import axios from 'axios'
import AliceCarousel from 'react-alice-carousel';        // https://www.npmjs.com/package/react-alice-carousel
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import ModalPolicyDetails from './components/modals/ModalPolicyDetails'


const agents = [
  {
    name: "Damien Sim",
    image: "damien_sim.jpg",
    tel: "97437992",
    email: "damiensim@pruadviser.com.sg",
    licenseType: "RNF",
    licenseCode: "SCL200216201",
    organisation: "Damien Sim & Associates - an agency unit of Prudential Singapore",
    insurersRepresented: "Prudential",
    languages: "English, Mandarin, Hokkien",
    connectionsIn: "Singapore",
    businessMotto: "To Serve my Best with Honesty & Sincerity. [COMP/AGY/218/03/17]"

  },

  {
    name: "Melvin Liu",
    image: "melvin_liu.jpg",
    tel: "97557988",
    email: "melvin.liu@manulife.com.sg",
    licenseType: "RNF",
    licenseCode: "LMY300122765",
    organisation: "Inspire GA - an agency unit representing Manulife (Singapore) Pte Ltd",
    insurersRepresented: "Manulife, Aviva, NTUC Income",
    languages: "English, Mandarin",
    connectionsIn: "Engineering, Education, Business Owners",
    businessMotto: "To create a Significant, Secure and Sustainable Future for my Clients and those that matter to them."

  },



  {
    name: "David Wong",
    image: "david_wong.jpg",
    tel: "93391466",
    email: "david.wong@promiseland.com.sg",
    licenseType: "RNF",
    licenseCode: "WCW300150250",
    organisation: "Promiseland Independent Pte Ltd",
    insurersRepresented: "Aviva, AIA, Tokio Marine, AXA, NTUC Income, China Life, LIC, Manulife, TransAmerica Life Bermuda, Raffles Health, Cigna, Sompo, Aetna, Liberty, Allied World",
    languages: "English, Mandarin",
    connectionsIn: "PMET, Engineering, IT",
    businessMotto: "Everyone should have the right to be free of financial worries and focus on happiness of life."
  },

  {
    name: "Gary Lim",
    image: "gary_lim.jpg",
    tel: "97612150",
    email: "garylim@pruadviser.com.sg",
    licenseType: "RNF",
    licenseCode: "LCA200203007",
    organisation: "Jeanette Pay & Associates - an agency unit of Prudential Singapore",
    insurersRepresented: "Prudential",
    languages: "English, Mandarin",
    connectionsIn: "Banking & Financial, Business Owners, Foreigners",
    businessMotto: "Providing Honest, Sincere and Professional Advice for all, friendly and approachable financial consultant. [COMP/AGY/463/04/17]"
  },


  {
    name: "John Lee",
    image: "john_lee.jpg",
    tel: "96658165",
    email: "johnleezh@rep-sg.greateasternlife.com",
    licenseType: "RNF",
    licenseCode: "LZH200095113",
    organisation: "Steven Foo & Associates",
    insurersRepresented: "Great Eastern",
    languages: "English, Mandarin, Malay",
    connectionsIn: "Dentistry, Banking, Christian Organisations",
    businessMotto: "Your financial doctor. Your wealth coach. Your ultimate resource network. I am here to help you become a better, wealthier and more empowered version of yourself. [ I can only accept enquiries from Singapore residents. ]"
  },


  {
    name: "Isaac Fang",
    image: "isaac_fang.jpg",
    tel: "97306977",
    email: "isaacfangcn@phillip.com.sg",
    licenseType: "RNF",
    licenseCode: "FCN200135730",
    organisation: "Phillip Securities Pte Ltd",
    insurersRepresented: "Aviva, AXA, NTUC Income, AIA, Manulife, Tokio Marine, LIC, China Life, Raffles Health, Transamerica, Allied World, Chubb, ECICS, EQ, Etiqa, Liberty, Aetna, QBE, MSIG, Sompo, First Capital",
    languages: "English, Mandarin",
    connectionsIn: "Medical, Senior Management, SMEs",
    businessMotto: "CFP, ChFC, CFA certifications holder. Delivering Multipurpose Financial Services Excellence through the Offerings Multitude of Phillip Securities Pte Ltd."
  },

  {
    name: "Roy Ong Kwang Yong",
    image: "roy_ong.jpg",
    tel: "97925632",
    email: "royoky@aia.com.sg",
    licenseType: "RNF",
    licenseCode: "OKY200205873",
    organisation: "SP-ROYKY - authorized representative of AIA Singapore Private Limited",
    insurersRepresented: "AIA",
    languages: "English, Mandarin",
    connectionsIn: "IT, Security, FnB",
    businessMotto: "Serve the Best to the Best. [CC/ET/047/17]"
  },

  {
    name: "Taferina Chua Pair Shen",
    image: "taferina_chua.jpg",
    tel: "96881667",
    email: "taferina@ippfa.com",
    licenseType: "RNF",
    licenseCode: "CPS100028499",
    organisation: "IPP Financial Advisers Pte Ltd",
    insurersRepresented: "AXA, Tokio Marine, NTUC Income, Aviva, Manulife, China Life, ICS, Zurich, Friends Provident, AIA",
    languages: "English, Mandarin",
    connectionsIn: "Statutory Boards, Teachers, Finance Professionals",
    businessMotto: "I help clients to achieve their lifetime financial success through proven processes, systems and financial planning methodology."
  },

  {
    name: "Tin Teck Siong",
    image: "tin_teck_siong.jpg",
    tel: "96506930",
    email: "tstin@aia.com.sg",
    licenseType: "RNF",
    licenseCode: "TTS200135274",
    organisation: "SP-TIN - authorized representative of AIA Singapore Private Limited",
    insurersRepresented: "AIA",
    languages: "English, Mandarin, Hokkien, Cantonese",
    connectionsIn: "Business Owners, Professionals, Mass Affluent Market",
    businessMotto: "Client Before Self. [CC-EW-056-17]"
  },

  {
    name: "Jacqueline Aw",
    image: "jacqueline_aw.jpg",
    tel: "96817252",
    email: "jacqueline.aw@manulife.com.sg",
    licenseType: "RNF",
    licenseCode: "ABH200040742 / GIA - C004561",
    organisation: "Manulife (Singapore) Pte Ltd",
    insurersRepresented: "Manulife, Aviva, NTUC Income, Allied World, AIG, MSIG",
    languages: "English, Chinese, Cantonese, Hokkien",
    connectionsIn: "Families, Business Owners, Expatriates",
    businessMotto: "Provide flexible and comprehensive solutions in meeting clients needs."
  },
  {
    name: "Ng Ray Seen",
    image: "ng_ray_seen.jpg",
    tel: "87990515",
    email: "seentheray@aia.com.sg",
    licenseType: "RNF",
    licenseCode: "NRS300300384",
    organisation: "AIA Singapore Private Limited",
    insurersRepresented: "AIA",
    languages: "English, Mandarin",
    connectionsIn: "Business owners",
    businessMotto: "The best, for the best. [CC-EW-057-17]"
  }

]




let homepageVariant = GlobalHelpers.generateRandomInteger(5) // 0-5

switch (homepageVariant) {
  case 0:
    homepageVariant = 19
    break
  case 1:
    homepageVariant = 24
    break
  case 2:
    homepageVariant = 25
    break
  case 3:
    homepageVariant = 28
    break
  case 4:
    homepageVariant = 29
    break
  default:
    break
}


homepageVariant = 25

const styles = {
  page_width: Constants.style.page.width,
  page_text_width: Constants.style.page.text_width,
  page_padding: Constants.style.page.padding,

  align_center: Constants.style.align.center,

  bold: Constants.style.text.bold,
  break_word: Constants.style.text.break_word,

  title1: Constants.style.text.title1,
  body1: Constants.style.text.body1,

  margin_top_xl: Constants.style.margin_top.xl,
  margin_top_lg: Constants.style.margin_top.lg,
  margin_top_md: Constants.style.margin_top.md,
  margin_top_sm: Constants.style.margin_top.sm,
  margin_top_xs: Constants.style.margin_top.xs,
  margin_top_xxs: Constants.style.margin_top.xxs,
}


// const styles = JSON.parse(JSON.stringify(Constants.style))
let referrer = ""


class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      policy_details_modal_open: false,
      policy_details_modal_policy: ""
    };
  }


  componentDidMount = () => {
    Constants.store_referrer(this)
  }


  handlePolicyDetailsClicked = (policy) => {
    this.setState({
      policy_details_modal_open: true,
      policy_details_modal_policy: policy
    })
  }


  renderAgents = () => {
    let renderable = []
    agents.forEach(agent => {
      const agentRenderable =
        <div style={{ marginTop: "20px" }}>
          <Grid container spacing={0} alignItems='stretch' style={{ border: "1px solid rgba(128, 128, 128, 0.52)", padding: "10px" }} >

            <Grid item xs={12} sm={3} alignItems='stretch' style={{ padding: '0px 8px 20px 8px' }}>
              <img style={{ maxWidth: '200px', width: "100%", margin: '0px 0px 0px 0px', padding: '0px 0px 0px 0px' }} src={(`/img/i-search-professional/${agent.image}`)} ></img>
            </Grid>


            <Grid item xs={12} sm={9} alignItems='stretch' style={{ padding: '0px 8px 20px 8px', textAlign: "left", fontSize: 14, lineHeight: 1.2 }}>
              <div style={{ fontSize: 20 }}><b>{agent.name}</b></div>
              <div style={{ marginTop: "5px" }} > <a href={`tel:${agent.tel}`} >{agent.tel}</a></div>
              <div style={{ marginTop: "3px" }} > <a href={`mailto:${agent.email}`} >{agent.email}</a></div>

              <div style={{ marginTop: "5px" }}><b>License Code Number:</b><br />{agent.licenseCode}</div>
              <div style={{ marginTop: "5px" }}><b>Organization/Employer:</b><br />{agent.organisation}</div>
              <div style={{ marginTop: "5px" }}><b>Insurers Represented:</b><br />{agent.insurersRepresented}</div>
              <div style={{ marginTop: "5px" }}><b>Languages Spoken:</b><br />{agent.languages}</div>
              <div style={{ marginTop: "5px" }}><b>I have customer Connections in:</b><br />{agent.connectionsIn}</div>
              <div style={{ marginTop: "5px" }}><b>My Business Motto:</b><br />{agent.businessMotto}</div>

            </Grid>

          </Grid>


        </div>
      renderable.push(agentRenderable)
    })

    return renderable
  }


  renderProducts = () => {
    let returnable = []
    let numberOfProducts = Constants.products_on_sale.length

    for (let i = 0; i < numberOfProducts; i++) {

      let product = Constants.products_on_sale[i]
      let cycle_length = Constants.product_details[product.policy].cycle_length
      // Constants.products_on_sale.forEach(product => {
      returnable.push(

        <Grid item xs={6} md={4} lg={3} alignItems='stretch' style={{ padding: '2px 3px 2px 3px', maxWidth: "220px", display: "inline-block" }}>
          <Paper style={{ padding: '10px 0px 0px 0px', margin: '0px 0px 15px 0px', height: '100%', textAlign: 'center', position: "relative", paddingBottom: "10px" }}>
            <div style={{ margin: '10px 3px 0px 3px', fontSize: '19px', fontWeight: 'bold', lineHeight: '1', color: '#7cda24', minHeight: "40px" }}>  Cyclic: {name_display(product.policy)}</div>
            <img style={{ maxWidth: '150px', width: '100%', margin: '0px 0px 0px 0px', padding: '0px 0px 0px 0px' }} src={('/img/' + product.img)} />
            {/* <div style={{ margin: '10px 10px 0px 10px', fontSize: '16px', lineHeight: '1' }}>$20.40 Premium + $9 Admin Fee </div>
    <div style={{ margin: '10px 10px 0px 10px', fontSize: '16px', lineHeight: '1', color: '#e09900', fontWeight: 'bold' }}>= Just $29.40 per 3 months </div> */}

            <table style={{ marginLeft: "auto", marginRight: "auto", width: isBrowser ? "90%" : "100%" }}>
              <tbody style={{ textAlign: "center", width: "100%" }}>
                {/* <tr > <td colspan="2" style={{ textAlign: "center", paddingTop: "0px", fontSize: "16px", fontWeight:"bold" }}>Per 3 Months</td> </tr> */}
                <tr> <td style={{ width: "55%", textAlign: "left", paddingLeft: "5px" }}>Premium</td> <td style={{ textAlign: "right", paddingRight: "5px" }}>{price_display(product.policy, "premium", cycle_length, true)} </td> </tr>
                <tr style={{ height: "5px", padding: 0, margin: 0 }}> <td colspan="2" style={{ padding: "0px", margin: "0px", textAlign: "right", padding: "0px 5px 0px 1px", fontSize: "13px", transform: "translateY(-5px)", color: Constants.color.gold }}> ({price_display(product.policy, "premium", 1)} per month) </td> </tr>

                <tr> <td style={{ width: "55%", textAlign: "left", paddingLeft: "5px" }}>Admin Fee</td> <td style={{ textAlign: "right", paddingRight: "5px" }}>{price_display(product.policy, "admin_fee", cycle_length, true)}</td> </tr>
                <tr style={{ height: "5px", padding: 0, margin: 0 }}> <td colspan="2" style={{ padding: "0px", margin: "0px", textAlign: "right", padding: "0px 5px 0px 1px", fontSize: "13px", transform: "translateY(-5px)", color: Constants.color.gold }}> ({price_display(product.policy, "admin_fee", 1)} per month) </td> </tr>
                {/* <tr style={{ height: "5px", padding: 0, margin: 0 }}> <td colspan="2" style={{ padding: "0px", margin: "0px", textAlign: "right", padding: "0px 5px 0px 1px", fontSize: "13px", transform: "translateY(-8px)", color: Constants.color.darkgreen }}><b>40% discount!</b></td> </tr> */}

                <tr> <td style={{ width: "55%", textAlign: "left", paddingLeft: "5px", fontWeight: "bold", color: Constants.color.darkgreen }}>Total</td> <td style={{ textAlign: "right", paddingRight: "5px", fontWeight: "bold", color: Constants.color.darkgreen }}>{price_display(product.policy, "total", cycle_length, true)}</td> </tr>
                <tr > <td colspan="2" style={{ textAlign: "right", padding: "5px 5px 1px 1px", fontSize: "13px", fontWeight: "bold", transform: "translateY(-6px)", color: Constants.color.darkgreen, lineHeight: 1.1 }}>per Cycle of <span style={{ fontStyle: "italic", color: cycle_length == 6 ? "rgb(50, 67, 183)" : "rgb(134, 149, 253)" }}>{cycle_length} months</span></td> </tr>
                <tr style={{ height: "5px", padding: 0, margin: 0 }}> <td colspan="2" style={{ padding: "0px", margin: "0px", textAlign: "right", padding: "0px 5px 0px 1px", fontSize: "13px", transform: "translateY(-5px)", color: Constants.color.gold }}> ({price_display(product.policy, "total", 1, true)} per month) </td> </tr>


                {/* text-shadow: 0 0 1px #fff, 0 0 2px #fff, 0 0 2px #00ff39, 0 0 4px #00ff39, 0 0 5px #00ff39, 0 0 2px #00ff39, 0 0 2px #00ff39;                 */}
              </tbody>
            </table>

            <div style={{ textAlign: "center", width: "100%", marginTop: "10px" }}>
              <Button style={{ fontSize: '10px', padding: "5px 0px 5px 0px", backgroundColor: "blue", opacity: 0.7, color: 'white', lineHeight: 1.1 }} onClick={() => { this.handlePolicyDetailsClicked(product.policy) }}> Policy<br />Details</Button>

              <Link href={{ pathname: Constants.get_new_page("signup", this.state.referrer), query: { products: `["${product.policy}"]` } }}>

                <Button
                  onClick={() => {
                    axios.post("/api/trackingData", {
                      tracking_data: {
                        homepage_variant: homepageVariant,
                        title: title[homepageVariant],
                        button: "get_insured"
                      }
                    }).catch(err => {
                    });
                  }}
                  variant="contained"
                  style={{ fontSize: '13px', margin: "10px 0px 10px 0px", backgroundColor: Constants.color.green, color: 'white', height: "31px" }}>
                  Purchase
                     </Button>
              </Link>


              {/* <Button style={{ fontSize: '10px', padding: "5px 0px 5px 0px", backgroundColor: "blue", opacity: 0.7, color: 'white', lineHeight: 1.1 }} onClick={() => { this.handlePolicyDetailsClicked(product.policy) }}> Policy<br />Details</Button>
           <Button style={{ fontSize: '13px', padding: "5px 10px 5px 10px", backgroundColor: '#7cda24', color: 'white' }} onClick={() => { this.handleAddToCart(product.policy) }}>
             Add to Cart
 </Button> */}
              {/* </div> */}

            </div>


            {product.availability !== "available" ? <p style={{
              position: "absolute", bottom: 0, fontSize: "24px", background: "rgba(251,251,251,0.6)",
              height: "100%", width: "100%", textAlign: "center", display: "flex", margin: 0
            }}>
              <Typography style={{ fontSize: "26px", color: "darkblue", opacity: 0.4, flex: "auto", alignSelf: "center", fontWeight: "bold", transform: "rotate(-45deg)" }}> Coming soon </Typography>
            </p>
              :
              ""
            }

          </Paper>
        </Grid>
      )
    }

    if (true) {
      return (
        <Grid container spacing={0} alignItems='stretch' style={{ display: "flex", justifyContent: "center" }} >
          {returnable}
        </Grid>

      )
    } else {
      return (
        <div style={{ padding: "0px 20px 0px 20px" }}>
          {returnable}
        </div>
      )
    }

  }


  renderContent = () => {
    const { classes } = this.props;

    return (
      // <Grid container spacing={0} direction="column" align="center" style={{}}>
      //   <Grid item xs={12} sm={10} md={9} lg={8} style={{ marginTop: "50px", textAlign: 'center', wordWrap: "break-word", padding: "0px 40px 0px 40px", color: Constants.color.grey }}>

      <div className={classNames(classes.align_center)}>
        <div className={classNames(classes.page_text_width, classes.page_padding, classes.break_word)}>

          <div style={{ marginTop: "100px" }}></div>

          <div style={{ fontSize: '35px', lineHeight: '1', margin: '0px 0px 0px 0px', fontWeight: "bold" }}>iSearchProfessional</div>


          <div>

            {this.renderAgents()}

          </div>


        </div >

      </div >
      //   </Grid>
      // </Grid >
    )
  }


  render() {
    return (
      <div>
        <NoSsr>
          <TopNavBar selected="i-search-professional" referrer={this.state.referrer} />
        </NoSsr>

        <NextSeo config={Constants.next_seo_config("home")}></NextSeo>

        <NoSsr>
          <ModalPolicyDetails
            page={"products"}
            open={this.state.policy_details_modal_open}
            handleClose={() => { console.log("handleCLose activated"); this.setState({ policy_details_modal_open: false }) }}
            policy={this.state.policy_details_modal_policy}
            referrer={this.state.referrer}
          />

          {this.renderContent()}
        </NoSsr>

        <BotNavBar page='home' referrer={this.state.referrer} />


      </div>
    );
  }
}



export default withStyles(styles)(Home);