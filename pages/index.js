import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NoSsr, Button, Divider, Grid, Paper, IconButton, Typography, Tooltip } from '@material-ui/core';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
// import Logo from './ifinsg_logo.png'
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import TopNavBar from './components/TopNavBar';
import BotNavBar from './components/BotNavBar';
import Router from 'next/router';
import Constants from 'constants/constants';
import ScrollToTop from './components/widgets/ScrollToTop';
import { BrowserView, MobileView, isBrowser, isMobile } from 'react-device-detect';
import Head from 'next/head';
import Link from 'next/link';
import ModalHomeFeatures from './components/modals/ModalHomeFeatures';
// import Helpers from '../global/helpers'
import GlobalConstants from '../global/constants'; // This doe not work, but helpers works. wut???
import GlobalHelpers from '../global/helpers'; // This doe not work, but helpers works. wut???
import axios from 'axios';
import AliceCarousel from 'react-alice-carousel'; // https://www.npmjs.com/package/react-alice-carousel
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import ModalPolicyDetails from './components/modals/ModalPolicyDetails';

import HomePageProduct from '../components/product/HomePageProduct';

import { createMuiTheme, MuiThemeProvider, makeStyles } from '@material-ui/core/styles';
import HomeReferrerCarousel from 'components/carousel/HomeReferrerCarousel';
import Features from 'components/Features';
import Intro from 'components/intro';
import TopSection from 'components/home/topSection';
import TopSection2 from 'components/home/topSection2';
import styled from 'styled-components';

import './index.css';

const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#569818',
            main: '#7cda23',
            dark: '#96e14f',
            contrastText: '#fff'
        },
        secondary: {
            light: '#6f7fff',
            main: '#4c5fff',
            dark: '#3542b2',
            contrastText: '#fff'
        }
    }
});

const name_display = (policy) => {
    return Constants.product_details[policy].name;
};

const price_display = (policy, price_component, period, line_break) => {
    let price_original = -1;
    let price_promotion = -1;
    let on_promotion = false;

    switch (price_component) {
        case 'premium':
            price_original = Constants.product_details[policy].premium;
            if (Constants.product_details[policy].premium_on_promotion) {
                price_promotion = Constants.product_details[policy].premium_promotional_price;
                on_promotion = true;
            }
            break;
        case 'admin_fee':
            price_original = Constants.product_details[policy].admin_fee;
            if (Constants.product_details[policy].admin_fee_on_promotion) {
                price_promotion = Constants.product_details[policy].admin_fee_promotional_price;
                on_promotion = true;
            }
            break;
        case 'total':
            price_original = Constants.product_details[policy].total;
            if (Constants.product_details[policy].total_on_promotion) {
                price_promotion = Constants.product_details[policy].total_promotional_price;
                on_promotion = true;
            }
            break;
        default:
            break;
    }

    if (on_promotion) {
        // console.log(`PROMOTION PRICE - `price_component + " - price_promotion - " + price_promotion)
        return (
            <span>
                {/* <span style={{ textDecoration: "line-through" }}>${(period * price_original).toFixed(2)}</span> {line_break ? <br></br> : ""} */}
                <span style={{ color: Constants.color.green, fontWeight: 'bold' }}>
                    {' '}
                    <i>${(period * price_promotion).toFixed(2)}</i>{' '}
                </span>
            </span>
        );
    } else {
        // console.log(`NORMAL PRICE - ` + price_component + " - price_original - " + price_original)
        return <span>${(period * price_original).toFixed(2)} </span>;
    }
};

const title = {
    0: 'Insurance which makes no Profit off your premiums',
    1: 'Systematic help at the lowest cost.',
    2: 'Is Insurance actually cheaper than we thought?',
    3: 'Because we want you to have Enough money in good times and bad times.',
    4: 'Insurance redesigned for fairness.',
    5: 'Premiums - Claims = PremiumBack.',
    6: 'We do not Profit from your Premiums.',
    7: 'We do not Profit from your insurance Premiums.',
    8: 'We make no Profit off your insurance Premiums.',
    9: 'Insurance which does not make Profit off your premiums.',
    10: 'The Insurance system which does not make Profit off your premiums.',
    11: 'The Insurance system which makes no Profit off your premiums.',
    12: 'Insurance which makes no Profit off your premiums.',
    13: 'A flat fee to run an Insurance system without Profit.',
    14: 'Insurance Premiums collected only to pay out Claims.',
    15: 'A system where total Claims = total insurance Premiums.',
    16: 'Cyclic insurance only collects premiums as a pool in order to pay out claims.',
    17: 'Insurance which collects Premiums just to pay out Claims.',
    18: 'Insurance redesigned.',
    19: 'Because Insurance should be created Differently.',
    20: 'Should Insurance remain like what it is today?',
    21: 'Can insurance be cheaper?',
    22: 'We return excess premiums back to you.',
    23: 'No Claims? Get your PremiumBack!',
    24: 'The same protection, done Different.',
    25: 'The same protection, done Differently.',
    26: 'Simple protection for frugal people.',
    27: 'Simple protection for thrifty people.',
    28: 'Because Insurance should be done Differently.',
    29: 'Insurance the way you want it.'
};

let homepageVariant = GlobalHelpers.generateRandomInteger(5); // 0-5

switch (homepageVariant) {
    case 0:
        homepageVariant = 19;
        break;
    case 1:
        homepageVariant = 24;
        break;
    case 2:
        homepageVariant = 25;
        break;
    case 3:
        homepageVariant = 28;
        break;
    case 4:
        homepageVariant = 29;
        break;
    default:
        break;
}

homepageVariant = 25;

// console.log(homepageVariant)

const styles = {
    page_width: Constants.style.page.width,
    page_text_width: Constants.style.page.text_width,
    page_padding: Constants.style.page.padding,

    align_center: Constants.style.align.center,

    bold: Constants.style.text.bold,
    break_word: Constants.style.text.break_word,

    title1: Constants.style.text.title1,
    body1: Constants.style.text.body1,

    margin_top_xl: Constants.style.margin_top.xl,
    margin_top_lg: Constants.style.margin_top.lg,
    margin_top_md: Constants.style.margin_top.md,
    margin_top_sm: Constants.style.margin_top.sm,
    margin_top_xs: Constants.style.margin_top.xs,
    margin_top_xxs: Constants.style.margin_top.xxs
};

let referrer = '';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            policy_details_modal_open: false,
            policy_details_modal_policy: ''
        };
    }

    componentDidMount() {
        Constants.store_referrer(this);

        const trackDom = document.getElementById('trackId');
        trackDom &&
            trackDom.addEventListener('click', function(node) {
                let count = 5;
                function findTrackId(target) {
                    if (target && target.dataset && target.dataset.trackId) {
                        return target.dataset.trackId;
                    } else {
                        if (count > 0) {
                            count--;
                            return findTrackId(target.parentNode);
                        } else {
                            return null;
                        }
                    }
                }
                const trackId = findTrackId(node.target);
                if (trackId) {
                    console.log('trackid ---> ', trackId);
                    axios
                        .post('/api/trackingData', {
                            tracking_data: {
                                homepage_variant: homepageVariant,
                                title: title[homepageVariant],
                                button: trackId
                            }
                        })
                        .catch((err) => {
                            console.log(err);
                        });
                }
            });
    }

    render() {
        const { classes } = this.props;
        console.log('Home -> render -> isMobile', isMobile);

        return (
            <div id="trackId" className="index-page">
                <MuiThemeProvider theme={theme}>
                    {/* <NoSsr> */}
                    <TopNavBar
                        selected="home"
                        referrer={this.state.referrer}
                        homepageVariant={homepageVariant}
                        title={''}
                    />
                    {/* </NoSsr> */}
                    <NextSeo config={Constants.next_seo_config('home')}></NextSeo>
                    <Head>
                        <link href="/slick.css" rel="stylesheet" />
                        <link href="/slick-theme.css" rel="stylesheet" />
                    </Head>
                    <Wrapper>
                        <section className="top_section_1">
                            <TopSection />
                        </section>
                        <NoSsr>
                            <section
                                className="top_section_1"
                                style={{
                                    marginTop: isMobile ? 50 : 100,
                                    padding: '0 20px',
                                    height: isMobile ? '210px' : '428px'
                                }}
                            >
                                <iframe
                                    width="100%"
                                    height="100%"
                                    src="https://www.youtube.com/embed/GWaJ7vaBOY8?rel=0&modestbranding=1&autohide=1&showinfo=0&fs=0&nologo=1&modestbranding=1"
                                    frameBorder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowFullScreen
                                ></iframe>
                            </section>
                        </NoSsr>
                        <section className="top_section_2">
                            <Divider style={{ margin: '80px 20px' }} />
                            <TopSection2 />
                        </section>
                        <section className="carousel">
                            <Divider style={{ margin: '80px 20px' }} />
                            <HomeReferrerCarousel />
                        </section>

                        <section className="intro">
                            <Divider style={{ margin: '80px 20px' }} />
                            <Intro />
                        </section>
                        <section className="product_summary">
                            <Divider style={{ margin: '80px 20px' }} />
                            <div className="home_products_title"> What you can Sign Up for </div>
                            <div className="home_products">
                                <HomePageProduct isHome={true} />
                            </div>
                        </section>

                        <section className="features">
                            <Divider style={{ margin: '100px' }} />
                            <Features />
                        </section>
                    </Wrapper>
                    <BotNavBar page="home" referrer={this.state.referrer} />
                </MuiThemeProvider>
            </div>
        );
    }
}

export default withStyles(styles)(Home);

export const Wrapper = styled.div`
    max-width: 800px;
    margin: 0 auto;
    padding-top: 130px;

    .top_section_1 {
    }

    .top_section_2 {
    }

    .carousel {
    }

    .intro {
    }

    .product_summary {
        .home_products_title {
            font-family: Merriweather;
            font-size: 42px;
            font-weight: 400;
            line-height: 1.3;
            margin: 50px 0 30px 0;
            padding: 0;
            text-align: center;
            color: #4a4a4a;
        }
        .home_products {
            margin: 40px 0 0 0;
        }
    }

    .features {
    }
`;
