import React, { Component } from 'react';
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
// import { NoSsr, Card, CardMedia, CardContent, Button, Grid, TextField } from '@material-ui/core';
import { NoSsr, Button, Typography, AppBar, Tabs, Tab, TextField, autoComplete, FormControl, InputLabel, MenuItem, Select, Card, CardContent, FormControlLabel, Checkbox, Fab, Divider, IconButton, Grid, FormLabel, RadioGroup, Radio, Chip } from '@material-ui/core';

import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import TopNavBar from './components/TopNavBar'
import BotNavBar from './components/BotNavBar'
import Router from 'next/router';
import axios from 'axios'
import SwipeableViews from 'react-swipeable-views';
import Head from 'next/head';

import GlobalConstants from '../global/constants';
import Constants from 'constants/constants';
import ComponentLogin from './components/ComponentLogin';
import Cookies from 'universal-cookie';
const cookie = new Cookies();
var validator = require("email-validator")   //Source: https://www.npmjs.com/package/email-validator

var owasp = require('owasp-password-strength-test');

owasp.config({
  allowPassphrases: false,
  maxLength: 50,
  minLength: 6,
  minPhraseLength: 20,
  minOptionalTestsToPass: 4,
});

const do_not_validate = false

let coming_soon = false

if (GlobalConstants.staging) {
  coming_soon = false
} else {
  coming_soon = true
}

const allow_signups_as_well = false
// const allow_signups_as_well = true


// const do_not_login_via_token = false


function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}


class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginTab: 1,  // Default display tab for login/signup


      login_email: "",
      login_password: "",
      // login_email: "",
      signup_password: "",
      signup_password_retype: "",

      email_error: false,
      email_error_text: ""
    };
  }




  handleVerifyEmail = () => {
    console.log('handleVerifyEmail()')
    this.setState({
      verify_email_button_disabled: true
    }, () => {
      // Verify the verification code
      axios.post("/api/auth/verifyEmail", {
        signup_username: this.state.login_email,
        email_verification_code: this.state.email_verification_code
      }).then(res => {
        if (res.status === 200) {
          if (res.data.err) {
            alert(res.data.err)     // Eg: ERROR - Email already in use
            this.setState({ verify_email_button_disabled: false })
          } else {
            // console.log(res.data)
            if (res.data.verification_result) {
              console.log("Verification OK")
              if (res.data.token) {
                cookie.set('ifinsg_token', res.data.token, { path: '/' })
                localStorage.setItem('ifinsg_token', res.data.token)
                Router.push(Constants.get_new_page("profile", this.state.referrer))
              } else {
                console.log("Server error - Unable to get token")
                this.setState({ verify_email_button_disabled: false })
              }

            } else {
              console.log("Unable to get token from server")
              this.setState({ verify_email_button_disabled: false })

            }
          }
        } else {
          console.log("ERROR - ", res)
          this.setState({ verify_email_button_disabled: false })
        }
      }).catch(err => {
        console.log(err);
      });
    })

  }


  renderSignupFields = () => {
    let renderable = []

    const login_email =
      <div style={{ padding: "0px 0px 5px 0px" }}>
        <TextField
          name="email"
          label="email"
          type="email"
          id="email"
          autoComplete="email"
          autoFocus
          value={this.state.login_email}
          onChange={this.handleFieldChange('login_email')}
          fullWidth
        // InputProps={{
        //   endAdornment: <InputAdornment>@email.com</InputAdornment>,
        // }}
        />
      </div>

    const signup_password =
      <div style={{ padding: "0px 0px 2px 0px" }}>
        <TextField
          name="password"
          label="Password"
          type="password"
          id="password"
          autoComplete="password"
          autoFocus
          value={this.state.signup_password}
          onChange={this.handleFieldChange('signup_password')}
          fullWidth
        />
      </div>

    const signup_password_retype =
      <div style={{ padding: "0px 0px 2px 0px" }}>
        <TextField
          label="Re-type your password"
          onKeyPress={(ev) => { if (ev.key === 'Enter') { this.handleSignup() } }}
          value={this.state.signup_password_retype}
          onChange={this.handleFieldChange('signup_password_retype')}
          type="password"
          fullWidth
        />
      </div>

    const signup_button =
      <Button style={{ float: "right", margin: "10px 0px 0px 0px" }} disabled={this.state.signup_button_disabled} onClick={() => { this.handleSignup() }}> Signup </Button>



    if (!this.state.show_email_verification_field) {
      renderable = [login_email, signup_password, signup_password_retype, signup_button]
    }

    return (renderable)

  }

  renderEmailVerificationFields = () => {
    let renderable = []

    const instructions =
      <Typography style={{ padding: "20px 0px 2px 0px" }}> Please enter email verification code sent to {this.state.login_email}: </Typography>

    const email_verification_field =
      <div style={{ padding: "20px 0px 2px 0px" }}>
        <TextField
          label="Email verification code"
          onKeyPress={(ev) => { if (ev.key === 'Enter') { this.handleVerifyEmail() } }}
          value={this.state.email_verification_code}
          onChange={this.handleFieldChange('email_verification_code')}
          type="number"
          fullWidth
        />
      </div>

    const verify_email_button =
      <Button style={{ float: "right", margin: "10px 0px 0px 0px" }} disabled={this.state.verify_email_button_disabled} onClick={() => { this.handleVerifyEmail() }}> Verify </Button>


    const resend_verification_code_button =
      <Button style={{ float: "left", margin: "10px 0px 0px 0px", fontSize: "12px", padding: 0 }} disabled={this.state.resend_verification_code_button_disabled} onClick={() => { this.handleResendVerificationCode() }}> Resend </Button>


    if (this.state.show_email_verification_field) {
      renderable = [instructions, email_verification_field, verify_email_button]
      // renderable = [email_verification_field, resend_verification_code_button, verify_email_button]
    }

    return (
      renderable
    )
  }

  handleSignup = () => {
    console.log("handleSignup")

    // Link with DB to actually signup
    const login_email = this.state.login_email
    const signup_password = this.state.signup_password
    const signup_password_retype = this.state.signup_password_retype

    // const credentials = { login_email, signup_password, signup_password_retype }
    // console.log(credentials)

    // Validate fields
    if (!do_not_validate) {
      if (!validator.validate(login_email)) {
        alert("Error - Invalid email")
        return
      } else if (signup_password === "") {
        alert("Error - Please enter a password")
        return
      } else if (signup_password_retype === "") {
        alert("Error - Please re-enter your password")
        return
      }

      const password_strength = owasp.test(signup_password);
      console.log(password_strength)

      if (password_strength.requiredTestErrors.length > 0) {
        alert(password_strength.requiredTestErrors[0])
        return
      } else if (password_strength.optionalTestErrors.length > 2) {
        alert(password_strength.optionalTestErrors[0])
        return
      }

      if (signup_password !== signup_password_retype) {
        alert("Error - Passwords do not match")
        return
      }


      this.setState({
        signup_button_disabled: true
      }, () => {
        // Send to server
        axios.post("/api/auth/signup", {
          login_email,
          signup_password
        }).then(res => {
          if (res.status === 200) {
            if (res.data.err) {
              alert(res.data.err)     // Eg: ERROR - Email already in use
            } else {
              // console.log(res.data)

              this.setState({
                show_email_verification_field: true
              })

              // Let the resend_verification_code_button be active after 15 seconds
              setTimeout(() => {
                this.setState({
                  resend_verification_code_button_disabled: false
                })
              }, 10000)
            }
          } else {
            console.log("ERROR - ", res)
          }
        }).catch(err => {
          console.log(err);
        });

      })




    } else {
      this.nextPanelAfterSignup()
    }

    // Disable Login/Signup expansion panel
  }

  handleLogin = () => {
    console.log("handleLogin")


    console.log("handleLogin()");

    const login_email = this.state.login_email
    const login_password = this.state.login_password


    // const credentials = { login_email, login_password }
    // console.log(credentials)


    if (!validator.validate(login_email)) {
      alert("Error - Invalid email")
      return
    }


    // Server login Link with DB to actually login
    this.setState({ login_button_disabled: true }, () => {
      // Send to server
      axios.post("/api/auth/login", {
        login_email,
        login_password
      }).then(res => {
        if (res.status === 200) {
          console.log("POST code 200 OK")
          if (res.data.err) {
            alert(res.data.err)
            this.setState({ login_button_disabled: false })
          } else {

            try {
              // console.log(res.data)
              cookie.set('ifinsg_token', res.data.token, { path: '/' })
              localStorage.setItem('ifinsg_token', res.data.token)
              Router.push(Constants.get_new_page("profile", this.state.referrer))

            } catch (err) {
              console.log(err)
              console.log("Unexpected params received from api/auth/login")
              this.setState({ login_button_disabled: false })
            }
          }
        } else {
          console.log("ERROR - ", res)
          this.setState({ login_button_disabled: false })
        }
      }).catch(err => {
        console.log(err);
        this.setState({ login_button_disabled: false })
      });

    })


  }







  handleKeyPress(target) {
    if (target.charCode == 13) {
      return this.handleLogin()
    }
  }


  handleLoginTabChange = (event, value) => {
    // console.log("handleLoginTabChange")
    this.setState({ loginTab: value });
  };


  handleFieldChange = (prop) => e => {

    switch (prop) {
      case "email":
        this.setState({
          email_error: false,
          email_error_text: "",
        })
        break;
      default:
        break;
    }
    this.setState({
      [prop]: e.target.value
    });
  }

  handleChangePassword = () => {
    // window.location = "http://www.ifinsg.com"
  }

  getMuiTheme = () => {
    return createMuiTheme({
      overrides: {
        MuiPaper: {
          elevation4: {
            boxShadow: "0px 1px 1px 0px rgba(0, 0, 0, 0.1), 0px 0px 0px 0px rgba(0, 0, 0, 0.1), 0px 0px 1px 0px rgba(0, 0, 0, 0.1)" //Reduce the perceived elevation
          },
          rounded: {
            borderRadius: "0"
          }
        },
      }
    });
  }

  renderLoginFields = () => {
    return (
      <div>

        <Typography variant="h6"> Login </Typography>
        <ComponentLogin referrer={this.state.referrer} onLogin={() => { Router.push(Constants.get_new_page("profile", this.state.referrer)) }} />
        <Typography style={{ color: "grey", margin: "30px 0px 10px 0px" }} >If you are a new user, please make a <a href={Constants.get_new_page("products", this.state.referrer)}><u>Purchase</u></a> to create a new account. </Typography>

      </div>


    )
  }


  renderContent = () => {

    // if (coming_soon) {
    if (false) {

      return (
        <div style={{ textAlign: "center", paddingTop: "50px" }}>
          <Typography variant="h6"> Coming soon! </Typography>
        </div>

      )
    } else {

      if (allow_signups_as_well) {
        return (
          <div>
            <div style={{ marginTop: "80px" }}></div>
            <AppBar position="static" color="default" style={{ backgroundColor: "white", }}>
              <Tabs
                value={this.state.loginTab}
                onChange={this.handleLoginTabChange}
                indicatorColor="primary"
                textColor="primary"
                variant="fullWidth"
                style={{ backgroundColor: "white" }}
              >
                <Tab label="New User" />
                <Tab label="Existing User" />

              </Tabs>
            </AppBar>
            <SwipeableViews
              axis={'x'}  // 'x-reverse'
              index={this.state.loginTab}
              onChangeIndex={this.handleLoginTabChangeIndex}
            >

              <TabContainer >

                {this.renderSignupFields()}

                {this.renderEmailVerificationFields()}

              </TabContainer>


              <TabContainer >

                <div>
                  {this.renderLoginFields()}
                </div>


              </TabContainer>

            </SwipeableViews>
          </div>
        )
      } else {
        return (
          <div style={{ margin: "50px 30px 50px 30px" }}>
            {this.renderLoginFields()}
          </div>

        )
      }
    }
  }



  componentDidMount() {
    Constants.store_referrer(this)

    const trackDom = document.getElementById('trackId');
    trackDom && trackDom.addEventListener('click', function (node) {
      let count = 5;
      function findTrackId(target) {
        if (target && target.dataset && target.dataset.trackId) {
          return target.dataset.trackId
        } else {
          if (count > 0) {
            count--;
            return findTrackId(target.parentNode)
          } else {
            return null;
          }
        }
      }
      const trackId = findTrackId(node.target);
      if (trackId) {
        console.log('trackid ---> ', trackId);
        axios.post("/api/trackingData", {
          tracking_data: {
            button: trackId
          }
        }).catch(err => {
          console.log(err);
        });
      }
    })

  }

  render() {
    return (
      <div id="trackId" className="login">
        <TopNavBar referrer={this.state.referrer} />


        <NextSeo config={Constants.next_seo_config("login")} />

        <NoSsr>

          <MuiThemeProvider theme={this.getMuiTheme()}>

            {/* <Grid container spacing={0} direction="column" align="center"> */}
            <Grid container spacing={0} >

              <Grid item xs={0} sm={3} md={4} lg={4} style={{ marginTop: "50px" }} />

              <Grid xs={12} sm={6} md={4} lg={4} style={{ marginTop: "50px" }}>


                <CardContent style={{ paddingBottom: "0px" }}>




                  {this.renderContent()}



                  {/* <div style={{ padding: "15px 15px 5px 15px" }}>
                      <TextField
                        label="Email"
                        value={this.state.email.toLowerCase()}
                        onChange={this.handleFieldChange('email')}
                        fullWidth
                        error={this.state.email_error}
                        helperText={this.state.email_error_text}

                      // InputProps={{
                      //   endAdornment: <InputAdornment>@email.com</InputAdornment>,
                      // }}
                      />
                    </div>
                    <div style={{ padding: "15px 15px 2px 15px" }}>

                      <TextField
                        label="Password"
                        onKeyPress={(ev) => {
                          if (ev.key === 'Enter') {
                            // ev.preventDefault();
                            this.handleLogin()
                          }
                        }} value={this.state.password}
                        onChange={this.handleFieldChange('password')}
                        type="password"
                        fullWidth
                      />
                    </div>

                    <Button onClick={this.handleChangePassword} size="small" color="primary" style={{ float: "left", marginLeft: "15px", padding: 0, fontSize: 9, minHeight: "9px" }}> change password </Button> */}

                </CardContent>


              </Grid>

              <Grid item xs={0} sm={3} md={4} lg={4} style={{ marginTop: "50px" }} />

            </Grid>


          </MuiThemeProvider>

        </NoSsr>


        <BotNavBar referrer={this.state.referrer} />

      </div>
    );
  }
}


export default Login;