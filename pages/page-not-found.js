import React, { Component } from 'react';

import { NoSsr, Button, Typography, IconButton, AppBar, Toolbar } from '@material-ui/core';

// import Logo from './ifinsg_logo.png'
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo

import TopNavBar from './components/TopNavBar'
import BotNavBar from './components/BotNavBar'
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Constants from 'constants/constants';


const styles = {
  page_width: Constants.style.page.width,
  page_text_width: Constants.style.page.text_width,

  page_padding: Constants.style.page.padding,

  align_center: Constants.style.align.center,
  align_left: Constants.style.align.left,

  bold: Constants.style.text.bold,
  break_word: Constants.style.text.break_word,

  title1: Constants.style.text.title1,
  body1: Constants.style.text.body1,

  margin_top_xl: Constants.style.margin_top.xl,
  margin_top_lg: Constants.style.margin_top.lg,
  margin_top_md: Constants.style.margin_top.md,
  margin_top_sm: Constants.style.margin_top.sm,
  margin_top_xs: Constants.style.margin_top.xs,
  margin_top_xxs: Constants.style.margin_top.xxs,
}

class TermsOfUse extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount = () => {
    Constants.store_referrer(this)
  }

  render() {
    const { classes } = this.props;

    return (
      <div>
        <TopNavBar referrer={this.state.referrer}/>


        <NextSeo config={Constants.next_seo_config("not-found")} />


        <NoSsr>



          <div className={classNames(classes.align_center)}>
            <div className={classNames(classes.page_width, classes.page_padding, classes.break_word, classes.align_left)}>

              <div style={{ padding: '200px 0px 150px 0px', margin: "0px 0px 0px 0px", textAlign: "center" }}>
                <Typography style={{ fontSize: '50px', lineHeight: '1', margin: '30px 0px 0px 0px', color: "#4cae4f", fontWeight: "bold" }}>
                  404 Error: Page not found
                </Typography>
              </div>

            </div>

          </div>

        </NoSsr>
        <BotNavBar referrer={this.state.referrer}/>

      </div>
    );
  }
}


export default withStyles(styles)(TermsOfUse);