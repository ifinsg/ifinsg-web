import React, { Component } from 'react';

import { NoSsr, Button, Grid } from '@material-ui/core';
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import TopNavBar from './components/TopNavBar';
import BotNavBar from './components/BotNavBar';

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Constants from 'constants/constants';
import Tick from 'components/icons/Tick';
import Head from 'next/head';
import Link from 'next/link';

const styles = {
    page_width: Constants.style.page.width,
    page_text_width: Constants.style.page.text_width,

    page_padding: Constants.style.page.padding,

    align_center: Constants.style.align.center,

    bold: Constants.style.text.bold,
    break_word: Constants.style.text.break_word,

    title1: Constants.style.text.title1,
    body1: Constants.style.text.body1,

    margin_top_xl: Constants.style.margin_top.xl,
    margin_top_lg: Constants.style.margin_top.lg,
    margin_top_md: Constants.style.margin_top.md,
    margin_top_sm: Constants.style.margin_top.sm,
    margin_top_xs: Constants.style.margin_top.xs,
    margin_top_xxs: Constants.style.margin_top.xxs
};

class Premiumback extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount = () => {
        Constants.store_referrer(this);
    };

    render() {
        const { classes } = this.props;

        return (
            <div>
                <TopNavBar selected="premiumback" referrer={this.state.referrer} />

                <NextSeo config={Constants.next_seo_config('premium-back')} />

                <NoSsr>
                    <div className={classNames(classes.align_center)}>
                        <div className={classNames(classes.page_text_width, classes.page_padding, classes.break_word)}>
                            <div
                                style={{
                                    margin: '10px 0',
                                    fontSize: '40px',
                                    fontWeight: 'bold',
                                    fontFamily: 'Merriweather'
                                }}
                            >
                                The Cyclic insurance PremiumBack
                            </div>
                            <div style={{ margin: '70px 0px 0px 0px', padding: '0px 30px 0px 30px', fontSize: '20px' }}>
                                Our mission is to bring insurance back to what it really should be:{' '}
                            </div>
                            <div style={{ margin: '70px 0px 0px 0px', padding: '0px 30px 0px 30px', fontSize: '20px' }}>
                                <span style={{ fontWeight: 'bold' }}>1st</span>, to be Affordable for any human being
                                who wants to be protected.{' '}
                            </div>
                            <div style={{ margin: '70px 0px 0px 0px', padding: '0px 30px 0px 30px', fontSize: '20px' }}>
                                <span style={{ fontWeight: 'bold' }}>2nd</span>, to make sure insurance protection need
                                not be a strain on Cash flows.{' '}
                            </div>
                            <div style={{ margin: '70px 0px 0px 0px', padding: '0px 30px 0px 30px', fontSize: '20px' }}>
                                <span style={{ fontWeight: 'bold' }}>3rd</span>, to ensure service Sustainability and
                                not profitability.{' '}
                            </div>
                            <div style={{ margin: '70px 0px 0px 0px', padding: '0px 30px 0px 30px', fontSize: '20px' }}>
                                <span style={{ fontWeight: 'bold' }}>
                                    This is where our PremiumBack solves everything.{' '}
                                </span>
                            </div>

                            <div>
                                {' '}
                                <img
                                    style={{
                                        maxWidth: '400px',
                                        width: '100%',
                                        margin: '150px 0px 0px 0px',
                                        padding: '0px 0px 0px 0px'
                                    }}
                                    src={'/img/Pic-13-PremiumBack.png'}
                                />{' '}
                            </div>

                            <div style={{ paddingTop: '30px' }}></div>
                            <div
                                style={{
                                    margin: '0px 0px 0px 0px',
                                    fontSize: '40px',
                                    lineHeight: '1',
                                    fontFamily: 'Merriweather'
                                }}
                            >
                                Why you’ll love PremiumBack
                            </div>
                            <div style={{ paddingTop: '50px' }}></div>
                            <div style={{ margin: '40px 0px 0px 0px', padding: '0px 30px 0px 30px', fontSize: '20px' }}>
                                {' '}
                                Cyclic insurance treats collected premiums as if they were still your money, not ours.{' '}
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', padding: '0px 30px 0px 30px', fontSize: '20px' }}>
                                {' '}
                                Cyclic insurance only earns a flat and transparent monthly{' '}
                                <span style={{ fontWeight: 'bold' }}>Admin Fee</span> per policy per user, and collects
                                premiums as a pool in order to pay out claims.{' '}
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', padding: '0px 30px 0px 30px', fontSize: '20px' }}>
                                {' '}
                                At the end of every cycle, after paying out verified claims, all premiums unused for
                                claims will be returned as <span style={{ fontWeight: 'bold' }}>PremiumBack</span> to
                                users who made 0 claims and users who made low claims.{' '}
                            </div>
                            <div style={{ margin: '40px 0px 0px 0px', padding: '0px 30px 0px 30px', fontSize: '20px' }}>
                                <span style={{ fontWeight: 'bold' }}>This means:</span>
                            </div>

                            <div style={{ marginTop: '20px' }}>
                                <div style={{ float: 'left' }}>
                                    {' '}
                                    <Tick />{' '}
                                </div>
                                <div style={{ margin: '0px 0px 0px 25px', fontSize: '20px' }}>
                                    {' '}
                                    There’s never a conflict of interest in terms of claims, because every $ of unused
                                    premiums are returned to users at the end of each cycle anyway.{' '}
                                </div>
                            </div>
                            <div style={{ marginTop: '20px' }}>
                                <div style={{ float: 'left' }}>
                                    {' '}
                                    <Tick />{' '}
                                </div>
                                <div style={{ margin: '0px 0px 0px 25px', fontSize: '20px' }}>
                                    You’ll never have to worry about Cash-flows, because some money comes back quickly
                                    every 3 months, even when you make 0 claims or low claims.{' '}
                                </div>
                            </div>
                            <div style={{ marginTop: '20px' }}>
                                <div style={{ float: 'left' }}>
                                    {' '}
                                    <Tick />{' '}
                                </div>
                                <div style={{ margin: '0px 0px 0px 25px', fontSize: '20px' }}>
                                    {' '}
                                    You get amazing insurance, at the lowest possible cost.{' '}
                                </div>
                            </div>

                            <div style={{ paddingTop: '100px' }}></div>

                            <div
                                style={{
                                    margin: '100px 0px 0px 0px',
                                    fontSize: '40px',
                                    lineHeight: '1',
                                    fontFamily: 'Merriweather'
                                }}
                            >
                                How PremiumBack works
                            </div>
                            <div>
                                {' '}
                                <img
                                    style={{
                                        maxWidth: '400px',
                                        margin: '40px 0px 0px 0px',
                                        padding: '0px 0px 0px 0px'
                                    }}
                                    src={'/img/Pic-14-PremiumBack.png'}
                                />{' '}
                            </div>
                            <div style={{ margin: '0px 0px 0px 0px', padding: '0px 30px 0px 30px', fontSize: '20px' }}>
                                Signup for the Cyclic insurance you want.
                            </div>
                            <div style={{ paddingTop: '10px' }}></div>
                            <div>
                                {' '}
                                <img
                                    style={{
                                        maxWidth: '400px',
                                        margin: '40px 0px 0px 0px',
                                        padding: '0px 0px 0px 0px'
                                    }}
                                    src={'/img/Pic-15-PremiumBack.png'}
                                />{' '}
                            </div>
                            <div style={{ margin: '0px 0px 0px 0px', padding: '0px 30px 0px 30px', fontSize: '20px' }}>
                                At the end of each cycle, we do a rigorous check to make sure users get the correct
                                PremiumBack.
                            </div>
                            <div style={{ paddingTop: '10px' }}></div>
                            <div>
                                {' '}
                                <img
                                    style={{
                                        maxWidth: '400px',
                                        margin: '40px 0px 0px 0px',
                                        padding: '0px 0px 0px 0px'
                                    }}
                                    src={'/img/Pic-16-PremiumBack.png'}
                                />{' '}
                            </div>
                            <div style={{ margin: '0px 0px 0px 0px', padding: '0px 30px 0px 30px', fontSize: '20px' }}>
                                The lower the total claims you make, the higher the PremiumBack you will qualify for
                                each cycle!
                            </div>

                            <Link href={Constants.get_new_page('products', this.state.referrer)}>
                                <Button
                                    variant="contained"
                                    style={{
                                        margin: '120px 0px 0px 0px',
                                        padding: '10px 20px 10px 20px',
                                        fontSize: '15px',
                                        backgroundColor: '#7cda24',
                                        fontFamily: "Lato",
                                        color: 'white'
                                    }}
                                >
                                    What can I sign up for?
                                </Button>
                            </Link>

                            <div style={{ paddingTop: '100px' }}></div>
                        </div>
                    </div>
                </NoSsr>

                <BotNavBar referrer={this.state.referrer} />
            </div>
        );
    }
}

export default withStyles(styles)(Premiumback);
