import React, { Component } from 'react';

// import Logo from './ifinsg_logo.png'
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import { NoSsr } from '@material-ui/core';

import TopNavBar from './components/TopNavBar'
import BotNavBar from './components/BotNavBar'

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Constants from 'constants/constants';
import EmailJoanna from './components/constant_components/EmailJoanna'
import Head from 'next/head';


const styles = {
  page_width: Constants.style.page.width,
  page_text_width: Constants.style.page.text_width,

  page_padding: Constants.style.page.padding,

  align_center: Constants.style.align.center,
  align_left: Constants.style.align.left,

  bold: Constants.style.text.bold,
  break_word: Constants.style.text.break_word,

  title1: Constants.style.text.title1,
  body1: Constants.style.text.body1,

  margin_top_xl: Constants.style.margin_top.xl,
  margin_top_lg: Constants.style.margin_top.lg,
  margin_top_md: Constants.style.margin_top.md,
  margin_top_sm: Constants.style.margin_top.sm,
  margin_top_xs: Constants.style.margin_top.xs,
  margin_top_xxs: Constants.style.margin_top.xxs,
}

class PrivacyPolicy extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }


  componentDidMount = () => {
    Constants.store_referrer(this)
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <TopNavBar referrer={this.state.referrer}/>

        <NextSeo config={Constants.next_seo_config("privacy-policy")} />

        <NoSsr>


          <div className={classNames(classes.align_center)}>
            <div className={classNames(classes.page_width, classes.page_padding, classes.break_word, classes.align_left)}>

              <div style={{ padding: '50px 0px 0px 0px', textAlign: "center" }}>
                <img style={{ maxWidth: '200px', margin: '40px 0px 0px 0px', padding: '0px 0px 0px 0px' }} src={('/img/iFin-SG-Logo.png')} />
                <div style={{ fontSize: '50px', lineHeight: '1', margin: '30px 0px 0px 0px', color: "#4cae4f", fontWeight: "bold" }}>PRIVACY POLICY</div>
              </div>


              <div style={{ padding: '0px 0px 50px 0px' }}>

                <div style={{ margin: '60px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Introduction </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>The Personal Data Protection Act (“PDPA”) governs the collection, storage, use and disclosure of personal data in Singapore. We take our responsibilities with respect to personal data protection seriously and have thus formulated a policy on the use of personal data for your protection. The provision of your personal data is voluntary but if you choose not to provide us with any requested data, such failure may inhibit our ability to provide services to you or affect the quality of the services we provide to you through the Website.</li>
                  </ol>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Data Collection </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>Data will be collected from you in the following manner:
                <ol>
                        <li type="a">When there is usage of our products or services, access to our online services or to our Website or use of our mobile application or other online services;</li>
                        <li type="a">When documents or application forms are submitted to us for the purchase of our products or services;</li>
                        <li type="a">When you submit your enquiries, or issue instructions or make any requests or updates to your account with us or requests for changes to any products or services purchased;</li>
                        <li type="a">When you interact or communicate with the Service Providers profiled on the Website, our agents, our personnel, or customer service officers whether by telephone, email, face to face meeting, SMS messages, fax, by letter or otherwise;</li>
                        <li type="a">When we receive referrals and collect or share personal data from within our group of related or affiliated businesses and/or companies and third party providers of services to us or to other members of our group of related or affiliated businesses and/or companies;</li>
                        <li type="a">When we seek information from third parties who hold personal data concerning your goodself, including your medical, healthcare, legal, banking, accounting professionals;</li>
                        <li type="a">When you submit personal data to us during the course of your participation in any marketing promotional or advertising event organised or hosted by us or our related or affiliated businesses and/or companies; and</li>
                        <li type="a">When you respond to our approaches or promotions.</li>
                      </ol>
                    </li>
                  </ol>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Use of Personal Data </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>Personal Data information directly obtained from you is used for the following purposes:
                <ol>
                        <li type="a">To evaluate and provide advice or recommendations to you to fulfil your requirements for our services, and so that we may tailor our services to your needs;</li>
                        <li type="a">To fulfil any applicable rules and regulations whether of a local governmental nature or of foreign authorities and generally for legal compliance requirements;</li>
                        <li type="a">To respond to your requests and enquiries, to receive feedback and complaints from you to improve the quality of our services to you and in this regard, to verify the identity of your representative(s) who contact us;</li>
                        <li type="a">To communicate with you to obtain your instructions or regarding any changes or updates to our policies or to introduce any products or services that may be relevant to you, to update you on policies and rules and regulations or compliance requirements of any governmental agencies that are relevant to you;</li>
                        <li type="a">To create, administer, maintain, and to manage your account with us or the products and services offered to you or purchased by you including processing your instructions, preparing documentation and instruments relating to the products and services you have purchased, and the issuing and printing of statements of accounts to you on a regular basis;</li>
                        <li type="a">For statistical, trend analysis and planning purposes including data processing, statistical or market analysis;</li>
                        <li type="a">To store and safekeep or to archive documents and records in both electronic and physical format as part of the administrative and operational requirements of our business;</li>
                        <li type="a">To comply with any contractual or other obligations, and arrangements for data sharing and information within the Company, our affiliates and related business companies and partners who provide administrative, professional advisory or other support services to us;</li>
                        <li type="a">To prevent, detect and investigate any crime, including fraud and any form of financial crime, analysing and managing any financial risks;</li>
                        <li type="a">For marketing or promotional purposes relating to the Company, our related or affiliated businesses and/or companies;</li>
                        <li type="a">To conduct our internal checks, to carry out financial reporting and analysis related to our business operations; and</li>
                        <li type="a">For any other purposes directly relating to the above purposes.</li>
                      </ol>
                    </li>
                    <li>If you have provided your contact number(s) and addresse(s) and have indicated that you consent to receiving marketing or promotional material, then our Registered Service Providers, affiliates and related business companies and partners who offer to provide you with their services or products to this Website or who provide administrative, professional advisory or other support services to us, may contact you using such contacts to inform you or to recommend to you relevant products and services that we or they feel may be of interest to you.</li>
                  </ol>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Online Collection of Data </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>In addition to the information we collect directly from you, we also use “cookies” to monitor and track usage of our Website, viewing and traffic patterns, customer behaviour and other similar information.</li>
                    <li>We compile this information into a statistical aggregate and use this information as well as all of your personal or business information for our internal business and administrative purposes which includes improving our Website, our product and service offerings, our operations, and our customer service.</li>
                    <li>The Company including our Registered Service Providers, related or affiliated businesses and/or companies also might use your personal or business information from time to time to send you notices about special offers, new products, new services, promotions, and other similar information when you use our online services or access our Website.</li>
                    <li>Should you wish to disable the cookies associated with these technologies, you may do so by changing the setting on your browser but you may not then be able to enter certain part(s) of our Website.</li>
                    <li>Our Website may contain links to other sites but the Company is not responsible for the privacy practices or the content of these other Websites.</li>
                    <li>We encourage you to learn about the privacy policies of such third party Websites. Some of these third party Websites may be co-branded with our logo or trademark, even though they are not operated or maintained by us. Once you have left our Website, you should check the applicable data protection policy of the third party to determine how they will handle any information they collect from you.</li>
                  </ol>
                </div>



                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Disclosure of Personal Data </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>Your personal data will be protected subject to disclosure to the Company’s agents, our Registered Service Providers, our personnel, or customer service officers who are servicing you on a “need to know” basis as a necessary part of our business and operational requirements.</li>
                  </ol>
                </div>



                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Limited Disclosure to Third Parties </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>Personal Data is only otherwise disclosed to the following third parties:
                <ol>
                        <li type="a">The Company’s holding company or subsidiary or a subsidiary of the Company’s holding company;</li>
                        <li type="a">A company or business entity in which any of our Directors or shareholders hold an equity or business interest of not less than %;</li>
                        <li type="a">Life Insurance Association Singapore, the Monetary Authority of Singapore or other like competent Government or non-Governmental agencies or authorities;</li>
                        <li type="a">Operators of payment systems such as Network for Electronic Transfers (Singapore) Pte Ltd, banks , financial institutions, credit card companies and their respective service providers;</li>
                        <li type="a">Our financial or legal advisers or other professional advisers for the purposes of obtaining financial, or accounting, legal or other professional advice and services in relation to our business operations;</li>
                        <li type="a">Governmental agencies or regulatory agencies or authorities for legal compliance purposes;</li>
                        <li type="a">Any liquidator, receiver, official assignee / trustee, judicial manager under or pursuant to any applicable law or court order in connection with the bankruptcy, liquidation, winding up, judicial management or any other like process; and</li>
                        <li type="a">Any third party company or business with whom we have a business relationship and who provide us with administrative or other support services such as telecommunications, call centre, mailing, information technology, payment, payroll, data processing training, marketing research, storage and archival services or who are our sub-contractors, advisers, assistants or agents [whether on paid or voluntary basis] tasked with performing all or part of the services we have contracted to provide to you.</li>
                      </ol>
                    </li>
                    <li>Save for Governmental agencies or regulatory agencies or authorities, your personal data is disclosed only to such of the above mentioned third parties as have entered into an express or implied Confidentiality Agreement with us to preserve the confidentiality of your personal data and on a “need to know” basis and for the purposes we have set out above.</li>
                  </ol>
                </div>



                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> No Trading In Personal Data </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ul>
                    <li>Save as provided herein, we do not disclose any customer information to any other third party unless we are required by law or by an Order of Court. In addition, we only permit authorized employees, who are trained in the proper handling of customer information to have access to such information and on a “need to know” basis.</li>
                    <li>We do not trade or sell your personal data or such information to anyone under any circumstances.</li>
                  </ul>
                </div>




                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Withdrawal Of Consent </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ul>
                    <li>You may withdraw your consent for the collection, use or disclosure or your personal data in our possession by contacting ICH iFinancial Singapore Private Limited, UEN: 201631146G at: <EmailJoanna/>.</li>
                    <li>Please note that if you withdraw your consent to any or all use or disclosure of your Personal Data, depending on the nature of your request, we may not be in a position to continue to provide our products or services to you or administer any contractual relationship in place and such withdrawal may also result in the termination of any Agreement we may have with you.</li>
                    <li>All Service Providers are required to keep all Personal Data received from End-Users strictly confidential and to comply strictly with this <span style={{fontWeight:"bold"}}>PRIVACY POLICY</span> with respect to the handling of all Personal Data of End-Users received by them. In the event of your breach of any of the above matters, the Service Provider shall be liable to indemnify and hold harmless us, our officers, our employees, affiliates, agents and representatives against all damages, costs and expenses including any claim from any third parties and legal costs on full solicitor and client indemnity basis that arises from such breach of this <span style={{fontWeight:"bold"}}>PRIVACY POLICY</span> to us. This Clause shall survive the termination of this Web User Agreement or any agreement we may have with any Service Provider.</li>
                    <li>All Service Providers acknowledge that any such breach will cause the Company irreparable injury and damage. Accordingly, the Service Provider agrees that the Company shall be entitled to immediate equitable relief including without limitation injunction in the event of any such breach of this <span style={{fontWeight:"bold"}}>PRIVACY POLICY</span>.</li>
                  </ul>
                </div>



                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Contacting Us </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  If you have any questions about our <span style={{fontWeight:"bold"}}>PRIVACY POLICY</span> or the privacy practices of our Website, please contact ICH iFinancial Singapore Private Limited, UEN: 201631146G at <EmailJoanna/> .
          </div>


              </div>
            </div>
          </div>
        </NoSsr>
        <BotNavBar referrer={this.state.referrer}/>

      </div>
    );
  }
}


export default withStyles(styles)(PrivacyPolicy);