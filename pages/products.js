import React, { Component } from 'react';

// import Logo from './ifinsg_logo.png'
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo

import {
    NoSsr,
    Button,
    Typography,
    Checkbox,
    Grid,
    Paper,
    FormControlLabel,
    IconButton,
    Divider,
    Card
} from '@material-ui/core';
import ModalPolicyDetails from './components/modals/ModalPolicyDetails';
import ModalHomeFeatures from './components/modals/ModalHomeFeatures';

import CloseIcon from '@material-ui/icons/Close';

import { BrowserView, MobileView, isBrowser, isMobile } from 'react-device-detect';
import Head from 'next/head';

import TopNavBar from './components/TopNavBar';
import BotNavBar from './components/BotNavBar';
import Router from 'next/router';

import classNames from 'classnames';
import { createMuiTheme, MuiThemeProvider, makeStyles, withStyles } from '@material-ui/core/styles';
import Constants from 'constants/constants';

import Tick from 'components/icons/Tick';

import HomePageProduct from 'components/product/HomePageProduct';

const styles = {
    page_width: Constants.style.page.width,

    page_text_width: Constants.style.page.text_width,

    page_padding: Constants.style.page.padding,

    align_center: Constants.style.align.center,

    full_width: Constants.style.align.full_width,
    position_fixed: Constants.style.align.position_fixed,

    bold: Constants.style.text.bold,
    break_word: Constants.style.text.break_word,

    title1: Constants.style.text.title1,
    body1: Constants.style.text.body1,

    margin_top_xl: Constants.style.margin_top.xl,
    margin_top_lg: Constants.style.margin_top.lg,
    margin_top_md: Constants.style.margin_top.md,
    margin_top_sm: Constants.style.margin_top.sm,
    margin_top_xs: Constants.style.margin_top.xs,
    margin_top_xxs: Constants.style.margin_top.xxs
};

const theme = createMuiTheme({
    typography: {
        fontFamily: ['Lato'].join(',')
    },
    palette: {
        primary: {
            light: '#569818',
            main: '#7cda23',
            dark: '#96e14f',
            contrastText: '#fff'
        },
        secondary: {
            light: '#6f7fff',
            main: '#4c5fff',
            dark: '#3542b2',
            contrastText: '#fff'
        }
    }
});

class Products extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checkout_padding_top: '60px',
            checkout_height: '120px',
            products: [],
            expanded_small_accident: false,
            expanded_small_critical_checkup: false,
            policy_details_modal_open: false
        };
    }

    componentDidMount = () => {
        window.scrollTo(0, 0);
        fbq('track', 'ViewContent');
    };

    render() {
        const { classes } = this.props;
        return (
            <div>
                <MuiThemeProvider theme={theme}>
                    <TopNavBar selected="products" referrer={this.state.referrer} />
                    <NextSeo config={Constants.next_seo_config('products')} />
                    <div className={classNames(classes.align_center)}>
                        <div className={classNames(classes.page_text_width, classes.page_padding, classes.break_word)}>
                            <div style={{ width: '100%' }}>
                                <h1
                                    style={{
                                        margin: '10px 0',
                                        fontSize: '40px',
                                        fontWeight: 'bold',
                                        fontFamily: 'Merriweather'
                                    }}
                                >
                                    Products
                                </h1>
                                <HomePageProduct />
                                {/* </NoSsr> */}
                                {/* </BrowserView> */}
                                {/* <NoSsr> */}
                                <div
                                    style={{
                                        textAlign: 'center',
                                        marginTop: '30px',
                                        fontSize: '16px',
                                        padding: '0px 20px 0px 20px'
                                    }}
                                >
                                    <div style={{ marginTop: '30px' }}>
                                        <Typography>
                                            We only earn a flat and transparent monthly{' '}
                                            <span style={{ fontWeight: 'bold' }}>Admin Fee </span> per policy per user,
                                            with no hidden charges.
                                        </Typography>
                                    </div>
                                    {!Constants.promotion_ongoing ? (
                                        ''
                                    ) : (
                                        <img
                                            style={{
                                                maxWidth: '480px',
                                                width: '100%',
                                                margin: '30px 0px 0px 0px',
                                                padding: '0px 0px 0px 0px'
                                            }}
                                            src={'/img/promo_priority_claims_status.png'}
                                        ></img>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* </NoSsr> */}
                    <BotNavBar referrer={this.state.referrer} />
                </MuiThemeProvider>
            </div>
        );
    }
}

export default withStyles(styles)(Products);
