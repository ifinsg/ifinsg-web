import React, { Component } from 'react';
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import TopNavBar from './components/TopNavBar';
import BotNavBar from './components/BotNavBar';
import ModalPolicyDetails from './components/modals/ModalPolicyDetails';
import ModalChangePassword from './components/modals/ModalChangePassword';
import ModalEditField from './components/modals/ModalEditField';
import purple from '@material-ui/core/colors/purple';
import lightGreen from '@material-ui/core/colors/lightGreen';
import styled from 'styled-components';

import {
    NoSsr,
    Button,
    Typography,
    AppBar,
    Tabs,
    Tab,
    Paper,
    TextField,
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    Card,
    FormControlLabel,
    Checkbox,
    Fab,
    Divider,
    IconButton,
    Grid,
    FormLabel,
    RadioGroup,
    Radio,
    Switch
} from '@material-ui/core';

import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';

import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import Head from 'next/head';
import get from 'lodash/get';
import groupBy from 'lodash/groupBy';
import DeleteIcon from '@material-ui/icons/Delete';
import SelectDoctor from './components/selects/SelectDoctor';
import SelectClinic from './components/selects/SelectClinic';
import PhoneIcon from '@material-ui/icons/Phone';
import AccessibilityNewIcon from '@material-ui/icons/AccessibilityNew';
import LocationCityIcon from '@material-ui/icons/LocationCity';
import ChildFriendlyIcon from '@material-ui/icons/ChildFriendly';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import HomeWorkIcon from '@material-ui/icons/HomeWork';
import BusinessIcon from '@material-ui/icons/Business';
import GridEmptySpace from 'components/universal/GridEmptySpace';

import SwipeableViews from 'react-swipeable-views';

import { BrowserView, MobileView, isBrowser, isMobile } from 'react-device-detect';
import axios from 'axios';

import Router from 'next/router';
import 'url-search-params-polyfill'; //Source: https://www.npmjs.com/package/url-search-params-polyfill

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Constants from 'constants/constants';
import GlobalConstants from '../global/constants';
import GlobalHelpers from '../global/helpers';

import ModalSubmitClaimForm from './components/modals/ModalSubmitClaimForm';
import ModalConfirmCancelOrReinstate from './components/modals/ModalConfirmCancelOrReinstate';

let allow_edit_field = false;
let allow_claims_history = false;

if (GlobalConstants.staging) {
    allow_edit_field = true;
    allow_claims_history = true;
}

const getSumAssuredText = (policy_name) => {
    return Constants.product_details[policy_name].sum_assured_text;
};

const styles = {
    page_width: Constants.style.page.width,
    page_text_width: Constants.style.page.text_width,
    page_padding: Constants.style.page.padding,

    align_center: Constants.style.align.center,

    bold: Constants.style.text.bold,
    break_word: Constants.style.text.break_word,

    title1: Constants.style.text.title1,
    body1: Constants.style.text.body1,

    margin_top_xl: Constants.style.margin_top.xl,
    margin_top_lg: Constants.style.margin_top.lg,
    margin_top_md: Constants.style.margin_top.md,
    margin_top_sm: Constants.style.margin_top.sm,
    margin_top_xs: Constants.style.margin_top.xs,
    margin_top_xxs: Constants.style.margin_top.xxs
};

/////////////////////////////////// Developer Settings ///////////////////////////////////
const do_not_login_via_token = false;
/////////////////////////////////// Constants ///////////////////////////////////

const convertStatusHistoryStatusCodeToEvent = {
    pending_approval: 'Purchased',
    pending_rework: 'Rework requested',
    pending_rework_approval: 'Rework submitted',
    active: 'Active',
    rejected: 'Rejected',
    cancellation_requested: 'Cancellation Requested',
    cancelled: 'Cancelled',
    reinstatement_requested: 'Reinstatement Requested'
};

const convertStatusCodeToStatus = {
    pending_approval: 'Pending Approval',
    pending_rework: 'Pending Rework',
    pending_rework_approval: 'Pending Rework Approval',
    approved: 'Active',
    active: 'Active',
    rejected: 'Rejected',
    cancellation_requested: 'Cancellation Requested',
    cancelled: 'Cancelled',
    reinstatement_requested: 'Reinstatement Requested'
};

const getStatusColor = {
    pending_approval: 'skyblue',
    approved: 'green',
    active: 'green'
};

function TabContainer({ children, dir }) {
    return (
        <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
            {children}
        </Typography>
    );
}

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            tab: 0,
            groupTab: 0,
            tabPolicy: 0,
            policyTab: 0,
            user_info: {},
            policy_details_modal_open: false,
            submit_claim_modal_open: false,
            submit_claim_modal_policy_details: {},
            change_password_modal_open: false,

            edit_field_modal_open: false,
            edit_field_user_or_policy: false,
            edit_field_username_or_id: false,
            edit_field_name: false,
            edit_field_title: false,

            policies: [],
            policyGroupBy: {},
            enable_change_password_button: false, // Just an impulse to trigger ModalChangePassword to enable it's change password button
            enable_edit_field_button: false, // Just an impulse to trigger ModalChangePassword to enable it's change password button

            medical_registrar_smc: true,

            modal_confirm_cancel_or_reinstate_open: false
        };
    }

    /////////////////////////////////// Component Level ///////////////////////////////////

    componentDidMount = () => {
        this.getUserPolicies()
            .then(() => {
                return this.redirectToLoginIfNeeded();
            })
            .then(() => {
                this.getAccidentsAndClaims();
                return this.setSwitchStatuses();
            });

        Constants.store_referrer(this);

        this.setState({
            isLoading: false
        });
    };

    getUserPolicies = () => {
        return new Promise((resolve, reject) => {
            try {
                if (!do_not_login_via_token) {
                    const token = localStorage.getItem('ifinsg_token');
                    if (token) {
                        axios
                            .post('/api/auth/getUserDataFromToken', {
                                token,
                                get_policies: true,
                                get_claims: true
                            })
                            .then((res) => {
                                if (res.status === 200) {
                                    if (res.data.err) {
                                        resolve();
                                    } else {
                                        try {
                                            const { policies } = res.data.user_info || {};
                                            const policyGroupBy = groupBy(policies, 'representing');
                                            this.setState(
                                                {
                                                    user_info: res.data.user_info,
                                                    policyGroupBy: policyGroupBy
                                                },
                                                () => {
                                                    resolve();
                                                }
                                            );
                                        } catch (err) {
                                            resolve();
                                        }
                                    }
                                } else {
                                    resolve();
                                }
                            })
                            .catch((err) => {
                                resolve();
                            });
                    } else {
                        resolve();
                    }
                }
            } catch (err) {
                resolve();
            }
        });
    };

    redirectToLoginIfNeeded = () => {
        try {
            if (!this.state.user_info.username) {
                Router.push(Constants.get_new_page('login', this.state.referrer));
            }
        } catch (err) {
            Router.push(Constants.get_new_page('login', this.state.referrer));
        }
    };

    setSwitchStatuses = () => {
        let policies = this.state.user_info.policies;
        try {
            for (let i = 0; i < policies.length; i++) {
                policies[i].expanded = false;
                policies[i].submit_claim = {
                    accident: '',
                    accident_description: '',
                    treatment_description: '',
                    uploaded_images: [],
                    ack_claim_from_other_insurers_first: false,
                    addon_notes: ''
                };

                switch (policies[i].status) {
                    case 'approved':
                        policies[i].status_active_switch = true;
                        break;
                    case 'active':
                        policies[i].status_active_switch = true;
                        break;
                    default:
                        policies[i].status_active_switch = false;
                        break;
                }
            }

            this.setState({
                policies
            });
        } catch (err) {}
    };

    handleTabChange = (event, value) => {
        this.setState({ tab: value });
    };

    handlePolicyTabChange = (event, value) => {
        this.setState({ policyTab: value });
    };

    handleTabChangeIndex = (index) => {
        this.setState({ tab: index });
    };

    handlePolicyTabChangeIndex = (index) => {
        this.setState({ policyTab: index });
    };

    handleSubmitClaimClicked = (policy_details) => {
        this.setState({
            submit_claim_modal_open: true,
            submit_claim_modal_mode: 'new_claim',
            submit_claim_modal_policy_details: policy_details
        });
    };

    handlePolicyDetailsClicked = (policy) => {
        this.setState({
            policy_details_modal_open: true,
            policy_details_modal_policy: policy
        });
    };

    getSwitchChecked = (i) => {
        let value_to_return = false;
        try {
            if (this.state.user_info.policies[i].status_active_switch) {
                value_to_return = true;
            }
        } catch (err) {}
        return value_to_return;
    };

    getSwitchDisabled = (i) => {
        // TOGGLE SWITCH
        let allow_togglable = false;
        try {
            switch (this.state.user_info.policies[i].status) {
                case 'live':
                    allow_togglable = true;
                    break;
                case 'active':
                    allow_togglable = true;
                    break;
                case 'approved':
                    allow_togglable = true;
                    break;
                case 'cancelled':
                    allow_togglable = true;
                    break;
                default:
                    allow_togglable = false;
                    break;
            }
            return !allow_togglable; // true=disable, false=enable
        } catch (err) {}
    };

    togglePolicyExpand = (i) => {
        let user_info = JSON.parse(JSON.stringify(this.state.user_info));

        // Collapsing all policies
        for (let j = 0; j < user_info.policies.length; j++) {
            user_info.policies[j].expanded = false;
        }

        if (this.state.user_info.policies[i].expanded) {
            // If policy is expanded, collapse it
            // Do nothing.
        } else {
            // If it is currently collapsed, expand it.
            user_info.policies[i].expanded = true;
        }

        this.setState({
            user_info
        });
    };

    getPolicyExpanded = (i) => {
        let returnable = false;
        if (this.state.user_info.policies[i].expanded) {
            returnable = this.state.user_info.policies[i].expanded;
        }
        return returnable;
    };

    handleSwitchChange = (i) => (event) => {
        let checked = event.target.checked;
        let policies = this.state.user_info.policies;
        if (checked) {
            this.setState({
                modal_confirm_cancel_or_reinstate_open: true,
                modal_confirm_cancel_or_reinstate_data: {
                    policy_id: policies[i].id,
                    policy_index: i,
                    policy_status: 'reinstatement_requested',
                    checked: checked,
                    message_label: false,
                    modal_title: 'Request to reinstate'
                }
            });
        } else {
            // Trigger intent to cancel (de-activate)
            this.setState({
                modal_confirm_cancel_or_reinstate_open: true,
                modal_confirm_cancel_or_reinstate_data: {
                    policy_id: policies[i].id,
                    policy_index: i,
                    policy_status: 'cancellation_requested',
                    checked: checked,
                    message_label: 'Reason for cancellation',
                    modal_title: 'Request to cancel policy'
                }
            });
        }

        // this.setState({ [name]: event.target.checked });
    };

    /////////////////////////////////// Policies ///////////////////////////////////

    renderPolicyTab = (policy, i) => {
        // return (<div>hii</div>)

        switch (this.state.policyTab) {
            case 0:
                return (
                    <div style={{ padding: '10px' }}>
                        {/* ACCOUNTS SUMMARY */}
                        <Typography
                            variant="subtitle2"
                            style={{ textAlign: 'center', fontWeight: 'bold', margin: '30px 0px 30px 0px' }}
                        >
                            {' '}
                            Nett Premium Lifetime Average{' '}
                        </Typography>
                        <Typography variant="body1" style={{ textAlign: 'center', marginTop: '10px' }}>
                            {' '}
                            $0 spent per Month{' '}
                        </Typography>
                        {/* <Typography variant="body1" style={{ textAlign: "center", fontWeight: 'bold', marginTop: '10px' }}>  Average of $0 nett Premiums spent per Month  </Typography> */}

                        <div style={{ marginTop: '20px' }}></div>

                        <Typography
                            variant="subtitle2"
                            style={{ textAlign: 'center', marginTop: '40px', fontWeight: 'bold' }}
                        >
                            {' '}
                            Nett Premiums History{' '}
                        </Typography>

                        <Card style={{ padding: '10px', margin: '20px 5px 0px 5px' }}>
                            <Typography variant="body1" style={{ textAlign: 'left' }}>
                                {' '}
                                Cover pending approval{' '}
                            </Typography>

                            <div>
                                <Divider
                                    style={{
                                        width: '2px',
                                        padding: '10px 1px 10px 1px',
                                        float: 'left',
                                        transform: 'translate(10px,-9px)'
                                    }}
                                />
                                <Divider
                                    style={{
                                        width: '2px',
                                        padding: '10px 1px 10px 1px',
                                        float: 'right',
                                        transform: 'translate(-10px,-9px)'
                                    }}
                                />
                                <Divider style={{ margin: '20px 10px 10px 10px', padding: '2px 0px 0px 2px' }} />
                            </div>

                            <div>
                                <Typography variant="body1" style={{ float: 'left' }}>
                                    {' '}
                                    1 Oct 2019{' '}
                                </Typography>
                                <Typography variant="body1" style={{ float: 'right' }}>
                                    {' '}
                                    31 Dec 2019{' '}
                                </Typography>
                            </div>

                            <div style={{ textAlign: 'center' }}>
                                <Typography variant="body1" style={{ marginTop: '50px', display: 'inline-block' }}>
                                    {' '}
                                    $0 Premium - $0 PremiumBack{' '}
                                </Typography>
                                <Typography variant="body1" style={{ marginTop: '10px' }}>
                                    {' '}
                                    = $0 nett Premiums spent in this 3 months{' '}
                                </Typography>
                                <Typography variant="body1" style={{ marginTop: '10px' }}>
                                    {' '}
                                    = $0 nett Premium spent per Month{' '}
                                </Typography>
                            </div>
                        </Card>
                    </div>
                );
            case 1:
                return <div style={{ textAlign: 'left' }}>{this.renderClaimsHistory(policy, i)}</div>;
            case 2:
                return <div>{this.renderInsuredInfo(policy, i)}</div>;
            default:
                break;
        }
    };

    renderClaimsHistory = (policy, i) => {
        if (!this.state.accidents_and_claims) {
            return;
        }

        let returnable = [];
        let accidents = [];

        this.state.accidents_and_claims.forEach((ele) => {
            if (ele.ref_policy_id == policy.id) {
                accidents.push(ele);
            }
        });

        for (let j = accidents.length - 1; j >= 0; j--) {
            let accident = (
                <div style={{ width: '100%', onMouseOver: { backgroundColor: 'blue' } }}>
                    {j != accidents.length - 1 ? <Divider style={{ margin: '10px 0px 10px 0px' }}></Divider> : ''}
                    {/* <Divider style={{ margin: "10px 0px 10px 0px" }}></Divider> */}
                    <div
                        style={{}}
                        onClick={() => {
                            let set_state_object = {
                                submit_claim_modal_open: true,
                                submit_claim_modal_policy_details: policy,
                                submit_claim_modal_mode: 'edit_accident',
                                submit_claim_modal_edit_accident: accidents[j]
                            };
                            set_state_object.submit_claim_modal_edit_accident.index_accident = j;

                            this.setState(set_state_object);
                        }}
                    >
                        <Typography variant="body1" style={{ textAlign: 'center', fontWeight: 'bold' }}>
                            {' '}
                            Incident ID:{accidents[j].id}{' '}
                        </Typography>
                    </div>
                </div>
            );

            returnable.push(accident);

            for (let k = 0; k < accidents[j].claims.length; k++) {
                let claim = (
                    <div
                        stlye={{ paddingLeft: '30px', backgroundColor: 'green' }}
                        onClick={() => {
                            let set_state_object = {
                                submit_claim_modal_open: true,
                                submit_claim_modal_policy_details: policy,
                                submit_claim_modal_mode: 'edit_claim',
                                submit_claim_modal_edit_claim: accidents[j].claims[k]
                            };
                            set_state_object.submit_claim_modal_edit_claim.index_accident = j;
                            set_state_object.submit_claim_modal_edit_claim.index_claim = k;

                            this.setState(set_state_object, () => {});
                        }}
                    >
                        <Typography variant="body1" style={{ textAlign: 'center' }}>
                            {' '}
                            Claim ID:{accidents[j].claims[k].id}{' '}
                        </Typography>
                    </div>
                );
                returnable.push(claim);
            }
        }

        if (returnable.length === 0 || allow_claims_history === false) {
            return (
                <Typography variant="body1" style={{ textAlign: 'center', margin: '40px 0px 40px 0px' }}>
                    {' '}
                    No claims to show{' '}
                </Typography>
            );
        } else {
            return (
                <div
                    style={{
                        textAlign: 'left',
                        display: 'inline-block',
                        padding: '50px 20px 20px 20px',
                        width: '100%'
                    }}
                >
                    {returnable}
                </div>
            );
        }
    };

    renderInsuredInfo = (policy, i) => {
        if (policy.representing === 'self') {
            // if (false) {

            return <div style={{ margin: '40px 0px 40px 0px' }}>Same as user info</div>;
        } else {
            return (
                <div
                    style={{
                        width: '100%',
                        maxWidth: '500px',
                        textAlign: 'center',
                        display: 'inline-block',
                        paddingBottom: '20px'
                    }}
                >
                    {/* <div style={{ textAlign: "center", marginTop: "10px" }}>
          <Typography>User info</Typography>
        </div> */}
                    {/* <div style={{ marginTop: "20px" }}>
          </div> */}

                    <Grid
                        container
                        spacing={4}
                        style={{ padding: '15px 30px 0px 30px', margin: '10px 0px 0px 0px', wordBreak: 'break-word' }}
                    >
                        {policy.pending_rework_items && policy.pending_rework_items.length <= 0 ? (
                            ''
                        ) : (
                            <Grid item xs={12} style={{ textAlign: 'center', paddingLeft: '5px', marginTop: '5px' }}>
                                <Typography style={{ color: 'red' }}> {policy.pending_rework_message} </Typography>
                                {policy.pending_rework_items &&
                                policy.pending_rework_items.indexOf('uploaded_images') === -1 ? (
                                    ''
                                ) : (
                                    <div style={{ float: 'right' }}>
                                        {this.renderFieldEditButton(
                                            false,
                                            'policy',
                                            policy.id,
                                            'uploaded_images',
                                            'Upload Image',
                                            i
                                        )}
                                    </div>
                                )}
                            </Grid>
                        )}

                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>Relationship:</Typography>{' '}
                        </Grid>
                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>{policy.relationship_to_payer}</Typography>{' '}
                        </Grid>
                        <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            <div style={{ float: 'right' }}>
                                {this.renderFieldEditButton(
                                    false,
                                    'policy',
                                    policy.id,
                                    'relationship_to_payer',
                                    'Relationship',
                                    i
                                )}
                            </div>
                        </Grid>

                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>Payer Name:</Typography>{' '}
                        </Grid>
                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>{policy.full_name}</Typography>{' '}
                        </Grid>
                        <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            <div style={{ float: 'right' }}>
                                {this.renderFieldEditButton(false, 'policy', policy.id, 'full_name', 'Payer Name', i)}
                            </div>
                        </Grid>

                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>Preferred Name:</Typography>{' '}
                        </Grid>
                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>{policy.preferred_name}</Typography>{' '}
                        </Grid>
                        <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            <div style={{ float: 'right' }}>
                                {this.renderFieldEditButton(
                                    true,
                                    'policy',
                                    policy.id,
                                    'preferred_name',
                                    'Preferred Name',
                                    i
                                )}
                            </div>
                        </Grid>

                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>NRIC/Passport:</Typography>{' '}
                        </Grid>
                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>{policy.nric_number}</Typography>{' '}
                        </Grid>
                        <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            <div style={{ float: 'right' }}>
                                {this.renderFieldEditButton(
                                    false,
                                    'policy',
                                    policy.id,
                                    'nric_number',
                                    'NRIC Number',
                                    i
                                )}
                            </div>
                        </Grid>

                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>Gender:</Typography>{' '}
                        </Grid>
                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>{policy.gender}</Typography>{' '}
                        </Grid>
                        <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            <div style={{ float: 'right' }}>
                                {this.renderFieldEditButton(false, 'policy', policy.id, 'gender', 'Gender', i)}
                            </div>
                        </Grid>

                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>Date of Birth:</Typography>{' '}
                        </Grid>
                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>
                                {policy.dob ? GlobalHelpers.displayDate(policy.dob, 'dd-MM-yyyy') : ''}
                            </Typography>{' '}
                        </Grid>
                        <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            <div style={{ float: 'right' }}>
                                {this.renderFieldEditButton(false, 'policy', policy.id, 'dob', 'Date of Birth', i)}
                            </div>
                        </Grid>

                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>Place of Birth:</Typography>{' '}
                        </Grid>
                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>{policy.place_of_birth}</Typography>{' '}
                        </Grid>
                        <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            <div style={{ float: 'right' }}>
                                {this.renderFieldEditButton(
                                    false,
                                    'policy',
                                    policy.id,
                                    'place_of_birth',
                                    'Place of Birth',
                                    i
                                )}
                            </div>
                        </Grid>

                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>Mobile Number:</Typography>{' '}
                        </Grid>
                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>{policy.mobile_number}</Typography>{' '}
                        </Grid>
                        <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            <div style={{ float: 'right' }}>
                                {this.renderFieldEditButton(
                                    true,
                                    'policy',
                                    policy.id,
                                    'mobile_number',
                                    'Mobile Number',
                                    i
                                )}
                            </div>
                        </Grid>

                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>Email:</Typography>{' '}
                        </Grid>
                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>{policy.email}</Typography>{' '}
                        </Grid>
                        <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            <div style={{ float: 'right' }}>
                                {this.renderFieldEditButton(true, 'policy', policy.id, 'email', 'Email', i)}
                            </div>
                        </Grid>

                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>Occupation:</Typography>{' '}
                        </Grid>
                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>{policy.occupation}</Typography>{' '}
                        </Grid>
                        <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            <div style={{ float: 'right' }}>
                                {this.renderFieldEditButton(true, 'policy', policy.id, 'occupation', 'Occupation', i)}
                            </div>
                        </Grid>

                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>Employment Status:</Typography>{' '}
                        </Grid>
                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>
                                {policy.self_employed === undefined
                                    ? ''
                                    : policy.self_employed === true
                                    ? 'Self-employed'
                                    : 'Employed'}
                            </Typography>{' '}
                        </Grid>
                        <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            <div style={{ float: 'right' }}>
                                {this.renderFieldEditButton(
                                    true,
                                    'policy',
                                    policy.id,
                                    'self_employed',
                                    'Employment Status',
                                    i
                                )}
                            </div>
                        </Grid>

                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}> PREMIUMBACK paid to:</Typography>{' '}
                        </Grid>
                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>
                                {' '}
                                {Constants.bank_full_name_from_short_name[policy.bank_premiumback.split(' - ')[0]] +
                                    ' - ' +
                                    policy.bank_premiumback.split(' - ')[1]}{' '}
                                {policy.bank_premiumback_to_payer ? '(same as payer)' : ''}{' '}
                            </Typography>{' '}
                        </Grid>
                        <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            <div style={{ float: 'right' }}>
                                {/* {this.renderFieldEditButton(false, "policy", policy.id, "bank_premiumback", "Bank (premiumback)", i)} */}
                            </div>
                        </Grid>

                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}> CLAIMS paid to:</Typography>{' '}
                        </Grid>
                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>
                                {' '}
                                {Constants.bank_full_name_from_short_name[policy.bank_claims.split(' - ')[0]] +
                                    ' - ' +
                                    policy.bank_claims.split(' - ')[1]}{' '}
                                {policy.bank_claims_to_payer ? '(same as payer)' : ''}
                            </Typography>{' '}
                        </Grid>
                        <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            <div style={{ float: 'right' }}>
                                {this.renderFieldEditButton(
                                    true,
                                    'policy',
                                    policy.id,
                                    'bank_claims',
                                    'Bank (claims)',
                                    i
                                )}
                            </div>
                        </Grid>
                    </Grid>

                    {/* <div style={{ textAlign: "center", margin: "20px 0px 20px 0px" }}>
            <Button onClick={() => { this.handleChangePasswordClicked() }}>Change Password</Button>
          </div> */}
                </div>
            );
        }
    };

    renderPolicyContent = (type) => {
        const { policyGroupBy } = this.state;
        const data = policyGroupBy[type];
        if (data && data.length > 0) {
            let renderable = [];
            const renderPolicyStatusHistory = (status_history) => {
                let renderable = [];
                for (let i = 0; i < status_history.length; i++) {
                    let date = GlobalHelpers.displayDate(status_history[i].created_at, 'dd-MM-yyyy HH:mm');
                    let status = status_history[i].status;
                    let history_item = (
                        <Grid container item spacing={2}>
                            <GridEmptySpace xs={'hidden'} sm={2} md={2} lg={2} />
                            <Grid item xs={9} sm={5} md={5} lg={5}>
                                <Typography align="left" style={{ height: '38px', lineHeight: '38px' }}>
                                    {date} - {convertStatusHistoryStatusCodeToEvent[status]}{' '}
                                </Typography>
                            </Grid>
                        </Grid>
                    );
                    renderable.push(history_item);
                }
                return renderable;
            };

            const policyExpansionPanel = (i) => {
                let policy = data[i];
                return (
                    <ExpansionPanel expanded={this.getPolicyExpanded(i)} style={{ cursor: 'auto' }}>
                        <ExpansionPanelSummary>
                            <Grid container spacing={1}>
                                <Grid container item spacing={0}>
                                    <Grid item xs={12} sm={12} md={12} lg={12}>
                                        <Typography fontSize="14px">
                                            Your credit card / debit card will only be billed{' '}
                                            <b>after your application is Approved</b>.
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Grid container item spacing={0}>
                                    <Grid item xs={12} sm={12} md={12} lg={12}>
                                        <Typography fontSize="14px">
                                            <b>
                                                {policy.product_details.name} (ID:{policy.id})
                                            </b>
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Grid container item spacing={2}>
                                    <GridEmptySpace xs={'hidden'} sm={2} md={2} lg={2} />
                                    <Grid item xs={6} sm={3} md={3} lg={3}>
                                        <Typography
                                            align="left"
                                            fontSize="14px"
                                            style={{ height: '38px', lineHeight: '38px' }}
                                        >
                                            Life Insured name:
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={6} sm={6} md={6} lg={6}>
                                        <Typography
                                            align="left"
                                            fontSize="14px"
                                            style={{ height: '38px', lineHeight: '38px' }}
                                        >
                                            {policy.full_name}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Grid container item spacing={2}>
                                    <GridEmptySpace xs={'hidden'} sm={2} md={2} lg={2} />
                                    <Grid item xs={6} sm={3} md={3} lg={3}>
                                        <Typography
                                            align="left"
                                            fontSize="14px"
                                            style={{ height: '38px', lineHeight: '38px' }}
                                        >
                                            NRIC/Passport:
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={6} sm={6} md={6} lg={6}>
                                        <Typography
                                            align="left"
                                            fontSize="14px"
                                            style={{ height: '38px', lineHeight: '38px' }}
                                        >
                                            {policy.nric_number}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Grid container item spacing={2}>
                                    <GridEmptySpace xs={'hidden'} sm={2} md={2} lg={2} />
                                    <Grid item xs={6} sm={3} md={3} lg={3}>
                                        <Typography align="left" fontSize="14px" style={{ marginTop: 8 }}>
                                            Status:
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={3} sm={3} md={3} lg={3}>
                                        <Typography
                                            align="left"
                                            fontSize="14px"
                                            style={{ marginTop: 8, color: getStatusColor[policy.status] }}
                                        >
                                            <b>{convertStatusCodeToStatus[policy.status]}</b>
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={3} sm={2} md={2} lg={2}>
                                        <Switch
                                            checked={this.getSwitchChecked(i)}
                                            onChange={this.handleSwitchChange(i)}
                                            disabled={this.getSwitchDisabled(i)}
                                            color="primary"
                                        />
                                    </Grid>
                                </Grid>
                                <Grid container item spacing={2}>
                                    <GridEmptySpace xs={'hidden'} sm={2} md={2} lg={2} />
                                    <Grid item xs={6} sm={3} md={3} lg={3}>
                                        <Typography align="left" fontSize="14px">
                                            Sum assured:
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={3} sm={3} md={3} lg={3}>
                                        <Typography align="left" fontSize="14px">
                                            {getSumAssuredText(policy.product)}
                                        </Typography>
                                        <div style={{ width: '100%', marginTop: '10px' }}>
                                            {policy.product == 'small_pmd_protect' ? (
                                                <a
                                                    style={{
                                                        textDecoration: 'none',
                                                        float: 'left',
                                                        position: 'absolute',
                                                        left: '0px'
                                                    }}
                                                    href="mailto:adele@ifinancialsingapore.com?subject=I changed to a new PMD&body=Hi iFinSG,
                          %0D%0A
                          %0D%0A
                          %0D%0AI wish to change my PMD protected with iFinSG.
                          %0D%0A
                          %0D%0AHere are my new PMD details:
                          %0D%0A
                          %0D%0ABrand:
                          %0D%0AModel:
                          %0D%0AColor:
                          %0D%0ASerial number:
                          %0D%0ALTA number:
                          %0D%0AUL number:
                          %0D%0A
                          %0D%0A( Attach photos of the front and back of your NRIC, so that we know it's really you )
                          %0D%0A
                          %0D%0A( Attach photos of the purchase Receipts of your new PMD purchase )
                          %0D%0A
                          %0D%0A* If you attach photos of the Receipts of your new PMD purchase that took place within the past 48 hours, you will be allowed to make claims within the 1st 30 days of Approved cover for any new PMD registered with us.
                          %0D%0A
                          "
                                                >
                                                    <Button
                                                        style={{
                                                            backgroundColor: 'rgba(0, 0, 0, 0.75)',
                                                            opacity: 0.7,
                                                            color: 'white'
                                                        }}
                                                        onClick={() => {}}
                                                    >
                                                        {' '}
                                                        <span style={{ lineHeight: 1.1 }}>
                                                            Changed
                                                            <br />
                                                            PMD?
                                                        </span>
                                                    </Button>
                                                </a>
                                            ) : (
                                                ''
                                            )}
                                        </div>
                                    </Grid>
                                    <Grid item xs={3} sm={2} md={2} lg={2}>
                                        <Button
                                            style={{ backgroundColor: 'red', color: 'white', marginTop: '5px' }}
                                            onClick={() => {
                                                this.handleSubmitClaimClicked(policy);
                                            }}
                                        >
                                            <span style={{ lineHeight: 1.1 }}>
                                                Submit
                                                <br />
                                                Claim
                                            </span>
                                        </Button>
                                    </Grid>
                                    <GridEmptySpace xs={6} sm={5} md={5} lg={5} />
                                    <Grid item xs={6} sm={7} md={7} lg={7} align="left">
                                        <Button
                                            style={{ backgroundColor: 'blue', color: 'white' }}
                                            onClick={() => {
                                                this.handlePolicyDetailsClicked(policy.product);
                                            }}
                                        >
                                            {' '}
                                            <span style={{ lineHeight: 1.1 }}>
                                                Policy
                                                <br />
                                                Details
                                            </span>{' '}
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid container item spacing={2}>
                                <GridEmptySpace xs={'hidden'} sm={2} md={2} lg={2} />
                                <Grid item xs={6} sm={3} md={3} lg={3}>
                                    <Typography
                                        fontSize="24px"
                                        align="left"
                                        style={{ height: '38px', lineHeight: '38px' }}
                                    >
                                        <b>Policy History</b>
                                    </Typography>
                                </Grid>
                            </Grid>
                            {renderPolicyStatusHistory(policy.status_history)}
                            <Grid container item spacing={0}>
                                <Grid item xs={12} sm={12} md={12} lg={12}>
                                    <IconButton
                                        onClick={() => {
                                            this.togglePolicyExpand(i);
                                        }}
                                    >
                                        {!policy.expanded ? (
                                            <ExpandMoreIcon style={{ color: Constants.color.green }} />
                                        ) : (
                                            <ExpandMoreIcon
                                                style={{ color: Constants.color.green, transform: 'rotate(180deg)' }}
                                            />
                                        )}
                                    </IconButton>
                                </Grid>
                            </Grid>
                        </ExpansionPanelSummary>
                        {/* <PolicyHistoryWrapper> */}
                        <div style={{ boxSizing: 'border-box', padding: '0 10px' }}>
                            <Grid container item spacing={2}>
                                <GridEmptySpace xs={'hidden'} sm={2} md={2} lg={2} />
                                <Grid item xs={12} sm={8} md={8} lg={8}>
                                    <ExpansionPanelDetails>
                                        <Card>
                                            <AppBar position="static" color="default">
                                                <Tabs
                                                    value={this.state.policyTab}
                                                    onChange={this.handlePolicyTabChange}
                                                    indicatorColor="primary"
                                                    textColor="primary"
                                                    variant="fullWidth"
                                                >
                                                    <Tab style={{ wordBreak: 'normal' }} label="Accounts Summary" />
                                                    <Tab style={{ wordBreak: 'normal' }} label="Claims History" />
                                                    <Tab style={{ wordBreak: 'normal' }} label="LIFE Insured Info" />
                                                </Tabs>
                                            </AppBar>
                                            <TabContainer>{this.renderPolicyTab(policy, i)}</TabContainer>
                                            <IconButton
                                                onClick={() => {
                                                    this.togglePolicyExpand(i);
                                                }}
                                            >
                                                <ExpandLessIcon />
                                            </IconButton>
                                        </Card>
                                    </ExpansionPanelDetails>
                                </Grid>
                            </Grid>
                        </div>
                        {/* </PolicyHistoryWrapper> */}
                    </ExpansionPanel>
                );
            };
            data.sort((a, b) => {
                return b.id - a.id;
            });
            console.log('sort *******', data);
            for (let i = 0; i < data.length; i++) {
                renderable.push(policyExpansionPanel(i));
            }
            return renderable;
        } else {
            const no_policy_message = (
                <Typography style={{ textAlign: 'center', margin: '40px 0px 40px 0px', width: '100%' }}>
                    {' '}
                    No policies{' '}
                </Typography>
            );
            return <div style={{ minWidth: '300px' }}>{no_policy_message}</div>;
        }
    };

    renderPolicies = () => {
        const { policies } = this.state;
        const renderPolicyDetailsOrPayerInfo = (representing) => {
            return (
                <div>
                    <AppBar position="static" color="default" style={{ backgroundColor: 'white', width: '100%' }}>
                        <Tabs
                            value={this.state.tab}
                            onChange={this.handleTabChange}
                            indicatorColor="secondary"
                            textColor="secondary"
                            variant="fullWidth"
                            style={{ backgroundColor: 'white' }}
                        >
                            <Tab
                                label="Policy Info"
                                // {this.state.user_info && this.state.user_info.policies ?
                                //   // `Policy Info (${this.state.user_info.policies.length})`
                                //   `Policy Info <div>|</div>`
                                //   :
                                //    "Policy Info"
                                //    }
                                style={{ width: '50%', borderRight: '2px solid lightgrey' }}
                            />

                            <Tab label="Payer Info" style={{ width: '50%' }} />
                        </Tabs>
                    </AppBar>
                    <SwipeableViews
                        disabled
                        axis={'x'} // 'x-reverse'
                        index={this.state.tab}
                        onChangeIndex={this.handleTabChangeIndex}
                        style={{ textAlign: 'center' }}
                    >
                        <TabContainer className="tab container">{this.renderPolicyContent(representing)}</TabContainer>
                        <TabContainer className="tab container 1111">
                            {this.renderUserInfoTab(representing)}
                        </TabContainer>
                    </SwipeableViews>
                </div>
            );
        };

        return (
            <Card style={{ width: '100%', margin: 'auto' }}>
                <AppBar position="static" color="default">
                    <Tabs
                        value={this.state.groupTab}
                        onChange={(event, value) => {
                            this.setState({ groupTab: value });
                        }}
                        indicatorColor="none"
                        textColor="primary"
                        variant="fullWidth"
                    >
                        <Tab icon={<EmojiPeopleIcon />} label="SELF" />
                        <Tab icon={<ChildFriendlyIcon />} label="FAMILY" />
                        <Tab icon={<BusinessIcon />} label="COMPANY" />
                    </Tabs>
                </AppBar>
                <SwipeableViews
                    disabled
                    axis={'x'} // 'x-reverse'
                    index={this.state.groupTab}
                    onChangeIndex={(index) => {
                        this.setState({ groupTab: index });
                    }}
                    style={{ textAlign: 'center' }}
                >
                    <TabContainer>
                        {renderPolicyDetailsOrPayerInfo('self')}
                        {/* {this.renderPolicyContent('self')} */}
                    </TabContainer>
                    <TabContainer>
                        {renderPolicyDetailsOrPayerInfo('family')}
                        {/* {this.renderPolicyContent('family')} */}
                    </TabContainer>
                    <TabContainer>
                        {renderPolicyDetailsOrPayerInfo('entity')}
                        {/* {this.renderPolicyContent('entity')} */}
                    </TabContainer>
                </SwipeableViews>
            </Card>
        );
    };
    handleSubmitClaimFieldChange = (i, prop) => (e) => {
        const fields_with_type_number = [
            'personal_mobile_number',
            'entity_uen',
            'personal_bank_acct_number',
            'mobile_number',
            'premiumback_bank_acct_number',
            'claims_bank_acct_number'
        ];
        if (fields_with_type_number.includes(prop)) {
            if ('' === e.target.value || (isFinite(e.target.value) && !e.target.value.includes('.'))) {
                // Only allows integers in type strings or int to go through
                // Do nothing
            } else {
                return;
            }
        }

        let policies = JSON.parse(JSON.stringify(this.state.user_info.policies));
        policies[i].submit_claim[prop] = e.target.value;
        this.setState({
            policies
        });
    };

    handleCloseModalChangePassword = () => {
        this.setState({ change_password_modal_open: false });
    };

    handleChangePasswordClicked = () => {
        this.setState({ change_password_modal_open: true, enable_change_password_button: true });
    };

    getAccidentsAndClaims = () => {
        const token = localStorage.getItem('ifinsg_token');
        if (token) {
            axios
                .post('/api/getAccidentsAndClaims', {
                    token,
                    username: this.state.user_info.username
                })
                .then((res) => {
                    if (res.status === 200) {
                        if (res.data.err) {
                            alert(res.data.err);
                        } else {
                            this.setState({
                                accidents_and_claims: res.data.accidents_and_claims
                            });
                        }
                    } else {
                    }
                })
                .catch((err) => {
                    console.log(err);
                });
        } else {
        }
    };
    handleChangePassword = (password_old, password_new) => {
        const token = localStorage.getItem('ifinsg_token');
        if (token) {
            axios
                .post('/api/auth/changePassword', {
                    token,
                    password_old,
                    password_new
                })
                .then((res) => {
                    if (res.status === 200) {
                        if (res.data.err) {
                            alert(res.data.err);
                            this.setState({
                                enable_change_password_button: this.state.enable_change_password_button + 1
                            });
                        } else {
                            try {
                                this.handleCloseModalChangePassword();
                                alert('Password change successfully');
                            } catch (err) {
                                alert('ERROR - Password change failed');
                            }
                        }
                    } else {
                        alert('ERROR - Password change failed');
                    }
                    this.setState({ change_password_modal_open: false, enable_change_password_button: true });
                })
                .catch((err) => {
                    alert('ERROR - Password change failed');
                    this.setState({ change_password_modal_open: false, enable_change_password_button: true });
                });
        } else {
            alert('Sorry, your session has expired. Please login again and retry');
            this.setState({ change_password_modal_open: false, enable_change_password_button: true });
        }
    };

    handleCloseModalEditField = () => {
        this.setState({ edit_field_modal_open: false });
    };

    handleEditFieldClicked = (user_or_policy, username_or_id, field_name, field_title, i) => {
        // Setting states for modal to rceive
        this.setState({
            edit_field_modal_open: true,
            enable_edit_field_button: true,
            edit_field_user_or_policy: user_or_policy,
            edit_field_username_or_id: username_or_id,
            edit_field_name: field_name,
            edit_field_title: field_title,
            edit_field_i: i
        });
    };

    handleEditField = (password, user_or_policy, username_or_id, field_name, new_value, i) => {
        const token = localStorage.getItem('ifinsg_token');
        if (!password) {
            alert('Password not filled in');
            return;
        }
        if (!user_or_policy) {
            console.log('ERROR - user_or_policy is empty!');
            alert('Something went wrong :/');
            return;
        }
        if (!username_or_id) {
            console.log('ERROR - username_or_id is empty!');
            alert('Something went wrong :/');
            return;
        }
        if (!field_name) {
            console.log('ERROR - field_name is empty!');
            alert('Something went wrong. field_name empty :/');
            return;
        }
        if (field_name === 'self_employed' || field_name === 'medical_registrar_smc') {
        } else {
            if (!new_value) {
                alert('Something went wrong with new_value ' + new_value + ':/');
                return;
            }
        }
        if (token) {
            axios
                .post('/api/editField', {
                    token,
                    username: this.state.user_info.username_norm,
                    password,
                    user_or_policy,
                    username_or_id,
                    field_name,
                    new_value
                })
                .then((res) => {
                    if (res.status === 200) {
                        if (res.data.err) {
                            alert(res.data.err);
                            this.setState({
                                enable_change_password_button: this.state.enable_change_password_button + 1
                            });
                        } else {
                            try {
                                this.handleCloseModalChangePassword();
                                alert('Field changed successfully');
                                let user_info = this.state.user_info;
                                if (user_or_policy === 'user') {
                                    user_info[field_name] = new_value;
                                    if (field_name === 'bank') {
                                        user_info.bank = new_value[0];
                                        user_info.bank_acct_number = new_value[1];
                                    }
                                } else {
                                    let index_policy = i;
                                    if (field_name === 'bank') {
                                        user_info.policies[index_policy].bank = new_value[0];
                                        user_info.policies[index_policy].bank_acct_number = new_value[1];
                                    } else if (field_name === 'bank_claims') {
                                        user_info.policies[index_policy].bank_claims = new_value[0];
                                        user_info.policies[index_policy].bank_acct_number_claims = new_value[1];
                                    } else if (field_name === 'bank_premiumback') {
                                        user_info.policies[index_policy].bank_premiumback = new_value[0];
                                        user_info.policies[index_policy].bank_acct_number_premiumback = new_value[1];
                                    } else {
                                        user_info.policies[index_policy][field_name] = new_value;
                                    }
                                }

                                let pending_rework_items_new = [];
                                if (user_or_policy === 'user') {
                                    pending_rework_items_new = user_info.pending_rework_items;
                                } else {
                                    pending_rework_items_new = user_info.policies[i].pending_rework_items;
                                }

                                var index = pending_rework_items_new.indexOf(field_name);
                                if (index > -1) {
                                    pending_rework_items_new.splice(index, 1);
                                    if (user_or_policy === 'user') {
                                        user_info.pending_rework_items = pending_rework_items_new;
                                        if (pending_rework_items_new.length === 0) {
                                            user_info.policies.forEach((ele) => {
                                                ele.status = 'pending_rework_approval';
                                            });
                                        }
                                    } else {
                                        user_info.policies[i].pending_rework_items = pending_rework_items_new;
                                        if (pending_rework_items_new.length === 0) {
                                            let make_all_pending_rework_approval = true;
                                            user_info.policies.forEach((ele) => {
                                                if (ele.pending_rework_items && ele.pending_rework_items.length > 0) {
                                                    make_all_pending_rework_approval = false;
                                                }
                                            });

                                            if (make_all_pending_rework_approval) {
                                                user_info.policies.forEach((ele) => {
                                                    ele.status = 'pending_rework_approval';
                                                });
                                            }
                                            // Check if all others policies are also cleared
                                            user_info.policies[i].status = 'pending_rework_approval';
                                        }
                                    }
                                }
                                this.setState({ user_info });
                            } catch (err) {
                                alert('ERROR - Field change failed');
                            }
                        }
                    } else {
                        alert('ERROR - Field change failed');
                    }
                    this.setState({ edit_field_modal_open: false, enable_edit_field_button: true });
                })
                .catch((err) => {
                    alert('ERROR - Field change failed');
                    this.setState({ edit_field_modal_open: false, enable_edit_field_button: true });
                });
        } else {
            alert('Sorry, your session has expired. Please login again and retry');
            this.setState({ edit_field_modal_open: false, enable_edit_field_button: true });
        }
    };

    renderFieldEditButton = (always_allow, user_or_policy, username_or_id, field_name, field_title, i) => {
        if (!allow_edit_field) {
            return '';
        } else {
            try {
                let to_rework = false;
                if (user_or_policy === 'user') {
                    to_rework = this.state.user_info.pending_rework_items.indexOf(field_name) !== -1;
                } else {
                    to_rework = this.state.user_info.policies[i].pending_rework_items.indexOf(field_name) !== -1;
                }

                if (always_allow || to_rework) {
                    let fontWeight = false;
                    let color = '';
                    let button_text = 'Edit';

                    if (field_name === 'uploaded_images') {
                        button_text = 'Upload';
                    }
                    if (to_rework) {
                        color = 'red';
                    }

                    return (
                        <Button
                            onClick={() => {
                                this.handleEditFieldClicked(user_or_policy, username_or_id, field_name, field_title, i);
                            }}
                            style={{
                                fontSize: '14px',
                                fontWeight: 'unset',
                                padding: 0,
                                minWidth: '40px',
                                height: '20px',
                                lineHeight: 0
                            }}
                        >
                            <span style={{ color: color, fontWeight: fontWeight }}> {button_text} </span>
                        </Button>
                    );
                }
            } catch (err) {}
        }
    };

    handleCheckboxToggle = (name, policy_index, submit_claim) => (event) => {
        let setStateObject = { [name]: event.target.checked };

        if (policy_index || policy_index === 0) {
            let policies = this.state.user_info.policies;
            if (submit_claim) {
                policies[policy_index].submit_claim[name] = event.target.checked;
            } else {
                policies[policy_index][name] = event.target.checked;
            }
            setStateObject = { policies };
        } else {
            // Do nothing
        }
        this.setState(setStateObject);
    };

    fileUploadedClaim = (i, uploaded_file_name) => {
        let policies = JSON.parse(JSON.stringify(this.state.user_info.policies));
        policies[i].submit_claim.uploaded_images.push(uploaded_file_name);

        this.setState({
            policies
        });
    };

    fileRemovedClaim = (i, uploaded_file_name) => {
        let policies = JSON.parse(JSON.stringify(this.state.user_info.policies));
        policies[i].submit_claim.uploaded_images.splice(
            policies[i].submit_claim.uploaded_images.indexOf(uploaded_file_name),
            1
        ); // Remove uploaded_file_name from the array policies[i].submit_claim.uploaded_images
        this.setState({
            policies
        });
    };

    renderUserInfoTab = (representing) => {
        if (!this.state.user_info) return;
        if (representing == 'entity') {
            if (!(this.state.user_info && this.state.user_info.company_details)) {
                return;
            }
            return (
                <div style={{ boxSizing: 'border-box', padding: '20px' }}>
                    <Grid container spacing={3}>
                        {this.state.user_info &&
                        this.state.user_info.pending_rework_items &&
                        this.state.user_info.pending_rework_items.length <= 0 ? (
                            ''
                        ) : (
                            <Grid item xs={12} style={{ textAlign: 'center', paddingLeft: '5px', marginTop: '5px' }}>
                                <Typography style={{ color: 'red' }}>
                                    {' '}
                                    {this.state.user_info.pending_rework_message}{' '}
                                </Typography>
                                {this.state.user_info &&
                                this.state.user_info.pending_rework_items &&
                                this.state.user_info.pending_rework_items.indexOf('uploaded_images') === -1 ? (
                                    ''
                                ) : (
                                    <div style={{ float: 'right' }}>
                                        {this.renderFieldEditButton(
                                            true,
                                            'user',
                                            this.state.user_info.username_norm,
                                            'uploaded_images',
                                            'Upload Image'
                                        )}
                                    </div>
                                )}
                            </Grid>
                        )}
                        <Grid container item spacing={0}>
                            <GridEmptySpace xs={'hidden'} sm={2} md={2} lg={2} />
                            <Grid
                                item
                                xs={6}
                                sm={4}
                                md={4}
                                lg={4}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>Username:</Typography>{' '}
                            </Grid>
                            <Grid
                                item
                                xs={6}
                                sm={6}
                                md={6}
                                lg={6}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>
                                    {this.state.user_info.username}
                                </Typography>{' '}
                            </Grid>
                        </Grid>
                        <Grid container item spacing={0}>
                            <GridEmptySpace xs={'hidden'} sm={2} md={2} lg={2} />
                            <Grid
                                item
                                xs={6}
                                sm={4}
                                md={4}
                                lg={4}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>Company Name:</Typography>{' '}
                            </Grid>
                            <Grid
                                item
                                xs={6}
                                sm={6}
                                md={6}
                                lg={6}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>
                                    {this.state.user_info.company_details.name}
                                </Typography>{' '}
                            </Grid>
                        </Grid>

                        <Grid container item spacing={0}>
                            <GridEmptySpace xs={'hidden'} sm={2} md={2} lg={2} />
                            <Grid
                                item
                                xs={6}
                                sm={4}
                                md={4}
                                lg={4}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>Company UEN:</Typography>{' '}
                            </Grid>
                            <Grid
                                item
                                xs={6}
                                sm={6}
                                md={6}
                                lg={6}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>
                                    {this.state.user_info.company_details.uen}
                                </Typography>{' '}
                            </Grid>
                        </Grid>

                        <Grid container item spacing={0}>
                            <GridEmptySpace xs={'hidden'} sm={2} md={2} lg={2} />
                            <Grid
                                item
                                xs={6}
                                sm={4}
                                md={4}
                                lg={4}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>Representative's Name:</Typography>{' '}
                            </Grid>
                            <Grid
                                item
                                xs={6}
                                sm={6}
                                md={6}
                                lg={6}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>
                                    {this.state.user_info.company_details.rep_name}
                                </Typography>{' '}
                            </Grid>
                        </Grid>

                        <Grid container item spacing={0}>
                            <GridEmptySpace xs={'hidden'} sm={2} md={2} lg={2} />
                            <Grid
                                item
                                xs={6}
                                sm={4}
                                md={4}
                                lg={4}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>Representative’s Job Title:</Typography>{' '}
                            </Grid>
                            <Grid
                                item
                                xs={6}
                                sm={6}
                                md={6}
                                lg={6}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>
                                    {this.state.user_info.company_details.rep_job_title}
                                </Typography>{' '}
                            </Grid>
                        </Grid>

                        <Grid container item spacing={0}>
                            <GridEmptySpace xs={'hidden'} sm={2} md={2} lg={2} />
                            <Grid
                                item
                                xs={6}
                                sm={4}
                                md={4}
                                lg={4}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>
                                    Representative’s NRIC/Passport:
                                </Typography>{' '}
                            </Grid>
                            <Grid
                                item
                                xs={6}
                                sm={6}
                                md={6}
                                lg={6}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>
                                    {this.state.user_info.company_details.rep_nric}
                                </Typography>{' '}
                            </Grid>
                        </Grid>

                        <Grid container item spacing={0}>
                            <GridEmptySpace xs={'hidden'} sm={2} md={2} lg={2} />
                            <Grid
                                item
                                xs={6}
                                sm={4}
                                md={4}
                                lg={4}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>
                                    Representative’s Mobile Number:
                                </Typography>{' '}
                            </Grid>
                            <Grid
                                item
                                xs={6}
                                sm={6}
                                md={6}
                                lg={6}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>
                                    {this.state.user_info.company_details.tel}
                                </Typography>{' '}
                            </Grid>
                        </Grid>

                        <Grid container item spacing={0}>
                            <GridEmptySpace xs={'hidden'} sm={2} md={2} lg={2} />
                            <Grid
                                item
                                xs={6}
                                sm={4}
                                md={4}
                                lg={4}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>Representative’s Email:</Typography>{' '}
                            </Grid>
                            <Grid
                                item
                                xs={6}
                                sm={6}
                                md={6}
                                lg={6}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>
                                    {this.state.user_info.company_details.rep_email}
                                </Typography>{' '}
                            </Grid>
                        </Grid>

                        <Grid container item spacing={0}>
                            <GridEmptySpace xs={'hidden'} sm={2} md={2} lg={2} />
                            <Grid
                                item
                                xs={6}
                                sm={4}
                                md={4}
                                lg={4}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>Name of Owner:</Typography>{' '}
                            </Grid>
                            <Grid
                                item
                                xs={6}
                                sm={6}
                                md={6}
                                lg={6}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>
                                    {this.state.user_info.company_details.owner_name}
                                </Typography>{' '}
                            </Grid>
                        </Grid>

                        <Grid container item spacing={0}>
                            <GridEmptySpace xs={'hidden'} sm={2} md={2} lg={2} />
                            <Grid
                                item
                                xs={6}
                                sm={4}
                                md={4}
                                lg={4}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>Mobile Number of Owner:</Typography>{' '}
                            </Grid>
                            <Grid
                                item
                                xs={6}
                                sm={6}
                                md={6}
                                lg={6}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>
                                    {this.state.user_info.company_details.owner_tel}
                                </Typography>{' '}
                            </Grid>
                        </Grid>

                        <Grid container item spacing={0}>
                            <GridEmptySpace xs={'hidden'} sm={2} md={2} lg={2} />
                            <Grid
                                item
                                xs={6}
                                sm={4}
                                md={4}
                                lg={4}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>Email of Owner:</Typography>{' '}
                            </Grid>
                            <Grid
                                item
                                xs={6}
                                sm={6}
                                md={6}
                                lg={6}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>
                                    {this.state.user_info.company_details.owner_email}
                                </Typography>{' '}
                            </Grid>
                        </Grid>

                        <Grid container item spacing={0}>
                            <GridEmptySpace xs={'hidden'} sm={2} md={2} lg={2} />
                            <Grid
                                item
                                xs={6}
                                sm={4}
                                md={4}
                                lg={4}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>Bank:</Typography>{' '}
                            </Grid>
                            <Grid
                                item
                                xs={6}
                                sm={6}
                                md={6}
                                lg={6}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>
                                    {this.state.user_info.company_details.bank}
                                </Typography>{' '}
                            </Grid>
                        </Grid>

                        <Grid container item spacing={0}>
                            <GridEmptySpace xs={'hidden'} sm={2} md={2} lg={2} />
                            <Grid
                                item
                                xs={6}
                                sm={4}
                                md={4}
                                lg={4}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>Company Bank Account Number:</Typography>{' '}
                            </Grid>
                            <Grid
                                item
                                xs={6}
                                sm={6}
                                md={6}
                                lg={6}
                                style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}
                            >
                                {' '}
                                <Typography style={{ fontSize: '15px' }}>
                                    {this.state.user_info.company_details.bank_acct_number}
                                </Typography>{' '}
                            </Grid>
                        </Grid>
                        <Grid item xs={12} sm={12} md={12} lg={12} alignContent="center">
                            <Button
                                onClick={() => {
                                    this.handleChangePasswordClicked();
                                }}
                            >
                                Change Password
                            </Button>
                        </Grid>
                    </Grid>
                </div>
            );
        }
        return (
            <div
                style={{
                    width: '100%',
                    maxWidth: '500px',
                    textAlign: 'center',
                    display: 'inline-block'
                }}
            >
                <Grid
                    container
                    spacing={4}
                    style={{ padding: '15px 30px 0px 30px', margin: '10px 0px 0px 0px', wordBreak: 'break-word' }}
                >
                    {this.state.user_info &&
                    this.state.user_info.pending_rework_items &&
                    this.state.user_info.pending_rework_items.length <= 0 ? (
                        ''
                    ) : (
                        <Grid item xs={12} style={{ textAlign: 'center', paddingLeft: '5px', marginTop: '5px' }}>
                            <Typography style={{ color: 'red' }}>
                                {' '}
                                {this.state.user_info.pending_rework_message}{' '}
                            </Typography>
                            {this.state.user_info &&
                            this.state.user_info.pending_rework_items &&
                            this.state.user_info.pending_rework_items.indexOf('uploaded_images') === -1 ? (
                                ''
                            ) : (
                                <div style={{ float: 'right' }}>
                                    {this.renderFieldEditButton(
                                        true,
                                        'user',
                                        this.state.user_info.username_norm,
                                        'uploaded_images',
                                        'Upload Image'
                                    )}
                                </div>
                            )}
                        </Grid>
                    )}
                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>Username:</Typography>{' '}
                    </Grid>
                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>{this.state.user_info.username}</Typography>{' '}
                    </Grid>
                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>Payer Name:</Typography>{' '}
                    </Grid>
                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>{this.state.user_info.full_name}</Typography>{' '}
                    </Grid>
                    <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        <div style={{ float: 'right' }}>
                            {this.renderFieldEditButton(
                                false,
                                'user',
                                this.state.user_info.username_norm,
                                'full_name',
                                'Payer Name'
                            )}
                        </div>
                    </Grid>
                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>NRIC/Passport:</Typography>{' '}
                    </Grid>
                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>{this.state.user_info.nric_number}</Typography>{' '}
                    </Grid>
                    <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        <div style={{ float: 'right' }}>
                            {this.renderFieldEditButton(
                                false,
                                'user',
                                this.state.user_info.username_norm,
                                'nric_number',
                                'NRIC Number'
                            )}
                        </div>
                    </Grid>
                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>Preferred Name:</Typography>{' '}
                    </Grid>
                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>{this.state.user_info.preferred_name}</Typography>{' '}
                    </Grid>
                    <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        <div style={{ float: 'right' }}>
                            {this.renderFieldEditButton(
                                true,
                                'user',
                                this.state.user_info.username_norm,
                                'preferred_name',
                                'Preferred Name'
                            )}
                        </div>
                    </Grid>
                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>Gender:</Typography>{' '}
                    </Grid>
                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>{this.state.user_info.gender}</Typography>{' '}
                    </Grid>
                    <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        <div style={{ float: 'right' }}>
                            {this.renderFieldEditButton(
                                false,
                                'user',
                                this.state.user_info.username_norm,
                                'gender',
                                'Gender'
                            )}
                        </div>
                    </Grid>
                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>Date of Birth:</Typography>{' '}
                    </Grid>
                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>
                            {this.state.user_info.dob ? GlobalHelpers.displayDate(this.state.user_info.dob) : ''}
                        </Typography>{' '}
                    </Grid>
                    <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        <div style={{ float: 'right' }}>
                            {this.renderFieldEditButton(
                                false,
                                'user',
                                this.state.user_info.username_norm,
                                'dob',
                                'Date of Birth'
                            )}
                        </div>
                    </Grid>
                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>Place of Birth:</Typography>{' '}
                    </Grid>
                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>{this.state.user_info.place_of_birth}</Typography>{' '}
                    </Grid>
                    <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        <div style={{ float: 'right' }}>
                            {this.renderFieldEditButton(
                                false,
                                'user',
                                this.state.user_info.username_norm,
                                'place_of_birth',
                                'Place of Birth'
                            )}
                        </div>
                    </Grid>

                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>Mobile Number:</Typography>{' '}
                    </Grid>
                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>{this.state.user_info.mobile_number}</Typography>{' '}
                    </Grid>
                    <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        <div style={{ float: 'right' }}>
                            {this.renderFieldEditButton(
                                true,
                                'user',
                                this.state.user_info.username_norm,
                                'mobile_number',
                                'Mobile Number'
                            )}
                        </div>
                    </Grid>

                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>Occupation:</Typography>{' '}
                    </Grid>
                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>{this.state.user_info.occupation}</Typography>{' '}
                    </Grid>
                    <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        <div style={{ float: 'right' }}>
                            {this.renderFieldEditButton(
                                true,
                                'user',
                                this.state.user_info.username_norm,
                                'occupation',
                                'Occupation'
                            )}
                        </div>
                    </Grid>

                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>Employment Status:</Typography>{' '}
                    </Grid>
                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>
                            {this.state.user_info.self_employed === undefined
                                ? ''
                                : this.state.user_info.self_employed === true
                                ? 'Self-employed'
                                : 'Employed'}
                        </Typography>{' '}
                    </Grid>
                    <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        <div style={{ float: 'right' }}>
                            {this.renderFieldEditButton(
                                true,
                                'user',
                                this.state.user_info.username_norm,
                                'self_employed',
                                'Employment Status'
                            )}
                        </div>
                    </Grid>

                    <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        {' '}
                        <Typography style={{ fontSize: '15px' }}>Bank:</Typography>{' '}
                    </Grid>
                    {this.state.user_info.bank && (
                        <Grid item xs={5} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                            {' '}
                            <Typography style={{ fontSize: '15px' }}>
                                {Constants.bank_full_name_from_short_name[this.state.user_info.bank.split(' - ')[0]] +
                                    ' - ' +
                                    this.state.user_info.bank.split(' - ')[1]}
                            </Typography>{' '}
                        </Grid>
                    )}
                    <Grid item xs={2} style={{ textAlign: 'left', paddingLeft: '5px', marginTop: '5px' }}>
                        <div style={{ float: 'right' }}>
                            {this.renderFieldEditButton(
                                true,
                                'user',
                                this.state.user_info.username_norm,
                                'bank',
                                'Bank'
                            )}
                        </div>
                    </Grid>
                </Grid>
                <div style={{ textAlign: 'center', margin: '20px 0px 20px 0px' }}>
                    <Button
                        onClick={() => {
                            this.handleChangePasswordClicked();
                        }}
                    >
                        Change Password
                    </Button>
                </div>
            </div>
        );
    };

    renderModals = () => {
        return (
            <div>
                <ModalConfirmCancelOrReinstate
                    open={this.state.modal_confirm_cancel_or_reinstate_open}
                    handleClose={() => {
                        this.setState({ modal_confirm_cancel_or_reinstate_open: false });
                    }}
                    data={this.state.modal_confirm_cancel_or_reinstate_data}
                    onSuccess={() => {
                        this.getUserPolicies();
                    }}
                    username={this.state.user_info.username}
                />

                <ModalPolicyDetails
                    open={this.state.policy_details_modal_open}
                    handleClose={() => {
                        this.setState({ policy_details_modal_open: false });
                    }}
                    policy={this.state.policy_details_modal_policy}
                    referrer={this.state.referrer}
                />

                <ModalChangePassword
                    open={this.state.change_password_modal_open}
                    handleClose={() => {
                        this.handleCloseModalChangePassword();
                    }}
                    handleChangePassword={this.handleChangePassword}
                    enable_change_password_button={this.state.enable_change_password_button}
                />

                <ModalEditField
                    open={this.state.edit_field_modal_open}
                    handleClose={() => {
                        this.handleCloseModalEditField();
                    }}
                    handleEditField={this.handleEditField}
                    enable_edit_field_button={this.state.enable_edit_field_button}
                    user_or_policy={this.state.edit_field_user_or_policy}
                    username_or_id={this.state.edit_field_username_or_id}
                    field_name={this.state.edit_field_name}
                    field_title={this.state.edit_field_title}
                    i={this.state.edit_field_i}
                    username={this.state.user_info.username}
                />
            </div>
        );
    };

    renderContent = () => {
        return (
            <>
                {this.renderModals()}
                <div
                    style={{
                        margin: '10px 0',
                        fontSize: '40px',
                        fontFamily: 'Merriweather'
                    }}
                >
                    Profile page
                </div>
                <Card>{this.renderPolicies()}</Card>
            </>
        );
    };

    getMuiTheme = () => {
        return createMuiTheme({
            palette: {
                primary: lightGreen,
                secondary: {
                    main: '#177eff'
                }
            },
            overrides: {
                MuiExpansionPanelDetails: {
                    root: {
                        display: 'block',
                        padding: '0'
                    }
                },
                MuiTypography: {
                    root: {
                        padding: '0px!important'
                    }
                    // body1: {
                    //   padding: '0px!important'
                    // }
                },
                MuiExpansionPanelSummary: {
                    root: {
                        padding: '0 12px'
                    },
                    content: {
                        display: 'block'
                    }
                }
            }
        });
    };

    render() {
        const { classes } = this.props;
        return this.state.isLoading ? (
            'Loading...'
        ) : (
            <div>
                <Head>
                    <link href="/filepicker.css" rel="stylesheet" />
                    <link href="/dropzone.min.css" rel="stylesheet" />
                </Head>
                <TopNavBar user_info={this.state.user_info} referrer={this.state.referrer} />
                <NextSeo config={Constants.next_seo_config('profile')} />
                <NoSsr>
                    <ModalSubmitClaimForm
                        open={this.state.submit_claim_modal_open}
                        handleClose={() => {
                            this.setState({ submit_claim_modal_open: false });
                        }}
                        mode={this.state.submit_claim_modal_mode}
                        edit_accident={this.state.submit_claim_modal_edit_accident}
                        edit_claim={this.state.submit_claim_modal_edit_claim}
                        accidents_and_claims={this.state.accidents_and_claims}
                        // policy={this.state.policy_details_modal_policy}
                        // referrer={this.props.referrer}
                        policy_details={this.state.submit_claim_modal_policy_details}
                        getAccidentsAndClaims={this.getAccidentsAndClaims}
                        username={
                            this.state.user_info && this.state.user_info.username_norm
                                ? this.state.user_info.username_norm
                                : ''
                        }
                    />

                    <ModalConfirmCancelOrReinstate
                        open={this.state.modal_confirm_cancel_or_reinstate}
                        handleClose={() => {
                            this.setState({ submit_claim_modal_open: false });
                        }}
                        mode={this.state.submit_claim_modal_mode}
                    />
                </NoSsr>

                <MuiThemeProvider theme={this.getMuiTheme()}>
                    <div className={classNames(classes.align_center)}>
                        <div className={classNames(classes.page_text_width, classes.page_padding, classes.break_word)}>
                            {this.renderContent()}
                        </div>
                    </div>
                </MuiThemeProvider>

                <BotNavBar referrer={this.state.referrer} />
            </div>
        );
    }
}

export default withStyles(styles)(Profile);

const PolicyHistoryWrapper = styled.div`
    width: 600px;
    margin: 0 auto 40px auto;
    @media (max-width: 360px) {
        width: 100%;
    }
`;
