import React, { Component } from 'react';

import { NoSsr, Button, Typography, IconButton, AppBar, Toolbar } from '@material-ui/core';

// import Logo from './ifinsg_logo.png'
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo

import TopNavBar from './components/TopNavBar'
import BotNavBar from './components/BotNavBar'
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Constants from 'constants/constants';
import Head from 'next/head';


const styles = {
  page_width: Constants.style.page.width,
  page_text_width: Constants.style.page.text_width,

  page_padding: Constants.style.page.padding,

  align_center: Constants.style.align.center,
  align_left: Constants.style.align.left,

  bold: Constants.style.text.bold,
  break_word: Constants.style.text.break_word,

  title1: Constants.style.text.title1,
  body1: Constants.style.text.body1,

  margin_top_xl: Constants.style.margin_top.xl,
  margin_top_lg: Constants.style.margin_top.lg,
  margin_top_md: Constants.style.margin_top.md,
  margin_top_sm: Constants.style.margin_top.sm,
  margin_top_xs: Constants.style.margin_top.xs,
  margin_top_xxs: Constants.style.margin_top.xxs,
}

class QR extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }


  componentDidMount = () => {
    Constants.store_referrer(this)
  }


  renderQRCode = () => {

    if (this.state.referrer) {

      const images = [
        {
          original: 'http://api.qrserver.com/v1/create-qr-code/?color=000000&amp;bgcolor=FFFFFF&amp;data=wef&amp;qzone=1&amp;margin=0&amp;size=400x400&amp;ecc=L',
          thumbnail: 'http://api.qrserver.com/v1/create-qr-code/?color=000000&amp;bgcolor=FFFFFF&amp;data=wef&amp;qzone=1&amp;margin=0&amp;size=400x400&amp;ecc=L',
        },
      ]

      let url = "ifinsg.com/"
      url = url + this.state.referrer

      return (

        <div style={{ width: "100%", textAlign: "center" }}>
          {/* Source: http://goqr.me/  WARNING: Actual URL for editing is not same as <img src> url. Check network to get actual URL */}
          <img style={{ width: "100%" }} src={"http://api.qrserver.com/v1/create-qr-code/?color=000000&bgcolor=FFFFFF&data=" + url + "&qzone=1&margin=0&size=400x400&ecc=L"} alt="qr code" />

          {/* <a href= { "https://" + url}>
            <Typography> <u> {"https://" + url} </u> </Typography>
          </a>
           */}
        </div>


      )
    } else {
      return (
        <div>
          No QR extension found :(
        </div>
      )
    }

  }
  render() {
    const { classes } = this.props;

    return (
      <div style={{ width: "100%", height: "100%" }}>
        <TopNavBar referrer={this.state.referrer} />
        <NextSeo config={Constants.next_seo_config("qr")} />
        <NoSsr>

          <script dangerouslySetInnerHTML={{      // How to add dangerouslySetInnerHTML: https://github.com/zeit/next.js/issues/1413
            __html: `
            <object data="https://www.google.com" width="600" height="400">
    <embed src="https://www.ifinsg.com" width="600" height="400"> </embed>
    Error: Embedded data could not be displayed.
</object>
         `}} />




          <div className={classNames(classes.align_center)}>
            <div className={classNames(classes.page_width, classes.page_padding, classes.break_word, classes.align_left)}>
              <div style={{ marginTop: "130px" }}> </div>
              {this.renderQRCode()}
            </div>
          </div>

        </NoSsr>
        <BotNavBar referrer={this.state.referrer} />

      </div>
    );
  }
}


export default withStyles(styles)(QR);