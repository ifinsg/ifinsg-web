import React, { Component } from 'react';
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
// import { NoSsr, Card, CardMedia, CardContent, Button, Grid, TextField } from '@material-ui/core';
import { NoSsr, Button, Typography, TextField, FormControl, InputLabel, MenuItem, Select, Card, CardContent, FormControlLabel, Checkbox, Fab, Divider, IconButton, Grid, FormLabel, RadioGroup, Radio, Chip } from '@material-ui/core';

import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import TopNavBar from './components/TopNavBar'
import BotNavBar from './components/BotNavBar'
import Router from 'next/router';
import axios from 'axios'

import 'url-search-params-polyfill';      //Source: https://www.npmjs.com/package/url-search-params-polyfill
import Head from 'next/head';

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Constants from 'constants/constants';
import GlobalConstants from '../global/constants';
var owasp = require('owasp-password-strength-test');
import Cookies from 'universal-cookie';
const cookie = new Cookies();

const styles = {
  page_width: Constants.style.page.width,
  page_padding: Constants.style.page.padding,

  align_center: Constants.style.align.center,

  bold: Constants.style.text.bold,
  break_word: Constants.style.text.break_word,

  title1: Constants.style.text.title1,
  body1: Constants.style.text.body1,

  margin_top_xl: Constants.style.margin_top.xl,
  margin_top_lg: Constants.style.margin_top.lg,
  margin_top_md: Constants.style.margin_top.md,
  margin_top_sm: Constants.style.margin_top.sm,
  margin_top_xs: Constants.style.margin_top.xs,
  margin_top_xxs: Constants.style.margin_top.xxs,
}




owasp.config({
  allowPassphrases: false,
  maxLength: 50,
  minLength: 6,
  minPhraseLength: 20,
  minOptionalTestsToPass: 4,
});


class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      forget_password_verification_code: "",
      password_new: "",
      password_new_retype: "",
      reset_password_button_disabled: false,
    };
  }

  componentDidMount = () => {

    Constants.store_referrer(this)


    const urlParamsRaw = new URLSearchParams(window.location.search);
    let forget_password_verification_code = urlParamsRaw.get("code")
    this.setState({
      forget_password_verification_code
    })


  }

  handleGetEmailLink = () => {
    const login_email = this.state.login_email
    if (!validator.validate(login_email)) {
      alert("Error - Invalid email")
      return
    }

    this.setState({ get_email_button_disabled: true }, () => {
      // Send to server
      axios.post( "/api/auth/getForgetPasswordEmailLink", {
        login_email,
      }).then(res => {
        if (res.status === 200) {
          console.log("POST code 200 OK")
          if (res.data.err) {
            alert(res.data.err)
            this.setState({ login_button_disabled: false })
          } else {

            try {
              // console.log(res.data)
              cookie.set('ifinsg_token', res.data.token, { path: '/' })
              localStorage.setItem('ifinsg_token', res.data.token)
              Router.push(Constants.get_new_page("profile", this.state.referrer))

            } catch (err) {
              console.log(err)
              console.log("Unexpected params received from api/auth/login")
              this.setState({ login_button_disabled: false })
            }
          }
        } else {
          console.log("ERROR - ", res)
          this.setState({ login_button_disabled: false })
        }
      }).catch(err => {
        console.log(err);
        this.setState({ login_button_disabled: false })
      });

    })


  }


  handleFieldChange = (prop) => e => {
    this.setState({
      [prop]: e.target.value
    });
  }


  getMuiTheme = () => {
    return createMuiTheme({
      overrides: {
        MuiPaper: {
          elevation4: {
            boxShadow: "0px 1px 1px 0px rgba(0, 0, 0, 0.1), 0px 0px 0px 0px rgba(0, 0, 0, 0.1), 0px 0px 1px 0px rgba(0, 0, 0, 0.1)" //Reduce the perceived elevation
          },
          rounded: {
            borderRadius: "0"
          }
        },
      }
    });
  }


  handleResetPassword = () => {
    console.log("handleResetPassword()")
    const forget_password_verification_code = this.state.forget_password_verification_code
    const password_new = this.state.password_new
    const password_new_retype = this.state.password_new_retype

    if (password_new === "") {
      alert("Error - Please enter a password")
      return
    } else if (password_new_retype === "") {
      alert("Error - Please re-enter your password")
      return
    }

    const password_strength = owasp.test(password_new);
    console.log(password_strength)

    if (password_strength.requiredTestErrors.length > 0) {
      alert(password_strength.requiredTestErrors[0])
      return
    } else if (password_strength.optionalTestErrors.length > 2) {
      alert(password_strength.optionalTestErrors[0])
      return
    }

    if (password_new !== password_new_retype) {
      alert("Error - Passwords do not match")
      return
    }


    this.setState({
      reset_password_button_disabled: true
    }, () => {
      // Send to server
      axios.post( "/api/auth/resetPassword", {
        forget_password_verification_code,
        password_new
      }).then(res => {
        if (res.status === 200) {
          if (res.data.err) {
            alert(res.data.err)     // Eg: ERROR - Email already in use
            if (res.data.err_code === "reset_password_code_expired") {
              Router.push(Constants.get_new_page("forgot-password", this.state.referrer))
              
            }
          } else {
            // console.log(res.data)
            alert("Pasword changed successfully")
            Router.push(Constants.get_new_page("login", this.state.referrer))
          }
        } else {
          console.log("ERROR - ", res)
        }
      }).catch(err => {
        console.log(err);
      });

    })


  }

  renderResetPasswordFields = () => {
    let renderable = []

    const password_new =
      <div style={{ padding: "0px 0px 2px 0px" }}>
        <TextField
          label="New password"
          value={this.state.password_new}
          onChange={this.handleFieldChange('password_new')}
          type="password"
          fullWidth
        />
      </div>

    const password_new_retype =
      <div style={{ padding: "0px 0px 2px 0px" }}>
        <TextField
          label="Re-type your new password"
          onKeyPress={(ev) => { if (ev.key === 'Enter') { this.handleResetPassword() } }}
          value={this.state.password_new_retype}
          onChange={this.handleFieldChange('password_new_retype')}
          type="password"
          fullWidth
        />
      </div>

    const reset_password_button =
      <Button style={{ float: "right", margin: "10px 0px 0px 0px" }} disabled={this.state.reset_password_button_disabled} onClick={() => { this.handleResetPassword() }}> Reset Password </Button>



    renderable = [password_new, password_new_retype, reset_password_button]


    return (renderable)

  }

  renderContent = () => {

    return (
      <div style={{ minWidth: "270px" }}>
        <div style={{ textAlign: "left" }}> <Typography variant="h6"> Reset Password </Typography>
        </div>
        {/* <Typography variant="subtitle1" style={{ marginTop: "5px", lineHeight: 1.2 }}> A reset password link will be sent to you via your registered email </Typography> */}

        {this.renderResetPasswordFields()}

        <div style={{ marginTop: "50px" }}></div>

      </div>
    )

  }


  render() {
    const { classes } = this.props;

    return (
      <div>
        <TopNavBar referrer={this.state.referrer} />

        <NextSeo config={Constants.next_seo_config("reset-password")} />

        <NoSsr>

          <MuiThemeProvider theme={this.getMuiTheme()}>

            {/* <Grid container spacing={0} direction="column" align="center"> */}
            {/* <Grid container spacing={0} > */}

            <div className={classNames(classes.align_center)}>
              <div className={classNames(classes.page_width, classes.page_padding, classes.break_word)}>

                <div style={{ marginTop: "100px" }}></div>

                <CardContent style={{ paddingBottom: "60px" }}>
                  {this.renderContent()}

                </CardContent>


                {/* </Grid>

              <Grid item xs={0} sm={3} md={4} lg={4} style={{ marginTop: "50px" }} />

            </Grid> */}


              </div>
            </div>


          </MuiThemeProvider>

        </NoSsr>


        <BotNavBar referrer={this.state.referrer}/>

      </div>
    );
  }
}


export default withStyles(styles)(Login);