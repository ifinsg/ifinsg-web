import React, { Component } from 'react';

import { NoSsr, Grid } from '@material-ui/core';
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import TopNavBar from './components/TopNavBar';
import BotNavBar from './components/BotNavBar';

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Constants from 'constants/constants';
import Head from 'next/head';

const styles = {
    page_width: Constants.style.page.width,
    page_text_width: Constants.style.page.text_width,

    page_padding: Constants.style.page.padding,

    align_center: Constants.style.align.center,

    bold: Constants.style.text.bold,
    break_word: Constants.style.text.break_word,

    title1: Constants.style.text.title1,
    body1: Constants.style.text.body1,

    margin_top_xl: Constants.style.margin_top.xl,
    margin_top_lg: Constants.style.margin_top.lg,
    margin_top_md: Constants.style.margin_top.md,
    margin_top_sm: Constants.style.margin_top.sm,
    margin_top_xs: Constants.style.margin_top.xs,
    margin_top_xxs: Constants.style.margin_top.xxs
};

class Resources extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const { classes } = this.props;

        return (
            <div>
                <TopNavBar selected="resources" referrer={this.state.referrer} />
                <NextSeo config={Constants.next_seo_config('resources')} />
                <NoSsr>
                    <div data-d1={'d1'} className={classNames(classes.align_center)}>
                        <div
                            data-d2={'d2'}
                            className={classNames(classes.page_text_width, classes.page_padding, classes.break_word)}
                        >
                            <div
                                style={{
                                    margin: '10px 0',
                                    fontSize: '40px',
                                    fontWeight: 'bold',
                                    fontFamily: 'Merriweather'
                                }}
                            >
                                iConsumerForms
                            </div>
                            <div style={{ padding: '20px 20px 50px 20px', textAlign: 'center' }}>
                                <Grid container spacing={24} alignItems="stretch">
                                    <Grid item xs={6} md={4} alignItems="stretch">
                                        <a
                                            href={'https://cws.prudential.com.sg/en/our-services/customer-forms/'}
                                            target={'blank'}
                                        >
                                            <img
                                                style={{
                                                    maxWidth: '200px',
                                                    width: '100%',
                                                    margin: '0px 0px 0px 0px',
                                                    padding: '0px 0px 0px 0px'
                                                }}
                                                src={'/img/PRU.jpg'}
                                            />
                                        </a>
                                    </Grid>
                                    <Grid item xs={6} md={4} alignItems="stretch">
                                        <a
                                            href={'https://www.income.com.sg/policy-downloads-and-forms'}
                                            target={'blank'}
                                        >
                                            <img
                                                style={{
                                                    maxWidth: '200px',
                                                    width: '100%',
                                                    margin: '0px 0px 0px 0px',
                                                    padding: '0px 0px 0px 0px'
                                                }}
                                                src={'/img/NTUC.jpg'}
                                            />
                                        </a>
                                    </Grid>
                                    <Grid item xs={6} md={4} alignItems="stretch">
                                        <a
                                            href={'https://www.tokiomarine.com/sg/en/personal/resources/forms.html'}
                                            target={'blank'}
                                        >
                                            <img
                                                style={{
                                                    maxWidth: '200px',
                                                    width: '100%',
                                                    margin: '0px 0px 0px 0px',
                                                    padding: '0px 0px 0px 0px'
                                                }}
                                                src={'/img/Tokiomarine.jpg'}
                                            />
                                        </a>
                                    </Grid>
                                    <Grid item xs={6} md={4} alignItems="stretch">
                                        <a
                                            href={'https://www.aia.com.sg/en/help-support/form-library.html'}
                                            target={'blank'}
                                        >
                                            <img
                                                style={{
                                                    maxWidth: '200px',
                                                    width: '100%',
                                                    margin: '0px 0px 0px 0px',
                                                    padding: '0px 0px 0px 0px'
                                                }}
                                                src={'/img/AIA.png'}
                                            />
                                        </a>
                                    </Grid>
                                    <Grid item xs={6} md={4} alignItems="stretch">
                                        <a href={'https://www.manulife.com.sg/self-serve.html'} target={'blank'}>
                                            <img
                                                style={{
                                                    maxWidth: '200px',
                                                    width: '100%',
                                                    margin: '0px 0px 0px 0px',
                                                    padding: '0px 0px 0px 0px'
                                                }}
                                                src={'/img/Manulife.png'}
                                            />
                                        </a>
                                    </Grid>
                                    <Grid item xs={6} md={4} alignItems="stretch">
                                        <a
                                            href={
                                                'https://www.aviva.com.sg/en/downloads/life-health-savings-retirement/'
                                            }
                                            target={'blank'}
                                        >
                                            <img
                                                style={{
                                                    maxWidth: '200px',
                                                    width: '100%',
                                                    margin: '0px 0px 0px 0px',
                                                    padding: '0px 0px 0px 0px'
                                                }}
                                                src={'/img/AVIVA.png'}
                                            />
                                        </a>
                                    </Grid>
                                    <Grid item xs={6} md={4} alignItems="stretch">
                                        <a
                                            href={
                                                'https://www.greateasternlife.com/sg/en/personal-insurance/get-help/customer-service.html'
                                            }
                                            target={'blank'}
                                        >
                                            <img
                                                style={{
                                                    maxWidth: '200px',
                                                    width: '100%',
                                                    margin: '0px 0px 0px 0px',
                                                    padding: '0px 0px 0px 0px'
                                                }}
                                                src={'/img/GE.png'}
                                            />
                                        </a>
                                    </Grid>
                                    <Grid item xs={6} md={4} alignItems="stretch">
                                        <a href={'https://www.axa.com.sg/customer-care/contact-us'} target={'blank'}>
                                            <img
                                                style={{
                                                    maxWidth: '200px',
                                                    width: '100%',
                                                    margin: '0px 0px 0px 0px',
                                                    padding: '0px 0px 0px 0px'
                                                }}
                                                src={'/img/AXA.png'}
                                            />
                                        </a>
                                    </Grid>
                                    <Grid item xs={6} md={4} alignItems="stretch">
                                        <a
                                            href={'https://www.insurance.hsbc.com.sg/help/download-forms/'}
                                            target={'blank'}
                                        >
                                            <img
                                                style={{
                                                    maxWidth: '200px',
                                                    width: '100%',
                                                    margin: '0px 0px 0px 0px',
                                                    padding: '0px 0px 0px 0px'
                                                }}
                                                src={'/img/HSBC.png'}
                                            />
                                        </a>
                                    </Grid>
                                </Grid>
                            </div>
                            <div
                                style={{
                                    padding: '50px 0px 0px 0px',
                                    fontSize: '30px',
                                    lineHeight: '1',
                                    fontWeight: 'bold',
                                    fontFamily: 'Merriweather'
                                }}
                            >
                                {' '}
                                <u> iSearchProfessional </u>{' '}
                            </div>
                            <div style={{ padding: '20px 20px 50px 50px', textAlign: 'center' }}>
                                <Grid container>
                                    <Grid item xs={6} md={4} alignItems="stretch">
                                        <a href={'/i-search-professional'} target={'blank'}>
                                            <img
                                                style={{
                                                    maxWidth: '300px',
                                                    width: '100%',
                                                    margin: '0px 0px 0px 0px',
                                                    padding: '0px 0px 0px 0px'
                                                }}
                                                src={'/img/iSearchProfessional.png'}
                                            />
                                        </a>
                                    </Grid>
                                </Grid>
                            </div>
                            <div
                                style={{
                                    padding: '50px 0px 0px 0px',
                                    fontSize: '30px',
                                    lineHeight: '1',
                                    fontWeight: 'bold',
                                    fontFamily: 'Merriweather'
                                }}
                            >
                                {' '}
                                <u> SMC: Singapore Medical Council </u>{' '}
                            </div>
                            <div style={{ padding: '20px 20px 50px 50px', textAlign: 'center' }}>
                                <Grid container>
                                    <Grid item xs={6} md={4} alignItems="stretch">
                                        <a
                                            href={'https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=SMC'}
                                            target={'blank'}
                                        >
                                            <img
                                                style={{
                                                    maxWidth: '300px',
                                                    width: '100%',
                                                    margin: '0px 0px 0px 0px',
                                                    padding: '0px 0px 0px 0px'
                                                }}
                                                src={'/img/SMC.png'}
                                            />
                                        </a>
                                    </Grid>
                                </Grid>
                            </div>
                            <div
                                style={{
                                    padding: '50px 0px 0px 0px',
                                    fontSize: '30px',
                                    lineHeight: '1',
                                    fontWeight: 'bold',
                                    fontFamily: 'Merriweather'
                                }}
                            >
                                {' '}
                                <u>TCMPB: Traditional Chinese Medicine Practitioners Board </u>{' '}
                            </div>
                            <div style={{ padding: '20px 20px 50px 50px', textAlign: 'center' }}>
                                <Grid container>
                                    <Grid item xs={6} md={4} alignItems="stretch">
                                        <a
                                            href={'https://prs.moh.gov.sg/prs/internet/profSearch/main.action?hpe=TCM'}
                                            target={'blank'}
                                        >
                                            <img
                                                style={{
                                                    maxWidth: '300px',
                                                    width: '100%',
                                                    margin: '0px 0px 0px 0px',
                                                    padding: '0px 0px 0px 0px'
                                                }}
                                                src={'/img/TCMPB.jpg'}
                                            />
                                        </a>
                                    </Grid>
                                </Grid>
                            </div>
                        </div>
                    </div>
                </NoSsr>
                <BotNavBar referrer={this.state.referrer} />
            </div>
        );
    }
}

export default withStyles(styles)(Resources);
