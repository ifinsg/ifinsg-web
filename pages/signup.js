import React, { Component } from 'react';

import {
    NoSsr,
    Button,
    Typography,
    AppBar,
    Tabs,
    Tab,
    TextField,
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    Card,
    FormControlLabel,
    Checkbox,
    Fab,
    Divider,
    IconButton,
    Grid,
    FormLabel,
    RadioGroup,
    Radio,
    Chip
} from '@material-ui/core';

import CancelIcon from '@material-ui/icons/Cancel';
import { emphasize } from '@material-ui/core/styles/colorManipulator';

import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import DeleteIcon from '@material-ui/icons/Delete';
import CloseIcon from '@material-ui/icons/Close';

import AddIcon from '@material-ui/icons/Add';
import Cookies from 'universal-cookie';
import Router, { withRouter } from 'next/router';

const cookie = new Cookies();

import SwipeableViews from 'react-swipeable-views';
import { scrollToElement } from '../components/signup/utils';
import { BrowserView, MobileView, isBrowser, isMobile } from 'react-device-detect';
const { detect } = require('detect-browser');
const browser = detect();

import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import TopNavBar from './components/TopNavBar';
import BotNavBar from './components/BotNavBar';
import SelectOccupation from './components/selects/SelectOccupation';
import SelectCountry from './components/selects/SelectCountry';
import Tick from 'components/icons/Tick';
import LoadingModal from './components/modals/LoadingModal';

import axios from 'axios';

import Head from 'next/head';

// Image upload handlling
import ImageUpload from './components/widgets/ImageUpload';
// import html2canvas from "html2canvas"; //
import FormData from 'form-data';
// import { saveAs } from 'file-saver';

import StripeCheckout from 'react-stripe-checkout'; // Source: https://www.npmjs.com/package/react-stripe-checkout

import GlobalHelpers from 'global/helpers';
import GlobalConstants from 'global/constants';

import Constants from 'constants/constants';
import BackProtect from './components/widgets/BackProtect';
import { Link, Element, Events } from 'react-scroll';
import groupBy from 'lodash/groupBy';

import 'url-search-params-polyfill'; //Source: https://www.npmjs.com/package/url-search-params-polyfill

var validator = require('email-validator'); //Source: https://www.npmjs.com/package/email-validator
import DatePickerRMC from 'components/DatePickerRMC/DatePickerRMC'; // Adapted from: https://github.com/react-component/m-date-picker
import { runInThisContext } from 'vm';
import ModalMessage from './components/modals/ModalMessage';

// import DateFnsUtils from '@date-io/date-fns';
// import { MuiPickersUtilsProvider, TimePicker, DatePicker } from 'material-ui-pickers';

var owasp = require('owasp-password-strength-test');

var input_refs = {
    // personal_nric_number: "",
    // email_verification_field: ""
};

owasp.config({
    allowPassphrases: false,
    maxLength: 50,
    minLength: 6,
    minPhraseLength: 20,
    minOptionalTestsToPass: 4
});

// const assumeNewUser = true
const assumeNewUser = false;

let promotion_name = '';

const price_get = (policy, price_component, period, line_break) => {
    let price_original = -1;
    let price_promotion = -1;
    let on_promotion = false;

    switch (price_component) {
        case 'premium':
            price_original = Constants.product_details[policy].premium;
            if (Constants.product_details[policy].premium_on_promotion) {
                price_promotion = Constants.product_details[policy].premium_promotional_price;
                on_promotion = true;
            }
            break;
        case 'admin_fee':
            price_original = Constants.product_details[policy].admin_fee;
            if (Constants.product_details[policy].admin_fee_on_promotion) {
                price_promotion = Constants.product_details[policy].admin_fee_promotional_price;
                on_promotion = true;
            }
            break;
        case 'total':
            price_original = Constants.product_details[policy].total;
            if (Constants.product_details[policy].total_on_promotion) {
                price_promotion = Constants.product_details[policy].total_promotional_price;
                on_promotion = true;
            }
            break;
        default:
            break;
    }
    if (on_promotion) {
        return price_promotion * period;
    } else {
        return price_original * period;
    }
};

const sources = ['Our Facebook page', 'Facebook Ads', 'Flyer (Home)', 'Flyer (Outdoors)', 'Clinics', 'Others'];

let backProtect = false;
/////////////////////////////////// Development Settings ///////////////////////////////////

const do_not_validate = false;
// const do_not_validate = true

const do_not_login_via_token = false;
// const do_not_login_via_token = true

/////////////////////////////////// Constants ///////////////////////////////////

const bank_full_name_from_short_name = Constants.bank_full_name_from_short_name;

const get_products_on_sale = () => {
    let returnable = [];
    Constants.products_on_sale.forEach((product) => {
        if (product.availability === 'available') {
            returnable.push(product.policy);
        } else if (product.policy == 'small_pmd_protect') {
            returnable.push(product.policy);
        }
    });
    return returnable;
};

const makeNum2DecimalPlace = (number) => {
    return parseFloat(number).toFixed(2);
};

function TabContainer({ children, dir }) {
    return (
        <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
            {children}
        </Typography>
    );
}

class Mobile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalMessageOpen: false,
            /////////////////////////////////// Styles / Dynamics ///////////////////////////////////
            style_login: { visibility: 'visible' },
            style_representing: { visibility: 'collapse' },
            style_pmd_details: { visibility: 'collapse' },

            // style_personal_info: { visibility: "collapse" },
            style_personal_info: {
                tableLayout: 'fixed',
                visibility: 'collapse',
                width: '100%',
                margin: '1px 0px 0px 0px',
                display: 'contents'
            },
            style_insured_info: {
                tableLayout: 'fixed',
                visibility: 'collapse',
                width: '100%',
                margin: '1px 0px 0px 0px'
            },
            style_confirm: { visibility: 'collapse' },
            expanded_login: true,
            expanded_representing: false,
            expanded_pmd_details: false,
            expanded_personal_info: false,
            expanded_insured_info: false,
            expanded_confirm: false,
            /////////////////////////////////// Component-level ///////////////////////////////////
            product_catalog: get_products_on_sale(),
            products: [],
            policies: {},
            total_per_pax: 0,
            total_grand: 0,
            loading_modal_open: false,
            unload_protect_active: true,
            /////////////////////////////////// Login ///////////////////////////////////
            loginTab: 0, // Default display tab for login/signup
            login_email: '',
            login_password: '',
            // login_email: "",
            signup_password: '',
            signup_password_retype: '',
            signup_button_disabled: false,
            show_email_verification_field: false,
            email_verification_code: '',
            verify_email_button_disabled: false,
            resend_verification_code_button_disabled: true,
            login_button_disabled: false,
            expansion_panel_login_disabled: false,
            personal_info_already_in_db: false,
            /////////////////////////////////// Personal Info ///////////////////////////////////
            company_details: null,
            personal_full_name: '',
            personal_preferred_name: '',
            personal_nric_number: '',
            personal_gender: '',
            personal_dob: '',
            personal_place_of_birth: '',
            personal_mobile_number: '',
            personal_occupation: '',
            personal_self_employed: false,
            personal_uploaded_images: [],
            /////////////////////////////////// Representing ///////////////////////////////////
            representingTab: 0, // 0=self, 1=entity, 2=family, 3=group
            representing: 'self',
            ack_pdpa: true,
            personal_bank: '',
            personal_bank_acct_number: '',
            personal_ack_50_cents_fee: false,
            entity_name: '',
            entity_uen: '',
            entity_claims_pay_to_same: true, // Not fixed
            family_claims_pay_to_same: true, // Not fixed
            /////////////////////////////////// Insured Info ///////////////////////////////////
            num_of_insured: 0,
            /////////////////////////////////// Confirm ///////////////////////////////////
            confirm_terms_and_conditions: false,
            // confirm_marketing_consent: false,
            image_upload_signup_screenshot: '',
            confirm_button_pressed: false,
            source: '',
            source_others: '',
            isAddNewMember: false,
            boughtPolicies: null
        };
    }

    /////////////////////////////////////////////////////////
    ////////////////////  Component Panel ///////////////////
    /////////////////////////////////////////////////////////

    handleCloseLoadingModal = () => {
        this.setState({
            loading_modal_open: false
        });
    };

    screenshotSignupPage() {
        return new Promise((resolve, reject) => {
            try {
                const input = document.getElementById('signup_page');
                const html2canvas = require('html2canvas');
                html2canvas(input).then((canvas) => {
                    try {
                        canvas.toBlob(
                            (blob) => {
                                let data = new FormData();
                                const file_name = 'signup-screenshot.jpeg';
                                data.append('file', blob, file_name);
                                axios
                                    .post('/api/image/upload', data, {
                                        headers: {
                                            accept: 'application/json',
                                            'Accept-Language': 'en-US,en;q=0.8',
                                            'Content-Type': `multipart/form-data; boundary=${data._boundary}`,
                                            'Content-Type': 'image/jpeg',
                                            username: this.state.login_email,
                                            image_type: 'SIGNUP-SCREENSHOT'
                                        }
                                    })
                                    .then((res) => {
                                        if (res.data) {
                                            this.setState(
                                                {
                                                    image_upload_signup_screenshot: res.data
                                                },
                                                () => {
                                                    resolve();
                                                }
                                            );
                                        } else {
                                            reject('ERROR - Successful upload, but unable to save image file name');
                                        }
                                    })
                                    .catch((err) => {
                                        reject(err);
                                    });
                            },
                            'image/jpeg',
                            0.2
                        ); // JPEG at 10% quality
                    } catch (err) {
                        alert('Something went wrong :/');
                    }
                });
            } catch (err) {
                alert('Something went wrong :/');
            }
        });
    }

    parsedPolicies = (user_info = {}) => {
        const { policies } = user_info;
        const result = {};
        if (policies) {
            const productGroupBy = groupBy(policies, 'product');
            const productGroupByKeys = Object.keys(productGroupBy);
            for (const productName of productGroupByKeys) {
                const policyArray = productGroupBy[productName];
                const policyGroupBy = groupBy(policyArray, 'representing');
                result[productName] = policyGroupBy;
            }
        }
        return result;
    };

    getUserData = (token) => {
        axios
            .post('/api/auth/getUserDataFromToken', {
                token
            })
            .then((res) => {
                const { status, data } = res || {};
                if (status === 200) {
                    if (!data.err) {
                        try {
                            this.setState(
                                {
                                    // Save user_info to state
                                    personal_info_already_in_db: data.personal_info_already_in_db,
                                    user_info: data.user_info,
                                    policies: this.parsedPolicies(data.user_info),
                                    // Setting login_email to username from DB
                                    login_email: data.user_info.username,

                                    // Disabling expansion panel
                                    expansion_panel_login_disabled: true
                                },
                                () => {
                                    this.nextPanelAfterSignup();
                                }
                            );
                        } catch (err) {
                            console.log(err);
                        }
                    }
                }
            })
            .catch((err) => {});
    };

    loginViaToken = () => {
        try {
            if (!do_not_login_via_token) {
                const token = localStorage.getItem('ifinsg_token');
                if (token) {
                    this.getUserData(token);
                } else {
                }
            }
        } catch (err) {}
    };

    componentDidMount = () => {
        if (browser && browser.name) {
            if (Constants.browsers_supported.indexOf(browser.name) != -1) {
                // ok
            } else {
                // alert("For your best viewing experience, please use the Chrome browser :)")
            }
        }

        Constants.store_referrer(this);

        this.loginViaToken();

        // Getting and validating url params. If successful, handleAddInsured(). If unsuccessful, redirect back to /products
        try {
            const urlParamsRaw = new URLSearchParams(window.location.search);
            let urlParams = urlParamsRaw.get('products');
            let urlParamsJSON = JSON.parse(urlParams);

            if (urlParamsJSON.length < 1) {
                Router.push(Constants.get_new_page('products', this.state.referrer));
            } else {
                let valid_urlParamsJSON = true;

                for (let i = 0; i < urlParamsJSON.length; i++) {
                    if (this.state.product_catalog.indexOf(urlParamsJSON[i]) >= 0) {
                        // If the all items in the url param are valid products, then carry on
                        // do nothing
                    } else {
                        valid_urlParamsJSON = false;
                        break;
                    }
                }

                if (valid_urlParamsJSON) {
                    this.setState({
                        products: urlParamsJSON
                    });
                } else {
                    Router.push(Constants.get_new_page('products', this.state.referrer));
                }
            }
        } catch (err) {
            Router.push(Constants.get_new_page('products', this.state.referrer));
        }
    };

    handleFieldChange = (prop) => (e) => {
        const fields_with_type_number = [
            'personal_mobile_number',
            'personal_bank_acct_number',
            'mobile_number',
            'premiumback_bank_acct_number',
            'claims_bank_acct_number'
        ];

        if (fields_with_type_number.indexOf(prop) != -1) {
            if ('' === e.target.value || (isFinite(e.target.value) && !e.target.value.indexOf('.') != -1)) {
            } else {
                return;
            }
        }

        this.setState(
            {
                [prop]: e.target.value
            },
            () => {}
        );
    };

    handleOccupationFieldChange = (value, type, i) => {
        switch (type) {
            case 'personal':
                this.setState({
                    personal_occupation: value
                });
                break;

            case 'insured':
                let insured = GlobalHelpers.clone_deep(this.state.insured);
                insured[i].occupation = value;
                this.setState({
                    insured
                });
                break;

            default:
                // Should not happen
                break;
        }
    };

    handlePlaceOfBirthFieldChange = (value, type, i) => {
        switch (type) {
            case 'personal':
                this.setState({
                    personal_place_of_birth: value
                });
                break;

            case 'insured':
                let insured = GlobalHelpers.clone_deep(this.state.insured);
                insured[i].place_of_birth = value;
                this.setState({
                    insured
                });
                break;

            default:
                // Should not happen
                break;
        }
    };

    handleCheckboxToggle = (name) => (event) => {
        let setStateObject = { [name]: event.target.checked };

        if (name === 'confirm_terms_and_conditions') {
            if (event.target.checked) {
                if (!this.validate_all()) {
                    return false;
                } else {
                    // this.screenshotSignupPage()   // Asynchronous
                }
            } else {
                // this.setState({
                //   image_upload_signup_screenshot: ""  // Reset it to prevent sending an old screenshot file name to the DB.
                // })
            }
        }

        this.setState(setStateObject);
    };

    handleRadioButtonChange = (event) => {
        this.setState({ selectedValue: event.target.value });
    };

    handleDateChange = (field, i) => (date) => {
        if (i || i === 0) {
            let insured = GlobalHelpers.clone_deep(this.state.insured);
            insured[i][field] = date;
            this.setState({ insured });
        } else {
            this.setState({ [field]: date });
        }
    };

    renderPMDFields = () => {
        const pmd_brand = (
            <Element name="pmd_brand" className="element">
                <div
                    style={{ marginTop: '15px' }}
                    onFocus={() => {
                        scrollToElement('pmd_brand');
                    }}
                >
                    <TextField
                        label="Brand"
                        autoComplete="none"
                        value={this.state.pmd_brand}
                        onChange={this.handleFieldChange('pmd_brand')}
                        fullWidth
                    />
                </div>
            </Element>
        );

        const pmd_model = (
            <Element name="pmd_model" className="element">
                <div
                    style={{ marginTop: '5px' }}
                    onFocus={() => {
                        scrollToElement('pmd_model');
                    }}
                >
                    <TextField
                        label="Model"
                        autoComplete="none"
                        value={this.state.pmd_model}
                        onChange={this.handleFieldChange('pmd_model')}
                        fullWidth
                    />
                </div>
            </Element>
        );

        const pmd_color = (
            <Element name="pmd_color" className="element">
                <div
                    style={{ marginTop: '5px' }}
                    onFocus={() => {
                        scrollToElement('pmd_color');
                    }}
                >
                    <TextField
                        label="Color"
                        autoComplete="none"
                        value={this.state.pmd_color}
                        onChange={this.handleFieldChange('pmd_color')}
                        fullWidth
                    />
                </div>
            </Element>
        );

        const pmd_serial_number = (
            <Element name="pmd_serial_number" className="element">
                <div
                    style={{ marginTop: '5px' }}
                    onFocus={() => {
                        scrollToElement('pmd_serial_number');
                    }}
                >
                    <TextField
                        label="Serial Number"
                        autoComplete="none"
                        value={this.state.pmd_serial_number}
                        onChange={this.handleFieldChange('pmd_serial_number')}
                        fullWidth
                    />
                </div>
            </Element>
        );

        const pmd_lta_number = (
            <Element name="pmd_lta_number" className="element">
                <div
                    style={{ marginTop: '5px' }}
                    onFocus={() => {
                        scrollToElement('pmd_lta_number');
                    }}
                >
                    <TextField
                        label="LTA Number"
                        autoComplete="none"
                        value={this.state.pmd_lta_number}
                        onChange={this.handleFieldChange('pmd_lta_number')}
                        fullWidth
                    />
                </div>
            </Element>
        );

        const pmd_ul_number = (
            <Element name="pmd_ul_number" className="element">
                <div
                    style={{ marginTop: '5px' }}
                    onFocus={() => {
                        scrollToElement('pmd_ul_number');
                    }}
                >
                    <TextField
                        label="UL Number (if your PMD is UL certified)"
                        autoComplete="none"
                        value={this.state.pmd_ul_number}
                        onChange={this.handleFieldChange('pmd_ul_number')}
                        fullWidth
                    />
                    <div style={{ color: 'grey', fontSize: '12px', marginTop: '5px' }}>
                        * In order to comply with national regulations, by 19th June 2020, you will have to provide us
                        your new PMD's particulars with the UL number, for us to continue to cover your PMD.
                    </div>
                </div>
            </Element>
        );

        const arrayOfPersonalInfoFieldsPMD = [
            pmd_brand,
            pmd_model,
            pmd_color,
            pmd_serial_number,
            pmd_lta_number,
            pmd_ul_number,
            <Button
                style={{ float: 'right', margin: '10px 0px 0px 0px' }}
                onClick={() => {
                    if (this.validate_pmd_details()) {
                        this.nextPanelAfterRepresenting(this.state.representing, true);
                    }
                }}
            >
                OK
            </Button>
        ];

        // let renderable = [arrayOfPersonalInfoFieldsPMD]
        return <div style={{ padding: '0px 30px 30px 30px' }}>{arrayOfPersonalInfoFieldsPMD}</div>;
    };

    validate_pmd_details = () => {
        // PMD Details
        const pmd_brand = this.state.pmd_brand;
        const pmd_model = this.state.pmd_model;
        const pmd_color = this.state.pmd_color;
        const pmd_serial_number = this.state.pmd_serial_number;
        const pmd_lta_number = this.state.pmd_lta_number;
        const pmd_ul_number = this.state.pmd_ul_number;

        if (this.state.products.indexOf('small_pmd_protect') != -1) {
            if (pmd_brand == null) {
                alert('Error - PMD Brand not filled in');
                return false;
            } else if (pmd_model == null) {
                alert('Error - PMD Model not filled in');
                return false;
            } else if (pmd_color == null) {
                alert('Error - PMD Color not filled in');
                return false;
            } else if (pmd_serial_number == null) {
                alert('Error - PMD Serial Number not filled in');
                return false;
            } else if (pmd_lta_number == null) {
                alert('Error - PMD LTA Number not filled in');
                return false;
            }
        }

        return true;
    };

    configureExpansionPanels = (arrayOfSettings) => {
        const collapse_all = (setStateObject) => {
            setStateObject.expanded_login = false;
            setStateObject.expanded_representing = false;
            setStateObject.expanded_pmd_details = false; //pmd_details_expand
            setStateObject.expanded_personal_info = false;
            setStateObject.expanded_insured_info = false;
            setStateObject.expanded_confirm = false;

            setStateObject.confirm_terms_and_conditions = false; // To do final validation again if panels are opened again.
        };

        const hide_all_after = (
            panel,
            setStateObject,
            style_representing,
            style_pmd_details,
            style_personal_info,
            style_insured_info,
            style_confirm
        ) => {
            switch (panel) {
                case 'login':
                    // Should not happen
                    break;
                case 'representing':
                    setStateObject.style_representing = style_representing;
                    setStateObject.style_representing.visibility = 'visible';

                    // setStateObject.style_pmd_details = style_pmd_details;
                    // setStateObject.style_pmd_details.visibility = "collapse";

                    // COMMENTED OUT THIS CHUNK 2019-12-17
                    // setStateObject.style_personal_info = style_personal_info;
                    // setStateObject.style_personal_info.visibility = "collapse";
                    // setStateObject.style_insured_info = style_insured_info;
                    // setStateObject.style_insured_info.visibility = "collapse";

                    // setStateObject.style_confirm = style_confirm;
                    // setStateObject.style_confirm.visibility = "collapse";

                    break;
                case 'pmd_details':
                    setStateObject.style_pmd_details = style_pmd_details;
                    setStateObject.style_pmd_details.visibility = 'visible';
                    break;

                case 'personal_info':
                    setStateObject.style_personal_info = style_personal_info;
                    setStateObject.style_personal_info.visibility = 'visible';

                    setStateObject.style_insured_info = style_insured_info;
                    setStateObject.style_insured_info.visibility = 'collapse';

                    setStateObject.style_confirm = style_confirm;
                    setStateObject.style_confirm.visibility = 'collapse';
                    break;
                case 'insured_info':
                    setStateObject.style_insured_info = style_insured_info;
                    setStateObject.style_insured_info.visibility = 'visible';

                    setStateObject.style_confirm = style_confirm;
                    setStateObject.style_confirm.visibility = 'collapse';
                    break;
                case 'confirm':
                    setStateObject.style_confirm = style_confirm;
                    setStateObject.style_confirm.visibility = 'visible';
                    break;
                default:
                    break;
            }
        };

        return new Promise((resolve, reject) => {
            let setStateObject = {};

            // let style_login = ""   // Not used.
            let style_login = (style_login = GlobalHelpers.clone_deep(this.state.style_login));
            let style_representing = GlobalHelpers.clone_deep(this.state.style_representing);
            let style_pmd_details = GlobalHelpers.clone_deep(this.state.style_pmd_details);
            let style_personal_info = GlobalHelpers.clone_deep(this.state.style_personal_info);
            let style_insured_info = GlobalHelpers.clone_deep(this.state.style_insured_info);
            let style_confirm = GlobalHelpers.clone_deep(this.state.style_confirm);

            for (let i = 0; i < arrayOfSettings.length; i++) {
                // Setting configurations of panels
                switch (arrayOfSettings[i]) {
                    case 'login_expand':
                        collapse_all(setStateObject);
                        hide_all_after(
                            'login',
                            setStateObject,
                            style_login,
                            style_representing,
                            style_pmd_details,
                            style_personal_info,
                            style_insured_info,
                            style_confirm
                        );
                        setStateObject.expanded_login = true;
                        break;

                    case 'login_collapse':
                        setStateObject.expanded_login = false;
                        break;

                    case 'pmd_details_expand':
                        collapse_all(setStateObject);
                        hide_all_after(
                            'pmd_details',
                            setStateObject,
                            style_representing,
                            style_pmd_details,
                            style_personal_info,
                            style_insured_info,
                            style_confirm
                        );
                        setStateObject.expanded_pmd_details = true;
                        break;
                    case 'pmd_details_collapse':
                        setStateObject.expanded_pmd_details = false;
                        break;

                    case 'representing_expand':
                        collapse_all(setStateObject);
                        hide_all_after(
                            'representing',
                            setStateObject,
                            style_representing,
                            style_pmd_details,
                            style_personal_info,
                            style_insured_info,
                            style_confirm
                        );
                        setStateObject.expanded_representing = true;
                        break;
                    case 'representing_collapse':
                        setStateObject.expanded_representing = false;
                        break;

                    case 'personal_info_expand':
                        collapse_all(setStateObject);
                        hide_all_after(
                            'personal_info',
                            setStateObject,
                            style_representing,
                            style_pmd_details,
                            style_personal_info,
                            style_insured_info,
                            style_confirm
                        );
                        setStateObject.expansion_panel_personal_info_disabled = false;
                        setStateObject.expanded_personal_info = true;
                        if (setStateObject.style_personal_info) {
                            setStateObject.style_personal_info.margin = '15px 0px';
                        } else {
                            setStateObject.style_personal_info = { margin: '15px 0px' };
                        }
                        break;
                    case 'personal_info_collapse':
                        style_personal_info.margin = '1px 0px 0px 0px';
                        setStateObject.expanded_personal_info = false;
                        setStateObject.style_personal_info = style_personal_info;
                        break;
                    case 'personal_info_visible':
                        style_personal_info.visibility = 'visible';
                        // style_personal_info.margin = "15px 0px"
                        setStateObject.style_personal_info = style_personal_info;
                        break;
                    case 'personal_info_visible_disabled':
                        style_personal_info.visibility = 'visible';
                        style_personal_info.margin = '1px 0px 0px 0px';
                        setStateObject.style_personal_info = style_personal_info;
                        setStateObject.expansion_panel_personal_info_disabled = true;
                        break;
                    case 'personal_info_invisible':
                        style_personal_info.visibility = 'collapse';
                        style_personal_info.margin = '1px 0px 0px 0px';
                        setStateObject.style_personal_info = style_personal_info;
                        break;

                    case 'insured_info_expand':
                        collapse_all(setStateObject);
                        hide_all_after(
                            'insured_info',
                            setStateObject,
                            style_representing,
                            style_pmd_details,
                            style_personal_info,
                            style_insured_info,
                            style_confirm
                        );
                        setStateObject.buy_for_all_members = false;
                        setStateObject.expanded_insured_info = true;
                        if (setStateObject.style_insured_info) {
                            setStateObject.style_insured_info.margin = '15px 0px';
                        } else {
                            setStateObject.style_insured_info = { margin: '15px 0px' };
                        }
                        break;
                    case 'insured_info_collapse':
                        style_insured_info.margin = '1px 0px 0px 0px';
                        setStateObject.expanded_insured_info = false;
                        setStateObject.style_insured_info = style_insured_info;
                        break;
                    case 'insured_info_visible':
                        style_insured_info.visibility = 'visible';
                        style_insured_info.margin = '15px 0px';
                        setStateObject.style_insured_info = style_insured_info;
                        break;
                    case 'insured_info_invisible':
                        style_insured_info.visibility = 'collapse';
                        style_insured_info.margin = '1px 0px 0px 0px';
                        setStateObject.style_insured_info = style_insured_info;
                        break;

                    case 'confirm_expand':
                        collapse_all(setStateObject);
                        hide_all_after(
                            'confirm',
                            setStateObject,
                            style_representing,
                            style_pmd_details,
                            style_personal_info,
                            style_insured_info,
                            style_confirm
                        );
                        setStateObject.expanded_confirm = true;

                        break;
                    case 'confirm_collapse':
                        setStateObject.expanded_confirm = false;
                        break;
                    case 'confirm_visible':
                        style_confirm.visibility = 'visible';
                        setStateObject.style_confirm = style_confirm;
                        break;

                    default:
                        break;
                }
            }

            this.setState(setStateObject, () => {
                resolve();
            });
        });
    };

    /////////////////////////////////////////////////////////
    ////////////////////  Login Panel ///////////////////
    /////////////////////////////////////////////////////////

    handleLoginTabChange = (event, value) => {
        this.setState({ loginTab: value });
    };

    handleLoginTabChangeIndex = (index) => {
        this.setState({ loginTab: index });
    };

    handleSignup = () => {
        if (this.state.signup_button_disabled === true) {
            return;
        }

        // Link with DB to actually signup
        const login_email = this.state.login_email;
        const signup_password = this.state.signup_password;
        const signup_password_retype = this.state.signup_password_retype;
        // const referrer = this.state.referrer;
        const referrer = sessionStorage.getItem('ifinsg_referrer') || '';

        // Validate fields
        if (!do_not_validate) {
            if (!validator.validate(login_email)) {
                alert('Error - Invalid email');
                return;
            } else if (signup_password === '') {
                alert('Error - Please enter a password');
                return;
            } else if (signup_password_retype === '') {
                alert('Error - Please re-enter your password');
                return;
            }

            const password_strength = owasp.test(signup_password);

            if (password_strength.requiredTestErrors.length > 0) {
                alert(password_strength.requiredTestErrors[0]);
                return;
            } else if (password_strength.optionalTestErrors.length > 2) {
                alert(password_strength.optionalTestErrors[0]);
                return;
            }

            if (signup_password !== signup_password_retype) {
                alert('Error - Passwords do not match');
                return;
            }

            this.setState(
                {
                    signup_button_disabled: true
                },
                () => {
                    // Send to server
                    axios
                        .post('/api/auth/signup', {
                            login_email,
                            signup_password,
                            referrer
                        })
                        .then((res) => {
                            fbq('track', 'CompleteRegistration');
                            if (res.status === 200) {
                                if (res.data.err) {
                                    alert(res.data.err); // Eg: ERROR - Email already in use
                                    this.setState({ signup_button_disabled: false });
                                } else {
                                    this.setState({
                                        show_email_verification_field: true
                                    });

                                    // Let the resend_verification_code_button be active after 15 seconds
                                    setTimeout(() => {
                                        this.setState({
                                            resend_verification_code_button_disabled: false
                                        });
                                    }, 10000);
                                }
                            } else {
                                this.setState({ signup_button_disabled: false });
                            }
                        })
                        .catch((err) => {
                            this.setState({ signup_button_disabled: false });
                        });
                }
            );
        } else {
            this.nextPanelAfterSignup();
        }

        // Disable Login/Signup expansion panel
    };

    renderSignupFields = () => {
        let renderable = [];

        const login_email = (
            <Element name="login_email" className="element">
                <div
                    onFocus={() => {
                        scrollToElement('login_email');
                    }}
                >
                    <div style={{ padding: '0px 0px 5px 0px' }}>
                        <TextField
                            label="Email"
                            autoComplete="email"
                            value={this.state.login_email}
                            onChange={this.handleFieldChange('login_email')}
                            fullWidth
                            // InputProps={{
                            //   endAdornment: <InputAdornment>@email.com</InputAdornment>,
                            // }}
                        />
                    </div>
                </div>
            </Element>
        );

        const signup_password = (
            <Element name="signup_password" className="element">
                <div
                    onFocus={() => {
                        scrollToElement('signup_password');
                    }}
                >
                    <div style={{ padding: '0px 0px 2px 0px' }}>
                        <TextField
                            label="Password"
                            value={this.state.signup_password}
                            autoComplete="new-password"
                            onChange={this.handleFieldChange('signup_password')}
                            type="password"
                            fullWidth
                        />
                    </div>
                </div>
            </Element>
        );

        const signup_password_retype = (
            <Element name="signup_password_retype" className="element">
                <div
                    onFocus={() => {
                        scrollToElement('signup_password_retype');
                    }}
                >
                    <div style={{ padding: '0px 0px 2px 0px' }}>
                        <TextField
                            label="Re-type your password"
                            onKeyPress={(ev) => {
                                if (ev.key === 'Enter') {
                                    this.handleSignup();
                                }
                            }}
                            value={this.state.signup_password_retype}
                            onChange={this.handleFieldChange('signup_password_retype')}
                            type="password"
                            fullWidth
                        />
                    </div>
                </div>
            </Element>
        );

        const signup_button = (
            <Button
                style={{ float: 'right', margin: '10px 0px 0px 0px' }}
                disabled={this.state.signup_button_disabled}
                onClick={() => {
                    this.handleSignup();
                }}
            >
                Signup
            </Button>
        );

        if (!this.state.show_email_verification_field) {
            renderable = [login_email, signup_password, signup_password_retype, signup_button];
        }

        return <form action="javascript:void(0);">{renderable}</form>;
    };

    renderEmailVerificationFields = () => {
        let renderable = [];

        const instructions = (
            <Typography style={{ padding: '20px 0px 2px 0px' }}>
                Please enter email verification code sent to {this.state.login_email}:
            </Typography>
        );

        const email_verification_field = (
            <Element name="email_verification_field" className="element">
                <div
                    onFocus={() => {
                        scrollToElement('email_verification_field');
                    }}
                >
                    <div style={{ padding: '20px 0px 2px 0px' }}>
                        <TextField
                            inputRef={(node) => (input_refs.email_verification_field = node)}
                            label="Email verification code"
                            onKeyPress={(ev) => {
                                if (ev.key === 'Enter') {
                                    this.handleVerifyEmail();
                                }
                            }}
                            value={this.state.email_verification_code}
                            onChange={this.handleFieldChange('email_verification_code')}
                            type="number"
                            fullWidth
                            // inputProps = {{
                            //   onfocusout : (e) =>{
                            //     e.blur()
                            //   }
                            // }}
                        />
                    </div>
                </div>
            </Element>
        );

        const verify_email_button = (
            <Button
                style={{ float: 'right', margin: '10px 0px 0px 0px' }}
                disabled={this.state.verify_email_button_disabled}
                onClick={() => {
                    this.handleVerifyEmail();
                }}
            >
                Verify
            </Button>
        );

        const resend_verification_code_button = (
            <Button
                style={{
                    float: 'left',
                    margin: '10px 0px 0px 0px',
                    fontSize: '12px',
                    padding: 0
                }}
                disabled={this.state.resend_verification_code_button_disabled}
                onClick={() => {
                    this.handleResendVerificationCode();
                }}
            >
                Resend
            </Button>
        );

        if (this.state.show_email_verification_field) {
            renderable = [instructions, email_verification_field];
            return (
                <div>
                    <form action="javascript:void(0);">{renderable}</form>
                    {verify_email_button}
                </div>
            );
        } else {
        }
    };

    handleVerifyEmail = () => {
        if (input_refs.email_verification_field) {
            input_refs.email_verification_field.blur();
        }
        this.setState(
            {
                verify_email_button_disabled: true
            },
            () => {
                // Verify the verification code
                axios
                    .post('/api/auth/verifyEmail', {
                        signup_username: this.state.login_email,
                        email_verification_code: this.state.email_verification_code
                    })
                    .then((res) => {
                        if (res.status === 200) {
                            if (res.data.err) {
                                alert(res.data.err); // Eg: ERROR - Email already in use
                                this.setState({
                                    verify_email_button_disabled: false
                                });
                            } else {
                                if (res.data.verification_result) {
                                    if (res.data.token) {
                                        cookie.set('ifinsg_token', res.data.token, { path: '/' });
                                        localStorage.setItem('ifinsg_token', res.data.token);
                                    }
                                    this.setState({
                                        expanded_login: false,
                                        expansion_panel_login_disabled: true
                                    });

                                    this.nextPanelAfterSignup();
                                }
                            }
                        } else {
                        }
                    })
                    .catch((err) => {});
            }
        );
    };

    handleResendVerificationCode = () => {
        this.setState(
            {
                resend_verification_code_button_disabled: true
            },
            () => {
                // Let the resend_verification_code_button be active after 15 seconds
                setTimeout(() => {
                    this.setState({
                        resend_verification_code_button_disabled: false
                    });
                }, 10000);
            }
        );
    };

    handleLogin = () => {
        if (input_refs.login_password_field) {
            input_refs.login_password_field.blur();
        }

        const login_email = this.state.login_email;
        const login_password = this.state.login_password;

        if (!do_not_validate) {
            if (!validator.validate(login_email)) {
                alert('Error - Invalid email');
                return;
            }
        }

        // Server login Link with DB to actually login
        this.setState(
            {
                login_button_disabled: true
            },
            () => {
                // Send to server
                axios
                    .post('/api/auth/login', {
                        login_email,
                        login_password
                    })
                    .then((res) => {
                        if (res.status === 200) {
                            if (res.data.err) {
                                alert(res.data.err); // Eg: ERROR - Email already in use
                                this.setState({ login_button_disabled: false });
                            } else {
                                try {
                                    cookie.set('ifinsg_token', res.data.token, { path: '/' });
                                    localStorage.setItem('ifinsg_token', res.data.token);
                                    this.getUserData(res.data.token);
                                    this.setState({
                                        // Save user_info to state
                                        personal_info_already_in_db: res.data.personal_info_already_in_db,
                                        user_info: res.data.user_info,
                                        policies: this.parsedPolicies(res.data.user_info),
                                        // Setting login_email to username from DB
                                        login_email: res.data.user_info.username,

                                        // Disabling expansion panel
                                        expansion_panel_login_disabled: true
                                    });

                                    this.configureExpansionPanels(['representing_expand']);
                                } catch (err) {
                                    this.setState({ login_button_disabled: false });
                                }
                            }
                        } else {
                        }
                    })
                    .catch((err) => {
                        this.setState({ login_button_disabled: false });
                    });
            }
        );
    };

    renderLoginFilledDetails = () => {
        let renderable = [];

        const email = (
            <Typography style={{ fontSize: '13px', color: 'grey' }}>
                {this.state.loginTab == 0 ? this.state.login_email : this.state.login_email}
            </Typography>
        );

        renderable.push(email);
        return renderable;
    };

    nextPanelAfterSignup = () => {
        this.configureExpansionPanels(['representing_expand']);
        window.scrollTo(0, 0);
    };

    renderAddNewMemberButton = (representing, boughtPolicies) => {
        if (representing === 'self') return null;
        if (representing === 'family')
            return (
                <Button
                    variant="contained"
                    color="primary"
                    style={{ margin: '0 auto' }}
                    onClick={() => {
                        this.nextPanelAfterRepresenting(representing, null, true, boughtPolicies);
                    }}
                >
                    Add new FAMILY member?
                </Button>
            );
        if (representing === 'entity')
            return (
                <Button
                    variant="contained"
                    color="primary"
                    style={{ margin: '0 auto' }}
                    onClick={() => {
                        this.nextPanelAfterRepresenting(representing, null, true, boughtPolicies);
                    }}
                >
                    Add new COMPANY staff?
                </Button>
            );
    };

    renderContainerTab = (representing) => {
        const { policies, products, representingTab } = this.state;
        const product = products && products[0];
        const alreadyBought = products && products.length === 1 && policies[product] && policies[product][representing];
        if (alreadyBought) {
            let boughtStatus = alreadyBought[0].status;
            return (
                <div
                    component="div"
                    style={{
                        padding: 8 * 3,
                        display: 'flex',
                        flexDirection: 'column',
                        height: '100%',
                        minHeight: '400px',
                        boxSizing: 'border-box'
                    }}
                >
                    <div style={{ padding: '0px 0px 5px 0px', margin: 'auto', textAlign: 'center' }}>
                        {boughtStatus === 'active' && representing === 'self' && (
                            <div style={{ color: '#7cda24' }}>You are already covered for this.</div>
                        )}
                        {boughtStatus === 'active' && representing === 'family' && (
                            <div style={{ color: '#7cda24' }}>Your FAMILY members are already covered for this.</div>
                        )}
                        {boughtStatus === 'active' && representing === 'entity' && (
                            <div style={{ color: '#7cda24' }}>Your COMPANY staff are already covered for this.</div>
                        )}
                        {boughtStatus === 'pending_approval' && representing === 'self' && (
                            <div style={{ color: '#007bff' }}>You already applied for this cover.</div>
                        )}
                        {boughtStatus === 'pending_approval' && representing === 'family' && (
                            <div style={{ color: '#007bff' }}>You already applied for this FAMILY cover.</div>
                        )}
                        {boughtStatus === 'pending_approval' && representing === 'entity' && (
                            <div style={{ color: '#007bff' }}>You already applied for this COMPANY staff cover.</div>
                        )}
                        {boughtStatus === 'cancelled' && representing === 'self' && (
                            <div style={{ color: '#007bff' }}>Your cover for this has been paused.</div>
                        )}
                        {boughtStatus === 'cancelled' && representing === 'family' && (
                            <div style={{ color: '#007bff' }}>Your FAMILY cover for this has been paused.</div>
                        )}
                        {boughtStatus === 'cancelled' && representing === 'entity' && (
                            <div style={{ color: '#007bff' }}>Your COMPANY cover for this has been paused.</div>
                        )}
                        {boughtStatus === 'cancellation_requested' && (
                            <div style={{ color: '#f44336' }}>You already submitted a cancellation for this.</div>
                        )}
                    </div>
                    {/* <Button variant="contained" color="primary" style={{ margin: '0 auto' }}>
              Re-continue?
            </Button>  */}
                    {boughtStatus === 'cancelled' ? null : this.renderAddNewMemberButton(representing, alreadyBought)}
                    {/* Cover Re-continue application sent. */}
                </div>
            );
        }
        return (
            <TabContainer>
                <div>
                    <div style={{ padding: '0px 0px 5px 0px' }}>
                        <div style={{ textAlign: 'center' }}>
                            <Typography variant="subtitle1" style={{ margin: '20px 0px 0px 0px' }}>
                                Pay PREMIUMBACK to:
                            </Typography>
                        </div>
                        {representing === 'self' && (
                            <div style={{ textAlign: 'center' }}>
                                <FormControlLabel
                                    disabled
                                    style={{ margin: '0px 10px 0px 10px' }}
                                    control={
                                        <Radio
                                            style={{ padding: '6px' }}
                                            name="group_premiumback_pay_to_same"
                                            checked
                                        />
                                    }
                                    label="Self"
                                />
                            </div>
                        )}
                        {representing === 'family' && (
                            <div style={{ textAlign: 'center' }}>
                                <FormControlLabel
                                    disabled
                                    style={{ margin: '0px 10px 0px 10px' }}
                                    control={
                                        <Radio
                                            style={{ padding: '6px' }}
                                            name="family_premiumback_pay_to_same"
                                            checked
                                        />
                                    }
                                    label="Self"
                                />
                                <FormControlLabel
                                    disabled
                                    style={{ margin: '0px 10px 0px 10px' }}
                                    control={<Radio style={{ padding: '6px' }} name="family_premiumback_pay_to_same" />}
                                    label="Family member"
                                />
                            </div>
                        )}
                        {representing === 'entity' && (
                            <div style={{ textAlign: 'center' }}>
                                <FormControlLabel
                                    disabled
                                    style={{ margin: '0px 10px 0px 10px' }}
                                    control={
                                        <Radio
                                            style={{ padding: '6px' }}
                                            name="entity_premiumback_pay_to_same"
                                            checked={true}
                                        />
                                    }
                                    label="Company"
                                />
                                <FormControlLabel
                                    disabled
                                    style={{ margin: '0px 10px 0px 10px' }}
                                    control={<Radio style={{ padding: '6px' }} name="entity_premiumback_pay_to_same" />}
                                    label="Individual"
                                />
                            </div>
                        )}

                        <div style={{ textAlign: 'center' }}>
                            <Typography variant="subtitle1" style={{ margin: '20px 0px 0px 0px' }}>
                                Pay CLAIMS to:
                            </Typography>
                        </div>
                        {representing === 'self' && (
                            <div style={{ textAlign: 'center' }}>
                                <FormControlLabel
                                    disabled
                                    style={{ margin: '0px 0px 0px 0px' }}
                                    control={
                                        <Radio style={{ padding: '6px' }} name="group_claims_pay_to_same" checked />
                                    }
                                    label="Self"
                                />
                            </div>
                        )}
                        {representing === 'family' && (
                            <div style={{ textAlign: 'center' }}>
                                <FormControlLabel
                                    style={{ margin: '0px 10px 0px 10px' }}
                                    control={
                                        <Radio
                                            style={{ padding: '6px' }}
                                            name="family_claims_pay_to_same"
                                            checked={this.state.family_claims_pay_to_same}
                                            onChange={() => {
                                                this.setState({ family_claims_pay_to_same: true });
                                            }}
                                        />
                                    }
                                    label="Self"
                                />
                                <FormControlLabel
                                    style={{ margin: '0px 10px 0px 10px' }}
                                    control={
                                        <Radio
                                            style={{ padding: '6px' }}
                                            name="family_claims_pay_to_same"
                                            checked={!this.state.family_claims_pay_to_same}
                                            onChange={() => {
                                                this.setState({ family_claims_pay_to_same: false });
                                            }}
                                        />
                                    }
                                    label="Family member"
                                />
                            </div>
                        )}
                        {representing === 'entity' && (
                            <div style={{ textAlign: 'center' }}>
                                <FormControlLabel
                                    style={{ margin: '0px 10px 0px 10px' }}
                                    control={
                                        <Radio
                                            style={{ padding: '6px' }}
                                            name="entity_claims_pay_to_same"
                                            checked={this.state.entity_claims_pay_to_same}
                                            onChange={() => {
                                                this.setState({ entity_claims_pay_to_same: true });
                                            }}
                                        />
                                    }
                                    label="Company"
                                />
                                <FormControlLabel
                                    style={{ margin: '0px 10px 0px 10px' }}
                                    control={
                                        <Radio
                                            style={{ padding: '6px' }}
                                            name="entity_claims_pay_to_same"
                                            checked={!this.state.entity_claims_pay_to_same}
                                            onChange={() => {
                                                this.setState({ entity_claims_pay_to_same: false });
                                            }}
                                        />
                                    }
                                    label="Individual"
                                />
                            </div>
                        )}
                        {this.renderRepresentingFields(representing)}
                    </div>
                </div>
            </TabContainer>
        );
    };

    renderTabContainers = () => {
        const { policies, products, representingTab, representing } = this.state;

        let renderable = [];

        const tab_containers_self = this.renderContainerTab('self');

        const tab_containers_entity = this.renderContainerTab('entity');

        const tab_containers_family = this.renderContainerTab('family');

        renderable = [tab_containers_self, tab_containers_family, tab_containers_entity];
        if (this.state.products.indexOf('small_pmd_protect') != -1) {
            renderable = [tab_containers_self];
        }

        return renderable;
    };

    handleRepresentingTabChange = (event, value) => {
        this.representingTabChange(value);
    };

    handleRepresentingTabChangeIndex = (index) => {
        this.representingTabChange(index);
    };

    representingTabChange = (index) => {
        let representing;
        switch (index) {
            case 0:
                representing = 'self';
                break;
            case 1:
                representing = 'family';
                break;
            case 2:
                representing = 'entity';
                break;
            default:
                representing = 'self';
                break;
        }

        if (index === 0) {
            this.configureExpansionPanels(['insured_info_invisible']);
        }
        this.setState({
            representingTab: index,
            representing
        });
    };

    renderRepresentingFields = (representing) => {
        let renderable = [];

        const ok_button = (
            <div>
                <div>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.state.ack_pdpa}
                                onChange={this.handleCheckboxToggle('ack_pdpa')}
                                value="ack_pdpa"
                                color="primary"
                            />
                        }
                        // label={<span style={{ color: "rgba(0, 0, 0, 0.54)", fontSize: "13px", lineHeight: 0 }}>I acknowledge that a fee of $0.50 will be imposed on all Premiumback & Claims payouts. FREE for payouts to DBS/POSB accounts</span>}
                        label={
                            <span
                                style={{
                                    color: 'rgba(0, 0, 0, 0.54)',
                                    fontSize: '13px',
                                    lineHeight: 0
                                }}
                            >
                                I am aware that iFinSG's service requires accurate user identification, and I agree to
                                declare full and complete information of my NRIC / Passport as an applicant or user of
                                iFinSG.
                            </span>
                        }
                        style={{ margin: '30px 0px 0px 0px' }}
                    />

                    <div
                        style={{ padding: '5px 0px 0px 47px' }}
                        onClick={() => {
                            // this.setState({modalMessageOpen:true})
                        }}
                    >
                        (
                        <a
                            href="https://www.mas.gov.sg/regulation/anti-money-laundering/targeted-financial-sanctions"
                            target="blank"
                        >
                            https://www.mas.gov.sg/regulation/anti-money-laundering/targeted-financial-sanctions
                        </a>
                        )
                    </div>
                </div>
                <Button
                    style={{ float: 'right', margin: '10px 0px 0px 0px' }}
                    onClick={() => {
                        if (this.state.ack_pdpa) {
                            this.nextPanelAfterRepresenting(representing);
                        } else {
                            alert('Please accept the terms to continue.');
                            return;
                        }
                    }}
                >
                    OK
                </Button>
            </div>
        );

        switch (representing) {
            case 'self':
                renderable = [ok_button];
                return renderable;
                break;

            case 'family':
                renderable = [ok_button];
                return renderable;
                break;

            case 'entity':
                renderable = [ok_button];
                return renderable;
                break;

            case 'group':
                if (this.state.representing === 'group') {
                    renderable = [ok_button];
                    return renderable;
                }
                break;

            default:
                break;
        }
    };

    showPMDFields = () => {
        this.configureExpansionPanels(['pmd_details_expand']);
    };

    renderRepresentingFilledDetails = () => {
        let renderable = [];
        let representing = '';
        let premiumback_payouts_to = '';
        let claims_payouts_to = '';

        switch (this.state.representing) {
            case 'self':
                representing = 'Myself';
                premiumback_payouts_to = 'Self';
                claims_payouts_to = 'Self';
                break;
            case 'family':
                representing = 'Family members';
                premiumback_payouts_to = 'Self';
                claims_payouts_to = this.state.family_claims_pay_to_same ? 'Self' : 'Individual';
                break;
            case 'entity':
                representing = `My company staff`;
                // if (this.state.entity_name) { representing = representing + ` | ${this.state.entity_name}` }
                // if (this.state.entity_uen) { representing = representing + ` | ${this.state.entity_uen}` }
                premiumback_payouts_to = 'Company';
                claims_payouts_to = this.state.entity_claims_pay_to_same ? 'Company' : 'Individual';
                break;
            case 'group':
                representing = 'My friends';
                premiumback_payouts_to = 'Individual';
                claims_payouts_to = 'Individual';
                break;
            default:
                break;
        }

        const representing_component = (
            <Typography style={{ fontSize: '13px', color: 'grey' }}>{representing}</Typography>
        );
        const pay_premiumback_payouts_to = (
            <Typography style={{ fontSize: '13px', color: 'grey' }}>
                Pay PremiumBack to: {premiumback_payouts_to}
            </Typography>
        );
        const pay_claims_payouts_to = (
            <Typography style={{ fontSize: '13px', color: 'grey' }}>Pay claims to: {claims_payouts_to}</Typography>
        );

        renderable.push(representing_component);
        renderable.push(pay_premiumback_payouts_to);
        renderable.push(pay_claims_payouts_to);

        return renderable;
    };

    validate_representing = () => {
        const entity_name = this.state.entity_name;
        const entity_uen = this.state.entity_uen;

        // if (this.state.representing === "entity") {
        //   // If representing entity,
        //   if (entity_name === "") {
        //     alert("Error - Company name is not filled in");
        //     return;
        //   } else if (entity_uen === "") {
        //     alert("Error - Company UEN is not filled in");
        //     return;
        //   }
        // }

        return true;
    };

    nextPanelAfterRepresenting = (representing, fromPMD, isAddNewMember, boughtPolicies) => {
        this.setState({ isAddNewMember });
        // Form validation
        if (!do_not_validate) {
            if (!this.validate_representing()) {
                // If not valid, break function with return
                return;
            }

            if (!assumeNewUser && representing === 'self' && this.state.personal_info_already_in_db) {
                if (!this.validate_special_small_accident_max_age('personal')) {
                    return;
                }
            }
        }

        this.setState({ boughtPolicies });

        if (this.state.products.indexOf('small_pmd_protect') == -1 || fromPMD) {
            if (this.state.representing == 'family' && this.state.buy_for_all_members) {
                // Go stgraight to confirm
                this.calcBasketPrices();
                this.configureExpansionPanels(['personal_info_visible_disabled', 'confirm_expand']).then(() => {
                    window.scrollTo(0, 0);
                });
            } else if (this.state.representing == 'entity') {
                if (this.state.user_info && this.state.user_info.company_details != null) {
                    // If company_details already exists, go to add insured info
                    // Go to insured_info
                    if (this.state.buy_for_all_members) {
                        // Go stgraight to confirm
                        this.calcBasketPrices();
                        this.configureExpansionPanels(['personal_info_visible_disabled', 'confirm_expand']).then(() => {
                            window.scrollTo(0, 0);
                        });
                    } else {
                        if (this.state.num_of_insured === 0) {
                            this.handleAddInsured();
                        }
                        this.configureExpansionPanels(['personal_info_visible_disabled', 'insured_info_expand']).then(
                            () => {
                                window.scrollTo(0, 0);
                            }
                        );
                    }
                } else {
                    // If company_details does not exist, go to add payer info
                    this.configureExpansionPanels(['personal_info_visible', 'personal_info_expand']).then(() => {
                        window.scrollTo(0, 0);
                    });
                }
            } else if (!assumeNewUser && this.state.personal_info_already_in_db) {
                let user_info = this.state.user_info;
                this.setState({
                    personal_nric_number: user_info.nric_number,
                    personal_bank: user_info.bank,
                    personal_bank_acct_number: user_info.bank_acct_number,
                    personal_dob: user_info.dob,
                    personal_full_name: user_info.full_name,
                    personal_gender: user_info.gender,
                    personal_mobile_number: user_info.mobile_number,
                    personal_occupation: user_info.occupation,
                    personal_self_employed: user_info.self_employed,

                    personal_place_of_birth: user_info.place_of_birth,
                    personal_preferred_name: user_info.preferred_name
                });

                if (this.state.representing === 'self') {
                    // Go straight to confirm
                    this.calcBasketPrices();

                    this.configureExpansionPanels(['personal_info_visible_disabled', 'confirm_expand']).then(() => {
                        window.scrollTo(0, 0);
                    });
                } else {
                    // Go to insured_info
                    if (this.state.num_of_insured === 0) {
                        this.handleAddInsured();
                    }
                    this.configureExpansionPanels(['personal_info_visible_disabled', 'insured_info_expand']).then(
                        () => {
                            window.scrollTo(0, 0);
                        }
                    );
                }
            } else {
                this.configureExpansionPanels(['personal_info_expand']).then(() => {
                    window.scrollTo(0, 0);
                });
            }
        } else {
            // Show PMD Fields
            this.showPMDFields();
        }
    };

    /////////////////////////////////////////////////////////
    ////////// Personal Info Expansion Panel /////////////
    /////////////////////////////////////////////////////////

    renderPersonalInfoFields = () => {
        const { representing } = this.state || {};

        const full_name = (
            <Element name="personal_full_name" className="element">
                <div
                    style={{ marginTop: '20px' }}
                    onFocus={() => {
                        scrollToElement('personal_full_name');
                    }}
                >
                    {representing == 'entity' && (
                        <div>
                            <TextField
                                style={{ margin: '15px 0px 0px 0px' }}
                                label="Company Name"
                                value={this.state.company_details ? this.state.company_details.name : ''}
                                onChange={(e) => {
                                    let company_details = this.state.company_details;
                                    if (!company_details) {
                                        company_details = {};
                                    }
                                    company_details.name = e.target.value;
                                    this.setState({ company_details });
                                }}
                                fullWidth
                            />
                            <TextField
                                style={{ margin: '5px 0px 0px 0px' }}
                                label="Company UEN"
                                value={this.state.company_details ? this.state.company_details.uen : ''}
                                onChange={(e) => {
                                    let company_details = this.state.company_details;
                                    if (!company_details) {
                                        company_details = {};
                                    }
                                    company_details.uen = e.target.value;
                                    this.setState({ company_details });
                                }}
                                fullWidth
                            />
                        </div>
                    )}

                    {representing == 'entity' ? (
                        <TextField
                            label={'Name ( of Company representative )'}
                            autoComplete="name"
                            value={this.state.company_details ? this.state.company_details.rep_name : ''}
                            onChange={(e) => {
                                let company_details = this.state.company_details;
                                if (!company_details) {
                                    company_details = {};
                                }
                                company_details.rep_name = e.target.value;
                                this.setState({ company_details }, () => {
                                });
                            }}
                            fullWidth
                        />
                    ) : (
                        <TextField
                            label={'Name'}
                            autoComplete="name"
                            value={this.state.personal_full_name}
                            onChange={this.handleFieldChange('personal_full_name')}
                            fullWidth
                        />
                    )}
                </div>
            </Element>
        );

        const preferred_name = (
            <Element name="personal_preferred_name" className="element">
                <div
                    onFocus={() => {
                        scrollToElement('personal_preferred_name');
                    }}
                >
                    {representing == 'entity' ? (
                        <TextField
                            style={{ margin: '5px 0px 0px 0px' }}
                            label={'Job Title'}
                            autoComplete="name"
                            value={this.state.company_details ? this.state.company_details.rep_job_title : ''}
                            onChange={(e) => {
                                let company_details = this.state.company_details;
                                if (!company_details) {
                                    company_details = {};
                                }
                                company_details.rep_job_title = e.target.value;
                                this.setState({ company_details });
                            }}
                            fullWidth
                        />
                    ) : (
                        <TextField
                            style={{ margin: '5px 0px 0px 0px' }}
                            label="Preferred Name"
                            autoComplete="name"
                            value={this.state.personal_preferred_name}
                            onChange={this.handleFieldChange('personal_preferred_name')}
                            fullWidth
                        />
                    )}
                </div>
            </Element>
        );

        const nric_number = (
            <Element name="personal_nric_number" className="element">
                <div
                    onFocus={() => {
                        scrollToElement('personal_nric_number');
                    }}
                >
                    {representing == 'entity' ? (
                        <TextField
                            autoComplete="nric"
                            style={{ margin: '5px 0px 0px 0px' }}
                            inputRef={(node) => (input_refs.personal_nric_number = node)}
                            label="NRIC/Passport number"
                            value={this.state.company_details ? this.state.company_details.rep_nric : ''}
                            onChange={(e) => {
                                let company_details = this.state.company_details;
                                if (!company_details) {
                                    company_details = {};
                                }
                                company_details.rep_nric = e.target.value;
                                this.setState({ company_details });
                            }}
                            fullWidth
                            onKeyPress={(ev) => {
                                if (ev.key === 'Enter') {
                                    if (input_refs.personal_nric_number) {
                                        input_refs.personal_nric_number.blur();
                                    }
                                    scrollToElement('personal_gender');
                                }
                            }}
                        />
                    ) : (
                        <TextField
                            autoComplete="nric"
                            style={{ margin: '5px 0px 0px 0px' }}
                            inputRef={(node) => (input_refs.personal_nric_number = node)}
                            label="NRIC/Passport number"
                            value={this.state.personal_nric_number}
                            onChange={this.handleFieldChange('personal_nric_number')}
                            fullWidth
                            onKeyPress={(ev) => {
                                if (ev.key === 'Enter') {
                                    if (input_refs.personal_nric_number) {
                                        input_refs.personal_nric_number.blur();
                                    }
                                    scrollToElement('personal_gender');
                                }
                            }}
                        />
                    )}
                </div>
            </Element>
        );
        const gender = (
            <Element name="personal_gender" className="element">
                <div style={{ marginTop: '20px', textAlign: 'center' }}>
                    <FormControlLabel
                        style={{ margin: '0px 10px 0px 10px' }}
                        control={
                            <Radio
                                style={{ padding: '6px' }}
                                name="personal_gender"
                                checked={this.state.personal_gender === 'M'}
                                onChange={() => {
                                    this.setState({ personal_gender: 'M' });
                                }}
                            />
                        }
                        label="Male"
                    />
                    <FormControlLabel
                        style={{ margin: '0px 10px 0px 10px' }}
                        control={
                            <Radio
                                style={{ padding: '6px' }}
                                name="personal_gender"
                                checked={this.state.personal_gender === 'F'}
                                onChange={() => {
                                    this.setState({ personal_gender: 'F' });
                                }}
                            />
                        }
                        label="Female"
                    />
                </div>
            </Element>
        );

        const DOB = (
            <Element name="personal_dob" className="element">
                <div
                    onFocus={() => {
                        scrollToElement('personal_dob');
                    }}
                >
                    <DatePickerRMC
                        date={this.state.personal_dob}
                        autoComplete="bday"
                        handleDateChange={this.handleDateChange('personal_dob')}
                        placeholder="Date of Birth"
                        defaultDate="1992-01-01" // default = today
                        minDate="1850-01-01"
                        // maxDate = "1993-01-01" // default = today
                    />
                </div>
            </Element>
        );

        const place_of_birth = (
            <Element name="personal_place_of_birth" className="element">
                <div
                    onFocus={() => {
                        scrollToElement('personal_place_of_birth');
                    }}
                    style={{ marginTop: '4px' }}
                >
                    <SelectCountry
                        country={this.state.personal_place_of_birth}
                        handleCountryFieldChange={this.handlePlaceOfBirthFieldChange}
                        type="personal"
                        placeholder="Place of Birth (as of NRIC/Passport)"
                    />
                </div>
            </Element>
        );

        const HP_number = (
            <Element name="personal_mobile_number" className="element">
                <div
                    onFocus={() => {
                        scrollToElement('personal_mobile_number');
                    }}
                >
                    {representing == 'entity' ? (
                        <TextField
                            autoComplete="tel"
                            style={{ margin: '4px 0px 0px 0px' }}
                            label="Mobile Number"
                            value={this.state.company_details ? this.state.company_details.tel : ''}
                            onChange={(e) => {
                                let company_details = this.state.company_details;
                                if (!company_details) {
                                    company_details = {};
                                }
                                company_details.tel = e.target.value;
                                this.setState({ company_details });
                            }}
                            fullWidth
                            // type="number"
                        />
                    ) : (
                        <TextField
                            autoComplete="tel"
                            style={{ margin: '4px 0px 0px 0px' }}
                            label="Mobile Number"
                            value={this.state.personal_mobile_number}
                            onChange={this.handleFieldChange('personal_mobile_number')}
                            fullWidth
                            // type="number"
                        />
                    )}
                </div>
            </Element>
        );

        const emailComponent = (
            <Element name="personal_email" className="element">
                <div
                    onFocus={() => {
                        scrollToElement('personal_email');
                    }}
                >
                    {representing == 'entity' ? (
                        <TextField
                            autoComplete="email"
                            style={{ margin: '4px 0px 0px 0px' }}
                            label="Email ( of Company representative )"
                            value={this.state.company_details ? this.state.company_details.rep_email : ''}
                            onChange={(e) => {
                                let company_details = this.state.company_details;
                                if (!company_details) {
                                    company_details = {};
                                }
                                company_details.rep_email = e.target.value;
                                this.setState({ company_details });
                            }}
                            fullWidth
                            type="email"
                        />
                    ) : (
                        <TextField
                            autoComplete="email"
                            style={{ margin: '4px 0px 0px 0px' }}
                            label="Email ( of Company representative )"
                            value={this.state.personal_email}
                            onChange={this.handleFieldChange('personal_email')}
                            fullWidth
                            type="email"
                        />
                    )}
                </div>
            </Element>
        );
        const ownerNameComponent = (
            <Element name="ownerName" className="element">
                <div
                    onFocus={() => {
                        scrollToElement('ownerName');
                    }}
                >
                    <TextField
                        autoComplete="owner name"
                        style={{ margin: '4px 0px 0px 0px' }}
                        label="Name (of 1 Company owner)"
                        value={this.state.company_details ? this.state.company_details.owner_name : ''}
                        onChange={(e) => {
                            let company_details = this.state.company_details;
                            if (!company_details) {
                                company_details = {};
                            }
                            company_details.owner_name = e.target.value;
                            this.setState({ company_details });
                        }}
                        fullWidth
                    />
                </div>
            </Element>
        );
        const ownerNumberComponent = (
            <Element name="ownerNumber" className="element">
                <div
                    onFocus={() => {
                        scrollToElement('ownerNumber');
                    }}
                >
                    <TextField
                        autoComplete="tel"
                        style={{ margin: '4px 0px 0px 0px' }}
                        label="Mobile Number (of 1 Company owner)"
                        value={this.state.company_details ? this.state.company_details.owner_tel : ''}
                        onChange={(e) => {
                            let company_details = this.state.company_details;
                            if (!company_details) {
                                company_details = {};
                            }
                            company_details.owner_tel = e.target.value;
                            this.setState({ company_details });
                        }}
                        fullWidth
                    />
                </div>
            </Element>
        );
        const ownerEmailComponent = (
            <Element name="ownerEmail" className="element">
                <div
                    onFocus={() => {
                        scrollToElement('ownerEmail');
                    }}
                >
                    <TextField
                        autoComplete="email"
                        style={{ margin: '4px 0px 0px 0px' }}
                        label="Email (of 1 Company owner)"
                        value={this.state.company_details ? this.state.company_details.owner_email : ''}
                        onChange={(e) => {
                            let company_details = this.state.company_details;
                            if (!company_details) {
                                company_details = {};
                            }
                            company_details.owner_email = e.target.value;
                            this.setState({ company_details });
                        }}
                        fullWidth
                        type="email"
                    />
                </div>
            </Element>
        );

        const occupation = (
            <Element name="personal_occupation" className="element">
                <div
                    onFocus={() => {
                        scrollToElement('personal_occupation');
                    }}
                    style={{ marginTop: '4px' }}
                >
                    <SelectOccupation
                        occupation={this.state.personal_occupation}
                        handleOccupationFieldChange={this.handleOccupationFieldChange}
                        type="personal"
                        placeholder="Select Occupation"
                    />
                </div>
            </Element>
        );

        const self_employed = (
            <div style={{ marginTop: '20px', textAlign: 'center' }}>
                <FormControlLabel
                    style={{ margin: '0px 10px 0px 10px' }}
                    control={
                        <Radio
                            style={{ padding: '6px' }}
                            name="personal_self_employed"
                            checked={!this.state.personal_self_employed}
                            onChange={() => {
                                this.setState({ personal_self_employed: false });
                            }}
                        />
                    }
                    label="Employed"
                />
                <FormControlLabel
                    style={{ margin: '0px 10px 0px 10px' }}
                    control={
                        <Radio
                            style={{ padding: '6px' }}
                            name="personal_self_employed"
                            checked={this.state.personal_self_employed}
                            onChange={() => {
                                this.setState({ personal_self_employed: true });
                            }}
                        />
                    }
                    label="Self-employed"
                />
            </div>
        );

        const bank_details = (
            <div>
                <Element name="personal_bank" className="element">
                    <div
                        onFocus={() => {
                            scrollToElement('personal_bank');
                        }}
                    >
                        <div style={{ textAlign: 'center' }}>
                            <Typography variant="subtitle1" style={{ margin: '20px 0px 0px 0px' }}>
                                {representing == 'entity' ? (
                                    <div>
                                        Payer (company) Bank Account Details <br />
                                        {this.state.entity_claims_pay_to_same === false
                                            ? '(PREMIUMBACK):'
                                            : '(PREMIUMBACK & CLAIMS):'}
                                    </div>
                                ) : (
                                    <div>
                                        Payer Bank Account Details <br />
                                        {representing === 'family' && this.state.family_claims_pay_to_same === false
                                            ? '(PREMIUMBACK):'
                                            : '(PREMIUMBACK & CLAIMS):'}
                                    </div>
                                )}
                            </Typography>
                        </div>
                        <FormControl style={{ width: '100%', marginTop: '0px' }}>
                            <InputLabel>Bank (Singapore)</InputLabel>
                            <Select
                                value={
                                    representing == 'entity'
                                        ? this.state.company_details
                                            ? this.state.company_details.bank
                                            : ''
                                        : this.state.personal_bank
                                }
                                onChange={
                                    representing == 'entity'
                                        ? (e) => {
                                              let company_details = this.state.company_details;
                                              if (!company_details) {
                                                  company_details = {};
                                              }
                                              company_details.bank = e.target.value;
                                              this.setState({ company_details });
                                          }
                                        : this.handleFieldChange('personal_bank')
                                }
                            >
                                <MenuItem value={'dbs'}>DBS/POSB (preferred)</MenuItem>
                                <MenuItem value={'ocbc'}>OCBC</MenuItem>
                                <MenuItem value={'uob'}>UOB</MenuItem>
                                <MenuItem value={'sc'}>Standard Charted</MenuItem>
                                <MenuItem value={'hsbc'}>HSBC </MenuItem>
                                <MenuItem value={'citi'}>Citibank</MenuItem>
                                <MenuItem value={'maybank'}>Maybank</MenuItem>
                                <MenuItem value={'rhb'}>RHB</MenuItem>
                                <MenuItem value={'cimb'}>CIMB</MenuItem>
                                <MenuItem value={'bo_china'}>Bank of China</MenuItem>
                                <MenuItem value={'deutsche'}> Deutsche Bank </MenuItem>
                                <MenuItem value={'hl_bank'}> HL Bank</MenuItem>
                                <MenuItem value={'bnp_paribas'}> BNP Paribas</MenuItem>
                                <MenuItem value={'far_eastern'}> Far Eastern Bank</MenuItem>
                                <MenuItem value={'mizuho'}> Mizuho Bank</MenuItem>
                                <MenuItem value={'sumitomo_mitsui'}>Sumitomo Mitsui Banking Corporation</MenuItem>
                                <MenuItem value={'bo_tokyo_mitsubishi_ufj'}>The Bank of Tokyo-Mitsubishi UFJ</MenuItem>
                                <MenuItem value={'bo_scotland'}>The Royal Bank of Scotland</MenuItem>
                                <MenuItem value={'aus_nz_banking_grp'}>
                                    Australia and New Zealand Banking Group
                                </MenuItem>
                            </Select>
                        </FormControl>
                    </div>
                </Element>

                <Element name="personal_bank_acct_number" className="element">
                    <div
                        onFocus={() => {
                            scrollToElement('personal_bank_acct_number');
                        }}
                    >
                        {representing == 'entity' ? (
                            <TextField
                                style={{ margin: '4px 0px 0px 0px' }}
                                label="Bank account number (Singapore)"
                                value={this.state.company_details ? this.state.company_details.bank_acct_number : ''}
                                onChange={(e) => {
                                    let company_details = this.state.company_details;
                                    if (!company_details) {
                                        company_details = {};
                                    }
                                    company_details.bank_acct_number = e.target.value;
                                    this.setState({ company_details });
                                }}
                                fullWidth
                                // type="number"
                            />
                        ) : (
                            <TextField
                                style={{ margin: '4px 0px 0px 0px' }}
                                label="Bank account number (Singapore)"
                                value={this.state.personal_bank_acct_number}
                                onChange={this.handleFieldChange('personal_bank_acct_number')}
                                fullWidth
                                // type="number"
                            />
                        )}
                    </div>
                </Element>
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={this.state.personal_ack_50_cents_fee}
                            onChange={this.handleCheckboxToggle('personal_ack_50_cents_fee')}
                            value="ack_50_cents_fee"
                            color="primary"
                        />
                    }
                    // label={<span style={{ color: "rgba(0, 0, 0, 0.54)", fontSize: "13px", lineHeight: 0 }}>I acknowledge that a fee of $0.50 will be imposed on all Premiumback & Claims payouts. FREE for payouts to DBS/POSB accounts</span>}
                    label={
                        <span
                            style={{
                                color: 'rgba(0, 0, 0, 0.54)',
                                fontSize: '13px',
                                lineHeight: 0
                            }}
                        >
                            I am aware that if the Singapore bank account I provide is not a POSB/DBS bank account, the
                            bank will impose a 50 cents charge to every transaction ( Claims and PremiumBack ) I receive
                            from www.iFinSG.com.
                        </span>
                    }
                    style={{ margin: '30px 0px 0px 0px' }}
                />
            </div>
        );

        const upload_image = (
            <div>
                <div style={{ textAlign: 'center' }}>
                    <Typography variant="subtitle1" style={{ margin: '30px 0px 10px 0px' }}>
                        Upload NRIC/Passport (front & back):
                    </Typography>
                </div>

                <ImageUpload
                    image_type="SIGNUP-IMAGE"
                    username={this.state.login_email}
                    maxFiles={this.state.login_email.length}
                    type={'personal'}
                    fileUploadedPersonal={this.fileUploadedPersonal}
                    fileRemovedPersonal={this.fileRemovedPersonal}
                />
            </div>
        );

        const arrayOfPersonalInfoFields1 = [full_name, preferred_name, nric_number];
        let arrayOfPersonalInfoFields2 = [];
        if (representing !== 'entity') {
            arrayOfPersonalInfoFields2 = [gender, DOB, place_of_birth];
        }
        arrayOfPersonalInfoFields2.push(HP_number);
        if (representing === 'entity') {
            arrayOfPersonalInfoFields2.push(emailComponent);
            arrayOfPersonalInfoFields2.push(ownerNameComponent);
            arrayOfPersonalInfoFields2.push(ownerNumberComponent);
            arrayOfPersonalInfoFields2.push(ownerEmailComponent);
        }
        if (representing !== 'entity') {
            arrayOfPersonalInfoFields2 = arrayOfPersonalInfoFields2.concat([occupation, self_employed]);
        }

        let arrayOfPersonalInfoFields3 = [];
        if (representing !== 'entity') {
            arrayOfPersonalInfoFields3.push(upload_image);
        }
        arrayOfPersonalInfoFields3 = arrayOfPersonalInfoFields3.concat([
            bank_details,
            <Button
                style={{ float: 'right', margin: '20px 0px 0px 0px' }}
                onClick={() => {
                    this.handleNextAfterPersonalInfo();
                }}
            >
                OK
            </Button>
        ]);

        return (
            <div>
                <form
                    onSubmit={() => {
                        scrollToElement('personal_gender');
                    }}
                >
                    {arrayOfPersonalInfoFields1}
                </form>
                <form>{arrayOfPersonalInfoFields2}</form>
                {arrayOfPersonalInfoFields3}
            </div>
        );
    };

    getSelectValue = (field) => {
        if (field.value) {
            return field.value;
        } else {
            return field;
        }
    };

    fileUploadedPersonal = (uploaded_file_name) => {
        let personal_uploaded_images = GlobalHelpers.clone_deep(this.state.personal_uploaded_images);
        personal_uploaded_images.push(uploaded_file_name);
        this.setState({ personal_uploaded_images });
    };

    fileRemovedPersonal = (uploaded_file_name) => {
        let personal_uploaded_images = GlobalHelpers.clone_deep(this.state.personal_uploaded_images);
        personal_uploaded_images.splice(personal_uploaded_images.indexOf(uploaded_file_name), 1); // Remove uploaded_file_name from the array personal_uploaded_images

        this.setState({ personal_uploaded_images });
    };

    renderPersonalFilledDetails = () => {
        let renderable = [];
        const personal_full_name = (
            <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                <Typography style={{ fontSize: '13px', color: 'grey' }}>{this.state.personal_full_name}</Typography>
            </Grid>
        );
        const preferred_name = (
            <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                <Typography style={{ fontSize: '13px', color: 'grey' }}>
                    {this.state.personal_preferred_name}
                </Typography>
            </Grid>
        );
        const personal_nric_number = (
            <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                <Typography style={{ fontSize: '13px', color: 'grey' }}>{this.state.personal_nric_number}</Typography>
            </Grid>
        );
        const personal_gender = () => {
            let gender = '';
            switch (this.state.personal_gender) {
                case 'M':
                    return (
                        <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                            <Typography style={{ fontSize: '13px', color: 'grey' }}>Male</Typography>
                        </Grid>
                    );
                case 'F':
                    return (
                        <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                            <Typography style={{ fontSize: '13px', color: 'grey' }}>Female</Typography>
                        </Grid>
                    );
                default:
                    // Do nothing
                    break;
            }
        };

        const personal_dob = () => {
            // if (this.state.personal_dob !== "") {
            if (typeof this.state.personal_dob.getMonth === 'function') {
                const date_obj = new Date(this.state.personal_dob);
                const year = GlobalHelpers.padNumber(date_obj.getFullYear(), 4);
                const month = GlobalHelpers.padNumber(date_obj.getMonth() + 1, 2);
                const date = GlobalHelpers.padNumber(date_obj.getDate(), 2);
                const display_dob = date + '-' + month + '-' + year;

                return (
                    <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                        {month === 'NaN' ? (
                            ''
                        ) : (
                            <Typography style={{ fontSize: '13px', color: 'grey' }}>{display_dob}</Typography>
                        )}
                    </Grid>
                );
            }
        };

        const personal_place_of_birth = (
            <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                <Typography style={{ fontSize: '13px', color: 'grey' }}>
                    {this.state.personal_place_of_birth ? this.getSelectValue(this.state.personal_place_of_birth) : ''}
                </Typography>
            </Grid>
        );
        const personal_mobile_number = (
            <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                <Typography style={{ fontSize: '13px', color: 'grey' }}>{this.state.personal_mobile_number}</Typography>
            </Grid>
        );
        const personal_occupation = (
            <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                <Typography style={{ fontSize: '13px', color: 'grey' }}>
                    {this.state.personal_occupation
                        ? this.getSelectValue(this.state.personal_occupation) +
                          (this.state.personal_self_employed ? ' (self-employed)' : ' (employed)')
                        : ''}
                </Typography>
            </Grid>
        );

        const personal_uploaded_images = () => {
            let personal_uploaded_images_state = this.state.personal_uploaded_images;
            let personal_uploaded_images = '';

            for (let i = 0; i < personal_uploaded_images_state.length; i++) {
                if (i !== 0) {
                    personal_uploaded_images = personal_uploaded_images + ', ';
                }
                // personal_uploaded_images = personal_uploaded_images + GlobalHelpers.showPreview(personal_uploaded_images_state[i].split("_")[2],20)
                personal_uploaded_images = personal_uploaded_images + personal_uploaded_images_state[i].split('_')[4];
            }

            return (
                <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                    <Typography style={{ fontSize: '13px', color: 'grey' }}>{personal_uploaded_images}</Typography>
                </Grid>
            );
        };
        const personal_bank = (
            <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                <Typography style={{ fontSize: '13px', color: 'grey' }}>
                    {bank_full_name_from_short_name[this.state.personal_bank]}
                </Typography>
            </Grid>
        );
        const personal_bank_acct_number = (
            <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                <Typography style={{ fontSize: '13px', color: 'grey' }}>
                    {this.state.personal_bank_acct_number}
                </Typography>
            </Grid>
        );

        if (this.state.personal_info_already_in_db) {
            renderable = [
                personal_full_name,
                preferred_name,
                personal_nric_number,
                personal_gender(),
                personal_dob(),
                personal_place_of_birth,
                personal_mobile_number,
                personal_occupation,
                personal_bank,
                personal_bank_acct_number
            ];
        } else {
            renderable = [
                personal_full_name,
                preferred_name,
                personal_nric_number,
                personal_gender(),
                personal_dob(),
                personal_place_of_birth,
                personal_mobile_number,
                personal_occupation,
                personal_uploaded_images(),
                personal_bank,
                personal_bank_acct_number
            ];
        }

        return (
            <Grid container spacing={8} style={{ padding: '4px 0px 0px 0px', lineHeight: 0.5 }}>
                {renderable}
            </Grid>
        );
    };

    renderCompanyPayerDetails = () => {
        let renderable = [];
        let company_details = null;
        if (this.state.user_info && this.state.user_info.company_details) {
            company_details = this.state.user_info.company_details;
        } else {
            company_details = this.state.company_details;
        }
        if (!company_details) {
            return;
        }

        const renderString = (variable) => {
            if (!variable) {
                return;
            }
            return (
                <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                    <Typography style={{ fontSize: '13px', color: 'grey' }}>{variable}</Typography>
                </Grid>
            );
        };

        const render_bank = (bank) => {
            if (!bank) {
                return;
            } else {
                return (
                    <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                        <Typography style={{ fontSize: '13px', color: 'grey' }}>
                            {bank_full_name_from_short_name[bank]}
                        </Typography>
                    </Grid>
                );
            }
        };

        renderable = [
            renderString(company_details.name),
            renderString(company_details.uen),
            renderString(company_details.rep_name),
            renderString(company_details.rep_job_title),
            renderString(company_details.rep_nric),
            renderString(company_details.tel),
            renderString(company_details.rep_email),
            renderString(company_details.owner_name),
            renderString(company_details.owner_tel),
            renderString(company_details.owner_email),
            render_bank(company_details.bank),
            renderString(company_details.bank_acct_number)
        ];

        return (
            <Grid container spacing={8} style={{ padding: '4px 0px 0px 0px', lineHeight: 0.5 }}>
                {renderable}
            </Grid>
        );
    };

    validate_personal_info = () => {
        const { representing } = this.state || {};

        try {
            const personal_full_name = this.state.personal_full_name;
            const personal_preferred_name = this.state.personal_preferred_name;
            const personal_nric_number = this.state.personal_nric_number;
            const personal_gender = this.state.personal_gender;
            const personal_dob = this.state.personal_dob;
            const personal_place_of_birth = this.state.personal_place_of_birth;
            const personal_mobile_number = this.state.personal_mobile_number;
            const personal_occupation = this.state.personal_occupation;
            const personal_uploaded_images = this.state.personal_uploaded_images;
            const personal_bank_acct_number = this.state.personal_bank_acct_number;
            const personal_bank = this.state.personal_bank;
            const personal_ack_50_cents_fee = this.state.personal_ack_50_cents_fee;

            if (representing == 'entity') {
                if (!this.state.company_details) {
                    alert('Some company details not filled in');
                    return false;
                } else if (!this.state.company_details.bank) {
                    alert('Bank not filled in');
                    return false;
                } else if (!this.state.company_details.bank_acct_number) {
                    alert('Bank account number not filled in');
                    return false;
                } else if (!this.state.company_details.name) {
                    alert('Company name not filled in');
                    return false;
                } else if (!this.state.company_details.owner_email) {
                    alert('Owner email not filled in');
                    return false;
                } else if (!this.state.company_details.owner_name) {
                    alert('Owner name not filled in');
                    return false;
                } else if (!this.state.company_details.owner_tel) {
                    alert('Owner tel not filled in');
                    return false;
                } else if (!this.state.company_details.rep_email) {
                    alert('Representative email not filled in');
                    return false;
                } else if (!this.state.company_details.rep_job_title) {
                    alert('Representative job title not filled in');
                    return false;
                } else if (!this.state.company_details.rep_name) {
                    alert('Representative name not filled in');
                    return false;
                } else if (!this.state.company_details.rep_nric) {
                    alert('Representative NRIC not filled in');
                    return false;
                } else if (!this.state.company_details.tel) {
                    alert('Tel not filled in');
                    return false;
                } else if (!this.state.company_details.uen) {
                    alert('UEN not filled in');
                    return false;
                }
            } else {
                if (personal_full_name === '') {
                    alert('Error - Full Name is not filled in');
                    return false;
                } else if (personal_preferred_name === '') {
                    alert('Error - Preferred name is not filled in');
                    return false;
                } else if (personal_nric_number === '') {
                    alert('Error - NRIC number is not filled in');
                    return false;
                } else if (personal_mobile_number === '') {
                    alert('Error - Personal Mobile number is not filled in');
                    return false;
                } else if (personal_bank === '') {
                    alert("Error - Bank to credit claims payouts in 'Payer Details' is not filled in");
                    return false;
                } else if (personal_bank_acct_number === '') {
                    alert("Error - Bank account number to credit claims payouts  in 'Payer Details' is not filled in");
                    return false;
                } else if (personal_ack_50_cents_fee === false) {
                    alert(
                        "Error - Please acknowledge terms and conditions for payouts to bank account  in 'Payer Details'"
                    );
                    return false;
                } else if (representing !== 'entity') {
                    if (personal_gender === '') {
                        alert('Error - Gender is not filled in');
                        return false;
                    } else if (personal_dob === '') {
                        alert('Error - DOB is not filled in');
                        return false;
                    } else if (personal_place_of_birth === '') {
                        alert('Error - Place of birth is not filled in');
                        return false;
                    } else if (personal_occupation === '') {
                        alert('Error - Personal occupation is not filled in');
                        return false;
                    } else if (JSON.stringify(personal_uploaded_images) === JSON.stringify([])) {
                        alert('Error - No images are uploaded');
                        return false;
                    }
                }
            }
        } catch (err) {}

        return true;
    };

    handleNextAfterPersonalInfo = () => {
        // Form validation
        if (!do_not_validate) {
            if (!this.validate_personal_info()) {
                return;
            }

            if (this.state.representing === 'self' && !this.validate_special_small_accident_max_age('personal')) {
                return;
            }
        }

        this.nextPanelAfterPersonalInfo();
    };

    nextPanelAfterPersonalInfo = () => {
        if (this.state.num_of_insured === 0) {
            this.handleAddInsured();
        }

        if (this.state.representing === 'self') {
            //representing === "self"

            this.configureExpansionPanels(['personal_info_collapse', 'insured_info_invisible', 'confirm_expand']).then(
                () => {
                    window.scrollTo(0, 0);
                }
            );
        } else {
            this.configureExpansionPanels(['insured_info_visible', 'insured_info_expand']).then(() => {
                window.scrollTo(0, 0);
            });
        }
    };

    /////////////////////////////////////////////////////////
    ////////////// Insured Info Expansion Panel /////////////
    /////////////////////////////////////////////////////////

    calculateExistingNumberAlreadyBoughtFor = (group) => {
        let number = 0;
        let policies = this.state.policies;
        let product = this.state.products[0];
        let dictOfInsured = {};
        // Eg { "Full Name Of Person" : true }
        let arrayOfPolicies = Object.keys(policies);

        for (let i = 0; i < arrayOfPolicies.length; i++) {
            // For each policy, check group purchase, add to dictOfInsured the full_name of the insured person
            let policy = policies[arrayOfPolicies[i]];
            try {
                policy[group].forEach((insured) => {
                    if (!dictOfInsured[insured.full_name]) {
                        dictOfInsured[insured.full_name] = true;
                    }
                });
            } catch (err) {
                console.log(err);
                continue;
            }
        }


        return Object.keys(dictOfInsured).length;
    };

    renderInsuredInfoFieldsCards = () => {
        let arrayOfInsuredInfoFieldsCards = [];

        const num_of_insured = this.state.num_of_insured;

        for (let i = 0; i < num_of_insured; i++) {
            let insuredInfoFieldsCard = (
                <div>
                    <ExpansionPanelDetails style={{ padding: '5px 5px 5px 5px' }}>
                        <div
                            style={{
                                padding: '10px 40px 25px 40px',
                                width: '100%',
                                marginBottom: '15px'
                            }}
                        >
                            {i === 0 ? (
                                <div />
                            ) : (
                                <div style={{ textAlign: 'right' }}>
                                    <IconButton
                                        onClick={() => {
                                            this.handleSubtractInsured(i);
                                        }}
                                        aria-label="Delete"
                                        color="black"
                                    >
                                        <CloseIcon style={{ color: 'red' }} />
                                    </IconButton>
                                </div>
                            )}

                            <div>{this.renderInsuredInfoFields(i)}</div>
                        </div>
                    </ExpansionPanelDetails>
                    <Divider style={{ margin: '20px 20px 10px 20px' }} />
                </div>
            );

            arrayOfInsuredInfoFieldsCards.push(insuredInfoFieldsCard);
        }

        return arrayOfInsuredInfoFieldsCards;
    };

    renderInsuredInfoFields = (i) => {
        const { boughtPolicies } = this.state;
        let arrayOfInsuredInfoFields1 = [];
        let arrayOfInsuredInfoFields2 = [];
        let arrayOfInsuredInfoFields3 = [];

        // let userHasCompanyDetailsSavedInDB = false
        let userHasBoughtForFamilyBefore = false;
        let userHasBoughtForEntityBefore = false;

        // if (this.state.user_info && this.state.user_info.company_details) {
        //   userHasCompanyDetailsSavedInDB = true
        // }
        ///// Checking if any products has been bought for FAMILY
        let policies = null;
        if (this.state.user_info && this.state.user_info.policies) {
            policies = this.state.user_info.policies;
        }
        if (policies) {
            for (let i = 0; i < policies.length; i++) {
                if (policies[i].representing == 'family') {
                    userHasBoughtForFamilyBefore = true;
                } else if (policies[i].representing == 'entity') {
                    userHasBoughtForEntityBefore = true;
                }
            }
        }

        const buy_for_all_members = (group) => {
            return (
                <div>
                    <Button
                        variant="contained"
                        style={{ margin: '0px 0px 30px 0px', backgroundColor: 'green', color: 'white' }}
                        onClick={() => {
                            let setStateObject = { buy_for_all_members: true };
                            this.setState(setStateObject, () => {
                                // Calculate how many members to buy for, then go to confirm page
                                this.calcBasketPrices(this.calculateExistingNumberAlreadyBoughtFor(group));
                                this.configureExpansionPanels(['confirm_expand']).then(() => {
                                    window.scrollTo(0, 0);
                                });
                            });
                        }}
                    >
                        Buy this cover for all existing {group == 'family' ? 'FAMILY members' : 'COMPANY staff'} with
                        iFinSG!
                    </Button>

                    {/* <FormControlLabel
            control={
              <Checkbox
                checked={this.state.buy_for_all_members}
                onChange={(e) => {
                  let checked = e.target.checked
                  let setStateObject = { buy_for_all_members: checked }
                  this.setState(setStateObject, () => {
                    if (checked) {
                      // Calculate how many members to buy for, then go to confirm page
                      this.calcBasketPrices(this.calculateExistingNumberAlreadyBoughtFor(group))
                      this.configureExpansionPanels(["confirm_expand"]).then(() => { window.scrollTo(0, 0); });
                    }
                  })
                }}
                value='ack_pdpa'
                color='primary'
              />
            }
            // label={<span style={{ color: "rgba(0, 0, 0, 0.54)", fontSize: "13px", lineHeight: 0 }}>I acknowledge that a fee of $0.50 will be imposed on all Premiumback & Claims payouts. FREE for payouts to DBS/POSB accounts</span>}
            label={
              <span
                style={{
                  color: "rgba(0, 0, 0, 0.54)",
                  fontSize: "13px",
                  lineHeight: 0
                }}
              >
                I wish to buy this cover for all existing {group == 'family' ? 'FAMILY' : 'COMPANY'} members with iFinSG.
              </span>
            }
            style={{ margin: "0px 0px 40px 0px" }}
          /> */}
                </div>
            );
        };

        const relationship_to_payer = (
            <FormControl style={{ marginBottom: '0px' }} fullWidth>
                <InputLabel style={{ width: '100%' }}>I am financially protecting my</InputLabel>
                <Select
                    value={this.state.insured[i].relationship_to_payer}
                    onChange={this.handleInsuredFieldChange(i, 'relationship_to_payer')}
                >
                    <MenuItem value={'child'}>Child</MenuItem>
                    <MenuItem value={'spouse'}>Spouse</MenuItem>
                    <MenuItem value={'parent'}>Parent</MenuItem>
                </Select>
            </FormControl>
        );

        const index_insured = (
            <Element name={'member_index_' + i} className="element">
                <div>
                    <Typography style={{ fontSize: '13px', fontWeight: 'bold' }}>
                        {this.state.representing === 'entity' &&
                            `Company Staff ${i + 1 + (boughtPolicies ? boughtPolicies.length : 0)}`}
                        {this.state.representing === 'family' &&
                            `Family Member ${i + 1 + (boughtPolicies ? boughtPolicies.length : 0)}`}
                    </Typography>
                </div>
            </Element>
        );

        const insured_full_name = (
            <Element name={'full_name_' + i} className="element">
                <div
                    onFocus={() => {
                        scrollToElement('full_name_' + i);
                    }}
                    style={{ marginTop: '4px' }}
                >
                    <TextField
                        autoComplete="off"
                        label="Full name in English (NRIC/Passport)"
                        value={this.state.insured[i].full_name}
                        onChange={this.handleInsuredFieldChange(i, 'full_name')}
                        fullWidth
                    />
                </div>
            </Element>
        );

        const insured_preffered_name = (
            <Element name={'preferred_name_' + i} className="element">
                <div
                    onFocus={() => {
                        scrollToElement('preferred_name_' + i);
                    }}
                >
                    <TextField
                        autoComplete="off"
                        style={{ margin: '5px 0px 0px 0px' }}
                        label="Preferred Name"
                        value={this.state.insured[i].preferred_name}
                        onChange={this.handleInsuredFieldChange(i, 'preferred_name')}
                        fullWidth
                    />
                </div>
            </Element>
        );

        const insured_nric_number = (
            <Element name={'nric_number_' + i} className="element">
                <div
                    onFocus={() => {
                        scrollToElement('nric_number_' + i);
                    }}
                >
                    <TextField
                        autoComplete="off"
                        inputRef={(node) => (input_refs['nric_number_' + i] = node)}
                        onKeyPress={(ev) => {
                            if (ev.key === 'Enter') {
                                if (input_refs['nric_number_' + i]) {
                                    input_refs['nric_number_' + i].blur();
                                }
                                scrollToElement('gender_' + i);
                            }
                        }}
                        style={{ margin: '5px 0px 0px 0px' }}
                        label="NRIC/Passport number"
                        value={this.state.insured[i].nric_number}
                        onChange={this.handleInsuredFieldChange(i, 'nric_number')}
                        fullWidth
                    />
                </div>
            </Element>
        );

        const gender = (
            <Element name={'gender_' + i} className="element">
                <div style={{ margin: '20px 0px 5px 0px', textAlign: 'center' }}>
                    <FormControlLabel
                        style={{ margin: '0px 10px 0px 10px' }}
                        control={
                            <Radio
                                style={{ padding: '6px' }}
                                name="gender"
                                checked={this.state.insured[i].gender === 'M'}
                                onChange={() => {
                                    let insured = this.state.insured;
                                    insured[i].gender = 'M';
                                    this.setState({ insured });
                                }}
                            />
                        }
                        label="Male"
                    />
                    <FormControlLabel
                        style={{ margin: '0px 10px 0px 10px' }}
                        control={
                            <Radio
                                style={{ padding: '6px' }}
                                name="gender"
                                checked={this.state.insured[i].gender === 'F'}
                                onChange={() => {
                                    let insured = this.state.insured;
                                    insured[i].gender = 'F';
                                    this.setState({ insured });
                                }}
                            />
                        }
                        label="Female"
                    />
                </div>
            </Element>
        );

        const insured_DOB = (
            <Element name={'dob_' + i} className="element">
                <div
                    onFocus={() => {
                        scrollToElement('dob_' + i);
                    }}
                >
                    <DatePickerRMC
                        date={this.state.insured[i].dob}
                        handleDateChange={this.handleDateChange('dob', i)}
                        placeholder="Date of Birth"
                        defaultDate="1992-01-01" // default = today
                        minDate="1850-01-01"
                        // maxDate = "1993-01-01" // default = today
                    />
                </div>
            </Element>
        );

        const insured_place_of_birth = (
            <Element name={'place_of_birth_' + i} className="element" style={{ marginTop: '4px' }}>
                <div
                    onFocus={() => {
                        scrollToElement('place_of_birth_' + i);
                    }}
                >
                    <SelectCountry
                        country={this.state.insured[i].place_of_birth}
                        handleCountryFieldChange={this.handlePlaceOfBirthFieldChange}
                        type="insured"
                        i={i}
                        placeholder="Place of Birth (as of NRIC/Passport)"
                    />
                </div>
            </Element>
        );

        const HP_number = (
            <Element name={'mobile_number_' + i} className="element">
                <div
                    onFocus={() => {
                        scrollToElement('mobile_number_' + i);
                    }}
                >
                    <TextField
                        autoComplete="off"
                        style={{ margin: '5px 0px 0px 0px' }}
                        label="Mobile Number"
                        value={this.state.insured[i].mobile_number}
                        onChange={this.handleInsuredFieldChange(i, 'mobile_number')}
                        fullWidth
                    />
                </div>
            </Element>
        );

        const insured_email = (
            <Element name={'email_' + i} className="element">
                <div
                    onFocus={() => {
                        scrollToElement('email_' + i);
                    }}
                    style={{ marginTop: '4px' }}
                >
                    <TextField
                        autoComplete="off"
                        label="Email"
                        value={this.state.insured[i].email}
                        onChange={this.handleInsuredFieldChange(i, 'email')}
                        fullWidth
                    />
                </div>
            </Element>
        );

        const occupation = (
            <Element name={'occupation_' + i} className="element">
                <div
                    onFocus={() => {
                        scrollToElement('occupation_' + i);
                    }}
                    style={{ marginTop: '4px' }}
                >
                    <SelectOccupation
                        placeholder="Select Occupation"
                        occupation={this.state.insured[i].occupation}
                        handleOccupationFieldChange={this.handleOccupationFieldChange}
                        type="insured"
                        i={i}
                    />
                </div>
            </Element>
        );

        const self_employed = (
            <div style={{ marginTop: '20px', textAlign: 'center' }}>
                <FormControlLabel
                    style={{ margin: '0px 10px 0px 10px' }}
                    control={
                        <Radio
                            style={{ padding: '6px' }}
                            name="self_employed"
                            checked={!this.state.insured[i].self_employed}
                            onChange={() => {
                                let insured = this.state.insured;
                                insured[i].self_employed = false;
                                this.setState({ insured });
                            }}
                        />
                    }
                    label="Employed"
                />

                <FormControlLabel
                    style={{ margin: '0px 10px 0px 10px' }}
                    control={
                        <Radio
                            style={{ padding: '6px' }}
                            name="self_employed"
                            checked={this.state.insured[i].self_employed}
                            onChange={() => {
                                let insured = this.state.insured;
                                insured[i].self_employed = true;
                                this.setState({ insured });
                            }}
                        />
                    }
                    label="Self-employed"
                />
            </div>
        );

        const bank_details_claims = (
            <div>
                <div style={{ textAlign: 'center' }}>
                    <Typography variant="subtitle1" style={{ margin: '30px 0px 0px 0px', whiteSpace: 'pre-wrap' }}>
                        Life Insured Bank Account Details <br />
                        (CLAIMS):
                    </Typography>
                </div>
                <div>
                    <Element name={'claims_bank_' + i} className="element">
                        <div
                            onFocus={() => {
                                scrollToElement('claims_bank_' + i);
                            }}
                        >
                            <FormControl style={{ width: '100%', marginTop: '0px' }}>
                                <InputLabel>Bank (Singapore)</InputLabel>
                                <Select
                                    value={this.state.insured[i].claims_bank}
                                    onChange={this.handleInsuredFieldChange(i, 'claims_bank')}
                                >
                                    <MenuItem value={'dbs'}>DBS/POSB (preferred)</MenuItem>
                                    <MenuItem value={'ocbc'}>OCBC</MenuItem>
                                    <MenuItem value={'uob'}>UOB</MenuItem>
                                    <MenuItem value={'sc'}>Standard Charted</MenuItem>
                                    <MenuItem value={'citi'}>Citibank</MenuItem>
                                    <MenuItem value={'maybank'}>Maybank</MenuItem>
                                    <MenuItem value={'rhb'}>RHB</MenuItem>
                                    <MenuItem value={'cimb'}>CIMB</MenuItem>
                                    <MenuItem value={'bo_singapore'}>Bank of Singapore</MenuItem>
                                    <MenuItem value={'bo_china'}>Bank of China</MenuItem>
                                    <MenuItem value={'bo_india'}>Bank of India</MenuItem>
                                </Select>
                            </FormControl>
                        </div>
                    </Element>

                    <Element name={'claims_bank_acct_number_' + i} className="element">
                        <div
                            onFocus={() => {
                                scrollToElement('claims_bank_acct_number_' + i);
                            }}
                        >
                            <TextField
                                style={{ margin: '4px 0px 0px 0px' }}
                                label="Bank account number (Singapore)"
                                value={this.state.insured[i].claims_bank_acct_number}
                                onChange={this.handleInsuredFieldChange(i, 'claims_bank_acct_number')}
                                fullWidth
                            />
                        </div>
                    </Element>

                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.state.insured[i].claims_ack_50_cents_fee}
                                onChange={this.handleInsuredCheckboxToggle(i, 'claims_ack_50_cents_fee')}
                                value="claims_ack_50_cents_fee"
                                color="primary"
                            />
                        }
                        label={
                            <span
                                style={{
                                    color: 'rgba(0, 0, 0, 0.54)',
                                    fontSize: '13px',
                                    lineHeight: 0
                                }}
                            >
                                I am aware that if the Singapore bank account I provide is not a POSB/DBS bank account,
                                the bank will impose a 50 cents charge to every transaction ( Claims and PremiumBack ) I
                                receive from www.iFinSG.com.
                            </span>
                        }
                        style={{ margin: '30px 0px 10px 0px' }}
                    />
                </div>
            </div>
        );

        const get_image_upload_label = () => {
            let label = 'Upload NRIC/Passport (front & back):';
            switch (this.state.insured[i].relationship_to_payer) {
                case 'child':
                    label = "Upload child's birth certificate:";
                    break;
                case 'spouse':
                    label = "Upload your spouse's NRIC/Passport (front & back):";
                    break;
                case 'parent':
                    label = "Upload your parent's NRIC/Passport (front & back):";
                    break;
                default:
                    break;
            }
            return label;
        };

        const image_upload = (
            <div>
                <div style={{ textAlign: 'center' }}>
                    <Typography variant="subtitle1" style={{ margin: '30px 0px 10px 0px' }}>
                        {get_image_upload_label()}
                    </Typography>
                </div>
                <ImageUpload
                    image_type="SIGNUP-IMAGE"
                    username={this.state.login_email}
                    maxFiles={3}
                    type={'insured'}
                    insuredIndex={i}
                    fileUploadedInsured={this.fileUploadedInsured}
                    fileRemovedInsured={this.fileRemovedInsured}
                />
            </div>
        );

        switch (this.state.representing) {
            case 'self':
                break;

            case 'family':
                arrayOfInsuredInfoFields1 = [
                    userHasBoughtForFamilyBefore && !this.state.isAddNewMember ? buy_for_all_members('family') : '',
                    relationship_to_payer,
                    index_insured,
                    insured_full_name,
                    insured_preffered_name,
                    insured_nric_number
                ];
                arrayOfInsuredInfoFields2 = [
                    gender,
                    insured_DOB,
                    insured_place_of_birth,
                    HP_number,
                    insured_email,
                    occupation
                ];
                arrayOfInsuredInfoFields3 = [self_employed, image_upload];
                if (!this.state.family_claims_pay_to_same) {
                    arrayOfInsuredInfoFields3.push(bank_details_claims);
                }

                break;
            case 'entity':
                arrayOfInsuredInfoFields1 = [
                    userHasBoughtForEntityBefore && !this.state.isAddNewMember ? buy_for_all_members('entity') : '',

                    index_insured,
                    insured_full_name,
                    insured_preffered_name,
                    insured_nric_number
                ];
                arrayOfInsuredInfoFields2 = [
                    gender,
                    insured_DOB,
                    insured_place_of_birth,
                    HP_number,
                    insured_email,
                    occupation
                ];
                arrayOfInsuredInfoFields3 = [self_employed, image_upload];
                if (!this.state.entity_claims_pay_to_same) {
                    arrayOfInsuredInfoFields3.push(bank_details_claims);
                }
                break;
            default:
                break;
        }

        return (
            <div>
                <form> {arrayOfInsuredInfoFields1} </form>
                <form> {arrayOfInsuredInfoFields2} </form>
                {arrayOfInsuredInfoFields3}
            </div>
        );
    };

    handleAddInsured = () => {
        const num_of_insured = this.state.num_of_insured;
        if (num_of_insured >= 20) {
            alert('Maximum number of applicants reached!');
            return;
        }

        const new_insured = {
            relationship_to_payer: '',
            full_name: '',
            preferred_name: '',
            nric_number: '',
            gender: '',
            dob: '',
            place_of_birth: '',
            mobile_number: '',
            email: '',
            occupation: '',
            self_employed: false,

            premiumback_bank: '',
            premiumback_bank_acct_number: '',
            premiumback_ack_50_cents_fee: false,

            claims_bank: '',
            claims_bank_acct_number: '',
            claims_ack_50_cents_fee: false,

            uploaded_images: []
        };

        let insured = '';

        if (this.state.insured) {
            insured = GlobalHelpers.clone_deep(this.state.insured);
            insured[num_of_insured] = new_insured;
        } else {
            insured = [new_insured];
        }

        this.setState(
            {
                insured: insured,
                num_of_insured: this.state.num_of_insured + 1
            },
            () => {
                this.calcBasketPrices();
            }
        );
    };

    handleSubtractInsured = (i) => {
        if (this.state.insured.length > 0) {
            let insured = GlobalHelpers.clone_deep(this.state.insured);
            insured.splice(i, 1); // removing that insured object in index i

            this.setState({
                insured: insured,
                num_of_insured: this.state.num_of_insured - 1
            });
        }
    };

    handleInsuredFieldChange = (i, prop) => (e) => {
        const fields_with_type_number = [
            'personal_mobile_number',
            'entity_uen',
            'personal_bank_acct_number',
            'mobile_number',
            'premiumback_bank_acct_number',
            'claims_bank_acct_number'
        ];

        if (fields_with_type_number.indexOf(prop) != -1) {
            if ('' === e.target.value || (isFinite(e.target.value) && !e.target.value.indexOf('.') != -1)) {
            } else {
                return;
            }
        }

        let insured = GlobalHelpers.clone_deep(this.state.insured);
        insured[i][prop] = e.target.value;
        this.setState({
            insured
        });
    };

    handleInsuredCheckboxToggle = (i, prop) => (e) => {
        let insured = GlobalHelpers.clone_deep(this.state.insured);
        insured[i][prop] = e.target.checked;
        this.setState({ insured });
    };

    fileUploadedInsured = (i, uploaded_file_name) => {
        let insured = GlobalHelpers.clone_deep(this.state.insured);
        insured[i].uploaded_images.push(uploaded_file_name);
        this.setState({ insured });
    };

    fileRemovedInsured = (i, uploaded_file_name) => {
        let insured = GlobalHelpers.clone_deep(this.state.insured);
        insured[i].uploaded_images.splice(insured[i].uploaded_images.indexOf(uploaded_file_name), 1);
        this.setState({ insured });
    };

    renderInsuredFilledDetails = () => {
        const { boughtPolicies, insured, representing } = this.state;

        let renderable = [];
        if (!this.state.insured || this.state.insured.length < 1) {
            // if (false) {
            // do nothing
        } else {
            let displayInsuredData = insured;
            if (boughtPolicies) {
                displayInsuredData = [...boughtPolicies, ...insured];
            }


            // Show all company staff who already purchased product
            if (false) {
                // if (this.state.representing == 'entity' && this.state.policies && this.state.policies[this.state.products[0]] && this.state.policies[this.state.products[0]].entity && this.state.policies[this.state.products[0]].entity.length > 0) {
                let entityPolicies = this.state.policies[this.state.products[0]].entity;

                renderable.push(
                    <Grid item xs={12} style={{ textAlign: 'left' }}>
                        <Typography style={{ fontSize: '13px', color: 'grey' }}>
                            Already Purchased this cover for:
                        </Typography>
                    </Grid>
                );

                for (let i = 0; i < entityPolicies.length; i++) {
                    renderable.push(
                        <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                            <Typography style={{ fontSize: '13px', color: 'grey' }}>
                                {entityPolicies[i].full_name}
                            </Typography>
                        </Grid>
                    );
                }
            }

            for (let i = 0; i < displayInsuredData.length; i++) {
                if (!displayInsuredData[i].full_name) {
                    continue;
                }

                const full_name = (
                    <Grid item xs={5} style={{ textAlign: 'left', paddingTop: '15px', paddingBottom: '15px' }}>
                        <Typography style={{ fontSize: '13px', color: 'grey' }}>
                            {displayInsuredData[i].full_name}
                        </Typography>
                    </Grid>
                );
                const preferred_name = (
                    <Grid item xs={5} style={{ textAlign: 'left', paddingTop: '15px', paddingBottom: '15px' }}>
                        <Typography style={{ fontSize: '13px', color: 'grey' }}>
                            {displayInsuredData[i].preferred_name}
                        </Typography>
                    </Grid>
                );
                const nric_number = (
                    <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                        <Typography style={{ fontSize: '13px', color: 'grey' }}>
                            {displayInsuredData[i].nric_number}
                        </Typography>
                    </Grid>
                );
                const gender = () => {
                    let gender = '';
                    switch (displayInsuredData[i].gender) {
                        case 'M':
                            return (
                                <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                                    <Typography style={{ fontSize: '13px', color: 'grey' }}>Male</Typography>
                                </Grid>
                            );
                        case 'F':
                            return (
                                <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                                    <Typography style={{ fontSize: '13px', color: 'grey' }}>Female</Typography>
                                </Grid>
                            );
                        default:
                            // Do nothing
                            break;
                    }
                };

                const dob = () => {
                    if (displayInsuredData[i].dob !== '') {
                        const date_obj = new Date(displayInsuredData[i].dob);
                        const year = GlobalHelpers.padNumber(date_obj.getFullYear(), 4);
                        const month = GlobalHelpers.padNumber(date_obj.getMonth() + 1, 2);
                        const date = GlobalHelpers.padNumber(date_obj.getDate(), 2);
                        const display_dob = date + '-' + month + '-' + year;

                        return (
                            <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                                {month === 'NaN' ? (
                                    ''
                                ) : (
                                    <Typography style={{ fontSize: '13px', color: 'grey' }}>{display_dob}</Typography>
                                )}
                            </Grid>
                        );
                    }
                };

                const place_of_birth = (
                    <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                        <Typography style={{ fontSize: '13px', color: 'grey' }}>
                            {displayInsuredData[i].place_of_birth ? displayInsuredData[i].place_of_birth.value : ''}
                        </Typography>
                    </Grid>
                );
                const mobile_number = (
                    <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                        <Typography style={{ fontSize: '13px', color: 'grey' }}>
                            {displayInsuredData[i].mobile_number}
                        </Typography>
                    </Grid>
                );
                const email = (
                    <Grid item xs={5} style={{ textAlign: 'left', paddingTop: '15px', paddingBottom: '15px' }}>
                        <Typography style={{ fontSize: '13px', color: 'grey' }}>
                            {displayInsuredData[i].email}
                        </Typography>
                    </Grid>
                );
                {
                    /* const occupation = (
          <Grid
            item
            xs={5}
            style={{ textAlign: "left", padding: "0px 0px 0px 4px" }}
          >
            <Typography style={{ fontSize: "13px", color: "grey" }}>
              {displayInsuredData[i].occupation
                ? displayInsuredData[i].occupation.value +
                (displayInsuredData[i].self_employed
                  ? " (self-employed)"
                  : " (employed)")
                : ""}
            </Typography>
          </Grid>
        ); */
                }
                {
                    /* 
        const uploaded_images = () => {
          let uploaded_images_state = displayInsuredData[i].uploaded_images;
          let uploaded_images = "";
          for (let i = 0; i < uploaded_images_state.length; i++) {
            if (i !== 0) {
              uploaded_images = uploaded_images + ", ";
            }
            uploaded_images =
              uploaded_images + uploaded_images_state[i].split("_")[4];
          }
          return (
            <Grid
              item
              xs={5}
              style={{ textAlign: "left", padding: "0px 0px 0px 4px" }}
            >
              <Typography style={{ fontSize: "13px", color: "grey" }}>
                {uploaded_images}
              </Typography>
            </Grid>
          );
        }; */
                }

                const bank_and_acct_number = () => {
                    let renderable = [];

                    const bank = (payout_type) => {
                        if (payout_type === 'claims') {
                            return (
                                <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                                    <Typography style={{ fontSize: '13px', color: 'grey' }}>
                                        {bank_full_name_from_short_name[displayInsuredData[i].claims_bank]}
                                    </Typography>
                                </Grid>
                            );
                        } else if (payout_type === 'premiumback') {
                            return (
                                <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                                    <Typography style={{ fontSize: '13px', color: 'grey' }}>
                                        {bank_full_name_from_short_name[displayInsuredData[i].premiumback_bank]}
                                    </Typography>
                                </Grid>
                            );
                        }
                    };

                    const bank_acct_number = (payout_type) => {
                        if (payout_type === 'claims') {
                            return (
                                <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                                    <Typography style={{ fontSize: '13px', color: 'grey' }}>
                                        {displayInsuredData[i].claims_bank_acct_number}
                                    </Typography>
                                </Grid>
                            );
                        } else if (payout_type === 'premiumback') {
                            return (
                                <Grid item xs={5} style={{ textAlign: 'left', padding: '0px 0px 0px 4px' }}>
                                    <Typography style={{ fontSize: '13px', color: 'grey' }}>
                                        {displayInsuredData[i].premiumback_bank_acct_number}
                                    </Typography>
                                </Grid>
                            );
                        }
                    };

                    switch (this.state.representing) {
                        case 'self':
                            // Nothing to render
                            break;

                        case 'family':
                            if (!this.state.family_claims_pay_to_same) {
                                renderable.push(bank('claims'));
                                renderable.push(bank_acct_number('claims'));
                            }
                            if (!this.state.family_premiumback_pay_to_same) {
                                renderable.push(bank('premiumback'));
                                renderable.push(bank_acct_number('premiumback'));
                            }
                            break;
                        case 'entity':
                            if (!this.state.entity_claims_pay_to_same) {
                                renderable.push(bank('claims'));
                                renderable.push(bank_acct_number('claims'));
                            }
                            if (!this.state.entity_premiumback_pay_to_same) {
                                renderable.push(bank('premiumback'));
                                renderable.push(bank_acct_number('premiumback'));
                            }
                            break;
                        case 'group':
                            if (!this.state.group_claims_pay_to_same) {
                                renderable.push(bank('claims'));
                                renderable.push(bank_acct_number('claims'));
                            }
                            if (!this.state.group_premiumback_pay_to_same) {
                                renderable.push(bank('premiumback'));
                                renderable.push(bank_acct_number('premiumback'));
                            }
                            break;
                        default:
                            // Should not happen
                            break;
                    }

                    return renderable;
                };

                renderable.push(
                    <Grid item xs={12} style={{ textAlign: 'left', padding: '5px 20px 5px 4px' }}>
                        <Divider />
                    </Grid>
                );
                // TODO: This is the one responsible for the overlapping tables. Overlap is fixed but the UI still looks broken
                renderable.push(
                    <Grid item xs={5} style={{ textAlign: 'left', paddingBottom: '0px', paddingTop: '15px' }}>
                        <Typography style={{ fontSize: '13px', color: 'grey', fontWeight: 'bold' }}>
                            {representing !== 'self' && representing === 'family' ? 'Family Member ' : 'Company Staff '}
                        </Typography>
                    </Grid>
                );

                renderable.push(full_name);
                renderable.push(preferred_name);
                {
                    /* renderable.push(nric_number); */
                }
                {
                    /* renderable.push(gender()); */
                }
                {
                    /* renderable.push(dob()); */
                }
                {
                    /* renderable.push(place_of_birth); */
                }
                {
                    /* renderable.push(mobile_number);  */
                }
                renderable.push(email);
                {
                    /* renderable.push(occupation); */
                }
                {
                    /* renderable.push(uploaded_images()); */
                }
                {
                    /* renderable.push(bank_and_acct_number()); */
                }
            }
        }

        return (
            <Grid container spacing={8} style={{ padding: '4px 0px 0px 0px', lineHeight: 0.5 }}>
                {renderable}
            </Grid>
        );
    };

    validate_insured_info = () => {
        const insured = this.state.insured;

        if (this.state.representing === 'self') {
            // If representing self, no need to validate
            return true;
        } else if (this.state.num_of_insured === 0) {
            alert('Must have at least 1 insured person');
            return false;
        } else {
            for (let i = 0; i < insured.length; i++) {
                const relationship_to_payer = insured[i].relationship_to_payer;
                const full_name = insured[i].full_name;
                const preferred_name = insured[i].preferred_name;
                const nric_number = insured[i].nric_number;
                const gender = insured[i].gender;
                const dob = insured[i].dob;
                const place_of_birth = insured[i].place_of_birth;

                const mobile_number = insured[i].mobile_number;
                const email = insured[i].email;
                const occupation = insured[i].occupation;
                const self_employed = insured[i].self_employed;

                const premiumback_bank = insured[i].premiumback_bank;
                const premiumback_bank_acct_number = insured[i].premiumback_bank_acct_number;

                const premiumback_ack_50_cents_fee = insured[i].premiumback_ack_50_cents_fee;
                const claims_bank = insured[i].claims_bank;
                const claims_bank_acct_number = insured[i].claims_bank_acct_number;
                const claims_ack_50_cents_fee = insured[i].claims_ack_50_cents_fee;
                const uploaded_images = insured[i].uploaded_images;

                if (full_name === '') {
                    alert('Error - Full name for insured member number ' + (i + 1) + ' is not filled in');
                    return false;
                } else if (preferred_name === '') {
                    alert('Error - Preferred name for ' + full_name + ' is not filled in');
                    return false;
                } else if (nric_number === '') {
                    alert('Error - NRIC for ' + full_name + ' is not filled in');
                    return false;
                } else if (gender === '') {
                    alert('Error - Gender for ' + full_name + ' is not filled in');
                    return false;
                } else if (dob === '') {
                    alert('Error - Date of birth for ' + full_name + ' is not filled in');
                    return false;
                } else if (place_of_birth === '') {
                    alert('Error - Place of birth for ' + full_name + ' is not filled in');
                    return false;
                } else if (mobile_number === '') {
                    alert('Error - Mobile number for ' + full_name + ' is not filled in');
                    return false;
                } else if (email === '') {
                    alert('Error - Email for ' + full_name + ' is not filled in');
                    return false;
                } else if (occupation === '') {
                    alert('Error - Occupation for ' + full_name + ' is not filled in');
                    return false;
                } else if (JSON.stringify(uploaded_images) === JSON.stringify([])) {
                    alert('Error - No images uploaded for ' + full_name);
                    return false;
                }

                let validate_bank_claims = false;
                let validate_bank_premiumback = false;

                switch (this.state.representing) {
                    case 'family': // Representing family
                        if (relationship_to_payer === '') {
                            alert('Error - How ' + full_name + ' is related to you is not filled in');
                            return false;
                        }
                        if (this.state.family_claims_pay_to_same) {
                        } else {
                            validate_bank_claims = true;
                        }
                        break;
                    case 'entity': // Representing entity
                        if (this.state.entity_claims_pay_to_same) {
                        } else {
                            validate_bank_claims = true;
                        }
                        break;
                    case 'group': // Representing group
                        validate_bank_premiumback = true;
                        validate_bank_claims = true;
                        break;
                    default:
                        // Not supposed to happen
                        break;
                }

                if (validate_bank_premiumback) {
                    if (premiumback_bank === '') {
                        alert('Error - Bank to credit PremiumBack payouts for ' + full_name + ' is not filled in');
                        return false;
                    } else if (premiumback_bank_acct_number === '') {
                        alert(
                            'Error - Bank account number to credit PremiumBack payouts for ' +
                                full_name +
                                ' is not filled in'
                        );
                        return false;
                    } else if (premiumback_ack_50_cents_fee === false) {
                        alert(
                            'Error - Please acknowledge terms and conditions for payouts to bank account for ' +
                                full_name
                        );
                        return false;
                    }
                }

                if (validate_bank_claims) {
                    if (claims_bank === '') {
                        alert('Error - Bank to credit claims payouts for ' + full_name + ' is not filled in');
                        return false;
                    } else if (claims_bank_acct_number === '') {
                        alert(
                            'Error - Bank account number to credit claims payouts for ' +
                                full_name +
                                ' is not filled in'
                        );
                        return false;
                    } else if (claims_ack_50_cents_fee === false) {
                        alert(
                            'Error - Please acknowledge terms and conditions for payouts to bank account for ' +
                                full_name
                        );
                        return false;
                    }
                }
            }
        }

        return true;
    };

    handleNextAfterInsuredInfo = () => {
        // Do form validation
        // Form validation
        if (!do_not_validate) {
            if (!this.validate_insured_info()) {
                // If not valid, break function with return
                return;
            }
            if (!this.validate_special_small_accident_max_age('insured')) {
                return;
            }
        }
        this.nextPanelAfterInsuredInfo();
    };

    nextPanelAfterInsuredInfo = () => {
        this.configureExpansionPanels(['confirm_expand']).then(() => {
            window.scrollTo(0, 0);
        });

        // let style_confirm = GlobalHelpers.clone_deep(this.state.style_confirm)
        // style_confirm.visibility = "visible"

        // this.setState({
        //   expanded_insured_info: false,
        //   expanded_confirm: true,
        //   style_confirm
        // }, () => {
        //   window.scrollTo(0, 0);
        // })
    };

    /////////////////////////////////////////////////////////
    /////////////// Confirm Expansion Panel /////////////////
    /////////////////////////////////////////////////////////

    get_all_existing_members_in_group = () => {
        let all_existing_members_in_group = [];
        let group = this.state.representing;
        let policies = this.state.policies;
        let policies_bought = Object.keys(this.state.policies);
        policies_bought.forEach((policy) => {
            if (policies[policy][group] && policies[policy][group].length > 0) {
                policies[policy][group].forEach((policy) => {
                    all_existing_members_in_group.push(policy);
                });
            }
        });

        return all_existing_members_in_group;
    };

    handleConfirm = (stripe_details) => {
        fbq('track', 'AddPaymentInfo');

        if (!do_not_validate) {
            const confirm_terms_and_conditions = this.state.confirm_terms_and_conditions;
            // const confirm_marketing_consent = this.state.confirm_marketing_consent
            if (confirm_terms_and_conditions === false) {
                alert('Please acknowledge terms and conditions to continue');
                return;
            }
            // if (confirm_marketing_consent === false) { alert("Please consent to allow us to communicate with you"); return }
        }

        this.setState(
            {
                // confirm_button_pressed: true,
                loading_modal_open: true
            },
            () => {
                this.screenshotSignupPage().then(() => {
                    const token = localStorage.getItem('ifinsg_token');

                    let purchase = {
                        token,

                        personal_info_already_in_db: this.state.personal_info_already_in_db,

                        products: this.state.products,

                        product_details: Constants.product_details,

                        username: this.state.login_email,

                        representing: this.state.representing,
                        family_claims_pay_to_same: this.state.family_claims_pay_to_same,
                        entity_claims_pay_to_same: this.state.entity_claims_pay_to_same,

                        pmd_brand: this.state.pmd_brand,
                        pmd_model: this.state.pmd_model,
                        pmd_color: this.state.pmd_color,
                        pmd_serial_number: this.state.pmd_serial_number,
                        pmd_lta_number: this.state.pmd_lta_number,
                        pmd_ul_number: this.state.pmd_ul_number,

                        personal_full_name: this.state.personal_full_name,
                        personal_preferred_name: this.state.personal_preferred_name,
                        personal_nric_number: this.state.personal_nric_number,
                        personal_gender: this.state.personal_gender,
                        personal_dob: this.state.personal_dob,
                        personal_place_of_birth: this.state.personal_place_of_birth.value,
                        personal_mobile_number: this.state.personal_mobile_number,
                        personal_email: this.state.personal_email,
                        onwerName: this.state.onwerName,
                        onwerNumber: this.state.onwerNumber,
                        onwerEmail: this.state.onwerEmail,

                        personal_occupation: this.state.personal_occupation.value,
                        personal_self_employed: this.state.personal_self_employed,

                        personal_uploaded_images: this.state.personal_uploaded_images,

                        personal_bank: this.state.personal_bank,
                        personal_bank_acct_number: this.state.personal_bank_acct_number,
                        personal_ack_50_cents_fee: this.state.personal_ack_50_cents_fee,

                        image_upload_signup_screenshot: this.state.image_upload_signup_screenshot,

                        total_grand: this.state.total_grand,
                        total_per_pax: this.state.total_per_pax,
                        entity_name: this.state.entity_name,
                        entity_uen: this.state.entity_uen
                    };

                    // Checking if promotion has expired
                    let products = purchase.products;
                    products.forEach((product) => {
                        try {
                            let promotion = purchase.product_details[product].promotion;
                            if (
                                new Date(promotion.date_start) <= new Date() &&
                                new Date(promotion.date_end) > new Date()
                            ) {
                                // do nothing
                            } else {
                                alert('Promotion has expired. Please refresh and try again :/');
                                return false;
                            }
                        } catch (err) {
                            console.log(err);
                        }
                    });

                    purchase.personal_dob = GlobalHelpers.dateStringFormattedyyyyMMdd(purchase.personal_dob);

                    // Extracting value from selects
                    if (this.state.representing !== 'self') {
                        if (this.state.buy_for_all_members) {
                            let all_existing_members_in_group = this.get_all_existing_members_in_group(); // group = this.state.representing
                            purchase.insured = all_existing_members_in_group;
                        } else {
                            purchase.insured = this.state.insured;
                            // Converting ...occupation: {value:"ABC", label:"ABC"}  ==>  ...occupation: "ABC". Not needed for buy_for_all_members since it was extracted from DB, Already formatted
                            for (let i = 0; i < purchase.insured.length; i++) {
                                purchase.insured[i].occupation = purchase.insured[i].occupation.value;
                                purchase.insured[i].place_of_birth = purchase.insured[i].place_of_birth.value;
                                purchase.insured[i].dob = GlobalHelpers.dateStringFormattedyyyyMMdd(
                                    purchase.insured[i].dob
                                );
                            }
                        }
                    }

                    if (this.state.representing == 'entity') {
                        if (
                            this.state.user_info &&
                            this.state.user_info.company_details &&
                            this.state.user_info.company_details.stripe_details
                        ) {
                        } else {
                            purchase.company_details = this.state.company_details;
                            if (stripe_details) {
                                purchase.company_details.stripe_details = stripe_details;
                            }
                        }
                    } else {
                        if (stripe_details) {
                            purchase.stripe_details = stripe_details;
                        }
                    }

                    if (sessionStorage.getItem('ifinsg_referrer')) {
                        purchase.referrer = sessionStorage.getItem('ifinsg_referrer');
                    }

                    if (this.state.source) {
                        purchase.source = this.state.source;
                    }

                    axios
                        .post('/api/Purchase', {
                            purchase: purchase
                        })
                        .then((res) => {
                            fbq('track', 'Purchase', { value: purchase.total_per_pax || 0.0, currency: 'SGD' });
                            if (res.status === 200) {
                                if (res.data.err) {
                                    alert(res.data.err);
                                    if (res.data.err_code == 'verify_token_expired') {
                                        location.reload();
                                    }
                                    this.setState({
                                        confirm_button_pressed: false,
                                        loading_modal_open: false
                                    });
                                } else {
                                    // SUCCESS!!!
                                    // this.setState(
                                    //     {
                                    //         unload_protect_active: false
                                    //     },
                                    //     () => {
                                            
                                    //     }
                                    // );
                                    if(purchase.referrer === 'lunapark'){
                                        const imgdom = document.createElement('img');
                                        imgdom.src = 'https://x.trc85.com/aff_l?offer_id=3515"'
                                        imgdom.width = 1
                                        imgdom.height = 1
                                        document.body.append(imgdom)
                                    }
                                    Router.push('/profile');
                                }
                            }
                        })
                        .catch((err) => {
                            alert('An error occured - Purchase failed :/');
                            this.setState({
                                confirm_button_pressed: false,
                                loading_modal_open: false
                            });
                        });
                });
            }
        );
    };

    validate_special_small_accident_max_age = (type) => {
        // REQUIRED
        //    type - "personal"  OR  'insured'
        // If insured is 77 y/o and above, reject

        const max_age_for_purchase = 77;
        const products = this.state.products;
        if (products.indexOf('small_accident') != -1) {
            const representing = this.state.representing;
            let insured = '';
            if (this.state.insured) {
                insured = this.state.insured;
            }

            try {
                const year = GlobalHelpers.padNumber(new Date().getFullYear(), 4);
                const month = GlobalHelpers.padNumber(new Date().getMonth() + 1, 2);
                const date = GlobalHelpers.padNumber(new Date().getDate(), 2);
                const years_ago_77_string =
                    (parseInt(year) - max_age_for_purchase).toString() + '-' + month + '-' + date;
                const years_ago_77 = new Date(years_ago_77_string);

                switch (type) {
                    case 'personal':
                        let payer_dob = this.state.personal_dob;
                        if (this.state.personal_info_already_in_db) {
                            payer_dob = this.state.user_info.dob;
                        }
                        if (new Date(payer_dob) > years_ago_77) {
                        } else {
                            alert(`The Small Accident coverage has an age limit of 77 y/o. Payer's age exceeds 77 y/o`);
                            // alert(`
                            // this.state.personal_dob = ` + this.state.personal_dob + `
                            // new Date(this.state.personal_dob) = ` + new Date(this.state.personal_dob) + `
                            // const year = new Date().getFullYear() = `+year +`
                            // const month = new Date().getMonth() = `+ month+`
                            // const date = new Date().getDate() = `+ date +`
                            // const years_ago_77_string =  `+ years_ago_77_string +`
                            // const years_ago_77 =  `+ years_ago_77 +`
                            // `)
                            return false;
                        }
                        break;
                    case 'insured':
                        for (let i = 0; i < insured.length; i++) {
                            if (new Date(insured[i].dob) > years_ago_77) {
                            } else {
                                let insured_name = i;
                                try {
                                    insured_name = insured[i].full_name;
                                } catch (err) {
                                    console.log(err);
                                }
                                alert(
                                    'The Small Accident coverage has an age limit of 77 y/o. Insured ' +
                                        insured_name +
                                        "'s age exceeds 77 y/o"
                                );
                                return false;
                            }
                        }

                        break;
                    default:
                        break;
                }
            } catch (err) {
                console.log(err);
            }
        }

        return true;
    };

    validate_all = () => {
        try {
            if (!this.state.personal_info_already_in_db) {
                if (!this.validate_personal_info()) {
                    return false;
                }
            }

            switch (this.state.representing) {
                case 'self':
                    if (!this.state.login_email) {
                        return false;
                    }
                    if (!this.validate_special_small_accident_max_age('personal')) {
                        return false;
                    }
                    break;

                case 'family':
                    if (this.state.buy_for_all_members) {
                        // No need to validate since there is no insured info
                    } else {
                        if (!this.state.login_email) {
                            return false;
                        } else if (!this.validate_insured_info()) {
                            return false;
                        } else if (!this.validate_special_small_accident_max_age('insured')) {
                            return false;
                        }
                    }

                    break;
                case 'entity':
                    // Do validation later on

                    break;
                case 'group':
                    // Do validation later on
                    break;
                default:
                    // Should not happen
                    break;
            }
        } catch (err) {
            console.log(err);
        }

        return true;
    };

    renderProductPrices = () => {
        let renderable = [];
        let products = this.state.products;
        for (let i = 0; i < products.length; i++) {
            let product_name = Constants.product_details[products[i]].name;
            let product_premium = price_get(products[i], 'premium', 3); // Constants.product_details[].premium
            let cycle_length = Constants.product_details[products[i]].cycle_length;
            let product_admin_fee = price_get(products[i], 'admin_fee', 3); //Constants.product_details[products[i]].admin_fee
            let total = product_premium + product_admin_fee;

            let renderable_product = (
                <Grid container spacing={0} style={{ padding: '5px 10px 5px 10px', lineHeight: 1.2 }}>
                    <Grid item xs={5} style={{ textAlign: 'left' }}>
                        <div> {product_name} </div>
                    </Grid>
                    <Grid item xs={5} style={{ textAlign: 'right' }}>
                        <div>
                            <span style={{ fontSize: '15px', color: 'grey', marginRight: '3px' }}>(Premium)</span>
                            {makeNum2DecimalPlace((product_premium * cycle_length) / 3)}
                        </div>
                        <div>
                            <span style={{ fontSize: '15px', color: 'grey', marginRight: '3px' }}>(Admin Fee)</span>
                            {makeNum2DecimalPlace((product_admin_fee * cycle_length) / 3)}
                        </div>
                    </Grid>
                    <Grid item xs={2} style={{ textAlign: 'right' }}>
                        {makeNum2DecimalPlace((total * cycle_length) / 3)}
                    </Grid>
                </Grid>
            );

            renderable.push(renderable_product);
        }

        return renderable;
    };

    calcBasketPrices = (buy_for_number_of_members) => {
        let products = this.state.products;
        let total_per_pax = 0;

        for (let i = 0; i < products.length; i++) {
            let product_name = Constants.product_details[products[i]].name;
            let cycle_length = Constants.product_details[products[i]].cycle_length;

            let product_premium = price_get(this.state.products[i], 'premium', cycle_length); // this.state.product_details[products[i]].premium
            let product_admin_fee = price_get(this.state.products[i], 'admin_fee', cycle_length); // this.state.product_details[products[i]].admin_fee
            let total = product_premium + product_admin_fee;

            total_per_pax += total;
        }

        const num_of_insured = buy_for_number_of_members > 0 ? buy_for_number_of_members : this.state.num_of_insured;

        let total_grand = total_per_pax * num_of_insured;
        if (this.state.representing === 'self') {
            total_grand = total_per_pax;
        }


        let setStateObject = {
            total_per_pax,
            total_grand
        };

        if (buy_for_number_of_members) {
            setStateObject.num_of_insured_buy_for_all_in_group = buy_for_number_of_members;
        }

        this.setState(setStateObject);
    };

    renderConfirmButton = () => {
        let stripe_button = (
            <StripeCheckout
                style={{ float: 'right' }}
                token={this.handleConfirm}
                stripeKey={GlobalConstants.stripe_key_publishable}
                allowRememberMe={false}
                // opened={this.handleConfirm}
                closed={()=> this.setState({ 
                    confirm_button_pressed: false
                })}
                panelLabel="Get me protected"
                description="Powered by Stripe"
                name="iFinSG Payment"
                image="/ifinsg_logo.png"
                email={GlobalHelpers.normalise_string(this.state.login_email)}
            >
                <div style={{ textAlign: 'center', margin: '30px 0px 30px 0px' }}>
                    <Button
                        disabled={
                            this.state.confirm_terms_and_conditions
                                ? this.state.confirm_button_pressed
                                    ? true
                                    : false
                                : true
                        }
                        style={{
                            marginTop: '15px',
                            padding: '10px 20px 10px 20px',
                            fontSize: '15px',
                            backgroundColor: Constants.color.darkgreen,
                            opacity: 0.8,
                            color: 'white'
                        }}
                        onClick={() => {
                            this.setState({ confirm_button_pressed: true });
                            fbq('track', 'InitiateCheckout');
                        }}
                    >
                        Stripe Payment
                    </Button>
                </div>
            </StripeCheckout>
        );
        let purchase_button_without_stripe = (
            <div style={{ textAlign: 'center', margin: '30px 0px 30px 0px' }}>
                <Button
                    style={{
                        marginTop: '15px',
                        padding: '10px 20px 10px 20px',
                        fontSize: '15px',
                        backgroundColor: Constants.color.darkgreen,
                        opacity: 0.8,
                        color: 'white'
                    }}
                    onClick={this.handleConfirm}
                >
                    Payment
                </Button>
            </div>
        );

        if (this.state.representing == 'entity') {
            if (
                this.state.user_info &&
                this.state.user_info.company_details &&
                this.state.user_info.company_details.stripe_details
            ) {
                return purchase_button_without_stripe;
            }
        } else if (this.state.personal_info_already_in_db) {
            return purchase_button_without_stripe;
        }

        let disabled = true;
        if (this.state.confirm_terms_and_conditions) {
            if (this.state.confirm_button_pressed) {
                disabled = true;
            } else {
                disabled = false;
            }
        } else {
            disabled = true;
        }

        if (disabled) {
            return (
                <div style={{ textAlign: 'center', margin: '30px 0px 30px 0px' }}>
                    <Button
                        disabled={true}
                        style={{
                            marginTop: '15px',
                            padding: '10px 20px 10px 20px',
                            fontSize: '15px',
                            backgroundColor: Constants.color.darkgreen,
                            opacity: 0.8,
                            color: 'white'
                        }}
                    >
                        Stripe Payment
                    </Button>
                </div>
            );
        } else {
            return stripe_button;
        }
    };

    renderItemsPurchasing = () => {
        let renderable = [];

        for (let i = 0; i < this.state.products.length; i++) {
            const tick = (
                <div style={{ float: 'left', height: '20px', width: '20px' }}>
                    <Tick />
                </div>
            );
            const item = (
                <Typography
                    style={{
                        marginLeft: '30px',
                        fontWeight: 'bold',
                        color: Constants.color.grey
                    }}
                >
                    {Constants.product_details[this.state.products[i]].name}
                </Typography>
            );
            renderable.push(
                <div style={{ minWidth: '100px' }}>
                    {tick}
                    {item}
                </div>
            );
        }

        return renderable;
    };

    getMuiTheme = () => {
        return createMuiTheme({
            overrides: {
                MuiInputBase: {
                    input: {
                        color: 'green'
                    }
                }
            }
        });
    };

    renderLifeInsuredDetailsTitle = (representing) => {
        const { isAddNewMember } = this.state;
        let title = 'Life Insured Details';
        if (representing === 'self') return title;
        if (isAddNewMember) {
            if (representing === 'family') {
                title += ' (addition of Family member)';
            } else {
                title += ' (addition of Company staff)';
            }
        }
        return title;
    };

    renderContent = () => {
        const { policies, products, representingTab, representing, isAddNewMember, boughtPolicies } = this.state;

        return (
            <div id="signup_page" style={{ maxWidth: '480px', margin: 'auto' }}>
                <div
                    style={{
                        margin: '0px 0px 5px 10px',
                        display: 'flex',
                        alignItems: 'center',
                        minHeight: '50px',
                        justifyContent: 'space-between'
                    }}
                >
                    <div style={{ float: 'left', fontSize: '12px', fontWeight: 'bold' }}>
                        {this.renderItemsPurchasing()}
                    </div>

                    <div style={{ margin: '0px 10px 0px 54px', display: 'block' }}>
                        <a
                            href="https://api.whatsapp.com/send?phone=6596898676"
                            target="blank"
                            style={{ color: 'white', float: 'right' }}
                        >
                            <img
                                style={{
                                    float: 'right',
                                    maxWidth: '35px',
                                    width: '100%',
                                    marginLeft: '10px'
                                }}
                                src={'/img/icon-whatsapp.png'}
                            />
                        </a>

                        <a href="tel:+6596898676" target="blank" style={{ color: 'white' }}>
                            <img
                                style={{
                                    float: 'right',
                                    maxWidth: '35px',
                                    width: '100%',
                                    marginLeft: '10px'
                                }}
                                src={'/img/icon-phone.png'}
                            />
                        </a>
                        <div style={{ padding: '8px 0px 0px 20px', fontSize: '15px' }}>Support</div>
                    </div>
                </div>

                {/* ///////////////////////////////////////////////////////////////////////////////////////////////// */}
                {/* /////////////////////////////////////////////// Login ////////////////////////////////////////////*/}
                {/* ///////////////////////////////////////////////////////////////////////////////////////////////// */}

                <ExpansionPanel
                    disabled={this.state.expansion_panel_login_disabled}
                    style={this.state.style_login}
                    expanded={this.state.expanded_login}
                    onChange={(event, expanded) => {
                        if (expanded) {
                            this.configureExpansionPanels(['login_expand']);
                        } else {
                            this.configureExpansionPanels(['login_collapse']);
                        }
                    }}
                >
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <div style={{ width: '100%' }}>
                            <Typography style={{ fontSize: '15px' }}>Signup | Login</Typography>
                            {/* {this.renderLoginFilledDetails()} */}
                        </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <div style={{ width: '100%' }}>
                            <div>
                                <AppBar position="static" color="default" style={{ backgroundColor: 'white' }}>
                                    <Tabs
                                        value={this.state.loginTab}
                                        onChange={this.handleLoginTabChange}
                                        indicatorColor="primary"
                                        textColor="primary"
                                        variant="fullWidth"
                                        style={{ backgroundColor: 'white' }}
                                    >
                                        <Tab label="New User" />
                                        <Tab label="Existing User" />
                                    </Tabs>
                                </AppBar>
                                <SwipeableViews
                                    axis={'x'} // 'x-reverse'
                                    index={this.state.loginTab}
                                    onChangeIndex={this.handleLoginTabChangeIndex}
                                >
                                    <TabContainer>
                                        {this.renderSignupFields()}

                                        {this.renderEmailVerificationFields()}
                                    </TabContainer>

                                    <TabContainer>
                                        <form>
                                            <TextField
                                                style={{ padding: '0px 0px 5px 0px' }}
                                                label="Email"
                                                autoComplete="email"
                                                value={this.state.login_email}
                                                onChange={this.handleFieldChange('login_email')}
                                                fullWidth
                                            />

                                            <TextField
                                                style={{ padding: '0px 0px 2px 0px' }}
                                                inputRef={(node) => (input_refs.login_password_field = node)}
                                                label="Password"
                                                autoComplete="current-password"
                                                value={this.state.login_password}
                                                onChange={this.handleFieldChange('login_password')}
                                                type="password"
                                                onKeyPress={(ev) => {
                                                    if (ev.key === 'Enter') {
                                                        this.handleLogin();
                                                    }
                                                }}
                                                fullWidth
                                            />
                                        </form>

                                        <Button
                                            style={{
                                                fontSize: '9px',
                                                padding: 0,
                                                marginTop: '5px'
                                            }}
                                            onClick={() => {
                                                Router.push(
                                                    Constants.get_new_page('forgot-password', this.state.referrer)
                                                );
                                            }}
                                        >
                                            Forgot Password
                                        </Button>

                                        <Button
                                            style={{ float: 'right', margin: '10px 0px 0px 0px' }}
                                            disabled={this.state.login_button_disabled}
                                            onClick={() => {
                                                this.handleLogin();
                                            }}
                                        >
                                            Login
                                        </Button>
                                    </TabContainer>
                                </SwipeableViews>
                            </div>
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>

                {/* ///////////////////////////////////////////////////////////////////////////////////////////////// */}
                {/* //////////////////////////////////////// Representing /////////////////////////////////////////*/}
                {/* ///////////////////////////////////////////////////////////////////////////////////////////////// */}

                <ExpansionPanel
                    style={this.state.style_representing}
                    expanded={this.state.expanded_representing}
                    onChange={(event, expanded) => {
                        if (expanded) {
                            this.configureExpansionPanels(['representing_expand']);
                        } else {
                            this.configureExpansionPanels(['representing_collapse']);
                        }
                    }}
                >
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <div style={{ width: '100%' }}>
                            <Typography style={{ fontSize: '15px' }}>I am buying for</Typography>
                            {this.renderRepresentingFilledDetails()}
                        </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails style={{ padding: '24px 0px 24px 0px' }}>
                        <div style={{ width: '100%', padding: 0 }}>
                            <div>
                                <AppBar position="static" color="default" style={{ backgroundColor: 'white' }}>
                                    <Tabs
                                        value={representingTab}
                                        onChange={this.handleRepresentingTabChange}
                                        indicatorColor="primary"
                                        textColor="primary"
                                        variant="fullWidth"
                                        style={{ backgroundColor: 'white' }}
                                        centered
                                        scrollButtons="off"
                                    >
                                        <Tab label="Self" />
                                        {this.state.products.indexOf('small_pmd_protect') !== -1 ? (
                                            ''
                                        ) : (
                                            <Tab label="Family" />
                                        )}
                                        {this.state.products.indexOf('small_pmd_protect') !== -1 ? (
                                            ''
                                        ) : (
                                            <Tab label="Company" />
                                        )}
                                    </Tabs>
                                </AppBar>
                                <SwipeableViews
                                    axis={'x'}
                                    index={representingTab}
                                    onChangeIndex={this.handleRepresentingTabChangeIndex}
                                >
                                    {this.renderTabContainers()}
                                </SwipeableViews>
                            </div>
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>

                {/* ///////////////////////////////////////////////////////////////////////////////////////////////// */}
                {/* //////////////////////////////////////// PMD Details /////////////////////////////////////////*/}
                {/* ///////////////////////////////////////////////////////////////////////////////////////////////// */}

                <ExpansionPanel
                    style={this.state.style_pmd_details}
                    expanded={this.state.expanded_pmd_details}
                    onChange={(event, expanded) => {
                        if (expanded) {
                            this.configureExpansionPanels(['pmd_details_expand']);
                        } else {
                            this.configureExpansionPanels(['pmd_details_collapse']);
                        }
                    }}
                >
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <div style={{ width: '100%' }}>
                            <Typography style={{ fontSize: '15px' }}>PMD Details</Typography>
                        </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails style={{ padding: '24px 0px 24px 0px' }}>
                        <div style={{ width: '100%', padding: 0 }}>{this.renderPMDFields()}</div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>

                {/* ///////////////////////////////////////////////////////////////////////////////////////////////// */}
                {/* ////////////////////////////////////////// Personal Info /////////////////////////////////////////*/}
                {/* ///////////////////////////////////////////////////////////////////////////////////////////////// */}

                <table style={this.state.style_personal_info}>
                    <tbody style={{ display: 'contents' }}>
                        <tr style={{ display: 'contents' }}>
                            <ExpansionPanel
                                disabled={this.state.expansion_panel_personal_info_disabled}
                                expanded={this.state.expanded_personal_info}
                                onChange={(event, expanded) => {
                                    if (expanded) {
                                        this.configureExpansionPanels(['personal_info_expand']);
                                    } else {
                                        this.configureExpansionPanels(['personal_info_collapse']);
                                    }
                                }}
                            >
                                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                    <div style={{ width: '100%' }}>
                                        <div style={{}}>
                                            <Typography style={{ fontSize: '15px', whiteSpace: 'pre-wrap' }}>
                                                <b>Payer Details </b>
                                                {this.state.representing === 'entity' && '(for Company purchase)'}
                                            </Typography>
                                        </div>
                                        {/* {this.state.representing == "entity" ? (this.renderCompanyPayerDetails()) : (this.renderPersonalFilledDetails())} */}
                                    </div>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    <div style={{ width: '100%' }}>
                                        <div style={{ padding: '0px 20px 5px 20px' }}>
                                            {this.renderPersonalInfoFields()}
                                        </div>
                                    </div>
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                        </tr>
                    </tbody>
                </table>

                {/* ///////////////////////////////////////////////////////////////////////////////////////////////// */}
                {/* /////////////////////////////////////////// Insured Info /////////////////////////////////////////*/}
                {/* ///////////////////////////////////////////////////////////////////////////////////////////////// */}

                <table style={this.state.style_insured_info}>
                    <tbody style={{ display: 'contents' }}>
                        <tr style={{ display: 'contents' }}>
                            <ExpansionPanel
                                expanded={this.state.expanded_insured_info}
                                onChange={(event, expanded) => {
                                    if (expanded) {
                                        this.configureExpansionPanels(['insured_info_expand']);
                                    } else {
                                        this.configureExpansionPanels(['insured_info_collapse']);
                                    }
                                }}
                            >
                                {/* TODO: Life Insured Table that requires fixing */}
                                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                    <div style={{ width: '100%' }}>
                                        <div style={{ display: 'inline-flex', whiteSpace: 'pre-wrap' }}>
                                            <Typography style={{ fontSize: '15px' }}>
                                                {this.renderLifeInsuredDetailsTitle(representing)}
                                            </Typography>
                                        </div>
                                        {this.renderInsuredFilledDetails()}
                                    </div>
                                </ExpansionPanelSummary>

                                {this.renderInsuredInfoFieldsCards()}

                                <div style={{ textAlign: 'center' }}>
                                    <Button
                                        style={{
                                            margin: '0px 0px 0px 0px',
                                            fontSize: '28px',
                                            color: 'rgba(0, 0, 0, 0.54)'
                                        }}
                                        onClick={() => {
                                            this.handleAddInsured();
                                        }}
                                    >
                                        +
                                    </Button>
                                </div>
                                <Button
                                    style={{ float: 'right', margin: '10px 30px 20px 0px' }}
                                    onClick={() => {
                                        this.handleNextAfterInsuredInfo();
                                    }}
                                >
                                    OK
                                </Button>
                            </ExpansionPanel>
                        </tr>
                    </tbody>
                </table>

                {/* ///////////////////////////////////////////////////////////////////////////////////////////////// */}
                {/* ///////////////////////////////////////////// Confirm ////////////////////////////////////////////*/}
                {/* ///////////////////////////////////////////////////////////////////////////////////////////////// */}

                <ExpansionPanel
                    style={this.state.style_confirm}
                    expanded={this.state.expanded_confirm}
                    onChange={(event, expanded) => {
                        if (expanded) {
                            this.configureExpansionPanels(['confirm_expand']);
                        } else {
                            this.configureExpansionPanels(['confirm_collapse']);
                        }
                    }}
                >
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <div style={{ width: '100%' }}>
                            <Typography style={{ fontSize: '15px' }}>Confirmation</Typography>
                        </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <div style={{ width: '100%' }}>
                            <div>
                                <Grid
                                    container
                                    spacing={0}
                                    style={{
                                        padding: '10px 10px 10px 10px',
                                        margin: '20px 0px 0px 0px',
                                        lineHeight: 1.2
                                    }}
                                >
                                    <Grid item xs={6} style={{ textAlign: 'left' }}>
                                        <div style={{}}> Product </div>
                                    </Grid>
                                    <Grid item xs={4} style={{ textAlign: 'center' }}>
                                        Price
                                    </Grid>
                                    <Grid item xs={2} style={{ textAlign: 'right' }}>
                                        Total
                                    </Grid>
                                </Grid>

                                <Divider />

                                {this.renderProductPrices()}

                                <Divider />

                                <Grid container spacing={0} style={{ padding: '5px 10px 10px 10px', lineHeight: 1.2 }}>
                                    <Grid item xs={3} style={{ textAlign: 'left' }} />
                                    <Grid item xs={7} style={{ textAlign: 'right' }}>
                                        Total per pax:
                                    </Grid>
                                    <Grid item xs={2} style={{ textAlign: 'right' }}>
                                        {makeNum2DecimalPlace(this.state.total_per_pax)}
                                    </Grid>
                                </Grid>

                                <Divider />

                                <Grid container spacing={0} style={{ padding: '5px 10px 10px 10px', lineHeight: 1.2 }}>
                                    <Grid item xs={3} style={{ textAlign: 'left' }} />
                                    <Grid item xs={7} style={{ textAlign: 'right' }}>
                                        Total for{' '}
                                        {this.state.representing === 'self'
                                            ? 1
                                            : this.state.num_of_insured_buy_for_all_in_group
                                            ? this.state.num_of_insured_buy_for_all_in_group
                                            : this.state.num_of_insured}{' '}
                                        pax:
                                    </Grid>
                                    <Grid item xs={2} style={{ textAlign: 'right' }}>
                                        {makeNum2DecimalPlace(
                                            this.state.representing === 'self'
                                                ? this.state.total_per_pax
                                                : // : this.state.total_grand
                                                this.state.num_of_insured_buy_for_all_in_group
                                                ? this.state.num_of_insured_buy_for_all_in_group *
                                                  this.state.total_per_pax
                                                : this.state.total_per_pax * this.state.num_of_insured
                                        )}
                                    </Grid>
                                </Grid>

                                <Grid container spacing={0} style={{ padding: '5px 10px 10px 10px', lineHeight: 1.2 }}>
                                    <Grid item xs={2} sm={6} style={{ textAlign: 'left' }} />

                                    <Grid item xs={10} sm={6} style={{ textAlign: 'right' }}>
                                        <Typography style={{ fontSize: '12px' }}>
                                            Payment amount will be pro-rated, if your cover starts in the midst of each
                                            cycle.
                                        </Typography>
                                    </Grid>
                                </Grid>

                                <div style={{}}>
                                    <Typography style={{ fontSize: '15px', marginTop: '20px' }}>
                                        <u>Terms & Conditions</u>
                                    </Typography>

                                    <Typography style={{ fontSize: '12px', marginTop: '10px' }}>
                                        <span style={{ fontWeight: 'bold' }}>
                                            <span style={{ fontSize: '15px', fontWeight: 'normal', marginRight: 5 }}>
                                                (1)
                                            </span>
                                            We do not guarantee that our premiums will always stay the same.
                                        </span>
                                        <div style={{ marginTop: '10px' }} />
                                        Although we strongly prefer to keep our premiums the same, premiums for your
                                        policy are determined based on true claims data from the previous cycle of
                                        cover.
                                        <div style={{ marginTop: '10px' }} />
                                        <div style={{ marginTop: '10px' }} />
                                        Premiums may be increased in the exceptional occasions of Situation [B] or
                                        Situation [C] :
                                        <div style={{ marginTop: '10px' }} />
                                        Situation [A] - Premiums are sufficient
                                        <div style={{ marginTop: '10px' }} />
                                        As all users are allowed make Claims for up to 3 months after the Date of an
                                        Accident / Incident, some Claims ( whether made by you or other users ) which
                                        take place in your current cycle could be due to an Accident / Incident which
                                        happened in an earlier cycle, so you must be aware that part of your Premiums
                                        for your current cycle may be shared for that purpose.
                                        <div style={{ marginTop: '10px' }} />
                                        Situation [B] - Premiums are insufficient due to a Brief increase of claims
                                        <div style={{ marginTop: '10px' }} />
                                        If there are exceptional situations where there is an insufficiency of premiums
                                        which lead to Pending Claims ( whether made by you or other users ) from an
                                        earlier cycle, the premiums pool will need to be temporarily increased in your
                                        current cycle to pay out the Pending Claims from the earlier cycle, so you must
                                        be aware that you may pay a temporary Higher Premium ( eg. if the Small Accident
                                        cover’s premium is temporarily more than $6.80 a month ) for your current cycle
                                        for that purpose.
                                        <div style={{ marginTop: '10px' }} />
                                        We will temporarily increase premiums just for the immediate next cycle, and the
                                        following cycle’s premium will revert back to the original.
                                        <div style={{ marginTop: '10px' }} />
                                        Situation [C] - Premiums are insufficient due to a Trend of a general increase
                                        in claims
                                        <div style={{ marginTop: '10px' }} />
                                        If there is an exceptional situation where there is a trend of an insufficiency
                                        of premiums / an insufficiency of premiums which lead to Pending Claims, the
                                        premiums pool will need to be permanently increased for future cycles to ensure
                                        premium sufficiency, so you must be aware that you may need to pay a permanently
                                        Higher Premium ( eg. the Small Accident cover’s premium becomes permanently more
                                        than $6.80 a month ) for future cycles for that purpose.
                                        <div style={{ marginTop: '10px' }} />
                                        We will permanently increase premiums for future cycles to ensure that the
                                        premiums accurately reflect the trend of claim increases.
                                        <div style={{ marginTop: '10px' }} />
                                        For new users, before any increase of premiums takes place in Situation [B] or
                                        Situation [C], rest assured that we will definitely update the info on our
                                        website, and any new premium amount will only start on the 1st Day of each new
                                        cycle.
                                        <div style={{ marginTop: '10px' }} />
                                        For existing users, before any increase of premiums takes place in Situation [B]
                                        or Situation [C], rest assured that we will definitely inform you via email at
                                        least 3 days in advance ( in case you do not wish to continue with your cover,
                                        just reply us to stop the Credit or Debit card billing ), and any new premium
                                        amount will only start on the 1st Day of each new cycle.
                                        <div style={{ marginTop: '10px' }} />
                                        In the unforeseen event that you did not manage to read your email within the 3
                                        days in advance, should you decide to discontinue your cover, just reply us that
                                        you would like to stop, and we will give you a Cancellation Refund based on the
                                        remaining unused days of the cycle = pro-rated premiums ( from the premiums pool
                                        ) + pro-rated Admin Fees – full Stripe's charges incurred.
                                        <div style={{ marginTop: '10px' }} />
                                        <div style={{ marginTop: '10px' }} />
                                        We don’t earn a single dollar from premiums, thus we will never increase
                                        premiums unnecessarily. Every dollar of premiums is only for 2 purposes:
                                        PremiumBack and Claims.
                                        <div style={{ marginTop: '10px' }} />
                                        The only income we earn as a business is a flat and transparent monthly Admin
                                        Fee per policy per user.
                                        <div style={{ marginTop: '10px' }} />
                                        <span style={{ fontWeight: 'bold' }}>
                                            <span style={{ fontSize: '15px', fontWeight: 'normal', marginRight: 5 }}>
                                                (2)
                                            </span>
                                            By applying to be a user of iFinSG's service, I hereby give explicit PDPA
                                            consent to ICH iFinancial Singapore Private Limited ( UEN: 201631146G ) to
                                            contact me via all forms of communication, including but not limited to
                                            phone call, messaging (such as WhatsApp and SMS), and email.
                                        </span>
                                    </Typography>
                                    <Typography style={{ fontSize: '12px', marginTop: '10px' }}>
                                        <span style={{ fontWeight: 'bold' }}>
                                            <span style={{ fontSize: '15px', fontWeight: 'normal', marginRight: 5 }}>
                                                (3)
                                            </span>
                                            I am aware that my Cyclic Insurance will be automatically renewed for each
                                            cycle of 3 months, until I inform iFinSG to cancel / pause my cover at any
                                            time.
                                        </span>
                                    </Typography>

                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                checked={this.state.confirm_terms_and_conditions}
                                                onChange={this.handleCheckboxToggle('confirm_terms_and_conditions')}
                                                value="confirm_terms_and_conditions"
                                                color="primary"
                                            />
                                        }
                                        label={
                                            <>
                                                <Typography>
                                                    I have read and agree with these terms and conditions.
                                                </Typography>
                                            </>
                                        }
                                        style={{ margin: '10px 0px 0px 0px' }}
                                    />
                                    <div style={{ marginTop: '20px', textAlign: 'left' }}>
                                        <div style={{ textAlign: 'left' }}>
                                            <Typography style={{ margin: '0px 0px 0px 50px' }}>
                                                Where did you find out about Cyclic insurance?
                                            </Typography>
                                        </div>
                                        <FormControlLabel
                                            style={{ margin: '0px 10px 0px 10px', width: '100%' }}
                                            control={
                                                <Radio
                                                    style={{ padding: '6px' }}
                                                    checked={this.state.source === sources[0]}
                                                    onChange={() => {
                                                        this.setState({ source: sources[0] });
                                                    }}
                                                />
                                            }
                                            label={sources[0]}
                                        />
                                        <FormControlLabel
                                            style={{ margin: '0px 10px 0px 10px', width: '100%' }}
                                            control={
                                                <Radio
                                                    style={{ padding: '6px' }}
                                                    checked={this.state.source === sources[1]}
                                                    onChange={() => {
                                                        this.setState({ source: sources[1] });
                                                    }}
                                                />
                                            }
                                            label={sources[1]}
                                        />
                                        <FormControlLabel
                                            style={{ margin: '0px 10px 0px 10px', width: '100%' }}
                                            control={
                                                <Radio
                                                    style={{ padding: '6px' }}
                                                    checked={this.state.source === sources[2]}
                                                    onChange={() => {
                                                        this.setState({ source: sources[2] });
                                                    }}
                                                />
                                            }
                                            label={sources[2]}
                                        />
                                        <FormControlLabel
                                            style={{ margin: '0px 10px 0px 10px', width: '100%' }}
                                            control={
                                                <Radio
                                                    style={{ padding: '6px' }}
                                                    checked={this.state.source === sources[3]}
                                                    onChange={() => {
                                                        this.setState({ source: sources[3] });
                                                    }}
                                                />
                                            }
                                            label={sources[3]}
                                        />
                                        <FormControlLabel
                                            style={{ margin: '0px 10px 0px 10px', width: '100%' }}
                                            control={
                                                <Radio
                                                    style={{ padding: '6px' }}
                                                    checked={this.state.source === sources[4]}
                                                    onChange={() => {
                                                        this.setState({ source: sources[4] });
                                                    }}
                                                />
                                            }
                                            label={sources[4]}
                                        />
                                        <FormControlLabel
                                            style={{ margin: '0px 10px 0px 10px', width: '100%' }}
                                            control={
                                                <Radio
                                                    style={{ padding: '6px' }}
                                                    checked={this.state.source.indexOf(sources[5]) !== -1}
                                                    onChange={() => {
                                                        this.setState({ source: sources[5] });
                                                    }}
                                                />
                                            }
                                            label={
                                                <div>
                                                    <span
                                                        style={{
                                                            paddingTop: '4px',
                                                            display: 'inline-block'
                                                        }}
                                                    >
                                                        {sources[5]}:
                                                    </span>
                                                    <TextField
                                                        value={this.state.source_others}
                                                        onChange={(e) => {
                                                            this.setState({
                                                                source_others: e.target.value,
                                                                source: sources[5] + ': ' + e.target.value
                                                            });
                                                        }}
                                                    />
                                                </div>
                                            }
                                        />
                                    </div>

                                    {this.state.products.indexOf('small_pmd_protect') != -1 ? (
                                        <div style={{ margin: '30px 0px 30px 0px' }}>
                                            Upload your Receipt photos if you purchased a new PMD within the past 48
                                            hours ( and you will be allowed to make claims within the 1st 30 days of
                                            Approved cover ):
                                            <ImageUpload
                                                image_type="SIGNUP-IMAGE"
                                                username={this.state.login_email}
                                                maxFiles={this.state.login_email.length}
                                                type={'personal'}
                                                fileUploadedPersonal={this.fileUploadedPersonal}
                                                fileRemovedPersonal={this.fileRemovedPersonal}
                                            />
                                        </div>
                                    ) : (
                                        ''
                                    )}

                                    <Typography
                                        style={{
                                            margin: '20px 0px 0px 0px',
                                            textAlign: 'center'
                                        }}
                                    >
                                        Your credit card / debit card will only be billed{' '}
                                        <b>after your application is Approved</b>.
                                    </Typography>
                                </div>
                            </div>

                            {this.renderConfirmButton()}
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        );
    };

    render() {
        return (
            <div style={{ wordBreak: 'break-word' }}>
                <Head>
                    <link href="/filepicker.css" rel="stylesheet" />
                    <link href="/dropzone.min.css" rel="stylesheet" />

                    <script src="/canvas-to-blob.min.js" />
                </Head>

                <TopNavBar
                    user_info={this.state.user_info}
                    username={this.state.expansion_panel_login_disabled ? this.state.login_email : ''}
                    referrer={this.state.referrer}
                />

                <NextSeo config={Constants.next_seo_config('signup')} />

                <Head>
                    <link href="/filepicker.css" rel="stylesheet" />
                    <link href="/dropzone.min.css" rel="stylesheet" />

                    <script src="/canvas-to-blob.min.js" />
                    {/* https://github.com/blueimp/JavaScript-Canvas-to-Blob */}

                    <script src="/google-analytics-tracking.js" />

                    {/*  GlobalConstants.staging ?  
            <script src="/prevent-web-view.js"> </script>  
            : "" */}
                </Head>

                {/* <ModalMessage open={this.state.modalMessageOpen} message="pdpa" handleClose={()=>{this.setState({modalMessageOpen: false})}} /> */}

                <LoadingModal open={this.state.loading_modal_open} handleClose={this.handleCloseLoadingModal} />
                <BackProtect unload_protect_active={this.state.unload_protect_active} back_protect_active={true} />

                <NoSsr>
                    <BrowserView>
                        <div style={{ paddingTop: '85px', textAlign: 'center', width: '100%' }} />
                        {this.renderContent()}
                    </BrowserView>

                    <MobileView>
                        <div style={{ paddingTop: '100px' }} />
                        {this.renderContent()}
                    </MobileView>
                </NoSsr>

                <BotNavBar referrer={this.state.referrer} />
            </div>
        );
    }
}

export default withRouter(Mobile);
