import React, { useEffect } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import '../public/tailwind.css';
import CheckIcon from '@material-ui/icons/Check';
import DoneOutlinedIcon from '@material-ui/icons/DoneOutlined';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

export default function SmallAccidentP1() {
    return (
        <>
            <Head>
                <link href="https://fonts.googleapis.com/css2?family=Fjalla+One&display=swap" rel="stylesheet" />
            </Head>
            <div className="tw-container tw-mx-auto tw-px-3 tw-antialiased">
                <h2 className="tw-font-alt tw-text-center tw-mt-10 tw-mb-12 tw-text-2xl md:tw-text-3.8xl tw-font-medium tw-leading-tight tw-text-highlights1">
                    "Because you can't control accidents"
                </h2>

                <h1
                    className="tw-font-alt tw-text-center tw-mx-auto tw-text-3xl md:tw-text-5.4xl tw-text-highlights0 tw-leading-tight"
                    style={{ maxWidth: '1100px' }}
                >
                    Especially for{' '}
                    <b className=" tw-text-highlights2">
                        Food Delivery Professionals, PHV Drivers, Self Employed Individuals
                    </b>{' '}
                    <span className="tw-underline">and anyone who spends a lot of time on-the-go...</span>
                </h1>

                <h2
                    className="tw-font-alt tw-text-2xl md:tw-text-4.5xl tw-mt-12 tw-text-highlights3 tw-font-bold tw-mx-auto tw-text-center tw-leading-tight"
                    style={{ maxWidth: '1100px' }}
                >
                    The quick-and-easy way to <span className="tw-underline">Stop paying your medical bills</span> for
                    practical daily dangers.
                </h2>

                <div className="tw-flex tw-items-center tw-justify-center tw-mt-8">
                    <img alt="p1" src="/advert/1.png" style={{ maxWidth: '941px', width: '100%' }} />
                </div>

                <div className="tw-mx-auto tw-mt-10 tw-space-y-8" style={{ maxWidth: '1100px' }}>
                    <p className="tw-text-1xl tw-text-left tw-leading-snug">
                        iFinSG is an Insurance Technology company based in Singapore, and we believe that insurance
                        should be <b>Simple, Fast and Cheap.</b>
                    </p>

                    <p className="tw-text-1xl tw-text-left tw-leading-snug">
                        After many years of experience in the insurance industry, we discovered that the problem was not
                        about who was selling it, but <b>the Product itself.</b>
                    </p>

                    <p className="tw-text-1xl tw-text-left tw-leading-snug">
                        It’s the Pricing system, Product features, and Usage system which caused all the pain.
                    </p>

                    <p className="tw-text-1xl tw-text-left tw-leading-snug">
                        <b>
                            Commissions for agents, Compulsory bundled features (which you pay for), Complicated
                            Exclusions Lists,
                        </b>{' '}
                        are all pains which you could not avoid in the past.
                    </p>

                    <div className="tw-text-1xl tw-text-left tw-leading-snug">
                        <p className="tw-italic">
                            - Why do I always have to Meet you for a sales <b>"coffee session" or "zoom session"</b>?
                        </p>
                        <p className="tw-italic">
                            - Why can't I pick only the feature I find the <b>most Practical</b>, instead of paying for
                            the whole bundle?
                        </p>
                        <p className="tw-italic">
                            - Why does your <b>Exclusions list seem as complicated</b> as nuclear physics?
                        </p>
                    </div>

                    <p className="tw-text-1xl tw-text-left tw-leading-snug">Sounds familiar?</p>

                    <div className="tw-flex tw-items-center tw-justify-center">
                        <img alt="insurance-form" src="/advert/2.png" style={{ maxWidth: '699px', width: '100%' }} />
                    </div>

                    <p className="tw-text-1xl tw-text-left tw-leading-snug">
                        Which is why we've created a revolutionary system which uses the original roots of insurance, to
                        make <b>Cyclic Insurance</b> painless, fair, and affordable for everyone!
                    </p>
                    <p className="tw-text-1xl tw-text-left tw-leading-snug">
                        Traditional insurance collects premiums into a pool for users to make claims, and{' '}
                        <i>whatever remains in the pool is the company's Underwriting Profit.</i>
                    </p>
                    <p className="tw-text-1xl tw-text-left tw-leading-snug">
                        Likewise, <b>Cyclic Insurance</b> does that as well, except... every dollar which remains in the
                        pool is not our Underwriting Profit, but is{' '}
                        <b>given back to users after the end of every 3 months cycle.</b>
                    </p>
                    <p className="tw-text-1xl tw-text-left tw-leading-snug">
                        Unlike Traditional insurance, Cyclic insurance:
                    </p>
                </div>

                <div className="tw-mt-20 tw-flex tw-items-center tw-justify-center">
                    <img alt="choose" src="/advert/3.png" style={{ width: '100%', maxWidth: '1100px' }} />
                </div>

                <div className="tw-mx-auto tw-mt-3 tw-space-y-1" style={{ maxWidth: '1100px' }}>
                    <div>
                        <span
                            className="tw-inline-block tw-bg-highlights tw-text-center tw-bg-highlights6 tw-text-white tw-text-2xl tw-border-b-2 tw-border-solid tw-border-highlights7 tw-border-t-0 tw-border-l-0 tw-border-r-0"
                            style={{
                                minHeight: '44px',
                                minWidth: '44px',
                                lineHeight: '44px',
                                marginRight: '10px'
                            }}
                        >
                            1
                        </span>
                        <span className="tw-text-1xl tw-leading-snug">
                            We{' '}
                            <b>
                                only earn a flat and transparent monthly Admin Fee per policy per user, with no hidden
                                charges
                            </b>
                            . And .... it's just $3 a month now!
                        </span>
                        <hr />
                    </div>
                    <div>
                        <span
                            className="tw-inline-block tw-bg-highlights tw-text-center tw-bg-highlights6 tw-text-white tw-text-2xl tw-border-b-2 tw-border-solid tw-border-highlights7 tw-border-t-0 tw-border-l-0 tw-border-r-0"
                            style={{
                                minHeight: '44px',
                                minWidth: '44px',
                                lineHeight: '44px',
                                marginRight: '10px'
                            }}
                        >
                            2
                        </span>
                        <span className="tw-text-1xl tw-leading-snug">
                            <b>Unclaimed premiums in the pool, will be distributed back to users</b> who made 0 claims &
                            users who made low claims, at the end of each 3 months cycle.
                        </span>
                        <hr />
                    </div>
                    <div>
                        <span
                            className="tw-inline-block tw-bg-highlights tw-text-center tw-bg-highlights6 tw-text-white tw-text-2xl tw-border-b-2 tw-border-solid tw-border-highlights7 tw-border-t-0 tw-border-l-0 tw-border-r-0"
                            style={{
                                minHeight: '44px',
                                minWidth: '44px',
                                lineHeight: '44px',
                                marginRight: '10px'
                            }}
                        >
                            3
                        </span>
                        <span className="tw-text-1xl tw-leading-snug">
                            We will <b>always allow you to make a 2nd claim from us for the same accident / incident</b>
                            , even after you make a 1st claim from a traditional insurer.
                        </span>
                    </div>
                </div>

                <h2
                    className="tw-font-alt tw-text-4.75xl tw-my-16 tw-text-highlights3 tw-mx-auto tw-text-center tw-leading-snug"
                    style={{ maxWidth: '1100px' }}
                >
                    <b>Here's a shocking fact...</b>
                    <span
                        style={{ fontWeight: 'normal' }}
                    >{` Did you know, for most Traditional Personal Accident plans:`}</span>
                </h2>

                <div className="tw-mx-auto tw-space-y-1" style={{ maxWidth: '1100px' }}>
                    <div>
                        <span
                            className="tw-bg-highlights3 tw-text-white tw-p-2 tw-mr-1 tw-border-b-2 tw-border-solid tw-border-red-900 tw-rounded tw-border-t-0 tw-border-l-0 tw-border-r-0"
                            style={{
                                fontSize: 'x-large'
                            }}
                        >
                            <DoneOutlinedIcon />
                        </span>
                        <span className="tw-text-1xl tw-text-left tw-leading-snug" style={{ lineHeight: '34px' }}>
                            If you make a 1st reimbursement claim for a small accident, you{' '}
                            <i>
                                <b>cannot make a 2nd reimbursement claim for the same accident</b>
                            </i>
                            , even if you have been diligently paying premiums for 2 Traditional Insurance plans!
                        </span>
                        <hr className="tw-mt-8" />
                    </div>
                    <div>
                        <span
                            className="tw-bg-highlights3 tw-text-white tw-p-2 tw-mr-1 tw-border-b-2 tw-border-solid tw-border-red-900 tw-rounded tw-border-t-0 tw-border-l-0 tw-border-r-0"
                            style={{
                                fontSize: 'x-large'
                            }}
                        >
                            <DoneOutlinedIcon />
                        </span>
                        <span className="tw-text-1xl tw-text-left tw-leading-snug" style={{ lineHeight: '34px' }}>
                            If you read through your Exclusions List in detail, you may feel as though you're{' '}
                            <i>
                                <b>
                                    walking through a minefield of clauses which prevent you from making a successful
                                    claim.
                                </b>
                            </i>
                        </span>
                        <hr className="tw-mt-8" />
                    </div>
                    <div>
                        <span className="tw-text-1xl tw-text-left tw-leading-snug">
                            I guess you'll want to check your Policy certificate or Benefit illustration soon... Well,
                            we say... check out iFinSG's Small Accident cover 1st,{' '}
                            <b>because it'll take you way less time!</b>
                        </span>
                    </div>
                </div>

                <div className="tw-mt-10 tw-flex tw-items-center tw-justify-center ">
                    <Link href="/products">
                        <button className="tw-cursor-pointer tw-bg-brand tw-text-white tw-font-bold tw-text-2xl md:tw-text-3.5xl tw-border tw-border-brand tw-border-opacity-25 tw-rounded tw-px-7 tw-py-2.5 hover:tw-opacity-75">
                            Get iFinSG's Small Accident Cover Today! Just $9.80 Per Month!
                        </button>
                    </Link>
                </div>

                <div className="tw-mt-20 tw-flex tw-items-center tw-justify-center">
                    <img alt="choose" src="/advert/4.png" style={{ width: '100%', maxWidth: '505px' }} />
                </div>

                <div className="tw-mx-auto tw-mt-10 tw-space-y-8" style={{ maxWidth: '1100px' }}>
                    <p className="tw-text-1xl tw-text-left tw-leading-snug">The best part?</p>
                    <p className="tw-text-1xl tw-text-left tw-leading-snug">
                        In the next 10 minutes, apply online to get yourself covered for Practical Daily Dangers like
                        burns, scalds, sprains, cuts and even dengue fever... <b>All for less than $3 a week.</b>
                    </p>
                    <p className="tw-text-1xl tw-text-left tw-leading-snug">
                        That's less than 3 cups of Kopi a week. (And you'll even get back some money if you made 0
                        claims or low claims in every 3 months cycle!)
                    </p>
                </div>

                <div className="tw-mt-20 tw-flex tw-items-center tw-justify-center">
                    <img alt="coffee" src="/advert/5.jpg" style={{ width: '100%', maxWidth: '970px' }} />
                </div>

                <div className="tw-mx-auto tw-mt-10 tw-space-y-5" style={{ maxWidth: '1100px' }}>
                    <p className="tw-text-brand tw-font-bold tw-text-1xl">
                        If you no longer want to worry about irritating medical bills due to Practical Daily Dangers...
                    </p>

                    <p className="tw-text-brand tw-font-bold tw-text-1xl">
                        If you want to be assured of your Accident Security in a truly affordable and sustainable way...
                    </p>

                    <p className="tw-text-brand tw-font-bold tw-text-1xl">If you're ready to try us out today....</p>

                    <p className="tw-text-1xl">
                        We have great news... iFinSG's Small Accident cover is here to help. Signing up is so easy and
                        takes about 10 minutes!
                    </p>

                    <p className="tw-text-1xl">Check out our Small Accident cover details here:</p>
                </div>

                <div className="tw-mt-8 tw-mb-10 tw-flex tw-items-center tw-justify-center">
                    <Link href="/products">
                        <button className="tw-cursor-pointer tw-bg-brand tw-text-white tw-font-bold tw-text-2xl md:tw-text-3.5xl tw-border tw-border-brand tw-border-opacity-25 tw-rounded tw-px-7 tw-py-2.5 hover:tw-opacity-75">
                            Get iFinSG's Small Accident Cover Today! Just $9.80 Per Month!
                        </button>
                    </Link>
                </div>
            </div>
            <div className="tw-bg-highlights4 tw-pt-8 tw-pb-10 tw-px-3">
                <div className="tw-container tw-mx-auto">
                    <div className="tw-mx-auto" style={{ maxWidth: '1100px' }}>
                        <div className="tw-flex tw-items-center tw-justify-center">
                            <img alt="logo" src="/advert/6.png" width="100px" height="100px" />
                        </div>
                        <p className="tw-mt-5 tw-text-center tw-text-highlights5">
                            This site is not a part of the Facebook website or Facebook Inc. Additionally, this is NOT
                            endorsed by Facebook in any way. FACEBOOK is a trademark of FACEBOOK, INC.
                        </p>
                        <ul className="tw-mt-5 tw-text-center md:tw-text-left md:tw-flex tw-text-white tw-space-x-3 tw-items-center tw-justify-center tw-font-bold">
                            <li className="tw-cursor-pointer hover:tw-underline">
                                <Link href="/general-terms-and-conditions">
                                    <a className="tw-text-white" href="#">
                                        GENERAL TERMS &amp; CONDITIONS
                                    </a>
                                </Link>
                            </li>
                            <li className="tw-cursor-pointer hover:tw-underline">
                                <Link href="/terms-of-use">
                                    <a className="tw-text-white" href="#">
                                        TERMS OF USE
                                    </a>
                                </Link>
                            </li>
                            <li className="tw-cursor-pointer hover:tw-underline">
                                <Link href="/terms-of-service">
                                    <a className="tw-text-white" href="#">
                                        TERMS OF SERVICE
                                    </a>
                                </Link>
                            </li>
                            <li className="tw-cursor-pointer hover:tw-underline">
                                <Link href="/disclaimer-of-liability">
                                    <a className="tw-text-white" href="#">
                                        DISCLAIMER OF LIABILITY
                                    </a>
                                </Link>
                            </li>
                            <li className="tw-cursor-pointer hover:tw-underline">
                                <Link href="/privacy-policy">
                                    <a className="tw-text-white" href="#">
                                        PRIVACY POLICY
                                    </a>
                                </Link>
                            </li>
                        </ul>
                        <p className="tw-mt-6 tw-text-center tw-text-highlights5 tw-leading-snug">
                            Copyright © by ICH iFinancial Singapore Private Limited. UEN: 201631146G. 3D Martia Road,
                            Singapore (424786)
                        </p>
                    </div>
                </div>
            </div>
        </>
    );
}
