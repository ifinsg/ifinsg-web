import React, { Component } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import '../public/tailwind.css';
import TopNavBar from './components/TopNavBar';

export default class SmallAccidentP1 extends Component {
    constructor() {
        super();
        this.state = {};
    }
    componentDidMount = () => {
        window.scrollTo(0, 0);
    };

    render() {
        return (
            <div className="advertising">
                <TopNavBar selected="" referrer={this.state.referrer} />
                <Head>
                    <link href="https://fonts.googleapis.com/css2?family=Fjalla+One&display=swap" rel="stylesheet" />
                </Head>
                <div style={{ position: 'absolute', left: 0, right: 0, top: '72px', bottom: 0 }}>
                    <div className="tw-container tw-mx-auto tw-px-3 tw-antialiased">
                        <h2 className="tw-font-alt tw-text-center tw-mt-10 tw-mb-12 tw-text-2xl md:tw-text-3.8xl tw-font-medium tw-leading-tight tw-text-highlights1">
                            "Out Of A Job, Single Mother of 2, Things were tough."
                        </h2>
                        <h1
                            className="tw-font-alt tw-text-center tw-mx-auto tw-text-3xl md:tw-text-5.4xl tw-text-highlights0 tw-leading-tight"
                            style={{ maxWidth: '1100px' }}
                        >
                            Find Out How Linda got her life{' '}
                            <b className="tw-text-highlights2 tw-underline">back on Track</b>
                        </h1>

                        <div className="tw-flex tw-items-center tw-justify-center tw-mt-8">
                            <img alt="p1" src="/advert/step1.jpg" style={{ maxWidth: '941px', width: '100%' }} />
                        </div>
                        <div className="tw-mx-auto tw-mt-10 tw-space-y-8" style={{ maxWidth: '1100px' }}>
                            <p className="tw-text-1xl tw-text-left tw-leading-snug">
                                When Linda reached her 30s, <b>she got retrenched.</b>
                            </p>
                        </div>
                        <div className="tw-mx-auto tw-mt-10 tw-space-y-8" style={{ maxWidth: '1100px' }}>
                            <p className="tw-text-1xl tw-text-left tw-leading-snug">
                                As a mother of 2, she had to{' '}
                                <b>immediately start a new career as a Food Delivery professional,</b> despite the
                                unstable and uncertain income.
                            </p>
                        </div>
                        <div className="tw-flex tw-items-center tw-justify-center tw-mt-8">
                            <img alt="p1" src="/advert/step2.jpg" style={{ maxWidth: '941px', width: '100%' }} />
                        </div>

                        <div className="tw-mx-auto tw-mt-10 tw-space-y-8" style={{ maxWidth: '1100px' }}>
                            <p className="tw-text-1xl tw-text-left tw-leading-snug">
                                The nagging stress of having to pay unexpected medical bills from{' '}
                                <b>practical Daily Dangers </b>bothered her...
                            </p>
                        </div>
                        <div className="tw-flex tw-items-center tw-justify-center tw-mt-8">
                            <img alt="p1" src="/advert/step4.jpg" style={{ maxWidth: '941px', width: '100%' }} />
                        </div>
                        <div className="tw-mx-auto tw-mt-10 tw-space-y-8" style={{ maxWidth: '1100px' }}>
                            <p className="tw-text-1xl tw-text-left tw-leading-snug">
                                But she could no <b>longer afford to pay for comprehensive accident policies</b> for her
                                children and herself as it'll affect her finances too much.
                            </p>
                        </div>
                        <div className="tw-flex tw-items-center tw-justify-center tw-mt-8">
                            <img alt="p1" src="/advert/step3.jpg" style={{ maxWidth: '941px', width: '100%' }} />
                        </div>
                        <div className="tw-mx-auto tw-mt-10 tw-space-y-8" style={{ maxWidth: '1100px' }}>
                            <p className="tw-text-1xl tw-text-left tw-leading-snug">
                                Thankfully she found iFinSG's Small Accident cover.
                            </p>
                        </div>

                        <div className="tw-mt-10 tw-flex tw-items-center tw-justify-center ">
                            <Link href="/smallaccidentlp1">
                                <button className="tw-cursor-pointer tw-bg-brand tw-text-white tw-font-bold tw-text-2xl md:tw-text-3.5xl tw-border tw-border-brand tw-border-opacity-25 tw-rounded tw-px-7 tw-py-2.5 hover:tw-opacity-75">
                                    Find out how Linda got her life back on track with an insurance which really cares
                                    for the less fortunate.
                                </button>
                            </Link>
                        </div>
                        <div className="tw-mx-auto tw-mt-10 tw-space-y-8" style={{ maxWidth: '1100px' }}>
                            <p className="tw-text-1xl tw-text-left tw-leading-snug">
                                For just S$9.80/month, she <b>no longer worries about having to pay for sudden medical
                                bills</b> due to Daily Dangers.
                            </p>
                            <p className="tw-text-1xl tw-text-left tw-leading-snug">
                                Best of all, every 3 months, she gets a portion of her premiums back in her bank account
                                whenever she and her children make 0 claims!
                            </p>
                            <p className="tw-text-1xl tw-text-left tw-leading-snug">
                                iFinSG is an Insurance Technology company based in Singapore, and we believe that
                                insurance should be <b>Simple, Fast and Cheap.</b>
                            </p>
                            <p className="tw-text-1xl tw-text-left tw-leading-snug">
                                After many years of experience in the insurance industry, we discovered that the problem
                                was not about who was selling it, but the <b>Product itself.</b>
                            </p>
                            <p className="tw-text-1xl tw-text-left tw-leading-snug">
                                It’s the Pricing system, Product features, and Usage system which caused all the pain.
                            </p>
                            <p className="tw-text-1xl tw-text-left tw-leading-snug">
                                Which is why we've created a revolutionary system - <b>Cyclic Insurance</b> - which uses
                                the original roots of insurance, to{' '}
                                <b>make Insurance painless, fair, and affordable for everyone!</b>
                            </p>
                        </div>
                        <div className="tw-mt-20 tw-flex tw-items-center tw-justify-center">
                            <img alt="choose" src="/advert/3.png" style={{ width: '100%', maxWidth: '1100px' }} />
                        </div>

                        <div className="tw-mx-auto tw-mt-3 tw-space-y-1" style={{ maxWidth: '1100px' }}>
                            <div>
                                <span
                                    className="tw-inline-block tw-bg-highlights tw-text-center tw-bg-highlights6 tw-text-white tw-text-2xl tw-border-b-2 tw-border-solid tw-border-highlights7 tw-border-t-0 tw-border-l-0 tw-border-r-0"
                                    style={{
                                        minHeight: '44px',
                                        minWidth: '44px',
                                        lineHeight: '44px',
                                        marginRight: '10px'
                                    }}
                                >
                                    1
                                </span>
                                <span className="tw-text-1xl tw-leading-snug">
                                    We only earn a flat and transparent monthly Admin Fee per policy per user, with no
                                    hidden charges. And .... it's just $3 a month now!
                                </span>
                                <hr />
                            </div>
                            <div>
                                <span
                                    className="tw-inline-block tw-bg-highlights tw-text-center tw-bg-highlights6 tw-text-white tw-text-2xl tw-border-b-2 tw-border-solid tw-border-highlights7 tw-border-t-0 tw-border-l-0 tw-border-r-0"
                                    style={{
                                        minHeight: '44px',
                                        minWidth: '44px',
                                        lineHeight: '44px',
                                        marginRight: '10px'
                                    }}
                                >
                                    2
                                </span>
                                <span className="tw-text-1xl tw-leading-snug">
                                    Unclaimed premiums in the pool, will be distributed back to users who made 0 claims
                                    & users who made low claims, at the end of each 3 months cycle.
                                </span>
                                <hr />
                            </div>
                            <div>
                                <span
                                    className="tw-inline-block tw-bg-highlights tw-text-center tw-bg-highlights6 tw-text-white tw-text-2xl tw-border-b-2 tw-border-solid tw-border-highlights7 tw-border-t-0 tw-border-l-0 tw-border-r-0"
                                    style={{
                                        minHeight: '44px',
                                        minWidth: '44px',
                                        lineHeight: '44px',
                                        marginRight: '10px'
                                    }}
                                >
                                    3
                                </span>
                                <span className="tw-text-1xl tw-leading-snug">
                                    We will always allow you to make a 2nd claim from us for the same accident /
                                    incident, even after you make a 1st claim from a traditional insurer.
                                </span>
                            </div>
                        </div>

                        <div className="tw-mt-20 tw-flex tw-items-center tw-justify-center">
                            <img alt="choose" src="/advert/step5.jpg" style={{ width: '100%', maxWidth: '1100px' }} />
                        </div>

                        <div className="tw-mt-10 tw-flex tw-items-center tw-justify-center ">
                            <Link href="/smallaccidentlp1">
                                <button className="tw-cursor-pointer tw-bg-brand tw-text-white tw-font-bold tw-text-2xl md:tw-text-3.5xl tw-border tw-border-brand tw-border-opacity-25 tw-rounded tw-px-7 tw-py-2.5 hover:tw-opacity-75">
                                    Find out how Linda got her life back on track with an insurance which really cares
                                    for the less fortunate.
                                </button>
                            </Link>
                        </div>
                        <br />
                        <br />
                    </div>
                    <div className="tw-bg-highlights4 tw-pt-8 tw-pb-10 tw-px-3">
                        <div className="tw-container tw-mx-auto">
                            <div className="tw-mx-auto" style={{ maxWidth: '1100px' }}>
                                <div className="tw-flex tw-items-center tw-justify-center">
                                    <img alt="logo" src="/advert/6.png" width="100px" height="100px" />
                                </div>
                                <p className="tw-mt-5 tw-text-center tw-text-highlights5">
                                    This site is not a part of the Facebook website or Facebook Inc. Additionally, this
                                    is NOT endorsed by Facebook in any way. FACEBOOK is a trademark of FACEBOOK, INC.
                                </p>
                                <ul className="tw-mt-5 tw-text-center md:tw-text-left md:tw-flex tw-text-white tw-space-x-3 tw-items-center tw-justify-center tw-font-bold">
                                    <li className="tw-cursor-pointer hover:tw-underline">
                                        <Link href="/general-terms-and-conditions">
                                            <a className="tw-text-white" href="#">
                                                GENERAL TERMS &amp; CONDITIONS
                                            </a>
                                        </Link>
                                    </li>
                                    <li className="tw-cursor-pointer hover:tw-underline">
                                        <Link href="/terms-of-use">
                                            <a className="tw-text-white" href="#">
                                                TERMS OF USE
                                            </a>
                                        </Link>
                                    </li>
                                    <li className="tw-cursor-pointer hover:tw-underline">
                                        <Link href="/terms-of-service">
                                            <a className="tw-text-white" href="#">
                                                TERMS OF SERVICE
                                            </a>
                                        </Link>
                                    </li>
                                    <li className="tw-cursor-pointer hover:tw-underline">
                                        <Link href="/disclaimer-of-liability">
                                            <a className="tw-text-white" href="#">
                                                DISCLAIMER OF LIABILITY
                                            </a>
                                        </Link>
                                    </li>
                                    <li className="tw-cursor-pointer hover:tw-underline">
                                        <Link href="/privacy-policy">
                                            <a className="tw-text-white" href="#">
                                                PRIVACY POLICY
                                            </a>
                                        </Link>
                                    </li>
                                </ul>
                                <p className="tw-mt-6 tw-text-center tw-text-highlights5 tw-leading-snug">
                                    Copyright © by ICH iFinancial Singapore Private Limited. UEN: 201631146G. 3D Martia
                                    Road, Singapore (424786)
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
