import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NoSsr, Button, Divider, Grid, Paper, IconButton, Typography, Tooltip, TextField, Checkbox, FormControlLabel, Modal } from '@material-ui/core';

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
// import Logo from './ifinsg_logo.png'
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import TopNavBar from './components/TopNavBar'
import BotNavBar from './components/BotNavBar'
import Router from 'next/router';
import Constants from 'constants/constants';
import ScrollToTop from './components/widgets/ScrollToTop';
import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";
import Head from 'next/head';
import Link from "next/link";
import ModalHomeFeatures from "./components/modals/ModalHomeFeatures";
// import Helpers from '../global/helpers'
import GlobalConstants from '../global/constants'      // This doe not work, but helpers works. wut???
import GlobalHelpers from '../global/helpers'      // This doe not work, but helpers works. wut???
import axios from 'axios'
import AliceCarousel from 'react-alice-carousel';        // https://www.npmjs.com/package/react-alice-carousel
import ModalPolicyDetails from './components/modals/ModalPolicyDetails'
import ModalMoneyBackGuarantee from './components/modals/ModalMoneyBackGuarantee'

import Tick from 'components/icons/Tick';
var validator = require("email-validator")   //Source: https://www.npmjs.com/package/email-validator

const name_display = (policy) => {
  console.log('name_display', policy)
  return Constants.product_details[policy].name
}

const cycle_length = 3

const price_display = (policy, price_component, period, line_break) => {
  let price_original = -1
  let price_promotion = -1
  let on_promotion = false

  switch (price_component) {
    case "premium":
      price_original = Constants.product_details[policy].premium
      if (Constants.product_details[policy].premium_on_promotion) {
        price_promotion = Constants.product_details[policy].premium_promotional_price
        on_promotion = true
      }
      break;
    case "admin_fee":
      price_original = Constants.product_details[policy].admin_fee
      if (Constants.product_details[policy].admin_fee_on_promotion) {
        price_promotion = Constants.product_details[policy].admin_fee_promotional_price
        on_promotion = true
      }
      break;
    case "total":
      price_original = Constants.product_details[policy].total
      if (Constants.product_details[policy].total_on_promotion) {
        price_promotion = Constants.product_details[policy].total_promotional_price
        on_promotion = true
      }
      break;
    default:
      break;
  }



  if (on_promotion) {
    console.log(price_component + " - price_promotion - " + price_promotion)
    return (
      <span>
        {/* <span style={{ textDecoration: "line-through" }}>${(period * price_original).toFixed(2)}</span> {line_break ? <br></br> : ""} */}
        <span style={{ color: Constants.color.green, fontWeight: "bold" }}> <i>${(period * price_promotion).toFixed(2)}</i> </span>
      </span>
    )
  } else {
    // console.log(price_component + " - price_original - " + price_original)
    return <span>${(period * price_original).toFixed(2)}  </span>
  }
}



const title = {
  0: "Insurance which makes no Profit off your premiums",
  1: "Systematic help at the lowest cost.",
  2: "Is Insurance actually cheaper than we thought?",

  3: "Because we want you to have Enough money in good times and bad times.",
  4: "Insurance redesigned for fairness.",
  5: "Premiums - Claims = PremiumBack.",
  6: "We do not Profit from your Premiums.",
  7: "We do not Profit from your insurance Premiums.",
  8: "We make no Profit off your insurance Premiums.",
  9: "Insurance which does not make Profit off your premiums.",
  10: "The Insurance system which does not make Profit off your premiums.",
  11: "The Insurance system which makes no Profit off your premiums.",
  12: "Insurance which makes no Profit off your premiums.",
  13: "A flat fee to run an Insurance system without Profit.",
  14: "Insurance Premiums collected only to pay out Claims.",
  15: "A system where total Claims = total insurance Premiums.",
  16: "Cyclic insurance only collects premiums as a pool in order to pay out claims.",
  17: "Insurance which collects Premiums just to pay out Claims.",
  18: "Insurance redesigned.",
  19: "Because Insurance should be created Differently.",
  20: "Should Insurance remain like what it is today?",
  21: "Can insurance be cheaper?",
  22: "We return excess premiums back to you.",
  23: "No Claims? Get your PremiumBack!",
  24: "The same protection, done Different.",
  25: "The same protection, done Differently."
}

const homepageVariant = GlobalHelpers.generateRandomInteger(26)
// const homepageVariant = 2


const styles = {
  page_width: Constants.style.page.width,
  page_text_width: Constants.style.page.text_width,
  page_padding: Constants.style.page.padding,

  align_center: Constants.style.align.center,

  bold: Constants.style.text.bold,
  break_word: Constants.style.text.break_word,

  title1: Constants.style.text.title1,
  body1: Constants.style.text.body1,

  margin_top_xl: Constants.style.margin_top.xl,
  margin_top_lg: Constants.style.margin_top.lg,
  margin_top_md: Constants.style.margin_top.md,
  margin_top_sm: Constants.style.margin_top.sm,
  margin_top_xs: Constants.style.margin_top.xs,
  margin_top_xxs: Constants.style.margin_top.xxs,
}

let referrer = ""


class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: 'small_pmd_protect',
      // promo: "promo_small_accident_1",
      name: "",
      email: "",
      mobile: "",
      policy_details_modal_open: false,
      policy_details_modal_policy: ""
    };
  }


  componentDidMount = () => {
    Constants.store_referrer(this)
  }


  handlePolicyDetailsClicked = (policy) => {
    console.log('handlePolicyDetailsClicked', policy)

    this.setState({
      policy_details_modal_open: true,
      policy_details_modal_policy: policy
    })
  }


  handleFieldChange = (prop) => e => {
    this.setState({
      [prop]: e.target.value
    })
  }


  renderProducts = () => {
    let product = Constants.products_on_sale[4]

    if (true) {//(isBrowser) {
      return (
        <Paper style={{ padding: '10px 20px 0px 20px', margin: '0px 0px 15px 0px', height: '100%', textAlign: 'center', position: "relative", maxWidth: "500px", display: "inline-block" }}>

          <div style={{ margin: '50px 3px 10px 3px', fontSize: '24px', fontWeight: 'bold', lineHeight: '1', color: '#7cda24', minHeight: "40px" }}>  Cyclic: {name_display(product.policy)}</div>

          <Grid container spacing={0} align="center" style={{}}>
            <Grid item xs={12} sm={6} md={6} style={{ marginTop: "0px", textAlign: 'center', wordWrap: "break-word", color: Constants.color.grey }}>

              <img style={{ maxWidth: '270px', width: '100%', margin: '0px 0px 0px 0px', padding: '0px 0px 0px 0px' }} src={('/img/' + product.img)} />
            </Grid>

            <Grid item xs={12} sm={6} md={6} style={{ marginTop: "20px", textAlign: 'center', wordWrap: "break-word", color: Constants.color.grey }}>

              <table style={{ marginLeft: "auto", marginRight: "auto", minWidth: "70%", marginBottom: "10px" }}>
                <tbody style={{ textAlign: "center", width: "100%", fontSize: '20px', }}>
                  {/* <tr > <td colspan="2" style={{ textAlign: "center", paddingTop: "0px", fontSize: "16px", fontWeight:"bold" }}>Per 3 Months</td> </tr> */}
                  <tr> <td style={{ width: "65%", textAlign: "left", paddingLeft: "5px" }}>Premium</td> <td style={{ textAlign: "right", paddingRight: "5px" }}>{price_display(product.policy, "premium", 3, true)} </td> </tr>
                  <tr style={{ height: "5px", padding: 0, margin: 0 }}> <td colspan="2" style={{ padding: "0px", margin: "0px", textAlign: "right", padding: "0px 5px 0px 1px", fontSize: "20px", transform: "translateY(-5px)", color: Constants.color.gold }}> ({price_display(product.policy, "premium", 1)} per month) </td> </tr>

                  <tr> <td style={{ width: "65%", textAlign: "left", paddingLeft: "5px" }}>Admin Fee</td> <td style={{ textAlign: "right", paddingRight: "5px" }}>{price_display(product.policy, "admin_fee", 3, true)}</td> </tr>
                  <tr style={{ height: "5px", padding: 0, margin: 0 }}> <td colspan="2" style={{ padding: "0px", margin: "0px", textAlign: "right", padding: "0px 5px 0px 1px", fontSize: "20px", transform: "translateY(-5px)", color: Constants.color.gold }}> ($3.00 per month) </td> </tr>

                  <tr> <td style={{ width: "65%", textAlign: "left", paddingLeft: "5px", fontWeight: "bold", color: Constants.color.darkgreen }}>Total</td> <td style={{ textAlign: "right", paddingRight: "5px", fontWeight: "bold", color: Constants.color.darkgreen }}>{price_display(product.policy, "total", 3, true)}</td> </tr>
                  <tr > <td colspan="2" style={{ textAlign: "right", padding: "5px 5px 1px 1px", fontSize: "13px", fontWeight: "bold", transform: "translateY(-6px)", color: Constants.color.darkgreen, lineHeight: 1.1 }}>per Cycle of <span style={{ fontStyle: "italic", color: cycle_length == 6 ? "rgb(50, 67, 183)" : "rgb(134, 149, 253)" }}>{cycle_length} months</span></td> </tr>
                  <tr style={{ height: "5px", padding: 0, margin: 0 }}> <td colspan="2" style={{ padding: "0px", margin: "0px", textAlign: "right", padding: "0px 5px 0px 1px", fontSize: "20px", transform: "translateY(-5px)", color: Constants.color.gold }}> ({price_display(product.policy, "total", 1, true)} per month) </td> </tr>

                </tbody>
              </table>

              <div style={{ width: "100%", margin: "0px 0px 5px 0px", }}>
                <Button style={{ fontSize: "15px" }} onClick={() => { this.handlePolicyDetailsClicked(this.state.product) }}> <u>Policy Details</u> </Button>
              </div>

            </Grid>
          </Grid>

          <div style={{ textAlign: "center", width: "100%", margin: "10px 0px 10px 0px" }}>

            {/* <Link href={{ pathname: Constants.get_new_page("products", this.props.referrer) }}>
              <Button
                onClick={() => {
                  // axios.post("/api/trackingData", {
                  //   tracking_data: {
                  //     homepage_variant: homepageVariant,
                  //     title: title[homepageVariant],
                  //     button: "get_insured"
                  //   }
                  // }).catch(err => {
                  //   console.log(err);
                  // });
                }}
                variant="contained"
                style={{ fontSize: '17px', width: "100%", margin: "0px 0px 10px 0px", backgroundColor: Constants.color.green, color: 'white' }}>
                BUY NOW
              </Button>
            </Link> */}

            {/* <div style={{ padding: "0px", margin: "0px", textAlign: "right", padding: "0px 5px 0px 1px", fontSize: "13px", transform: "translateY(-5px)" }}> *Usual price $5.00</div> */}

          </div>

          {product.availability !== "available" ? <p style={{
            position: "absolute", bottom: 0, fontSize: "24px", background: "rgba(251,251,251,0.6)",
            height: "100%", width: "100%", textAlign: "center", display: "flex", margin: 0
          }}>
            <Typography style={{ fontSize: "26px", color: "darkblue", opacity: 0.4, flex: "auto", alignSelf: "center", fontWeight: "bold", transform: "rotate(-45deg)" }}> Coming soon </Typography>
          </p>
            :
            ""
          }

        </Paper>
      )
    }






    let returnable = []


    // for(let i = 0; i < Constants.products_on_sale.length; i++){
    //   if(i === 0){
    returnable.push(
      // <Grid item xs={12} md={4} lg={3} alignItems='stretch' style={{ padding: '2px 0px 2px 0px', display: "inline-block" }}>
      <Grid item xs={12} alignItems='stretch' style={{ padding: '2px 0px 2px 0px', display: "inline-block" }}>

        <Paper style={{ padding: '10px 0px 0px 0px', margin: '0px 0px 15px 0px', height: '100%', textAlign: 'center', position: "relative" }}>

          <div style={{ margin: '10px 3px 0px 3px', fontSize: '19px', fontWeight: 'bold', lineHeight: '1', color: '#7cda24', minHeight: "40px" }}>  Cyclic: {name_display(product.policy)}</div>
          <img style={{ maxWidth: '150px', width: '100%', margin: '0px 0px 0px 0px', padding: '0px 0px 0px 0px' }} src={('/img/' + product.img)} />

          <table style={{ marginLeft: "auto", marginRight: "auto", }}>
            <tbody style={{ textAlign: "center", width: "100%" }}>
              {/* <tr > <td colspan="2" style={{ textAlign: "center", paddingTop: "0px", fontSize: "16px", fontWeight:"bold" }}>Per 3 Months</td> </tr> */}
              <tr> <td style={{ width: "65%", textAlign: "left", paddingLeft: "5px" }}>Premium</td> <td style={{ textAlign: "right", paddingRight: "5px" }}>{price_display(product.policy, "premium", 3, true)} </td> </tr>
              <tr style={{ height: "5px", padding: 0, margin: 0 }}> <td colspan="2" style={{ padding: "0px", margin: "0px", textAlign: "right", padding: "0px 5px 0px 1px", fontSize: "13px", transform: "translateY(-5px)" }}> ({price_display(product.policy, "premium", 1)} per month) </td> </tr>

              <tr> <td style={{ width: "65%", textAlign: "left", paddingLeft: "5px" }}>Admin Fee</td> <td style={{ textAlign: "right", paddingRight: "5px" }}>{price_display(product.policy, "admin_fee", 3, true)}</td> </tr>
              <tr style={{ height: "5px", padding: 0, margin: 0 }}> <td colspan="2" style={{ padding: "0px", margin: "0px", textAlign: "right", padding: "0px 5px 0px 1px", fontSize: "13px", transform: "translateY(-5px)" }}> ({price_display(product.policy, "admin_fee", 1)} per month) </td> </tr>

              <tr> <td style={{ width: "65%", textAlign: "left", paddingLeft: "5px", fontWeight: "bold", color: Constants.color.gold }}>Total</td> <td style={{ textAlign: "right", paddingRight: "5px", fontWeight: "bold", color: Constants.color.gold }}>{price_display(product.policy, "total", 3, true)}</td> </tr>
              <tr > <td colspan="2" style={{ textAlign: "right", padding: "0px 5px 1px 1px", fontSize: "13px", fontWeight: "bold", transform: "translateY(-6px)", color: Constants.color.gold }}>per 3 months</td> </tr>
            </tbody>
          </table>

          <div style={{ textAlign: "center", width: "100%", margin: "10px 0px 10px 0px" }}>

            <Link href={{ pathname: Constants.get_new_page("products", this.props.referrer) }}>


              <Button
                onClick={() => {
                  // axios.post("/api/trackingData", {
                  //   tracking_data: {
                  //     homepage_variant: homepageVariant,
                  //     title: title[homepageVariant],
                  //     button: "get_insured"
                  //   }
                  // }).catch(err => {
                  //   console.log(err);
                  // });
                }}
                variant="contained"
                style={{ fontSize: '13px', width: "85%", margin: "0px 0px 10px 0px", backgroundColor: Constants.color.green, color: 'white' }}>
                Get Insured
                              </Button>
            </Link>


            {/* <Button style={{ fontSize: '10px', padding: "5px 0px 5px 0px", backgroundColor: "blue", opacity: 0.7, color: 'white', lineHeight: 1.1 }} onClick={() => { this.handlePolicyDetailsClicked(product.policy) }}> Policy<br />Details</Button>
                    <Button style={{ fontSize: '13px', padding: "5px 10px 5px 10px", backgroundColor: '#7cda24', color: 'white' }} onClick={() => { this.handleAddToCart(product.policy) }}>
                      Add to Cart
          </Button> */}
          </div>



          {product.availability !== "available" ? <p style={{
            position: "absolute", bottom: 0, fontSize: "24px", background: "rgba(251,251,251,0.6)",
            height: "100%", width: "100%", textAlign: "center", display: "flex", margin: 0
          }}>
            <Typography style={{ fontSize: "26px", color: "darkblue", opacity: 0.4, flex: "auto", alignSelf: "center", fontWeight: "bold", transform: "rotate(-45deg)" }}> Coming soon </Typography>
          </p>
            :
            ""
          }

        </Paper>
      </Grid>
    )


    if (isMobile) {
      return (
        <Grid container spacing={0} alignItems='stretch' >
          {returnable}
        </Grid>
      )
    } else {
      return (
        <div style={{ padding: "0px 0px 0px 0px" }}>
          {returnable}
        </div>
      )
    }

  }





  render() {
    const { classes } = this.props;

    return (
      <div>
        <NoSsr>
          <TopNavBar selected="home" referrer={this.state.referrer} homepageVariant={homepageVariant} title={title[homepageVariant]} />
        </NoSsr>

        <NextSeo config={Constants.next_seo_config("home")}> </NextSeo>

        <NoSsr>

          <div style={{ textAlign: "center", width: "100%" }}>

            <div className={classNames(classes.page_text_width, classes.page_padding, classes.break_word)}>
              <div style={{ paddingTop: "60px" }}></div>
              <div> <img style={{ maxWidth: '260px', padding: '70px 0px 0px 0px' }} src={('/img/Pic-1-Home.png')} /> </div>
              <div className={classNames(classes.title1, classes.margin_top_xxs)} style={{ fontWeight: "bold", marginTop: "35px", maxWidth: "500px", display: "inline-block", fontSize: "30px", fontFamily: "tahoma" }}> Because accidents could happen to you or your PMD </div>

            </div>
            {/* <BrowserView> */}
            <ModalPolicyDetails
              page={"products"}
              open={this.state.policy_details_modal_open}
              handleClose={() => { console.log("handleCLose activated"); this.setState({ policy_details_modal_open: false }) }}
              policy={this.state.policy_details_modal_policy}
              referrer={this.state.referrer}
            />


            <Grid container spacing={0} align="center" style={{}}>
              <Grid item xs={12} sm={6} md={6} style={{ marginTop: "50px", textAlign: isBrowser ? 'right' : "center", wordWrap: "break-word", color: Constants.color.grey }}>
                <div style={{ marginRight: isBrowser ? "20px" : 0, }}>
                  {this.renderProducts()}
                </div>
              </Grid>
              <Grid item xs={12} sm={6} md={6} style={{ marginTop: "40px", textAlign: 'left', paddingleft: "10px", wordWrap: "break-word", color: Constants.color.grey }}>
                <div style={{ margin: "0px 20px 0px 20px", maxWidth: "500px" }}>



                  <table style={{ textAlign: "left", fontSize: "20px", marginTop: "0px", maxWidth: "800px", display: "inline-block", paddingRight: "20px" }} border={0}>

                    <tr >
                      <td style={{ verticalAlign: "top" }}>
                        <div style={{ float: "right" }}>
                          <ModalHomeFeatures feature="policy_details_small_pmd_protect" />
                        </div>
                        <div style={{ padding: "10px" }}>
                          <Tick /> <b>Reimburses you 80% of the PMD repair bill</b>!
                        </div >

                      </td>
                    </tr>


                    <tr >
                      <td style={{ verticalAlign: "top", paddingTop: "10px" }}>
                        <div style={{ float: "right" }}>
                          <ModalHomeFeatures feature="policy_details_small_pmd_protect" />
                        </div>
                        <div style={{ padding: "10px" }}>
                          <Tick /><b>Up to $40 for a One-way transport fee</b> to the repair centre!
                      </div>
                      </td>
                    </tr>


                    <tr >
                      <td style={{ verticalAlign: "top", paddingTop: "10px" }}>
                        <div style={{ float: "right" }}>
                          <ModalHomeFeatures feature="policy_details_small_pmd_protect" />
                        </div>
                        <div style={{ padding: "10px" }}>
                          <Tick /><b>Up to $500 per accident</b> for physical injuries!
                      </div>
                      </td>
                    </tr>


                    <tr>
                      <td style={{ verticalAlign: "top" }}>
                        <div style={{ float: "right" }}>
                          <ModalHomeFeatures feature="practical" />
                        </div>
                        <div style={{ padding: "10px" }}>
                          <Tick /> Make <b>a 2nd Reimbursement claim with us</b> even for the same accident!
                         </div>
                      </td>
                    </tr>


                    <tr>
                      <td style={{ verticalAlign: "top" }}>
                        <div style={{ float: "right" }}>
                          <ModalHomeFeatures feature={6} />
                        </div>
                        <div style={{ padding: "10px" }}>
                          <Tick /> Guaranteed <b>PremiumBack</b> every 3 months even when you make 0 claims / low claims!
                         </div>
                      </td>
                    </tr>
                  </table>
                </div>

              </Grid>

              <Grid item xs={12} style={{ margin: "20px 20px 0px 20px", textAlign: "center", wordWrap: "break-word", color: Constants.color.grey }}>
                <Typography>We only earn a flat and transparent monthly <span style={{ fontWeight: "bold" }}>Admin Fee </span> per policy per user, with no hidden charges.</Typography>
              </Grid>
              <Grid item xs={12} style={{ marginTop: "30px", textAlign: "center", wordWrap: "break-word", color: Constants.color.grey }}>
                <div style={{ maxWidth: "500px", width: "100%", display: "inline-block" }}>
                  <Button style={{ height: "60px", borderRadius: "35px", width: "51%", fontSize: '24px', padding: "10px 0px 10px 0px", backgroundColor: "orange", color: 'white' }} onClick={() => {
                    let urlParams = "?products=" + '[' + JSON.stringify(this.state.product) + ']'
                    let page = Constants.get_new_page("signup", this.state.referrer) + urlParams
                    Router.push(page)
                  }}>
                    Get Insured
                </Button>

                  <div style={{ width: "100%", margin: "10px 0px 0px 0px", }}>
                    <ModalMoneyBackGuarantee />
                  </div>

                </div>
              </Grid>



              <div style={{ position: "relative", width: "100%", textAlign: "center" }}>
                <div>
                  {!Constants.promotion_ongoing ? "" : <img style={{ maxWidth: '420px', width: "100%", margin: '35px 0px 0px 0px', padding: '0px 0px 0px 0px' }} src={('/img/promo_priority_claims_status.png')} ></img>}
                  <div style={{
                    position: "absolute", display: "flex", top: "130px", width: "100%",
                    fontSize: "20px", fontWeight: "bold", textAlign: "center"
                  }}>
                    <div style={{ width: "150px", marginLeft: "auto", marginRight: "auto", lineHeight: 1.1 }}>
                    </div>
                  </div>
                </div>
              </div>

              <div style={{ margin: "50px" }}></div>

            </Grid>

          </div>
        </NoSsr>

        <BotNavBar page='home' referrer={this.state.referrer} />

      </div >
    );
  }
}



export default withStyles(styles)(Home);