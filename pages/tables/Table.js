import React, { Component } from 'react';

import { NoSsr, Button, Typography, IconButton, AppBar, Toolbar } from '@material-ui/core';
import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";
import classNames from 'classnames';
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import { withStyles } from '@material-ui/core/styles';
import Constants from 'constants/constants';
import MUIDataTable from "mui-datatables";      // Source: https://github.com/gregnb/mui-datatables
import axios from 'axios'
import GlobalConstants from '../../global/constants';


const styles = {
  page_width: Constants.style.page.width,
  page_text_width: Constants.style.page.text_width,

  page_padding: Constants.style.page.padding,

  align_center: Constants.style.align.center,
  align_left: Constants.style.align.left,

  bold: Constants.style.text.bold,
  break_word: Constants.style.text.break_word,

  title1: Constants.style.text.title1,
  body1: Constants.style.text.body1,

  margin_top_xl: Constants.style.margin_top.xl,
  margin_top_lg: Constants.style.margin_top.lg,
  margin_top_md: Constants.style.margin_top.md,
  margin_top_sm: Constants.style.margin_top.sm,
  margin_top_xs: Constants.style.margin_top.xs,
  margin_top_xxs: Constants.style.margin_top.xxs,
}



class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // NOTE: title, data can be dynamically altered. columns cannot.
      // title: "Table Title",
      // columns: [
      //   { name: "name", label: "Name", options: { filter: true, sort: true, } },
      //   { name: "company", label: "Company", options: { filter: true, sort: true, } },
      //   { name: "city", label: "City", options: { filter: true, sort: true, } },
      //   { name: "state", label: "State", options: { filter: true, sort: true, } },
      // ],
      // data: [
      //   { name: "Joe James", company: "Test Corp", city: "Yonkers", state: "NY" },
      //   { name: "John Walsh", company: "Test Corp", city: "Hartford", state: "CT" },
      //   { name: "Bob Herm", company: "Test Corp", city: "Tampa", state: "FL" },
      //   { name: "James Houston", company: "Test Corp", city: "Dallas", state: "TX" },
      // ],
      // columns: ["City222", "Company", "City", "State"],    
      // data: [
      //   ["Joe James", "Test Corp", "Yonkers", "NY"],
      //   ["John Walsh", "Test Corp", "Hartford", "CT"],
      //   ["Bob Herm", "Test Corp", "Tampa", "FL"],
      //   ["James Houston", "Test Corp", "Dallas", "TX"],
      // ],
      options: {
        responsive: "scroll", // stacked or scroll
        selectableRows: false, // show checkboxes
        rowsPerPageOptions: [10, 20, 50],
        // rowsPerPage: 20,  // Should be one of above
        filterType: "checkbox",
        sort: true,
        print: false,
        download: false,
        search: true,
        caseSensitive: false, //caseSensitivity does not work well - cannot find all letters in CAPs :(
        // viewColumns: false,
        // filter: false,

        // resizableColumns:true,
        // expandableRows: true,
        // renderExpandableRow: (rowData, rowMeta) => {
        //   return (
        //     <tr style={{ rowspan: "3", width: "100%" }}>
        //       <td  colspan="100%" >testing testing test test test testing test test test testing </td>
        //     </tr>
        //   )
        // },
        // onRowsSelect: (currentRowsSelected, rowsSelected) => {
        //   alert(
        //     JSON.stringify(currentRowsSelected) + " " + JSON.stringify(rowsSelected)
        //   );
        // },
        // onCellClick: (colData, cellMeta) => {
        //   console.log(colData)
        //   console.log(cellMeta)
        // },
        // customToolbarSelect: () => { }
      }
    };
  }



  componentDidUpdate = (nextProps) => {
    // if (this.props.date_start != nextProps.date_start) {
    //   this.getTableData()
    // }
    if (this.props != nextProps) {
      this.getTableData()
    }
  }


  getTableData = () => {
    console.log("getTableData()")
    try {
      const token = localStorage.getItem('ifinsg_token');
      if (token) {

        let date_timezone_offset = new Date().getTimezoneOffset()

        // console.log(date1.getTimezoneOffset());
        // console.log(new Date(date1.getTime() + offset * 1000 * 60 ))
        // let date_start = new Date(new Date("2019-05-22").getTime() + date_timezone_offset * 60 * 1000)
        let date_start = new Date(new Date(this.props.date_start).getTime() + date_timezone_offset * 60 * 1000)
        let date_end = new Date(date_start.getTime() + 24 * 60 * 60 * 1000)

        axios.post( "/api/admin/getTableData", {
          token,
          table: this.props.table,
          table_variant: this.props.table_variant,
          date_start,
          date_end,
        }).then(res => {
          if (res.status === 200) {
            if (res.data.err) {
              alert(res.data.err)     // Eg: ERROR - Email already in use
            } else {
              try {
                // console.log(res.data)

                // Correcting for 
                // let columns = res.data.columns

                this.setState({
                  title: res.data.title,
                  data: res.data.data,
                  columns: res.data.columns
                }, () => {
                  // console.log(this.state)
                })
              } catch (err) {
                console.log(err)
                console.log("Unexpected params received from api/getTableData")
              }
            }
          } else {
            console.log("ERROR - ", res)
            alert("ERROR - ", res)
          }
        }).catch(err => {
          console.log(err);
          alert("ERROR - ", err)
        });
      }
    } catch (err) {
      console.log(err)
      alert("ERROR - ", err)
    }
  }


  componentDidMount = () => {
    this.getTableData()
  }



  getMuiTheme = () => {
    return createMuiTheme({
      overrides: {
        MuiTable: {
          root: {
            whiteSpace: "nowrap"
          }
        },
        // MuiPaper: {
        //   elevation4: {
        //     boxShadow: "0px 1px 1px 0px rgba(0, 0, 0, 0.1), 0px 0px 0px 0px rgba(0, 0, 0, 0.1), 0px 0px 1px 0px rgba(0, 0, 0, 0.1)" //Reduce the perceived elevation
        //   },
        // },

        // MUIDataTableHeadRow: {
        //   //Change table header CSS
        //   root: {
        //     "&>th": {
        //       // width: '10rem',
        //       padding: "1px 10px 1px 10px"
        //     },
        //     // "&>th:nth-child(2)": { //first column
        //     //   paddingLeft: "25px" //padding before first column for non-checkboxed tables
        //     // },
        //     // "&>th:nth-child(1)": { //first column
        //     //   backgroundColor: "yellow",
        //     //   display: "none"
        //     // },
        //   }
        // },

        // MUIDataTableBodyCell: {
        //   //CSS of all cells
        //   root: {
        //     padding: "1px 10px 1px 10px"
        //   }
        // },

      }
    });
  }



  render() {

    return (
      <div>
        <MuiThemeProvider theme={this.getMuiTheme()}>
          <MUIDataTable
            title={this.state.title}
            data={this.state.data}
            columns={this.state.columns}
            options={this.state.options}
          />
        </MuiThemeProvider>
      </div>
    );
  }
}


export default withStyles(styles)(Table);