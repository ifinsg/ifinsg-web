import React, { Component } from 'react';

// import Logo from './ifinsg_logo.png'
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import { NoSsr } from '@material-ui/core';

import TopNavBar from './components/TopNavBar'
import BotNavBar from './components/BotNavBar'


import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Constants from 'constants/constants';
import EmailProfileAdvertising from './components/constant_components/EmailProfileAdvertising';
import Head from 'next/head';


const styles = {
  page_width: Constants.style.page.width,
  page_text_width: Constants.style.page.text_width,

  page_padding: Constants.style.page.padding,

  align_center: Constants.style.align.center,
  align_left: Constants.style.align.left,

  bold: Constants.style.text.bold,
  break_word: Constants.style.text.break_word,

  title1: Constants.style.text.title1,
  body1: Constants.style.text.body1,

  margin_top_xl: Constants.style.margin_top.xl,
  margin_top_lg: Constants.style.margin_top.lg,
  margin_top_md: Constants.style.margin_top.md,
  margin_top_sm: Constants.style.margin_top.sm,
  margin_top_xs: Constants.style.margin_top.xs,
  margin_top_xxs: Constants.style.margin_top.xxs,
}
class TermsOfService extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }


  componentDidMount = () => {
    Constants.store_referrer(this)
  }


  render() {
    const { classes } = this.props;
    return (
      <div>
        <TopNavBar referrer={this.state.referrer} />

        <NextSeo config={Constants.next_seo_config("terms-of-service")} />

        <NoSsr>
          <div className={classNames(classes.align_center)}>
            <div className={classNames(classes.page_width, classes.page_padding, classes.break_word, classes.align_left)}>


              <div style={{ padding: '50px 0px 0px 0px', textAlign: "center" }}>
                <img style={{ maxWidth: '200px', margin: '40px 0px 0px 0px', padding: '0px 0px 0px 0px' }} src={('/img/iFin-SG-Logo.png')} />
                <div style={{ fontSize: '50px', lineHeight: '1', margin: '30px 0px 0px 0px', color: "#4cae4f", fontWeight: "bold" }}>TERMS OF SERVICE</div>
              </div>


              <div style={{ padding: '0px 0px 50px 0px' }}>

                <div style={{ margin: '70px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}><u>PROVISION OF DIRECTORY SERVICES</u></div>

                <div style={{ margin: '100px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}>Purpose As a Directory</div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>The Website allows a Service Provider (“the Service Provider”) to offer to perform services to members of the public seeking such services (“the End-User”). The contacts of the Service Provider and the type of Services he provides will be displayed on the Website to be accessed by the interested End-User, utilising the Company’s proprietary matching technology.</li>
                    <li>Any legal contract is strictly between the End-User and the Service Provider themselves only and not with the Company. The Services are offered by the Service Provider and requested or accepted by the End-Users who are both parties contracting as independent entities between themselves not involving the Company.</li>
                    <li>The terms of such contracts are subject to the <span style={{ fontWeight: "bold" }}>TERMS OF USE</span>, these <span style={{ fontWeight: "bold" }}>TERMS OF SERVICE</span> as well as such additional terms and conditions as may be agreed between End-User and the Service Provider.</li>
                    <li>In all instances, the Company only serves to host a directory for the registered Service Providers and End-Users, with ancillary value-added supporting services such as communication, payment, review and rating and such like services.</li>
                  </ol>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}>Registration & Dedicated Marketing Area</div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>A service provider (“Service Provider”) is required to register with the Company as a Registered Service Provider and make payment of a Listing Fee for setting up of a dedicated online profile advertising page (“Dedicated Marketing Area”) for the service provider. Subscription is currently only available for Yearly mode, and the annual membership renewal fee is payable and due, once the previous 12 months of subscription has been utilized.</li>
                    <li>The Company will provide initial assistance to the Registered Service Provider to set up the online Dedicated Marketing Area. It shall then be up to the Registered Service Provider to maintain and update its allotted dedicated online Dedicated Marketing Area, using such instructions, guidelines and updates as may be provided by the Company to the Registered Service Provider for such purposes.</li>
                    <li>To enable us to set-up the Dedicated Marketing Area for the use of the Service Provider, the Service Provider is required to email us at: <EmailProfileAdvertising /> with a detailed corporate or business profile of the Service Provider, applicable professional license or other required certificate of competence to serve as a Service Provider in the particular trade/profession/industry professed, including personal contact details and any other relevant documentation to enable the Company to verify the Service Provider’s qualifications and status.</li>
                  </ol>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}>Submission Of Information</div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>All information or material provided to or uploaded on to the Website by a Service Provider including any videos must be:
                <ol>
                        <li>true, accurate and valid.</li>
                        <li>shall not be false, inaccurate or misleading</li>
                        <li>shall not be offensive, indecent, obscene, pornographic, menacing, abusive, defamatory or in poor taste;</li>
                        <li>will not be in breach of any duty of confidence or any other rights and, in particular, will not infringe any third party’s copyright or Intellectual Property Rights, trade secrets or other proprietary rights;</li>
                        <li>will not be fraudulent or involve the sale of counterfeit or stolen items or passing off</li>
                        <li>shall not be in breach of any applicable laws or regulations (including, but not limited to, laws or regulations governing e-commerce, distance selling, data protection, consumer health and safety, export control, consumer protection and advertising);</li>
                        <li>will not create, or be likely to create, liability for the Company or cause the Company to lose (in whole or in part) the services of its internet service or other suppliers;</li>
                        <li>shall not contain any Virus; and</li>
                        <li>shall not cause this Website or any part of it or the Dedicated Marketing Area of the Registered Service Provider or of any other Registered Service Provider to lose its functionality or cause to the same to be interrupted, damaged or impaired in any way.</li>
                      </ol>
                    </li>
                    <li>If any information a Service Provider provides is found to be false, the Company may suspend or terminate your registration and account and also take legal action if the Company or any party suffers any loss or detriment owing to breach of this term. The Registered Service Provider undertakes to indemnify and keep the Company harmless from all claims, loss, expenses and damages including any claims from any registered user or any party incurred by the Company including all legal costs on full solicitor and client indemnity basis.</li>
                  </ol>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}>Representations By Service Provider</div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>As a condition of your use of our Website, the Service Provider represents that:
                <ol>
                        <li>you are an individual who is at least 21 years of age;</li>
                        <li>you have the authority and capacity to enter into a binding legal obligation;</li>
                        <li>you are the authorised owner and signatory of any PayPal, bank account and/or other payment facilitation service and/or credit or charge card provided to us to receive the Agreed Price;</li>
                        <li>you are a registered commercial business entity, duly registered with the Accounting and Corporate Regulatory Authority of Singapore and may legitimately provide the registered services and receive payments in Singapore;</li>
                        <li>you have the necessary skills and experience and is competent to provide the services you submit to this Website;</li>
                        <li>you have the right to provide any and all information you submit to the Website is accurate, true, current and complete; and</li>
                        <li>By submitting or transmitting any material on or through the Website, you further represent that you are the owner of the material, or have a right to reproduce, display or transmit such material or are making your submission or transmission with the express consent of the owner and that there is no infringement of any intellectual property rights of another.</li>
                      </ol>
                    </li>
                    <li>In the event of your misrepresentation of any of the above matters, the Service Provider shall be liable to indemnify and hold harmless us, our affiliates, agents and representatives against all damages, costs and expenses including any claim from any third parties and legal costs on full solicitor and client indemnity basis that arises from such misrepresentation to us.</li>
                  </ol>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}>Obligations Of Service Provider</div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>The Service Provider further hereby agrees as follows:
                      <ol>
                        <li>Your use of the Website and the Dedicated Marketing Area will at all times comply with our <span style={{ fontWeight: "bold" }}>TERMS OF USE</span>, and these <span style={{ fontWeight: "bold" }}>TERMS OF SERVICE</span>;</li>
                        <li>You shall provide proper Services that comply with the letter and spirit of the terms of the respective requests of the End-Users;</li>
                        <li>You shall provide the registered services and receive payments in a legal manner;</li>
                        <li>You shall safeguard your account and supervise the use of your account, and you shall be responsible for your own use and the use of your account and Dedicated Marketing Area by anyone who has access to your account or your Dedicated Marketing Area;</li>
                        <li>You accept our Privacy and Personal Data Protection Act (“the PDPA”) Policy (our “<span style={{ fontWeight: "bold" }}>PRIVACY POLICY</span>”) with respect to the handling of personal data of End-Users transmitted to you, and agree that the Company is hereby authorized by you to collect, use and/or disclose any personal data (as defined in the PDPA) concerning you which may be provided by you in accordance with the purposes set out in these <span style={{ fontWeight: "bold" }}>TERMS OF USE</span>, these <span style={{ fontWeight: "bold" }}>TERMS OF SERVICE</span>, and in our <span style={{ fontWeight: "bold" }}>PRIVACY POLICY</span> (as may be updated and posted on this Website from time to time), and consent to such collection, use and/or disclosure;</li>
                        <li>You acknowledge and accept that there may be interruptions in service or events that are beyond our control. While we use reasonable efforts to keep the Website accessible, the Website may be unavailable from time to time for any reason including, without limitation, routine maintenance or upgrading. You understand and acknowledge that due to circumstances both within and outside of our control, access to the Website may be interrupted, suspended or terminated at any time, without prior notice to you. We retain the right, at our sole discretion, to deny service or access to the Website to anyone or any account, at any time and for any reason as we may in our discretion deem to be appropriate.</li>
                        <li>A Service Provider may not assign or sub-contract any of his or her obligations or rights to another.</li>
                      </ol>
                    </li>
                    <li>We reserve complete and sole discretion with respect to the operation of Website. This means that we may, amongst other things:-
                <ol>
                        <li>Delete any email or private message if it has not been accessed by a Service Provider within the time established by our policies in force from time to time;</li>
                        <li>Make available to third parties information relating to the Service Provider;</li>
                        <li>Withdraw, suspend or discontinue any functionality or feature of the Website; and</li>
                        <li>Review forums, user comments, chats, or videos, posted on the Dedicated Marketing Area and impose restrictions or prohibitions for access thereto if we reasonably finds that it violates or infringes our <span style={{ fontWeight: "bold" }}>TERMS OF USE</span>, these <span style={{ fontWeight: "bold" }}>TERMS OF SERVICE</span> or our <span style={{ fontWeight: "bold" }}>PRIVACY POLICY</span>.</li>
                      </ol>
                    </li>

                    <li>All Service Providers accept that all interactions on the Website with End-Users must be lawful and must also comply with these <span style={{ fontWeight: "bold" }}>TERMS OF SERVICE</span>. To the extent your conduct (as judged by us in our sole discretion), restricts or inhibits any other user from using or enjoying any part of the Website, we may limit your privileges on, or access to, the Website and/or seek other remedies which may be available to us. Service Providers are not permitted to engage in the following activities as they are prohibited on the Website and constitute express violations of these <span style={{ fontWeight: "bold" }}>TERMS OF SERVICE</span>:
                    <ol>
                        <li>Submitting any purposely inaccurate information, committing fraud or falsifying information in connection with your account or in order to create multiple accounts;</li>
                        <li>Attempting to, or actually accessing data not intended for you, such as logging into a server, module or an account which you are not authorised to access or which is not meant for you to access;</li>
                        <li>Attempting to scan, or test the security or configuration of the Website or to breach security or authentication measures without proper authorisation;</li>
                        <li>Tampering or interfering with the proper functioning of any part, page or area of the Website and any and all functions and services provided by the Company;</li>
                        <li>Attempting to interfere with service to any user in any manner, including, without limitation, by means of submitting a virus to the Website, or attempts at overloading, “flooding”, “spamming”, “mail bombing” or “crashing” the Website;</li>
                        <li>Disseminating or transmitting material that is unlawful, harmful, threatening, abusive, harassing, defamatory, vulgar, obscene, profane, hateful, racially or ethnically discriminating or otherwise objectionable nature including, but not limited to, any material which encourages conduct that would constitute a criminal offence, violate the rights of others, or otherwise violate any applicable local, state, national or international law or which impersonate any person or entity or to misrepresent your affiliation or association with any person or entity;</li>
                        <li>Disseminating, storing or transmitting files, graphics, software or other material that actually or potentially infringes the copyright, trade mark, patent, trade secret or other intellectual property right of any person;</li>
                        <li>Using the Website or any of its contents to advertise or solicit, for any other commercial, social, political or religious purpose, or to compete, directly or indirectly with the Company;</li>
                        <li>Reselling or repurposing your access to the Website or any purchases made through the Website;</li>
                        <li>Using the Website or any of its resources to solicit professional service providers, End-Users or other business partners of the Company to become users or partners of other online or offline services directly or indirectly competitive or potentially competitive with the Company, including without limitation, aggregating current or previously offered service offerings;</li>
                        <li>Using another Service Provider or End-User information from the Website for any commercial purpose, including, but not limited to, marketing;</li>
                        <li>Submitting false or fraudulent claims under any Guarantee and/or Warranty;</li>
                        <li>Accessing, monitoring or copying any content or information from the Website using any robot, spider, scraper or other automated means or any manual process for any purpose without our express written permission;</li>
                        <li>Violating the restrictions in any robot exclusion headers on the Website or bypassing or circumventing other measures employed to prevent or limit access to the Website;</li>
                        <li>Taking any action that places excessive demand on our services, or imposes, or may impose an unreasonable or disproportionately large load on our servers or other portion of our infrastructure (as determined in our sole discretion);</li>
                        <li>Aggregating any live or post-feature content or other information from the Website (whether using links or other technical means or physical records associated with purchases made through the Website) with material from other sites or on a secondary site without our express written permission.</li>
                        <li>Deep-linking to any portion of the Website without our express written permission;</li>
                        <li>Acting illegally or maliciously against the business interests or reputation of the Company, any Service Provider, any End-User, any Services or the services provided through the Website (including posting reviews which are, in our sole discretion, defamatory, purposefully malicious, frivolous and/or baseless);</li>
                        <li>Hyperlinking to the Website from any other Website without our initial and ongoing consent; or</li>
                        <li>Engaging in any other activity deemed by us to be in conflict with the spirit or intent of these <span style={{ fontWeight: "bold" }}>TERMS OF SERVICE</span>.</li>
                      </ol>

                      <li>You shall be responsible for, and indemnify and hold harmless us, our affiliates, agents and representatives against all damages, costs and expenses including any claim from any third parties and legal costs on full solicitor and client basis that arises from your breach of any of these <span style={{ fontWeight: "bold" }}>TERMS OF SERVICE</span> governing your conduct on the Website.</li>
                    </li>
                  </ol>


                  <div style={{ margin: '100px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}><u>PROVISION OF CYCLIC INSURANCE SERVICES</u></div>

                  <div style={{ margin: '100px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}>Purpose & Role of the Company  </div>
                  <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                    <ol>
                      <li>Apart from providing Directory Services, this Website serves as an online platform for Web Users to join in the Peer to Peer [“P2P”] insurance services whereby Web Users provide insurance for each other.</li>
                      <li>The Company as the Administrator of the P2P Cyclic insurance services promoted through this Website, acts for and on behalf of all participants in the P2P cyclic insurance services and has the power to implement all measures, to make required decisions and to take all necessary or reasonable steps in its absolute discretion for the proper administration and enforcement of all insurance services promoted on this Website including but not limited to:-</li>
                      <ol>
                        <li type="a">Processing applications for signing up for P2P cyclic insurance services; </li>
                        <li type="a">Collection of premiums and administration fees; </li>
                        <li type="a">Verification of claims; </li>
                        <li type="a">Refund of PremiumBack sums;  </li>
                        <li type="a">Processing and Payout of claims;</li>
                        <li type="a">Cancellation of policies & Processing of Cancellation Refunds; and </li>
                        <li type="a">Removal of any participant who displays any form of behavior detrimental to the      Company, Cyclic insurance and it's participants.</li>
                      </ol>
                    </ol>
                  </div>


                  <div style={{ margin: '100px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}>Duty of Participants in Cyclic insurance</div>
                  <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                    <ol>
                      <li>The services of the Company as the Administrator of the P2P Cyclic insurance services is provided on the condition and subject to the concerned participant:- </li>
                      <ol>
                        <li type="a">Ensuring that all details, information and documents submitted to the Company through online forms or at the request of the Company are true and accurate;</li>
                        <li type="a"> Providing updated information and documents to the Company; and </li>
                        <li type="a"> Fully and faithfully disclosing to the Company all information that such concerned participant knows or ought reasonably be expected to know </li>
                      </ol>
                      otherwise the Company as the Administrator of the P2P Cyclic insurance service, may not approve your application, may reject any claims made by you, or may discontinue our services for you without refund.
                    </ol>
                  </div>


                  <div style={{ margin: '100px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}>Payments</div>
                  <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                    <ol>
                      <li>Participants in P2P Cyclic insurance shall make payment of the requisite premiums and Administrative Fees to the Company.</li>
                      <li>The  Company shall refund payment of unused premiums in the premiums pool, by way of PremiumBack subject to such terms and conditions set out in this Website.</li>
                    </ol>
                  </div>


                  <div style={{ margin: '100px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}>Claims Process & Payout</div>
                  <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                    <ol>
                      <li> Claims are to be submitted through the online form provided on this Website and with the required supporting documents.</li>
                      <li> All claims filed are subject to verification of the authenticity and legitimacy thereof by the Company, and the Company reserves the right to request for additional information / documents or updated information / documents as part of such verification process.</li>
                      <li> Approval of claims is strictly within the absolute discretion of the Company. Amongst others, the Company is entitled not to approve a claim in the event that there is any incorrect statement, inaccurate declaration, or withholding of material information at the time of signing up for P2P insurance or anytime during the filing of a claim, including but not limited to misrepresentations, failure to disclose any material facts or any fraud involved, or where the incident giving rise to a claim is due to causes not covered by the policy including but not limited to a pre-existing medical condition which was known or reasonably ought to have been known, deliberate or intentional acts of self-inflicted injury, or as a result of participation in a criminal act such as a riot or a fight.</li>
                      <li> Payment of a claim is made directly by way of transfer to the Singapore bank account of the claimant.</li>
                    </ol>
                  </div>


                  <div style={{ margin: '100px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}>Cancellation</div>
                  <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                    <ol>
                      <li>Cancellation of Cyclic insurance services / policies may be made at any time by notice to the Company including email notice, and Cancellation Refund where appropriate will be computed and refunded by direct transfer to the Singapore bank account of the P2P insurance participant, subject to such terms and conditions as set out in this Website.</li>
                    </ol>
                  </div>


                </div>

              </div>

            </div>
          </div>


        </NoSsr>
        <BotNavBar referrer={this.state.referrer} />

      </div>
    );
  }
}


export default withStyles(styles)(TermsOfService);