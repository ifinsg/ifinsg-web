import React, { Component } from 'react';

import { NoSsr, Button, Typography, IconButton, AppBar, Toolbar } from '@material-ui/core';

// import Logo from './ifinsg_logo.png'
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo

import TopNavBar from './components/TopNavBar'
import BotNavBar from './components/BotNavBar'
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Constants from 'constants/constants';
import EmailJoanna from './components/constant_components/EmailJoanna';
import Head from 'next/head';


const styles = {
  page_width: Constants.style.page.width,
  page_text_width: Constants.style.page.text_width,

  page_padding: Constants.style.page.padding,

  align_center: Constants.style.align.center,
  align_left: Constants.style.align.left,

  bold: Constants.style.text.bold,
  break_word: Constants.style.text.break_word,

  title1: Constants.style.text.title1,
  body1: Constants.style.text.body1,

  margin_top_xl: Constants.style.margin_top.xl,
  margin_top_lg: Constants.style.margin_top.lg,
  margin_top_md: Constants.style.margin_top.md,
  margin_top_sm: Constants.style.margin_top.sm,
  margin_top_xs: Constants.style.margin_top.xs,
  margin_top_xxs: Constants.style.margin_top.xxs,
}

class TermsOfUse extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }


  componentDidMount = () => {
    Constants.store_referrer(this)
  }

  render() {
    const { classes } = this.props;

    return (
      <div>
        <TopNavBar referrer={this.state.referrer}/>

        <NextSeo config={Constants.next_seo_config("terms-of-use")} />

        <NoSsr>



          <div className={classNames(classes.align_center)}>
            <div className={classNames(classes.page_width, classes.page_padding, classes.break_word, classes.align_left)}>

              <div style={{ padding: '50px 0px 0px 0px', textAlign: "center" }}>
                <img style={{ maxWidth: '200px', margin: '40px 0px 0px 0px', padding: '0px 0px 0px 0px' }} src={('/img/iFin-SG-Logo.png')} />
                <div style={{ fontSize: '50px', lineHeight: '1', margin: '30px 0px 0px 0px', color: "#4cae4f", fontWeight: "bold" }}>TERMS OF USE</div>
              </div>


              <div style={{ padding: '0px 0px 50px 0px' }}>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>The following are the terms and conditions for the use of this Website. All access to and use of this Website are governed by our <span style={{fontWeight:"bold"}}>GENERAL TERMS AND CONDITIONS</span>, these <span style={{fontWeight:"bold"}}>TERMS OF USE</span>, our <span style={{fontWeight:"bold"}}>TERMS OF SERVICE</span>, <span style={{fontWeight:"bold"}}>DISCLAIMER OF LIABILITY</span> and <span style={{fontWeight:"bold"}}>PRIVACY POLICY</span>. By using this Website or registering with us, you indicate that you have read our <span style={{fontWeight:"bold"}}>GENERAL TERMS AND CONDITIONS</span>, these <span style={{fontWeight:"bold"}}>TERMS OF USE</span>, our <span style={{fontWeight:"bold"}}>TERMS OF SERVICE</span>, <span style={{fontWeight:"bold"}}>DISCLAIMER OF LIABILITY</span> and <span style={{fontWeight:"bold"}}>PRIVACY POLICY</span> and have agreed to be bound by them.</div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ul>
                    <li style={{listStyle:"none"}}>As used in this Agreement, the terms “you” means you, as well as all users of the our Services through your account, and you represent and warrant that you have authority to enter into this Web User Agreement for yourself and other users including any firm, company or organization that you are acting on behalf of, as if such user(s) were the legal persons referred to as “you” herein.</li>
                  </ul>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Eligibility </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>Parts of this Website may be available to general online users without the need for any registration as a user. To use those parts of this Website as are only available to members however, you must apply for and be accepted by the Company for registration as a Member [“Member”] with a unique username approved by the Website, be a holder of a Singapore NRIC or FIN card, be at least 21 years of age, must not be an undischarged bankrupt, and be able to form or enter into a legally binding contract in accordance with applicable law. If you are acting on behalf of a firm, a company or organisation, your firm or company or organisation must be duly constituted and registered with the competent authorities of Singapore, must not be an insolvent entity and you must be fully authorized to apply for membership and to use this Website by your firm, company or organisation.</li>
                    <li>You are responsible for all access to all products and services available on or through this using your Internet connection or your User ID and/or Login ID even if the access or use is by another person without your authority or consent.</li>
                    <li>In the event that you are acquiring Third Party products and/or services through this Website, you shall abide by the terms and conditions set out by each such Third Party in addition to the terms and conditions in this Agreement.</li>
                    <li>All information provided to the Website for member registration or other purposes must be true, accurate and valid. If any information you provide is false, we may suspend or terminate your registration and account and also take legal action if our company or any party suffers any loss or detriment owing to a breach of this term.</li>
                    <li>You may not access the services if you are a direct competitor of Company, except with the Company’s prior written consent. In addition, you may not access the Website for purposes of monitoring its availability, performance or functionality, or for any other benchmarking or competitive purposes.</li>
                    <li>Payment for any chargeable products or services on this Website can only be made by our approved payment mechanism as published on this Website. You will be responsible for the payment of all online charges and for the communication facilities which enable access to our products and services including any applicable goods and services tax, sales taxes or all other taxes.</li>
                  </ol>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> P2P Cyclic insurance </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>
                      If you are a participant in the P2P Cyclic insurance services offered through this Website, YOU ARE UNDER A DUTY OF UTMOST GOOD FAITH, AND IN ORDER TO BE COVERED UNDER THE P2P CYCLIC INSURANCE SERVICE, YOU MUST:
                      <ol>
                        <li type="a">Ensure that all details, information and documents submitted to the Company through online forms or at the request of the Company are true and accurate;</li>
                        <li type="a">Provide updated information and documents to the Company; and </li>
                        <li type="a">Fully and faithfully disclose to the Company, all information that you know or ought reasonably be expected to know.</li>
                      </ol>
                      otherwise the Company as the Administrator of the P2P Cyclic insurance service, may not approve your application, may reject any claims made by you, or may discontinue our services for you without refund.

                    </li>
                    <li>
                      The Company is entitled to assume that details, information and documents provided by you are true, accurate and updated, and that you have not withheld any relevant information which you know or ought reasonably be expected to know.
                    </li>
                    <li>
                      The Company may in its sole discretion without prior notice immediately terminate your account for any reason including but without limited to:
                      <ol>
                        <li type="a">Breaches or violations of the Terms of Use, Terms of Service, Disclaimer of Liability, any statutory or legal provisions or regulations, or any rules and regulations which the Company has created for the P2P Cyclic insurance service;</li>
                        <li type="a">Upon the direction or request of any Competent Authorities; </li>
                        <li type="a">Infringement of intellectual property rights of others;</li>
                        <li type="a">Discontinuance or material modifications to the services provided on this online platform or part thereof;</li>
                        <li type="a">Unexpected technical or security issues or problems;</li>
                        <li type="a">Any form of behavior detrimental to the Company, Cyclic insurance and it's participants.</li>
                      </ol>
                    </li>
                    <li>
                      The decision of the Company to terminate any user’s accounts shall be absolutely final and conclusive.
                    </li>
                    <li>
                      Termination of your account shall result in:
                    <ol>
                        <li type="a">Removal of access and further use to all services on this online platform that are associated with the account; and</li>
                        <li type="a">Deletion of your password and all related information, files and content associated with or inside your account (or part thereof).</li>
                      </ol>
                    </li>
                    <li>
                      Termination of your account shall be without prejudice to any other rights or remedies of the Company and shall not affect any accrued rights or liabilities nor the coming into or continuance in force of any provision which is expressly or by implication survive such termination or is intended to come into effect after such termination.
                    </li>
                  </ol>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Posting of Materials </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>As a user, you agree not to post or transmit or upload any material which is of an unlawful, harmful, threatening, abusive, harassing, defamatory, vulgar, obscene, profane, hateful, racially or ethnically discriminating or otherwise objectionable nature including, but not limited to, any material which encourages conduct that would constitute a criminal offence, violate the rights of others, or otherwise violate any applicable local, state, national or international law or the Intellectual Property Rights of another.</li>
                    <li>You agree you will not impersonate any person or entity or to misrepresent your affiliation or association with any person or entity. By submitting material to or through this Website, you are representing that you are the owner of the material, or have a right to reproduce, display or transmit such material or are making your submission with the express consent of the owner and that there is no infringement of any Intellectual Property Rights belonging to another.</li>
                    <li> No user is to post or transmit or upload any material which he knows to contain a virus or corrupted data.</li>
                    <li>The Company and its affiliates reserve the right, in its sole discretion, to delete, edit, refuse to post or remove any material submitted to or posted on this Website which is submitted or posted in violation of any part of this Agreement and to deny access to anyone who violates any part of this Agreement.</li>
                    <li>Except as stated in the Privacy Statement, any communication or material which any user posts or transmits on this Website, is, and will be treated as, non-confidential and non-proprietary. A user assumes full responsibility for anything posted or transmitted or uploaded, and grants to the Company the right to use the user’s name in connection with the posted material as well as in connection with any advertising, marketing or promotional material related thereto as well as the right to edit, copy, publish and distribute any information or content that such user posts or transmits or uploads for any purpose.</li>
                  </ol>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Intellectual Property Rights </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>ICH iFinancial Singapore Private Limited, UEN: 201631146G respects the Intellectual Property rights of others, and we require all users of this Website to do the same. If you believe that a copy of your work has been reproduced or made available on this Website without your consent or that a copyright, trade mark, design right or other intellectual property infringement has otherwise occurred, please supply us with the relevant information and email to: <EmailJoanna/></li>
                    <li>The contents displayed on or available on or through this Website including forums documentation and manuals, training material, blogs, audio-video content, information provided through webinar, emails, smart phone applications and other platforms and including but not limited to text, software, photographs, graphics, illustrations and artwork layout, color combinations and other graphical elements, video, music and sound, names, logos, trademarks and service marks including the “look” and “feel” of this Website are owned or licensed for use by the Company, its affiliates or its licensors and are protected by copyright, trademark and other intellectual property laws. In particular, the right to use of any material on this Website is subject to the prior written consent of the Company.</li>
                    <li>No part or parts of this Website may be reproduced, distributed, adapted, modified, republished, displayed, broadcast, hyperlinked, framed or transmitted in any manner or by any means or stored in an information retrieval system without the prior written permission of the Company provided that permission is granted to download and print the materials on this Website for strictly personal, non-commercial use only and provided that you do not modify any materials and that you retain all copyright and other proprietary notices contained in such materials. You may not, without the permission of the Company, insert a hyperlink to this Website on any other Website or platform or “mirror” any material contained on this Website on any other server.</li>
                  </ol>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Termination </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>The Company may in its sole discretion without prior notice immediately terminate your account for any reason including but without limited to:</li>
                    <ol>
                      <li type="a"> Breaches or violations of the Terms of Use, Terms of Service, Disclaimer of Liability, any statutory or legal provisions or regulations, or any rules and regulations which the Company has created for the P2P Cyclic insurance service;</li>
                      <li type="a"> Upon the direction or request of any Competent Authorities;</li>
                      <li type="a">Infringement of intellectual property rights of others; </li>
                      <li type="a"> Discontinuance or material modifications to the services provided on this online platform or part thereof;</li>
                      <li type="a"> Unexpected technical or security issues or problems;</li>
                      <li type="a"> Any form of behavior detrimental to the Company, Cyclic insurance and it's participants.</li>
                    </ol>
                    <li>The decision of the Company to terminate any user’s accounts shall be absolutely final and conclusive.</li>
                    <li>Termination of your account shall result in:</li>
                    <ol>
                      <li type="a">Removal of access and further use to all services on this online platform that are associated with the account; and </li>
                      <li type="a">Deletion of your password and all related information, files and content associated with or inside your account (or part thereof). </li>
                    </ol>
                    <li>Termination of your account shall be without prejudice to any other rights or remedies of the Company and shall not affect any accrued rights or liabilities nor the coming into or continuance in force of any provision which is expressly or by implication survive such termination or is intended to come into effect after such termination.</li>
                  </ol>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Disclaimer of Liability </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>All use of this Website and communications made through this Website are subject to our Disclaimer of Liability clauses. Please read DISCLAIMER OF LIABILITY which form part of this Agreement.</li>
                  </ol>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Privacy </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>Your use of this Website is subject to our Privacy Policy. Please read PRIVACY POLICY which form part of this Agreement</li>
                  </ol>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Force Majeure </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>The Company and its affiliates will not be liable for any delay in performing or failure to perform its obligations under this Agreement due to any causes outside its reasonable control. Such delay or failure will not constitute a breach of this Agreement and the time of performance of the affected obligation will be extended by such period as is reasonable</li>
                  </ol>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}> Dispute Resolution </div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>If any dispute arises out of or in connection with this agreement, including any question regarding its existence, validity or termination, the parties agree to endeavour to settle the dispute through conciliation at the Singapore Mediation Centre (“the SMC”) in accordance with the Rules of Mediation and Conciliation of the SMC for the time being in force, which rules are deemed to be incorporated herein by reference.</li>
                    <li>Any issue or claim not resolved by conciliation shall be referred to and finally resolved by arbitration in Singapore at the Singapore International Arbitration Centre (“the SIAC”) in accordance with the SIAC Rules for the time being in force, which rules are deemed to be incorporated by reference in this Clause. In the event of arbitration, the arbitration tribunal shall consist of one (1) arbitrator to be appointed by the Chairman of the SIAC and the language of arbitration shall be in English. The Arbitrator’s decision shall be final and binding.</li>
                  </ol>
                </div>


                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8', fontWeight: "bold" }}>Miscellaneous</div>
                <div style={{ margin: '40px 0px 0px 0px', fontSize: '20px', lineHeight: '1.8' }}>
                  <ol>
                    <li>If any part of this Web User Agreement shall be found to be unlawful it shall not affect the validity or enforceability of the remainder of this Agreement.</li>
                    <li>No person other than a party to this Agreement may enforce this Agreement and the provisions of the Contracts (Rights of Third Parties) Act Cap. 53B of Singapore are hereby expressly excluded.</li>
                    <li>All transactions within the Website are and shall be deemed to have been made in Singapore and shall in all respects be governed by the laws of Singapore and the parties hereto hereby submit to the non-exclusive jurisdiction of the Courts of the Republic of Singapore.</li>
                  </ol>
                </div>


              </div>

            </div>


          </div>

        </NoSsr>
        <BotNavBar referrer={this.state.referrer}/>

      </div>
    );
  }
}


export default withStyles(styles)(TermsOfUse);