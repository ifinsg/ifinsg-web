import React, { Component } from 'react';

import { NoSsr, Button, Typography, IconButton, AppBar, Toolbar } from '@material-ui/core';

// import Logo from './ifinsg_logo.png'
import NextSeo from 'next-seo'; //Source: https://www.npmjs.com/package/next-seo
import { BrowserView, MobileView, isBrowser, isMobile } from 'react-device-detect';

import TopNavBar from './components/TopNavBar';
import BotNavBar from './components/BotNavBar';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import GlobalHelpers from '../global/helpers';
import Constants from 'constants/constants';
import MUIDataTable from 'mui-datatables'; // Source: https://github.com/gregnb/mui-datatables
import Table from './tables/Table';
import ComponentLogin from './components/ComponentLogin';

const styles = {
    page_width: Constants.style.page.width,
    page_text_width: Constants.style.page.text_width,

    page_padding: Constants.style.page.padding,

    align_center: Constants.style.align.center,
    align_left: Constants.style.align.left,

    bold: Constants.style.text.bold,
    break_word: Constants.style.text.break_word,

    title1: Constants.style.text.title1,
    body1: Constants.style.text.body1,

    margin_top_xl: Constants.style.margin_top.xl,
    margin_top_lg: Constants.style.margin_top.lg,
    margin_top_md: Constants.style.margin_top.md,
    margin_top_sm: Constants.style.margin_top.sm,
    margin_top_xs: Constants.style.margin_top.xs,
    margin_top_xxs: Constants.style.margin_top.xxs
};

const allow_access_to = [
    'AFFILIATESMQ@EMAIL.COM',
    'MARQSIEW@IFINANCIALSINGAPORE.COM',
    'JIEXIONGKHOO@GMAIL.COM',
    'W1BBOND@GMAIL.COM',
    'WANGYIBU123@GMAIL.COM',
    'JOANNASEEXIAOLIN@GMAIL.COM',
    'YIWIGO2039@LANCASTERCOC.COM'
];

class Affiliates extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date_start: this.getDateStart(0),
            offset: 0,
            mode: 'daily'
        };
    }

    getDateStart = (offset) => {
        // offset in days from the start of today
        console.log('getDateStart()');
        let date_object = new Date();
        if (this.state && offset) {
            date_object = new Date(new Date().getTime() + offset * 24 * 60 * 60 * 1000);
            // console.log(date_object)
        }
        return GlobalHelpers.dateStringFormattedyyyyMMdd(date_object);
    };

    componentDidMount = () => {
        Constants.store_referrer(this);
    };

    chooseMode = () => {
        // if (this.state.mode === "daily") {
        if (false) {
            return (
                <div style={{ textAlign: 'center', width: '100%' }}>
                    <div style={{ maxWidth: '400px', margin: 'auto' }}>
                        <Button
                            style={{ float: 'left' }}
                            onClick={() => {
                                let offset = this.state.offset - 1;
                                this.setState({
                                    offset,
                                    date_start: this.getDateStart(offset)
                                });
                            }}
                        >
                            {' '}
                            PREV{' '}
                        </Button>

                        <Button
                            style={{ float: 'right' }}
                            onClick={() => {
                                let offset = this.state.offset + 1;
                                this.setState({
                                    offset,
                                    date_start: this.getDateStart(offset)
                                });
                            }}
                        >
                            {' '}
                            NEXT{' '}
                        </Button>

                        <div style={{ paddingTop: '5px', fontSize: '20px' }}>
                            <Typography variant="subtitle1" style={{ fontWeight: 'bold' }}>
                                {' '}
                                {GlobalHelpers.displayDate(this.state.date_start)}
                            </Typography>
                        </div>
                    </div>

                    <div
                        style={{
                            width: '100%',
                            display: 'inline-block',
                            textAlign: 'left'
                        }}
                    >
                        <Table table="affiliates" date_start={this.state.date_start} />
                    </div>
                </div>
            );
        } else {
            // Mode === "overall"
            return (
                <div>
                    <div
                        style={{
                            width: '100%',
                            display: 'inline-block',
                            textAlign: 'left'
                        }}
                    >
                        <Table table="tracking" />
                    </div>
                </div>
            );
        }
    };

    renderTableAffiliates = () => {
        if (this.state.user_info && allow_access_to.indexOf(this.state.user_info.username_norm) !== -1) {
            return (
                <div>
                    {/* <div style={{ width: "100%", textAlign: "center", marginBottom: "10px" }}>
            <Button style={{ fontSize: "20px" }} onClick={() => {
              if (this.state.mode === "daily") {
                this.setState({ mode: "all" })
              } else {
                this.setState({ mode: "daily" })
              }
            }}> <span style={{ lineHeight: 0.8 }}> {this.state.mode === "daily" ? "Daily mode" : "Overall mode"}   <br />  <span style={{ fontSize: "11px" }}>(click to switch)</span> </span></Button>
          </div> */}

                    {this.chooseMode()}
                </div>
            );
        } else {
            return (
                <div style={{ textAlign: 'center', marginTop: '45px' }}>
                    {this.state.user_info ? (
                        <Typography> You do not have rights access to this page </Typography>
                    ) : (
                        <div
                            style={{
                                textAlign: 'left',
                                padding: '0px 20px 0px 20px',
                                minWidth: '350px'
                            }}
                        >
                            <Typography variant="h6"> Login </Typography>
                            <ComponentLogin
                                referrer={this.state.referrer}
                                onLogin={() => {
                                    location.reload();
                                }}
                            />
                        </div>
                    )}
                </div>
            );
        }
    };

    getUserInfo = (user_info) => {
        this.setState(
            {
                user_info
            },
            () => {
                console.log(this.state.user_info);
            }
        );
    };

    render() {
        const { classes } = this.props;

        return (
            <div>
                <TopNavBar referrer={this.state.referrer} getUserInfo={this.getUserInfo} />

                <NoSsr>
                    <BrowserView>
                        <div className={classNames(classes.align_center)}>
                            <div
                                className={classNames(
                                    classes.page_width,
                                    classes.page_padding,
                                    classes.break_word,
                                    classes.align_left
                                )}
                            >
                                <div style={{ marginTop: '125px' }}> </div>
                                {this.renderTableAffiliates()}
                            </div>
                        </div>
                    </BrowserView>

                    <MobileView>
                        <div style={{ paddingTop: '80px' }}> </div>
                        {this.renderTableAffiliates()}
                    </MobileView>
                </NoSsr>
                <BotNavBar referrer={this.state.referrer} />
            </div>
        );
    }
}

export default withStyles(styles)(Affiliates);
