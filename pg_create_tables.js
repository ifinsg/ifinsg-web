const { db } = require("./server/helpers/pg");

// knex.schema.createTable('tracking', function (table) {
//   table.increments('id')
//   table.timestamps(true, true)

//   table.specificType('data', 'jsonb')

// }).catch(err => { console.log(err); console.log("SUCCESS") }).then(result => {
//   console.log(result)
//   console.log("SUCCESS")
// })

// knex.schema.createTable('users', function (table) {
//   table.increments('id')
//   table.timestamps(true,true)

//   table.string('username')
//   table.string('username_norm')
//   table.unique('username_norm');

//    // Electric fence
//   table.string('nric_number')
//   table.string('nric_number_norm')

//    // 4/4 rule
//   table.string('full_name')
//   table.string('full_name_norm')
//   table.string('gender')
//   table.string('dob')
//   table.string('dob_norm')
//   table.string('place_of_birth')
//   table.string('place_of_birth_norm')

//    // Other details
//   table.string('mobile_number')
//   table.string('occupation')
//   table.boolean('self_employed')
//   table.string('preferred_name')
//   table.specificType('uploaded_images', 'jsonb')

//    // Finance
//   table.string('bank')
//   table.string('bank_acct_number')
//   table.json('stripe_details')
//   table.string('stripe_customer_id')

//    // Others
//   table.string('referrer')
//   table.string('email_verification_code')
//   table.boolean('email_verified')
//   table.string('forget_password_verification_code')
//   table.string('password_hash')

//   // Operations
//   table.boolean('payer_details_verified')
//   table.specificType('pending_rework_items', 'jsonb')
//   table.text('pending_rework_message','longtext')  // not tested

//   // PMD
//   table.string('pmd_brand')
//   table.string('pmd_model')
//   table.string('pmd_color')
//   table.string('pmd_serial_number')
//   table.string('pmd_lta_number')
//   table.string('pmd_ul_number')

// }).catch(err => { console.log(err); console.log("SUCCESS") }).then(result => {
//   console.log(result)
//   console.log("SUCCESS")
// })

db.schema
    .createTable("policies", function(table) {
        table.increments("id");
        table.timestamps(true, true);

        table.string("ref_username_norm").references("users.username_norm");

        // Electric fence
        table.string("nric_number");
        table.string("nric_number_norm");

        // 4/4 rule
        table.string("full_name");
        table.string("full_name_norm");
        table.string("gender");
        table.string("dob");
        table.string("dob_norm");
        table.string("place_of_birth");
        table.string("place_of_birth_norm");

        // Policy details
        table.string("product");
        table.json("product_details");
        table.string("relationship_to_payer"); // Family
        table.string("entity_name"); // Entity
        table.string("entity_uen"); // Entity

        table.string("representing");
        table.string("status");
        table.specificType("status_history", "jsonb");
        table.string("referrer");
        table.string("source");

        // Other details
        table.string("email");
        table.string("mobile_number");
        table.string("occupation");
        table.string("preferred_name");
        table.boolean("self_employed");
        table.specificType("uploaded_images", "jsonb");
        table.string("uploaded_image_signup_screenshot");

        // Finance
        table.string("bank");
        table.string("bank_acct_number");
        table.string("bank_claims");
        table.string("bank_acct_number_claims");
        table.string("bank_premiumback");
        table.string("bank_acct_number_premiumback");
        table.string("stripe_subscription_id");

        // Operations
        table.string("flag_staff_inbox_category");
        table.specificType("pending_rework_items", "jsonb");
        table.text("pending_rework_message", "longtext"); // not tested
        table.boolean("policy_details_verified");
    })
    .catch((err) => {
        console.log(err);
        console.log("SUCCESS");
    })
    .then((result) => {
        console.log(result);
        console.log("SUCCESS");
    });

// knex.schema.createTable('accidents', function (table) {
//   table.increments('id')
//   table.timestamps(true, true)

//   table.string('ref_username_norm').references('users.username_norm')
//   table.integer('ref_policy_id').unsigned().references('policies.id');

//   table.string('product')
//   table.string('accident_type')
//   table.string('accident_description')
//   table.timestamp('accident_date')

//   table.specificType('pending_rework_items', 'jsonb')
//   table.text('pending_rework_message', 'longtext')  // not tested
//   table.boolean('accident_details_verified')

// }).catch(err => { console.log(err); console.log("SUCCESS") }).then(result => {
//   console.log(result)
//   console.log("SUCCESS")
// })

// knex.schema.createTable('claims', function (table) {
//   table.increments('id')
//   table.timestamps(true, true)

//   table.string('ref_username_norm').references('users.username_norm');
//   table.integer('ref_policy_id').unsigned().references('policies.id');
//   table.integer('ref_accident_id').unsigned().references('accidents.id');

//   table.string('status')
//   table.specificType('status_history', 'jsonb')

//   table.boolean('medical_registrar_smc')
//   table.timestamp('clinic_visit_date')
//   table.string('doctor_seen')
//   table.string('clinic_visited')
//   table.string('treatment_description')
//   table.specificType('uploaded_images', 'jsonb')
//   table.string('addon_notes')
//   table.specificType('pending_rework_items', 'jsonb')
//   table.text('pending_rework_message', 'longtext')  // not tested

//   table.float('payout')

// }).catch(err => { console.log(err); console.log("SUCCESS") }).then(result => {
//   console.log(result)
//   console.log("SUCCESS")
// })

// knex.schema.createTable('events', function (table) {
//   table.increments('id')
//   table.timestamps(true, true)

//   table.string('ref_username_norm').references('users.username_norm');
//   table.integer('ref_policy_id').unsigned().references('policies.id');
//   table.integer('ref_accident_id').unsigned().references('accidents.id');
//   table.integer('ref_claim_id').unsigned().references('claims.id');

//   table.string('type')

//   table.json('data')

// }).catch(err => { console.log(err); console.log("SUCCESS") }).then(result => {
//   console.log(result)
//   console.log("SUCCESS")
// })

// knex.schema.createTable('raw_data', function (table) {
//   table.increments('id')
//   table.timestamps(true,true)

//   table.string('ref_username_norm').references('users.username_norm');
//   table.integer('ref_policy_id').unsigned().references('policies.id');
//   table.string('type')

//   table.json('data')

// }).catch(err => { console.log(err); console.log("SUCCESS") }).then(result => {
//   console.log(result)
//   console.log("SUCCESS")
// })

//////////////////// Not created yet (need to convert from mongoose schema) ////////////////////
// const claimsSchema = mongoose.Schema({
//   sn: { type: String, required: true, default: new Date().getTime().toString() + parseInt(Math.random() * 10).toString() },
//   created_at: { type: Date, required: true, default: Date.now },
//   claim_result: Boolean,
//   date_claim_submit: Date,
// })
// const accidentSchema = mongoose.Schema({
//   sn: { type: String, required: true, default: new Date().getTime().toString() + parseInt(Math.random() * 10).toString() },
//   created_at: { type: Date, required: true, default: Date.now },
//   accident: String,
//   date_accident: Date,
//   claims: [claimsSchema]
// })
