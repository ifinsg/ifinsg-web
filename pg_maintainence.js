const global_helpers = require("./global/helpers");

// const database = "ifinsgdev2_live";
// const database = 'ifinsgdev2_live'
// if (process.env.DB_NAME !== "ifinsgdev2_staging") {
//   throw new Error(
//     "Hi there, database is in LIVE mode. Turn this off to continue..."
//   );
// }
const { db } = require("./server/helpers/pg");
var input = JSON.parse('{ "path": "/foo/bar bam/baz.html" }');

const delete_user = (username) => {
    let username_norm = global_helpers.normalise_string(username);
    console.log(
        "ALERT!!!! Deleting an admin will cause all normal user's events linked to that admin to be deleted also. Please be careful."
    );
    db.transaction(function(trx) {
        return trx("events")
            .where({ ref_username_norm: username_norm })
            .del()
            .then(() => {
                return trx("claims")
                    .where({ ref_username_norm: username_norm })
                    .del();
            })
            .then(() => {
                return trx("accidents")
                    .where({ ref_username_norm: username_norm })
                    .del();
            })
            .then(() => {
                return trx("policies")
                    .where({ ref_username_norm: username_norm })
                    .del();
            })
            .then(() => {
                return trx("users")
                    .where({ username_norm: username_norm })
                    .del();
            });
    })
        .then((result) => {
            console.log(result);
        })
        .catch((err) => {
            console.log(err);
        });
};

const delete_policy = (policy_id) => {
    db.transaction(function(trx) {
        return trx("events")
            .where({ ref_policy_id: policy_id })
            .del()
            .then(() => {
                return trx("claims")
                    .where({ ref_policy_id: policy_id })
                    .del();
            })
            .then(() => {
                return trx("accidents")
                    .where({ ref_policy_id: policy_id })
                    .del();
            })
            .then(() => {
                return trx("policies")
                    .where({ id: policy_id })
                    .del();
            });
    })
        .then((result) => {
            console.log(result);
        })
        .catch((err) => {
            console.log(err);
        });
};

const check_db_connections = () => {
    db.raw(`SELECT  sum(numbackends) AS totalnum FROM pg_stat_database;`).then((res) => {
        console.log(res.rows);
    });
    // db.select('*').from('users').then(res => { console.log(res) })
};

// check_db_connections()
// delete_policy(97)
// delete_policy(103)
// delete_policy(104)

// delete_policy(105)

// delete_policy(31)

// delete_policy(78)

// delete_policy(59)
// delete_policy(58)

// delete_policy(57)
// delete_policy(56)
// delete_policy(55)

// delete_user("mmmmmm6mmmmmm@gmail.com".toUpperCase())
