window.onload = function () {

    // Source: https://stephenradford.me/link-to-url-scheme-or-not-and-force-out-of-the-app-youre-in/

    alert("prevent-web-view")

    var desktopFallback = "https://www.ifinsg.com/1",
    mobileFallback = "https://www.ifinsg.com/2",
        app = "https://www.ifinsg.com/3";
    if (/Android|iPhone|iPad|iPod/i.test(navigator.userAgent)) {
        window.location = app;
        window.setTimeout(function () {
            window.location = mobileFallback;
        }, 25);
    } else {
        window.location = desktopFallback;
    }
    function killPopup() {
        window.removeEventListener('pagehide', killPopup);
    }
    window.addEventListener('pagehide', killPopup);
};