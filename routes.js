const Router = require('nextjs-dynamic-routes')
const global_constants = require('./global/constants')
const router = new Router()

// router.add({ name: 'FAQ', pattern: '/FAQ/:referrer' })

// router.add({ name: 'Disclaimers', pattern: '/Disclaimers/:id' })

// router.add({
//   name: "Home",
//   pattern: '/:referrer',
//   page: '/'
// })

// router.add({ name: '/', pattern: '/:referrer' })


for (let i = 0; i < global_constants.list_of_pages.length; i++) {
  let page = global_constants.list_of_pages[i]
  
  router.add({
    name: page,
    pattern: '/' + page + '/:referrer',
    page: '/' + page
  })

  router.add({
    name: page,
    pattern: '/' + page + '/',
    page: '/' + page
  })
  // router.add({ name: 'page', pattern: '/' + page + '/:referrer' })
  // router.add(page, '/' + page + '/:referrer')     // https://stackoverflow.com/questions/52105699/nested-routes-in-next-js

}




module.exports = router