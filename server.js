const express = require('express');
const sm = require('sitemap');

const next = require('next');
var bodyParser = require('body-parser'); //https://stackoverflow.com/questions/5710358/how-to-retrieve-post-query-parameters
var favicon = require('serve-favicon');
var path = require('path');

const dev = process.env.NODE_ENV === 'development';

if (process.env.NODE_ENV === 'development') {
    require('dotenv').config({ path: path.join(__dirname, '/.env.development') });
} else {
    require('dotenv').config({ path: path.join(__dirname, '/.env.production') });
}

console.log('process.env.NODE_ENV : ', process.env.NODE_ENV);

const Router = require('./routes'); // https://www.npmjs.com/package/nextjs-dynamic-routes

const app = next({ dev });

const handle = Router.getRequestHandler(app);

// const handle = app.getRequestHandler()
var iframeReplacement = require('node-iframe-replacement');
var exphbs = require('express-handlebars');

const global_constants = require('./global/constants');

const mode_server_only = false;

const select_mode = () => {
    return new Promise((resolve, reject) => {
        if (!mode_server_only) {
            app.prepare()
                .then(() => {
                    resolve();
                })
                .catch((err) => {
                    reject(err);
                });
        } else {
            resolve();
        }
    });
};

/////////////////// Creating sitemap ///////////////////
select_mode()
    .then(() => {
        const server = express();
        const sitemap = sm.createSitemap({
            hostname: 'https://www.ifinsg.com',
            cacheTime: 600000, // 600 sec - cache purge period
            urls: [
                { url: '/faq', changefreq: 'monthly', priority: 0.9 },
                { url: '/disclaimers', changefreq: 'monthly', priority: 0.8 },
                { url: '/claims', changefreq: 'monthly', priority: 0.7 },
                { url: '/premium-back', changefreq: 'monthly', priority: 0.6 },
                { url: '/resources', changefreq: 'monthly', priority: 0.5 },
                { url: '/products', changefreq: 'monthly', priority: 0.4 },
                { url: '/about-us', changefreq: 'monthly', priority: 0.6 }

                // { url: '/privacy-policy', changefreq: 'monthly', priority: 0.3 },
                // { url: '/disclaimer-of-liability', changefreq: 'monthly', priority: 0.3 },
                // { url: '/forgot-password', changefreq: 'monthly', priority: 0.2 },
                // { url: '/general-terms-and-conditions', changefreq: 'monthly', priority: 0.3 },
                // { url: '/page-not-found', changefreq: 'monthly', priority: 0.1 },
                // { url: '/profile', changefreq: 'monthly', priority: 0.3 },
                // { url: '/reset-password', changefreq: 'monthly', priority: 0.1 },
                // { url: '/signup', changefreq: 'monthly', priority: 0.3 },
                // { url: '/terms-of-service', changefreq: 'monthly', priority: 0.3 },
                // { url: '/terms-of-use', changefreq: 'monthly', priority: 0.3 },
            ]
        });

        // Force the use of https when in GAE flex env
        // server.use(function (req, res, next) {
        //   if (req.headers['x-forwarded-proto'] && req.headers['x-forwarded-proto'] === "http") {
        //     return res.redirect(['https://', req.get('Host'), req.url].join(''));
        //   }
        //   next();
        // });

        // Force the use of www instead of a naked url
        // server.use(require('express-naked-redirect')({
        //   subDomain: 'www'
        // }))

        server.use(function(req, res, next) {
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
            next();
        });

        server.use(express.static('public'));
        server.use(favicon(path.join(__dirname, 'public', '/favicon.ico')));
        server.use(bodyParser.json({ limit: '25mb' }));
        server.use(bodyParser.urlencoded({ limit: '25mb', extended: false }));

        server.use('/api', require('./server/routes/index'));
        server.use('/api/image', require('./server/routes/image'));
        server.use('/api/auth', require('./server/routes/auth'));
        server.use('/api/admin', require('./server/routes/admin'));

        server.use(iframeReplacement);

        // server.engine('html', require('ejs').renderFile);
        // server.set('views', path.resolve(__dirname, 'views'));
        // server.set('view engine', 'html');

        // server.engine('jsx', require('express-react-views').createEngine());
        // server.set('views', path.resolve(__dirname, 'views'));
        // server.set('view engine', 'jsx');

        // server.engine('jsx', require('express-react-views').createEngine());
        // server.set('views', __dirname + 'views');
        // server.set('view engine', 'jsx');

        // server.get('/sitemap.xml', function (req, res) {
        //   sitemap.toXML(function (err, xml) {
        //     if (err) {
        //       return res.status(500).end();
        //     }
        //     res.header('Content-Type', 'application/xml');
        //     res.send(xml);
        //   });
        // });

        server.get('/sitemap.xml', (req, res) => {
            try {
                const xml = sitemap.toXML();
                res.header('Content-Type', 'application/xml');
                res.send(xml);
            } catch (e) {
                console.error(e);
                res.status(500).end();
            }
        });

        server.get('/blog', (req, res) => {
            console.log('/ is called');

            server.engine('html', require('ejs').renderFile);
            server.set('views', path.resolve(__dirname, 'views'));
            server.set('view engine', 'html');
            // respond to this request with our fake-news content embedded within the BBC News home page
            res.merge('fake-news', {
                // external url to fetch
                //  sourceUrl: 'http://www.bbc.co.uk/blog',
                sourceUrl: 'https://kevindam.partners.ifinsg.com',

                // css selector to inject our content into
                sourcePlaceholder: 'div[data-entityid="container-top-stories#1"]',
                // pass a function here to intercept the source html prior to merging
                transform: null
            });
        });

        server.get('/blog/:page', (req, res) => {
            console.log('/blog:page is called');
            let actualPage = '/';
            let page = req.params.page;

            server.engine('html', require('ejs').renderFile);
            server.set('views', path.resolve(__dirname, 'views'));
            server.set('view engine', 'html');
            // respond to this request with our fake-news content embedded within the BBC News home page
            res.merge('fake-news', {
                sourceUrl: 'https://kevindam.partners.ifinsg.com/' + page,
                sourcePlaceholder: 'div[data-entityid="container-top-stories#1"]',
                transform: null
            });
        });

        server.get('/blog/:page/:page2', (req, res) => {
            console.log('/blog:page is called');
            let page = req.params.page;
            let page2 = req.params.page2;
            server.engine('html', require('ejs').renderFile);
            server.set('views', path.resolve(__dirname, 'views'));
            server.set('view engine', 'html');
            res.merge('fake-news', {
                sourceUrl: 'https://kevindam.partners.ifinsg.com/' + page + '/' + page2,
                sourcePlaceholder: 'div[data-entityid="container-top-stories#1"]',
                transform: null
            });
        });

        server.get('/blog/:page/:page2/:page3', (req, res) => {
            console.log('/blog:page is called');
            let page = req.params.page;
            let page2 = req.params.page2;
            let page3 = req.params.page3;
            server.engine('html', require('ejs').renderFile);
            server.set('views', path.resolve(__dirname, 'views'));
            server.set('view engine', 'html');
            res.merge('fake-news', {
                sourceUrl: 'https://kevindam.partners.ifinsg.com/' + page + '/' + page2 + '/' + page3,
                sourcePlaceholder: 'div[data-entityid="container-top-stories#1"]',
                transform: null
            });
        });

        server.get('/blog/:page/:page2/:page3/:page4', (req, res) => {
            console.log('/blog:page is called');
            let page = req.params.page;
            let page2 = req.params.page2;
            let page3 = req.params.page3;
            let page4 = req.params.page4;
            server.engine('html', require('ejs').renderFile);
            server.set('views', path.resolve(__dirname, 'views'));
            server.set('view engine', 'html');
            res.merge('fake-news', {
                sourceUrl: 'https://kevindam.partners.ifinsg.com/' + page + '/' + page2 + '/' + page3 + '/' + page4,
                sourcePlaceholder: 'div[data-entityid="container-top-stories#1"]',
                transform: null
            });
        });

        server.get('/blog/:page/:page2/:page3/:page4/:page5', (req, res) => {
            console.log('/blog:page is called');
            let page = req.params.page;
            let page2 = req.params.page2;
            let page3 = req.params.page3;
            let page4 = req.params.page4;
            let page5 = req.params.page5;

            server.engine('html', require('ejs').renderFile);
            server.set('views', path.resolve(__dirname, 'views'));
            server.set('view engine', 'html');
            res.merge('fake-news', {
                sourceUrl:
                    'https://kevindam.partners.ifinsg.com/' +
                    page +
                    '/' +
                    page2 +
                    '/' +
                    page3 +
                    '/' +
                    page4 +
                    '/' +
                    page5,
                sourcePlaceholder: 'div[data-entityid="container-top-stories#1"]',
                transform: null
            });
        });

        server.get('/blog/:page/:page2/:page3/:page4/:page5/:page6', (req, res) => {
            console.log('/blog:page is called');
            let page = req.params.page;
            let page2 = req.params.page2;
            let page3 = req.params.page3;
            let page4 = req.params.page4;
            let page5 = req.params.page5;
            let page6 = req.params.page6;

            server.engine('html', require('ejs').renderFile);
            server.set('views', path.resolve(__dirname, 'views'));
            server.set('view engine', 'html');
            res.merge('fake-news', {
                sourceUrl:
                    'https://kevindam.partners.ifinsg.com/' +
                    page +
                    '/' +
                    page2 +
                    '/' +
                    page3 +
                    '/' +
                    page4 +
                    '/' +
                    page5 +
                    '/' +
                    page6,
                sourcePlaceholder: 'div[data-entityid="container-top-stories#1"]',
                transform: null
            });
        });

        server.get('/blog/:page/:page2/:page3/:page4/:page5/:page6/:page7', (req, res) => {
            console.log('/blog:page is called');
            let page = req.params.page;
            let page2 = req.params.page2;
            let page3 = req.params.page3;
            let page4 = req.params.page4;
            let page5 = req.params.page5;
            let page6 = req.params.page6;
            let page7 = req.params.page7;

            server.engine('html', require('ejs').renderFile);
            server.set('views', path.resolve(__dirname, 'views'));
            server.set('view engine', 'html');
            res.merge('fake-news', {
                sourceUrl:
                    'https://kevindam.partners.ifinsg.com/' +
                    page +
                    '/' +
                    page2 +
                    '/' +
                    page3 +
                    '/' +
                    page4 +
                    '/' +
                    page5 +
                    '/' +
                    page6 +
                    '/' +
                    page7,
                sourcePlaceholder: 'div[data-entityid="container-top-stories#1"]',
                transform: null
            });
        });

        server.get('/:page', (req, res) => {
            console.log('/:page is called');
            // server.engine('jsx', require('express-react-views').createEngine());
            // server.set('views', __dirname + 'views');
            // server.set('view engine', 'jsx');

            let actualPage = '/';
            let page = req.params.page;

            // if (page === "disclaimers") {
            //   res.redirect('/disclaimers')
            // } else if (page === "claims") {
            //   res.redirect('/claims')
            // } else
            if (global_constants.list_of_affiliates.includes(page)) {
                // Checking for legit affiliates on the home page
                const queryParams = { referrer: page };
                app.render(req, res, actualPage, queryParams);
            } else {
                // actualPage = '/' + "404"                            // URL broken
                // app.render(req, res, actualPage)
                console.log('let routes handle the routing');
                return handle(req, res);
            }
        });

        server.get('/:page/:ref', (req, res) => {
            console.log('/:page/:ref is called');

            let actualPage = '/';
            let page = req.params.page;
            let ref = req.params.ref;

            if (global_constants.list_of_affiliates.includes(ref)) {
                // Checking for legit affiliates
                return handle(req, res);
            } else {
                actualPage = '/' + 'page-not-found'; // URL broken (page BROKEN)
                app.render(req, res, actualPage);
            }

            // if (global_constants.list_of_pages.includes(page)){    // Checking for legit pages
            //   actualPage = '/' + page
            //   if (global_constants.list_of_affiliates.includes(ref)){    // Checking for legit affiliates
            //     const queryParams = { ref: ref }
            //     app.render(req, res, actualPage, queryParams)
            //   } else{
            //     actualPage = '/' + "404"                            // URL broken (page OK, ref BROKEN)
            //     app.render(req, res, actualPage)
            //   }
            // } else{
            //   actualPage = '/' + "404"                              // URL broken (page BROKEN)
            //   app.render(req, res, actualPage)
            // }
        });

        server.get('*', (req, res) => {
            // if (!req.secure) {       // No need to do this, use handler in app.yaml => secure: always    // Source: https://cloud.google.com/appengine/docs/standard/python/config/appref?hl=en#Python_app_yaml_Secure_URLs
            //   res.redirect(301, "https://" + req.headers.host + req.originalUrl);
            // } else {
            return handle(req, res);
            // }
        });

        server.listen(process.env.PORT, (err) => {
            if (err) throw err;
            console.log(`> Ready on http://localhost:${process.env.PORT}`, process.env.NODE_ENV);
        });
    })
    .catch((ex) => {
        console.error(ex.stack);
        process.exit(1);
    });
