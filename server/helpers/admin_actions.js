const email = require("../../email/app");
const stripe = require("./stripe");
const db = require("../helpers/pg").db;

const global_constants = require("../../global/constants");
const global_helpers = require("../../global/helpers");

const sample_function = (pass) => {
    // 1. Get something
    // 2. Do something else

    return new Promise((resolve, reject) => {
        try {
        } catch (err) {
            reject("Required params not present");
        }
    });
};

////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// User Actions /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

const send_email_upon_action = (model, pending_approval_action, email_address, email_message, amount) => {
    let link = "https://www.ifinsg.com/profile";
    let pass_email = {};
    let model_text = {
        user: {
            approve: "Your user details have been verified",
            rework: "Your attention is required. Some of your user details need to be edited.",
            reject: "Your application has been rejected."
        },
        policy: {
            approve: "Your policy has been approved and is now active.",
            // approve_reinstatement: "Your policy has been approved and is now active.",
            approve_cancel: "Your policy has been cancelled",
            rework: "Your attention is required. Some of your policy details need to be edited.",
            reject: "Your policy application has been rejected.",
            refund: `You have received a refund of $${amount}.`
        },
        accident: {
            approve: "Your accident details have been verified.",
            rework: "Your attention is required. Some of your accident details need to be edited.",
            reject: "Your accident details has been rejected."
        },
        claim: {
            approve: "Your claim has been approved.",
            rework: "Your attention is required. Some of your claim details need to be edited.",
            reject: "Your claim application has been rejected."
        },
        refund: {
            refund: `You have received a refund of $${amount}.`
        },
        charge: {
            charge: `You have been charged a total of $${amount}.`
        }
    };
    let email_subject = {
        user: {
            approve: "iFinSG Policy Approved",
            approve_cancel: "iFinSG Policy Cancellation request Approved",
            rework: "iFinSG Policy application Error",
            reject: "iFinSG Policy Rejected",
            refund: "iFinSG Payment Notice",
            charge: "iFinSG Charge Notice"
        },
        policy: {
            approve: "iFinSG Policy Approved",
            approve_cancel: "iFinSG Policy Cancellation request Approved",
            rework: "iFinSG Policy application Error",
            reject: "iFinSG Policy Rejected",
            refund: "iFinSG Payment Notice",
            charge: "iFinSG Charge Notice"
        },
        accident: {
            // not relevant?
            approve: "iFinSG Policy Approved",
            approve_cancel: "iFinSG Policy Cancellation request Approved",
            rework: "iFinSG Policy application Error",
            reject: "iFinSG Policy Rejected",
            refund: "iFinSG Payment Notice",
            charge: "iFinSG Charge Notice"
        },
        claim: {
            approve: "iFinSG Claim Approved",
            approve_cancel: "iFinSG Claim Cancellation request Approved",
            rework: "iFinSG Claim application Error",
            reject: "iFinSG Claim Rejected",
            refund: "iFinSG Payment Notice",
            charge: "iFinSG Charge Notice"
        },
        refund: {
            approve: "iFinSG Policy Approved",
            approve_cancel: "iFinSG Policy Cancellation request Approved",
            rework: "iFinSG Policy application Error",
            reject: "iFinSG Policy Rejected",
            refund: "iFinSG Payment Notice",
            charge: "iFinSG Charge Notice"
        },
        charge: {
            approve: "iFinSG Policy Approved",
            approve_cancel: "iFinSG Policy Cancellation request Approved",
            rework: "iFinSG Policy application Error",
            reject: "iFinSG Policy Rejected",
            refund: "iFinSG Payment Notice",
            charge: "iFinSG Charge Notice"
        }
    };

    if (model === "none") {
        pass_email.send_email_email_address = "yibu.wang@ifinancialsingapore.com";
        pass_email.send_email_subject = "iFinSG ERROR ALERT";
        pass_email.send_email_message = `
        <html>

        This message was not sent out due to an error with model in send_email_upon_action() in admin_actions.js

        pending_approval_action = ${pending_approval_action}
        email_address = ${email_address}
        email_message = ${email_message}
        </html>
        `;
        // pass_email
        email
            .send_email({
                to: pass_email.send_email_email_address,
                subject: pass_email.send_email_subject,
                text: "",
                html: pass_email.send_email_message
            })
            .catch((err) => {
                console.log(err);
            });
        return;
    }

    let generate_email_message = (email_message) => {
        if (!email_message) {
            return `
            <h4>Please use the link below to view the details.</h4>
            `;
        } else {
            return `
        <h4> ${email_message ? email_message : ""}</h4>
        <h4>Please use the link below to view the details.</h4>
        `;
        }
    };

    pass_email.send_email_email_address = email_address;
    pass_email.send_email_subject = email_subject[model][pending_approval_action];
    pass_email.send_email_message =
        `
        <html>
            <h2>Hello!</h2>
            <h4>${model_text[model][pending_approval_action]}</h4>

            <h4> ${generate_email_message(email_message)} </h4>

            <a href="` +
        link +
        `"> 
            <h4>` +
        link +
        `</h4>
            </a>
    
            <div>Always here for You,</div> 
            <br>
            <div>Adele</div>
            <div>Ops Team</div>
            <div>www.iFinSG.com</div>
            <br>
            <div>Phone: +65 968 986 76</div>
            <div>Email: adele@ifinancialsingapore.com</div>
            <div>Facebook: https://www.facebook.com/ifinancialsingapore</div>
            <div>LinkedIn: https://www.linkedin.com/company/ifinancial-singapore?trk=biz-companies-cym </div>

        </html>
        `;
    email
        .send_email({
            to: pass_email.send_email_email_address,
            subject: pass_email.send_email_subject,
            text: "",
            html: pass_email.send_email_message
        })
        .catch((err) => {
            console.log(err);
        });
};

const pending_approval_action = (pass) => {
    // 1. Get something
    // 2. Do something else

    return new Promise((resolve, reject) => {
        try {
            async function signups() {
                let updatable_policy = {};
                let updatable_policy_all = {};
                let updatable_user = {};

                switch (pass.pending_approval_action) {
                    case "approve":
                        if (global_constants.staging) {
                            // STAGING setting

                            if (pass.user_or_policy === "user") {
                                ////////////// payer_details_verified //////////////

                                updatable_user = {
                                    payer_details_verified: true
                                };
                                pass.pending_approval_action = "verify_payer_details";
                                // await db('users').where({ ref_username_norm: global_helpers.normalise_string(pass.username) }).update(updatable).then(rows => {
                            } else {
                                //if user_or_policy === "policy"
                                ////////////// Create customer and create subscription //////////////

                                // Get stripe info
                                await db
                                    .from("users")
                                    .select("*")
                                    .where({ email_verified: true })
                                    .where({ username_norm: global_helpers.normalise_string(pass.username) })
                                    .then((rows) => {
                                        let user_info = rows[0];

                                        try {
                                            pass.create_customer_and_start_subscription_email = user_info.username_norm; // eg "mildjx@gmail.com"
                                            pass.create_customer_and_start_subscription_token =
                                                user_info.stripe_details.id; // eg "tok_1EgTTvIQ6dUBUfMLjHdfswM8@gmail.com"
                                            pass.create_customer_and_start_subscription_customer_id =
                                                user_info.stripe_customer_id; // eg "tok_1EgTTvIQ6dUBUfMLjHdfswM8@gmail.com"
                                            pass.create_customer_and_start_subscription_plans = [
                                                { plan: global_constants.stripe_policy_payment_plans[pass.policy] }
                                            ];
                                            // pass.create_customer_and_start_subscription_coupon = global_constants.stripe_policy_coupons.forever_admin_fee_100
                                            pass.create_customer_and_start_subscription_coupon = pass.coupon_name
                                                ? global_constants.stripe_policy_coupons[pass.coupon_name]
                                                : null;

                                            pass.create_customer_and_start_subscription_billing_cycle_anchor = global_helpers.stripe_get_next_cycle_time_seconds(
                                                "quarterly",
                                                8 * 60 * 60
                                            ); // stripe_get_next_cycle_time_seconds("monthly"/"quarterly" , offset_sec eg. 8 * 60 * 60 )                    // eg 1560404681
                                            pass.create_customer_and_start_subscription_policy_id = pass.id;

                                            return stripe.create_customer_and_start_subscription(pass);
                                        } catch (err) {
                                            console.log(err);
                                            reject({
                                                success: false,
                                                err_code: "failed_to_retrieve_user_info_from_database",
                                                error: "Unable to retrieve user info from database"
                                            });
                                        }
                                    })
                                    .then((pass) => {
                                        // console.log(pass)
                                        updatable_policy = {
                                            flag_staff_inbox_category: "active",
                                            status: "active"
                                        };
                                    })
                                    .catch((err) => {
                                        console.log(err);
                                    });
                            }

                            break;
                        } else {
                            // LIVE setting
                            updatable_policy = {
                                flag_staff_inbox_category: "active",
                                status: "active"
                            };
                            break;
                        }

                    case "refund":
                        let event_insertable = {
                            ref_username_norm: pass.verify_token_username, // username of person making that action
                            ref_policy_id: pass.id, // username of person making that action
                            type: "admin_nuclear_" + pass.pending_approval_action,
                            data: {
                                action: "refund",
                                amount: pass.amount,
                                username_user: global_helpers.normalise_string(pass.username)
                            }
                        };

                        await db("events")
                            .insert(event_insertable)
                            .catch((err) => {
                                console.log(err);
                            })
                            .then((rows) => {
                                send_email_upon_action(
                                    "refund",
                                    pass.pending_approval_action,
                                    pass.username,
                                    pass.pending_rework_message,
                                    pass.amount
                                );
                                resolve(pass);
                                return true;
                            });
                        break;

                    case "charge":
                        await db
                            .from("users")
                            .select("*")
                            .where({ email_verified: true })
                            .where({ username_norm: global_helpers.normalise_string(pass.username) })
                            .then((rows) => {
                                let user_info = rows[0];
                                pass.create_charge_to_customer_customer = user_info.stripe_customer_id; // eg "tok_1EgTTvIQ6dUBUfMLjHdfswM8@gmail.com"
                                pass.create_charge_to_customer_amount = pass.amount * 100;
                                pass.create_charge_to_customer_currency = "SGD";

                                return stripe
                                    .create_charge_to_customer(pass)
                                    .then((pass) => {
                                        let event_insertable = {
                                            ref_username_norm: pass.verify_token_username, // username of person making that action
                                            ref_policy_id: pass.id, // username of person making that action
                                            type: "admin_nuclear_" + pass.pending_approval_action,
                                            data: {
                                                action: "charge",
                                                amount: pass.amount,
                                                username_user: global_helpers.normalise_string(pass.username)
                                            }
                                        };
                                        return db("events")
                                            .insert(event_insertable)
                                            .then((rows) => {
                                                send_email_upon_action(
                                                    "charge",
                                                    pass.pending_approval_action,
                                                    pass.username,
                                                    pass.pending_rework_message,
                                                    pass.amount
                                                );
                                                resolve(pass);
                                                return true;
                                            })
                                            .catch((err) => {
                                                console.log(err);
                                                reject(err);
                                            });
                                    })
                                    .catch((err) => {
                                        console.log(err);
                                        reject(err);
                                    });
                            })
                            .catch((err) => {
                                console.log(err);
                                reject(err);
                            });

                        break;

                    case "approve_cancel":
                        if (global_constants.staging) {
                            // STAGING setting

                            ////////////// STOP CUSTOMER'S SUBSCRIPTION //////////////
                            // Get stripe_subscription_id info
                            await db
                                .from("policies")
                                .select("*")
                                .where({ id: pass.id })
                                .then((rows) => {
                                    let policy_info = rows[0];
                                    try {
                                        pass.cancel_subscription_username = policy_info.ref_username_norm;
                                        pass.cancel_subscription_policy_id = policy_info.id;
                                        pass.cancel_subscription_subscription_id = policy_info.stripe_subscription_id;
                                        if (
                                            pass.cancel_subscription_subscription_id == null ||
                                            pass.cancel_subscription_subscription_id.length <= 5
                                        ) {
                                            reject({
                                                success: false,
                                                err_code: "stripe_subscription_id_not_found",
                                                err: "Policy Subscription ID unable to be found"
                                            });
                                            return false;
                                        } else {
                                            return stripe.cancel_subscription(pass);
                                        }
                                    } catch (err) {
                                        reject({
                                            success: false,
                                            err_code: "failed_to_retrieve_policy_info_from_database",
                                            error: "Unable to retrieve user info from database"
                                        });
                                    }
                                })
                                .then((pass) => {
                                    updatable_policy = {
                                        flag_staff_inbox_category: "cancelled",
                                        status: "cancelled"
                                    };
                                })
                                .catch((err) => {});

                            break;
                        } else {
                            // LIVE setting
                            updatable_policy = {
                                flag_staff_inbox_category: "cancelled",
                                status: "cancelled"
                            };
                            break;
                        }

                    case "rework":
                        if (pass.user_or_policy === "user") {
                            updatable_policy_all = {
                                flag_staff_inbox_category: "rework",
                                status: "pending_rework"
                            };
                            updatable_user = {
                                pending_rework_items: JSON.stringify(pass.pending_rework_items),
                                pending_rework_message: pass.pending_rework_message
                            };
                        } else {
                            updatable_policy = {
                                flag_staff_inbox_category: "rework",
                                status: "pending_rework",
                                pending_rework_items: JSON.stringify(pass.pending_rework_items),
                                pending_rework_message: pass.pending_rework_message
                            };
                        }
                        break;
                    case "reject":
                        updatable_policy = {
                            flag_staff_inbox_category: "rejected",
                            status: "rejected",
                            pending_rework_message: pass.pending_rework_message
                        };
                        break;
                    default:
                        // Not supposed to happen
                        break;
                }

                if (global_helpers.obj_is_empty(updatable_policy) && global_helpers.obj_is_empty(updatable_user)) {
                    // If there are updates to any policy
                    reject("ERROR - pending_approval_action not defined");
                } else {
                    db.transaction((trx) => {
                        if (!global_helpers.obj_is_empty(updatable_policy)) {
                            // If there are updates to any policy

                            return trx("policies")
                                .where({ ref_username_norm: global_helpers.normalise_string(pass.username) })
                                .where({ id: pass.id })
                                .update(updatable_policy)
                                .then(() => {
                                    // Updating policy status_history
                                    if (updatable_policy.status) {
                                        return trx.raw(
                                            `UPDATE policies SET status_history = jsonb_insert(status_history, '{0}','{"created_at":"${new Date().toISOString()}", "status": "${
                                                updatable_policy.status
                                            }"}') WHERE id = ${pass.id}`
                                        );
                                    }
                                })
                                .then(() => {
                                    // Updating user
                                    if (!global_helpers.obj_is_empty(updatable_user)) {
                                        // If there are updates to any policy

                                        return trx("users")
                                            .where({ username_norm: global_helpers.normalise_string(pass.username) })
                                            .update(updatable_user);
                                    }
                                })
                                .then(() => {
                                    let event_insertable = {
                                        ref_username_norm: pass.verify_token_username, // username of person making that action
                                        ref_policy_id: pass.id, // username of person making that action
                                        type: "admin_application_policy_" + pass.pending_approval_action,
                                        data: {
                                            username_user: pass.username // username of user
                                        }
                                    };
                                    if (pass.pending_rework_message) {
                                        event_insertable.data.message = pass.pending_rework_message;
                                    }

                                    return trx("events")
                                        .insert(event_insertable)
                                        .catch((err) => {
                                            console.log(err);
                                        });
                                });
                        } else if (!global_helpers.obj_is_empty(updatable_user)) {
                            // If there are updates to any policy

                            // If there are updates to any user, but not any policy
                            return trx("users")
                                .where({ username_norm: global_helpers.normalise_string(pass.username) })
                                .update(updatable_user)
                                .then(() => {
                                    if (!global_helpers.obj_is_empty(updatable_policy_all)) {
                                        // If there are updates to ALL policies
                                        return trx("policies")
                                            .where({
                                                ref_username_norm: global_helpers.normalise_string(pass.username)
                                            })
                                            .update(updatable_policy_all)
                                            .then((result) => {
                                                let event_insertable = {
                                                    ref_username_norm: pass.verify_token_username, // username of person making that action
                                                    // ref_policy_id: pass.id,
                                                    type: "admin_application_policy_" + pass.pending_approval_action,
                                                    data: {
                                                        username_user: pass.username // username of user
                                                    }
                                                };
                                                if (pass.pending_rework_message) {
                                                    event_insertable.data.message = pass.pending_rework_message;
                                                }
                                                return trx("events")
                                                    .insert(event_insertable)
                                                    .catch((err) => {
                                                        console.log(err);
                                                    });
                                            });
                                    } else {
                                        // If there are no updates to ALL policies
                                        let event_insertable = {
                                            ref_username_norm: pass.verify_token_username, // username of person making that action
                                            // ref_policy_id: pass.id,
                                            type: "admin_application_policy_" + pass.pending_approval_action,
                                            data: {
                                                username_user: pass.username // username of user
                                            }
                                        };
                                        if (pass.pending_rework_message) {
                                            event_insertable.data.message = pass.pending_rework_message;
                                        }
                                        return trx("events")
                                            .insert(event_insertable)
                                            .catch((err) => {
                                                console.log(err);
                                            });
                                    }
                                });
                        }
                    })
                        .then((inserts) => {
                            resolve(pass);

                            let model = pass.user_or_policy
                                ? pass.user_or_policy
                                : pass.accident_or_claim
                                ? pass.accident_or_claim
                                : "none";

                            if (pass.pending_approval_action === "verify_payer_details") {
                                // No need to send if verifying payer details
                            } else {
                                send_email_upon_action(
                                    model,
                                    pass.pending_approval_action,
                                    pass.username,
                                    pass.pending_rework_message
                                );
                            }
                        })
                        .catch((err) => {
                            console.error(err);
                            reject(err);
                        });
                }
            }

            async function claims() {
                let updatable_claim = {};
                let updatable_claim_where_filter = {};
                let updatable_accident = {};
                let updatable_accident_where_filter = {};

                switch (pass.pending_approval_action) {
                    case "approve":
                        if (global_constants.staging) {
                            // STAGING setting

                            console.log("BEFORE AWAIT");

                            if (pass.accident_or_claim === "accident") {
                                ////////////// payer_details_verified //////////////

                                updatable_accident = { accident_details_verified: true };
                                updatable_accident_where_filter = { id: pass.id };
                                pass.pending_approval_action = "verify_accident_details";
                                // await db('accidents').where({ ref_username_norm: global_helpers.normalise_string(pass.username) }).update(updatable).then(rows => {
                            } else {
                                //if accident_or_claim === "claim"

                                if (isNaN(pass.amount)) {
                                    reject({ success: "false", err: "Error amount is not a number!" });
                                    return;
                                }
                                updatable_claim = {
                                    status: "approved",
                                    payout: pass.amount
                                };
                            }

                            break;
                        } else {
                            // LIVE setting
                            if (isNaN(pass.amount)) {
                                reject({ success: "false", err: "Error amount is not a number!" });
                                return;
                            }
                            updatable_claim = {
                                status: "approved",
                                payout: pass.amount
                            };
                            break;
                        }

                    case "rework":
                        if (pass.accident_or_claim === "accident") {
                            updatable_accident = {
                                pending_rework_items: JSON.stringify(pass.pending_rework_items),
                                pending_rework_message: pass.pending_rework_message
                            };
                        } else {
                            updatable_claim = {
                                status: "pending_rework",
                                pending_rework_items: JSON.stringify(pass.pending_rework_items),
                                pending_rework_message: pass.pending_rework_message
                            };
                        }

                        break;
                    case "reject":
                        updatable_claim = {
                            status: "rejected",
                            pending_rework_message: pass.pending_rework_message
                        };
                        break;
                    default:
                        // Not supposed to happen
                        break;
                }

                if (global_helpers.obj_is_empty(updatable_claim) && global_helpers.obj_is_empty(updatable_accident)) {
                    // If there are updates to any claim
                    reject("ERROR - pending_approval_action not defined");
                } else {
                    db.transaction((trx) => {
                        if (!global_helpers.obj_is_empty(updatable_claim)) {
                            // If there are updates to any claim

                            return trx("claims")
                                .where({ ref_username_norm: global_helpers.normalise_string(pass.username) })
                                .where({ id: pass.id })
                                .update(updatable_claim)
                                .then(() => {
                                    // Updating claim status_history
                                    if (updatable_claim.status) {
                                        return trx.raw(
                                            `UPDATE claims SET status_history = jsonb_insert(status_history, '{0}','{"created_at":"${new Date().toISOString()}", "status": "${
                                                updatable_claim.status
                                            }"}') WHERE id = ${pass.id}`
                                        );
                                    }
                                })
                                .then(() => {
                                    // Updating accident
                                    if (!global_helpers.obj_is_empty(updatable_accident)) {
                                        // If there are updates to any claim
                                        return trx("accidents")
                                            .where({ id: pass.id })
                                            .update(updatable_accident);
                                    }
                                })
                                .then(() => {
                                    let event_insertable = {
                                        ref_username_norm: pass.verify_token_username, // username of person making that action
                                        type: "admin_application_claim_" + pass.pending_approval_action,
                                        data: updatable_claim
                                    };
                                    event_insertable.data.username_user = pass.username; // username of user

                                    if (pass.pending_rework_message) {
                                        event_insertable.data.message = pass.pending_rework_message;
                                    }

                                    if (pass.accident_or_claim === "accident") {
                                        event_insertable.ref_accident_id = pass.id;
                                    } else {
                                        event_insertable.ref_claim_id = pass.id;
                                    }
                                    return trx("events")
                                        .insert(event_insertable)
                                        .catch((err) => {
                                            console.log(err);
                                        });
                                });
                        } else if (!global_helpers.obj_is_empty(updatable_accident)) {
                            // If there are updates to any claim

                            // If there are updates to any accident, but not any claim
                            return trx("accidents")
                                .where({ ref_username_norm: global_helpers.normalise_string(pass.username) })
                                .where({ id: pass.id })
                                .update(updatable_accident)
                                .then(() => {
                                    // if (!global_helpers.obj_is_empty(updatable_claim_all)) {  // If there are updates to ALL policies
                                    //     return trx('claims').where({ ref_username_norm: global_helpers.normalise_string(pass.username) }).update(updatable_claim_all).then(result => {
                                    //         let event_insertable = {
                                    //             ref_username_norm: pass.verify_token_username, // username of person making that action
                                    //             type: "admin_application_" + pass.pending_approval_action,
                                    //             data: {
                                    //                 username_user: pass.username, // username of user
                                    //             }
                                    //         }
                                    //         if (pass.accident_or_claim === "accident") { event_insertable.ref_accident_id = pass.id}
                                    //         else{ event_insertable.ref_claim_id = pass.id}
                                    //         console.log(event_insertable)
                                    //         return trx('events').insert(event_insertable).catch(err=>{console.log(err)})
                                    //     })
                                    // } else {    // If there are no updates to ALL policies
                                    let event_insertable = {
                                        ref_username_norm: pass.verify_token_username, // username of person making that action
                                        type: "admin_application_accident_" + pass.pending_approval_action,
                                        data: {
                                            username_user: pass.username // username of user
                                        }
                                    };
                                    if (pass.pending_rework_message) {
                                        event_insertable.data.message = pass.pending_rework_message;
                                    }

                                    if (pass.accident_or_claim === "accident") {
                                        event_insertable.ref_accident_id = pass.id;
                                    } else {
                                        event_insertable.ref_claim_id = pass.id;
                                    }
                                    return trx("events")
                                        .insert(event_insertable)
                                        .catch((err) => {
                                            console.log(err);
                                        });
                                    // }
                                });
                        }
                    })
                        .then((inserts) => {
                            resolve(pass);

                            let model = pass.user_or_policy
                                ? pass.user_or_policy
                                : pass.accident_or_claim
                                ? pass.accident_or_claim
                                : "none";

                            if (
                                pass.pending_approval_action == "verify_accident_details" &&
                                pass.accident_or_claim == "accident"
                            ) {
                                // No need to send email
                            } else {
                                send_email_upon_action(
                                    model,
                                    pass.pending_approval_action,
                                    pass.username,
                                    pass.pending_rework_message
                                );
                            }
                        })
                        .catch((err) => {
                            console.error(err);
                            reject(err);
                        });
                }
            }

            if (pass.signups_or_claims === "signups") {
                signups();
            } else {
                claims();
            }
        } catch (err) {
            console.log(err);
            reject("Required params not present");
        }
    });
};

module.exports = {
    pending_approval_action
};
