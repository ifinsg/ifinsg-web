var jwt = require("jsonwebtoken"); // used to create, sign, and verify tokens
const bcrypt = require("bcryptjs"); // Password hashing - https://www.npmjs.com/package/bcrypt

// const email = require("../helpers/email");
const email = require("../../email/app");
const db = require("../helpers/pg").db;
const upsert = require("knex-upsert");

const TOKEN_PRIVATE_KEY = "!-hV28`FBk5]AGg2uY6f<<4'e<Tp$#86BY7m;2P<z/^NdvA";
// const TOKEN_EXPIRE_TIME_14D = "1209600s"; // 2 weeks (in seconds)
const TOKEN_EXPIRE_TIME_1D = "86400s"; // 1 day (in seconds)
// const TOKEN_EXPIRE_TIME_1D = "60s"; // 1 day (in seconds)

const global_constants = require("../../global/constants");
const global_helpers = require("../../global/helpers");

const sample_function = (pass) => {
    console.log("sample_function()");
    // 1. Get something
    // 2. Do something else

    return new Promise((resolve, reject) => {
        if (pass.sample_function) {
        } else {
            reject("Required params not present");
        }
    });
};

////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// Signup ////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

const signup = (pass) => {
    console.log("signup()");
    // 1. Check if sugnup rules are fulfilled
    // 2. Create new user account in DB
    // 3. Sign token

    return new Promise((resolve, reject) => {
        if (pass.username) {
            signup_rules_check(pass)
                .then((pass) => {
                    return create_new_account(pass);
                })
                .then((pass) => {
                    return issue_email_verification_email(pass);
                })
                .then((pass) => {
                    resolve(pass);
                })
                .catch((err) => {
                    reject(err);
                });
        } else {
            reject("Required params not present");
        }
    });
};

const signup_rules_check = (pass) => {
    console.log("signup_rules_check()");
    // 1. Check to see if username has been used already or not.

    return new Promise((resolve, reject) => {
        if (pass.username) {
            db.from("users")
                .select("*")
                .where({ username_norm: global_helpers.normalise_string(pass.username) })
                .where({ email_verified: true })
                .then((rows) => {
                    if (rows.length !== 0) {
                        const err = "Email already in use. Try logging in as an existing user instead :)";
                        console.log(err);
                        reject({
                            success: false,
                            err: err // This will actually be displayed to the user
                        });
                    } else {
                        console.log("accepting...");
                        resolve(pass);
                    }
                })
                .catch((err) => {
                    console.log(err);
                    reject(err);
                });
        } else {
            reject("ERROR - Required params not present");
        }
    });
};

const create_new_account = (pass) => {
    console.log("create_new_account()");
    // 1. Hash password
    // 2. Generate email_verification_code
    // 3. Push user to DB with username, email_verification_code, hashed password and datetime created

    return new Promise((resolve, reject) => {
        if (pass.username && pass.password) {
            // Hashing password
            bcrypt.genSalt(10, function(err, salt) {
                if (err) {
                    reject(err);
                } else {
                    bcrypt.hash(pass.password, salt, function(err, hash) {
                        if (err) {
                            reject(err);
                        } else {
                            pass.password_hash = hash;
                            delete pass.password;

                            ///// Generating email_verification_code /////
                            pass.email_verification_code = parseInt(Math.random() * 10000)
                                .toString()
                                .padStart(4, "0"); // '0005'

                            ///// Upserting new User /////
                            upsert({
                                db,
                                table: "users",
                                object: {
                                    username: pass.username,
                                    username_norm: global_helpers.normalise_string(pass.username),
                                    password_hash: pass.password_hash,
                                    email_verification_code: pass.email_verification_code,
                                    referrer: pass.referrer ? pass.referrer : ""
                                },
                                key: "username_norm"
                            })
                                .then((result) => {
                                    ///// Asynchronously Storing in events /////
                                    let storable = [
                                        {
                                            ref_username_norm: global_helpers.normalise_string(pass.username),
                                            type: "user_signup",
                                            data: {
                                                password_hash: hash
                                            }
                                        }
                                    ];
                                    db("events")
                                        .insert(storable)
                                        .catch((err) => {
                                            console.log(err);
                                        });

                                    resolve(pass);
                                })
                                .catch((err) => {
                                    console.log(err);
                                    reject(err);
                                });
                        }
                    });
                }
            });
        } else {
            reject("Required params not present");
        }
    });
};

const issue_email_verification_email = (pass) => {
    console.log("issue_email_verification_email()");

    return new Promise((resolve, reject) => {
        if ((pass.username, pass.email_verification_code)) {
            pass.send_email_email_address = pass.username;
            pass.send_email_subject = "iFinSG Email Verification Code";
            pass.send_email_message =
                `
                <html>
                    <h2>Hello!</h2>
                    <h4>Thanks for signing up with iFinSg</h4>
                    <h4>Please use this code for email verification :)</h4>
            
                    <br>
                    <div style="text-align:center">
                    <h1>` +
                pass.email_verification_code +
                `</h1>
                    </div>
                    <br>
            
                    <div>Always here for You,</div> 
                    <br>
                    <div>Adele</div>
                    <div>Ops Team</div>
                    <div>www.iFinSG.com</div>
                    <br>
                    <div>Phone: +65 968 986 76</div>
                    <div>Email: adele@ifinancialsingapore.com</div>
                    <div>Facebook: https://www.facebook.com/ifinancialsingapore</div>
                    <div>LinkedIn: https://www.linkedin.com/company/ifinancial-singapore?trk=biz-companies-cym </div>
            
                </html>
                `;
            email
                .send_email({
                    to: pass.send_email_email_address,
                    subject: pass.send_email_subject,
                    text: "",
                    html: pass.send_email_message
                })
                .then((pass) => {
                    // console.log(pass)
                    resolve(pass);
                })
                .catch((err) => {
                    console.log(err);
                    reject(err);
                });
        } else {
            reject("ERROR - Required params not present");
        }
    });
};

const verify_email_verification_code = (pass) => {
    console.log("verify_email_verification_code()");
    // 1. Get verification code from DB
    // 2. Update DB with email_verified = true
    // 3. Sign token

    return new Promise((resolve, reject) => {
        if ((pass.username, pass.email_verification_code)) {
            db.from("users")
                .select("*")
                .where({ username_norm: global_helpers.normalise_string(pass.username) })
                .then((rows) => {
                    console.log(rows);
                    let user_info = rows[0]; // There should only be 1 anyways
                    if (user_info.email_verification_code === pass.email_verification_code) {
                        console.log("email verification code verified");
                        sign_token(pass)
                            .then((pass) => {
                                // Log in
                                // Update email_verified = true
                                db("users")
                                    .where({ username_norm: global_helpers.normalise_string(pass.username) })
                                    .update({ email_verified: true })
                                    .then((result) => {
                                        resolve(pass);
                                        ///// Asynchronously Storing in events /////
                                        let storable = [
                                            {
                                                ref_username_norm: global_helpers.normalise_string(pass.username),
                                                type: "user_verified",
                                                data: {}
                                            }
                                        ];
                                        db("events")
                                            .insert(storable)
                                            .catch((err) => {
                                                console.log(err);
                                            });
                                    })
                                    .catch((err) => {
                                        console.log(err);
                                        reject(err);
                                    });
                            })
                            .catch((err) => {
                                console.log(err);
                                reject(err);
                            });
                    }
                })
                .catch((err) => {
                    console.log(err);
                    reject(err);
                });
        } else {
            reject("ERROR - Required params not present");
        }
    });
};

////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// Login /////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

const verify_credentials = (pass) => {
    console.log("verify_credentials()");
    //1. Get password_hash from DB
    //2. Compare with user entered password

    return new Promise((resolve, reject) => {
        try {
            db.from("users")
                .select("*")
                .where({ username_norm: global_helpers.normalise_string(pass.username) })
                .where({ email_verified: true })
                .then((rows) => {
                    if (rows.length === 0) {
                        reject({
                            success: false,
                            err_code: "user_does_not_exist",
                            err:
                                "Did you type in your Email address correctly?\n\nor\n\nDid you use another Email address for your account?"
                        });
                    } else {
                        try {
                            bcrypt.compare(pass.password, rows[0].password_hash, function(err, res) {
                                if (err) {
                                    console.log(err);
                                    reject(err);
                                } else if (res) {
                                    // res === true
                                    console.log("password correct!");
                                    delete pass.password;
                                    pass.user_info = rows[0];
                                    resolve(pass);
                                } else {
                                    console.log("password incorrect :(");
                                    delete pass.password;
                                    reject({
                                        success: false,
                                        err_code: "password_incorrect",
                                        err: "Wrong password entered"
                                    });
                                }
                            });
                        } catch (err) {
                            console.log(err);
                            reject("Unexpected params returned");
                        }
                    }
                })
                .catch((err) => {
                    reject(err);
                });
        } catch (err) {
            console.log(err);
            reject("Required params not present");
        }
    });
};

const sign_token = (pass) => {
    console.log("sign_token()");
    // 1. Signs token
    // 2. Stores token in DB, tagged to which email account, and time it was generated

    return new Promise((resolve, reject) => {
        if (pass.username) {
            jwt.sign(
                { username: pass.username },
                TOKEN_PRIVATE_KEY,
                { expiresIn: TOKEN_EXPIRE_TIME_1D },
                (err, token) => {
                    if (err) {
                        reject(err);
                    } else {
                        pass.token = token;

                        ///// Asynchronously Storing in events /////
                        let storable = [
                            {
                                ref_username_norm: global_helpers.normalise_string(pass.username),
                                type: "user_login",
                                data: {
                                    token: pass.token
                                }
                            }
                        ];
                        db("events")
                            .insert(storable)
                            .then((result) => {
                                resolve(pass);
                            })
                            .catch((err) => {
                                console.log(err);
                            });
                    }
                }
            );
        } else {
            reject("Required params not present");
        }
    });
};

const change_password = (pass) => {
    console.log("change_password()");
    // 1. Hash password_new
    // 2. Set new password_hashed

    return new Promise((resolve, reject) => {
        try {
            // Hashing password
            bcrypt.genSalt(10, function(err, salt) {
                if (err) {
                    reject(err);
                } else {
                    bcrypt.hash(pass.password_new, salt, function(err, hash) {
                        if (err) {
                            reject(err);
                        } else {
                            // pass.password_hash = hash
                            delete pass.password_new;

                            // console.log(password)
                            // console.log(pass)

                            db("users")
                                .update({ password_hash: hash })
                                .where({ email_verified: true })
                                .where({
                                    username_norm: global_helpers.normalise_string(pass.change_password_username)
                                })
                                .then((result) => {
                                    // pass.update_model_model = "User" // eg: User
                                    // pass.update_model_filter = { username_norm: global_helpers.normalise_string(pass.change_password_username), email_verified: true }
                                    // pass.update_model_update = { password_hash: hash }
                                    // mongo.update_model(pass).then(pass => {

                                    resolve(pass);

                                    ///// Asynchronously Storing in events /////
                                    let storable = [
                                        {
                                            ref_username_norm: global_helpers.normalise_string(
                                                pass.change_password_username
                                            ),
                                            type: "user_change_password",
                                            data: {
                                                password_hash_new: hash
                                            }
                                        }
                                    ];
                                    db("events")
                                        .insert(storable)
                                        .catch((err) => {
                                            console.log(err);
                                        });

                                    try {
                                        let preferred_name = pass.change_password_username.split("@")[0];
                                        if (pass.user_info && pass.user_info.preferred_name) {
                                            preferred_name = pass.user_info.preferred_name;
                                        }

                                        // Send email
                                        pass.send_email_email_address = pass.change_password_username;
                                        pass.send_email_subject = "Password Change Notification";
                                        pass.send_email_message =
                                            `
                                    <html>
                                    <h2>Dear ` +
                                            preferred_name +
                                            `,</h2>
                    

                                    <h4>Your iFinSg password has been changed.</h4> 
                                
                                    <h4>If this was not done by you, please reply to this email immediately.</h4>
                                        
                                    
                                    <div>Always here for You,</div> 
                                    <br>
                                    <div>Adele</div>
                                    <div>Ops Team</div>
                                    <div>www.iFinSG.com</div>
                                    <br>
                                    <div>Phone: +65 968 986 76</div>
                                    <div>Email: adele@ifinancialsingapore.com</div>
                                    <div>Facebook: https://www.facebook.com/ifinancialsingapore</div>
                                    <div>LinkedIn: https://www.linkedin.com/company/ifinancial-singapore?trk=biz-companies-cym </div>

                                    </html>
                                `;
                                        email.send_email({
                                            to: pass.send_email_email_address,
                                            subject: pass.send_email_subject,
                                            text: "",
                                            html: pass.send_email_message
                                        });
                                    } catch (err) {
                                        console.log(err);
                                        console.log("Failed to send out email");
                                    }
                                })
                                .catch((err) => {
                                    console.log(err);
                                    reject(err);
                                });
                        }
                    });
                }
            });
        } catch (err) {
            console.log(err);
            reject("Required params not present");
        }
    });
};

const send_forget_password_email_link = (pass) => {
    console.log("send_forget_password_email_link()");
    // 1. Generate link, store in DB, attach to users doc
    // 2. Send email with link

    return new Promise((resolve, reject) => {
        try {
            let forget_password_verification_code =
                pass.send_forget_password_email_link_username +
                "_" +
                parseInt(Math.random() * 10000).toString() +
                "_" +
                parseInt(new Date().getTime()).toString();

            db("users")
                .update({ forget_password_verification_code })
                .where({ email_verified: true })
                .where({
                    username_norm: global_helpers.normalise_string(pass.send_forget_password_email_link_username)
                })
                .then((result) => {
                    // let email_verification_link = "localhost:8080/reset-password?code=" + forget_password_verification_code
                    let email_verification_link =
                        "https://www.ifinsg.com/reset-password?code=" + forget_password_verification_code;
                    if (global_constants.staging) {
                        email_verification_link =
                            "https://staging-dot-ifinsgdev2.appspot.com/reset-password?code=" +
                            forget_password_verification_code;
                    }

                    console.log(email_verification_link);

                    pass.send_email_email_address = pass.send_forget_password_email_link_username;
                    pass.send_email_subject = "iFinSg Reset Your Password";
                    pass.send_email_message =
                        `
                    <html>
                        <h2>Hello!</h2>
                        <h4>You have just requested to reset your iFinSg password</h4>
                        <h4>Please use this link to reset your password</h4>
                        <h4>For your security, this link will expire in 10 mins</h4>

                        <a href="` +
                        email_verification_link +
                        `"> 
                        <h4>` +
                        email_verification_link +
                        `</h4>
                        </a>
                
                        <div>Always here for You,</div> 
                        <br>
                        <div>Adele</div>
                        <div>Ops Team</div>
                        <div>www.iFinSG.com</div>
                        <br>
                        <div>Phone: +65 968 986 76</div>
                        <div>Email: adele@ifinancialsingapore.com</div>
                        <div>Facebook: https://www.facebook.com/ifinancialsingapore</div>
                        <div>LinkedIn: https://www.linkedin.com/company/ifinancial-singapore?trk=biz-companies-cym </div>

                    </html>
                    `;
                    email
                        .send_email({
                            to: pass.send_email_email_address,
                            subject: pass.send_email_subject,
                            text: "",
                            html: pass.send_email_message
                        })
                        .then((pass) => {
                            ///// Asynchronously Storing in events /////
                            let storable = [
                                {
                                    ref_username_norm: global_helpers.normalise_string(
                                        pass.send_forget_password_email_link_username
                                    ),
                                    type: "user_forget_password",
                                    data: {}
                                }
                            ];
                            db("events")
                                .insert(storable)
                                .catch((err) => {
                                    console.log(err);
                                });

                            // console.log(pass)
                            resolve(pass);
                        })
                        .catch((err) => {
                            // console.log(err)
                            reject(err);
                        });
                })
                .catch((err) => {
                    console.log(err);
                    reject(err);
                });
        } catch (err) {
            console.log(err);
            reject("Required params not present");
        }
    });
};

const verify_forget_password_verification_code = (pass) => {
    console.log("verify_forget_password_verification_code()");
    // 1. Get appropriate parameters from verification code
    // 2. Check with DB to see if parameters correct

    return new Promise((resolve, reject) => {
        try {
            let forget_password_verification_code = pass.forget_password_verification_code;

            let username = forget_password_verification_code.split("_")[0];
            let time_stamp = parseInt(forget_password_verification_code.split("_")[2]);
            let time_now = parseInt(new Date().getTime());

            console.log(time_stamp);
            console.log(time_now);

            if (time_now > time_stamp + 1000 * 60 * 10) {
                console.log("code FAILED - expired");
                reject({
                    success: false,
                    err:
                        "Reset password link expired. Please reset password within 10 mins of receiving the email link",
                    err_code: "reset_password_code_expired"
                });
            } else {
                console.log("code OK - not expired");

                db.from("users")
                    .select("*")
                    .where({ email_verified: true })
                    .where({ username_norm: global_helpers.normalise_string(username) })
                    .then((rows) => {
                        try {
                            if (rows[0].forget_password_verification_code === forget_password_verification_code) {
                                resolve(pass);
                            } else {
                                reject({
                                    success: false,
                                    err: "Verification code invalid",
                                    err_code: "reset_password_code_invalid"
                                });
                            }
                        } catch (err) {
                            console.log(err);
                            reject({
                                success: false,
                                err: "Verification code invalid",
                                err_code: "reset_password_code_invald"
                            });
                        }
                    })
                    .catch((err) => {
                        console.log(err);
                        reject(err);
                    });
            }
        } catch (err) {
            console.log(err);
            reject("Required params not present");
        }
    });
};

////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Verify /////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

const verify_token = (pass) => {
    console.log("verify_token()");
    // 1. Verifies token using jwt.verify()
    // 2. Get username from token using get_user_login_from_token()

    return new Promise((resolve, reject) => {
        try {
            // const bearerHeader = pass.token;

            jwt.verify(pass.token, TOKEN_PRIVATE_KEY, (err, authData) => {
                if (err) {
                    if (err && err.name == "TokenExpiredError") {
                        reject({
                            success: false,
                            err: "Session has expired. Please re-login and try again.",
                            err_code: "verify_token_expired"
                        });
                    } else {
                        reject(err);
                    }
                } else {
                    console.log("Authorized access by token. Now Getting username from token");

                    db.from("events")
                        .select("*")
                        .whereRaw(`data ->> 'token' = '${pass.token}'`)
                        .then((rows) => {
                            try {
                                if (rows == null || rows.length == 0) {
                                    console.log("Aww man... cannot find");
                                    return reject({
                                        success: false,
                                        err: "Unable to verify token",
                                        err_code: "token_invalid"
                                    });
                                } else {
                                    pass.verify_token_username = rows[0].ref_username_norm;
                                    console.log(pass.verify_token_username);
                                    db.from("users")
                                        .select("*")
                                        .where({ username_norm: rows[0].ref_username_norm })
                                        .then((result) => {
                                            console.log(result);
                                            pass.affiliates = result[0].affiliates;
                                            return resolve(pass);
                                        });
                                    // delete pass.get_user_login_from_token_result
                                }
                            } catch (err) {
                                console.log(err);
                                reject({
                                    success: false,
                                    err: "Unable to verify token",
                                    err_code: "token_invalid"
                                });
                            }
                        })
                        .catch((err) => {
                            console.log(err);
                            reject(err);
                        });
                }
            });
        } catch (err) {
            console.log(err);
            reject("Required params not present");
        }
    });
};

////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Verify /////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

module.exports = {
    signup,
    verify_credentials,
    sign_token,
    // store_token,
    verify_token,
    verify_email_verification_code,
    change_password,
    send_forget_password_email_link,
    verify_forget_password_verification_code
};
