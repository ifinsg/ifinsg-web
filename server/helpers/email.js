const fs = require("fs");
const readline = require("readline");
const { google } = require("googleapis");

const global_constants = require("../../global/constants");
const global_helpers = require("../../global/helpers");

const GMAIL_SCOPES = ["https://mail.google.com/"]; // Scopes: https://developers.google.com/gmail/api/auth/scopes
const GMAIL_TOKEN_PATH = './email/token.json';

const sample_function = pass => {
  console.log("sample_function()");
  // 1. Get something
  // 2. Do something else

  return new Promise((resolve, reject) => {
    if (pass.sample_function) {
    } else {
      reject("Required params not present");
    }
  });
};

////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// Gmail ////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

const send_email_test = pass => {
  console.log("send_email()");
  return new Promise((resolve, reject) => {
    // Source: https://stackoverflow.com/questions/55546961/nodejs-google-api-send-email-doesnt-group-by-thread
    const sendObject = {};
    output.data.messages.map(each => {
      sendObject.threadId = each.threadId; // ThreadID is unique.
      each.payload.headers.map(head => {
        if (head.name === "From") {
          sendObject.replyTo = head.value;
        } else if (head.name === "Subject") {
          if (head.value.indexOf("Re:") >= 0) {
            sendObject.subject = head.value;
          } else {
            sendObject.subject = "Re: " + head.value;
          }
        } else if (head.name === "Message-Id") {
          sendObject.inReplyTo = head.value; // The last thread messageId is inserted into In-Reply-To tag
          sendObject.reference.push(head.value); // All the messageId are appended as part of References tag
        }
      });
    });

    const email_lines = [];
    email_lines.push("References:" + sendObject.reference.join(" "));
    email_lines.push("In-Reply-To:" + sendObject.inReplyTo);
    email_lines.push("Subject: " + sendObject.subject);
    email_lines.push("To:" + sendObject.replyTo);

    email_lines.push("");
    email_lines.push(req.body.body);

    const email = email_lines.join("\r\n").trim();

    let base64EncodedEmail = new Buffer(email).toString("base64");
    base64EncodedEmail = base64EncodedEmail
      .replace(/\+/g, "-")
      .replace(/\//g, "_");

    emailConfig.gmail.users.messages.send(
      {
        userId: "me",
        threadId: sendObject.threadId,
        resource: {
          raw: base64EncodedEmail
        }
      },
      async err => {
        if (err) {
          console.log("The Threads API returned an error: " + err);
        }
      }
    );

    resolve(pass);
  });
};

const send_email = pass => {
  console.log("send_email()");
  return new Promise((resolve, reject) => {
    try {
      // Require:
      //      pass.send_email_email_address
      //      pass.send_email_subject
      //      pass.send_email_message

      fs.readFile("./email/credentials.json", (err, content) => {
        // Load client secrets from a local file.
        if (err) {
          console.log(err);
          reject(err);
        } else {
          console.log("fs.readFile OK");

          authorize(JSON.parse(content), sendMessage); // Authorize a client with credentials, then call the Gmail API.
        }
      });

      function authorize(credentials, callback) {
        console.log("authorize()");

        const {
          client_secret,
          client_id,
          redirect_uris
        } = credentials.installed;
        const oAuth2Client = new google.auth.OAuth2(
          client_id,
          client_secret,
          redirect_uris[0]
        );

        // Check if we have previously stored a token.
        fs.readFile(GMAIL_TOKEN_PATH, (err, token) => {
          if (err) return getNewToken(oAuth2Client, callback);
          oAuth2Client.setCredentials(JSON.parse(token));
          callback(oAuth2Client);
        });
      }

      function getNewToken(oAuth2Client, callback) {
        const authUrl = oAuth2Client.generateAuthUrl({
          access_type: "offline",
          scope: GMAIL_SCOPES
        });
        console.log("Authorize this app by visiting this url:", authUrl);
        const rl = readline.createInterface({
          input: process.stdin,
          output: process.stdout
        });
        rl.question("Enter the code from that page here: ", code => {
          rl.close();
          oAuth2Client.getToken(code, (err, token) => {
            if (err) return console.error("Error retrieving access token", err);
            oAuth2Client.setCredentials(token);
            // Store the token to disk for later program executions
            fs.writeFile(GMAIL_TOKEN_PATH, JSON.stringify(token), err => {
              if (err) return console.error(err);
              console.log("Token stored to", GMAIL_TOKEN_PATH);
            });
            callback(oAuth2Client);
          });
        });
      }

      function makeBody(to, from, subject, message) {
        var str = [
          'Content-Type: text/html; charset="UTF-8"\n',
          "MIME-Version: 1.0\n",
          "Content-Transfer-Encoding: 7bit\n",
          "to: ",
          to,
          "\n",
          "from: ",
          from,
          "\n",
          "subject: ",
          subject,
          "\n\n",
          message
        ].join("");

        var encodedMail = new Buffer(str)
          .toString("base64")
          .replace(/\+/g, "-")
          .replace(/\//g, "_");
        return encodedMail;
      }

      function sendMessage(auth) {
        var raw = makeBody(
          pass.send_email_email_address,
          pass.send_email_email_address,
          pass.send_email_subject,
          pass.send_email_message
        );
        const gmail = google.gmail({ version: "v1", auth });

        gmail.users.messages.send(
          {
            auth: auth,
            userId: "me",
            resource: {
              raw: raw
            }
          },
          function(err, response) {
            if (err) {
              console.log(err);
              console.log("error :(");
              reject(err);
            } else {
              console.log("Email OK");
              resolve(pass);
            }
          }
        );
      }
    } catch (err) {
      reject("Required params not present");
    }
  });
};

////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Verify /////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

module.exports = {
  send_email
};
