const email = require('./email');

const global_constants = require('../../global/constants');
const global_helpers = require('../../global/helpers');

const db = require('../helpers/pg').db;

const sample_function = (pass) => {
    console.log('sample_function()');
    // 1. Get something
    // 2. Do something else

    return new Promise((resolve, reject) => {
        try {
        } catch (err) {
            console.log(err);
            reject('Required params not present');
        }
    });
};

const generate_premiumback_table = (period) => {
    console.log('generate_premiumback_table()');
    period = {
        start: '2019-07-01',
        end: '2019-10-01',
        offset_hours: 8
    };

    const policy_configuration = {
        small_accident: {
            claim_cap: 1500, // $
            premium_3mth: 20.4, // $
            admin_fee_3mth: 9, // $
            premiumback_reserve_no_claims: 0.05, // %
            premiumback_reserve_low_claims: 0.05, // %
            low_claim_ceiling: 60, // $
            payment_fee_fixed: 0.5, // $
            payment_fee_percentage: 0.034 // %
        }
    };

    return new Promise((resolve, reject) => {
        try {
            let policies = [];

            db.raw(
                `
                SELECT
                    json_build_object(
                        'id', id,
                        'created_at', created_at,
                        'updated_at',updated_at,
                        'ref_username_norm',ref_username_norm,
                        'product',product,
                        'status_history', status_history, 
                        'product_details', product_details, 
                        'claims', (
                            SELECT json_agg(json_build_object(
                                'id', claims.id,
                                'created_at', claims.created_at,
                                'updated_at', claims.updated_at ,
                                'ref_username_norm', claims.ref_username_norm ,
                                'ref_policy_id', claims.ref_policy_id ,
                                'ref_accident_id', claims.ref_accident_id ,
                                'payout', claims.payout,
                                'status', claims.status ,
                                'status_history', claims.status_history
                            ))
                            FROM claims 
                            WHERE policies.id = claims.ref_policy_id
                        )
                    )
                FROM policies
                `
            )
                // WHERE policies.ref_username_norm = 'JIEXIONGKHOO@GMAIL.COM'

                .then((result) => {
                    console.log('Total policies found = ' + result.rows.length);
                    /////////////// Formatting DB results slightly into policies_with_nested_claims array ///////////////
                    let policies_with_nested_claims = [];
                    result.rows.forEach((ele) => {
                        policies_with_nested_claims.push(ele.json_build_object);
                    });

                    /////////////// Setting PremiumBack calculation parameters ///////////////
                    let policy_cycle = {
                        start: new Date(period.start).getTime() + period.offset_hours * 60 * 60 * 1000,
                        end: new Date(period.end).getTime() + period.offset_hours * 60 * 60 * 1000
                    };
                    let policy_cycle_duration = policy_cycle.end - policy_cycle.start;

                    let policy_config = policy_configuration.small_accident;

                    // console.log(policy_cycle)
                    // console.log(policy_cycle_duration)
                    // console.log(policy_config)

                    // Calculation variables

                    let premiumback_reserve_no_claims_credit_total_credited = 0;
                    let premiumback_reserve_no_claims_credit_total_uncredited = 0;
                    let premiumback_reserve_low_claims_credit_total_credited = 0;
                    let premiumback_reserve_low_claims_credit_total_uncredited = 0;
                    let total_premiums_paid = 0;
                    let total_admin_fee_paid = 0;
                    let total_payment_fees_paid = 0;
                    let total_claims_paid_out = 0;
                    let total_premiumback_reserve_no_claims = 0;
                    let total_premiumback_reserve_low_claims = 0;
                    let total_unused_operational_premiums = 0;
                    let total_premiumback_standard_adjusted_individual_loss_from_ceiling = 0;
                    let total_premiumback_entitled = 0;
                    let ifinsg_gross_profit_without_payment_fee = 0;
                    let ifinsg_gross_profit = 0;

                    /////////////// 1. Iterate through each policy (all, no matter what status) ///////////////
                    for (let i = 0; i < policies_with_nested_claims.length; i++) {
                        let policy = policies_with_nested_claims[i];
                        let active_periods = [];
                        let seconds_active = 0;
                        let participation_ratio = 0;
                        let claims_paid_out = 0;

                        // console.log(policy)
                        console.log('policy');

                        /////////////// 2. For each policy, go through policy_history and see when active, paused, reinstated ///////////////
                        for (let j = policy.status_history.length - 1; j >= 0; j--) {
                            // iterating backwards, starting from the earliest status_change
                            let status_change = policy.status_history[j];
                            if (status_change.status === 'active') {
                                // Start of a period
                                let active_period = {
                                    start: status_change.created_at,
                                    end: null,
                                    duration: null
                                };
                                active_periods.push(active_period);
                            } else if (status_change.status === 'cancellation_requested') {
                                // Start of a period
                                let last_ele = active_periods[active_periods.length - 1];
                                last_ele.end = status_change.created_at;
                                last_ele.duration =
                                    new Date(last_ele.end).getTime() - new Date(last_ele.start).getTime();
                                // last_ele.mins = last_ele.duration / 1000 / 60
                            }
                        }

                        /////////////// Get proportion of time active in policy cycle ///////////////

                        let last_ele = active_periods[active_periods.length - 1];

                        if (active_periods.length === 1 && last_ele.duration === null) {
                            // If

                            if (new Date(last_ele.start) < new Date(policy_cycle.start)) {
                                // If policy has been active since before the start of the cycle
                                participation_ratio = 1;
                            } else {
                                seconds_active =
                                    new Date(policy_cycle.end).getTime() - new Date(last_ele.start).getTime();
                                participation_ratio = seconds_active / policy_cycle_duration;
                            }

                            console.log('participation_ratio = 1');
                        } else {
                            for (let k = 0; k < active_periods.length; k++) {
                                if (active_periods[k].duration) {
                                    // If there was a period of activity which started and stopped, add to seconds_active
                                    seconds_active = seconds_active + active_periods[k].duration;
                                } else if (k === active_periods.length - 1 && active_periods[k].duration === null) {
                                    // If policy still active now, add that time to end of policy_cycle
                                    seconds_active =
                                        seconds_active + policy_cycle.end - new Date(active_periods[k].start).getTime();
                                }
                            }
                            participation_ratio = seconds_active / policy_cycle_duration;
                        }

                        // console.log("participation_ratio - " + participation_ratio)

                        if (!participation_ratio) {
                            // If policy has never been active before, dont include it in premiumback calculations
                            console.log(
                                'PARTICIPATION RATIO IS 0 - This policy will not be include in premiumback calculations'
                            );
                            continue;
                        } else {
                            console.log('PARTICIPATION RATIO IS NOTTTTTTTTTTT 0 - ');
                        }

                        /////////////// Get total claims made ///////////////

                        // console.log("policies_with_nested_claims START")
                        // console.log(policies_with_nested_claims)
                        // console.log("policies_with_nested_claims END")

                        let claims = policy.claims;
                        if (claims) {
                            for (let x = 0; x < claims.length; x++) {
                                if (
                                    claims[x].status === 'approved' &&
                                    claims[x].payout !== null &&
                                    claims[x].payout > 0.0 &&
                                    new Date(claims[x].created_at).getTime() > policy_cycle.start &&
                                    new Date(claims[x].created_at).getTime() < policy_cycle.end
                                ) {
                                    // If claim was made during the cycle period
                                    console.log(
                                        'CLAIM CLAIM CLAIM CLAIM CLAIM CLAIM CLAIM CLAIM CLAIM CLAIM  --- ' +
                                            claims[x].payout
                                    );

                                    claims_paid_out += claims[x].payout;
                                }
                            }
                        }

                        /////////////// 3. Get details - how much was paid for premium and admin fee ///////////////
                        let temp = {
                            id: policy.id,
                            premium_paid: policy.product_details.premium * participation_ratio * 3,
                            admin_fee_paid: policy.product_details.admin_fee * participation_ratio * 3,
                            claims_paid_out,
                            participation_ratio,
                            premiumback_standard_actual_individual_loss_from_ceiling:
                                claims_paid_out >= policy_config.low_claim_ceiling
                                    ? 0
                                    : -(policy_config.low_claim_ceiling - claims_paid_out)
                        };
                        temp.est_payment_fee_paid =
                            (temp.premium_paid + temp.admin_fee_paid) * policy_config.payment_fee_percentage +
                            policy_config.payment_fee_fixed;

                        temp.premiumback_standard_adjusted_individual_loss_from_ceiling =
                            temp.premiumback_standard_actual_individual_loss_from_ceiling * temp.participation_ratio;

                        total_premiumback_standard_adjusted_individual_loss_from_ceiling +=
                            temp.premiumback_standard_adjusted_individual_loss_from_ceiling;
                        total_premiums_paid += temp.premium_paid;
                        total_admin_fee_paid += temp.admin_fee_paid;
                        total_payment_fees_paid += temp.est_payment_fee_paid;
                        total_claims_paid_out += temp.claims_paid_out;

                        /////////////// Calc Premiumback Reserve ///////////////
                        temp.premiumback_reserve_no_claims_supposed =
                            temp.premium_paid * policy_config.premiumback_reserve_no_claims;
                        if (temp.claims_paid_out < 0.001) {
                            temp.premiumback_reserve_no_claims_credit = temp.premiumback_reserve_no_claims_supposed;
                            premiumback_reserve_no_claims_credit_total_credited +=
                                temp.premiumback_reserve_no_claims_credit;
                        } else {
                            temp.premiumback_reserve_no_claims_credit = 0;
                            premiumback_reserve_no_claims_credit_total_uncredited +=
                                temp.premiumback_reserve_no_claims_supposed;
                        }
                        temp.premiumback_reserve_no_claims_actual = null;

                        temp.premiumback_reserve_low_claims_supposed =
                            temp.premium_paid * policy_config.premiumback_reserve_low_claims;
                        if (temp.claims_paid_out <= policy_config.low_claim_ceiling) {
                            temp.premiumback_reserve_low_claims_credit = temp.premiumback_reserve_low_claims_supposed;
                            premiumback_reserve_low_claims_credit_total_credited +=
                                temp.premiumback_reserve_low_claims_supposed;
                        } else {
                            temp.premiumback_reserve_low_claims_credit = 0;
                            premiumback_reserve_low_claims_credit_total_uncredited +=
                                temp.premiumback_reserve_low_claims_credit;
                        }
                        temp.premiumback_reserve_low_claims_actual = null;

                        console.log(temp);
                        console.log('temp temp temp temp temp temp temp temp temp temp temp temp ');

                        policy.premiumback_table_data = temp;
                        policies.push(policy);
                    }

                    /////////////// Overall calculations ///////////////
                    total_premiumback_reserve_no_claims =
                        total_premiums_paid * policy_config.premiumback_reserve_no_claims;
                    total_premiumback_reserve_low_claims =
                        total_premiums_paid * policy_config.premiumback_reserve_low_claims;

                    total_unused_operational_premiums =
                        total_premiums_paid -
                        total_claims_paid_out -
                        total_premiumback_reserve_no_claims -
                        total_premiumback_reserve_low_claims;

                    // Cater for TOO MUCH claims - If total_unused_operational_premiums < 0, make it 0. Premiumback system cannot take more money from customers
                    if (total_unused_operational_premiums < 0) {
                        total_unused_operational_premiums = 0;
                    }

                    policies.forEach((policy) => {
                        /////////////// Calc Premiumback Reserve Actual - by crediting total uncredited ///////////////
                        let no_claims_credit = policy.premiumback_table_data.premiumback_reserve_no_claims_credit;
                        if (premiumback_reserve_no_claims_credit_total_credited === 0) {
                            policy.premiumback_table_data.premiumback_reserve_no_claims_actual = 0;
                        } else if (premiumback_reserve_no_claims_credit_total_uncredited === 0) {
                            policy.premiumback_table_data.premiumback_reserve_no_claims_actual = no_claims_credit;
                        } else {
                            policy.premiumback_table_data.premiumback_reserve_no_claims_actual =
                                (no_claims_credit / premiumback_reserve_no_claims_credit_total_credited) *
                                premiumback_reserve_no_claims_credit_total_uncredited;
                        }

                        let low_claims_credit = policy.premiumback_table_data.premiumback_reserve_low_claims_credit;
                        if (premiumback_reserve_low_claims_credit_total_credited === 0) {
                            policy.premiumback_table_data.premiumback_reserve_low_claims_actual = 0;
                        } else if (premiumback_reserve_low_claims_credit_total_uncredited === 0) {
                            policy.premiumback_table_data.premiumback_reserve_low_claims_actual = low_claims_credit;
                        } else {
                            policy.premiumback_table_data.premiumback_reserve_low_claims_actual =
                                (low_claims_credit / premiumback_reserve_low_claims_credit_total_credited) *
                                premiumback_reserve_low_claims_credit_total_uncredited;
                        }

                        /////////////// Calc distributed unused operational premiums ///////////////
                        policy.premiumback_table_data.distributed_unused_operational_premiums =
                            total_premiumback_standard_adjusted_individual_loss_from_ceiling == 0
                                ? 0
                                : (policy.premiumback_table_data
                                      .premiumback_standard_adjusted_individual_loss_from_ceiling /
                                      total_premiumback_standard_adjusted_individual_loss_from_ceiling) *
                                  total_unused_operational_premiums;

                        policy.premiumback_table_data.premiumback_entitlement =
                            policy.premiumback_table_data.premiumback_reserve_no_claims_actual +
                            policy.premiumback_table_data.premiumback_reserve_low_claims_actual +
                            policy.premiumback_table_data.distributed_unused_operational_premiums;
                        total_premiumback_entitled += policy.premiumback_table_data.premiumback_entitlement;
                        policy.premiumback_table_data.net_profit_without_admin_fee =
                            -policy.premiumback_table_data.premium_paid +
                            policy.premiumback_table_data.claims_paid_out +
                            policy.premiumback_table_data.premiumback_entitlement;
                        policy.premiumback_table_data.net_profit =
                            policy.premiumback_table_data.net_profit_without_admin_fee -
                            policy.premiumback_table_data.admin_fee_paid;
                        ifinsg_gross_profit_without_payment_fee -= policy.premiumback_table_data.net_profit;
                        ifinsg_gross_profit = ifinsg_gross_profit_without_payment_fee - total_payment_fees_paid;
                    });

                    /////////////// SUMMARY ///////////////
                    policies.forEach((policy) => {
                        console.log(policy.premiumback_table_data);
                    });

                    // REPORTING
                    // console.log("premiumback_reserve_no_claims_credit_total_credited - " + premiumback_reserve_no_claims_credit_total_credited)
                    // console.log("premiumback_reserve_no_claims_credit_total_uncredited - " + premiumback_reserve_no_claims_credit_total_uncredited)

                    // console.log("premiumback_reserve_low_claims_credit_total_credited - " + premiumback_reserve_low_claims_credit_total_credited)
                    // console.log("premiumback_reserve_low_claims_credit_total_uncredited - " + premiumback_reserve_low_claims_credit_total_uncredited)

                    // console.log("total_premiums_paid - " + total_premiums_paid)
                    // console.log("total_admin_fee_paid - " + total_admin_fee_paid)
                    // console.log("total_payment_fees_paid - " + total_payment_fees_paid)
                    // console.log("total_unused_operational_premiums - " + total_unused_operational_premiums)
                    // console.log("total_premiumback_standard_adjusted_individual_loss_from_ceiling - " + total_premiumback_standard_adjusted_individual_loss_from_ceiling)
                    // console.log("total_premiumback_entitled - " + total_premiumback_entitled)

                    // console.log("ifinsg_gross_profit_without_payment_fee - " + ifinsg_gross_profit_without_payment_fee)
                    // console.log("ifinsg_gross_profit - " + ifinsg_gross_profit)

                    console.log(policies.length + ' policies found');
                    // resolve(policies_with_nested_claims)

                    let columns = [
                        { name: 'Policy ID', options: { filter: false, sort: true } },
                        { name: 'Premium Paid', options: { filter: true, sort: true } },
                        { name: 'Total Claims Made', options: { filter: true, sort: true } },
                        { name: 'Premiumback Reserve NO Claims Supposed', options: { filter: true, sort: true } },
                        { name: 'Premiumback Reserve NO Claims Credit', options: { filter: true, sort: true } },
                        { name: 'Premiumback Reserve NO Claims Actual', options: { filter: true, sort: true } },
                        { name: 'Premiumback Reserve LOW Claims Supposed', options: { filter: true, sort: true } },
                        { name: 'Premiumback Reserve LOW Claims Credit', options: { filter: true, sort: true } },
                        { name: 'Premiumback Reserve LOW Claims Actual', options: { filter: true, sort: true } },
                        { name: 'Premiumback Standard Participation Ratio', options: { filter: true, sort: true } },
                        {
                            name: 'Premiumback Standard Actual Indiv Loss From $60',
                            options: { filter: true, sort: true }
                        },
                        {
                            name: 'Premiumback Standard Adjusted Individual Loss from $60',
                            options: { filter: true, sort: true }
                        },
                        {
                            name: 'Premiumback Standard Distributed Unused Operational premiums',
                            options: { filter: true, sort: true }
                        },
                        { name: 'Total PremiumBack returned', options: { filter: true, sort: true } },
                        { name: 'Policy Net Profit Without Admin Fee', options: { filter: true, sort: true } },
                        { name: 'Policy Net Profit', options: { filter: true, sort: true } },
                        { name: 'Admin Fee', options: { filter: true, sort: true } },
                        { name: 'Payment Fee', options: { filter: true, sort: true } }
                    ];

                    let data = [];

                    policies.forEach((policy) => {
                        let policy_data = policy.premiumback_table_data;
                        let row = [
                            policy.id,
                            policy_data.premium_paid,
                            policy_data.claims_paid_out,
                            policy_data.premiumback_reserve_no_claims_supposed,
                            policy_data.premiumback_reserve_no_claims_credit,
                            policy_data.premiumback_reserve_no_claims_actual,
                            policy_data.premiumback_reserve_low_claims_supposed,
                            policy_data.premiumback_reserve_low_claims_credit,
                            policy_data.premiumback_reserve_low_claims_actual,
                            policy_data.participation_ratio,
                            policy_data.premiumback_standard_actual_individual_loss_from_ceiling,
                            policy_data.premiumback_standard_adjusted_individual_loss_from_ceiling,
                            policy_data.distributed_unused_operational_premiums,
                            policy_data.premiumback_entitlement,
                            policy_data.net_profit_without_admin_fee,
                            policy_data.net_profit,
                            policy_data.admin_fee_paid,
                            policy_data.est_payment_fee_paid
                        ];
                        let row2 = row.map((item) => global_helpers.make_financial(item));
                        row2[0] = policy.id;

                        data.push(row2);
                    });

                    let summary_table_data = {
                        columns: [
                            { name: 'Col 1', options: { filter: false, sort: false } },
                            { name: 'Col 2', options: { filter: false, sort: false } }
                        ],
                        data: [
                            ['Total Premiums Collected', global_helpers.make_financial(total_premiums_paid)],
                            ['Total Admin Fee Collected', global_helpers.make_financial(total_admin_fee_paid)],
                            ['Total Stripe Fee Collected', global_helpers.make_financial(total_payment_fees_paid)],
                            ['Total Claims Paid', global_helpers.make_financial(total_claims_paid_out)],
                            [
                                'PremiumBack Reserve - NO Claims',
                                global_helpers.make_financial(total_premiumback_reserve_no_claims)
                            ],
                            [
                                'PremiumBack Reserve - LOW Claims',
                                global_helpers.make_financial(total_premiumback_reserve_low_claims)
                            ],
                            [
                                'Unused Operational Premiums',
                                global_helpers.make_financial(total_unused_operational_premiums)
                            ],
                            ['PremiumBack To Pay', global_helpers.make_financial(total_premiumback_entitled)],
                            [
                                'iFinSG Gross Profit - without payment fee',
                                global_helpers.make_financial(ifinsg_gross_profit_without_payment_fee)
                            ],
                            ['iFinSG Gross Profit', global_helpers.make_financial(ifinsg_gross_profit)]
                        ]
                    };

                    resolve({ columns, data, summary_table_data });
                })
                .catch((err) => {
                    console.log(err);
                    reject('Error occured');
                });
        } catch (err) {
            console.log(err);
            reject('Required params not present');
        }
    });
};

////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// User Actions /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

const get_table_data = (pass) => {
    console.log('SERVER: get_table_data()');

    return new Promise((resolve, reject) => {
        try {
            // Initialising default response. To be overridden
            let title = 'Table Title';
            let columns = [
                { name: 'id', options: { filter: false, sort: true } },
                { name: 'Policy', options: { filter: true, sort: true } },
                { name: 'Status', options: { filter: true, sort: true } },

                { name: 'Username', options: { filter: false, sort: true } },
                { name: 'Purchased for', options: { filter: true, sort: true } },
                { name: 'Buyer', options: { filter: true, sort: true } },
                { name: 'Buyer Pref Name', options: { filter: true, sort: true } },
                { name: 'Insured', options: { filter: false, sort: true } },
                { name: 'Insured Pref Name', options: { filter: false, sort: true } },
                { name: 'NRIC', options: { filter: false, sort: true } },
                { name: 'DOB', options: { filter: false, sort: false } },
                { name: 'Email', options: { filter: false, sort: true } },
                { name: 'Gender', options: { filter: true, sort: true } },
                { name: 'Mobile', options: { filter: false, sort: true } },
                { name: 'Occupation', options: { filter: false, sort: true } },
                { name: 'Self-emp', options: { filter: true, sort: true } },
                { name: 'Place of Birth', options: { filter: true, sort: true } },

                { name: 'Referrer', options: { filter: true, sort: true } },
                { name: 'Source', options: { filter: true, sort: true } },

                { name: 'R/s to Payer', options: { filter: true, sort: true } },
                { name: 'Purchase Date', options: { filter: true, sort: true } },

                { name: 'Bank', options: { filter: true, sort: true } },
                // { name: "Bank Acct No", options: { filter: false, sort: true, } },
                { name: 'Bank (claims)', options: { filter: true, sort: true } },
                // { name: "Bank Acct No (claims)", options: { filter: false, sort: true, } },
                { name: 'Bank (premiumback)', options: { filter: true, sort: true } }
                // { name: "Bank Acct No (premiumback)", options: { filter: false, sort: true, } },
            ];
            let data = [];
            let data_json_array = [];
            let data_json_array_split = [];

            let table = pass.table;
            let table_variant = pass.table_variant;

            const events_data_to_row = (event, i, length) => {
                return [
                    event.id,
                    event.ref_policy_id,
                    event.ref_accident_id,
                    event.ref_claim_id,
                    event.type,
                    event.data.username_user ? event.data.username_user : event.ref_username_norm, // username_user
                    event.data.username_staff
                        ? event.data.username_staff
                        : event.data.username_user
                        ? event.ref_username_norm
                        : event.data.username_user, // username_staff
                    global_constants.mode_dev
                        ? global_helpers.displayDate(event.created_at, 'yyyy-MM-dd HH:mm:ss')
                        : global_helpers.displayDate(event.created_at, 'yyyy-MM-dd HH:mm:ss', 8 * 60 * 60 * 1000), // This is in GMT+0
                    JSON.stringify(event.data)
                ];
            };

            return new Promise((resolve, reject) => {
                switch (table) {
                    case 'premiumback':
                        generate_premiumback_table()
                            .then((result) => {
                                columns = result.columns;
                                data = result.data;
                                data_json_array_split.push(result.summary_table_data);
                                resolve();
                            })
                            .catch((err) => {
                                console.log(err);
                                reject(err);
                            });
                        // db.select(db.raw(`to_json(policies.*) as policy `), db.raw('to_json(users.*) as user'))
                        //     .from('policies')
                        //     .where({ flag_staff_inbox_category: "active" })
                        //     .where(function (builder) {
                        //         if (pass.date_start) { builder.andWhere('created_at', '>=', pass.date_start) }
                        //         if (pass.date_end) { builder.andWhere('created_at', '<', pass.date_end) }
                        //     }).limit(pass.limit)
                        //     .join('users', 'users.username_norm', 'policies.ref_username_norm')
                        //     .then(rows => {

                        //         let policies = rows

                        //         title = "CMS"
                        //         // columns = default

                        //         if (policies.length !== 0) {
                        //             for (let i = 0; i < policies.length; i++) {
                        //                 let policy = policies[i]
                        //                 let policy_json_and_array = global_helpers.policy_data_to_json_and_array(policy, i)
                        //                 data.push(policy_json_and_array.array)
                        //                 data_json_array.push(policy_json_and_array.json)
                        //             }
                        //             data_json_array_split = policies

                        //             // data_json_array = policies
                        //         }
                        //         resolve()
                        //     }).catch(err => {
                        //         console.log(err)
                        //         reject(err)
                        //     })
                        break;

                    case 'cms':
                        db.select(db.raw(`to_json(policies.*) as policy `), db.raw('to_json(users.*) as user'))
                            .from('policies')
                            .where({ flag_staff_inbox_category: 'active' })
                            .where(function(builder) {
                                if (pass.date_start) {
                                    builder.andWhere('created_at', '>=', pass.date_start);
                                }
                                if (pass.date_end) {
                                    builder.andWhere('created_at', '<', pass.date_end);
                                }
                            })
                            .limit(pass.limit)
                            .join('users', 'users.username_norm', 'policies.ref_username_norm')
                            .then((rows) => {
                                let policies = rows;

                                title = 'CMS';
                                // columns = default

                                if (policies.length !== 0) {
                                    for (let i = 0; i < policies.length; i++) {
                                        let policy = policies[i];
                                        let policy_json_and_array = global_helpers.policy_data_to_json_and_array(
                                            policy,
                                            i
                                        );
                                        data.push(policy_json_and_array.array);
                                        data_json_array.push(policy_json_and_array.json);
                                    }
                                    data_json_array_split = policies;

                                    // data_json_array = policies
                                }
                                resolve();
                            })
                            .catch((err) => {
                                console.log(err);
                                reject(err);
                            });
                        break;

                    case 'history':
                        db.from('events')
                            .select('*')
                            .where(function(builder) {
                                builder.whereNot({ id: null });
                                if (pass.date_start) {
                                    builder.andWhere('created_at', '>=', pass.date_start);
                                }
                                if (pass.date_end) {
                                    builder.andWhere('created_at', '<', pass.date_end);
                                }
                            })
                            .orderBy('created_at', 'desc')
                            .limit(pass.limit)
                            .then((rows) => {
                                let events = rows;

                                title = 'History of Events';
                                // columns = default
                                columns = [
                                    { name: 'id', options: { filter: false, sort: true } },
                                    { name: 'Policy ID', options: { filter: false, sort: true } },
                                    { name: 'Accident ID', options: { filter: false, sort: true } },
                                    { name: 'Claim ID', options: { filter: false, sort: true } },
                                    { name: 'Type', options: { filter: true, sort: true } },
                                    { name: 'Username', options: { filter: false, sort: true } },
                                    { name: 'Staff', options: { filter: false, sort: true } },
                                    { name: 'Created at', options: { filter: false, sort: false } },
                                    { name: 'Data', options: { filter: false, sort: false } }
                                ];

                                if (events.length !== 0) {
                                    for (let i = 0; i < events.length; i++) {
                                        let event = events[i];
                                        let row = events_data_to_row(event, i, events.length);
                                        data.push(row);
                                    }
                                    data_json_array = events;
                                }
                                resolve();
                            })
                            .catch((err) => {
                                console.log(err);
                                reject(err);
                            });
                        break;

                    case 'staff_inbox':
                        if (!table_variant) {
                            console.log('ERROR - table_variant not defined');
                            break;
                        }

                        db.select(db.raw(`to_json(policies.*) as policy `), db.raw('to_json(users.*) as user'))
                            .from('policies')
                            .where({ flag_staff_inbox_category: table_variant })
                            .where(function(builder) {
                                if (pass.date_start) {
                                    builder.andWhere('created_at', '>=', pass.date_start);
                                }
                                if (pass.date_end) {
                                    builder.andWhere('created_at', '<', pass.date_end);
                                }
                            })
                            .limit(pass.limit)
                            .join('users', 'users.username_norm', 'policies.ref_username_norm')
                            .then((rows) => {
                                let policies = rows;

                                switch (table_variant) {
                                    case 'pending_approval':
                                        title = 'Pending Approval Signups';
                                        break;
                                    case 'rework':
                                        title = 'Rework Signups';
                                        break;
                                    case 'pending_reinstatement':
                                        title = 'Pending Reinstatement';
                                        break;
                                    case 'pending_cancel':
                                        title = 'Pending Cancel Approval';
                                        break;
                                    case 'cancelled':
                                        title = 'Cancelled Signups';
                                        break;
                                    case 'rejected':
                                        title = 'Rejected Signups';
                                        break;
                                    default:
                                        title = 'Staff Inbox';
                                        break;
                                }
                                // columns = default

                                for (let i = 0; i < policies.length; i++) {
                                    let policy = policies[i];
                                    let policy_json_and_array = global_helpers.policy_data_to_json_and_array(policy, i);
                                    data.push(policy_json_and_array.array);
                                    data_json_array.push(policy_json_and_array.json);
                                }
                                data_json_array_split = policies;

                                resolve();
                            })
                            .catch((err) => {
                                console.log(err);
                                reject(err);
                            });
                        break;
                    case 'claims':
                        if (!table_variant) {
                            console.log('ERROR - table_variant not defined');
                            break;
                        }

                        let where_filter = { status: table_variant };
                        console.log(where_filter);
                        db('claims')
                            .select('*')
                            .where(function(builder) {
                                builder.andWhere(where_filter);
                                if (table_variant === 'rework') {
                                    builder.orWhere({ status: 'pending_rework' });
                                    builder.orWhere({ status: 'pending_rework_approval' });
                                }
                            })
                            .then((rows) => {
                                let claims = rows;

                                switch (table_variant) {
                                    case 'pending_approval':
                                        title = 'Pending Approval Claims';
                                        break;
                                    case 'rework':
                                        title = 'Rework Claims';
                                        break;
                                    case 'cancelled':
                                        title = 'Cancelled Claims';
                                        break;
                                    case 'rejected':
                                        title = 'Rejected Claims';
                                        break;
                                    case 'approved':
                                        title = 'Approved Claims';
                                        break;
                                    default:
                                        title = 'Claims';
                                        break;
                                }
                                // columns = default
                                columns = [
                                    { name: 'id', options: { filter: false, sort: true } },
                                    { name: 'Date Submitted', options: { filter: false, sort: false } },
                                    { name: 'Username', options: { filter: true, sort: true } },
                                    { name: 'Policy ID', options: { filter: true, sort: true } },
                                    { name: 'Accident ID', options: { filter: true, sort: true } },

                                    { name: 'Medical RegistrarID', options: { filter: true, sort: true } },
                                    { name: 'Clinic Visit Date', options: { filter: true, sort: true } },
                                    { name: 'Doctor Seen', options: { filter: false, sort: true } },
                                    { name: 'Clinic Visited', options: { filter: false, sort: true } },
                                    { name: 'Treatment Description', options: { filter: false, sort: false } },

                                    { name: 'Uploaded Images', options: { filter: false, sort: false } }
                                ];

                                for (let i = 0; i < claims.length; i++) {
                                    let claim = claims[i];
                                    let claim_json_and_array = global_helpers.claim_data_to_json_and_array(claim, i);
                                    data.push(claim_json_and_array.array);
                                    data_json_array.push(claim_json_and_array.json);
                                }
                                data_json_array_split = claims;

                                resolve();
                            })
                            .catch((err) => {
                                console.log(err);
                                reject(err);
                            });
                        break;

                    case 'tracking':
                        console.log('TRACKING');
                        db('tracking')
                            .select('*')
                            .then((rows) => {
                                // columns = default
                                columns = [
                                    { name: 'id', options: { filter: false, sort: true } },
                                    { name: 'Date Submitted', options: { filter: false, sort: false } },

                                    { name: 'Homepage Variant', options: { filter: true, sort: true } },
                                    { name: 'Button', options: { filter: true, sort: true } },
                                    { name: 'Title', options: { filter: true, sort: true } }
                                ];

                                for (let i = 0; i < rows.length; i++) {
                                    let tracker = rows[i];
                                    console.log(tracker);

                                    let row_array = [
                                        tracker.id,
                                        tracker.created_at,
                                        tracker.data.homepage_variant,
                                        tracker.data.button,
                                        tracker.data.title
                                    ];
                                    console.log(row_array);

                                    data.push(row_array);
                                }
                                data_json_array_split = rows;

                                resolve();
                            })
                            .catch((err) => {
                                console.log(err);
                                reject(err);
                            });
                        break;
                    case 'affiliates':
                        db.select(db.raw(`to_json(policies.*) as policy `), db.raw('to_json(users.*) as user'))
                            .from('policies')
                            .where(function(builder) {
                                builder.whereNot({ 'policies.id': null });
                                if (pass.date_start) {
                                    builder.andWhere('policies.created_at', '>=', pass.date_start);
                                }
                                if (pass.date_end) {
                                    builder.andWhere('policies.created_at', '<', pass.date_end);
                                }
                            })
                            .orderBy('policies.id', 'desc')
                            .limit(pass.limit)
                            .join('users', 'users.username_norm', 'policies.ref_username_norm')
                            .then((rows) => {
                                let policies = rows;

                                // firestore.get_data(pass).then((pass) => {
                                // console.log(pass.get_data_result)
                                title = 'Affiliates';
                                if (pass.affiliates && pass.affiliates !== 'all') {
                                    columns = [
                                        { name: 'Policy ID', options: { filter: false, sort: true } },
                                        // "id",
                                        {
                                            name: 'Referrer',
                                            options: { filter: true, sort: true, filterType: 'dropdown' }
                                        },
                                        {
                                            name: 'Source',
                                            options: { filter: true, sort: true, filterType: 'dropdown' }
                                        },

                                        'Purchased for',
                                        // {
                                        //     name: 'Buyer',
                                        //     options: { filter: true, sort: true, filterType: 'dropdown' }
                                        // },
                                        // {
                                        //     name: 'Insured',
                                        //     options: { filter: true, sort: true, filterType: 'dropdown' }
                                        // },
                                        'Policy',
                                        'Status',
                                        { name: 'Purchase Date', options: { filter: false, sort: false } }
                                    ];
                                }

                                if (pass.affiliates === 'all') {
                                    columns = [
                                        { name: 'Policy ID', options: { filter: false, sort: true } },
                                        // "id",
                                        {
                                            name: 'Referrer',
                                            options: { filter: true, sort: true, filterType: 'dropdown' }
                                        },
                                        {
                                            name: 'Source',
                                            options: { filter: true, sort: true, filterType: 'dropdown' }
                                        },

                                        {
                                            name: 'Username',
                                            options: { filter: true, sort: true, filterType: 'dropdown' }
                                        },
                                        'Purchased for',
                                        {
                                            name: 'Buyer',
                                            options: { filter: true, sort: true, filterType: 'dropdown' }
                                        },
                                        {
                                            name: 'Insured',
                                            options: { filter: true, sort: true, filterType: 'dropdown' }
                                        },
                                        'Policy',
                                        'Status',
                                        { name: 'Purchase Date', options: { filter: false, sort: false } }
                                    ];
                                }

                                if (policies.length !== 0) {
                                    for (let i = 0; i < policies.length; i++) {
                                        let policy = policies[i];
                                        if (pass.affiliates) {
                                            let row;
                                            let policy_json_and_array;
                                            if (policy.user.referrer == pass.affiliates) {
                                                row = [
                                                    policy.policy.id,
                                                    policy.user.referrer,
                                                    policy.policy.source,
                                                    policy.policy.representing,
                                                    // policy.policy.representing === 'self'
                                                    //     ? policy.user.preferred_name
                                                    //     : policy.user &&
                                                    //       policy.user.company_details &&
                                                    //       policy.user.company_details.name,
                                                    // policy.policy.representing === 'self'
                                                    //     ? policy.user.preferred_name
                                                    //     : policy.policy.full_name,
                                                    policy.policy.product,
                                                    policy.policy.status,
                                                    global_constants.mode_dev
                                                        ? new Date(new Date(policy.policy.created_at).getTime())
                                                              .toString()
                                                              .split('GMT')[0]
                                                        : new Date(
                                                              new Date(policy.policy.created_at).getTime() +
                                                                  8 * 60 * 60 * 1000
                                                          )
                                                              .toString()
                                                              .split('GMT')[0] // This is in GMT+0
                                                ];
                                                data.push(row);

                                                policy_json_and_array = global_helpers.policy_data_to_json_and_array(
                                                    policy,
                                                    i
                                                );
                                                data_json_array.push(policy_json_and_array.json);
                                                data_json_array_split = policies;
                                            } else if (pass.affiliates === 'all') {
                                                row = [
                                                    policy.policy.id,
                                                    policy.user.referrer,
                                                    policy.policy.source,
                                                    policy.policy.ref_username_norm,
                                                    policy.policy.representing,
                                                    policy.policy.representing === 'self' ||
                                                    policy.policy.representing == 'family'
                                                        ? policy.user.preferred_name
                                                        : policy.user &&
                                                          policy.user.company_details &&
                                                          policy.user.company_details.name,
                                                    policy.policy.representing === 'self'
                                                        ? policy.user.preferred_name
                                                        : policy.policy.preferred_name,
                                                    policy.policy.product,
                                                    policy.policy.status,
                                                    global_constants.mode_dev
                                                        ? new Date(new Date(policy.policy.created_at).getTime())
                                                              .toString()
                                                              .split('GMT')[0]
                                                        : new Date(
                                                              new Date(policy.policy.created_at).getTime() +
                                                                  8 * 60 * 60 * 1000
                                                          )
                                                              .toString()
                                                              .split('GMT')[0] // This is in GMT+0
                                                ];
                                                data.push(row);

                                                policy_json_and_array = global_helpers.policy_data_to_json_and_array(
                                                    policy,
                                                    i
                                                );
                                                data_json_array.push(policy_json_and_array.json);
                                                data_json_array_split = policies;
                                            }
                                        }
                                    }
                                }
                                resolve();
                            })
                            .catch((err) => {
                                console.log(err);
                                reject(err);
                            });
                        break;

                    case 'affiliates_passion':
                        db.select(db.raw(`to_json(policies.*) as policy `), db.raw('to_json(users.*) as user'))
                            .from('policies')
                            .where('policies.referrer', '=', 'passion')
                            .where(function(builder) {
                                builder.whereNot({ 'policies.id': null });
                                if (pass.date_start) {
                                    builder.andWhere('policies.created_at', '>=', pass.date_start);
                                }
                                if (pass.date_end) {
                                    builder.andWhere('policies.created_at', '<', pass.date_end);
                                }
                            })
                            .orderBy('policies.id', 'desc')
                            .limit(pass.limit)
                            .join('users', 'users.username_norm', 'policies.ref_username_norm')
                            .then((rows) => {
                                let policies = rows;

                                // firestore.get_data(pass).then((pass) => {
                                // console.log(pass.get_data_result)
                                title = 'Affiliates';
                                columns = [
                                    { name: 'Policy ID', options: { filter: false, sort: true } },
                                    // "id",
                                    { name: 'Referrer', options: { filter: true, sort: true, filterType: 'dropdown' } },
                                    { name: 'Source', options: { filter: true, sort: true, filterType: 'dropdown' } },

                                    { name: 'Username', options: { filter: true, sort: true, filterType: 'dropdown' } },
                                    'Purchased for',
                                    { name: 'Insured', options: { filter: true, sort: true, filterType: 'dropdown' } },
                                    'Policy',
                                    'Status',
                                    { name: 'Purchase Date', options: { filter: false, sort: false } }
                                ];

                                if (policies.length !== 0) {
                                    for (let i = 0; i < policies.length; i++) {
                                        let policy = policies[i];
                                        let row = [
                                            policy.policy.id,
                                            policy.policy.referrer,
                                            policy.policy.source,

                                            policy.policy.ref_username_norm,
                                            policy.policy.representing,
                                            policy.policy.full_name,
                                            policy.policy.product,
                                            policy.policy.status,
                                            global_constants.mode_dev
                                                ? new Date(new Date(policy.policy.created_at).getTime())
                                                      .toString()
                                                      .split('GMT')[0]
                                                : new Date(
                                                      new Date(policy.policy.created_at).getTime() + 8 * 60 * 60 * 1000
                                                  )
                                                      .toString()
                                                      .split('GMT')[0] // This is in GMT+0
                                        ];
                                        data.push(row);

                                        let policy_json_and_array = global_helpers.policy_data_to_json_and_array(
                                            policy,
                                            i
                                        );
                                        data_json_array.push(policy_json_and_array.json);
                                        data_json_array_split = policies;
                                    }
                                }
                                resolve();
                            })
                            .catch((err) => {
                                console.log(err);
                                reject(err);
                            });
                        break;
                    default:
                        reject('ERROR - Table not specified');
                        break;
                }
            })
                .then(() => {
                    pass.get_table_data_data = {
                        title,
                        data,
                        columns,
                        data_json_array,
                        data_json_array_split
                    };
                    // console.log(pass)
                    resolve(pass);
                })
                .catch((err) => {
                    console.log(err);
                    reject(err);
                });
        } catch (err) {
            console.log(err);
            reject('Failed to get_table_data()');
        }
    });
};

const get_accidents_and_claims = (pass) => {
    console.log('sample_function()');
    // 1. Get something
    // 2. Do something else

    return new Promise((resolve, reject) => {
        try {
            db.raw(
                `
            SELECT
                json_build_object(
                    'id', id,
                    'created_at', created_at,
                    'updated_at',updated_at,
                    'ref_username_norm',ref_username_norm,
                    'ref_policy_id',ref_policy_id,
                    'accident_description',accident_description,
                    'accident_date',accident_date,
                    'accident_type',accident_type,
                    'product',product,
                    'pending_rework_items',pending_rework_items,
                    'pending_rework_message',pending_rework_message,
                    'accident_details_verified',accident_details_verified,
                    'claims', (
                        SELECT json_agg(json_build_object(
                            'id', claims.id,
                            'created_at', claims.created_at,
                            'updated_at', claims.updated_at ,
                            'ref_username_norm', claims.ref_username_norm ,
                            'ref_policy_id', claims.ref_policy_id ,
                            'ref_accident_id', claims.ref_accident_id ,
                            'medical_registrar_smc', claims.medical_registrar_smc ,
                            'clinic_visit_date', claims.clinic_visit_date ,
                            'doctor_seen', claims.doctor_seen ,
                            'clinic_visited', claims.clinic_visited,
                            'treatment_description', claims.treatment_description ,
                            'uploaded_images', claims.uploaded_images,     
                            'addon_notes',addon_notes,
                            'status', claims.status ,
                            'status_history', claims.status_history ,
                            'pending_rework_items', claims.pending_rework_items, 
                            'pending_rework_message', claims.pending_rework_message 
                        ))
                        FROM claims 
                        WHERE accidents.id = claims.ref_accident_id
                    )
                )
            FROM accidents
            WHERE accidents.ref_username_norm = '${global_helpers.normalise_string(pass.username)}'
            `
            )
                .then((result) => {
                    // console.log(JSON.stringify(result.rows))
                    let accidents_and_claims = [];
                    result.rows.forEach((ele) => {
                        accidents_and_claims.push(ele.json_build_object);
                    });
                    pass.get_accidents_and_claims_result = accidents_and_claims;
                    resolve(pass);
                })
                .catch((err) => {
                    console.log(err);
                    reject(err);
                });
        } catch (err) {
            console.log(err);
            reject('Required params not present');
        }
    });
};

const get_accident = (pass) => {
    console.log('sample_function()');
    // 1. Get something
    // 2. Do something else

    return new Promise((resolve, reject) => {
        try {
            db('accidents')
                .select('*')
                .where({ id: pass.accident_id })
                .then((rows) => {
                    pass.get_accident_result = rows[0];
                    resolve(pass);
                })
                .catch((err) => {
                    console.log(err);
                    reject(err);
                });
        } catch (err) {
            console.log(err);
            reject('Required params not present');
        }
    });
};

module.exports = {
    get_table_data,
    get_accidents_and_claims,
    get_accident,
    generate_premiumback_table
};
