const global_constants = require("../../global/constants");
const db = require("knex")({
    client: "pg",
    pool: {
        min: 1,
        max: "live" === "live" ? 7 : 1,
        idleTimeoutMillis: "live" === "live" ? 300 : 100
    },
    connection: {
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME
    },
    searchPath: ["knex", "public"]
});
module.exports = {
    db
};

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// Mongoose Models ////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

// const userSchema = mongoose.Schema({
//     sn: { type: String, required: true, default: new Date().getTime().toString() + parseInt(Math.random() * 10).toString() },
//     created_at: { type: Date, required: true, default: Date.now },

//     nric_number: String,
//     nric_number_norm: String,
//     bank: String,
//     bank_acct_number: String,
//     dob: String,
//     dob_norm: String,

//     email_verification_code: String,
//     email_verified: Boolean,
//     full_name: String,
//     full_name_norm: String,
//     gender: String,

//     forget_password_verification_code: String,
//     occupation: String,
//     password_hash: String,
//     place_of_birth: String,
//     place_of_birth_norm: String,
//     preferred_name: String,
//     referrer: String,
//     self_employed: Boolean,
//     stripe_details: {},
//     stripe_customer_id: String,
//     uploaded_images: [],
//     username: String,
//     username_norm: String,

//     policies: [policySchema]
// })

// const User = mongoose.model("User", userSchema)
// const Policy = mongoose.model("Policy", policySchema)

// //////////////////////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////// Mongoose Functions - CREATE ///////////////////////////////////
// //////////////////////////////////////////////////////////////////////////////////////////////////

// const raw_data_store = (pass) => {
//     console.log("raw_data_store()");
//     // Required
//     //      pass.raw_data_store_data = {}

//     return new Promise((resolve, reject) => {
//         try {
//             let sn = new Date().getTime().toString() + parseInt(Math.random() * 10).toString()
//             let raw_data = new Raw({
//                 sn: sn,
//                 data: pass.raw_data_store_data
//             })
//             Raw.insertOne(raw_data).then(result => {
//                 console.log("raw_data_store OK")
//                 pass.raw_data_store_sn = sn
//                 resolve(pass)
//             }).catch(err => {
//                 console.log(err)
//                 reject(err)
//             })
//         } catch (err) {
//             console.log(err)
//             reject("Required params not present")
//         }
//     })
// }

const create_new_user = (pass) => {
    //     UPSERTING in knex https://jaketrent.com/post/upsert-knexjs/
    //   UPSERTING, checking for collisions:  https://www.dbrnd.com/2017/04/postgresql-9-5-multiple-columns-or-keys-in-on-conflict-clause/

    console.log("create_new_user()");
    // Required
    //      pass.create_new_user_data = {}

    return new Promise((resolve, reject) => {
        try {
            User.findOneAndUpdate(
                { username_norm: pass.create_new_user_data.username_norm },
                pass.create_new_user_data,
                { upsert: true } // Create new if not found, else update old one
            )
                .then((result) => {
                    console.log("create_new_user OK");
                    resolve(pass);
                })
                .catch((err) => {
                    reject(err);
                });

            // const user = new User(pass.create_new_user_data)
            // user.save().then(result => {
            //     console.log("create_new_user OK")
            //     resolve(pass)
            // }).catch(err => console.log(err))
        } catch (err) {
            console.log(err);
            reject("Required params not present");
        }
    });
};

// const create_new_accident = (pass) => {
//     console.log("add_policy_to_user()");
//     // 1. Get something
//     // 2. Do something else

//     return new Promise((resolve, reject) => {
//         // try {

//         //     let policies = pass.add_policies_to_user_policies.map(policy => new Policy(policy))

//         //     User.findOneAndUpdate(
//         //         { _id: pass.add_policies_to_user_id },
//         //         {
//         //             $push: {
//         //                 policies: policies,
//         //             }
//         //         }).then(result => {
//         //             console.log("add_policies_to_user OK")
//         //             resolve(pass)
//         //         }).catch(err => {
//         //             reject(err)
//         //         })

//         // } catch (err) {
//         //     console.log(err)
//         //     reject("Required params not present")
//         // }
//     })
// }

// //////////////////////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////// Mongoose Functions - UPDATE ///////////////////////////////////
// //////////////////////////////////////////////////////////////////////////////////////////////////

// const add_policies_to_user = (pass) => {

//     // Try using update_model ?

//     console.log("add_policy_to_user()");
//     // Required
//     //      pass.add_policies_to_user_policies = {}
//     //      pass.add_policies_to_user_sn

//     return new Promise((resolve, reject) => {
//         try {

//             let policies = pass.add_policies_to_user_policies.map(policy => new Policy(policy))

//             User.findOneAndUpdate(
//                 { sn: pass.add_policies_to_user_sn },
//                 {
//                     $push: {
//                         policies: policies,
//                     }
//                 }).then(result => {
//                     console.log("add_policies_to_user OK")
//                     resolve(pass)
//                 }).catch(err => {
//                     reject(err)
//                 })

//         } catch (err) {
//             console.log(err)
//             reject("Required params not present")
//         }
//     })
// }

// knex('users')
// .where({ id: 135 })
// .update({ email: 'hi@example.com' })

// const update_model = (pass) => {

//     // WARNING: make sure the modelSchema has this attribute that you are adding!

//     console.log("update_model()");

//     // Required
//     //      pass.update_model_model = model // eg: User
//     //      pass.update_model_filter = {}
//     //      pass.update_model_update = {}

//     return new Promise((resolve, reject) => {
//         try {
//             let model = ""
//             switch (pass.update_model_model) {
//                 case "User":
//                     model = User
//                     break;
//                 case "Policy":
//                     model = Policy
//                     break;
//                 case "Raw":
//                     model = Raw
//                     break;
//                 default:
//                     // Should not happen
//                     break
//             }

//             Policy.findOneAndUpdate(
//                 // pass.update_model_filter,
//                 {},
//                 pass.update_model_update,
//                 // { multi: pass.update_model_multi ? true : false }
//             ).then(result => {
//                 console.log(result)
//                 console.log("update_model OK")
//                 resolve(pass)
//             }).catch(err => {
//                 reject(err)
//             })

//         } catch (err) {
//             console.log(err)
//             reject("Required params not present")
//         }
//     })
// }

// //////////////////////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////// Mongoose Functions - GET //////////////////////////////////////
// //////////////////////////////////////////////////////////////////////////////////////////////////

// module.exports = {
//     store_events,
//     raw_data_store,
//     create_new_user,

//     add_policies_to_user,
//     update_model,

//     get_all_policies,
//     get_model_info,
//     // get_user_latest_info,

// }
