// const mongo = require("../helpers/mongo");

// const email = require("../helpers/email");
const global_constants = require("../../global/constants");
const global_helpers = require("../../global/helpers");
const db = require("../helpers/pg").db;

const stripe_key = {
    live: {
        // live (hugo.khoo@ifinancialsingapore.com)
        publishable: "pk_live_ElPNGjCMFqsY2q12HmLQoqTv",
        secret: "sk_live_KsC7F6G0ATfF6O2nOXk49KML"
    },
    test: {
        // test (hugo.khoo@ifinancialsingapore.com)
        publishable: "pk_test_0t5UyM3s5YvsWVj9417Ws71a",
        secret: "sk_test_aPJHPSGHveDEmakwKucC50mu"
    }
    // "test_jx" :{   // test (jiexiongkhoo@gmail.com)
    //     publishable:"pk_test_Xc43Wua0VWsQLILDkaTyEA2l",
    //     secret: "sk_test_TSMslzViyKLJrWhu5NQXfRBz"
    // },
};

let stripe_secret_key = global_constants.staging ? stripe_key.test.secret : stripe_key.live.secret;
var stripe = require("stripe")(stripe_secret_key);

// 1. React front-end captures a source (credit card)
// 2. In backend, we create a customer, and attach a source to it.

// 2. Create customer   - // https://stripe.com/docs/sources/customers
// // Set your secret key: remember to change this to your live secret key in production
// // See your keys here: https://dashboard.stripe.com/account/apikeys

let create_customer_and_start_subscription = (pass) => {
    return new Promise((resolve, reject) => {
        try {
            let email = pass.create_customer_and_start_subscription_email;
            let token = pass.create_customer_and_start_subscription_token; // token
            let customer_id = pass.create_customer_and_start_subscription_customer_id; // token
            let plans = pass.create_customer_and_start_subscription_plans;
            let billing_cycle_anchor = pass.create_customer_and_start_subscription_billing_cycle_anchor;

            console.log(customer_id, "customer_id");

            pass.create_customer_email = email;
            pass.create_customer_token = token;
            pass.create_customer_customer_id = customer_id;
            create_customer(pass)
                .then((pass) => {
                    pass.create_subscription_customer_id = pass.create_customer_customer_id;
                    pass.create_subscription_plans = plans;
                    pass.create_subscription_billing_cycle_anchor = billing_cycle_anchor;
                    return create_subscription(pass)
                        .then((pass) => {
                            console.log("Customer created and subscription activated");
                            if (pass.create_customer_result) {
                                if (pass.create_subscription_result) {
                                    console.log("BOTH SUCEEDED!!!");
                                }
                            }

                            resolve(pass);
                        })
                        .catch((err) => {
                            console.log(err);
                            reject(err);
                        });
                })
                .catch((err) => {
                    console.log(err);
                    reject(err);
                });
        } catch (err) {
            console.log(err);
            reject(err);
        }
    });
};

let create_customer = (pass) => {
    console.log("create_customer()");

    return new Promise((resolve, reject) => {
        try {
            let email = pass.create_customer_email;
            let token = pass.create_customer_token;
            let customer_id = pass.create_customer_customer_id;

            if (customer_id) {
                // If have customer already, then no need to create another customer.
                pass.create_customer_customer_id = customer_id;
                resolve(pass);
            } else {
                stripe.customers.create(
                    {
                        email,
                        source: token
                    },
                    function(err, result_stripe) {
                        if (err) {
                            console.log(err);
                            reject(err);

                            ///// Asynchronously Storing in events /////
                            let storable = [
                                {
                                    ref_username_norm: global_helpers.normalise_string(pass.create_customer_email),
                                    type: "stripe_create_customer",
                                    data: {
                                        message: pass.pending_rework_message ? pass.pending_rework_message : null,
                                        username_staff: "SYSTEM",
                                        success: false,
                                        err: err
                                    }
                                }
                            ];
                            db("events")
                                .insert(storable)
                                .catch((err) => {
                                    console.log(err);
                                });
                        } else {
                            // console.log(result)
                            pass.create_customer_result = result_stripe;
                            pass.create_customer_customer_id = result_stripe.id;
                            console.log("Customer created");

                            // pass.update_model_model = "User" // eg: User
                            // pass.update_model_filter = { username_norm: global_helpers.normalise_string(pass.create_customer_email) }
                            // pass.update_model_update = {
                            //     stripe_customer_id: result.id
                            // }
                            // mongo.update_model(pass).catch(err => { console.log(err) })

                            db("users")
                                .update({ stripe_customer_id: result_stripe.id })
                                .where({ email_verified: true })
                                .where({ username_norm: global_helpers.normalise_string(pass.create_customer_email) })
                                .then((result) => {
                                    resolve(pass);

                                    ///// Asynchronously Storing in events /////
                                    let storable = [
                                        {
                                            ref_username_norm: global_helpers.normalise_string(
                                                pass.create_customer_email
                                            ),
                                            type: "stripe_create_customer",
                                            data: {
                                                message: pass.pending_rework_message
                                                    ? pass.pending_rework_message
                                                    : null,
                                                success: true,
                                                username_staff: "SYSTEM",
                                                data: result_stripe
                                            }
                                        }
                                    ];
                                    db("events")
                                        .insert(storable)
                                        .catch((err) => {
                                            console.log(err);
                                        });
                                })
                                .catch((err) => {
                                    console.log(err);
                                    reject(err);
                                });
                        }
                    }
                );
            }
        } catch (err) {
            reject(err);
        }
    });
};

// 3. Create subscription with anchor (done when clerk approves user)   - // https://stripe.com/docs/sources/customers
// To create a subscription with a billing cycle anchor in the future and a prorated invoice until the anchor, perform a create subscription call with a timestamp for billing_cycle_anchor:
// var stripe = require("stripe")("sk_test_TSMslzViyKLJrWhu5NQXfRBz");
// const subscription = stripe.subscriptions.create({
//     customer: 'cus_4fdAW5ftNQow1a',
//     items: [{ plan: 'plan_CBb6IXqvTLXp3f' }],
//     billing_cycle_anchor: 1557280827,
// });

let create_subscription = (pass) => {
    console.log("create_subscription()");
    return new Promise((resolve, reject) => {
        try {
            let customer = pass.create_subscription_customer_id;
            let items = pass.create_subscription_plans;
            let billing_cycle_anchor = pass.create_subscription_billing_cycle_anchor;
            let coupon = pass.create_customer_and_start_subscription_coupon;

            if (!(customer && items && billing_cycle_anchor)) {
                console.log(customer, items, billing_cycle_anchor);
                reject("Insufficient params for create_subscription()");
            } else {
                stripe.subscriptions.create(
                    {
                        customer,
                        items,
                        billing_cycle_anchor,
                        coupon
                    },
                    function(err, result_stripe) {
                        if (err) {
                            console.log(err);
                            reject(err);

                            ///// Asynchronously Storing in events /////
                            let storable = [
                                {
                                    ref_username_norm: global_helpers.normalise_string(pass.create_customer_email),
                                    ref_policy_id: pass.create_customer_and_start_subscription_policy_id,
                                    type: "stripe_create_subscription",
                                    data: {
                                        message: pass.pending_rework_message ? pass.pending_rework_message : null,
                                        username_staff: "SYSTEM",
                                        success: false,
                                        err: err
                                    }
                                }
                            ];
                            db("events")
                                .insert(storable)
                                .catch((err) => {
                                    console.log(err);
                                });
                        } else {
                            // SUCCESSFUL
                            console.log("Subscription activated");
                            pass.create_subscription_result = result_stripe;

                            db("policies")
                                .update({ stripe_subscription_id: result_stripe.id })
                                .where({ id: pass.create_customer_and_start_subscription_policy_id })
                                .then((result) => {
                                    resolve(pass);

                                    ///// Asynchronously Storing in events /////
                                    let storable = [
                                        {
                                            ref_username_norm: global_helpers.normalise_string(
                                                pass.create_customer_email
                                            ),
                                            ref_policy_id: pass.create_customer_and_start_subscription_policy_id,
                                            type: "stripe_create_subscription",
                                            data: {
                                                message: pass.pending_rework_message
                                                    ? pass.pending_rework_message
                                                    : null,
                                                success: true,
                                                username_staff: "SYSTEM",
                                                data: result_stripe
                                            }
                                        }
                                    ];
                                    db("events")
                                        .insert(storable)
                                        .catch((err) => {
                                            console.log(err);
                                        });
                                })
                                .catch((err) => {
                                    console.log(err);
                                    reject(err);
                                });
                        }
                    }
                );
            }
        } catch (err) {
            console.log(err);
            reject(err);
        }
    });
};

let create_charge_to_customer = (pass) => {
    return new Promise((resolve, reject) => {
        try {
            let customer = pass.create_charge_to_customer_customer;
            let amount = pass.create_charge_to_customer_amount;
            let currency = pass.create_charge_to_customer_currency;

            stripe.charges.create(
                {
                    amount,
                    currency,
                    customer
                    // source: 'src_18eYalAHEMiOZZp1l9ZTjSU0',   // If you attempt to charge a Customer object without specifying a source, Stripe uses the customer’s default source.
                },
                function(err, charge) {
                    if (err) {
                        console.log(err);
                        reject(err);
                    } else {
                        console.log(charge);
                        pass.create_charge_to_customer_result = charge;
                        resolve(pass);

                        // asynchronously called
                    }
                }
            );
        } catch (err) {
            reject(err);
        }
    });
};

let cancel_subscription = (pass) => {
    console.log("cancel_subscription()");
    return new Promise((resolve, reject) => {
        try {
            let username = pass.cancel_subscription_username;
            let policy_id = pass.cancel_subscription_policy_id;
            let subscription_id = pass.cancel_subscription_subscription_id;

            console.log(username, policy_id, subscription_id);

            stripe.subscriptions.del(subscription_id, function(err, result_stripe) {
                if (err) {
                    console.log(err);
                    reject(err);
                    ///// Asynchronously Storing in events /////
                    let storable = [
                        {
                            ref_username_norm: global_helpers.normalise_string(username),
                            ref_policy_id: policy_id,
                            type: "stripe_cancel_subscription",
                            data: {
                                message: pass.pending_rework_message ? pass.pending_rework_message : null,
                                success: false,
                                username_staff: "SYSTEM",
                                err: err
                            }
                        }
                    ];
                    db("events")
                        .insert(storable)
                        .catch((err) => {
                            console.log(err);
                        });
                } else {
                    // SUCCESS
                    console.log(result_stripe);

                    db("policies")
                        .update({ stripe_subscription_id: null })
                        .where({ id: policy_id })
                        .then((result) => {
                            resolve(pass);

                            ///// Asynchronously Storing in events /////
                            let storable = [
                                {
                                    ref_username_norm: global_helpers.normalise_string(username),
                                    ref_policy_id: policy_id,
                                    type: "stripe_cancel_subscription",
                                    data: {
                                        message: pass.pending_rework_message ? pass.pending_rework_message : null,
                                        success: true,
                                        username_staff: "SYSTEM",
                                        data: result_stripe
                                    }
                                }
                            ];
                            db("events")
                                .insert(storable)
                                .catch((err) => {
                                    console.log(err);
                                });
                        })
                        .catch((err) => {
                            console.log(err);
                            reject(err);
                        });
                }
            });
        } catch (err) {
            reject(err);
        }
    });
};

// 4. Handle card charge failure - https://stripe.com/docs/billing/subscriptions/payment
// Need to email customer, to login to account page and change his/her card details

// Done!

let create_charge_to_source = (pass) => {
    // [TRY NOT TO USE] Use create_charge_to_customer instead

    return new Promise((resolve, reject) => {
        try {
            let amount = pass.create_charge.amount;
            let currency = pass.create_charge.currency;
            let source = pass.create_charge.source;
            let receipt_email = pass.create_charge.receipt_email;
            let metadata = pass.create_charge.metadata;

            stripe.charges
                .create({
                    amount,
                    currency,
                    source,
                    receipt_email,
                    metadata
                })
                .then((result_stripe) => {
                    pass.create_charge_to_token_result = result_stripe;
                    resolve(pass);
                })
                .catch((err) => {
                    reject(err);
                });
        } catch (err) {
            console.log(err);
            reject(err);
        }
    });
};

// Misc 1. Change card number    - // https://stripe.com/docs/sources/customers
// // Set your secret key: remember to change this to your live secret key in production
// // See your keys here: https://dashboard.stripe.com/account/apikeys
// var stripe = require("stripe")("sk_test_TSMslzViyKLJrWhu5NQXfRBz");

// stripe.customers.createSource('cus_AFGbOSiITuJVDs', {
//   source: 'src_18eYalAHEMiOZZp1l9ZTjSU0',
// });

// Misc 2. Charge a customer    - // https://stripe.com/docs/sources/customers
// var stripe = require("stripe")("sk_test_TSMslzViyKLJrWhu5NQXfRBz");
// stripe.charges.create({
//   amount: 1099,
//   currency: 'eur',
//   customer: 'cus_AFGbOSiITuJVDs',
//   source: 'src_18eYalAHEMiOZZp1l9ZTjSU0',   // If you attempt to charge a Customer object without specifying a source, Stripe uses the customer’s default source.
// }, function(err, charge) {
//   // asynchronously called
// });

const sample_function = (pass) => {
    console.log("sample_function()");
    // 1. Get something
    // 2. Do something else

    return new Promise((resolve, reject) => {
        try {
        } catch (err) {
            reject("Required params not present");
        }
    });
};

////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// Signup ////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

module.exports = {
    // Routine
    create_customer_and_start_subscription,
    // create_customer,     // Use create_customer_and_start_subscription
    // create_subscription,  // create_customer_and_start_subscription

    // Ad hoc charges to customer
    create_charge_to_customer,
    cancel_subscription,

    // Try not to use
    create_charge_to_source
};
