
const db = require("../helpers/pg").db;

const email = require("./email");
var isEmpty = require('lodash').isEmpty;

const global_constants = require("../../global/constants");
const global_helpers = require("../../global/helpers");

const sample_function = (pass) => {

    console.log("sample_function()");
    // 1. Get something
    // 2. Do something else

    return new Promise((resolve, reject) => {
        if (pass.sample_function) {

        } else {
            reject("Required params not present")
        }
    })
}





const check_for_duplicates_insured = (pass) => {

    console.log("check_for_duplicates_insured()");

    return new Promise((resolve, reject) => {
        try {

            async function check_for_duplicates_insured_recurssively() {
                console.log("check_for_duplicates_insured_recurssively()")

                const allow_recursion = true
                let to_recurse = false

                let conflict_with_policy_id = null

                let policy_and_user = null
                let policy_id = null
                let nric_number_norm = null
                let full_name_norm = null
                let gender = null
                let dob_norm = null
                let place_of_birth_norm = null

                //////////// Get the insured details ////////////

                await db.select(db.raw(`to_json(policies.*) as policy `), db.raw('to_json(users.*) as user'))
                    .from('policies')
                    // .whereNull('insured_details_verified').orWhere('insured_details_verified', false)
                    .whereNull('duplicate_check_result')
                    .where(function (builder) {
                        // if (pass.date_start) { builder.andWhere('created_at', '>=', pass.date_start) }
                        // if (pass.date_end) { builder.andWhere('created_at', '<', pass.date_end) }
                    }).limit(2)
                    .join('users', 'users.username_norm', 'policies.ref_username_norm')
                    .then(rows => {
                        if (rows.length > 0) {
                            if (allow_recursion && rows.length > 1) { to_recurse = true }
                            policy_and_user = rows[0]
                            policy_id = policy_and_user.policy.id
                            nric_number_norm = policy_and_user.policy.representing === "self" ? policy_and_user.user.nric_number_norm : policy_and_user.policy.nric_number_norm
                            full_name_norm = policy_and_user.policy.representing === "self" ? policy_and_user.user.full_name_norm : policy_and_user.policy.full_name_norm
                            gender = policy_and_user.policy.representing === "self" ? policy_and_user.user.gender : policy_and_user.policy.gender
                            dob_norm = policy_and_user.policy.representing === "self" ? policy_and_user.user.dob_norm : policy_and_user.policy.dob_norm
                            place_of_birth_norm = policy_and_user.policy.representing === "self" ? policy_and_user.user.place_of_birth_norm : policy_and_user.policy.place_of_birth_norm
                        } else {
                            console.log("No more policies to do a duplicate check. All clear! If a duplicate check was expected, there might be an error")
                            resolve(pass)
                            return false
                        }
                    })

                // Checking if able to get required values
                if (!nric_number_norm || !full_name_norm || !gender || !dob_norm || !place_of_birth_norm) {
                    // console.log("nric_number_norm - " + nric_number_norm)
                    // console.log("full_name_norm - " + full_name_norm)
                    // console.log("gender - " + gender)
                    // console.log("dob_norm - " + dob_norm)
                    // console.log("place_of_birth_norm - " + place_of_birth_norm)
                    console.log("ERROR - " + "!nric_number_norm || !full_name_norm || !gender || !dob_norm || !place_of_birth_norm")
                    reject("!nric_number_norm || !full_name_norm || !gender || !dob_norm || !place_of_birth_norm")
                    return false
                }

                //////////// Query all policies to find same NRIC where insured_details_verified ////////////

                let updatable = {
                    duplicate_check_result: {
                        passed: true,
                        conflict_with_id: null,
                        message: "passed"
                    }
                }

                //////////// ELECTTRIC FENCE (BUY FOR OTHERS) - Query all policies to find same 4/4 where insured_details_verified ////////////
                if (updatable.duplicate_check_result.passed === true) {     // If there are still no conflicts, continue checking
                    await db('policies').select('*')
                        .whereNot({ id: policy_id }).where({ insured_details_verified: true })
                        .where({ nric_number_norm: nric_number_norm })
                        .then(rows => {
                            if (rows.length > 0) {
                                updatable.duplicate_check_result = {
                                    passed: false,
                                    conflict_with_id: rows[0].id,
                                    message: "NRIC conflict - representing others"
                                }
                            }
                        })
                }

                //////////// 4/4 RULE (BUY FOR OTHERS) - Query all policies to find same 4/4 where insured_details_verified ////////////
                if (updatable.duplicate_check_result.passed === true) {     // If there are still no conflicts, continue checking
                    await db('policies').select('*')
                        .whereNot({ id: policy_id }).where({ insured_details_verified: true })
                        .where({ full_name_norm: full_name_norm }).where({ gender: gender }).where({ dob_norm: dob_norm }).where({ place_of_birth_norm: place_of_birth_norm })
                        .then(rows => {
                            if (rows.length > 0) {
                                updatable.duplicate_check_result = {
                                    passed: false,
                                    conflict_with_id: rows[0].id,
                                    message: "4/4 conflict - representing others"
                                }
                            }
                        })
                }

                //////////// ELECTTRIC FENCE (BUY FOR SELF) - Query all policies to find same 4/4 where insured_details_verified ////////////
                if (updatable.duplicate_check_result.passed === true) {     // If there are still no conflicts, continue checking
                    await db.select(db.raw(`to_json(policies.*) as policy `), db.raw('to_json(users.*) as user'))
                        .from('policies')
                        .whereNot('policies.id', policy_id).where({ insured_details_verified: true })
                        .where({ representing: 'self' })
                        .where('users.nric_number_norm', nric_number_norm)
                        .join('users', 'users.username_norm', 'policies.ref_username_norm')
                        .then(rows => {
                            if (rows.length > 0) {
                                updatable.duplicate_check_result = {
                                    passed: false,
                                    conflict_with_id: rows[0].id,
                                    message: "NRIC conflict - representing self"
                                }
                            }
                        })
                }

                //////////// 4/4 RULE (BUY FOR SELF) - Query all policies to find same 4/4 where insured_details_verified ////////////
                if (updatable.duplicate_check_result.passed === true) {     // If there are still no conflicts, continue checking
                    await db.select(db.raw(`to_json(policies.*) as policy `), db.raw('to_json(users.*) as user'))
                        .from('policies')
                        .whereNot('policies.id', policy_id).where({ insured_details_verified: true })
                        .where({ representing: 'self' })
                        .where('users.gender', gender).where('users.dob_norm', dob_norm).where('users.place_of_birth_norm', place_of_birth_norm)
                        .join('users', 'users.username_norm', 'policies.ref_username_norm')
                        .then(rows => {
                            if (rows.length > 0) {
                                updatable.duplicate_check_result = {
                                    passed: false,
                                    conflict_with_id: rows[0].id,
                                    message: "4/4 conflict - representing self"
                                }
                            }
                        })

                }


                //////////// Stringify to store int postgres as jsonb type ////////////
                updatable.duplicate_check_result = JSON.stringify(updatable.duplicate_check_result)

                //////////// Store results in column duplicate_check_result ////////////
                await db('policies').where({ id: policy_id }).update(updatable).then(pass => {
                    console.log("Storing of result SUCCESSFUL - " + JSON.stringify(updatable.duplicate_check_result))
                }).catch(err => { console.log(err); console.log("ERROR - Storing of result FAILED") })

                // Recurse if there are other policies to check. If no more policies to check, stop and resolve().
                if (to_recurse) { check_for_duplicates_insured() }
                else {
                    resolve(pass)
                }

            }

            check_for_duplicates_insured_recurssively()

        } catch (err) {
            console.log(err)
            reject("Required params not present")
        }
    })
}




////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// User Actions /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////






const purchase = (pass) => {

    console.log("purchase()");
    // 1. Store raw data
    // 2. get_user_model - For rules checking purposes.
    // 2. Reject by users RULES - Check for duplicates of info_verified users
    // 3. Reject by policies RULES - Check for duplicates of info_verified policies
    // 4. Store data

    // const get_user_model = (pass) => {
    //     console.log("get_user_model()");
    //     return new Promise((resolve, reject) => {
    //         try {
    //             // If bought for self, need this info to check against rules
    //             pass.get_1_user_verified_username = pass.purchase.username

    //             pass.get_model_info_model = "User"
    //             pass.get_model_info_filter = { username_norm: global_helpers.normalise_string(pass.purchase.username), email_verified: true }
    //             mongo.get_model_info(pass).then(pass => {
    //                 pass.user_current_doc = result;
    //                 resolve(pass)
    //             }).catch(err => { console.log(err); reject(err) })

    //         } catch (err) {
    //             console.log(err)
    //             reject("Invalid / missing param(s)")
    //         }
    //     })
    // }


    const generate_policies = (pass) => {
        console.log("generate_policies()");

        return new Promise((resolve, reject) => {
            try {

                let policies_generated = []

                for (let i = 0; i < pass.purchase.products.length; i++) {
                    console.log("Iterating through products number " + i)

                    try {

                        const policy_template = {
                            ref_username_norm: global_helpers.normalise_string(pass.purchase.username),
                            product: pass.purchase.products[i],
                            product_details: pass.purchase.product_details[pass.purchase.products[i]],          // Polivy details at the time of customer's purchase     
                            // EG: small_accident: {
                            //          name: "Small Accident",
                            //          premium: 20.4,
                            //          admin_fee: 9
                            //     },
                            representing: pass.purchase.representing,
                            uploaded_image_signup_screenshot: pass.purchase.image_upload_signup_screenshot,
                            status: 'pending_approval',
                            status_history: JSON.stringify([{ created_at: new Date().toISOString(), status: 'pending_approval' }]),
                            referrer: pass.purchase.referrer ? pass.purchase.referrer : "",
                            source: pass.purchase.source ? pass.purchase.source : "",
                            flag_staff_inbox_category: "pending_approval",
                        }

                        switch (pass.purchase.representing) {
                            case 'self':
                                let policy_new = global_helpers.clone_deep(policy_template);
                                policies_generated.push(policy_new)
                                break;
                            case 'entity':
                                console.log("Entity detected!")

                                /////////////// Getting info from self to populate new_policy ///////////////

                                try {

                                    for (let j = 0; j < pass.purchase.insured.length; j++) {

                                        // let policy = JSON.parse(JSON.stringify(policy_template))
                                        let policy_new = global_helpers.clone_deep(policy_template);

                                        // policy_new.claims_pay_to_same = pass.purchase.family_claims_pay_to_same
                                        policy_new.nric_number = pass.purchase.insured[j].nric_number
                                        policy_new.bank = pass.purchase.personal_bank
                                        policy_new.bank_acct_number = pass.purchase.personal_bank_acct_number
                                        policy_new.bank_claims = pass.purchase.insured[j].claims_bank
                                        policy_new.bank_acct_number_claims = pass.purchase.insured[j].claims_bank_acct_number
                                        policy_new.bank_premiumback = pass.purchase.insured[j].premiumback_bank
                                        policy_new.bank_acct_number_premiumback = pass.purchase.insured[j].premiumback_bank_acct_number
                                        policy_new.dob = pass.purchase.insured[j].dob
                                        policy_new.email = pass.purchase.insured[j].email
                                        policy_new.full_name = pass.purchase.insured[j].full_name
                                        policy_new.gender = pass.purchase.insured[j].gender
                                        policy_new.mobile_number = global_helpers.normalise_string(pass.purchase.insured[j].mobile_number)
                                        policy_new.place_of_birth = pass.purchase.insured[j].place_of_birth
                                        policy_new.preferred_name = pass.purchase.insured[j].preferred_name
                                        policy_new.entity_name = pass.purchase.insured[j].entity_name
                                        policy_new.entity_uen = pass.purchase.insured[j].entity_uen
                                        policy_new.occupation = pass.purchase.insured[j].occupation
                                        policy_new.self_employed = pass.purchase.insured[j].self_employed
                                        policy_new.uploaded_images = JSON.stringify(pass.purchase.insured[j].uploaded_images)


                                        policy_new.nric_number_norm = global_helpers.normalise_string(policy_new.nric_number)
                                        policy_new.dob_norm = global_helpers.normalise_string(policy_new.dob)
                                        policy_new.full_name_norm = global_helpers.normalise_string(policy_new.full_name)
                                        policy_new.place_of_birth_norm = global_helpers.normalise_string(policy_new.place_of_birth)


                                        policies_generated.push(policy_new)
                                        console.log("PUSHED " + pass.purchase.insured[j].full_name + " into policies_generated")
                                    }


                                } catch (err) {
                                    console.log(err)
                                    console.log("Failed to add 'family' - Probably missing param")
                                    reject("Failed to add 'family' - Probably missing param")
                                }
                                break;
                            case 'family':
                                console.log("Family detected!")

                                /////////////// Getting info from self to populate new_policy ///////////////

                                try {

                                    for (let j = 0; j < pass.purchase.insured.length; j++) {

                                        // let policy = JSON.parse(JSON.stringify(policy_template))
                                        let policy_new = global_helpers.clone_deep(policy_template);

                                        // policy_new.claims_pay_to_same = pass.purchase.family_claims_pay_to_same
                                        policy_new.nric_number = pass.purchase.insured[j].nric_number
                                        policy_new.bank = pass.purchase.personal_bank
                                        policy_new.bank_acct_number = pass.purchase.personal_bank_acct_number
                                        policy_new.bank_claims = pass.purchase.insured[j].claims_bank
                                        policy_new.bank_acct_number_claims = pass.purchase.insured[j].claims_bank_acct_number
                                        policy_new.bank_premiumback = pass.purchase.insured[j].premiumback_bank
                                        policy_new.bank_acct_number_premiumback = pass.purchase.insured[j].premiumback_bank_acct_number
                                        policy_new.dob = pass.purchase.insured[j].dob
                                        policy_new.email = pass.purchase.insured[j].email
                                        policy_new.full_name = pass.purchase.insured[j].full_name
                                        policy_new.gender = pass.purchase.insured[j].gender
                                        policy_new.mobile_number = global_helpers.normalise_string(pass.purchase.insured[j].mobile_number)
                                        policy_new.place_of_birth = pass.purchase.insured[j].place_of_birth
                                        policy_new.preferred_name = pass.purchase.insured[j].preferred_name
                                        policy_new.relationship_to_payer = pass.purchase.insured[j].relationship_to_payer
                                        policy_new.occupation = pass.purchase.insured[j].occupation
                                        policy_new.self_employed = pass.purchase.insured[j].self_employed
                                        policy_new.uploaded_images = JSON.stringify(pass.purchase.insured[j].uploaded_images)


                                        policy_new.nric_number_norm = global_helpers.normalise_string(policy_new.nric_number)
                                        policy_new.dob_norm = global_helpers.normalise_string(policy_new.dob)
                                        policy_new.full_name_norm = global_helpers.normalise_string(policy_new.full_name)
                                        policy_new.place_of_birth_norm = global_helpers.normalise_string(policy_new.place_of_birth)


                                        policies_generated.push(policy_new)
                                        console.log("PUSHED " + pass.purchase.insured[j].full_name + " into policies_generated")
                                    }


                                } catch (err) {
                                    console.log(err)
                                    console.log("Failed to add 'family' - Probably missing param")
                                    reject("Failed to add 'family' - Probably missing param")
                                }
                                break;
                            default:
                                console.log("ERROR - pass.purchase.representing does not meet any case! This should not happen!")
                                reject("ERROR - pass.purchase.representing does not meet any case! This should not happen!")
                                break;
                        }


                    } catch (err) {
                        console.log(err)
                        reject("Failed to push transactions to batch operation")
                    }
                }

                pass.generate_policies_policies = policies_generated

                resolve(pass)


            } catch (err) {
                console.log(err)
                reject("Invalid / missing param(s)")
            }
        })
    }


    const update_user_and_insert_policies = (pass) => {
        console.log("update_user_and_insert_policies()");

        return new Promise((resolve, reject) => {
            try {

                let updatable = {}

                if (pass.purchase.representing == 'entity') {
                    if (pass.purchase.company_details) {        // If company_details in DB is null and there is new company_details into coming in, update company_details
                        updatable.company_details = pass.purchase.company_details
                    }
                } else {
                    // Adding personal details storing (if applicable) to batch
                    if (pass.purchase.personal_info_already_in_db) {     // If personal_info_already_in_db then no need to store personal details
                        console.log("Personal details already previously stored. No need to store.")
                        try {
                            updatable.uploaded_images = JSON.stringify(pass.user_current_doc.uploaded_images.concat(pass.purchase.personal_uploaded_images))
                        } catch (err) {
                            console.log(err)
                            reject("Invalid / missing param(s)")
                        }

                    } else {
                        try {
                            if (pass.user_current_doc.full_name || pass.user_current_doc.nric_number) {  // Checking if personal_info_already_in_db is actually true to prevent multiple entries / over-writing of data
                                console.log("REJECT - multiple_entries")
                                reject({
                                    err: "ERROR - Multiple entries detected. Payer info already present in database. Please refresh and try again.",
                                    err_code: "multiple_entries",
                                    success: false
                                })
                            } else {     // No over-writing. Free to update user doc with personal_info
                                try {
                                    updatable.pmd_brand = pass.purchase.pmd_brand
                                    updatable.pmd_model = pass.purchase.pmd_model
                                    updatable.pmd_color = pass.purchase.pmd_color
                                    updatable.pmd_serial_number = pass.purchase.pmd_serial_number
                                    updatable.pmd_lta_number = pass.purchase.pmd_lta_number
                                    updatable.pmd_ul_number = pass.purchase.pmd_ul_number

                                    updatable.full_name = pass.purchase.personal_full_name
                                    updatable.preferred_name = pass.purchase.personal_preferred_name
                                    updatable.nric_number = pass.purchase.personal_nric_number
                                    updatable.gender = pass.purchase.personal_gender
                                    updatable.dob = pass.purchase.personal_dob
                                    updatable.place_of_birth = pass.purchase.personal_place_of_birth
                                    updatable.mobile_number = global_helpers.normalise_string(pass.purchase.personal_mobile_number)
                                    updatable.uploaded_images = JSON.stringify(pass.purchase.personal_uploaded_images)
                                    updatable.bank = pass.purchase.personal_bank
                                    updatable.bank_acct_number = pass.purchase.personal_bank_acct_number
                                    updatable.stripe_details = pass.purchase.stripe_details
                                    // updatable.referrer = pass.purchase.referrer ? pass.purchase.referrer : ""

                                    updatable.occupation = pass.purchase.personal_occupation
                                    updatable.self_employed = pass.purchase.personal_self_employed

                                    updatable.nric_number_norm = global_helpers.normalise_string(pass.purchase.personal_nric_number)
                                    updatable.dob_norm = global_helpers.normalise_string(pass.purchase.personal_dob)
                                    updatable.full_name_norm = global_helpers.normalise_string(pass.purchase.personal_full_name)
                                    updatable.place_of_birth_norm = global_helpers.normalise_string(pass.purchase.personal_place_of_birth)

                                } catch (err) {
                                    reject("Invalid / missing param(s)")
                                }
                            }

                        } catch (err) {
                            console.log(err)
                            reject("Invalid / missing param(s)")
                        }
                    }

                }



                // 1. Update user (if needed)
                // 2. Insert new policies



                if (isEmpty(updatable)) {     // If nothing to update, no need to update
                    db('policies').insert(pass.generate_policies_policies).returning('id').then(inserts => {
                        console.log(inserts.length + ' new policies saved.');

                        let event_insertable = []
                        for (let i = 0; i < inserts.length; i++) {
                            event_insertable.push({
                                ref_username_norm: global_helpers.normalise_string(pass.purchase.username), // username of person making that action
                                ref_policy_id: inserts[i], // username of person making that action
                                type: "user_purchase",
                                data: {
                                    policy: pass.generate_policies_policies[i].product, // username of user
                                    purchase: pass.purchase
                                }
                            })
                        }
                        db('events').insert(event_insertable).catch(err => { console.log(err) })

                        resolve(pass)
                    }).catch((err) => { console.error(err); reject(err) });
                } else {
                    db.transaction(function (trx) {
                        return trx('users').where({ username_norm: global_helpers.normalise_string(pass.purchase.username) }).where({ email_verified: true }).update(updatable)
                            .then(() => { return trx('policies').insert(pass.generate_policies_policies).returning('id') })
                    }).then(inserts => {
                        console.log(inserts)
                        console.log(inserts.length + ' new policies saved.')

                        let event_insertable = []
                        for (let i = 0; i < inserts.length; i++) {
                            event_insertable.push({
                                ref_username_norm: global_helpers.normalise_string(pass.purchase.username), // username of person making that action
                                ref_policy_id: inserts[i], // username of person making that action
                                type: "user_purchase",
                                data: {
                                    policy: pass.generate_policies_policies[i].product, // username of user
                                    purchase: pass.purchase

                                }
                            })
                        }
                        db('events').insert(event_insertable).catch(err => { console.log(err) })

                        resolve(pass)


                    }).catch((err) => { console.error(err); reject(err) });
                }


            } catch (err) {
                console.log(err)
                reject(err)
            }

        })
    }


    return new Promise((resolve, reject) => {
        try {

            // Get user_current_doc first
            db.from('users').select('*').where({ username_norm: global_helpers.normalise_string(pass.purchase.username) }).where({ email_verified: true }).then(rows => {
                pass.user_current_doc = rows[0]

                generate_policies(pass).then(pass => {          // To modify personal_info
                    return update_user_and_insert_policies(pass)                       // To ensure no duplicates with info_verified policies
                }).then(pass => {
                    // console.log(pass)


                    // Email confirmation
                    let preferred_name = 'Customer'
                    if (pass.purchase.username) {
                        preferred_name = pass.purchase.username.split('@')[0]
                    }
                    if (pass.purchase.personal_preferred_name) {
                        preferred_name = pass.purchase.personal_preferred_name
                    }

                    pass.send_email_email_address = pass.purchase.username
                    pass.send_email_subject = "iFinSG Policy Application Sent"
                    pass.send_email_message =
                        `
                <html>                    
                <h2>Dear `+ preferred_name + `,</h2>
               
                <h4>Your purchase has been successfully submitted.</h4>
        
                <h4>Please hang tight while we process your application!</h4>
                
                <h4>We will notify you as soon as it has been approved.</h4>
           
              
                <div>Always here for You,</div> 
                <br>
                <div>Adele</div>
                <div>Ops Team</div>
                <div>www.iFinSG.com</div>
                <br>
                <div>Phone: +65 968 986 76</div>
                <div>Email: adele@ifinancialsingapore.com</div>
                <div>Facebook: https://www.facebook.com/ifinancialsingapore</div>
                <div>LinkedIn: https://www.linkedin.com/company/ifinancial-singapore?trk=biz-companies-cym </div>
                </html>
                `
                    email.send_email(pass)
                    resolve(pass)

                    check_for_duplicates_insured(pass)

                }).catch(err => {
                    reject(err)
                })

            }).catch(err => { console.log(err); reject(err) })


        } catch (err) {
            console.log(err)
            reject("Failed to actions.purchase()")
        }
    })

}




const edit_field = (pass) => {
    console.log("edit_field()");
    // 1. Get something
    // 2. Do something else

    return new Promise((resolve, reject) => {
        try {

            // token: req.body.token,
            //     username: req.body.username,
            //     password: req.body.password,
            //     user_or_policy: req.body.user_or_policy,
            //     username_or_id: req.body.username_or_id,
            //     field_name: req.body.field_name,
            //     new_value: req.body.new_value



            console.log(pass)
            resolve(pass)

            let table = "users"
            let where_filter = { username_norm: global_helpers.normalise_string(pass.username) }
            let updatable = { [pass.field_name]: pass.new_value }

            if (pass.field_name === "bank") { updatable = { bank: pass.new_value[0], bank_acct_number: pass.new_value[1] } }

            if (pass.user_or_policy !== "user") {
                table = "policies"
                where_filter = { id: pass.username_or_id }
                if (pass.field_name === "bank") { updatable = { bank: pass.new_value[0], bank_acct_number: pass.new_value[1] } }
                if (pass.field_name === "bank_claims") { updatable = { bank_claims: pass.new_value[0], bank_acct_number_claims: pass.new_value[1] } }
                if (pass.field_name === "bank_premiumback") { updatable = { bank_premiumback: pass.new_value[0], bank_acct_number_premiumback: pass.new_value[1] } }

            }
            console.log(table, "table")
            console.log(updatable, JSON.stringify(updatable))
            console.log(where_filter, JSON.stringify(where_filter))

            // UPDATE PENDING_REWORK_ITEMS IN DB AFTER A USER CHANGES A FIELD

            db.transaction((trx) => {
                return trx(table).select("*").where(where_filter).then(rows => {

                    // Checking to see if there is a need to update pending_rework_items
                    let pending_rework_items_new = global_helpers.clone_deep(rows[0].pending_rework_items)
                    var index = pending_rework_items_new.indexOf(pass.field_name);
                    if (index > -1) {
                        pending_rework_items_new.splice(index, 1);
                        updatable.pending_rework_items = JSON.stringify(pending_rework_items_new)       // Add pending_rework_items_new to updatable
                    }

                    if (pass.field_name === "uploaded_images") {
                        let uploaded_images_old = rows[0].uploaded_images
                        let uploaded_images_new = uploaded_images_old.concat(pass.new_value)
                        updatable.uploaded_images = JSON.stringify(uploaded_images_new)
                    }

                    if (pending_rework_items_new.length === 0) {    // Check if all pending_reworking_items are cleared, then update statuses accordingly
                        if (pass.user_or_policy === "user") {
                            //    Make all its policy statuses to pending_rework_approval
                            let updatable_user_policy_status = { status: "pending_rework_approval" }

                            return trx("policies").update(updatable_user_policy_status).where({ ref_username_norm: global_helpers.normalise_string(pass.username) }).then(result => {

                                // Update user's fields
                                return trx(table).update(updatable).where(where_filter).then(result => {
                                    console.log(result);
                                    resolve(pass)
                                }).catch((err) => { console.error(err); reject(err) })
                            }).catch(err => {
                                console.log(err)
                            })

                        } else {      // pass.user_or_policy === "policy"
                            //    Just go ahead and change status to pending_rework_approval
                            updatable.status = "pending_rework_approval"


                            // Deciding whether to update all policies or not
                            return trx("policies").select('*').where({ ref_username_norm: global_helpers.normalise_string(pass.username) }).then(rows => {

                                let make_all_pending_rework_approval = true
                                rows.forEach((ele) => {
                                    if (ele.pending_rework_items && ele.pending_rework_items.length > 0 && ele.id !== pass.username_or_id) { make_all_pending_rework_approval = false; }
                                })

                                if (make_all_pending_rework_approval) {
                                    // Update all
                                    return trx(table).update({ status: "pending_rework_approval" }).where({ ref_username_norm: global_helpers.normalise_string(pass.username) }).whereNot(where_filter).then(result => {
                                        return trx(table).update(updatable).where(where_filter).then(result => {
                                            console.log(result);
                                            resolve(pass)
                                        }).catch((err) => { console.error(err); reject(err) })
                                    }).catch((err) => { console.error(err); reject(err) })
                                } else {
                                    // Update only that field
                                    return trx(table).update(updatable).where(where_filter).then(result => {
                                        console.log(result);
                                        resolve(pass)
                                    }).catch((err) => { console.error(err); reject(err) })
                                }
                            })
                        }
                    } else {    // If there are still other pending_rework_items, then just update as per normal
                        return trx(table).update(updatable).where(where_filter).then(result => {
                            console.log(result);
                            resolve(pass)
                        }).catch((err) => { console.error(err); reject(err) })
                    }
                })
            }).then(result => {
                let storable = [{
                    ref_username_norm: global_helpers.normalise_string(pass.username),
                    ref_policy_id: pass.user_or_policy !== "user" ? pass.username_or_id : null,
                    type: "user_edit_field",
                    data: updatable
                }]
                db('events').insert(storable).catch(err => { console.log(err) })
            }).catch(err => {
                console.log(err)
                reject(err)
            })

            // return trx(table).update(updatable).where(where_filter).then(result => {

            // return trx('policies').where({ ref_username_norm: global_helpers.normalise_string(pass.username) }).where({ id: pass.id }).update(updatable_policy)
            //     .then(() => {
            //         // Updating policy status_history
            //         if (updatable_policy.status) {
            //             return trx.raw(`UPDATE policies SET status_history = jsonb_insert(status_history, '{0}','{"created_at":"${new Date().toISOString()}", "status": "${updatable_policy.status}"}') WHERE id = ${pass.id}`)
            //         }
            //     })





            // db(table).update(updatable).where(where_filter).then(result => {

            //     resolve(pass)

            //     ///// Asynchronously Storing in events /////
            //     let storable = [{
            //         ref_username_norm: global_helpers.normalise_string(pass.username),
            //         ref_policy_id: pass.user_or_policy !== "user" ? pass.username_or_id : null,
            //         type: "user_edit_field",
            //         data: updatable
            //     }]
            //     db('events').insert(storable).catch(err => { console.log(err) })
            // }).catch(err => {
            //     console.log(err)
            //     reject(err)
            // })

        } catch (err) {
            console.log(err)
            reject("Required params not present")
        }
    })
}



const edit_field_claim = (pass) => {
    console.log("edit_field_claim()");
    // 1. Get something
    // 2. Do something else

    return new Promise((resolve, reject) => {
        try {

            // let pass = {
            //     token: req.body.token,
            //     username: req.body.username,
            //     password: req.body.password,
            //     accident_or_claim: req.body.accident_or_claim,
            //     id: req.body.id,
            //     field_name: req.body.field_name,
            //     new_value: req.body.new_value
            // }


            console.log(pass)
            resolve(pass)

            let table = "accidents"
            let where_filter = { id: pass.id }
            let updatable = { [pass.field_name]: pass.new_value }

            if (pass.accident_or_claim !== "accident") {
                table = "claims"
            }
            console.log(table, "table")
            console.log(updatable, JSON.stringify(updatable))
            console.log(where_filter, JSON.stringify(where_filter))

            // UPDATE PENDING_REWORK_ITEMS IN DB AFTER A USER CHANGES A FIELD

            db.transaction((trx) => {
                return trx(table).select("*").where(where_filter).then(rows => {

                    // Checking to see if there is a need to update pending_rework_items
                    let pending_rework_items_new = global_helpers.clone_deep(rows[0].pending_rework_items)
                    var index = pending_rework_items_new.indexOf(pass.field_name);
                    if (index > -1) {
                        pending_rework_items_new.splice(index, 1);
                        updatable.pending_rework_items = JSON.stringify(pending_rework_items_new)       // Add pending_rework_items_new to updatable
                    }

                    // handling updating of upload_images
                    if (pass.field_name === "uploaded_images") {
                        let uploaded_images_old = rows[0].uploaded_images
                        let uploaded_images_new = uploaded_images_old.concat(pass.new_value)
                        updatable.uploaded_images = JSON.stringify(uploaded_images_new)
                    }

                    if (pending_rework_items_new.length === 0) {    // Check if all pending_reworking_items are cleared, then update statuses accordingly
                        if (pass.accident_or_claim === "accident") {
                            //    Make all its claim statuses to pending_rework_approval
                            let updatable_claim_status = { status: "pending_rework_approval" }

                            return trx("claims").update(updatable_claim_status).where({ ref_accident_id: pass.id }).then(result => {

                                // Update user's fields
                                return trx(table).update(updatable).where(where_filter).then(result => {
                                    console.log(result);
                                    resolve(pass)
                                }).catch((err) => { console.error(err); reject(err) })
                            }).catch(err => {
                                console.log(err)
                            })

                        } else {      // pass.accident_or_claim === "claim"
                            //    Just go ahead and change status to pending_rework_approval
                            updatable.status = "pending_rework_approval"
                            // Update only that field
                            return trx(table).update(updatable).where(where_filter).then(result => {
                                console.log(result);
                                resolve(pass)
                            }).catch((err) => { console.error(err); reject(err) })

                            // // Deciding whether to update all policies or not
                            // return trx("claims").select('*').where({ ref_username_norm: global_helpers.normalise_string(pass.username) }).then(rows => {

                            //     let make_all_pending_rework_approval = true
                            //     rows.forEach((ele) => {
                            //         if (ele.pending_rework_items && ele.pending_rework_items.length > 0 && ele.id !== pass.username_or_id) { make_all_pending_rework_approval = false; }
                            //     })

                            //     if (make_all_pending_rework_approval) {
                            //         // Update all
                            //         return trx(table).update({ status: "pending_rework_approval" }).where({ ref_username_norm: global_helpers.normalise_string(pass.username) }).whereNot(where_filter).then(result => {
                            //             return trx(table).update(updatable).where(where_filter).then(result => {
                            //                 console.log(result);
                            //                 resolve(pass)
                            //             }).catch((err) => { console.error(err); reject(err) })
                            //         }).catch((err) => { console.error(err); reject(err) })
                            //     } else {

                            //     }
                            // })
                        }
                    } else {    // If there are still other pending_rework_items, then just update as per normal
                        return trx(table).update(updatable).where(where_filter).then(result => {
                            console.log(result);
                            resolve(pass)
                        }).catch((err) => { console.error(err); reject(err) })
                    }
                })
            }).then(result => {
                let storable_single = {
                    ref_username_norm: global_helpers.normalise_string(pass.username),
                    type: "user_edit_field",
                    data: updatable
                }
                if (pass.accident_or_claim === "accident") { storable_single.ref_accident_id = pass.id } else {
                    storable_single.ref_claim_id = pass.id
                }
                let storable = [storable_single]

                db('events').insert(storable).catch(err => { console.log(err) })
            }).catch(err => {
                console.log(err)
                reject(err)
            })

            // return trx(table).update(updatable).where(where_filter).then(result => {

            // return trx('policies').where({ ref_username_norm: global_helpers.normalise_string(pass.username) }).where({ id: pass.id }).update(updatable_policy)
            //     .then(() => {
            //         // Updating policy status_history
            //         if (updatable_policy.status) {
            //             return trx.raw(`UPDATE policies SET status_history = jsonb_insert(status_history, '{0}','{"created_at":"${new Date().toISOString()}", "status": "${updatable_policy.status}"}') WHERE id = ${pass.id}`)
            //         }
            //     })





            // db(table).update(updatable).where(where_filter).then(result => {

            //     resolve(pass)

            //     ///// Asynchronously Storing in events /////
            //     let storable = [{
            //         ref_username_norm: global_helpers.normalise_string(pass.username),
            //         ref_policy_id: pass.accident_or_claim !== "accident" ? pass.username_or_id : null,
            //         type: "user_edit_field",
            //         data: updatable
            //     }]
            //     db('events').insert(storable).catch(err => { console.log(err) })
            // }).catch(err => {
            //     console.log(err)
            //     reject(err)
            // })

        } catch (err) {
            console.log(err)
            reject("Required params not present")
        }
    })
}



const submit_claim = (pass) => {
    console.log("submit_claim()");
    // 1. Get something
    // 2. Do something else

    return new Promise((resolve, reject) => {
        try {
            if (pass.accident_id === "new") {
                let ref_accident_id = undefined
                let ref_claim_id = undefined

                let accident_new = {
                    ref_username_norm: global_helpers.normalise_string(pass.username),
                    ref_policy_id: pass.policy_id,
                    accident_description: pass.accident_description,
                    accident_date: pass.accident_date ? new Date(pass.accident_date) : null,
                    accident_type: pass.accident_type,
                    product: pass.product,
                }

                let claim_new = {
                    ref_username_norm: global_helpers.normalise_string(pass.username),
                    ref_policy_id: pass.policy_id,
                    clinic_visit_date: pass.clinic_visit_date ? new Date(pass.clinic_visit_date) : null,
                    medical_registrar_smc: pass.medical_registrar_smc,
                    doctor_seen: pass.doctor_seen,
                    clinic_visited: pass.clinic_visited,
                    treatment_description: pass.treatment_description,
                    uploaded_images: JSON.stringify(pass.uploaded_images),
                    addon_notes: pass.addon_notes,
                    status: "pending_approval",
                    status_history: JSON.stringify([{ "status": "pending_approval", "created_at": new Date() }]),
                    image_upload_claim_screenshot: pass.image_upload_claim_screenshot
                }

                db.transaction((trx) => {
                    return trx('accidents').insert(accident_new).returning('id').then(rows => {
                        console.log("accidents OK")
                        ref_accident_id = parseInt(rows[0])
                        claim_new.ref_accident_id = ref_accident_id

                        console.log(claim_new)
                        return trx('claims').insert(claim_new).returning('id').then(rows => {
                            console.log("claims OK")

                            let ref_claim_id = rows[0]

                            resolve(pass)

                            let storable = [{
                                ref_username_norm: global_helpers.normalise_string(pass.username),
                                ref_policy_id: pass.policy_id,
                                ref_accident_id: ref_accident_id,
                                ref_claim_id: ref_claim_id,
                                type: "user_submit_claim",
                                data: {
                                    accident: accident_new,
                                    claim: claim_new
                                }
                            }]
                            db('events').insert(storable).catch(err => { console.log(err) })




                            pass.send_email_email_address = pass.username
                            pass.send_email_subject = "iFinSG Policy Claim request Sent"
                            pass.send_email_message =
                                `
                        <html> 
                        <h2>Hello!</h2>
                                        
                        <h4>Your claim has been successfully submitted.</h4>
                
                        <h4>Please hang tight while we process your claim!</h4>
                        
                        <h4>We will notify you as soon as it has been approved.</h4>
                   
                      
                        <div>Always here for You,</div> 
                        <br>
                        <div>Adele</div>
                        <div>Ops Team</div>
                        <div>www.iFinSG.com</div>
                        <br>
                        <div>Phone: +65 968 986 76</div>
                        <div>Email: adele@ifinancialsingapore.com</div>
                        <div>Facebook: https://www.facebook.com/ifinancialsingapore</div>
                        <div>LinkedIn: https://www.linkedin.com/company/ifinancial-singapore?trk=biz-companies-cym </div>
                        </html>
                        `
                            email.send_email(pass).catch(err => { console.log(err); console.log("EMAIL FAILED!") })





                        }).catch(err => {
                            reject(pass)
                            console.log(err)
                        })
                    })
                }).then(pass => { resolve(pass) }).catch(err => { console.log(err); reject(err) })

                // } else {         // If not null, update
                //     // db('policies').where({ id: pass.policy_id }).insert(updatable)
                //     db.raw(`UPDATE policies SET accidents = jsonb_insert(accidents, '{0}','${JSON.stringify(accident_new)}') WHERE id = ${pass.policy_id}`)
                //         .then(result => {
                //             resolve(pass)
                //             console.log(result)
                //         }).catch(err => {
                //             reject(pass)
                //             console.log(err)
                //         })
                // }
                // })

            } else {
                console.log("Trying to process previously recorded accident " + pass.accident_id)

                try {
                    let claim_new = {
                        ref_username_norm: global_helpers.normalise_string(pass.username),
                        ref_policy_id: pass.policy_id,
                        clinic_visit_date: new Date(pass.clinic_visit_date),
                        medical_registrar_smc: pass.medical_registrar_smc,
                        doctor_seen: pass.doctor_seen,
                        clinic_visited: pass.clinic_visited,
                        treatment_description: pass.treatment_description,
                        uploaded_images: JSON.stringify(pass.uploaded_images),
                        addon_notes: pass.addon_notes,
                        status: "pending_approval",
                        status_history: JSON.stringify([{ "status": "pending_approval", "created_at": new Date() }]),
                        image_upload_claim_screenshot: pass.image_upload_claim_screenshot

                    }

                    if (!pass.accident_id) { reject("ERROR - accident_id not defined!"); return }
                    claim_new.ref_accident_id = pass.accident_id

                    console.log(claim_new)
                    db('claims').insert(claim_new).returning('id').then(rows => {
                        console.log("accidents OK")
                        ref_claim_id = rows[0]
                        claim_new.ref_accident_id = pass.accident_id

                        console.log("claims OK")

                        ref_claim_id = rows[0]

                        resolve(pass)

                        let storable = [{
                            ref_username_norm: global_helpers.normalise_string(pass.username),
                            ref_policy_id: pass.policy_id,
                            ref_accident_id: pass.accident_id,
                            ref_claim_id: ref_claim_id,
                            type: "user_submit_claim",
                            data: {
                                claim: claim_new
                            }
                        }]
                        db('events').insert(storable).catch(err => { console.log(err) })


                        pass.send_email_email_address = pass.username
                        pass.send_email_subject = "iFinSG Policy Claim request Sent"
                        pass.send_email_message =
                            `
                    <html>
                    <h2>Hello!</h2>
                                         
                    <h4>Your claim has been successfully submitted.</h4>
            
                    <h4>Please hang tight while we process your claim!</h4>
                    
                    <h4>We will notify you as soon as it has been approved.</h4>
               
                  
                    <div>Always here for You,</div> 
                    <br>
                    <div>Adele</div>
                    <div>Ops Team</div>
                    <div>www.iFinSG.com</div>
                    <br>
                    <div>Phone: +65 968 986 76</div>
                    <div>Email: adele@ifinancialsingapore.com</div>
                    <div>Facebook: https://www.facebook.com/ifinancialsingapore</div>
                    <div>LinkedIn: https://www.linkedin.com/company/ifinancial-singapore?trk=biz-companies-cym </div>
                    </html>
                    `
                        email.send_email(pass).catch(err => { console.log(err); console.log("EMAIL FAILED!") })



                    }).catch(err => {
                        reject(pass)
                        console.log(err)
                    })
                } catch (err) {
                    console.log(err)
                }

                // } else {         // If not null, update
                //     // db('policies').where({ id: pass.policy_id }).insert(updatable)
                //     db.raw(`UPDATE policies SET accidents = jsonb_insert(accidents, '{0}','${JSON.stringify(accident_new)}') WHERE id = ${pass.policy_id}`)
                //         .then(result => {
                //             resolve(pass)
                //             console.log(result)
                //         }).catch(err => {
                //             reject(pass)
                //             console.log(err)
                //         })
                // }
                // })

            }



            // db.transaction((trx) => {
            //     return trx(table).select("*").where(where_filter).then(rows => {

            //     })
            // }).then(result => {
            //     let storable = [{
            //         ref_username_norm: global_helpers.normalise_string(pass.username),
            //         ref_policy_id: pass.accident_or_claim !== "accident" ? pass.username_or_id : null,
            //         type: "user_edit_field",
            //         data: updatable
            //     }]
            //     db('events').insert(storable).catch(err => { console.log(err) })
            // }).catch(err => {
            //     console.log(err)
            //     reject(err)
            // })

        } catch (err) {
            console.log(err)
            reject("Required params not present")
        }
    })
}


const cancel_or_reinstate = (pass) => {
    console.log("cancel_or_reinstate()");
    // 1. Get something
    // 2. Do something else

    return new Promise((resolve, reject) => {
        try {

            // token: req.body.token,
            // username: req.body.username,
            // policy_id: req.body.policy_id,
            // policy_status: req.body.policy_status,
            // message: req.body.message,

            console.log(pass)
            console.log("THIS IS THE PASSABLEEEEeeeeeeeeeeeeeeeeeeeeeeeee ")
            db('policies').where({ id: pass.policy_id }).update({ status: pass.policy_status })
            db.transaction((trx) => {
                return trx('policies').where({ id: pass.policy_id }).update({
                    status: pass.policy_status,
                    flag_staff_inbox_category: pass.cancel_or_reinstate === "reinstate" ? "pending_reinstatement" : "pending_cancel",
                }).then(rows => {
                    // Updating policy status_history
                    return trx.raw(`UPDATE policies SET status_history = jsonb_insert(status_history, '{0}','{"created_at":"${new Date().toISOString()}", "status": "${pass.policy_status}"}') WHERE id = ${pass.policy_id}`).then(rows => {
                        let storable = [{
                            ref_username_norm: global_helpers.normalise_string(pass.username),
                            ref_policy_id: pass.policy_id,
                            type: `user_${pass.cancel_or_reinstate}_policy`,
                            data: {
                                message: pass.message
                            }
                        }]
                        return trx('events').insert(storable).catch(err => { console.log(err) })
                    })


                }).catch(err => {
                    reject(pass)
                    console.log(err)
                })
            }).then(pass => {

                resolve(pass)

                if (pass.cancel_or_reinstate === "reinstate") {

                    pass.send_email_email_address = pass.username
                    pass.send_email_subject = "Policy Reinstatement Request Successfully Submitted"
                    pass.send_email_message =
                        `
                <html>                    
                <h2>Hello!</h2>

                <h4>Your policy reinstatement has been successfully submitted.</h4>
        
                <h4>Please hang tight while we process your application!</h4>
                
                <h4>We will notify you as soon as it has been approved.</h4>
           
              
                <div>Always here for You,</div> 
                <br>
                <div>Adele</div>
                <div>Ops Team</div>
                <div>www.iFinSG.com</div>
                <br>
                <div>Phone: +65 968 986 76</div>
                <div>Email: adele@ifinancialsingapore.com</div>
                <div>Facebook: https://www.facebook.com/ifinancialsingapore</div>
                <div>LinkedIn: https://www.linkedin.com/company/ifinancial-singapore?trk=biz-companies-cym </div>
                </html>
                `
                    email.send_email(pass)
                }

            }).catch(err => { console.log(err); reject(err) })
        } catch (err) {
            console.log(err)
            reject("Required params not present")
        }
    })
}


module.exports = {
    purchase,
    edit_field,
    edit_field_claim,
    submit_claim,
    cancel_or_reinstate

};


