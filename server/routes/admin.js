const express = require("express");
var bodyParser = require("body-parser"); //https://stackoverflow.com/questions/5710358/how-to-retrieve-post-query-parameters
const router = express.Router();

const global_constants = require("../../global/constants");
const global_helpers = require("../../global/helpers");
// const server = express()
// server.use(bodyParser.urlencoded({ extended: false }));

const admin_actions = require("../helpers/admin_actions");
const auth = require("../helpers/auth");
// const email = require('../helpers/email');
const get_data = require("../helpers/get_data");

router.get("/sample_api", (req, res) => {
    // 1. Do something
    // 2. Do something else

    if (req.body.param) {
        let pass = {
            param: req.body.param
        };

        helper
            .helper_function(pass)
            .then((pass) => {
                res.status(200).json({
                    param: "param1"
                });
            })
            .catch((err) => {
                if (err.err) {
                    res.status(200).send(err);
                } else {
                    console.log(err);
                    res.status(400).send(err);
                }
            });
    } else {
        res.status(400).send("ERROR - url parameters not valid");
    }
});

router.get("/", (req, res) => {
    res.json({
        hello: "hello"
    });
});

router.post("/getTableData", (req, res) => {
    // 1. Do something
    // 2. Do something else

    try {
        let pass = {
            token: req.body.token,
            table: req.body.table,
            table_variant: req.body.table_variant,
            date_start: req.body.date_start,
            date_end: req.body.date_end
        };

        console.log("getTableData");
        // console.log(pass)

        auth.verify_token(pass)
            .then((pass) => {
                if (
                    global_constants.list_of_admins.indexOf(
                        global_helpers.normalise_string(pass.verify_token_username)
                    ) !== -1
                ) {
                    // if (true) {
                    return get_data.get_table_data(pass);
                } else {
                    res.status(200).json({
                        success: false,
                        err: "ERROR - User has insufficient rights to access this service",
                        err_code: "insufficient_rights"
                    });
                }
            })
            .then((pass) => {
                res.status(200).json(pass.get_table_data_data ? pass.get_table_data_data : {});
            })
            .catch((err) => {
                if (err.err) {
                    res.status(200).json({
                        success: false,
                        err: "ERROR - User has insufficient rights to access this service",
                        err_code: "insufficient_rights"
                    });
                } else {
                    console.log(err);
                    res.status(400).send(err);
                }
            });
    } catch (err) {
        res.status(400).send("ERROR - url parameters not valid");
    }
});

router.post("/pendingApprovalAction", (req, res) => {
    // 1. Do something
    // 2. Do something else

    try {
        let pass = {
            token: req.body.token,
            accident_or_claim: req.body.accident_or_claim,
            signups_or_claims: req.body.signups_or_claims,
            pending_approval_action: req.body.pending_approval_action,
            username: req.body.username,
            user_or_policy: req.body.user_or_policy,
            id: req.body.id,
            pending_rework_items: req.body.pending_rework_items,
            pending_rework_message: req.body.pending_rework_message,
            amount: req.body.amount,
            coupon_name: req.body.coupon_name,
            policy: req.body.policy
        };

        console.log("pendingApprovalAction");

        auth.verify_token(pass)
            .then((pass) => {
                // if (global_helpers.normalise_string(pass.verify_token_username) === global_helpers.normalise_string(pass.username)) {
                if (
                    global_helpers.normalise_string(pass.verify_token_username) ===
                        global_helpers.normalise_string(pass.username) ||
                    global_constants.list_of_admins.indexOf(
                        global_helpers.normalise_string(pass.verify_token_username)
                    ) !== -1
                ) {
                    return admin_actions.pending_approval_action(pass);
                } else {
                    res.status(200).json({
                        success: false,
                        err: "ERROR - User has insufficient rights to access this service",
                        err_code: "insufficient_rights"
                    });
                }
            })
            .then((pass) => {
                res.status(200).json(pass);
            })
            .catch((err) => {
                console.log(err);
                if (err.err) {
                    res.status(200).json({
                        success: false,
                        err: "ERROR - User has insufficient rights to access this service",
                        err_code: "insufficient_rights"
                    });
                } else {
                    console.log(err);
                    res.status(400).send(err);
                }
            });
    } catch (err) {
        console.log(err);
        res.status(400).send("ERROR - url parameters not valid");
    }
});

module.exports = router;
