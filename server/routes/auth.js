const express = require('express')
const router = express.Router()
var jwt = require("jsonwebtoken"); // used to create, sign, and verify tokens
const db = require("../helpers/pg").db;

const auth = require("../helpers/auth");

const global_constants = require("../../global/constants");
const global_helpers = require("../../global/helpers");

////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// APIs /////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

router.get('/', (req, res) => {
    res.json({
        test: "OK"
    })
})


router.post('/login', (req, res) => {
    console.log("API - api/auth/login")
    // 1. Verify credentials
    // 2. Sign token
    // 3. Store credentials into DB

    try {

        let pass = {
            username: req.body.login_email,
            password: req.body.login_password
        }

        auth.verify_credentials(pass).then(pass => {
            return auth.sign_token(pass)
        }).then(pass => {
            // console.log(pass)

            // Determining personal_info_already_in_db by looking at whether user has personal info data in
            let personal_info_already_in_db = false
            if (pass.user_info.full_name || pass.user_info.nric_number) {   // If user has already filled in personal details,
                personal_info_already_in_db = true
            }

            res.status(200).json({
                token: pass.token,
                personal_info_already_in_db,
                user_info: pass.user_info
            });

        }).catch(err => {
            if (err.err) {
                res.status(200).send(err)
            } else {
                console.log(err)
                res.status(400).send(err)
            }
        })

    } catch (err) {
        res.status(400).send("Unable to receive user credentials");
    }
})



router.post('/getUserDataFromToken', (req, res) => {

    console.log("API - api/auth/getUserDataFromToken")

    // 1. Verify user token
    // 2. Get associated username from token
    // 3. Get user data from username

    try {
        let pass = {
            token: req.body.token,
            get_policies: req.body.get_policies,
            get_claims: req.body.get_claims,
        }

        if (pass.token === undefined || pass.token === '') {
            res.json({
                error: 'No token'
            })
        } else {

            console.log(pass.token)
            console.log("THAT WAS pass.token")

            auth.verify_token(pass).then(pass => {

                // db.from('users').select('*').where({ username_norm: global_helpers.normalise_string(pass.verify_token_username) }).where({ email_verified: true }).then(rows => {

                db.select(db.raw(`to_json(policies.*) as policy `), db.raw('to_json(users.*) as user'))
                    .from('policies')
                    .where({ username_norm: global_helpers.normalise_string(pass.verify_token_username) })
                    .where({ email_verified: true })
                    .where(function (builder) {
                        // if (pass.date_start) { builder.andWhere('created_at', '>=', pass.date_start) }
                        // if (pass.date_end) { builder.andWhere('created_at', '<', pass.date_end) }
                    }).limit(pass.limit)
                    .join('users', 'users.username_norm', 'policies.ref_username_norm')
                    .then(rows => {

                        if (rows.length == 0) {

                            db.select('*').from('users').where({ username_norm: global_helpers.normalise_string(pass.verify_token_username) }).then(rows => {
                                delete rows[0].password_hash
                                if (rows.length === 0) {
                                    res.status(200).json({
                                        success: false,
                                        err: "Unable to get user from token",
                                    });
                                } else {
                                    let user_info = rows[0]
                                    user_info.bank = user_info.bank + " - " + user_info.bank_acct_number

                                    user_info.policies = []
                                    // Determining personal_info_already_in_db by looking at whether user has personal info data in
                                    let personal_info_already_in_db = false
                                    if (user_info.full_name || user_info.nric_number) {   // If user has already filled in personal details,
                                        personal_info_already_in_db = true
                                    }

                                    res.status(200).json({
                                        personal_info_already_in_db,
                                        user_info,
                                    });
                                }
                            }).catch(err => {
                                console.log(err)
                                res.status(400).json({
                                    success: false,
                                    error: "Unknown error",
                                    err: "unknown_error",
                                });
                            })


                        } else {
                            let user_info = rows[0].user
                            user_info.bank = user_info.bank + " - " + user_info.bank_acct_number

                            user_info.policies = []

                            for (let i = 0; i < rows.length; i++) {
                                let policy = rows[i]
                                let policy_json_and_array = global_helpers.policy_data_to_json_and_array(policy, i)
                                user_info.policies.push(policy_json_and_array.json)
                            }

                            // Determining personal_info_already_in_db by looking at whether user has personal info data in
                            let personal_info_already_in_db = false
                            if (user_info.full_name || user_info.nric_number) {   // If user has already filled in personal details,
                                personal_info_already_in_db = true
                            }

                            res.status(200).json({
                                personal_info_already_in_db,
                                user_info,
                            });
                        }

                    }).catch(err => { console.log(err); res.status(400).send(err) })

            }).catch(err => {
                if (err.err) {
                    res.status(200).json(err)
                } else {
                    console.log(err)
                    res.status(400).send(err)
                }
            })
        }



    } catch (err) {
        res.status(400).send("Unable to receive user credentials");
    }
})



router.post('/signup', (req, res) => {

    // 1. Verify credentials
    // 2. Sign token
    // 3. Store credentials into DB

    try {
        let pass = {
            username: req.body.login_email,
            password: req.body.signup_password,
            referrer: req.body.referrer,
        }
        // console.log(pass)
        auth.signup(pass).then(pass => {
            res.status(200).json({
                username: pass.username,
                token: pass.token
            });
        }).catch(err => {
            if (err.err) {
                res.status(200).send(err)
            } else {
                console.log(err)
                res.status(400).send(err)
            }
        })

    } catch (err) {
        res.status(400).send("ERROR - url parameters not valid")
    }

})



router.post('/verifyEmail', (req, res) => {

    // 1. Verify credentials
    // 2. Sign token
    // 3. Store credentials into DB

    try {
        let pass = {
            username: req.body.signup_username,
            email_verification_code: req.body.email_verification_code
        }
        // console.log(pass)
        auth.verify_email_verification_code(pass).then(pass => {
            res.status(200).json({
                verification_result: 1,
                token: pass.token
            });
        }).catch(err => {
            if (err.err) {
                res.status(200).send(err)
            } else {
                console.log(err)
                res.status(400).send(err)
            }
        })

    } catch (err) {
        res.status(400).send("ERROR - url parameters not valid")
    }

})


router.post('/changePassword', (req, res) => {
    console.log("API - api/auth/changePassword")
    // 1. Verify token
    // 2. Verify credentials
    // 3. Change generate password_hash & store

    try {

        let pass = {
            token: req.body.token,
            password: req.body.password_old,
            password_new: req.body.password_new
        }

        // console.log(pass)

        auth.verify_token(pass).then(pass => {
            pass.username = pass.verify_token_username
            return auth.verify_credentials(pass)
        }).then(pass => {
            pass.change_password_username = pass.username
            return auth.change_password(pass)
        }).then(pass => {
            res.status(200).json({
                result: "OK"
            });
        }).catch(err => {
            if (err.err) {
                res.status(200).send(err)
            } else {
                console.log(err)
                res.status(400).send(err)
            }
        })

    } catch (err) {
        res.status(400).send("Unable to receive user credentials");
    }
})


router.post('/getForgetPasswordEmailLink', (req, res) => {
    console.log("API - api/auth/getForgetPasswordEmailLink")
    // 1. Verify token
    // 2. Verify credentials
    // 3. Change generate password_hash & store

    try {

        let pass = {
            send_forget_password_email_link_username: req.body.login_email,
        }

        // console.log(pass)

        auth.send_forget_password_email_link(pass).then(pass => {

            res.status(200).json({
                result: "OK"
            });
        }).catch(err => {
            if (err.err) {
                res.status(200).send(err)
            } else {
                console.log(err)
                res.status(400).send(err)
            }
        })

    } catch (err) {
        res.status(400).send("Unable to receive user paramerters");
    }
})


router.post('/resetPassword', (req, res) => {
    console.log("API - api/auth/resetPassword")
    // 1. Verify email_verification_code
    // 2. Change generate password_hash & store

    try {

        let pass = {
            password_new: req.body.password_new,
            forget_password_verification_code: req.body.forget_password_verification_code
        }

        // console.log(pass)

        auth.verify_forget_password_verification_code(pass).then(pass => {
            pass.change_password_username = pass.forget_password_verification_code.split('_')[0]
            return auth.change_password(pass)
        }).then(pass => {
            res.status(200).json({
                result: "OK"
            });

        }).catch(err => {
            if (err.err) {
                res.status(200).send(err)
            } else {
                console.log(err)
                res.status(400).send(err)
            }
        })

    } catch (err) {
        res.status(400).send("Unable to receive user credentials");
    }
})


module.exports = router;