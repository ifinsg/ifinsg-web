const express = require('express');
const path = require('path');
const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
const bodyParser = require('body-parser'); //https://stackoverflow.com/questions/5710358/how-to-retrieve-post-query-parameters
const router = express.Router();

const auth = require('../helpers/auth');

const Multer = require('multer');

const global_constants = require('../../global/constants');
const global_helpers = require('../../global/helpers');

const crypto = require('crypto');
// const fs = require("fs");

///////////////////////////////////////////////////////////////////////////
////////////////////////////////// APIs ///////////////////////////////////
///////////////////////////////////////////////////////////////////////////

const { Storage } = require('@google-cloud/storage');

const keyFilename = path.join(__dirname, '../../credentials.json');
console.log('🚀 ~ file: image.js ~ line 24 ~ keyFilename', keyFilename);

const storage = new Storage({
    projectId: 'ifinsgdev2',
    keyFilename: keyFilename
});

const multer = Multer({
    storage: Multer.memoryStorage(),
    limits: {
        fileSize: 50 * 1024 * 1024 // no larger than 5mb, you can change as needed.
    }
});

let bucket_name = null;
let bucket = null;
if (process.env.NODE_ENV === 'production') {
    bucket_name = 'ifinsgdev2-live-users-images';
    bucket = storage.bucket(bucket_name);
} else {
    bucket_name = 'ifinsgdev2-staging-users-images';
    bucket = storage.bucket(bucket_name);
}

// Process the file upload and upload to Google Cloud Storage.
router.post('/upload', multer.single('file'), (req, res, next) => {
    console.log('upload');
    // console.log(req)
    // console.log(req.body)

    // if (req.body.file) {
    //     req.file = req.body.file;
    // }
    if (!req.file) {
        console.log('No file uploaded');

        res.status(400).send('No file uploaded.');
        return;
    }

    let username = 'UNKNOWN';
    if (req.body && req.body.username) {
        try {
            username = global_helpers.normalise_string(req.body.username);
        } catch (err) {
            console.log(err);
        }
    } else if (req.headers && req.headers.username) {
        // For screenshots, somehow unable to put username in body :/
        try {
            username = global_helpers.normalise_string(req.headers.username);
        } catch (err) {
            console.log(err);
        }
    }

    let image_type = 'TYPE';
    if (req.body && req.body.image_type) {
        try {
            image_type = global_helpers.normalise_string(req.body.image_type);
        } catch (err) {
            console.log(err);
        }
    } else if (req.headers && req.headers.image_type) {
        // For screenshots, somehow unable to put type in body :/
        try {
            image_type = global_helpers.normalise_string(req.headers.image_type);
        } catch (err) {
            console.log(err);
        }
    }

    let file_name =
        username + '_' + image_type + '_' + new Date(new Date().getTime() + 8 * 60 * 60 * 1000).toISOString(); // Eg: 2019-03-29T15:57:47.989Z in local (Singapore) time
    file_name = file_name + '_' + parseInt(Math.random() * 10000);
    file_name = file_name + '_' + req.file.originalname.replace(new RegExp(' ', 'g'), '-'); // Replacing spaces with -

    const blob = bucket.file(file_name);
    const blobStream = blob.createWriteStream();

    blobStream.on('error', (err) => {
        console.log(err);
        next(err);
    });

    blobStream.on('finish', () => {
        console.log('finish');
        // const publicUrl = format(`https://storage.googleapis.com/sat-forms-images/${blob.name}`);    // If gcloud storage storage privacy status set to public

        res.status(200).send(file_name);
    });

    blobStream.end(req.file.buffer);
});

router.post('/getFileSignedUrl', function(req, res, next) {
    console.log('getFileSignedUrl');

    let pass = {
        token: req.body.token,
        file_names: req.body.file_names,
        username: req.body.username
    };

    console.log(pass);

    auth.verify_token(pass)
        .then((pass) => {
            if (
                global_helpers.normalise_string(pass.verify_token_username) ===
                    global_helpers.normalise_string(pass.username) ||
                global_constants.list_of_admins.indexOf(global_helpers.normalise_string(pass.verify_token_username)) !==
                    -1
            ) {
                let signed_urls = [];
                // Generates a signed url which lasts for seconds_to_expire seconds
                // Sources: https://cloud.google.com/storage/docs/access-control/signing-urls-manually && https://stackoverflow.com/questions/20754279/creating-signed-urls-for-google-cloud-storage-using-nodejs

                let seconds_to_expire = 15;
                let expiry = parseInt(new Date().getTime() / 1000 + seconds_to_expire); // Units in seconds
                let accessId = '100745978904445892629'; // client_id
                // let privateKey = "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCiD0wR/3eetDB2\neE1sFytu06fEi7xfZxiXNJLaLPJDwXIci5ruWEFh16x7HMhp8iTYi7GvaWjPiPZ/\nBuoglSzfSAidyz+18IK6dsuSoUuScZRErFpmICAE0nYqb5vR3z79uiAR05rizF1T\nU3ZxGYcUM6Fu1wTf8oMcq3G4/BFI57BVkPtNXnIwdBx+chfFFayB7uSCioe1VWjk\n7vWP5Pxjp5Pw0WJZ68JXlS20hCV4DhLAkkwGg65I+5Mnn/1csQVP+zjOuU2hUjaI\nKP3PPmaNS242tCt7vU1IKCHAvpxdX4iAAAub46OuqndLr0taPeF1gYmQXA7TLKsX\nVbyzl9RlAgMBAAECggEABkqB7iu7IonyXN76ezw10oV2SUuqLw/q/sLeAojLiZrP\nibeK3T7HoJTSD5Nqw9+2oEHIAe9QtmG/cLRXHJdKslfZa2Eele41icFSN/Z5kkB4\nSRqU7DFjqZxCVXWXGG4XEJvYyKhf8yolloL/OyokOfU8V14S3kV+40fttoi+fDrf\n9XKZZVqzoaAXTA/mvD1OlHo4dVCNrWRpuzRtnX51YeY3mGrhZ3OsrvNzE7f5uUGC\nQqsfsGMG36SU7TS7IneaiRADG+6tt8Y3WA0yACv/uvSLCEfi21gCVdOBsxqJFKBF\nJ6jgc0RthBW7aPTpVaTwm7y4vC0eK/G1JwCPWX6E/wKBgQDjTMJL65zQgnbYsYbU\n3Kl8ApKz5F7CsD5K8v9WHrXDTl5d4W/H4I+xJSXb6wYEpIpwrybVYqDfxTYiAT3d\nw4+xpyRFYZZO9yGkOL0lnbVxIkTRY8ZAJa/g9Bz7YPnRfFH6Oyk/WJ4AeA/+0iR1\n/H2u4XO5TiUjA/I2SLkzZFZalwKBgQC2hbelzAbSnvQlXbqCQd0ovarDrX63MH2m\nxeqriPb8bj+h2JfolgJ8vg8J519+ZOyQK01XMZ1ZaRHjzaLm1fbVJauTBeq1zpeo\n9EBHWN6M0B3Jq4r7JC0SYtnxEm9VUqS02HPkgkuv0gD3vH77EJWEaKvof9ohSREw\nuSWc0LkUYwKBgQDfX67N12ULUDslZwkfJR92HD05hmNNptAA7d7aLmHn5ps8XXBx\nmBD+QIh9H/GRHHZkmOxr5Xo9jlxNfLTku+v55Uzy1h1Vyu4Hqi7PybXm0YuZzxik\nL6UxSBU3QS0gnrEdXYiClT7pLRRjEvb78D8/XpZQdChwyc79Y+iFRcTLTwKBgBIt\niRfDFR7e49Pg3NViN+N52Er6uXcHJkx4w6Fn16lWVZIJ3z8zyXWg/CBxYapV5NWK\n6n33nAMumrCEZ2f0AUxDHy5aVYDb3XDfOpOgfzGo/H58TjAq7y44mBoRSvNFcs/s\nCsbET0Nod940cLUSdsGkJxLVwOS1wTeICP3wL66fAoGAOnX/tRqwO3zz0Y9IfVKW\n1iRPeGIf9eDYQMLohw7nELnFfbPmyLZgo9fl7bGiRvrBPwcK+GxbElOADnHGPxxx\nXPfqDY0XUyFFc++iNb9B1RdlP/HD+6/dwU1gKVzOHpyGhTXrs3bOIKcyzgXStg8T\nxV1O5xye4UQ6a8RkOKD0aXc=\n-----END PRIVATE KEY-----\n"
                let privateKey =
                    '-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCiD0wR/3eetDB2\neE1sFytu06fEi7xfZxiXNJLaLPJDwXIci5ruWEFh16x7HMhp8iTYi7GvaWjPiPZ/\nBuoglSzfSAidyz+18IK6dsuSoUuScZRErFpmICAE0nYqb5vR3z79uiAR05rizF1T\nU3ZxGYcUM6Fu1wTf8oMcq3G4/BFI57BVkPtNXnIwdBx+chfFFayB7uSCioe1VWjk\n7vWP5Pxjp5Pw0WJZ68JXlS20hCV4DhLAkkwGg65I+5Mnn/1csQVP+zjOuU2hUjaI\nKP3PPmaNS242tCt7vU1IKCHAvpxdX4iAAAub46OuqndLr0taPeF1gYmQXA7TLKsX\nVbyzl9RlAgMBAAECggEABkqB7iu7IonyXN76ezw10oV2SUuqLw/q/sLeAojLiZrP\nibeK3T7HoJTSD5Nqw9+2oEHIAe9QtmG/cLRXHJdKslfZa2Eele41icFSN/Z5kkB4\nSRqU7DFjqZxCVXWXGG4XEJvYyKhf8yolloL/OyokOfU8V14S3kV+40fttoi+fDrf\n9XKZZVqzoaAXTA/mvD1OlHo4dVCNrWRpuzRtnX51YeY3mGrhZ3OsrvNzE7f5uUGC\nQqsfsGMG36SU7TS7IneaiRADG+6tt8Y3WA0yACv/uvSLCEfi21gCVdOBsxqJFKBF\nJ6jgc0RthBW7aPTpVaTwm7y4vC0eK/G1JwCPWX6E/wKBgQDjTMJL65zQgnbYsYbU\n3Kl8ApKz5F7CsD5K8v9WHrXDTl5d4W/H4I+xJSXb6wYEpIpwrybVYqDfxTYiAT3d\nw4+xpyRFYZZO9yGkOL0lnbVxIkTRY8ZAJa/g9Bz7YPnRfFH6Oyk/WJ4AeA/+0iR1\n/H2u4XO5TiUjA/I2SLkzZFZalwKBgQC2hbelzAbSnvQlXbqCQd0ovarDrX63MH2m\nxeqriPb8bj+h2JfolgJ8vg8J519+ZOyQK01XMZ1ZaRHjzaLm1fbVJauTBeq1zpeo\n9EBHWN6M0B3Jq4r7JC0SYtnxEm9VUqS02HPkgkuv0gD3vH77EJWEaKvof9ohSREw\nuSWc0LkUYwKBgQDfX67N12ULUDslZwkfJR92HD05hmNNptAA7d7aLmHn5ps8XXBx\nmBD+QIh9H/GRHHZkmOxr5Xo9jlxNfLTku+v55Uzy1h1Vyu4Hqi7PybXm0YuZzxik\nL6UxSBU3QS0gnrEdXYiClT7pLRRjEvb78D8/XpZQdChwyc79Y+iFRcTLTwKBgBIt\niRfDFR7e49Pg3NViN+N52Er6uXcHJkx4w6Fn16lWVZIJ3z8zyXWg/CBxYapV5NWK\n6n33nAMumrCEZ2f0AUxDHy5aVYDb3XDfOpOgfzGo/H58TjAq7y44mBoRSvNFcs/s\nCsbET0Nod940cLUSdsGkJxLVwOS1wTeICP3wL66fAoGAOnX/tRqwO3zz0Y9IfVKW\n1iRPeGIf9eDYQMLohw7nELnFfbPmyLZgo9fl7bGiRvrBPwcK+GxbElOADnHGPxxx\nXPfqDY0XUyFFc++iNb9B1RdlP/HD+6/dwU1gKVzOHpyGhTXrs3bOIKcyzgXStg8T\nxV1O5xye4UQ6a8RkOKD0aXc=\n-----END PRIVATE KEY-----\n';

                // let privateKey = fs.readFileSync("ifinsgdev2_credentials.json",  (err, file_contents) => { if (err) { console.log(err) } else { console.log(file_contents); return file_contents.privateKey } })
                let bucketName = bucket_name;

                for (let i = 0; i < pass.file_names.length; i++) {
                    // let key = 'JIEXIONGKHOO@GMAIL.COM_SIGNUP-IMAGE_2019-05-29T10:36:32.769Z_5128_Capture.JPG';
                    let key = pass.file_names[i]; //.replace(/(\s+)/g, '\\$1')
                    // console.log(pass.file_names)
                    // console.log(input.path
                    // console.log(key)
                    let stringPolicy = 'GET\n' + '\n' + '\n' + expiry + '\n' + '/' + bucketName + '/' + key;
                    let signature = encodeURIComponent(
                        crypto
                            .createSign('sha256')
                            .update(stringPolicy)
                            .sign(privateKey, 'base64')
                    );
                    console.log(signature);
                    console.log('stringPolicy - ' + stringPolicy);
                    console.log('expiry - ' + expiry);
                    console.log('bucketName - ' + bucketName);
                    console.log('key - ' + key);

                    let signedUrl =
                        'https://' +
                        bucketName +
                        '.commondatastorage.googleapis.com/' +
                        key +
                        '?GoogleAccessId=' +
                        accessId +
                        '&Expires=' +
                        expiry +
                        '&Signature=' +
                        signature;
                    signed_urls.push(signedUrl);
                    console.log(signedUrl);
                }

                res.status(200).json({
                    success: true,
                    signed_urls,
                    expiry: expiry * 1000
                });

                //   var stream = bucket.file(pass.file_name).createReadStream();
                //   res.writeHead(200, { 'Content-Type': 'image/jpg' });
                //   stream.on('data', function (data) {
                //     console.log('data');
                //     res.write(data);
                //   });
                //   stream.on('error', function (err) {
                //     console.log('error reading stream', err);
                //   });
                //   stream.on('end', function () {
                //     console.log('end!');
                //     res.end();
                //   });
            } else {
                res.status(200).json({
                    success: false,
                    err: 'insufficient_rights',
                    error:
                        'Unable to download file due to insufficient rights. Please contact admin if you believe you are authorised to download this file.'
                });
            }
        })
        .catch((err) => {
            console.log(err);
            res.status(400).json({
                success: false,
                err: err,
                error: 'Unknown error'
            });
        });
});

module.exports = router;
