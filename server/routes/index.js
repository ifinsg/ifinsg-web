const express = require("express");
var bodyParser = require("body-parser"); //https://stackoverflow.com/questions/5710358/how-to-retrieve-post-query-parameters
const router = express.Router();

const global_constants = require("../../global/constants");
const global_helpers = require("../../global/helpers");
// const server = express()
// server.use(bodyParser.urlencoded({ extended: false }));

const user_actions = require("../helpers/user_actions");
const auth = require("../helpers/auth");
const email = require("../../email/app");
const get_data = require("../helpers/get_data");
const db = require("../helpers/pg").db;

// const MongoClient = require('mongodb').MongoClient;
// // const uri = "mongodb+srv://ifinsgdev2:ifinsgdev2Mongo!@cluster0-ba1cr.gcp.mongodb.net/test?retryWrites=true&w=majority";
// const uri = "mongodb+srv://ifinsgdev2:ifinsgdev2Mongo!@cluster0-ba1cr.gcp.mongodb.net/test?&w=majority";
// const client = new MongoClient(uri, { useNewUrlParser: true });
// // const client = new MongoClient(uri);

// client.connect(err => {
//   const collection = client.db("test").collection("devices");
//   // perform actions on the collection object
//   client.close();
// });

// const mongoose = require('mongoose');
// mongoose.connect('mongodb://ifinsgdev2:.G1raffe@cluster0-ba1cr.gcp.mongodb.net/test?retryWrites=true&w=majority', { useNewUrlParser: true });
// const Schema = mongoose.Schema;
// const ObjectId = Schema.ObjectId;

// const BlogPost = new Schema({
//     author: ObjectId,
//     title: String,
//     body: String,
//     date: Date
// });

// router.get('/mongo', (req, res) => {
//     var post = new BlogPost();
//     post.comments.push({ title: 'My comment' });
//     post.save(function (err) {
//         if (err) {
//             res.status(400).json({
//                 success: false,
//             })
//         } else {
//             console.log('Success!');
//             res.status(200).json({
//                 success: true,
//             })
//         }
//     });
// })

router.get("/sample_api", (req, res) => {
    // 1. Do something
    // 2. Do something else
    // axios.get('/api/sample_api')
    console.log("sample_api", req);

    if (req.query) {
        let pass = {
            param: req.query
        };

        helper
            .helper_function(pass)
            .then((pass) => {
                res.status(200).json({
                    param: "param1"
                });
            })
            .catch((err) => {
                if (err.err) {
                    res.status(200).send(err);
                } else {
                    console.log(err);
                    res.status(400).send(err);
                }
            });
    } else {
        res.status(400).send("ERROR - url parameters not valid");
    }
});

router.get("/", (req, res) => {
    res.json({
        hello: "hello"
    });
});

router.post("/purchase", (req, res) => {
    // 1. Do something
    // 2. Do something else

    if (req.body.purchase) {
        let pass = {
            purchase: req.body.purchase,
            token: req.body.purchase.token
        };

        auth.verify_token(pass)
            .then((pass) => {
                if (
                    global_helpers.normalise_string(pass.verify_token_username) ===
                    global_helpers.normalise_string(pass.purchase.username)
                ) {
                    return user_actions.purchase(pass);
                } else {
                    console.log(global_helpers.normalise_string(pass.verify_token_username));
                    console.log(global_helpers.normalise_string(pass.purchase.username));
                    console.log("USERNAMES NOT EQUAL!!");

                    // PromiseRejectionEvent({
                    //     err: "ERROR - Unable to verify user credentials. Please contact admin",
                    //     err_code: "user_credentials_conflict_with_db",
                    //     success: false
                    // })

                    res.status(200).send({
                        success: false,
                        err: "ERROR - Unable to verify user credentials. Please try again, else contact admin",
                        err_code: "credentials_invalid"
                    });
                }
            })
            .then((pass) => {
                try {
                    res.status(200).json({
                        status: "Successful"
                    });
                } catch (err) {
                    console.log(err);
                }
            })
            .catch((err) => {
                if (err.err) {
                    res.status(200).json(err);
                } else {
                    console.log(err);
                    res.status(400).send(err);
                }
            });
    } else {
        res.status(400).send("ERROR - url parameters not valid");
    }
});

router.post("/submitClaim", (req, res) => {
    try {
        let pass = req.body.submit_claim;

        auth.verify_token(pass)
            .then((pass) => {
                if (
                    global_helpers.normalise_string(pass.verify_token_username) ===
                    global_helpers.normalise_string(pass.username)
                ) {
                    return user_actions.submit_claim(pass);
                } else {
                    res.status(200).send({
                        success: false,
                        err: "ERROR - Unable to verify user credentials. Please try again, else contact admin",
                        err_code: "credentials_invalid"
                    });
                }
            })
            .then((pass) => {
                try {
                    res.status(200).json({
                        sucess: "Successful"
                    });
                } catch (err) {
                    console.log(err);
                }
            })
            .catch((err) => {
                if (err.err) {
                    res.status(200).json(err);
                } else {
                    console.log(err);
                    res.status(400).send(err);
                }
            });
    } catch (err) {
        console.log(err);
        res.status(400).send("ERROR - url parameters not valid");
    }
});

router.post("/editField", (req, res) => {
    // 1. Do something
    // 2. Do something else

    try {
        let pass = {
            token: req.body.token,
            username: req.body.username,
            password: req.body.password,
            user_or_policy: req.body.user_or_policy,
            username_or_id: req.body.username_or_id,
            field_name: req.body.field_name,
            new_value: req.body.new_value
        };

        auth.verify_token(pass)
            .then((pass) => {
                if (
                    global_helpers.normalise_string(pass.verify_token_username) ===
                    global_helpers.normalise_string(pass.username)
                ) {
                    return user_actions.edit_field(pass);
                } else {
                    console.log(global_helpers.normalise_string(pass.verify_token_username));
                    console.log(global_helpers.normalise_string(pass.purchase.username));
                    console.log("USERNAMES NOT EQUAL!!");

                    res.status(200).send({
                        success: false,
                        err: "ERROR - Unable to verify user credentials. Please try again, else contact admin",
                        err_code: "credentials_invalid"
                    });
                }
            })
            .then((pass) => {
                try {
                    res.status(200).json({
                        sucess: "Successful"
                    });
                } catch (err) {
                    console.log(err);
                }
            })
            .catch((err) => {
                if (err.err) {
                    res.status(200).json(err);
                } else {
                    console.log(err);
                    res.status(400).send(err);
                }
            });
    } catch (err) {
        console.log(err);
        res.status(400).send("ERROR - url parameters not valid");
    }
});

router.post("/editFieldClaim", (req, res) => {
    // 1. Do something
    // 2. Do something else

    try {
        let pass = {
            token: req.body.token,
            username: req.body.username,
            password: req.body.password,
            accident_or_claim: req.body.accident_or_claim,
            id: req.body.id,
            field_name: req.body.field_name,
            new_value: req.body.new_value
        };

        console.log(pass);

        auth.verify_token(pass)
            .then((pass) => {
                if (
                    global_helpers.normalise_string(pass.verify_token_username) ===
                    global_helpers.normalise_string(pass.username)
                ) {
                    return user_actions.edit_field_claim(pass);
                } else {
                    console.log(global_helpers.normalise_string(pass.verify_token_username));
                    console.log(global_helpers.normalise_string(pass.purchase.username));
                    console.log("USERNAMES NOT EQUAL!!");

                    res.status(200).send({
                        success: false,
                        err: "ERROR - Unable to verify user credentials. Please try again, else contact admin",
                        err_code: "credentials_invalid"
                    });
                }
            })
            .then((pass) => {
                try {
                    res.status(200).json({
                        sucess: "Successful"
                    });
                } catch (err) {
                    console.log(err);
                }
            })
            .catch((err) => {
                if (err.err) {
                    res.status(200).json(err);
                } else {
                    console.log(err);
                    res.status(400).send(err);
                }
            });
    } catch (err) {
        console.log(err);
        res.status(400).send("ERROR - url parameters not valid");
    }
});

router.post("/getAccidentsAndClaims", (req, res) => {
    // 1. Do something
    // 2. Do something else

    try {
        let pass = {
            token: req.body.token,
            username: req.body.username
        };

        auth.verify_token(pass)
            .then((pass) => {
                if (
                    global_helpers.normalise_string(pass.verify_token_username) ===
                        global_helpers.normalise_string(pass.username) ||
                    global_constants.list_of_admins.indexOf(
                        global_helpers.normalise_string(pass.verify_token_username)
                    ) !== -1
                ) {
                    return get_data.get_accidents_and_claims(pass);
                } else {
                    console.log("USERNAMES NOT EQUAL!!");

                    res.status(200).send({
                        success: false,
                        err: "ERROR - Unable to verify user credentials. Please try again, else contact admin",
                        err_code: "credentials_invalid"
                    });
                }
            })
            .then((pass) => {
                try {
                    res.status(200).json({
                        sucess: "Successful",
                        accidents_and_claims: pass.get_accidents_and_claims_result
                    });
                } catch (err) {
                    console.log(err);
                }
            })
            .catch((err) => {
                if (err.err) {
                    res.status(200).json(err);
                } else {
                    console.log(err);
                    res.status(400).send(err);
                }
            });
    } catch (err) {
        console.log(err);
        res.status(400).send("ERROR - url parameters not valid");
    }
});

router.post("/getAccident", (req, res) => {
    // 1. Do something
    // 2. Do something else

    try {
        let pass = {
            token: req.body.token,
            username: req.body.username,
            accident_id: req.body.accident_id
        };

        auth.verify_token(pass)
            .then((pass) => {
                if (
                    global_helpers.normalise_string(pass.verify_token_username) ===
                        global_helpers.normalise_string(pass.username) ||
                    global_constants.list_of_admins.indexOf(
                        global_helpers.normalise_string(pass.verify_token_username)
                    ) !== -1
                ) {
                    return get_data.get_accident(pass);
                } else {
                    console.log("USERNAMES NOT EQUAL!!");

                    res.status(200).send({
                        success: false,
                        err: "ERROR - Unable to verify user credentials. Please try again, else contact admin",
                        err_code: "credentials_invalid"
                    });
                }
            })
            .then((pass) => {
                try {
                    res.status(200).json({
                        sucess: "Successful",
                        get_accident_result: pass.get_accident_result
                    });
                } catch (err) {
                    console.log(err);
                }
            })
            .catch((err) => {
                if (err.err) {
                    res.status(200).json(err);
                } else {
                    console.log(err);
                    res.status(400).send(err);
                }
            });
    } catch (err) {
        console.log(err);
        res.status(400).send("ERROR - url parameters not valid");
    }
});

router.post("/cancelOrReinstate", (req, res) => {
    // 1. Do something
    // 2. Do something else

    try {
        let pass = {
            token: req.body.token,
            username: req.body.username,
            policy_id: req.body.policy_id,
            policy_status: req.body.policy_status,
            message: req.body.message,
            cancel_or_reinstate: req.body.cancel_or_reinstate
        };

        auth.verify_token(pass)
            .then((pass) => {
                // pass.username refers to user's username, whereas token belongs to staff, Hence cannot compare username here to token username.
                if (
                    global_helpers.normalise_string(pass.verify_token_username) ===
                        global_helpers.normalise_string(pass.username) ||
                    global_constants.list_of_admins.indexOf(
                        global_helpers.normalise_string(pass.verify_token_username)
                    ) !== -1
                ) {
                    return user_actions.cancel_or_reinstate(pass);
                } else {
                    console.log(global_helpers.normalise_string(pass.verify_token_username));
                    console.log(global_helpers.normalise_string(pass.purchase.username));
                    console.log("USERNAMES NOT EQUAL!!");

                    res.status(200).send({
                        success: false,
                        err: "ERROR - Unable to verify user credentials. Please try again, else contact admin",
                        err_code: "credentials_invalid"
                    });
                }
            })
            .then((pass) => {
                try {
                    res.status(200).json({
                        sucess: "Successful"
                    });
                } catch (err) {
                    console.log(err);
                }
            })
            .catch((err) => {
                if (err.err) {
                    res.status(200).json(err);
                } else {
                    console.log(err);
                    res.status(400).send(err);
                }
            });
    } catch (err) {
        console.log(err);
        res.status(400).send("ERROR - url parameters not valid");
    }
});

router.post("/trackingData", (req, res) => {
    // 1. Do something
    // 2. Do something else
    console.log("API trackingData");

    try {
        let pass = {
            tracking_data: req.body.tracking_data
        };

        db("tracking")
            .insert({ data: JSON.stringify(pass.tracking_data) })
            .then((result) => {
                console.log("SUCCESS");
                res.status(200).json({
                    success: true
                });
            })
            .catch((err) => {
                console.log(err);
                console.log("ERROR");
                res.status(200).json({
                    success: true
                });
            });
    } catch (err) {
        console.log(err);
        res.status(500).json({
            success: false,
            err
        });
    }
});

router.post("/feedback", (req, res) => {
    console.log("API feedback");

    let pass = {
        send_email_email_address: global_constants.staging
            ? "yibu.wang@ifinancialsingapore.com"
            : "adele@ifinancialsingapore.com",
        send_email_subject: "Floating Button Feedback Form",
        send_email_message: `
        <html>
         <b>Sender Details</b><br/>
         Name: ${req.body.name} <br/>
         Email: ${req.body.email}<br/>
         Message: ${req.body.message}<br/>
         Product suggestions: ${req.body.suggestions}<br/>
         Subscribe to promotions and deals: ${req.body.keep_me_informed}<br/>

         </html>
         `
    };

    // pass
    email
        .send_email({
            to: pass.send_email_email_address,
            subject: pass.send_email_subject,
            text: "",
            html: pass.send_email_message
        })
        .then(() => {
            res.status(200).json({
                success: true
            });
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json({
                success: false
                // err
            });
        });
});

router.post("/addToMailingList", (req, res) => {
    console.log("API addToMailingList");

    let pass = {
        send_email_email_address: global_constants.staging
            ? "yibu.wang@ifinancialsingapore.com"
            : "adele@ifinancialsingapore.com",
        send_email_subject: `Interest Express from promotion ${req.body.promo}`,
        send_email_message: `
        <html>
         <b>Sender Details</b><br/>
         Name: ${req.body.name} <br/>
         Email: ${req.body.email}<br/>
         Mobile: ${req.body.mobile}<br/>
         </html>
         `
    };

    // pass
    email
        .send_email({
            to: pass.send_email_email_address,
            subject: pass.send_email_subject,
            text: "",
            html: pass.send_email_message
        })
        .then(() => {
            res.status(200).json({
                success: true
            });
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json({
                success: false,
                err
            });
        });
});

module.exports = router;
