// const axios = require('axios');
const request = require("request");
// const https = require('https');

// // default for all https requests
// // (whether using https directly, request, or another module)
// require('https').globalAgent.options.ca = rootCas;

// axios.get('https://prs.moh.gov.sg/prs/internet/profSearch/mgetSearchSummaryByName.action')
//     .then(response => {
//         console.log(response);
//         console.log("SUCCESS");

//     })
//     .catch(error => {
//         console.log(error);
//         console.log("FAILED");

//     });

// var secureContext = require('tls').createSecureContext({
//     ca: rootCas
//   // ...
//   });

// //var reqData = "username=ganesh&password=123456&grant_type=password";
// axios({
//     method: 'post',
//     url: 'https://prs.moh.gov.sg/prs/internet/profSearch/mgetSearchSummaryByName.action',
//     // httpsAgent: new https.Agent({
//     //     rejectUnauthorized: false
//     // })
//     //   strictSSL: false,
//     //   withCredentials: true,
//     //   crossdomain: true,
//     //   data: $.param (reqData),
//     //   headers: {
//     //     // "Content-Type": "application/x-www-form-urlencoded",
//     //     // "Cache-Control": "no-cache",
//     //     // "Postman-Token": "42e6c291-9a09-c29f-f28f-11872e2490a5"
//     //   }
// }).then(function (response) {
//     console.log("Heade With Authentication :" + response);
// }).catch(function (error) {
//     console.log("Post Error : " + error);
// });

// postman_request('https://prs.moh.gov.sg/prs/internet/profSearch/mgetSearchSummaryByName.action', function (error, response, body) {
//     console.log('error:', error); // Print the error if one occurred
//     console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
//     console.log('body:', body); // Print the HTML for the Google homepage.
// });

// const agent = new https.Agent({
//     rejectUnauthorized: false
// });
// axios.post('https://prs.moh.gov.sg/prs/internet/profSearch/mgetSearchSummaryByName.action', {
//     // "rejectUnauthorized": false,
//     httpsAgent: agent,

// }).then(response => {
//     console.log(response);
//     console.log("SUCCESS");
// }).catch(error => {
//     console.log(error);
//     console.log("FAILED");
// });

request(
  {
    rejectUnauthorized: false,
    url:
      "https://prs.moh.gov.sg/prs/internet/profSearch/mgetSearchSummaryByName.action",
    method: "GET",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      Cookie:
        "cookie.tableId=DEFAULT; cookie.currentPage=DEFAULT.4; JSESSIONID=2Qm4c1zMSnDgKwDHQXGz1xGBj6SB1KVpm92Lpvh4FvhLyylQGn1G!2126923699!-1592288597!1556050924394; Cookie=!Xe7uCfyXAfviz2If/6Px2nmG9boxpNSjh/26G2VNa8KRsYtbzGNr80IoVVOTRs7oQHTspPinLhDAcw==; s_ppn=no%20value; s_cc=true; s_ppvl=https%253A%2F%2Fwww.moh.gov.sg%2Fhpp%2Fall-healthcare-professionals%2Fhealthcare-professionals-search%2C91%2C91%2C754%2C1536%2C754%2C1536%2C864%2C1.25%2CP; s_ppv=https%253A%2F%2Fwww.moh.gov.sg%2Fhpp%2Fall-healthcare-professionals%2Fhealthcare-professionals-search%2C100%2C91%2C825%2C1536%2C754%2C1536%2C864%2C1.25%2CP"
    }
  },
  function(err, response, body) {
    if (err) {
      console.log(err);
      console.log("FAILED");
    } else {
      // console.log(response);
      console.log(body);
      console.log("SUCCESS");
    }
  }
);

// const {Builder, By, Key, until} = require('selenium-webdriver');

// (async function example() {
//   let driver = await new Builder().forBrowser('firefox').build();
//   try {
//     await driver.get('http://www.google.com/ncr');
//     await driver.findElement(By.name('q'));.sendKeys('webdriver', Key.RETURN);
//     await driver.wait(until.titleIs('webdriver - Google Search'), 1000);
//   } finally {
//     await driver.quit();
//   }
// })();
