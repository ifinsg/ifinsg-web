import NProgress from 'nprogress';
import Router from 'next/router';

NProgress.configure({ showSpinner: false });

Router.events.on('routeChangeStart', (url: string) => {
  process.env.NODE_ENV !== 'production' && console.log('[NProgress] Route to', url);
  NProgress.start();
});

Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());
