import React, { useEffect, useState } from 'react';
import Badbrowser from '@components/Badbrowser';
import { detect } from 'detect-browser';
import { NoSsr } from '@material-ui/core';

interface IProps {
    supportBrowser: string[]
}

const BrowseDetectProvider: React.FC<IProps> = (props) => {

    const browser = detect();
    if (browser && browser.name && !props.supportBrowser.includes(browser.name)) {
        console.log('browser : ', browser.name)
        return <NoSsr>
            <Badbrowser />
        </NoSsr>
    }
    return (
        <>
            {props.children}
        </>
    );
};

export default BrowseDetectProvider;
