import { createGlobalStyle } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
  /* -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale; */
  
  
  body {
    font-family: Lato;
  } 
 

  /* 
  #__next {
    height: 100%;
  }

  .ant-message {
    color: rgba(0, 0, 0, 0.75);
  }

  .ant-layout {
    height: 100%;

    .ant-layout-header {
      height: 56px;
      line-height: 56px;
      padding: 0 20px;
      background: white;
    }

    .ant-layout-sider {
      box-shadow: 0 4px 6px rgba(0, 0, 0, 0.075);
    }

    .ant-menu-inline, .ant-menu-vertical, .ant-menu-vertical-left {
      border-right: none;
    }
  } 
  */

  #nprogress{pointer-events:none}#nprogress .bar{background:#7cda24;position:fixed;z-index:1031;top:0;left:0;width:100%;height:2px}#nprogress .peg{display:block;position:absolute;right:0;width:100px;height:100%;box-shadow:0 0 10px #7cda24,0 0 5px #7cda24;opacity:1;-webkit-transform:rotate(3deg) translate(0px,-4px);-ms-transform:rotate(3deg) translate(0px,-4px);transform:rotate(3deg) translate(0px,-4px)}#nprogress .spinner{display:block;position:fixed;z-index:1031;top:15px;right:15px}#nprogress .spinner-icon{width:18px;height:18px;box-sizing:border-box;border:solid 2px transparent;border-top-color:#7cda24;border-left-color:#7cda24;border-radius:50%;-webkit-animation:nprogress-spinner 400ms linear infinite;animation:nprogress-spinner 400ms linear infinite}.nprogress-custom-parent{overflow:hidden;position:relative}.nprogress-custom-parent #nprogress .spinner,.nprogress-custom-parent #nprogress .bar{position:absolute}@-webkit-keyframes nprogress-spinner{0%{-webkit-transform:rotate(0deg)}100%{-webkit-transform:rotate(360deg)}}@keyframes nprogress-spinner{0%{transform:rotate(0deg)}100%{transform:rotate(360deg)}}
`;
