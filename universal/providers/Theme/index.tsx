import React from 'react';

import '@components/Loaders/ProgressBar';
import { GlobalStyles } from './Styled';

const ThemeProvider: React.FC<{}> = (props) => {
  return (
    <>
      <GlobalStyles />
      {props.children}
    </>
  );
};

export default ThemeProvider;
